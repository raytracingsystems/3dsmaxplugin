/*
Author: Ray Tracing Systems.
Overrride "target" tags in selected objects with HydraMtlTags materials.
*/


/*
Targets list: 

Undefined 				= 0 
PCscreen 					= 1 
PCkeyboard 				= 2
MacKeyboard 			= 3 
SmartphoneScreen 		= 4
TabletScreen 			= 5
RulerStraight			= 6
RulerAngular 			= 7
RulerProtractor 			= 8
Notepad 					= 9 
BoxPaperclips 			= 10
BoxStaples				= 11 
PictureHorizontal 		= 12 
PictureVertical 			= 13
ClockCircle 				= 14
ClockSquare 				= 15
ClockRecrangular 		= 16
GlossyMetal 				= 17
GlossyMetalHole 		= 18
GlossyPlastic 			= 19
GlossyPlasticHole 		= 20
DiffuseTag 				= 21
GlassClear 				= 22
Chrome 					= 23
CalendarOrPosterHoriz = 24
Book 						= 25
Whiteboard 				= 26
StorageBox 				= 27
CalendarOrPosterVert 	= 28
*/

clearListener()

newTarget = 15

(--start ms		
	for obj in selection do 
	(			
		obj.material.texTarget = newTarget 
	)		
	print "Complete override target tags."
)-- end ms