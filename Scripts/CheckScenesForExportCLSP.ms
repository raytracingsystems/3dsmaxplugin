/*
Last change:  08.05.2020
Author: Ray Tracing Systems.
Check all scenes for export in CLSP.
*/
-----------------------------------------------------------------------------------------------------------
	
rootPath 	= @"D:\HDD Storage\CLOUDS\YandexDisk\samsung_2019\3Dmodels_3dsmax_lib\"

if (not isDirectoryWriteable rootPath) do
(
	--rootPath = @"C:\Users\trofi\YandexDisk\samsung_2019\3Dmodels_3dsmax_lib\"
	rootPath = @"d:\Works\GeneratorInteriors\Test\"
)

-----------------------------------------------------------------------------------------------------------

fn CheckMat currMat fileName &numMatError = 
(	
	if (currMat == undefined) do
	(
		print "Error: The material has an undefined type."
		format "Scene : %\n\n" fileName
		numMatError += 1		
		return 0
	)
	
	if (currMat.name == undefined) do
	(
		print "Error: The material doesn't have a name."
		format "Scene : %\n\n" fileName
		numMatError += 1		
		return 0
	)
	
	if (classof currMat != HydraMtlTags) do 
	(
		print "Error: The material no HydraMtlTags type."
		format "Scene : %\n" fileName
		format "Material: %\n\n" currMat.name
		numMatError += 1		
		return 0
	)
			
	if (currMat.texTarget == 0 and currMat.matAny == on) do
	(
		print "Error: The material has any type and an indefinite target."
		format "Scene : %\n" fileName
		format "Material: %\n\n" currMat.name
		numMatError += 1				
	)	
)

fn CheckAnyMat sceneMaterials fileName &numMatError = 
(
	for mat in sceneMaterials do
	(
		if (classof mat == multiSubMaterial) then
			for subMat in mat.material do				
				CheckMat subMat fileName &numMatError					
		else
			CheckMat mat fileName &numMatError
	)
)


fn Main =
(	
	escapeEnable = true	
	actionMan.executeAction 0 "40472"  -- MAXScript Listener
	clearListener()
	

	arrayFullFileNames	= getFiles (rootPath + "*.max") recurse:true	
	numAllScenes 		= arrayFullFileNames.count 
	numCurrScene 		= 0
	numMatError			= 0
	numGeometryError	= 0	
	numRenameObj		= 0
	
	print "Scene scanning is running, please wait..."
	progressStart "Scene scanning progress" 	

	for f in arrayFullFileNames do
	(
		fileName = getFilenameFile f
		
-- 		maxVer = getMaxFileVersionData f		
-- 		if (maxVer[1] > 19000) do
-- 		(
-- 			format "Error: Not 2017 3ds Max ver. Scene: % \n" fileName
-- 			numGeometryError += 1			
-- 		)

		loadMAXFile f useFileUnits:true quiet:true
					
		-- Check object.
		if (objects.count > 1) then
		(
			print "Error: Many objects."
			format "Scene: %\n\n" fileName
			numGeometryError += 1			
		)				
		else
		(
			-- renaming an object with the file name.
			
			formatFileName	= filterString fileName " _0123456789"			
			arrayCount 		= formatFileName.count 			
			
			if (arrayCount > 2) then	
			(					
				Print "The name has more than two affixes (prefixes or suffixes). Only one prefix is allowed."
				format "File name: %\n\n" fileName
				numGeometryError += 1
			)
			else
			(
				newName
				case arrayCount of
				(
					1: newName = formatFileName[1]
					2: newName = formatFileName[2]			
				)
				--format "Rename object from \"%\" to \"%\"\n" geometry[1].name newName
				geometry[1].name = newName
				numRenameObj	+= 1
			)
		)
		
		-- Check materials.			
		CheckAnyMat sceneMaterials fileName &numMatError	
		
		progressUpdate ((numCurrScene as float) / ((numAllScenes-1) as float) * 100.0)
		if (getProgressCancel()) do
		(
			print "Cancel."
			progressEnd() 
			return 0	
		)
		numCurrScene += 1
		
		saveMaxFile f
	)

	progressEnd() 

	
	if 	(numGeometryError > 0 or numMatError > 0)	then
	(
		print "------------------------------------------------------"
		print "The scan is complete. Errors were detected."		
		format "Number geometry error:  %\t\n" numGeometryError 
		format "Number materials error: %\t\n" numMatError 
		format "Number rename objects:  %\t\n" numRenameObj 
		format "Number all scenes:      %\t\n" numAllScenes
		print "Please fix all errors and repeat the check again."
	)
	else 	
	(
		print "------------------------------------------------------"
		print "The scan is complete."
		format "Num all scenes: %\t\n" numAllScenes
		print "All the scenes are correct, thank you )"	
	)
)

Main()