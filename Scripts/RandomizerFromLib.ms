/*
Last change:  07.05.2020
Author: Ray Tracing Systems.
Random HydraMtlTags materials from special material library.
*/


useGUI 			= on

-- if not GUI
chkRndObj 		= off
chkRndMat 		= on
chkLights 		= off
chkRndSelOnly 	= off


rootPath 			= @"D:\HDD Storage\CLOUDS\YandexDisk\"

if (not isDirectoryWriteable rootPath) do
	rootPath 		= @"C:\Users\trofi\YandexDisk\"


modelLibPath 	= rootPath + @"GeneratorInteriors\3Dmodels_3dsmax_lib\"
textureLibPath 	= rootPath + @"CLSP\3dsmax\Materials_lib\1\Materials_lib.mat"

-----------------------------------------------------------------------------------------------------------------------------------------

mapPaths.add       (modelLibPath + @"BagsCasesBackpacks")

int fn RandomModel currObj arrayFullFileNames &arrayObjForDelete = 
(		
	format "Model name: % \n" currObj.name	
	modelName = filterString currObj.name " 0123456789"
	if (modelName[1] == undefined) do
	(
		print "Model no name."
		return 0
	)
		
	modelNamePattern = "*" + modelName[1] + "*"
		
	numFoundObj = 1
	arrayFindModels = #()
		
	for f in arrayFullFileNames do
	(		
		fileName = getFilenameFile f			
				
		if (matchPattern fileName pattern:modelNamePattern) then
		(			
			arrayFindModels[numFoundObj] = f
			numFoundObj += 1
		)
	)	
	
	if (numFoundObj == 1) do
	(
		print "Object not found in library."
		return 0
	)	
							
	format "Object found in library: %.\n" (numFoundObj - 1)
	--print arrayFindModels
		
	rndNum = random 1 (numFoundObj-1)
	--format "rndNum: %\n " rndNum 
	selectedMaxFile = arrayFindModels[rndNum]
	format "Selected file: %\n" selectedMaxFile
	
	if((mergeMAXFile selectedMaxFile #autoRenameDups #renameMtlDups quiet:true)==false) do
	(
		print "Not merge object."
		return 0
	)
	
	
	-- Align the new object with the old one, taking into account the size.
	newObj 				= getLastMergedNodes()	
	newObj 				= newObj[1] 
	newObj.transform	= currObj.transform				
	offsetCenter			= newObj.center - newObj.min
	newPos 				= currObj.min + offsetCenter
	newObj.pos.z 		= newPos.z
	
	arrayObjForDelete	+= currObj
	
	return 1
)
	
	
fn FindMatInLibrary sourceMat currObj =
(
	arrayMat 	= #()
	num 			= 0
	
	for matInLib in currentMaterialLibrary do
	(
		num += 1
		if (classof matInLib != HydraMtlTags) do continue
		
		if (classof sourceMat != HydraMtlTags) do 
		(
			print "The material is not HydraMtlTags type."
			format "Material: %\n" sourceMat.name
			format "Object:  %\n" currObj.name			           
			continue
		)
		
		if (sourceMat.texTarget == 0 and sourceMat.matAny == on) do
		(
			message = "The material has any type and an indefinite target. It may not have the right type or target assigned to it.\n"
			message += "Material: " 	+ sourceMat.name + "\n"
			message += "Object:    " 	+ currObj.name 	+ "\n"
			
			messageBox message title:"Warning" icon:#warning
			return Undefined
		)
		
		if ((sourceMat.texTarget 		!=  0	  and sourceMat.texTarget == matInLib.texTarget) or
			(sourceMat.texTarget 		==  0 and 	(	
			(sourceMat.matAny 			== on and matInLib.matAny 			== on) or 
			(sourceMat.matCeramic	== on and matInLib.matCeramic 	== on) or 
			(sourceMat.matConcrete 	== on and matInLib.matConcrete	== on) or
			(sourceMat.matEmission 	== on and matInLib.matEmission	== on) or
			(sourceMat.matFabric 		== on and matInLib.matFabric		== on) or
			(sourceMat.matGlass 		== on and matInLib.matGlass			== on) or
			(sourceMat.matLeather 	== on and matInLib.matLeather		== on) or
			(sourceMat.matLiquid 		== on and matInLib.matLiquid		== on) or
			(sourceMat.matMarble 		== on and matInLib.matMarble		== on) or
			(sourceMat.matMetal 		== on and matInLib.matMetal		== on) or
			(sourceMat.matOrganic  	== on and matInLib.matOrganic		== on) or 
			(sourceMat.matPaint  		== on and matInLib.matPaint			== on) or
			(sourceMat.matPaper  		== on and matInLib.matPaper		== on) or
			(sourceMat.matPlastic  	== on and matInLib.matPlastic		== on) or
			(sourceMat.matRubber  	== on and matInLib.matRubber		== on) or
			(sourceMat.matWood 		== on and matInLib.matWood		== on)))) do 
			(                                                           
				--format "Find mat: %\n" matInLib				
				append arrayMat num 		
			)
	)
	format "arrayFindMat: %\n" arrayMat
	--format "arrayMat.count: %\n" arrayMat.count
	seed (timeGetTime()/100)
	rndNum = random 1 arrayMat.count
	newRndMat = arrayMat[rndNum] 	
	format "Asign new random mat: %\n\n" newRndMat
	
 	newRndMat
)
	
fn GetNewMat &mat currObj &numChangeMat = 
(
	findMat = FindMatInLibrary mat currObj	
		
	if (findMat != undefined) do 
	(				
		newMat = currentMaterialLibrary[findMat]	
		numChangeMat	 += 1
		
		if (mat.rndOverrideColor == on) do
		(
			newMat.mtl_diffuse_color = mat.rndColor
			format "Override color: % \n" newMat.mtl_diffuse_color
		)		
		mat = newMat
	)				
)

fn RandomMaterials &currObj &numChangeMat = 
(		
	format "Object name: %\n" currObj
	currMat = currObj.material	
	
	if (classof currMat == multiSubMaterial) then
	(				
		numMat = 1
		for subMat in currMat do
		(				
			if (classof subMat != HydraMtlTags) do continue
			
			format "subMat: %\n" subMat 					
			idMat = currMat.materialIDList[numMat]
			GetNewMat &currObj.material[idMat] currObj &numChangeMat
			numMat += 1
		)
	)	
	else
	(
		if (classof currMat != HydraMtlTags) do return 0
			
		format "sourceMat:%\n" currMat		
		GetNewMat &currObj.material currObj &numChangeMat
	)	
)

fn RandomLights &currlight &numChangeLights =
(
	rnd = random 0 1
	currlight.on = rnd
	numChangeLights += 1
)

----------------------------------------------------------------------------------------------------------------------------------------
-- Main
----------------------------------------------------------------------------------------------------------------------------------------
fn Main chkRndObj chkRndMat chkLights chkRndSelOnly = 
(
	escapeEnable = true	
	clearListener()			
				
	arrayFullFileNames 	= getFiles (modelLibPath + "*.max") recurse:true
	
			
	print "-------------------------"
	print "Randomize objects."
	print "-------------------------"
	
	numMergeObj 	= 0	
		
	if (chkRndObj) do
	(
		arrayObjForDelete 	= #()		
		
		if (chkRndSelOnly) then
			arrayOldObj = for o in selection where classof o != TargetObject collect o				
		else
			arrayOldObj = for o in geometry where classof o != TargetObject collect o			

		for currObj in arrayOldObj do
			numMergeObj += RandomModel currObj arrayFullFileNames &arrayObjForDelete	
		
		delete arrayObjForDelete
	)
	
	
	print "-------------------------"
	print "Randomize materials."
	print "-------------------------"
	
	numChangeMat = 0		

	if (chkRndMat) do
	(		
		loadLibrary = loadMaterialLibrary textureLibPath
	
		if(not loadLibrary) do
		(
			messageBox "Material library not found." title:"Error" icon:#critical
			return false
		)

		if (chkRndSelOnly) then
			arrayOldObj = for o in selection where classof o != TargetObject collect o				
		else
			arrayOldObj = for o in geometry where classof o != TargetObject collect o		
	
		for currObj in arrayOldObj do
		(				
			if (currObj != undefined) do					
				RandomMaterials &currObj &numChangeMat
		)
	)	
	
	------------------------------
	-- Randomize lights
	------------------------------
		
	numChangeLights = 0		
	
	if (chkLights) do
	(
		if (chkRndSelOnly) then
			arrayLights = for o in selection where classof o != TargetObject and superClassOf o == light collect o				
		else
			arrayLights = for o in lights where classof o != TargetObject and superClassOf o == light collect o		
				
		for currlight in arrayLights do
		(	
			InstanceMgr.GetInstances currlight &instances
			InstanceMgr.MakeObjectsUnique &instances #prompt
			
			if (currlight != undefined) do					
				RandomLights &currlight &numChangeLights
		)
	)
	
	print "----------------------------------------------------------------"	
	format "Random objects:   %\t\n" numMergeObj 	
	format "Random materials: %\t\n" numChangeMat			
	format "Random lights:    %\t\n" numChangeLights		
	print "Random models and materials complete."
			
	
)

rollout mainRollout "Randomizer" width:200 height:350
(
	label 'lbl1' "Randomization of objects and HydraMtlTags of materials from the library." pos:[10,10] width:180 height:90 align:#left
	GroupBox 'grp1' "Randomize" pos:[10,110] width:180 height:230 align:#left								
	checkbox 'chk_obj' "objects" pos:[20,140] width:120 height:20 checked:false align:#left
	checkbox 'chk_mat' "materials" pos:[20,160] width:120 height:20 checked:true align:#left		
	checkbox 'chk_lights' "lights" pos:[20,180] width:120 height:20 checked:false align:#left
	checkbox 'chk_selOnly' "selection only" pos:[20,220] width:120 height:20 checked:false align:#left		
	button 'btn_apply' "Apply" pos:[40,270] width:120 height:30 align:#left		
		
	on btn_apply pressed do
	(	        
		chkRndObj 		= chk_obj.checked
		chkRndMat 		= chk_mat.checked			
		chkLights		= chk_lights.checked
		chkRndSelOnly 	= chk_selOnly.checked		
		
		Main chkRndObj chkRndMat chkLights chkRndSelOnly  	
	)
)

if useGUI then
	createDialog mainRollout
else
	Main chkRndObj chkRndMat chkLights chkRndSelOnly  	
