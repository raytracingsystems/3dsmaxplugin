/*
Last change:  27.09.2022
Author: Ray Tracing Systems.
Rescale and enter an object in bounding box [-1, 1]
*/

-----------------------------------------------------------------------------------------

(	
	escapeEnable = true	
	actionMan.executeAction 0 "40472"  -- MAXScript Listener
	clearListener()
	 	
	for obj in selection do
	(	
		lenghtX = abs(obj.min[1] - obj.max[1])
		lenghtY = abs(obj.min[2] - obj.max[2])
		lenghtZ = abs(obj.min[3] - obj.max[3])
		
		maxLenght = amax #(lenghtX, lenghtY, lenghtZ)		
				
		scaleMult = 2.0 / maxLenght
						
		format "lenghtX: % \n" lenghtX
		format "lenghtY: % \n" lenghtY
		format "lenghtZ: % \n"lenghtZ
		format "maxLenght: % \n" maxLenght 		
		format "scaleMult: % \n" scaleMult
		
		obj.scale *= scaleMult
		obj.pos = [0, 0, 0]
		
		ResetXForm obj
		collapseStack obj
	)		
)