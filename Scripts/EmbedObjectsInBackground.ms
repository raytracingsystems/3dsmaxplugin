/*
Last change: 11.12.2019
Author: Ray Tracing Systems.
Embed objects in the background. Catcher object should be called a "Catcher", and can be of any shape and material. 
You do not need to use HydraMtlCatcher for this method.
*/


fn ClampZeroOne x = 
(
	if 			x < 0.0 then x = 0.0 
	else if 	x > 1.0 do x = 1.0	
	
	x -- return x
)

fn Compos bmAll bmCatcher bmObj bmBack &bmRes = 
(		
	progressStart "Compositing progress" 	
		
	for y in 0 to bmBack.height-1 do
	(
		progressUpdate ((y as float) / ((bmBack.height-1) as float) * 100.0)
		if (getProgressCancel()) do exit	
		
		for x in 0 to bmBack.width-1 do		
		(				
			bmAllPixel 	= getPixels bmAll	  [x, y] 1 linear:true		
			bmCatcherPixel 	= getPixels bmCatcher [x, y] 1 linear:true		
			bmObjPixel 	= getPixels bmObj 	  [x, y] 1 linear:true
			bmBackPixel 	= getPixels bmBack 	  [x, y] 1 linear:true
		
			-- Compos back with shadow
			shadowR = bmBackPixel[1].r  * (bmAllPixel[1].r  / (bmCatcherPixel[1].r + 0.0001))
			shadowG = bmBackPixel[1].g * (bmAllPixel[1].g / (bmCatcherPixel[1].g + 0.0001))
			shadowB = bmBackPixel[1].b * (bmAllPixel[1].b / (bmCatcherPixel[1].b + 0.0001))
							
			myAlpha = ClampZeroOne bmObjPixel[1].a
							
			-- Add objects						             
			resR = shadowR + myAlpha * (bmAllPixel[1].r - shadowR)
			resG = shadowG + myAlpha * (bmAllPixel[1].g - shadowG)
			resB = shadowB + myAlpha * (bmAllPixel[1].b - shadowB)

			setPixels bmRes[x, y] #((Color resR resG resB))				
		)			
	)	
	progressEnd() 
)

clearListener()

(--start ms
hasCatcher = false
for i in geometry  do
(
	if (i.name == "Catcher") do
		hasCatcher = true	
)

if (hasCatcher == false) then
(
	messageBox "The receiving object must be named \"Catcher\"."	
)
else
(	
	-- Render pass all
	unhide geometry	
	print "Render pass 1"
	actionMan.executeAction 0 "50030"  -- Render: Render Last
	bmAll = GetLastRenderedImage copy:true
	--display bmAll

	-- Render pass only catcher
	print "Render pass 2"
	hide geometry	
	unhide $Catcher	
	actionMan.executeAction 0 "50030"  -- Render: Render Last
	bmCatcher = GetLastRenderedImage copy:true
	--display bmCatcher

	-- Render pass only objects
	print "Render pass 3"
	unhide geometry	
	hide $Catcher	
	actionMan.executeAction 0 "50030"  -- Render: Render Last
	bmObj = GetLastRenderedImage copy:true
	--display bmObj

	-- Render only backgroun
	print "Render pass 4"
	hide geometry	
	myBox = box length:1 width:1 height:1 -- Hydra render cannot render without objects.
	myBox.pos = [10000, 0, 0]	
	actionMan.executeAction 0 "50030"  -- Render: Render Last
	bmBack = GetLastRenderedImage copy:true
	delete myBox
	--display bmBack
	
	-- Composite
	bmRes = GetLastRenderedImage copy:true
	Compos bmAll bmCatcher bmObj bmBack &bmRes
	display bmRes
	
	-- Finish
	free bmBack	
	free bmAll
	free bmCatcher
	
	unhide geometry 
	print "Compositing complete."
	
)
)