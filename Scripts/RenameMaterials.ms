/*
Author: Ray Tracing Systems.
Rename materials.
*/

clearListener()

newName = "bebe"

(--start ms			
	
	iterr = 0
	
	for obj in selection do 
	(		
		newNameFull = newName + "_" + (iterr as string)
		obj.material.name = newNameFull 
		format "Material rename in % \n" newNameFull 
		iterr += 1
	)		
)-- end ms