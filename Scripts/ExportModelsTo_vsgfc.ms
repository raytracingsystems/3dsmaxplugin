/*
Last change: 29.08.2019
Author: Ray Tracing Systems.
Exporting objects in "vsgfc" the format of the Hydra renderer.
*/

-- Need build hydra renderer plugin with:
-- #define  BUILD_FOR_EXPORT_COMPRESS_MESHES true ("hydraRender mk3.h")
-- which turn on a  "COMPRESS_MESHES" checkbox in all export scenes, because there is no way to press it through the max script.


hydraDataPath	= @"c:\[Hydra]\pluginFiles\scenelib\data\"		
rootPath 			= @"C:\Users\mtrofimov\YandexDisk\samsung_2019\"
-- rootPath             =@"d:\RayTracingSystems\TestExport\"
models3dsmaxlib	= @"3Dmodels_3dsmax_lib"
modelsLib			= @"3Dmodels_lib\"

skipExisting = true


---------------------------------------------------------------------------------------------------------------------------------


numExportScenes = 0
numAllScenes = 0	

escapeEnable = true	
actionMan.executeAction 0 "40472"  -- MAXScript Listener
clearListener()


fn ExportModels startDir =
(
	dirArray  = GetDirectories (startDir + "*")

	for d in dirArray do 
	(
		--print d		
		
		currDirIn = substituteString d (rootPath + models3dsmaxlib) ""
		currDirIn = trimRight currDirIn "\\"
		--print currDirIn
		
		currDirOut = rootPath + modelsLib + currDirIn	
		--print currDirOut 
		
		makedir currDirOut
		
		files = getFiles (d + "*.max")
		print files
		
		for f in files do
		(
			fileName = getFilenameFile f

			fullFileNameInBase = currDirOut + "\\" + fileName + ".vsgfc"
			hasFileInBase = doesFileExist(fullFileNameInBase)
			
			if (skipExisting and hasFileInBase) do 
			(
				format "file % exist. Skip it.  \n" fileName
				continue
			)
					
			if (loadMAXFile f useFileUnits:true quiet:true) do 
				numExportScenes += 1			
			
			if (objects.count > 1) do 
			(
				messagebox "There should be only 1 object with material in the scene. Remove foreign objects." title:"Error"		
				actionMan.executeAction 0 "50029" --open scene selector
				return 0
			)
			
			ResetXForm objects[1]
			
			renderers.current = Hydra_Renderer()
			render vfb:false
		
			deleteFile (fullFileNameInBase)
			copyFile (hydraDataPath + "chunk_00001.vsgfc") (fullFileNameInBase)
			copyFile (hydraDataPath + "chunk_00002.vsgfc") (fullFileNameInBase) -- if chunk_00001.vsgfc does not exist.

			format "Export model: % \n" fileName
			
			numAllScenes += 1
		)

		ExportModels d 

	)
)

(--start ms			
	
	startDir = rootPath + models3dsmaxlib
		
	ExportModels startDir 	
	
	format "Export % models from % \n" numExportScenes numAllScenes
)-- end ms