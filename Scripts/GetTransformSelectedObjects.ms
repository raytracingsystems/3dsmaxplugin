/*
Last change:  13.02.2023
Author: Ray Tracing Systems.
Print transform selections objects.
*/

-- THE OBJECT MUST BE WITHOUT A TARGET!

-----------------------------------------------------------------------------------------

(
	escapeEnable = true	
	actionMan.executeAction 0 "40472"  -- MAXScript Listener
	clearListener()
	 
	maxFileNameShot = trimRight maxFileName ".max"
	logFile = createFile (maxFilePath + "TransformObjects_" + maxFileNameShot + ".txt")
	format "Scene: %\n\n" maxFileName to:logfile
	
	for obj in selection do
	(			
		hydraPos = (point3 obj.pos.x obj.pos.z -obj.pos.y) / 100.0
		objRot     = quatToEuler2 obj.rotation.controller.value
		objRot     = point3 objRot.x objRot.y objRot.z
		hydraRot = point3 objRot.x objRot.z objRot.y
		
		format "Object: %\n Position:\t%\n Rotation:\t%\n HPosition:\t%\n HRotation:\t%\n Scale:\t%\n\n" \
		obj.name obj.pos objRot hydraPos hydraRot obj.scale 
		
		
		format "Object: %\n Position:\t%\n Rotation:\t%\n HPosition:\t%\n HRotation:\t%\n Scale:\t%\n\n" \
		obj.name obj.pos objRot hydraPos hydraRot obj.scale to:logFile
	)	
	
	close logFile
)