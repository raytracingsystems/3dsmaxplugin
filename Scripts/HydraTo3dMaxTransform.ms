/*
Last change:  29.04.2022
Author: Ray Tracing Systems.
Convert Hydra to 3ds Max transform.
*/

hPos = point3 -1.65051 3.59635 -98.9018
hRot = point3 90 -158.012 45

-----------------------------------------------------------------------------------------

fn HydraTo3dMaxPos hydraPos =
(
	maxPos	= (point3 hydraPos.x -hydraPos.z hydraPos.y) * 100.0
	maxPos
)
	
fn HydraTo3dMaxRot hydraRot =
(
	maxRot	= point3 hydraRot.x (180.0-hydraRot.z) (180-hydraRot.y)
	maxRot
)

(
	escapeEnable = true	
	actionMan.executeAction 0 "40472"  -- MAXScript Listener
	clearListener()
	
	oldPos = $.pos
	oldRot = quatToEuler2 $.rotation
	format "Old pos: %\n" oldPos
	format "Old rot: %\n" oldRot

	newPos 	= HydraTo3dMaxPos hPos
	newRot 	= HydraTo3dMaxRot hRot
	newRot    	= eulerAngles newRot.x newRot.y newRot.z
	
	format "New pos: %\n" newPos
	format "New rot: %\n" newRot
	
	$.pos       	= newPos 
	$.rotation	= eulerToQuat newRot order:1	
)