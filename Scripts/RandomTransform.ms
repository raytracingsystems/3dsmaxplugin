posX = 0
posY = 0
posZ = 0

rotX = 0
rotY = 0
rotZ = 0

scaleMult = 0.2

for i in selection do 
(
	i.position 					+= random [-posX, -posY, posZ] [posX, posY, posZ]
	i.rotation.x_Rotation 	+= random -rotX rotX
	i.rotation.y_Rotation 	+= random -rotY rotY
	i.rotation.z_Rotation 	+= random -rotZ rotZ
	i.scale 						*= random (1.0-scaleMult) (1.0+scaleMult)
)