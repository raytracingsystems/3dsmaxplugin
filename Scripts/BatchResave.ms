/*
Last change:  24.047.2021
Author: Ray Tracing Systems.
Resave scenes to old 3ds Max.
*/


--rootPath = @"C:\Users\trofi\YandexDisk\samsung_2019\3Dmodels_3dsmax_lib\"
--rootPath = @"C:\Users\mtrofimov\YandexDisk\samsung_2019\3Dmodels_3dsmax_lib\"
rootPath = @"d:\Works\Ray-Tracing_Systems\HydraDevelop\3dsMaxPlugin\zz_tests\"
	
-- you can re-save to the previous three versions, starting with the current one.
-- For example, the 2021 version may resave to the 2020-2019 version.

needMaxVer = 2021 
	
	
-------------------------------------------------------------------------
	
fn GetMaxRawVer maxVer =
(
	-- 2017 (19000), 2018 (20000), 2019 (21000), 2020 (22000), 2021 (23000), 2022 (24000)
	
	maxVerArr = #(19000, 20000, 21000, 22000, 23000, 24000) -- start from 2017 ver (19000)
	
	return maxVerArr[maxVer - 2017 + 1] -- Indexes start at 1.
)

-------------------------------------------------------------------------

(--start ms			
	
	actionMan.executeAction 0 "40472"  -- MAXScript Listener
	clearListener()
	
	dirArray      = GetDirectories (rootPath + "*")
	numFailScenes = 0
	numAllScenes  = 0
	print "Start scan scenes, please wait..."
	
	
	for d in dirArray do 
	(		
		currDir = substituteString d rootPath ""
		currDir = trimRight currDir "\\"		
		
		files = getFiles (d + "*.max")
		print files
		
		for f in files do
		(
			fileName   = getFilenameFile f
			currMaxVer = getMaxFileVersionData f
			
			format "Scene: %, max ver.: % \n" fileName currMaxVer 
						
			if (currMaxVer[1] != (GetMaxRawVer needMaxVer)) do
			(				
				numFailScenes += 1						
				loadMAXFile f useFileUnits:true quiet:true
				saveMaxFile f saveAsVersion:needMaxVer
				format "Resave:% in % version.\n" fileName needMaxVer
			)
			numAllScenes += 1
		)
	)	
	
	if 	(numFailScenes > 0)	then	
		format "% scenes resave from %. \n" numFailScenes files.count	
	else 	
		print "All the scenes are correct, thank you )"
	
)-- end ms