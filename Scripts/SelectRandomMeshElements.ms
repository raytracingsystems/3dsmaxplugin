/*
Last change:  08.12.2022
Author: Ray Tracing Systems.
Select random elements in editable poly.
*/


numElements 			= 10000
maxAreaFace 			= 100
maxTryingFindFace	= numElements * 10

------------------------------------------------------------------------------------------------------------
	
escapeEnable    = true	

--actionMan.executeAction 0 "40472"  -- MAXScript Listener
clearListener()
	
obj =  $
	
if (obj == undefined) then
(
	print "No object selected.\n"	
)
else
(
	--disableSceneRedraw() 	
	
	convertTo obj PolyMeshObject
	max modify mode
	subobjectLevel = 4
		
	--st = timestamp()--get the start time in milliseconds
	
	numFaces = getNumFaces obj
	
	rndArray = #()
	rndArray[numFaces]	
	selectFace = 1
	
	
	for i = 1 to maxTryingFindFace while selectFace <= numElements do	
	(		
		rndNum = random 1 numFaces
		
		areaFace = polyop.getFaceArea obj rndNum
		--format "areaFace: % \n" areaFace
						
		if (areaFace < maxAreaFace) do
		(
			rndArray[selectFace] = rndNum
			selectFace += 1
		)		
	)		
					
	obj.selectedFaces = rndArray 		
	obj.selectElement()
	--obj.delete	#CurrentLevel
	
	--et = timestamp()--stop the time
	--print (et-st)--print the resulting time
		
	--enableSceneRedraw() 
	if (selectFace == 1) then
		messageBox "Not found face. Increase maxAreaFace." icon:#warning
	else
		print "Complete."
)
