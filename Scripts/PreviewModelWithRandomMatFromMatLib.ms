/*
Last change:  31.07.2020
Author: Ray Tracing Systems.
Preview random materials with HydraMtlTags and special material library.
*/

libPath = @"d:\Works\Ray-Tracing_Systems\HydraDevelop\CLSP\3dsmax\Materials_lib\1\Materials_lib.mat"


numCellWidth = 3
sizeCell = 200

-----------------------------------------------------------------------------------------------------------------------------------------

fn FindMatInLibrary sourceMat matLib =
(
	arrayMat = #()
	num = 0
	for matInLib in matLib do
	(
		num += 1
		if (classof matInLib != HydraMtlTags) do continue
		
		if ((sourceMat.texTarget 		!=  0	  and sourceMat.texTarget == matInLib.texTarget) or
			(sourceMat.texTarget 		==  0 and 	(	
			(sourceMat.matAny 			== on and matInLib.matAny 		== on) or 
			(sourceMat.matCeramic 	    == on and matInLib.matCeramic 	== on) or 
			(sourceMat.matConcrete 	== on and matInLib.matConcrete	== on) or
			(sourceMat.matEmission 	== on and matInLib.matEmission	== on) or
			(sourceMat.matFabric 		== on and matInLib.matFabric		== on) or
			(sourceMat.matGlass 		    == on and matInLib.matGlass		== on) or
			(sourceMat.matLeather 		== on and matInLib.matLeather	== on) or
			(sourceMat.matLiquid 		== on and matInLib.matLiquid		== on) or
			(sourceMat.matMarble 		== on and matInLib.matMarble		== on) or
			(sourceMat.matMetal 			== on and matInLib.matMetal		== on) or
			(sourceMat.matOrganic  	== on and matInLib.matOrganic  	== on) or 
			(sourceMat.matPaint  		== on and matInLib.matPaint		== on) or
			(sourceMat.matPaper  		== on and matInLib.matPaper		== on) or
			(sourceMat.matPlastic  		== on and matInLib.matPlastic		== on) or
			(sourceMat.matRubber  		== on and matInLib.matRubber	== on) or
			(sourceMat.matWood 		== on and matInLib.matWood		== on)))) do 
			(                                                           
				--format "Find mat: %\n" matInLib				
				append arrayMat num 		
			)
	)
	format "arrayFindMat: %\n" arrayMat
	--format "arrayMat.count: %\n" arrayMat.count
	--seed (timeGetTime()/100)
	rndNum = random 1 arrayMat.count
	format "Asign new random mat: %\n" arrayMat[rndNum] 

 	arrayMat[rndNum] 
)
	
fn RenderRndMaterials objectMatSource = 
(		
	newMat = objectMatSource 
	
	if (classof objectMatSource == multiSubMaterial) then
	(
		numMat = 1
		for subMat in objectMatSource do
		(				
			if (classof subMat != HydraMtlTags) do continue
			
			format "subMat: %\n" subMat 
			
			findMat = FindMatInLibrary subMat currentMaterialLibrary	
			
			if (findMat != undefined) do 
			(				
				newMat[numMat] = currentMaterialLibrary[findMat]	
				
				if (subMat.rndOverrideColor == on) do
				(
					newMat[numMat].mtl_diffuse_color = subMat.rndColor
					format "Override color: % \n" newMat[numMat].mtl_diffuse_color
				)				
			)			
			numMat += 1
		)
	)	
	else
	(
		if (classof objectMatSource != HydraMtlTags) do continue
			
		format "sourceMat:%\n" objectMatSource
		
		findMat = FindMatInLibrary objectMatSource currentMaterialLibrary	
			
		if (findMat != undefined) do 
		(				
			newMat = currentMaterialLibrary[findMat]	
			
			if (objectMatSource.rndOverrideColor == on) do
			(
				newMat.mtl_diffuse_color = objectMatSource.rndColor
				format "Override color: % \n" newMat.mtl_diffuse_color
			)
		)		
	)
	
	Geometry[1].material = newMat

	print "Render........."
 	bmRender = render outputwidth:sizeCell outputheight:sizeCell vfb:false		
	bmRender --return bmRender
)

----------------------------------------------------------------------------------------------------------------------------------------
-- Main
----------------------------------------------------------------------------------------------------------------------------------------
fn Main = 
(
	escapeEnable = true	
	
	numCellHeight = (numCellWidth / 1.5) as integer
	windowSizeWidth = sizeCell * numCellWidth 
	windowSizeHeigh = sizeCell * numCellHeight
	
	clearListener()			
	
	loadMaterialLibrary libPath
	
	tex 			= BitmapTexture()
	tex.filename 	= @"c:\[Hydra]\pluginFiles\material\EnvMatEdit.hdr"
	env 			= HydraBack_Envir()	
	env.pb_envirMap = tex
	env.pb_backMap 	= output()
	env.pb_backMap.output.Output_Amount = 0
	
	
	
	if (objects.count > 1) do 
	(
		messagebox "There should be only 1 object with material in the scene. Remove foreign objects." title:"Error"		
		actionMan.executeAction 0 "50029" --open scene selector
		return 0
	)
		
	meditMaterials[1]	= Geometry[1].material
	meditMaterials[2]	= copy meditMaterials[1]
	meditMaterials[2].name = "SourceMat"
	
	bmCanvas = bitmap windowSizeWidth windowSizeHeigh --color:black		
	
	maxScene = maxFilePath + maxFileName
	
	-- Generate 
	for j=0 to numCellHeight-1 do
	(
		for k=0 to numCellWidth-1 do
		(			
			loadMaxFile maxScene quiet:on
			
			environmentMap = env
			
			-- Random materials and render 1 image
			bmRender = RenderRndMaterials Geometry[1].material			

			-- Compose 1 images from several images
			x = k * sizeCell
			y = j * sizeCell
			pasteBitmap bmRender bmCanvas [0, 0] [x, y] 		
			display bmCanvas		

			--load source materials
			meditMaterials[1]		= copy meditMaterials[2]
			meditMaterials[1].name	= "RndMat"
			Geometry[1].material 	= meditMaterials[1]	
		)
	)
 	environmentMap = undefined
	print "Random render complete."
)

Main()