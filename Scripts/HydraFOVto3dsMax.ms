/*
Last change:  15.12.2022
Author: Ray Tracing Systems.
Convert Hydra camera vertical FOV to 3ds Max horizontal FOV.
*/

-- Specify the final rendering resolution for the correct aspect ratio calculation.
-- 
hydraFov = 52.9068266105088 as Double -- vertical FOV

-----------------------------------------------------------------------------------------
actionMan.executeAction 0 "40472"  -- MAXScript Listener
clearListener()


w = renderWidth as Double 
h = renderHeight as Double

--hfov = 2 * atan((0.5 * w) / (0.5 * h / tan(vfov / 2)))
--vfov = 2 * atan((0.5 * h) / (0.5 * w / tan(hfov / 2)))
maxFov = 2.0 * atan((0.5 * w) / (0.5 * h / tan(hydraFov / 2.0))) as float
	
if (superClassOf $ == Camera) then
	$.fov = maxFov
else
	"Camera not selected."
	
	
format "3ds Max camera FOV = % \n" maxFov