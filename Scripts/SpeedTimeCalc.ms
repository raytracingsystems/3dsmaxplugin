start = timeStamp()
process_mesh() -- do some big job
end = timeStamp()
format "Processing took % seconds\n" ((end - start) / 1000.0)