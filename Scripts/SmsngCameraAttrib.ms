/*
Last change:  16.03.2023
Author: Ray Tracing Systems.
Adding custom camera settings..
*/

-- DON'T FORGET TO SPECIFY THE LENS AND SENSOR FILE IN THE CAMERA SETTINGS.
	
def = attributes "SmsngCustomParam"
attribID:#(1188344381L, 2976808612L) -- not change this ID!!!
(	
	parameters main rollout:params
	(
		onSmsngCustParam	type: #integer 	ui:onSmsngCustParam_cb default:1			
		useGpu        		type: #integer 	ui:useGpu_cb 			default:1	
		lensFlare        	type: #integer 	ui:lensFlare_cb 			default:1
		skipSensor       	type: #integer 	ui:skipSensor_cb 		default:1
		threadsNum		type: #integer 	ui:threadsN_sp 			default:10		
		irisNumber       	type: #integer 	ui:irisNumber_sp 		default:0
		irisRotation       	type: #integer 	ui:irisRotation_sp 		default:0
		exposureMult		type: #float  		ui:exposure_sp 			default:1.0
		rWavelengths 	type: #float 		ui:rWavelengths_sp	default:550.6
		gWavelengths 	type: #float  		ui:gWavelengths_sp 	default:550.5
		bWavelengths 	type: #float  		ui:bWavelengths_sp 	default:550.4		
		
		lensFilePath		type: #string		 								default:""
		sensorFilePath	type: #string		 								default:""
		lensFileName		type: #string		 								default:"empty"
		sensorFileName	type: #string		 								default:"empty"
	)
	rollout params "Samsung Parameters"
	( 
		checkbox onSmsngCustParam_cb "On" 
		checkbox useGpu_cb 		"Use GPU" 
		checkbox lensFlare_cb 		"Lens Flare" 	
		checkbox skipSensor_cb 	"Skip Sensor" 					
		spinner 	threadsN_sp 		"Threads" 			type: #integer		
		spinner 	irisNumber_sp 	"Iris Number" 	type: #integer
		spinner 	irisRotation_sp 	"Iris Rotation" 	type: #integer
		spinner 	exposure_sp		"Exposure"		type: #float
		
		group "RGB Wave Lengths" 
		(
			spinner rWavelengths_sp across:3 	type:#float range:[380, 810, 0.1] width:48 align:#left
			spinner gWavelengths_sp  				type:#float range:[380, 810, 0.1] width:48 align:#left
			spinner bWavelengths_sp  				type:#float range:[380, 810, 0.1] width:48	align:#left	
		)

		group "Custom Files" 
		(
			button lensFile_bt "Lens File" 		align:#left 
			label lab2 lensFileName 				align:#left 	toolTip:lensFilePath
			
			button sensorFile_bt "Sensor File"	align:#left
			label lab4 sensorFileName 				align:#left		toolTip:sensorFilePath
		)				
			
		on lensFile_bt pressed do 
		(
			lensFilePath = getOpenFileName caption:"Open A Test File:" \
			types:"Json (*.json)|*.json|All (*.*)|*.*|" 
			
			lensFileName = filenameFromPath lensFilePath		
			lab2.text    	= lensFileName
		)
		on sensorFile_bt pressed do 
		(
			sensorFilePath = getOpenFileName caption:"Open A Test File:" \
			types:"Json (*.json)|*.json|All (*.*)|*.*|" 		
			
			sensorFileName = filenameFromPath sensorFilePath		
			lab4.text 			= sensorFileName
		)
	)
)

obj = selection
custAttributes.add obj def
