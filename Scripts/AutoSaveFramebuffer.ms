/*
Last change: 10.03.2019
Author: Ray Tracing Systems.
Autosave the framebuffer at intervals.
*/

(
	-- Work only with Standard 3ds Max Framebuffer !
	-- Run before rendering. (Ctrl+E)
	
	clearListener()
	
	local 	everyMin 		= 0.5
	local 	ref 			= on
	
	-------------------------------------------------------------------------------------
	
	Global	AutosaveImg   
	Global 	dnetTimer 		= dotnetobject "system.timers.timer" (everyMin * 60 * 1000)
	local  	elapsedTime 	= 0	
	local	currInterval 	= everyMin
	
	fn dntTick =
	(
		local Autosavedir 
		if (ref == on) then	Autosavedir = maxFilePath + maxFileName + "_ref"	+ "_"+ ((everyMin as integer) as string) + "min.png"
		else 				Autosavedir = maxFilePath + maxFileName            	+ "_"+ ((everyMin as integer) as string) + "min.png"
		
		::AutosaveImg 			= getLastRenderedImage copy:off
		
		AutosaveImg.filename 	= Autosavedir
		Save AutosaveImg		
		format "Save render: % \n" Autosavedir
		
		elapsedTime			+= currInterval	
		everyMin 			*= 2
		currInterval		= everyMin - elapsedTime
		dnetTimer.interval	= currInterval * 60 * 1000

-- 		format "elapsedTime: % \n" elapsedTime
-- 		format "everyMin: % \n" everyMin
-- 		format "currInterval: % \n" currInterval
	)
	
	
	dotnet.addEventHandler dnetTimer "Elapsed" dntTick
	
	callbacks.removeScripts id:#RenderAutoSave
	callbacks.addScript #preRender "dnetTimer.start()" id:#RenderAutoSave
	callbacks.addScript #postRender "dnetTimer.stop()" id:#RenderAutoSave

)