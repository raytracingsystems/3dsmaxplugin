#include "HydraBackEnvir.h"

///////////////////////////////////////////////////////////////////////////////////

#define M_PI 3.1415926535f

static HydraBackEnvirClassDesc HydraAODesc;
ClassDesc2* GetHydraBackEnvirDesc()
{
  return &HydraAODesc;
}

static ParamBlockDesc2 hydraBackEnvir_param_blk(hydraBackEnvir_params, _T("params"), 0, &HydraAODesc,
                                                P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF,
                                                //rollout
                                                IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
                                                // params
                                                pb_backMap, _T("pb_backMap"), TYPE_TEXMAP, P_OWNERS_REF, IDS_BACK_MAP,
                                                p_refno, 0,
                                                p_subtexno, 0,
                                                p_ui, TYPE_TEXMAPBUTTON, IDC_BACK_MAP,
                                                p_end,
                                                pb_backColor, _T("pb_backColor"), TYPE_RGBA, P_ANIMATABLE, IDS_BACK_COLOR,
                                                p_default, Color(1.0, 1.0, 1.0),
                                                p_ui, TYPE_COLORSWATCH, IDC_BACK_COLOR,
                                                p_end,
                                                pb_envirMap, _T("pb_envirMap"), TYPE_TEXMAP, P_OWNERS_REF, IDS_ENVIR_MAP,
                                                p_refno, 1,
                                                p_subtexno, 1,
                                                p_ui, TYPE_TEXMAPBUTTON, IDC_ENVIR_MAP,
                                                p_end,
                                                pb_envirColor, _T("pb_envirColor"), TYPE_RGBA, P_ANIMATABLE, IDS_ENVIR_COLOR,
                                                p_default, Color(1.0, 1.0, 1.0),
                                                p_ui, TYPE_COLORSWATCH, IDC_ENVIR_COLOR,
                                                p_end,
                                                p_end
);


hydraBackEnvirDlgProc::hydraBackEnvirDlgProc(HydraBackEnvir * cb) : hM(cb){}


INT_PTR hydraBackEnvirDlgProc::DlgProc(TimeValue t, IParamMap2 * map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thisHwnd = hWnd;
  const hydraBackEnvirDlgProc *dlg = DLGetWindowLongPtr<hydraBackEnvirDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
    case WM_INITDIALOG:
    {
      DragAcceptFiles(GetDlgItem(hWnd, IDC_BACK_MAP), true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_ENVIR_MAP), true);

      return TRUE;
    }
    //case WM_COMMAND:
    //  switch (LOWORD(wParam))
    //  {
    //  case IDC_BACK_MAP:
    //  case IDC_ENVIR_MAP:
    //  break;
    //  }
    case WM_DESTROY:
      break;
    case WM_DROPFILES:
      POINT pt;
      WORD numFiles;
      HWND dropTarget;
      int dropTargetID;

      hydraChar lpszFile[80];

      DragQueryPoint((HDROP)wParam, &pt);

      numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

      if (numFiles == 0)
      {
        DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
        dropTarget = RealChildWindowFromPoint(hWnd, pt);
        dropTargetID = GetDlgCtrlID(dropTarget);
        DropFileInMapSlot(dropTargetID, lpszFile);
      }

      DragFinish((HDROP)wParam);

      break;
    default:
      return FALSE;
  }
  return TRUE;
}

void hydraBackEnvirDlgProc::DropFileInMapSlot(int mapControlID, hydraChar * filename)
{
  BitmapTex* btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  WStr dir;
  WStr name;
  WStr extension;

  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);

  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap* tex = (Texmap*)btex;

  switch (mapControlID)
  {
    case IDC_BACK_MAP:
      hM->SetSubTexmap(BACK_TEX_SLOT, tex);
      hM->subTexSlots[BACK_TEX_SLOT] = tex;
      break;
    case IDC_ENVIR_MAP:
      hM->SetSubTexmap(ENVIR_TEX_SLOT, tex);
      hM->subTexSlots[ENVIR_TEX_SLOT] = tex;
      break;
    default:
      break;
  }
}

//ParamDlg* HydraAO::xyzGenDlg;

//--- HydraAO -------------------------------------------------------
HydraBackEnvir::HydraBackEnvir()
{
  for (auto& i : subTexSlots) i = nullptr;  
  GetHydraBackEnvirDesc()->MakeAutoParamBlocks(this);  
}


//From MtlBase
void HydraBackEnvir::Reset()
{
  ivalid.SetEmpty();
  //if (xyzGen) 
  //	xyzGen->Reset();
  //else 
  //	ReplaceReference( COORD_REF, GetNewDefaultXYZGen());

  // Reset texmap back to its default values
  for (int i = 0; i < NUM_SUBTEX; ++i)
  {
    if (subTexSlots[i])
    {
      DeleteReference(i);
      subTexSlots[i] = nullptr;
    }
  }

  GetHydraBackEnvirDesc()->MakeAutoParamBlocks(this);
}

void HydraBackEnvir::Update(TimeValue t, Interval& valid)
{
  // Add code to evaluate anything prior to rendering
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();

    pblock->GetValue(pb_backColor, t, backColor, ivalid);
    pblock->GetValue(pb_envirColor, t, envirColor, ivalid);

    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (auto& i : subTexSlots)    
      if (i) i->Update(t, ivalid);    
  }

  valid &= ivalid;
}

Interval HydraBackEnvir::Validity(TimeValue t)
{
  // Update ivalid here
  Interval valid = FOREVER;

  for (auto& i : subTexSlots)  
    if (i) valid &= i->Validity(t);
  
  return ivalid;
}

ParamDlg* HydraBackEnvir::CreateParamDlg(HWND hwMtlEdit, IMtlParams* imp)
{
  IAutoMParamDlg* masterDlg = GetHydraBackEnvirDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
  //xyzGenDlg = xyzGen->CreateParamDlg(hwMtlEdit, imp);
  //masterDlg->AddDlg(xyzGenDlg);

  //Set the user dialog proc of the param block, and do other initialization

  //hydraao_param_blk.SetUserDlgProc(new HydraAODlgProc(this));
  return masterDlg;
}

BOOL HydraBackEnvir::SetDlgThing(ParamDlg* dlg)
{
  //if (dlg == xyzGenDlg)
  //	xyzGenDlg->SetThing(xyzGen);
  //else
  //	return FALSE;
  //return TRUE;
  return FALSE;
}

void HydraBackEnvir::SetSubTexmap(int i, Texmap *m)
{
  //ReplaceReference(i + 2, m);
  ReplaceReference(i, m);
  //Store the 'i-th' sub-texmap managed by the texture

  switch (i)
  {
    case 0:
      hydraBackEnvir_param_blk.InvalidateUI(pb_backMap);
      pblock->SetValue(pb_backMap, TimeValue(0), m);
      ivalid.SetEmpty();
      break;
    case 1:
      hydraBackEnvir_param_blk.InvalidateUI(pb_envirMap);
      pblock->SetValue(pb_envirMap, TimeValue(0), m);
      ivalid.SetEmpty();
      break;
    default:
      break;
  }

  NotifyChanged();

}


#ifdef MAX2022
MSTR HydraBackEnvir::GetSubTexmapSlotName(int i, bool localized)
{
  //Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
  case 0:  return GetString(IDS_BACK_MAP);
  case 1:  return GetString(IDS_ENVIR_MAP);
  default: return _T("");
  }
}
#else
TSTR HydraBackEnvir::GetSubTexmapSlotName(int i)
{
  //Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
    case 0:  return GetString(IDS_BACK_MAP);
    case 1:  return GetString(IDS_ENVIR_MAP);
    default: return _T("");
  }
}
#endif // MAX2022


//From ReferenceMaker
RefTargetHandle HydraBackEnvir::GetReference(int i)
{
  //Return the references based on the index
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBLOCK_REF: return pblock;
  //	default: return subTexSlots[i-2];
  //	}

  if (i < NUM_SUBTEX)
    return subTexSlots[i];
  else
    return pblock;

}

void HydraBackEnvir::SetReference(int i, RefTargetHandle rtarg)
{
  //Store the reference handle passed into its 'i-th' reference
  //switch(i) {
  //	case COORD_REF:  xyzGen = (XYZGen *)rtarg; break;
  //	case PBLOCK_REF: pblock = (IParamBlock2 *)rtarg; break;
  //	default: subTexSlots[i-2] = (Texmap *)rtarg; break;
  //}

  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NUM_SUBTEX && s != L"ParamBlock2")
    subTexSlots[i] = (Texmap *)rtarg;
  else
    pblock = (IParamBlock2 *)rtarg;
}

//From ReferenceTarget
RefTargetHandle HydraBackEnvir::Clone(RemapDir &remap)
{
  HydraBackEnvir *mnew = new HydraBackEnvir();
  *((MtlBase*)mnew) = *((MtlBase*)this); // copy superclass stuff

  //Add other cloning stuff

  mnew->ReplaceReference(0, remap.CloneRef(pblock));

  mnew->backColor  = backColor;
  mnew->envirColor = envirColor;
  mnew->ivalid.SetEmpty();

  for (int i = 0; i < NUM_SUBTEX; i++)
  {
    mnew->subTexSlots[i] = nullptr;
    if (subTexSlots[i])
      mnew->ReplaceReference(i, remap.CloneRef(subTexSlots[i]));
  }

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}


Animatable* HydraBackEnvir::SubAnim(int i)
{
  // Return 'i-th' sub-anim
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBLOCK_REF: return pblock;
  //	default: return subTexSlots[i-2];
  //	}

  if (i < NUM_SUBTEX)
    return subTexSlots[i];
  else
    return pblock;

}


#ifdef MAX2022
MSTR HydraBackEnvir::SubAnimName(int i, bool localized)
{
  return _T("");
}
#else
TSTR HydraBackEnvir::SubAnimName(int i)
{
  // Return the sub-anim names
  //switch (i) {
  //	//case COORD_REF: return GetString(IDS_COORDS);
  //	case PBLOCK_REF: return GetString(IDS_PARAMS);
  //	default: return GetSubTexmapTVName(i-1);
  //	}

  return _T("");
}
#endif // MAX2022


RefResult HydraBackEnvir::NotifyRefChanged(const Interval& /*changeInt*/, RefTargetHandle hTarget, PartID& /*partID*/, RefMessage message, BOOL /*propagate*/)
{
  switch (message)
  {
    case REFMSG_TARGET_DELETED:
    {
      if      (hTarget == xyzGen) { xyzGen = nullptr; }
      else if (hTarget == pblock) { pblock = nullptr; }
      else
      {
        for (auto& i : subTexSlots)
        {
          if (i == hTarget)
          {
            i = nullptr;
            break;
          }
        }
      }
    }
    break;
  }
  return(REF_SUCCEED);
}

/*===========================================================================*\
|	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraBackEnvir::Save(ISave* isave)
{
  IOResult res;
  ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  isave->EndChunk();

  if (res != IO_OK) 
    return res;

  isave->BeginChunk(HYDRA_CHUNK);
  isave->Write(&backColor,  sizeof(Color), &nb);
  isave->Write(&envirColor, sizeof(Color), &nb);
  isave->EndChunk();

  return IO_OK;
}

IOResult HydraBackEnvir::Load(ILoad* iload)
{
  IOResult res;
  int id;
  ULONG nb;
  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
      case MTL_HDR_CHUNK:
        res = MtlBase::Load(iload);
        break;
      case HYDRA_CHUNK:
        res = iload->Read(&backColor , sizeof(Color), &nb);
        res = iload->Read(&envirColor, sizeof(Color), &nb);
        break;
    }
    iload->CloseChunk();

    if (res != IO_OK)
      return res;
  }

  return IO_OK;
}

AColor HydraBackEnvir::EvalColor(ShadeContext& sc)
{
  // After being evaluated, if a map or material has a non-zero gbufID, 
  // it should call ShadeContext::SetGBuffer() to store it into 
  // the shade context.
  if (gbufID)
    sc.SetGBufferID(gbufID);

  Color backTex = (subTexSlots[BACK_TEX_SLOT] != nullptr && sc.doMaps) ? subTexSlots[BACK_TEX_SLOT]->EvalColor(sc) : RGBA(0, 0, 0);
  
  Color envTex;

  if (subTexSlots[ENVIR_TEX_SLOT] != nullptr && sc.doMaps)
    envTex = subTexSlots[ENVIR_TEX_SLOT]->EvalColor(sc);
  else
    pblock->GetValue(pb_envirColor, TimeValue(0), envTex, ivalid);

  const IPoint2 scrCoord = sc.ScreenCoord();    
  if (scrCoord.x < scrCoord.y) return backTex;
  else                         return envTex;  
}


float HydraBackEnvir::EvalMono(ShadeContext& sc)
{
  // Evaluate the map for a "mono" channel
  return Intens(EvalColor(sc));
}


Point3 HydraBackEnvir::EvalNormalPerturb(ShadeContext& /*sc*/)
{
  // Return the perturbation to apply to a normal for bump mapping
  return Point3(0.0F, 0.0F, 0.0F);
}


ULONG HydraBackEnvir::LocalRequirements(int subMtlNum)
{
  // Specify various requirements for the material
  //return xyzGen->Requirements(subMtlNum);
  return 0;
}


FPInterface* HydraBackEnvirClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  else
    return ClassDesc2::GetInterface(id);
}


const MCHAR* HydraBackEnvirClassDesc::GetEntryName() const
{
  return const_cast<HydraBackEnvirClassDesc*>(this)->ClassName();
}


const MCHAR* HydraBackEnvirClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Maps\\Hydra";
  return str;
}


Bitmap* HydraBackEnvirClassDesc::GetEntryThumbnail() const noexcept
{
  return nullptr;
}

