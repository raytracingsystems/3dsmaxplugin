#pragma once
#include "resource.h"
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <maxtypes.h>
#include <IMaterialBrowserEntryInfo.h>

#include <stdmat.h>
#include <imtl.h>
#include <macrorec.h>

#include "3dsmaxport.h"

///////////////////////////////////////////////////////////////////////////////////

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define HydraBackEnvir_CLASS_ID	Class_ID(0xe954bddf, 0x4a0184fd)

enum { BACK_TEX_SLOT, ENVIR_TEX_SLOT, NUM_SUBTEX }; // NUM_SUBTEX must be last in list!

constexpr int PBLOCK_REF = NUM_SUBTEX;

using hydraStr           = std::wstring;
using hydraChar          = wchar_t;


class HydraBackEnvir: public Texmap
{
public:
  HydraBackEnvir();
  //~HydraBackEnvir(){};

  // References
  XYZGen*          xyzGen = nullptr;          // ref 0
  IParamBlock2*    pblock = nullptr;          // ref 1
  Texmap*          subTexSlots[NUM_SUBTEX]; // Reference array of sub-materials

                                    //static ParamDlg* xyzGenDlg;
  Interval         ivalid;

  Color backColor, envirColor;

  //From MtlBase
  ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams* imp) override;
  void      Update(TimeValue t, Interval& valid)            override;
  ULONG     LocalRequirements(int subMtlNum)                override;
  BOOL      SetDlgThing(ParamDlg* dlg)                      override;
  Interval  Validity(TimeValue t)                           override;
  void      Reset()                                         override;

  void NotifyChanged() { NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE); }

    // SubTexmap access methods
  int       NumSubTexmaps()                   noexcept override { return NUM_SUBTEX; }
  Texmap*   GetSubTexmap(int i)               noexcept override { return subTexSlots[i];  }
  void      SetSubTexmap(int i, Texmap* m)             override;

#ifdef MAX2022
  MSTR     GetSubTexmapSlotName(int i, bool localized) override;
#else      
  TSTR     GetSubTexmapSlotName(int i)                 override;
#endif // MAX2022


  //From Texmap
  RGBA     EvalColor        (ShadeContext& sc)         override;
  float    EvalMono         (ShadeContext& sc)         override;
  Point3   EvalNormalPerturb(ShadeContext& sc)         override;                                             
  XYZGen*  GetTheXYZGen()                     noexcept override { return xyzGen; }


  // Return anim index to reference index
  int      SubNumToRefNum(int subNum)         noexcept override { return subNum; }

  // If your class is derived from Tex3D then you should also
  //implement ReadSXPData for 3D Studio/DOS SXP texture compatibility
  //void ReadSXPData(TCHAR* /*name*/, void* /*sxpdata*/) {}

  // Loading/Saving
  IOResult Load(ILoad* iload)                           override;
  IOResult Save(ISave* isave)                           override;

  //From Animatable
  Class_ID        ClassID()                 override { return HydraBackEnvir_CLASS_ID; }
  SClass_ID       SuperClassID()   noexcept override { return TEXMAP_CLASS_ID; }

#ifdef MAX2022
  void            GetClassName(MSTR& s, bool localized = true) const override { s = GetString(IDS_CLASS_NAME); }
#else
  void            GetClassName(TSTR& s)     override { s = GetString(IDS_CLASS_NAME); }
#endif // MAX2022

  RefTargetHandle Clone(RemapDir& remap)    override;
  
  int             NumSubs()        noexcept override { return 1; }
  Animatable*     SubAnim(int i)            override;

#ifdef MAX2022
  MSTR            SubAnimName(int i, bool localized = false) override;
#else
  TSTR            SubAnimName(int i)        override;
#endif // MAX2022


  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate) override;


  // Maintain the number or references here
  int             NumRefs()            noexcept override { return 2 + NUM_SUBTEX; }
  RefTargetHandle GetReference(int i)           override;
  void            SetReference(int i, RefTargetHandle rtarg) override;

  int	            NumParamBlocks()     noexcept override { return 1; }	     // return number of ParamBlocks in this instance
  IParamBlock2*   GetParamBlock(int i)          override { return pblock; }; // return i'th ParamBlock
  IParamBlock2*   GetParamBlockByID(BlockID id) override { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

  void            DeleteThis()         noexcept override { delete this; }  
};


class HydraBackEnvirClassDesc: public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  int           IsPublic()          noexcept override { return TRUE; }
  void*         Create(BOOL loading = FALSE) override { return new HydraBackEnvir(); }
  const TCHAR *	ClassName()                  override { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  SClass_ID     SuperClassID()      noexcept override { return TEXMAP_CLASS_ID; }
  Class_ID      ClassID()                    override { return HydraBackEnvir_CLASS_ID; }
  const TCHAR*  Category()                   override { return GetString(IDS_CATEGORY); }
  const TCHAR*  InternalName()      noexcept override { return _T("HydraBack/Envir"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE     HInstance()         noexcept override { return hInstance; }					  // returns owning module handle

  // For entry category
  FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  const MCHAR*  GetEntryName()         const override;
  const MCHAR*  GetEntryCategory()     const override;
  Bitmap*       GetEntryThumbnail()    const noexcept override;

#ifdef MAX2022
  const MCHAR* NonLocalizedClassName()       override { return GetString(IDS_CLASS_NAME); }
#endif // MAX2022                                                             
};


class hydraBackEnvirDlgProc: public ParamMap2UserDlgProc
{
public:
  HydraBackEnvir *hM   = nullptr;
  HWND thisHwnd        = nullptr;
  bool additional_init = true;

  explicit hydraBackEnvirDlgProc(HydraBackEnvir *cb);

  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
  void    DropFileInMapSlot(int mapControlID, hydraChar* filename);
  void    DeleteThis() { delete this; }
};

ClassDesc2* GetHydraBackEnvirDesc();

enum { hydraBackEnvir_params };

// Add enums for various parameters
enum
{
  pb_backMap,
  pb_backColor,
  pb_envirMap,
  pb_envirColor
};
