#include "HydraAO.h"

//////////////////////////////////////////////////////////////////////////////////////////////////

static HydraAOClassDesc HydraAODesc;
ClassDesc2* GetHydraAODesc()
{
  return &HydraAODesc;
}

static ParamBlockDesc2 hydraAo_param_blk(PB_MAIN, _T("params"), 0, &HydraAODesc,
                                         P_AUTO_CONSTRUCT + P_AUTO_UI, PBMAIN_REF,
                                         //rollout
                                         IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
                                         // params
                                         pb_occludedMap, _T("mtl_occluded_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_OCCLUDED_MAP,
                                         p_refno, 0,
                                         p_subtexno, 0,
                                         p_ui, TYPE_TEXMAPBUTTON, IDC_OCCLUDED_MAP,
                                         p_end,
                                         pb_occludedColor, _T("mtl_occluded_color"), TYPE_RGBA, P_ANIMATABLE, IDS_OCCLUDED_COLOR,
                                         p_default, Color(0.0F, 0.0F, 0.0F),
                                         p_ui, TYPE_COLORSWATCH, IDC_OCCLUDED_COLOR,
                                         p_end,
                                         pb_unoccludedMap, _T("mtl_unoccluded_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_UNOCCLUDED_MAP,
                                         p_refno, 1,
                                         p_subtexno, 1,
                                         p_ui, TYPE_TEXMAPBUTTON, IDC_UNOCCLUDED_MAP,
                                         p_end,
                                         pb_unoccludedColor, _T("mtl_unoccluded_color"), TYPE_RGBA, P_ANIMATABLE, IDS_UNOCCLUDED_COLOR,
                                         p_default, Color(1.0F, 1.0F, 1.0F),
                                         p_ui, TYPE_COLORSWATCH, IDC_UNOCCLUDED_COLOR,
                                         p_end,
                                         pb_distanceMap, _T("mtl_distance_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_DISTANCE_MAP,
                                         p_refno, 2,
                                         p_subtexno, 2,
                                         p_ui, TYPE_TEXMAPBUTTON, IDC_DISTANCE_MAP,
                                         p_end,
                                         pb_distanceMult, _T("mtl_distance_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_DISTANCE,
                                         p_default, 0.01F,
                                         p_range, 0.0F, 1000.0F,
                                         p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_DISTANCE_MULT, IDC_DISTANCE_MULT_SPIN, 0.1f,
                                         p_end,
                                         pb_calculateFor, _T("mtl_calculate_for"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_DISTRIB,
                                         p_default, 0,
                                         p_ui, TYPE_INT_COMBOBOX, IDC_DISTRIB_COMBO, 1, IDS_CONCAVE, /*IDS_CONVEX, IDS_BOTH,*/
                                         p_tooltip, IDS_DISTRIB,
                                         p_end,
                                         pb_falloffMult, _T("mtl_falloff_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_FALLOFF,
                                         p_default, 1.0F,
                                         p_range, 0.1f, 10.0F,
                                         p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_FALLOFF_MULT, IDC_FALLOFF_MULT_SPIN, 0.1F,
                                         p_end,
                                         pb_onlyForThis, _T("mtl_only_for_this_on"), TYPE_BOOL, 0, IDS_ONLY_FOR_THIS,
                                         p_default, FALSE,
                                         p_ui, TYPE_SINGLECHEKBOX, IDC_ONLY_FOR_THIS_ON,
                                         p_end,
                                         p_end
);




hydraAODlgProc::hydraAODlgProc(HydraAO * cb) noexcept : hM(cb) {}


INT_PTR hydraAODlgProc::DlgProc(TimeValue t, IParamMap2 * map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thishWnd                  = hWnd;
  const hydraAODlgProc* dlg = DLGetWindowLongPtr<hydraAODlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) 
    return FALSE;

  switch (msg)
  {
    case WM_INITDIALOG:
    {
      //dlg = (hydraMaterialDlgProc *)lParam;
      //DLSetWindowLongPtr(hWnd, dlg);

      DragAcceptFiles(GetDlgItem(hWnd, IDC_OCCLUDED_MAP),   true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_UNOCCLUDED_MAP), true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_DISTANCE_MAP),   true);

      return TRUE;
    }
    case WM_COMMAND:
      switch (LOWORD(wParam))
      {
        case IDC_OCCLUDED_MAP:
        case IDC_UNOCCLUDED_MAP:
        case IDC_DISTANCE_MAP:
          if (HIWORD(wParam) == BN_BUTTONUP || HIWORD(wParam) == BN_RIGHTCLICK || HIWORD(wParam) == BN_BUTTONDOWN)
          {}
          break;
      }
      break;
    case WM_DESTROY:
      break;
    case WM_DROPFILES:
      POINT pt;
      WORD numFiles;
      HWND dropTarget;
      int dropTargetID;

      hydraChar lpszFile[80];

      DragQueryPoint((HDROP)wParam, &pt);

      numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

      if (numFiles == 0)
      {
        DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
        dropTarget = RealChildWindowFromPoint(hWnd, pt);
        dropTargetID = GetDlgCtrlID(dropTarget);
        DropFileInMapSlot(dropTargetID, lpszFile);
      }

      DragFinish((HDROP)wParam);

      break;
    default:
      return FALSE;
  }
  return TRUE;
}

void hydraAODlgProc::DropFileInMapSlot(int mapControlID, hydraChar* filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  WStr dir;
  WStr name;
  WStr extension;

  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);

  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap* tex = (Texmap*)btex;

  switch (mapControlID)
  {
    case IDC_OCCLUDED_MAP:
      hM->SetSubTexmap(OCCLUDED_TEX_SLOT, tex);
      hM->subtex[OCCLUDED_TEX_SLOT] = tex;
      break;
    case IDC_UNOCCLUDED_MAP:
      hM->SetSubTexmap(UNOCCLUDED_TEX_SLOT, tex);
      hM->subtex[UNOCCLUDED_TEX_SLOT] = tex;
      break;
    case IDC_DISTANCE_MAP:
      hM->SetSubTexmap(DISTANCE_TEX_SLOT, tex);
      hM->subtex[DISTANCE_TEX_SLOT] = tex;
      break;
    default:
      break;
  }
}


//ParamDlg* HydraAO::xyzGenDlg;


//--- HydraAO -------------------------------------------------------
HydraAO::HydraAO()
{
  for (auto& i : subtex) i = nullptr;
  GetHydraAODesc()->MakeAutoParamBlocks(this);
}


//From MtlBase
void HydraAO::Reset()
{
  ivalid.SetEmpty();
  //if (xyzGen) 
  //	xyzGen->Reset();
  //else 
  //	ReplaceReference( COORD_REF, GetNewDefaultXYZGen());

  // Reset texmap back to its default values
  for (int i = 0; i < NUM_SUBTEX; ++i)
  {
    if (subtex[i])
    {
      DeleteReference(i);
      subtex[i] = nullptr;
    }
  }

  GetHydraAODesc()->MakeAutoParamBlocks(this);
}


void HydraAO::Update(TimeValue t, Interval& valid)
{
  // Add code to evaluate anything prior to rendering
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();

    pblock->GetValue(pb_occludedColor,   t, occludedColor,   ivalid);
    pblock->GetValue(pb_unoccludedColor, t, unoccludedColor, ivalid);
    pblock->GetValue(pb_distanceMult,    t, distanceMult,    ivalid);
    pblock->GetValue(pb_calculateFor,    t, calculateFor,    ivalid);
    pblock->GetValue(pb_falloffMult,     t, falloffMult,     ivalid);
    pblock->GetValue(pb_onlyForThis,     t, onlyForThisOn,   ivalid);

    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (auto& i : subtex)    
      if (i) i->Update(t, ivalid);    
  }

  valid &= ivalid;
}


Interval HydraAO::Validity(TimeValue t)
{
  Interval valid = FOREVER;

  for (auto& i : subtex)  
    if (i) valid &= i->Validity(t);
  
  return ivalid;
}

ParamDlg* HydraAO::CreateParamDlg(HWND hwMtlEdit, IMtlParams* imp)
{
  IAutoMParamDlg* masterDlg = GetHydraAODesc()->CreateParamDlgs(hwMtlEdit, imp, this);
  //xyzGenDlg = xyzGen->CreateParamDlg(hwMtlEdit, imp);
  //masterDlg->AddDlg(xyzGenDlg);
  // Set the user dialog proc of the param block, and do other initialization

  //hydraao_param_blk.SetUserDlgProc(new HydraAODlgProc(this));
  return masterDlg;
}

BOOL HydraAO::SetDlgThing(ParamDlg* dlg)
{
  //if (dlg == xyzGenDlg)
  //	xyzGenDlg->SetThing(xyzGen);
  //else
  //	return FALSE;
  //return TRUE;
  return FALSE;
}

void HydraAO::SetSubTexmap(int i, Texmap *m)
{
  //ReplaceReference(i + 2, m);
  ReplaceReference(i, m);
  // Store the 'i-th' sub-texmap managed by the texture

  switch (i)
  {
    case 0:
      hydraAo_param_blk.InvalidateUI(pb_occludedMap);
      pblock->SetValue(pb_occludedMap, TimeValue(0), m);
      ivalid.SetEmpty();
      break;
    case 1:
      hydraAo_param_blk.InvalidateUI(pb_unoccludedMap);
      pblock->SetValue(pb_unoccludedMap, TimeValue(0), m);
      ivalid.SetEmpty();
      break;
    case 2:
      hydraAo_param_blk.InvalidateUI(pb_distanceMap);
      pblock->SetValue(pb_distanceMap, TimeValue(0), m);
      ivalid.SetEmpty();
      break;
    default:
      break;
  }

  NotifyChanged();
}


#ifdef MAX2022
MSTR HydraAO::GetSubTexmapSlotName(int i, bool localized)
{
  // Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
  case 0:  return GetString(IDS_OCCLUDED_MAP);
  case 1:  return GetString(IDS_UNOCCLUDED_MAP);
  case 2:  return GetString(IDS_DISTANCE_MAP);
  default: return _T("");
  }
}
#else
TSTR HydraAO::GetSubTexmapSlotName(int i)
{
  // Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
    case 0:  return GetString(IDS_OCCLUDED_MAP);
    case 1:  return GetString(IDS_UNOCCLUDED_MAP);
    case 2:  return GetString(IDS_DISTANCE_MAP);
    default: return _T("");
  }  
}
#endif // MAX2022


//From ReferenceMaker
RefTargetHandle HydraAO::GetReference(int i)
{
  // Return the references based on the index
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBMAIN_REF: return pblock;
  //	default: return subtex[i-2];
  //	}

  if (i < NUM_SUBTEX)
    return subtex[i];
  else
    return pblock;

}

void HydraAO::SetReference(int i, RefTargetHandle rtarg)
{
  // Store the reference handle passed into its 'i-th' reference
  //switch(i) {
  //	case COORD_REF:  xyzGen = (XYZGen *)rtarg; break;
  //	case PBMAIN_REF: pblock = (IParamBlock2 *)rtarg; break;
  //	default: subtex[i-2] = (Texmap *)rtarg; break;
  //}

  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NUM_SUBTEX && s != L"ParamBlock2")
    subtex[i] = (Texmap *)rtarg;
  else
    pblock = (IParamBlock2 *)rtarg;
}

//From ReferenceTarget
RefTargetHandle HydraAO::Clone(RemapDir &remap)
{
  auto* mnew = new HydraAO();
  *((MtlBase*)mnew) = *((MtlBase*)this); // copy superclass stuff

  // Add other cloning stuff

  mnew->ReplaceReference(0, remap.CloneRef(pblock));

  mnew->occludedColor   = occludedColor;
  mnew->unoccludedColor = unoccludedColor;
  mnew->distanceMult    = distanceMult;
  mnew->calculateFor    = calculateFor;
  mnew->falloffMult     = falloffMult;
  mnew->onlyForThisOn   = onlyForThisOn;

  mnew->ivalid.SetEmpty();

  for (int i = 0; i < NUM_SUBTEX; ++i)
  {
    mnew->subtex[i] = nullptr;
    if (subtex[i])
      mnew->ReplaceReference(i, remap.CloneRef(subtex[i]));
  }

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}


Animatable* HydraAO::SubAnim(int i)
{
  // Return 'i-th' sub-anim
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBMAIN_REF: return pblock;
  //	default: return subtex[i-2];
  //	}

  if (i < NUM_SUBTEX)
    return subtex[i];
  else
    return pblock;

}

#ifdef MAX2022
MSTR HydraAO::SubAnimName(int i, bool localized)
{
  return _T("");
}
#else
TSTR HydraAO::SubAnimName(int i)
{
  // Return the sub-anim names
  //switch (i) {
  //	//case COORD_REF: return GetString(IDS_COORDS);
  //	case PBMAIN_REF: return GetString(IDS_PARAMS);
  //	default: return GetSubTexmapTVName(i-1);
  //	}

  return _T("");
}
#endif // MAX2022


RefResult HydraAO::NotifyRefChanged(const Interval& /*changeInt*/, RefTargetHandle hTarget, PartID& /*partID*/, RefMessage message, BOOL /*propagate*/)
{
  switch (message)
  {
    case REFMSG_TARGET_DELETED:
    {
      if      (hTarget == xyzGen) { xyzGen = nullptr; }
      else if (hTarget == pblock) { pblock = nullptr; }
      else
      {
        for (auto& i : subtex)
        {
          if (i == hTarget)
          {
            i = nullptr;
            break;
          }
        }
      }
    }
    break;
  }
  return(REF_SUCCEED);
}

/*===========================================================================*\
|	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraAO::Save(ISave* isave)
{
  IOResult res;
  ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  isave->EndChunk();

  if (res != IO_OK)
    return res;

  isave->BeginChunk(HYDRA_CHUNK);
  isave->Write(&occludedColor  , sizeof(Color), &nb);
  isave->Write(&unoccludedColor, sizeof(Color), &nb);
  isave->Write(&distanceMult   , sizeof(float), &nb);
  isave->Write(&falloffMult    , sizeof(float), &nb);
  isave->Write(&calculateFor   , sizeof(int),   &nb);
  isave->Write(&onlyForThisOn  , sizeof(bool),  &nb);
  isave->EndChunk();

  return IO_OK;
}


IOResult HydraAO::Load(ILoad* iload)
{
  IOResult res;
  int id;
  ULONG nb;

  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
      case MTL_HDR_CHUNK:
        res = MtlBase::Load(iload);
        break;
      case HYDRA_CHUNK:
        res = iload->Read(&occludedColor  , sizeof(Color), &nb);
        res = iload->Read(&unoccludedColor, sizeof(Color), &nb);
        res = iload->Read(&distanceMult   , sizeof(float), &nb);
        res = iload->Read(&calculateFor   , sizeof(int),   &nb);
        res = iload->Read(&falloffMult    , sizeof(float), &nb);
        res = iload->Read(&onlyForThisOn  , sizeof(bool),  &nb);
        break;
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  return IO_OK;
}


AColor HydraAO::EvalColor(ShadeContext& sc)
{
  Color res;
  pblock->GetValue(pb_unoccludedColor, TimeValue(0), res, ivalid);
  return res;
}


float HydraAO::EvalMono(ShadeContext& sc)
{
  // Evaluate the map for a "mono" channel
  return Intens(EvalColor(sc));
}


Point3 HydraAO::EvalNormalPerturb(ShadeContext& /*sc*/)
{
  // Return the perturbation to apply to a normal for bump mapping
  return Point3(0.0F, 0.0F, 0.0F);
}


ULONG HydraAO::LocalRequirements(int subMtlNum)
{
  // Specify various requirements for the material
  //return xyzGen->Requirements(subMtlNum);
  return 0;
}


FPInterface* HydraAOClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  else
    return ClassDesc2::GetInterface(id);
}


const MCHAR* HydraAOClassDesc::GetEntryName() const
{
  return const_cast<HydraAOClassDesc*>(this)->ClassName();
}


const MCHAR* HydraAOClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Maps\\Hydra";
  return str;
}


Bitmap* HydraAOClassDesc::GetEntryThumbnail() const noexcept
{
  return nullptr;
}

