#include "HydraBlendedBox.h"

///////////////////////////////////////////////////////////////////////////////////

static HydraBlendedBoxClassDesc HydraAODesc;

ClassDesc2* GetHydraBlendedBoxDesc()
{
  return &HydraAODesc;
}


static ParamBlockDesc2 HydraBlendedBox_param_blk(HydraBlendedBox_params, _T("Parameters"), 0, &HydraAODesc,
  P_AUTO_CONSTRUCT + P_AUTO_UI, PBMAIN_REF,
  //rollout
  IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
  // params
  pb_blendSize, _T("blendSize"), TYPE_FLOAT, P_ANIMATABLE, IDS_BLENDSIZE,
    p_default, 10.0f,
    p_range, 1.0f, 100.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BLENDSIZE_EDIT, IDC_BLENDSIZE_SPIN, 1.0f,
    p_end,
  pb_numProj, _T("numProj"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_NUMPROJ,
    p_ui, TYPE_INT_COMBOBOX, IDC_NUMPROJ_COMBO, 3, IDS_1, IDS_3, IDS_6,
    p_tooltip, IDS_NUMPROJ,
    p_end,
  pb_mapScale, _T("mapScale"), TYPE_FLOAT, P_ANIMATABLE, IDS_MAPSCALE,
    p_default, 1.0f,
    p_range, 0.0001f, 10000.0f,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_MAPSCALE_EDIT, IDC_MAPSCALE_SPIN, 0.1f,
    p_end,
  pb_topMap, _T("topMap"), TYPE_TEXMAP, P_OWNERS_REF, IDS_TOP_MAP,
    p_refno, 0,
    p_subtexno, 0,
    p_ui, TYPE_TEXMAPBUTTON, IDC_TOP_MAP,
    p_end,
  p_end
);




HydraBlendedBoxDlgProc::HydraBlendedBoxDlgProc(HydraBlendedBox* cb) noexcept : hM(cb) {}


INT_PTR HydraBlendedBoxDlgProc::DlgProc(TimeValue t, IParamMap2* map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thishWnd = hWnd;
  HydraBlendedBoxDlgProc* dlg = DLGetWindowLongPtr<HydraBlendedBoxDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
  case WM_INITDIALOG:
  {
    //dlg = (hydraMaterialDlgProc *)lParam;
    //DLSetWindowLongPtr(hWnd, dlg);

    if (!gui_topMap) gui_topMap = GetICustButton(GetDlgItem(hWnd, IDC_TOP_MAP));
    
    DragAcceptFiles(GetDlgItem(hWnd, IDC_TOP_MAP), true);
    
    return TRUE;
  }
  //case WM_COMMAND:
  //  switch (LOWORD(wParam))
  //  {
  //  case IDC_BACK_MAP:
  //  case IDC_ENVIR_MAP:
  //  break;
  //  }
  case WM_DESTROY:
    ReleaseICustButton(gui_topMap);
    ReleaseICustButton(gui_envirMap);   

    break;
  case WM_DROPFILES:
    POINT pt;
    WORD numFiles;
    HWND dropTarget;
    int dropTargetID;

    hydraChar lpszFile[80];

    DragQueryPoint((HDROP)wParam, &pt);

    numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

    if (numFiles == 0)
    {
      DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
      dropTarget = RealChildWindowFromPoint(hWnd, pt);
      dropTargetID = GetDlgCtrlID(dropTarget);
      DropFileInMapSlot(dropTargetID, lpszFile);
    }

    DragFinish((HDROP)wParam);

    break;
  default:
    return FALSE;
  }
  return TRUE;
}


void HydraBlendedBoxDlgProc::DropFileInMapSlot(int mapControlID, hydraChar * filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  WStr dir;
  WStr name;
  WStr extension;

  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);

  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap* tex = (Texmap*)btex;

  switch (mapControlID)
  {
  case IDC_TOP_MAP:
    hM->SetSubTexmap(TOP_TEX_SLOT, tex); hM->subTexSlots[TOP_TEX_SLOT] = tex; break;      
  default: 
    break;
  }
}


//ParamDlg* HydraAO::xyzGenDlg;

//--- HydraAO -------------------------------------------------------
HydraBlendedBox::HydraBlendedBox()
{
  for (auto& i : subTexSlots) i = nullptr;  
  GetHydraBlendedBoxDesc()->MakeAutoParamBlocks(this);
  Reset();
}


//From MtlBase
void HydraBlendedBox::Reset()
{
  ivalid.SetEmpty();
  //if (xyzGen) 
  //	xyzGen->Reset();
  //else 
  //	ReplaceReference( COORD_REF, GetNewDefaultXYZGen());

  // Reset texmap back to its default values
  for (int i = 0; i < NUM_SUBTEX; ++i)
  {
    if (subTexSlots[i])
    {
      DeleteReference(i);
      subTexSlots[i] = nullptr;
    }
  }

  GetHydraBlendedBoxDesc()->MakeAutoParamBlocks(this);
}


void HydraBlendedBox::Update(TimeValue t, Interval& valid)
{
  // Add code to evaluate anything prior to rendering
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();


    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    for (auto& i : subTexSlots)    
      if (i) i->Update(t, ivalid);    
  }

  valid &= ivalid;
}


Interval HydraBlendedBox::Validity(TimeValue t)
{
  // Update ivalid here
  Interval valid = FOREVER;

  for (auto& i : subTexSlots)  
    if (i) valid &= i->Validity(t);
  
  return ivalid;
}


ParamDlg* HydraBlendedBox::CreateParamDlg(HWND hwMtlEdit, IMtlParams* imp)
{
  IAutoMParamDlg* masterDlg = GetHydraBlendedBoxDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
  //xyzGenDlg = xyzGen->CreateParamDlg(hwMtlEdit, imp);
  //masterDlg->AddDlg(xyzGenDlg);
  // Set the user dialog proc of the param block, and do other initialization

  //hydraao_param_blk.SetUserDlgProc(new HydraAODlgProc(this));
  return masterDlg;
}


BOOL HydraBlendedBox::SetDlgThing(ParamDlg* dlg)
{
  //if (dlg == xyzGenDlg)
  //	xyzGenDlg->SetThing(xyzGen);
  //else
  //	return FALSE;
  //return TRUE;
  return FALSE;
}


void HydraBlendedBox::SetSubTexmap(int i, Texmap *m)
{
  //ReplaceReference(i + 2, m);
  ReplaceReference(i, m);
  // Store the 'i-th' sub-texmap managed by the texture

  switch (i)
  {
  case 0:
    HydraBlendedBox_param_blk.InvalidateUI(pb_topMap);
    pblock->SetValue(pb_topMap, TimeValue(0), m);
    ivalid.SetEmpty();
    break;
  //case 1:
  //  HydraBlendedBox_param_blk.InvalidateUI(pb_bottomMap);
  //  pblock->SetValue(pb_bottomMap, TimeValue(0), m);
  //  ivalid.SetEmpty();
  //  break;
  default:
    break;
  }

  NotifyChanged();

}


#ifdef MAX2022
MSTR HydraBlendedBox::GetSubTexmapSlotName(int i, bool localized)
{
  // Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
  case 0:  return GetString(IDS_BACK_MAP);
  case 1:  return GetString(IDS_ENVIR_MAP);
  default: return _T("");
  }
}
#else
TSTR HydraBlendedBox::GetSubTexmapSlotName(int i)
{
  // Return the slot name of the 'i-th' sub-texmap
  switch (i)
  {
  case 0:  return GetString(IDS_BACK_MAP);
  case 1:  return GetString(IDS_ENVIR_MAP);
  default: return _T("");
  }  
}
#endif // MAX2022


//From ReferenceMaker
RefTargetHandle HydraBlendedBox::GetReference(int i)
{
  // Return the references based on the index
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBMAIN_REF: return pblock;
  //	default: return subTexSlots[i-2];
  //	}

  if (i < NUM_SUBTEX)
    return subTexSlots[i];
  else
    return pblock;

}

void HydraBlendedBox::SetReference(int i, RefTargetHandle rtarg)
{
  // Store the reference handle passed into its 'i-th' reference
  //switch(i) {
  //	case COORD_REF:  xyzGen = (XYZGen *)rtarg; break;
  //	case PBMAIN_REF: pblock = (IParamBlock2 *)rtarg; break;
  //	default: subTexSlots[i-2] = (Texmap *)rtarg; break;
  //}

  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NUM_SUBTEX && s != L"ParamBlock2")
    subTexSlots[i] = (Texmap*)rtarg;
  else
    pblock = (IParamBlock2*)rtarg;
}

//From ReferenceTarget
RefTargetHandle HydraBlendedBox::Clone(RemapDir &remap)
{
  HydraBlendedBox* mnew = new HydraBlendedBox();
  *((MtlBase*)mnew) = *((MtlBase*)this); // copy superclass stuff

  // Add other cloning stuff

  mnew->ReplaceReference(0, remap.CloneRef(pblock));

  mnew->ivalid.SetEmpty();

  for (int i = 0; i < NUM_SUBTEX; i++)
  {
    mnew->subTexSlots[i] = nullptr;
    if (subTexSlots[i])
      mnew->ReplaceReference(i, remap.CloneRef(subTexSlots[i]));
  }

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}


Animatable* HydraBlendedBox::SubAnim(int i)
{
  // Return 'i-th' sub-anim
  //switch (i) {
  //	case COORD_REF: return xyzGen;
  //	case PBMAIN_REF: return pblock;
  //	default: return subTexSlots[i-2];
  //	}

  if (i < NUM_SUBTEX)
    return subTexSlots[i];
  else
    return pblock;

}


#ifdef MAX2022
MSTR HydraBlendedBox::SubAnimName(int i, bool localized)
{
  return _T("");
}
#else
TSTR HydraBlendedBox::SubAnimName(int i)
{
  // Return the sub-anim names
  //switch (i) {
  //	//case COORD_REF: return GetString(IDS_COORDS);
  //	case PBMAIN_REF: return GetString(IDS_PARAMS);
  //	default: return GetSubTexmapTVName(i-1);
  //	}

  return _T("");
}
#endif // MAX2022


RefResult HydraBlendedBox::NotifyRefChanged(const Interval& /*changeInt*/, RefTargetHandle hTarget, PartID& /*partID*/, RefMessage message, BOOL /*propagate*/)
{
  switch (message)
  {
  case REFMSG_TARGET_DELETED:
  {
    if      (hTarget == xyzGen) { xyzGen = nullptr; }
    else if (hTarget == pblock) { pblock = nullptr; }
    else
    {
      for (auto& i : subTexSlots)
      {
        if (i == hTarget)
        {
          i = nullptr;
          break;
        }
      }
    }
  }
  break;
  }
  return(REF_SUCCEED);
}

/*===========================================================================*\
|	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraBlendedBox::Save(ISave* isave)
{
  IOResult res;
  ULONG    nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);
  isave->EndChunk();
  
  if (res != IO_OK)  
    return res;  

  isave->BeginChunk(HYDRA_CHUNK);
  isave->EndChunk();

  return IO_OK;
}


IOResult HydraBlendedBox::Load(ILoad* iload)
{
  IOResult res;
  int id;
  ULONG nb;
  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
    case MTL_HDR_CHUNK:
      res = MtlBase::Load(iload);
      break;
    case HYDRA_CHUNK:
      break;
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  return IO_OK;
}


AColor HydraBlendedBox::EvalColor(ShadeContext& sc)
{
  // Evaluate the color of texture map for the context.

  // After being evaluated, if a map or material has a non-zero gbufID, 
  // it should call ShadeContext::SetGBuffer() to store it into 
  // the shade context.
  if (gbufID)
    sc.SetGBufferID(gbufID);

  const AColor texColorTop = (subTexSlots[TOP_TEX_SLOT] != NULL && sc.doMaps) ? subTexSlots[TOP_TEX_SLOT]->EvalColor(sc) : RGBA(0, 0, 0);
  
  return texColorTop;
}


float HydraBlendedBox::EvalMono(ShadeContext& sc)
{
  // Evaluate the map for a "mono" channel
  return Intens(EvalColor(sc));
}


Point3 HydraBlendedBox::EvalNormalPerturb(ShadeContext& /*sc*/)
{
  // Return the perturbation to apply to a normal for bump mapping
  return Point3(0.0F, 0.0F, 0.0F);
}


ULONG HydraBlendedBox::LocalRequirements(int subMtlNum)
{
  // Specify various requirements for the material
  //return xyzGen->Requirements(subMtlNum);
  return 0;
}


FPInterface * HydraBlendedBoxClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}


const MCHAR * HydraBlendedBoxClassDesc::GetEntryName() const
{
  return const_cast<HydraBlendedBoxClassDesc*>(this)->ClassName();
}


const MCHAR * HydraBlendedBoxClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Maps\\Hydra";
  return str;
}


Bitmap* HydraBlendedBoxClassDesc::GetEntryThumbnail() const noexcept
{
  return nullptr;
}

