//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HydraBlendedBox.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_BACK_MAP                    5
#define IDS_TOP_MAP                     5
#define IDS_OCCLUDED_MAP                6
#define IDS_BACK_COLOR                  6
#define IDS_BLENDSIZE                   6
#define IDS_COORDS                      7
#define IDS_CONCAVE                     7
#define IDS_ENVIR_MAP                   7
#define IDS_MAPSCALE                    7
#define IDS_OCCLUDED_COLOR              8
#define IDS_ENVIR_COLOR                 8
#define IDS_UNOCCLUDED_MAP              9
#define IDS_NUMPROJ                     9
#define IDS_UNOCCLUDED_COLOR            10
#define IDS_1                           10
#define IDS_DISTANCE                    11
#define IDS_3                           11
#define IDS_DISTANCE_MAP                12
#define IDS_6                           12
#define IDS_FALLOFF                     13
#define IDS_DISTRIB                     14
#define IDS_ONLY_FOR_THIS               15
#define IDS_EDGE                        16
#define IDS_CONVEX                      16
#define IDS_BOTH                        17
#define IDD_PANEL                       101
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_ONLY_FOR_THIS_ON            1003
#define IDC_UNOCCLUDED_COLOR            1004
#define IDC_ENVIR_COLOR                 1004
#define IDC_OCCLUDED_COLOR              1005
#define IDC_BACK_COLOR                  1005
#define IDC_OCCLUDED_MAP                1006
#define IDC_BACK_MAP                    1006
#define IDC_DISTANCE_MAP                1007
#define IDC_UNOCCLUDED_MAP              1008
#define IDC_ENVIR_MAP                   1008
#define IDC_TOP_MAP                     1008
#define IDC_DISTRIB_COMBO               1009
#define IDC_NUMPROJ_COMBO               1013
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_FALLOFF_MULT                1490
#define IDC_BLENDSIZE_EDIT              1490
#define IDC_DISTANCE_MULT               1491
#define IDC_MAPSCALE_EDIT               1491
#define IDC_SPIN                        1496
#define IDC_FALLOFF_MULT_SPIN           1496
#define IDC_BLENDSIZE_SPIN              1496
#define IDC_DISTANCE_MULT_SPIN          1497
#define IDC_MAPSCALE_SPIN               1497

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
