float4 mix(float3 x, float3 y, float a)
{
  return to_float4(y + a*(x - y), 0);
}

float4 main(const SurfaceInfo* sHit, float3 color1, float3 color2)
{
  const float3 pos  = readAttr(sHit,"LocalPos");
  const float3 norm = readAttr(sHit,"Normal");
  
  const float3 rayDir  = hr_viewVectorHack;  
  const float cosAlpha = fabs(dot(norm,rayDir));
  const float IOR      = 1.5;
  const float R0       = pow((1.0 - IOR)/(1.0 + IOR), 2);
  const float blend    = R0 + (1.0 - R0)*pow(1.0 - cosAlpha, 5);
  
  return mix(color1, color2, 1.0-blend);
}