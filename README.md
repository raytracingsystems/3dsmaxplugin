# Hydra Renderer 3ds Max Plugins

This is the source code of the Hydra Renderer plugin for 3ds Max.

# Build Plugins

1) git clone https://gitlab.com/raytracingsystems/3dsmaxplugin (this repository).
2) git clone https://github.com/Ray-Tracing-Systems/HydraAPI.
3) git clone https://github.com/Ray-Tracing-Systems/HydraPP.
4) All these projects should be in one folder: 3dsmaxplugin, HydraAPI, HydraPP.
5) Build plugins using Visual Studio.
6) Build the other plugins from the folders: Materials, Maps and Lights.
7) Build and install HydraCore https://github.com/Ray-Tracing-Systems/HydraCore, or install it from https://github.com/Ray-Tracing-Systems/Hydra3dsMaxBinary.

# Acknowledgment

This project is supported by RFBR 18-31-20032 mol_a_ved.	
