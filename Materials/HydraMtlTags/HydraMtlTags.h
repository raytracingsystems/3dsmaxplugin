#pragma once

////////////////////////////////////////////////////////////////////////////////////////////////
// DESCRIPTION: Hydra material with tags
// AUTHOR: RAY TRACING SYSTEMS
////////////////////////////////////////////////////////////////////////////////////////////////

#include "resource.h"
#include "iparamm2.h"
#include <string>
#include "stdmat.h"
#include <IMaterialBrowserEntryInfo.h>
#include <Graphics/IHLSLMaterialTranslator.h>
#include <Graphics/RenderItemHandle.h>

#include "../../HydraRenderer/HydraLogger.h"

#include "../HydraMtl/IHydraMtl.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define M_E           2.71828182F

#define hydraMtlTags_CLASS_ID	Class_ID(0x341d33e9, 0x491a1bbc)

#define NSUBMTL	0 // number of sub-materials supported by this plugin 

const BlockID MAIN_PB_ID = 0;
const BlockID TAGS_PB_ID = 1;

enum extrusion  { EXTRUSION_STRONG, EXTRUSION_LUM };
enum brdf       { BRDF_PHONG, BRDF_TORRSPARR, BRDF_BECKMANN, BRDF_GGX, BRDF_TRGGX };
enum texSlot    { DIFFUSE_TEX_SLOT, SPECULAR_TEX_SLOT, REFLECT_TEX_SLOT, EMISSION_TEX_SLOT, TRANSP_TEX_SLOT, RELIEF_TEX_SLOT,
  SPEC_GLOSS_TEX_SLOT, REFL_GLOSS_TEX_SLOT, TRANSP_GLOSS_TEX_SLOT, OPACITY_TEX_SLOT, TRANSLUCENCY_TEX_SLOT, REFL_ANISOTR_TEX_SLOT,
  REFL_ROTATION_TEX_SLOT, NUM_SUBTEX }; // NSUBTEX should be last in the list!


// Reference list. Add texture slots, material slots, or parameter blocks to the end of the list.
enum refList { DIFFUSE_TEX_REF, SPECULAR_TEX_REF, REFLECT_TEX_REF, EMISSION_TEX_REF, TRANSPAR_TEX_REF, NORMALMAP_TEX_REF, 
  SPEC_GLOSS_TEX_REF, REFL_GLOSS_TEX_REF, TRANSP_GLOSS_TEX_REF, OPACITY_TEX_REF, TRANSLUCENCY_TEX_REF, MAIN_PBLOCK_REF, 
  TAGS_PBLOCK_REF, REFL_ANISOTR_TEX_REF, REFL_ROTATION_TEX_REF, NUM_REFS }; // NUM_REFS should be last in the list!


#define VERSION_CURRENT 1
#define MAX_SAMPLES 256


typedef std::wstring hydraStr;
typedef wchar_t hydraChar;

static float CosPowerFromGlosiness(const float glossiness) noexcept
{
  if (glossiness < 0.5f)
    return 0.495448f * pow(M_E, (4.865236f * glossiness + 0.78064f));
  else
    return 0.000575f * pow(M_E, 22.425084f * glossiness - 4.147466f) + 11.584066f;
}



class HydraMtlTags: public IHydraMtl
{
public:
  // Parameter block
  IParamBlock2*   pblockMain = nullptr;	
  IParamBlock2*   pblockTags = nullptr;	

  Texmap*         subTexSlots[NUM_SUBTEX]{}; 


  int   m_specularBrdf, m_reflectBrdf, m_specGlossOrCos, m_reflectGlossOrCos, m_transpGlossOrCos, m_reflectExtrusion;
  Color m_diffuseColor, m_specularColor, m_reflectColor, m_emissionColor, m_transpColor, m_transpDistColor, m_exitColor, m_translucColor;
  BOOL  m_displacement, m_invertHeight, m_noIcRecords, m_diffuseTint, m_specularTint, m_emissionTint, m_transpTint, m_reflectTint,
    m_transpThin, m_translucTint, m_affectShadows, m_specularFresnel, m_reflectFresnel, m_lockSpecular, m_emissionCastGi, m_opacitySmooth;
  float m_specularRough, m_reflectRough, m_reflectCospow, m_specularCospow, m_transpIor, m_specularIor, m_reflectIor, m_transpCospower,
    m_transpDistMult, m_displaceHeight, m_diffuseMult, m_specularMult, m_emissionMult, m_transpMult, m_reflectMult, m_specularGloss, m_reflectGloss,
    m_transpGloss, m_reliefAmount, m_reliefSmooth, m_bumpSigma, m_translucMult, m_diffuseRoughness, m_reflectAnisotr, m_reflectAnisRotat;

  Interval		      ivalid;

  // Load bitmap for environment map
  static Interface* ip;
  static BitmapTex* bmapTexEnv;
  static StdUVGen*  uvGenEnv;
  static Texmap*    texmapEnv;

  static int        offsetRnd;
  static bool       hasLoadStaticContent;


  ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) override;
  void      Update(TimeValue t, Interval& valid)            override;
  Interval  Validity(TimeValue t)                           override;
  void      Reset()                                         override;

  void              NotifyChanged();

  // From MtlBase and Mtl
  void      SetAmbient         (Color c, TimeValue t)                   override;
  void      SetDiffuse         (Color c, TimeValue t)                   override;
  void      SetSpecular        (Color c, TimeValue t)                   override;
  void      SetShininess       (float v, TimeValue t)                   override;
  Color     GetAmbient         (int mtlNum = 0, BOOL backFace = FALSE)  override;
  Color     GetDiffuse         (int mtlNum = 0, BOOL backFace = FALSE)  override;
  Color     GetSpecular        (int mtlNum = 0, BOOL backFace = FALSE)  override;
  Color     GetSelfIllumColor  (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float     GetXParency        (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float     GetShininess       (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float     GetShinStr         (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float     WireSize           (int mtlNum = 0, BOOL backFace = FALSE)  override;
  BOOL      GetTransparencyHint(TimeValue t, Interval& valid)           override;

  // Shade and displacement calculation
  //
  void      Shade               (ShadeContext& sc)  override;
  float     EvalDisplacement    (ShadeContext& sc)  override;
  Interval  DisplacementValidity(TimeValue t)       override;

  float DiffuseOrenNayar        (const Point3 l, const Point3 v, const Point3 n, const float roughness);
  void PhongSampleAndEvalBRDF   (Color& resultReflects, ShadeContext& sc, const Point2 rnd2d, const float cosPower, Texmap* a_texmapEnv, const Point3 reflVect);
  void BlinnSampleAndEvalBRDF   (Color& resultReflects, ShadeContext& sc, const Point2 rnd2d, const float cosPower, Texmap* a_texmapEnv, Point3 a_normal);
  void GGXSampleAndEvalBRDF     (Color& resultReflects, ShadeContext& sc, const Point2 rnd2d, const float gloss, Texmap* a_texmapEnv, Point3 a_normal);
  void GGXSample2AndEvalBRDF    (Color& resultReflects, ShadeContext& sc, const Point2 rnd2d, const float gloss, Texmap* a_texmapEnv, Point3 a_normal);
  void BeckmannSampleAndEvalBRDF(Color& resultReflects, ShadeContext& sc, const Point2 rnd2d, const float gloss, const float anisotropy, const float anisRotation, Texmap* a_texmapEnv, Point3 normal, const Point3 tbBasis[2]);
  void TRGGXSampleAndEvalBRDF   (Color& resultReflects, ShadeContext& sc, const Point2 rnd2d, const float gloss, const float anisotropy, const float anisRotation, Texmap* a_texmapEnv, Point3 normal, const Point3 tbBasis[2]);


  // SubMaterial access methods
  int     NumSubMtls()                            override { return NSUBMTL; }
  Mtl*    GetSubMtl        (int i)                override;
  void    SetSubMtl        (int i, Mtl *m)        override;
  TSTR    GetSubMtlSlotName(int i)                override;
  TSTR    GetSubMtlTVName  (int i);

  // SubTexmap access methods
  int     NumSubTexmaps()                         override { return NUM_SUBTEX; }
  Texmap* GetSubTexmap        (int i)             override;
  void    SetSubTexmap        (int i, Texmap *m)  override;
  TSTR    GetSubTexmapSlotName(int i)             override;
  TSTR    GetSubTexmapTVName  (int i);

  BOOL SetDlgThing(ParamDlg* dlg) override { return FALSE; };

  HydraMtlTags(BOOL loading);
  // ~hydraMtlTags();


   // For Paramblock2 Load and Save is not necessary. It saves everything automatically.
   // Loading/Saving
  IOResult  Load(ILoad *iload)          override;
  IOResult  Save(ISave *isave)          override;

  //From Animatable
  Class_ID  ClassID()                   override { return hydraMtlTags_CLASS_ID; }
  SClass_ID SuperClassID()              override { return MATERIAL_CLASS_ID; }
  void      GetClassName(TSTR& s)       override { s = GetString(IDS_CLASS_NAME); }

  RefTargetHandle Clone(RemapDir &remap)      override;
  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate) override;

  int             NumSubs()                     override { return 1 + NSUBMTL; }
  Animatable*     SubAnim(int i)                override;
  TSTR            SubAnimName(int i)            override;

  // Maintain the number or references here 
  int             NumRefs()                     override { return NUM_REFS; }
  RefTargetHandle GetReference(int i)           override;
  void            SetReference(int i, RefTargetHandle rtarg) override;

  int	            NumParamBlocks()              override { return 1 + TAGS_PB_ID; }	  // return number of ParamBlocks in this instance
  IParamBlock2*   GetParamBlock(int i)          override;                             // return i'th ParamBlock
  IParamBlock2*   GetParamBlockByID(BlockID id) override;                             // return id'd ParamBlock

  void DeleteThis()                           override { delete this; }

  // from interface IHydraMtl 
  // Diffuse
  Texmap* GetDiffuseTexmap()        override;
  float   GetDiffuseMult()          override;
  Color   GetDiffuseColor()         override;
  bool    GetDiffuseTint()          override;
  float   GetDiffuseRoughness()     override;

  // Reflectivity
  Texmap* GetReflTexmap()           override;
  Texmap* GetReflGlossTexmap()      override;
  Texmap* GetReflAnisTexmap()       override;
  Texmap* GetReflAnisRotatTexmap()  override;
  float   GetReflMult()             override;
  Color   GetReflColor()            override;
  bool    GetReflTint()             override;
  float   GetReflGloss()            override;
  int     GetReflBRDF()             override;
  float   GetReflIor()              override;
  bool    GetReflFresnel()          override;
  int     GetReflExtrus()           override;
  float   GetReflAnisotr()          override;
  float   GetReflAnisRotat()        override;

  // Transparency
  Texmap* GetTranspTexmap()         override;
  Texmap* GetTranspGlossTexmap()    override;
  float   GetTranspMult()           override;
  Color   GetTranspColor()          override;
  bool    GetTranspTint()           override;
  float   GetTranspGloss()          override;
  float   GetTranspIor()            override;
  float   GetTranspDistMult()       override;
  Color   GetTranspDistColor()      override;
  bool    GetTranspThin()           override;

  // Opacity/Special
  Texmap* GetOpacityTexmap()        override;
  bool    GetAffectShadow()         override;
  bool    GetOpacitySmooth()        override;
  bool    HasOpacity()              override;

  // Emission
  Texmap* GetEmissionTexmap()       override;
  float   GetEmissionMult()         override;
  Color   GetEmissionColor()        override;
  bool    GetEmissionTint()         override;
  bool    GetEmissionCastGi()       override;

  // Translucency
  Texmap* GetTranslucTexmap()       override;
  float   GetTranslucMult()         override;
  Color   GetTranslucColor()        override;
  bool    GetTranslucTint()         override;

  // Relief
  Texmap* GetReliefTexmap()         override;
  float   GetReliefAmount()         override;
  float   GetReliefSmooth()         override;

protected:
  Color GetShadeDiffuseColor     (ShadeContext &sc);
  Color GetShadeReflectionColor  (ShadeContext &sc);
  Color GetShadeSpecularColor    (ShadeContext &sc);
  Color GetShadeTransparencyColor(ShadeContext &sc);
  Color GetShadeTranslucencyColor(ShadeContext &sc);
  Color GetShadeEmissiveColor    (ShadeContext &sc);
  float GetShadeOpacity          (ShadeContext &sc);
  float GetShadeSpecularCosPow   (ShadeContext &sc);
  float GetShadeReflectionCosPow (ShadeContext &sc);
  float GetShadeReflectionGloss  (ShadeContext &sc);
  float GetShadeTransparCosPow   (ShadeContext &sc);
  float GetShadeTransparGloss    (ShadeContext &sc);
  float GetShadeAnisotropy       (ShadeContext &sc);
  float GetShadeAnisRotation     (ShadeContext &sc);
};



class hydraMtlTagsClassDesc: public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  int           IsPublic()                   override { return TRUE; }
  void*         Create(BOOL loading = FALSE) override { return new HydraMtlTags(loading); }
  const TCHAR *	ClassName()                  override { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  SClass_ID     SuperClassID()               override { return MATERIAL_CLASS_ID; }
  Class_ID      ClassID()                    override { return hydraMtlTags_CLASS_ID; }
  const TCHAR*  Category()                   override { return GetString(IDS_CATEGORY); }
  const TCHAR*  InternalName()               override { return _T("hydraMtlTags"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE     HInstance()                  override { return hInstance; }					// returns owning module handle

  // For entry category
  FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  const MCHAR* GetEntryName()     const override;
  const MCHAR* GetEntryCategory() const override;
  Bitmap* GetEntryThumbnail()     const override;
};



class hydraMtlTagsDlgProc: public ParamMap2UserDlgProc
{
public:
  HydraMtlTags* hM  = nullptr;
  HWND thisHwnd     = nullptr;

  hydraMtlTagsDlgProc(HydraMtlTags *cb);

  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
  void DropFileInMapSlot(int mapControlID, hydraChar* filename);
  void DeleteThis() override { delete this; }
};


ClassDesc2* GetHydraMtlTagsDesc();


//Add enums for various parameters. Dont remove parameters, only add!
enum
{
  pb_spin,
  mtl_mat1,
  mtl_affect_shadow,
  mtl_diffuse_color,
  mtl_diffuse_map,
  mtl_diffuse_tint_on,
  mtl_diffuse_mult,
  mtl_specular_color,
  mtl_specular_map,
  mtl_specular_tint_on,
  mtl_specular_mult,
  mtl_specular_brdf,
  mtl_specular_roughness,
  mtl_specular_cospower,
  mtl_specular_ior,
  mtl_refl_color,
  mtl_refl_map,
  mtl_refl_tint_on,
  mtl_refl_mult,
  mtl_refl_brdf,
  mtl_refl_cospower,
  mtl_refl_roughness,
  mtl_refl_ior,
  mtl_emission_color,
  mtl_emission_map,
  mtl_emission_tint_on,
  mtl_emission_mult,
  mtl_transpar_color,
  mtl_transpar_map,
  mtl_transpar_tint_on,
  mtl_transpar_mult,
  mtl_transpar_thin_on,
  mtl_transpar_ior,
  mtl_transpar_cospower,
  mtl_transpar_dist_color,
  mtl_transpar_dist_mult,
  mtl_exit_color,
  mtl_displacement_on,
  mtl_displacement_height,
  mtl_normal_map,
  mtl_displacement_invert_height_on,
  mtl_no_ic_records,
  mtl_spec_gloss,
  mtl_spec_gl_map,
  mtl_spec_gloss_or_cos,
  mtl_spec_fresnel_on,
  mtl_refl_gloss,
  mtl_refl_gl_map,
  mtl_refl_gloss_or_cos,
  mtl_refl_fresnel_on,
  mtl_transpar_gloss,
  mtl_transpar_gl_map,
  mtl_transpar_gloss_or_cos,
  mtl_lock_specular,
  mtl_relief_amount,
  mtl_relief_smooth,
  mtl_bump_sigma,
  mtl_emission_cast_gi,
  mtl_opacity_map,
  mtl_opacity_smooth,
  mtl_transluc_color,
  mtl_transluc_mult,
  mtl_transluc_map,
  mtl_transluc_tint_on,
  mtl_refl_extrusion,
  mtl_roughness_mult,
  mtl_refl_anisotr,
  mtl_refl_rotat,
  mtl_refl_anisotr_map,
  mtl_refl_rotat_map
};

enum
{
  pb_matAny,
  pb_matCeramic,
  pb_matConcrete,
  pb_matEmission,
  pb_matFabric,
  pb_matGlass,
  pb_matLeather,
  pb_matLiquid,
  pb_matMarble,
  pb_matMetal,
  pb_matOrganic,
  pb_matPaint,
  pb_matPaper,
  pb_matPlastic,
  pb_matRubber,
  pb_matWood,

  pb_texTarget,

  pb_rndOverrideColor,
  pb_rndColor,
  pb_rndColorHue,
  pb_rndColorHueDistrib,
  pb_rndColorSat,
  pb_rndColorSatDistrib,
  pb_rndDiffMult,
  pb_rndDiffMultDistrib,
  pb_rndRoughMult,
  pb_rndRoughMultDistrib,
  pb_rndReflMult,
  pb_rndReflMultDistrib,
  pb_rndReflGloss,
  pb_rndReflGlossDistrib,
  pb_rndRefrMult,
  pb_rndRefrMultDistrib,
  pb_rndRefrGloss,
  pb_rndRefrGlossDistrib,
  pb_rndEmissMult,
  pb_rndEmissMultDistrib

};
