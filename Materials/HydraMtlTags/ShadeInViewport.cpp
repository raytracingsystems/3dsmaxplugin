#include "HydraMtlTags.h"
#include <lslights.h>
#include <units.h>
#include <Graphics/TextureHandleUtility.h>
#include <Graphics/Matrix44.h>

#include "pbrtMath.h"

////////////////////////////////////////////////////////////////////////////////////////////////
#define HydraLight_CLASS_ID	Class_ID(0x69729747, 0x8c40aaf7)

float toMeters(const float a_val)
{
  return GetMasterScale(UNITS_METERS) * a_val;
}

// Render and save texture.
void SaveTexture(Texmap* a_tex, const int a_width, const int a_height, const wchar_t* a_fullFileName)
{
  if (a_tex != nullptr)
  {
    BitmapInfo bi;
    bi.SetType(BMM_REALPIX_32);
    bi.SetFlags(MAP_HAS_ALPHA);
    bi.SetCustomFlag(0);
    bi.SetWidth(a_width);
    bi.SetHeight(a_height);
    bi.SetName(a_fullFileName);

    Bitmap* bmap = TheManager->Create(&bi);
        
    a_tex->RenderBitmap(0, bmap, 1.0f, true);

    bmap->OpenOutput(&bi);
    bmap->Write(&bi);
    bmap->Close(&bi);
    bmap->DeleteThis();
  }
}

//float clamp(float u, float a, float b) { float r = fmax(a, u); return fmin(r, b); }
float clamp(float u) { float r = fmax(0.0f, u); return fmin(r, 1.0f); }

void GetNormal(const LightscapeLight* pHLight, Point3 &lightNormal, const Matrix3 &tm)
{
  Point3 vertex[3];
  pHLight->GetShape(vertex, 3);
  const Point3 vec1 = vertex[0] - vertex[1];
  const Point3 vec2 = vertex[1] - vertex[2];
  lightNormal = Normalize(tm.VectorTransform(CrossProd(vec1, vec2)));
}

void MaxMatrix3ToMatrix44(const Matrix3& a_matrix3, MaxSDK::Graphics::Matrix44& a_matrix44)
{
  Point4 col1, col2, col3;

  col1 = a_matrix3.GetColumn(0);
  col2 = a_matrix3.GetColumn(1);
  col3 = a_matrix3.GetColumn(2);

  a_matrix44._11 = col1.x;
  a_matrix44._12 = col1.y;
  a_matrix44._13 = col1.z;
  a_matrix44._14 = col1.w;

  a_matrix44._21 = col2.x;
  a_matrix44._22 = col2.y;
  a_matrix44._23 = col2.z;
  a_matrix44._24 = col2.w;

  a_matrix44._31 = col3.x;
  a_matrix44._32 = col3.y;
  a_matrix44._33 = col3.z;
  a_matrix44._34 = col3.w;

  a_matrix44._41 = 0.0f;
  a_matrix44._42 = 0.0f;
  a_matrix44._43 = 0.0f;
  a_matrix44._44 = 1.0f;
}

Point3 GetColorWithTexture(const bool hasTexture, const Point3 color, const bool tint)
{
  if (hasTexture)
  {
    if (tint) return color;    
    else      return Point3(1.0f, 1.0f, 1.0f);          
  }
  else  
    return color;  
}


//const MaxSDK::Graphics::HLSLMaterialHandle & HydraMtlTags::GetHLSLMaterialHandle(MaxSDK::Graphics::GraphicFeatureLevel featureLevel)
//{
//  if (!m_HLSLMaterialHandle.IsValid())
//    CreateHLSLMaterialHandle(featureLevel);
//
//  return m_HLSLMaterialHandle;
//}


//void HydraMtlTags::CreateHLSLMaterialHandle(MaxSDK::Graphics::GraphicFeatureLevel featureLevel)
//{
//  bool result = m_HLSLMaterialHandle.InitializeWithFile(L"c:\\[Hydra]\\pluginFiles\\material\\hydraMtl.fx");
//
//  if (!result)
//  {
//    const wchar_t* log = m_HLSLMaterialHandle.GetShaderErrorInformation().data();
//    DebugOutputString(log);
//    materialLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, L"c:\\[Hydra]\\pluginFiles\\material\\hydraMtl.fx not initialized.");
//    materialLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, std::wstring(log));
//    SetMtlFlag(MTL_HW_MAT_ENABLED | MTL_HW_TEX_ENABLED);
//  }
//}

//bool HydraMtlTags::UpdateHLSLMaterial(const TimeValue t, MaxSDK::Graphics::GraphicFeatureLevel featureLevel)
//{
//  MaxSDK::Graphics::HLSLMaterialHandle matHandle = GetHLSLMaterialHandle(featureLevel);
//
//  static Point3 white       = Point3(1.0f, 1.0f, 1.0f);
//  static float masterScaler = GetMasterScale(UNITS_METERS);
//
//  //////////////////////////////////////////
//
//  // Get environment.
//
//  BOOL   useEnvMap     = GetCOREInterface()->GetUseEnvironmentMap();
//  Point3 envColor      = GetCOREInterface()->GetBackGround(t, FOREVER);
//  Point3 envAmbColor   = GetCOREInterface()->GetAmbient   (t, FOREVER);
//  Point3 globLightTint = GetCOREInterface()->GetLightTint (t, FOREVER) * GetCOREInterface()->GetLightLevel(t, FOREVER);  
//  bool   hasEnvirMap   = false;
//
//  
//  Texmap* envTex       = GetCOREInterface()->GetEnvironmentMap();   
//  
//  // Save and load 32 bit, but not reload texture in shader.
//  //SaveTexture(envTex, 1024, 512, L"c:\\[Hydra]\\pluginFiles\\material\\envir.hdr");  
//  //m_hlslEnvirMap.Initialize(L"c:\\[Hydra]\\pluginFiles\\material\\envir.hdr");    
//
//  // For some reason, it cuts to 8 bits.
//  m_hlslEnvirMap       = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(envTex, t);
//  
//
//  if (m_hlslEnvirMap.IsValid() && useEnvMap) 
//    hasEnvirMap = true;
//
//  MaxSDK::Graphics::Matrix44 envUVmatrix44;
//  envUVmatrix44.MakeIdentity();
//  if (hasEnvirMap)
//  {
//    Matrix3 envUVmatrix3;
//    envUVmatrix3.IdentityMatrix();
//    envTex->GetUVTransform(envUVmatrix3);
//    MaxMatrix3ToMatrix44(envUVmatrix3, envUVmatrix44);
//  }
//
//  //float uOffset = 0.0f;
//  //float vOffset = 0.0f;
//  //UVGen* coords = envTex->GetTheUVGen();
//  //if (coords && coords->IsStdUVGen())
//  //{
//  //  auto const stdUVCoords = static_cast<StdUVGen*>(coords);
//  //  uOffset = stdUVCoords->GetUOffs(t);
//  //  vOffset = stdUVCoords->GetVOffs(t);  
//  //}
//    
//  //////////////////////////////////////////
//
//
//  // Get Lights.
//
//  bool   lightEnable      = false;
//  bool   lightHasDir      = false;
//  int    lightType        = 0;
//  Point3 lightColor;  
//  Point4 lightArea(1, 1, 1, 0); 
//
//  //float IESrotX       = pHLight->GetWebRotateX();
//  //float IESrotY       = pHLight->GetWebRotateY();
//  //float IESrotZ       = pHLight->GetWebRotateZ();
//
//  //float hotspot       = pHLight->GetHotspot(t, valid);
//  //float fallsize      = pHLight->GetFallsize(t, valid);
//
//  //const LightscapeLight::DistTypes lightDistributionType = pHLight->GetDistribution();
//
//  enum { INTENS_UNIT_IMAGE, INTENS_UNIT_CD, INTENS_UNIT_LM };
//  enum { TYPE_POINT, TYPE_RECTANGLE, TYPE_DISK, TYPE_SPHERE, TYPE_CYLINDER, TYPE_PORTAL };
//
//  static int nLights        = 0;
//  const int  maxLightSample = 32;
//  //static std::vector<Point3> lightPos(maxLightSample);
//  static Point3 lightPos;
//  static Point3 lightDir(1.0f, 0.0f, 0.0f);
//
//  INode* root = GetCOREInterface()->GetRootNode();
//  for (int i = 0; i < root->NumberOfChildren(); i++)
//  {
//    INode* currNode = root->GetChildNode(i);
//    const ObjectState os = currNode->EvalWorldState(t);
//
//    if (os.obj != nullptr && os.obj->IsSubClassOf(HydraLight_CLASS_ID))
//    {
//      LightscapeLight* pHLight = static_cast<LightscapeLight*>(os.obj);
//
//      Matrix3 tm = currNode->GetObjectTM(TimeValue(0));
//      lightPos = tm.GetTrans();
//
//      //for (int i = 0; i < maxLightSample; i++)
//      //{
//      //  Point3 offset;
//      //  for (size_t j = 0; j < lightWidth; j++)
//      //    for (size_t k = 0; k < lightLength; k++)          
//      //      offset.Set(j, k, 0.0f);
//      //          
//      //  lightPos[i] = lightCenter.x + offset;
//      //}
//      lightEnable = pHLight->GetUseLight();// && (bool)pHLight->IsRenderable();
//
//      lightType      = pHLight->Type();
//      if (lightType == TYPE_RECTANGLE || lightType == TYPE_DISK || lightType == TYPE_PORTAL)
//      {
//        lightHasDir = true;      
//        GetNormal(pHLight, lightDir, tm); // Take the normal for flat light sources.
//      }
//
//      lightColor = pHLight->GetRGBColor(t);           
//      lightArea  = { toMeters(pHLight->GetLength(t)), toMeters(pHLight->GetWidth(t)), toMeters(pHLight->GetRadius(t)), pHLight->GetResultingIntensity(t) };
//
//      nLights++;
//    }
//  }
//
//  //////////////////////////////////////////
//  // Get textures.
//
//  bool hasDiffuseMap     = false;
//  bool hasReflectMap     = false;
//  bool hasReflGlosMap    = false;
//  bool hasTranspMap      = false;
//  bool hasTranspGlossMap = false;
//  bool hasOpacityMap     = false;
//  bool hasTranslucMap    = false;
//  bool hasEmissionMap    = false;
//  bool hasReliefMap      = false;
//  
//
//  m_hlslDiffuseMap     = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(DIFFUSE_TEX_SLOT)     , t);
//  m_hlslReflectMap     = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(REFLECT_TEX_SLOT)     , t);
//  m_hlslReflGlossMap   = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(REFL_GLOSS_TEX_SLOT)  , t);
//  m_hlslTranspMap      = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(TRANSP_TEX_SLOT)    , t);
//  m_hlslTranspGlossMap = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(TRANSP_GLOSS_TEX_SLOT), t);
//  m_hlslOpacityMap     = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(OPACITY_TEX_SLOT)     , t);
//  m_hlslTranslucMap    = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(TRANSLUCENCY_TEX_SLOT), t);
//  m_hlslEmissionMap    = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(EMISSION_TEX_SLOT)    , t);
//  m_hlslReliefMap      = MaxSDK::Graphics::TextureHandleUtility::CreateTextureHandle(GetSubTexmap(RELIEF_TEX_SLOT)   , t);
//
//  if (m_hlslDiffuseMap    .IsValid()) hasDiffuseMap     = true;
//  if (m_hlslReflectMap    .IsValid()) hasReflectMap     = true;
//  if (m_hlslReflGlossMap  .IsValid()) hasReflGlosMap    = true;
//  if (m_hlslTranspMap     .IsValid()) hasTranspMap      = true;
//  if (m_hlslTranspGlossMap.IsValid()) hasTranspGlossMap = true;
//  if (m_hlslOpacityMap    .IsValid()) hasOpacityMap     = true;
//  if (m_hlslTranslucMap   .IsValid()) hasTranslucMap    = true;
//  if (m_hlslEmissionMap   .IsValid()) hasEmissionMap    = true;
//  if (m_hlslReliefMap     .IsValid()) hasReliefMap      = true;
//
//  //////////////////////////////////////////
//
//  // Set HLSL custom parameters.
//  
//  matHandle.SetFloatParameter  (_M("g_masterScaler")     , masterScaler);
//  
//  // Environment.
//
//  matHandle.SetFloat3Parameter (_M("g_envColor")         , envColor);
//  matHandle.SetBoolParameter   (_M("g_hasEnvirMap")      , hasEnvirMap);
//  matHandle.SetTextureParameter(_M("g_envirMap")         , m_hlslEnvirMap);    
//  matHandle.SetFloat3Parameter (_M("g_globLightTint")    , globLightTint);    
//  matHandle.SetFloat3Parameter (_M("g_envAmbColor")      , envAmbColor);
//  matHandle.SetMatrixParameter(_M("g_envUVmatrix44")     , envUVmatrix44);
//  
//  
//  
//
//  // Lights.
//  ////matHandle.SetIntParameter(_M("g_maxLightSample")   , maxLightSample);
//  ////matHandle.SetFloat3ArrayParameter(_M("g_lightPos") , &lightPos[0], maxLightSample);
//  
//  matHandle.SetBoolParameter   (_M("g_lightEnable")      , lightEnable);
//  matHandle.SetFloat3Parameter (_M("g_lightPos")         , lightPos);
//  matHandle.SetFloat3Parameter (_M("g_lightDir")         , lightDir);
//  matHandle.SetFloat4Parameter (_M("g_lightColor")       , lightColor);
//  //matHandle.SetFloatParameter  (_M("g_lightIntens")      , lightIntens);
//  matHandle.SetIntParameter    (_M("g_lightType")        , lightType);
//  matHandle.SetBoolParameter   (_M("g_lightHasDir")      , lightHasDir);  
//  matHandle.SetFloat4Parameter (_M("g_lightArea")        , lightArea);
//
//
//  // Diffuse.
//  matHandle.SetFloatParameter  (_M("g_diffuseMult")      , diffuseMult);
//  matHandle.SetBoolParameter   (_M("g_hasDiffuseMap")    , hasDiffuseMap);
//  matHandle.SetTextureParameter(_M("g_diffuseMap")       , m_hlslDiffuseMap);
//  matHandle.SetFloat3Parameter (_M("g_diffuseColor")     , GetColorWithTexture(hasDiffuseMap, diffuseColor, diffuseTint));
//  matHandle.SetFloatParameter  (_M("g_diffuseRough")     , diffuseRoughness);
//
//  // Reflecivity.
//  matHandle.SetFloatParameter  (_M("g_reflectMult")      , reflectMult);
//  matHandle.SetBoolParameter   (_M("g_hasReflectMap")    , hasReflectMap);
//  matHandle.SetTextureParameter(_M("g_reflectMap")       , m_hlslReflectMap);
//  matHandle.SetFloat3Parameter (_M("g_reflectColor")     , GetColorWithTexture(hasReflectMap, reflectColor, reflectTint));
//  matHandle.SetFloatParameter  (_M("g_reflectGloss")     , reflectGloss);
//  matHandle.SetBoolParameter   (_M("g_hasReflGlosMap")   , hasReflGlosMap);
//  matHandle.SetTextureParameter(_M("g_reflectGlosMap")   , m_hlslReflGlossMap);
//  matHandle.SetFloatParameter  (_M("g_reflectIOR")       , reflectFresnel? reflectIor : 101);
//  matHandle.SetIntParameter    (_M("g_reflectBrdf")      , reflectBrdf);
//  matHandle.SetIntParameter    (_M("g_reflectExtrus")    , reflectExtrusion);
//                                                         
//  // Transparency.                                       
//  matHandle.SetFloatParameter  (_M("g_transpMult")       , transpMult);
//  matHandle.SetBoolParameter   (_M("g_hasTranspMap")     , hasTranspMap);
//  matHandle.SetTextureParameter(_M("g_transpMap")        , m_hlslTranspMap);
//  matHandle.SetFloat3Parameter (_M("g_transpColor")      , GetColorWithTexture(hasTranspMap, m_transpColor, transpTint));
//  matHandle.SetFloatParameter  (_M("g_transpGloss")      , transpGloss);
//  matHandle.SetBoolParameter   (_M("g_hasTranspGlossMap"), hasTranspGlossMap);
//  matHandle.SetTextureParameter(_M("g_transpGlosMap")    , m_hlslTranspGlossMap);
//  matHandle.SetFloatParameter  (_M("g_transpIOR")        , ior);
//  
//  // Opacity.
//  matHandle.SetBoolParameter   (_M("g_hasOpacityMap")    , hasOpacityMap);
//  matHandle.SetTextureParameter(_M("g_opacityMap")       , m_hlslOpacityMap);
//  matHandle.SetBoolParameter   (_M("g_opacitySmooth")    , opacitySmooth);
//
//  // Translucency.
//  matHandle.SetFloatParameter  (_M("g_translucMult")     , translucMult);
//  matHandle.SetBoolParameter   (_M("g_hasTranslucMap")   , hasTranslucMap);
//  matHandle.SetTextureParameter(_M("g_translucMap")      , m_hlslTranslucMap);
//  matHandle.SetFloat3Parameter (_M("g_translucColor")    , GetColorWithTexture(hasTranslucMap, m_translucColor, translucTint));
//  
//  // Emission.
//  matHandle.SetFloatParameter  (_M("g_emissionMult")     , emissionMult);
//  matHandle.SetBoolParameter   (_M("g_hasEmissionMap")   , hasEmissionMap);
//  matHandle.SetTextureParameter(_M("g_emissionMap")      , m_hlslEmissionMap);
//  matHandle.SetFloat3Parameter (_M("g_emissionColor")    , GetColorWithTexture(hasEmissionMap, emissionColor, emissionTint));
//
//  // Relief. 
//  matHandle.SetFloatParameter  (_M("g_bumpAmount")       , m_reliefAmount);
//  matHandle.SetBoolParameter   (_M("g_hasReliefMap")     , hasReliefMap);
//  matHandle.SetTextureParameter(_M("g_ReliefMap")        , m_hlslReliefMap);
//    
//  return true;
//}

