#include "HydraMtlTags.h"
#include <math.h> 
#include <cstdlib> // for random
#include "pbrtMath.h"


////////////////////////////////////////////////////////////////////////////////////////////////



void HydraMtlTags::Shade(ShadeContext& sc)
{  
  if (gbufID)
    sc.SetGBufferID(gbufID);

  sc.shadow                 = false;

  bool bumped               = false;
  Point3 sourceNormal       = sc.Normal();
  Point3 tbnBasis[2]; //The bump basic vectors. [0] - U (tangent). [1] - V (bitangent).
  Point3 normal;
  Point3 light;

  normal                    = sourceNormal;

  const Color diffColor     = GetShadeDiffuseColor(sc);
  Color translucColor       = GetShadeTranslucencyColor(sc);
  const float translucCoeff = fmax(translucColor.r, fmax(translucColor.g, translucColor.b));

  Color resultDiffuse(0.0f, 0.0f, 0.0f);
  Color resultTransluc(0.0f, 0.0f, 0.0f);


  // Calculate diffuse and translucence for real lights
  for (int i = 0; i < sc.nLights; i++)
  {
    LightDesc* l = sc.Light(i);

    if (l != nullptr)
    {
      float NdotL     = 0.0f;
      float diffCoeff = 0.0f;
      Color lightColor(1.0f, 1.0f, 1.0f);

      if (subTexSlots[RELIEF_TEX_SLOT] != nullptr && sc.doMaps) // perturb normal if we have any such and move all vectors to tangent space
      {
        bumped = true;

        Point3 reliefNormal = subTexSlots[RELIEF_TEX_SLOT]->EvalNormalPerturb(sc);

        //invert height not supported yet.
        //if (m_invertHeight)
        //{        
        //  reliefNormal.x *= (-1.0f);
        //  reliefNormal.y *= (-1.0f);
        //}      

        Point3 zero(0.0f, 0.0f, 0.0f);
        Lerp(reliefNormal, zero, 1.0f - m_reliefAmount / 5.0f);
        reliefNormal *= 5.0f; // looks like render and standart mat (bump amount 100).
        normal = Normalize(sc.Normal() + (sc.backFace ? -reliefNormal : reliefNormal));
        sc.SetNormal(normal);
      }

      l->Illuminate(sc, normal, lightColor, light, NdotL, diffCoeff);

      float nDotLMinus = 0.0f;

      if (NdotL < 0.0f)
      {
        nDotLMinus     = NdotL;
        NdotL          = 0.0f;
      }
      else
        translucColor = { 0, 0, 0 };

      // Diffuse
      if (m_diffuseRoughness > 0.0f)
        resultDiffuse += lightColor * diffColor * NdotL * DiffuseOrenNayar(light, -sc.OrigView(), normal, m_diffuseRoughness); // Oren-Nayar      
      else
        resultDiffuse += lightColor * diffColor * NdotL; // Lambert

      // Translucence
      resultTransluc += (lightColor * translucColor * abs(nDotLMinus));
      Lerp(resultDiffuse, resultTransluc, translucCoeff);
    }
  }


  //sc.DPdUVW(tbnBasis); //This returns the bump basis vectors (tangent, bitangent, normal) for UVW in camera space. 
  //sc.BumpBasisVectors(tbnBasis, AXIS_UV); 

  // Calculate approximate tangent and binormal from normal and view ray.
  const Point3 xAxis(1.0f, 0.0f, 0.0f);
  const float4x4 mRot             = RotateAroundVector4x4(xAxis, HALFPI);
  const Point3 rayTop             = mul3x3(mRot, sc.OrigView());
  tbnBasis[0]                     = CreateTangent(normal, rayTop);
  tbnBasis[1]                     = CrossProd(normal, tbnBasis[0]);



  sc.SetIOR(m_transpIor);
  Point3 reflVect                 = sc.ReflectVector();
  Point3 refractVect              = sc.RefractVector(m_transpIor);

  Color bg;
  Color bgt;
  sc.GetBGColor(bg, bgt, false);

  const Color transpColor         = GetShadeTransparencyColor(sc);
  const float transparencyCoeff   = fmax(transpColor.r, fmax(transpColor.g, transpColor.b));
  const float opacityCoeff        = GetShadeOpacity(sc);

  Color brdfReflects(0.0f, 0.0f, 0.0f);
  Color brdfRefracts(0.0f, 0.0f, 0.0f);
  const float glossReflect        = GetShadeReflectionGloss(sc);
  const float cosPowerReflect     = GetShadeReflectionCosPow(sc); // power of cosine in the original Phong model
  const float glossTransp         = GetShadeTransparGloss(sc);
  const float cosPowerTransp      = GetShadeTransparCosPow(sc); // power of cosine in the original Phong model

  const float reflectAnisotropy   = GetShadeAnisotropy(sc);
  const float reflectAnisRotate    = GetShadeAnisRotation(sc);

  // Extrusions
  const Color reflColor           = GetShadeReflectionColor(sc);
  float reflectCoeff;
  if (m_reflectExtrusion           == 0)
    reflectCoeff                  = fmax(reflColor.r, fmax(reflColor.g, reflColor.b)); // strong
  else
    reflectCoeff                  = Luminance(Point3(reflColor));                            // luminance

  // Fresnel
  float fresnel                   = 1.0f;
  if (m_reflectFresnel)
  {
    const float dotVN = abs(DotProd(sc.OrigView(), sc.Normal()));
    fresnel = FrCond(dotVN, m_reflectIor, 0.0f);
  }

  // Sampling. 
  int sample                = 0;
  int differCount           = 0;
  int maxGoalCount          = 32;
  float diffLumSummColor    = 0.0f;
  float prevLumSummColor    = 0.0f;
  const float error         = 0.001f;
  Color summColor(0.0f, 0.0f, 0.0f);

  offsetRnd++;
  if (offsetRnd >= UINT_MAX) offsetRnd = 0;
  srand(offsetRnd);

  // No need to parallelize here. 3d max apparently parallelize the rendering of the pixels themselves above.
  do
  {
    if (texmapEnv != nullptr)
    {
      Point2 rnd2d;
      rnd2d.x = simpleRand1D();
      rnd2d.y = simpleRand1D();

      if (rnd2d.y == 0.0f) continue;

      // Reflectivity
      if (reflectCoeff > 0.0f)
      {
        if (glossReflect < 1.0f)
        {
          switch (m_reflectBrdf)
          {
            case BRDF_PHONG:
              PhongSampleAndEvalBRDF(brdfReflects, sc, rnd2d, cosPowerReflect, texmapEnv, reflVect);
              break;
            case BRDF_TORRSPARR:
              BlinnSampleAndEvalBRDF(brdfReflects, sc, rnd2d, cosPowerReflect, texmapEnv, normal);
              break;
            case BRDF_BECKMANN:
              BeckmannSampleAndEvalBRDF(brdfReflects, sc, rnd2d, glossReflect, reflectAnisotropy, reflectAnisRotate, texmapEnv, normal, tbnBasis);
              break;
            case BRDF_GGX:
              GGXSample2AndEvalBRDF(brdfReflects, sc, rnd2d, glossReflect, texmapEnv, normal);
              break;
            case BRDF_TRGGX:
              TRGGXSampleAndEvalBRDF(brdfReflects, sc, rnd2d, glossReflect, reflectAnisotropy, reflectAnisRotate, texmapEnv, normal, tbnBasis);
              break;
            default:
              break;
          }
        }
        else
          brdfReflects = sc.EvalEnvironMap(texmapEnv, reflVect);
      }

      // Refraction
      if (Luminance(Point3(transpColor)) > 0.0f)
      {
        if (glossTransp < 1.0f)
          PhongSampleAndEvalBRDF(brdfRefracts, sc, rnd2d, cosPowerTransp, texmapEnv, refractVect);
        else
          brdfRefracts = sc.EvalEnvironMap(texmapEnv, refractVect);
      }

    }

    // Add components.

    // Diffuse.
    sc.out.c = resultDiffuse;


    // Transparency (mix with background).
    Lerp(sc.out.c, (brdfRefracts * transpColor), transparencyCoeff);


    // Reflectivity.
    Lerp(sc.out.c, brdfReflects * reflColor, reflectCoeff * fresnel);


    // Opacity / Cutoff (mix with background). 
    Lerp(sc.out.c, bg, 1.0f - opacityCoeff);


    // calculate final color
    const float alpha = 1.0f / float(sample + 1);
    summColor = summColor * (1.0f - alpha) + sc.out.c * alpha;


    // calculate error for stop sampling
    const float lumSummColor = pow(Luminance(Point3(summColor)), 1.0f / 2.2f); // with gamma correct

    diffLumSummColor = abs((prevLumSummColor - lumSummColor));
    prevLumSummColor = lumSummColor;


    if (diffLumSummColor < error)
      differCount++;

    sample++;
  } while (differCount < maxGoalCount/* && sample < MAX_SAMPLES*/); // end loop sampling.

  sc.out.c = summColor;

  // Emission.
  sc.out.c += GetShadeEmissiveColor(sc);


  // increased exposure and tone mapping, for non-linear visible, similar to most scenes.
  sc.out.c *= 1.5f;

  const float knee = 5.0f; // from 10 to 1. lower = softer.
  const float invKnee = 1.0f / knee;

  sc.out.c.r /= pow((1.0f + pow(sc.out.c.r, knee)), invKnee);
  sc.out.c.g /= pow((1.0f + pow(sc.out.c.g, knee)), invKnee);
  sc.out.c.b /= pow((1.0f + pow(sc.out.c.b, knee)), invKnee);

  // Opacity / Cutoff (mix with background). Once again dangerous, after compression.
  Lerp(sc.out.c, bg, 1.0f - opacityCoeff);


  if (bumped) sc.SetNormal(sourceNormal); // restore normal
}


//-----------------------------------------------------------------------------------


float HydraMtlTags::EvalDisplacement(ShadeContext& sc)
{
  if (subTexSlots[RELIEF_TEX_SLOT] != nullptr && sc.doMaps)
  {
    TSTR className;
    subTexSlots[RELIEF_TEX_SLOT]->GetClassName(className);

    if (className == L"Normal Bump")
      return 0.0f;
    else
      return m_reliefAmount * subTexSlots[RELIEF_TEX_SLOT]->EvalMono(sc);
  }

  return 0.0f;
}

Interval HydraMtlTags::DisplacementValidity(TimeValue t)
{
  //Interval iv; iv.SetInfinite();
  //return iv;	
  return FOREVER;
}



Color HydraMtlTags::GetShadeDiffuseColor(ShadeContext &sc)
{
  // After being evaluated, if a map or material has a non-zero gbufID, 
  // it should call ShadeContext::SetGBuffer() to store it into 
  // the shade context.
  if (gbufID)
    sc.SetGBufferID(gbufID);

  Color res;
  if (subTexSlots[DIFFUSE_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor = subTexSlots[DIFFUSE_TEX_SLOT]->EvalColor(sc);

    if (m_diffuseTint)
    {
      res = texColor * m_diffuseColor*m_diffuseMult;
      res.ClampMinMax();
      return res;
    }
    else
    {
      res = texColor * m_diffuseMult;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = m_diffuseColor * m_diffuseMult;
    res.ClampMinMax();
    return res;
  }
}

Color HydraMtlTags::GetShadeSpecularColor(ShadeContext &sc)
{
  Color res;

  if (subTexSlots[SPECULAR_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor = subTexSlots[SPECULAR_TEX_SLOT]->EvalColor(sc);

    if (m_specularTint)
    {
      res = texColor * m_specularColor * m_specularMult;
      res.ClampMinMax();
      return res;

    }
    else
    {
      res = texColor * m_specularMult;
      res.ClampMinMax();
      return res;

    }
  }
  else
  {
    res = m_specularColor * m_specularMult;
    res.ClampMinMax();
    return res;
  }
}

Color HydraMtlTags::GetShadeReflectionColor(ShadeContext &sc)
{
  Color res;

  if (subTexSlots[REFLECT_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor = subTexSlots[REFLECT_TEX_SLOT]->EvalColor(sc);

    if (m_reflectTint)
    {
      res = texColor * m_reflectColor * m_reflectMult;
      res.ClampMinMax();
      return res;
    }
    else
    {
      res = texColor * m_reflectMult;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = m_reflectColor * m_reflectMult;
    res.ClampMinMax();
    return res;
  }
}

Color HydraMtlTags::GetShadeEmissiveColor(ShadeContext &sc)
{
  if (subTexSlots[EMISSION_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor = subTexSlots[EMISSION_TEX_SLOT]->EvalColor(sc);

    if (m_emissionTint)
      return texColor * m_emissionColor * m_emissionMult;
    else
      return texColor * m_emissionMult;
  }
  else
    return m_emissionColor * m_emissionMult;
}

Color HydraMtlTags::GetShadeTransparencyColor(ShadeContext &sc)
{
  Color fogTransparency(1, 1, 1);
  Color res;
  if (!m_transpThin)
  {
    Lerp(fogTransparency, m_transpDistColor, (m_transpDistMult * 0.05f));
  }

  if (subTexSlots[TRANSP_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor(1, 1, 1);
    texColor = subTexSlots[TRANSP_TEX_SLOT]->EvalColor(sc);

    if (m_transpTint)
    {
      res = texColor * m_transpColor * m_transpMult * fogTransparency;
      res.ClampMinMax();
      return res;
    }
    else
    {
      res = texColor * m_transpMult * fogTransparency;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = m_transpColor * m_transpMult * fogTransparency;
    res.ClampMinMax();
    return res;
  }
}

float HydraMtlTags::GetShadeOpacity(ShadeContext & sc)
{
  if (subTexSlots[OPACITY_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor = subTexSlots[OPACITY_TEX_SLOT]->EvalColor(sc);

    float result = fmax(texColor.r, fmax(texColor.g, texColor.b));

    Clamp1(result);

    if (m_opacitySmooth)
      return result;
    else
    {
      if (result < 0.5f)
        return 0.0f;
      else
        return 1.0f;
    }
  }
  else
    return 1.0f;
}

Color HydraMtlTags::GetShadeTranslucencyColor(ShadeContext & sc)
{
  Color res;

  if (subTexSlots[TRANSLUCENCY_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor = subTexSlots[TRANSLUCENCY_TEX_SLOT]->EvalColor(sc);

    if (m_translucTint)
    {
      res = texColor * m_translucColor * m_translucMult;
      res.ClampMinMax();
      return res;
    }
    else
    {
      res = texColor * m_translucMult;
      res.ClampMinMax();
      return res;
    }
  }
  else
  {
    res = m_translucColor * m_translucMult;
    res.ClampMinMax();
    return res;
  }
}



float HydraMtlTags::GetShadeSpecularCosPow(ShadeContext &sc)
{
  if (m_specGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[SPEC_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_specularGloss * (subTexSlots[SPEC_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_specularGloss;

    return CosPowerFromGlosiness(glosiness);
  }
  else
    return m_specularCospow;
}

float HydraMtlTags::GetShadeReflectionCosPow(ShadeContext &sc)
{
  if (m_reflectGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[REFL_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_reflectGloss * (subTexSlots[REFL_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_reflectGloss;

    return CosPowerFromGlosiness(glosiness);
  }
  else
    return m_reflectCospow;
}

float HydraMtlTags::GetShadeReflectionGloss(ShadeContext &sc)
{
  if (m_reflectGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[REFL_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_reflectGloss * (subTexSlots[REFL_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_reflectGloss;

    Clamp01_2(glosiness);

    return glosiness;
  }
  else
    return m_reflectCospow;
}

float HydraMtlTags::GetShadeTransparCosPow(ShadeContext &sc)
{
  if (m_transpGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[TRANSP_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_transpGloss * (subTexSlots[TRANSP_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_transpGloss;

    return CosPowerFromGlosiness(glosiness);
  }
  else
    return m_transpCospower;
}

float HydraMtlTags::GetShadeTransparGloss(ShadeContext &sc)
{
  if (m_transpGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[TRANSP_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_transpGloss * (subTexSlots[TRANSP_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_transpGloss;

    Clamp01_2(glosiness);

    return glosiness;
  }
  else
    return m_transpCospower;
}

float HydraMtlTags::GetShadeAnisotropy(ShadeContext &sc)
{
  float anisotropy;

  if (subTexSlots[REFL_ANISOTR_TEX_SLOT] != NULL && sc.doMaps)
    anisotropy = m_reflectAnisotr * (subTexSlots[REFL_ANISOTR_TEX_SLOT]->EvalMono(sc));
  else
    anisotropy = m_reflectAnisotr;

  Clamp01_2(anisotropy);

  return anisotropy;
}

float HydraMtlTags::GetShadeAnisRotation(ShadeContext &sc)
{
  float anisRot;

  if (subTexSlots[REFL_ROTATION_TEX_SLOT] != NULL && sc.doMaps)
    anisRot = subTexSlots[REFL_ROTATION_TEX_SLOT]->EvalMono(sc);
  else
    anisRot = m_reflectAnisRotat / 360.0f;

  Clamp01_2(anisRot);

  return anisRot;
}



////////////////////////////////////////////////////////////////////////////////////////////
// BRDFs
////////////////////////////////////////////////////////////////////////////////////////////

// Oren-Nayar
float HydraMtlTags::DiffuseOrenNayar(const Point3 a_l, const Point3 a_v, const Point3 a_n, const float a_roughness)
{
  const float cosTheta_wi = DotProd(a_l, a_n);
  const float cosTheta_wo = DotProd(a_v, a_n);

  const float sinTheta_wi = sqrt(fmax(0.0f, 1.0f - cosTheta_wi * cosTheta_wi));
  const float sinTheta_wo = sqrt(fmax(0.0f, 1.0f - cosTheta_wo * cosTheta_wo));

  const float sigma       = a_roughness * HALFPI; //Radians(sig)
  const float sigma2      = sigma * sigma;
  const float A           = 1.0f - (sigma2 / (2.0f * (sigma2 + 0.33f)));
  const float B           = 0.45f * sigma2 / (sigma2 + 0.09f);

  ///////////////////////////////////////////////////////////////////////////// to PBRT coordinate system
  // wo = a_v = -ray_dir
  // wi = a_l = newDir
  //
  Point3 nx, ny = a_n, nz;
  CoordinateSystem(ny, nx, nz);
  {
    const Point3 temp = ny;
    ny = nz;
    nz = temp;
  }

  const Point3 wo = Point3(-DotProd(a_v, nx), -DotProd(a_v, ny), -DotProd(a_v, nz));
  const Point3 wi = Point3(-DotProd(a_l, nx), -DotProd(a_l, ny), -DotProd(a_l, nz));
  //
  ///////////////////////////////////////////////////////////////////////////// to PBRT coordinate system

  // Compute cosine term of Oren-Nayar model
  float maxcos = 0.0f;

  if (sinTheta_wi > 1e-4 && sinTheta_wo > 1e-4)
  {
    const float sinphii = SinPhiPBRT(wi, sinTheta_wi);
    const float cosphii = CosPhiPBRT(wi, sinTheta_wi);
    const float sinphio = SinPhiPBRT(wo, sinTheta_wo);
    const float cosphio = CosPhiPBRT(wo, sinTheta_wo);
    const float dcos    = cosphii * cosphio + sinphii * sinphio;
    maxcos              = fmax(0.0f, dcos);
  }

  // Compute sine and tangent terms of Oren-Nayar model
  float sinalpha = 0.0f, tanbeta = 0.0f;

  if (fabs(cosTheta_wi) > fabs(cosTheta_wo))
  {
    sinalpha = sinTheta_wo;
    tanbeta  = sinTheta_wi / fmax(fabs(cosTheta_wi), DEPSILON);
  }
  else
  {
    sinalpha = sinTheta_wi;
    tanbeta  = sinTheta_wo / fmax(fabs(cosTheta_wo), DEPSILON);
  }

  return (A + B * maxcos * sinalpha * tanbeta);
}


// Phong
void HydraMtlTags::PhongSampleAndEvalBRDF(Color &resultReflects, ShadeContext &sc, const Point2 rnd2d, const float cosPower, Texmap* a_texmapEnv, const Point3 reflVect)
{
  const float f = TWOPI * rnd2d.x;
  const float sin_theta = sqrt(1.0f - pow(rnd2d.y, 2.0f / (cosPower + 1.0f)));

  float3 deviation;
  deviation.x = sin_theta * cos(f);
  deviation.y = sin_theta * sin(f);
  deviation.z = pow(rnd2d.y, 1.0f / (cosPower + 1.0f));

  //Generation of tangent and bitangent spaces to align the hemisphere to normal.      
  //const Point3 reflect = reflVect; // x - horiz, y - vert, z - far
  Point3 nx, nz;
  CoordinateSystem(reflVect, nx, nz);

  const Point3 newDir = Normalize(nx * deviation.x + reflVect * deviation.z + nz * deviation.y); // deviation.z <-> deviation.y !!!
  
  const float cosAlpha                  = DotProd(newDir, reflVect);
  const float powCosAlphaCosPow         = pow(cosAlpha, cosPower);
  const float powCosAlphaCosPowInvTwoPi = powCosAlphaCosPow * INV_TWOPI;
  const float pdf                       = fmax(powCosAlphaCosPowInvTwoPi * (cosPower + 1.0f), 1e-6f);
  const float brdf                      = powCosAlphaCosPowInvTwoPi * (cosPower + 2.0f);

  resultReflects = brdf / pdf * cosAlpha * sc.EvalEnvironMap(texmapEnv, newDir);
}


// Torrance-Sparrow with Blinn distribution.
void HydraMtlTags::BlinnSampleAndEvalBRDF(Color &resultReflects, ShadeContext &sc, const Point2 rnd2d, const float cosPower,
                                      Texmap* a_texmapEnv, Point3 a_normal)
{
  // wo = v = ray_dir
  // wi = l = -newDir

  Point3 nx, ny = a_normal, nz;
  CoordinateSystem(ny, nx, nz);
  {
    Point3 temp = ny;
    ny          = nz;
    nz          = temp;
  }
  const Point3 ray_dir = sc.OrigView();
  const Point3 wo      = Point3(-DotProd(ray_dir, nx), -DotProd(ray_dir, ny), -DotProd(ray_dir, nz));

  ///////////////////////////////////////////////////////////////////////////// to PBRT coordinate system

  const float exponent = cosPower;

  const float costheta = pow(rnd2d.y, 1.0f / (exponent + 1.0f));
  const float sintheta = sqrt(fmax(0.0f, 1.0f - costheta * costheta));
  const float phi      = TWOPI * rnd2d.x;

  // Compute sampled half-angle vector wh for Blinn distribution
  const Point3 wh      = SphericalDirection(sintheta, costheta, phi);
  const Point3 wi      = (2.0f * DotProd(wo, wh) * wh) - wo;            // Compute incident direction by reflecting about wh
  const Point3 newDir  = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL    = DotProd(a_normal, newDir);

  if (dotNL > 0.0f)
  {
    const float D      = ((exponent + 2.0f) * INV_TWOPI * pow(costheta, exponent));
    const float GF1    = TorranceSparrowGF1(wo, wi);
    const float pdf    = ((exponent + 1.0f) * pow(costheta, exponent)) / fmax(TWOPI * 4.0f * DotProd(wo, wh), 1e-6f);

    resultReflects     = (dotNL * D * GF1 / pdf * sc.EvalEnvironMap(a_texmapEnv, newDir));
  }
}


void HydraMtlTags::BeckmannSampleAndEvalBRDF(Color & resultReflects, ShadeContext & sc, const Point2 rnd2d, const float gloss, const float anisotropy, const float anisRotation, Texmap * a_texmapEnv, Point3 a_normal, const Point3 tbBasis[2])
{
  const float roughness = 0.5f - 0.5f*gloss;
  const float anisoMult = 1.0f - anisotropy;
  const Point2 alpha(BeckmannRoughnessToAlpha(roughness*roughness), BeckmannRoughnessToAlpha(roughness*roughness*anisoMult*anisoMult));

  Point3 nx, ny = a_normal, nz;

  // wo = v = ray_dir
  // wi = l = -newDir

  if (fabs(alpha.x - alpha.y) > 1.0e-5f)
  {
    nx = tbBasis[1]; //a_bitan;
    nz = tbBasis[0]; //a_tan;

    const float4x4 mRot = RotateAroundVector4x4(ny, anisRotation * TWOPI);
    nx = mul3x3(mRot, nx);
    nz = mul3x3(mRot, nz);
  }
  else
    CoordinateSystem(ny, nx, nz);

  const Point3 temp    = ny;
  ny                   = nz;
  nz                   = temp;

  const Point3 ray_dir = sc.OrigView();
  const Point3 wo      = Point3(-DotProd(ray_dir, nx), -DotProd(ray_dir, ny), -DotProd(ray_dir, nz));
  const Point3 wh      = BeckmannDistributionSampleWH(wo, Point2(rnd2d.x, rnd2d.y), alpha.x, alpha.y);
  const Point3 wi      = (2.0f * DotProd(wo, wh) * wh) - wo; // Compute incident direction by reflecting about wh
  const Point3 newDir  = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL    = DotProd(a_normal, newDir);
  if (dotNL > 0.0f)
  {
    const float pdf    = fmaxf(BeckmannDistributionPdf(wo, wh, alpha.x, alpha.y), DEPSILON);
    resultReflects     = (dotNL * fmin(BeckmannBRDF_PBRT(wo, wi, alpha.x, alpha.y), 500.0f) / pdf * sc.EvalEnvironMap(a_texmapEnv, newDir));
  }
}



static float GGX_GeomShadMask(const float cosThetaN, const float alpha)
{
  const float cosTheta_sqr = Clamp01_2(cosThetaN*cosThetaN);
  const float tan2         = (1.0f - cosTheta_sqr) / fmax(cosTheta_sqr, 1e-6f);
  const float GP           = 2.0f / (1.0f + sqrt(1.0f + alpha * alpha * tan2));
  return GP;
}

static float GGX_Distribution(const float cosThetaNH, const float alpha)
{
  const float alpha2 = alpha * alpha;
  const float NH_sqr = Clamp01_2(cosThetaNH * cosThetaNH);
  const float den    = NH_sqr * alpha2 + (1.0f - NH_sqr);
  return alpha2 / fmax(PI * den * den, 1e-6f);
}


void HydraMtlTags::GGXSampleAndEvalBRDF(Color & resultReflects, ShadeContext & sc, const Point2 rnd2d, const float gloss, Texmap * a_texmapEnv, Point3 a_normal)
{
  const float  roughness = 1.0f - gloss;
  const float  roughSqr  = roughness * roughness;

  Point3 nx, ny, nz      = a_normal;

  CoordinateSystem(nz, nx, ny);

  const Point3 ray_dir = sc.OrigView();

  // wo = v = ray_dir
  // wi = l = -newDir

  // to PBRT coordinate system
  const Point3 wo       = Point3(-DotProd(ray_dir, nx), -DotProd(ray_dir, ny), -DotProd(ray_dir, nz));

  // Compute sampled half-angle vector wh
  const float phi       = rnd2d.x * TWOPI;
  const float cosTheta  = Clamp01_2(sqrt((1.0f - rnd2d.y) / (1.0f + roughSqr * roughSqr * rnd2d.y - rnd2d.y)));
  const float sinTheta  = sqrt(1.0f - cosTheta * cosTheta);
  Point3 wh             = SphericalDirection(sinTheta, cosTheta, phi);
  const Point3 wi       = (2.0f * DotProd(wo, wh) * wh) - wo;              // Compute incident direction by reflecting about wh    
  const Point3 newDir   = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);          // back to normal coordinate system

  const float dotNL     = DotProd(a_normal, newDir);

  if (dotNL > 0.0f)
  {
    const Point3 v      = (-1.0f) * ray_dir;
    const Point3 h      = Normalize(v + newDir); // half vector.
    const float dotNV   = DotProd(a_normal, v);
    const float dotNH   = DotProd(a_normal, h);
    const float dotHV   = DotProd(h, v);

    //const float F     = Fresnel is not needed here, because it is used for the blend with diffusion.
    const float D       = GGX_Distribution(dotNH, roughSqr);
    const float G       = GGX_GeomShadMask(dotNV, roughSqr) * GGX_GeomShadMask(dotNL, roughSqr);

    const float pdf     = D * dotNH / fmax(4.0f * dotHV, 1e-6f);
    const float bxdfVal = D /** F*/ * G / fmax(4.0f * dotNV, 1e-6f); // removing dotNL from the denominator (4.0f*dotNV*dotNL), since there is a division by it at the end.

    resultReflects      = (bxdfVal / pdf * sc.EvalEnvironMap(a_texmapEnv, newDir));
  }
}

///////////////////////////////////////////
// GGX sample v.2
// https://hal.archives-ouvertes.fr/hal-01509746/document
// https://schuttejoe.github.io/post/ggximportancesamplingpart2/
///////////////////////////////////////////

float SmithGGXMasking(const float dotNV, float roughSqr)
{
  const float denomC = sqrt(roughSqr + (1.0f - roughSqr) * dotNV * dotNV) + dotNV;
  return 2.0f * dotNV / fmax(denomC, 1e-6f);
}


float SmithGGXMaskingShadowing(const float dotNL, const float dotNV, float roughSqr)
{
  const float denomA = dotNV * sqrt(roughSqr + (1.0f - roughSqr) * dotNL * dotNL);
  const float denomB = dotNL * sqrt(roughSqr + (1.0f - roughSqr) * dotNV * dotNV);
  return 2.0f * dotNL * dotNV / fmax(denomA + denomB, 1e-6f);
}


Point3 GgxVndf(Point3 wo, float roughness, float u1, float u2)
{
  // -- Stretch the view vector so we are sampling as though
  // -- roughness==1
  const Point3 v = Normalize(Point3(wo.x * roughness, wo.y * roughness, wo.z));

  // -- Build an orthonormal basis with v, t1, and t2
  const Point3 XAxis(1.0f, 0.0f, 0.0f);
  const Point3 ZAxis(0.0f, 0.0f, 1.0f);
  const Point3 t1 = (v.z < 0.999f) ? Normalize(CrossProd(v, ZAxis)) : XAxis;
  const Point3 t2 = CrossProd(t1, v);

  // -- Choose a point on a disk with each half of the disk weighted
  // -- proportionally to its projection onto direction v
  const float a = 1.0f / (1.0f + v.z);
  const float r = sqrt(u1);
  const float phi = (u2 < a) ? (u2 / a) * PI : PI + (u2 - a) / (1.0f - a) * PI;
  const float p1 = r * cos(phi);
  const float p2 = r * sin(phi) * ((u2 < a) ? 1.0f : v.z);

  // -- Calculate the normal in this stretched tangent space
  const Point3 n = p1 * t1 + p2 * t2 + sqrt(fmax(0.0f, 1.0f - p1 * p1 - p2 * p2)) * v;

  // -- unstretch and normalize the normal
  return Normalize(Point3(roughness * n.x, roughness * n.y, fmax(0.0f, n.z)));
}


void HydraMtlTags::GGXSample2AndEvalBRDF(Color & resultReflects, ShadeContext & sc, const Point2 rnd2d, const float gloss, Texmap * a_texmapEnv, Point3 a_normal)
{
  const float  roughness = 1.0f - gloss;
  const float  roughSqr = roughness * roughness;

  Point3 nx, ny, nz = a_normal;

  CoordinateSystem(nz, nx, ny);

  const Point3 ray_dir = sc.OrigView();

  // to PBRT coordinate system
  // wo = v = ray_dir
  // wi = l = -newDir   

  const Point3 wo       = Point3(-DotProd(ray_dir, nx), -DotProd(ray_dir, ny), -DotProd(ray_dir, nz));
  const Point3 wh       = GgxVndf(wo, roughSqr, rnd2d.x, rnd2d.y);
  const Point3 wi       = (2.0f * DotProd(wo, wh) * wh) - wo;            // Compute incident direction by reflecting about wm    
  const Point3 newDir   = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL     = DotProd(a_normal, newDir);

  if (dotNL > 0.0f)
  {
    const float dotNV   = DotProd(a_normal, -ray_dir);

    //const float F     = Fresnel is not needed here, because it is used for the blend with diffusion.
    const float G1      = SmithGGXMasking(dotNV, roughSqr);
    const float G2      = SmithGGXMaskingShadowing(dotNL, dotNV, roughSqr);

    resultReflects      = /*F **/ G2 / G1 * sc.EvalEnvironMap(a_texmapEnv, newDir);
  }
}

void HydraMtlTags::TRGGXSampleAndEvalBRDF(Color & resultReflects, ShadeContext & sc, const Point2 rnd2d, const float gloss, const float anisotropy, const float anisRotation, Texmap * a_texmapEnv, Point3 a_normal, const Point3 tbBasis[2])
{
  const float roughness = 0.5f - 0.5f*gloss;
  const float anisoMult = 1.0f - anisotropy;
  const Point2 alpha(BeckmannRoughnessToAlpha(roughness*roughness), BeckmannRoughnessToAlpha(roughness*roughness*anisoMult*anisoMult));

  Point3 nx, ny = a_normal, nz;

  // wo = v = ray_dir
  // wi = l = -newDir
  //
  if (fabs(alpha.x - alpha.y) > 1.0e-5f)
  {
    nx = tbBasis[1]; //a_bitan;
    nz = tbBasis[0]; //a_tan;

    const float4x4 mRot = RotateAroundVector4x4(ny, anisRotation * TWOPI);
    nx = mul3x3(mRot, nx);
    nz = mul3x3(mRot, nz);
  }
  else
    CoordinateSystem(ny, nx, nz);

  const Point3 temp = ny;
  ny = nz;
  nz = temp;

  const Point3 ray_dir = sc.OrigView();
  const Point3 wo = Point3(-DotProd(ray_dir, nx), -DotProd(ray_dir, ny), -DotProd(ray_dir, nz));
  const Point3 wh = TrowbridgeReitzDistributionSampleWH(wo, Point2(rnd2d.x, rnd2d.y), alpha.x, alpha.y);
  const Point3 wi = (2.0f * DotProd(wo, wh) * wh) - wo;                // Compute incident direction by reflecting about wh
  const Point3 newDir = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL = DotProd(a_normal, newDir);

  if (dotNL > 0.0f)
  {
    const float pdf = fmaxf(TrowbridgeReitzDistributionPdf(wo, wh, alpha.x, alpha.y), DEPSILON);
    resultReflects = (dotNL * TrowbridgeReitzBRDF_PBRT(wo, wi, alpha.x, alpha.y) / pdf * sc.EvalEnvironMap(a_texmapEnv, newDir));
  }
}
