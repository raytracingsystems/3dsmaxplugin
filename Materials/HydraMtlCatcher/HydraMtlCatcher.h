#pragma once

#include "resource.h"
#include "iparamm2.h"
#include <string>
#include "stdmat.h"
#include <IMaterialBrowserEntryInfo.h>

//////////////////////////////////////////////////////////////////////////////////

extern TCHAR    *GetString(int id);
extern HINSTANCE hInstance;

#define HydraMtlCatcher_CLASS_ID	Class_ID(0x3cd640a9, 0x36af3dcd)

enum
{
  CAMERAMAPPED_TEX_SLOT, SPECULAR_TEX_SLOT, REFLECT_TEX_SLOT, EMISSION_TEX_SLOT, TRANSPAR_TEX_SLOT, NORMALMAP_TEX_SLOT,
  SPEC_GLOSS_TEX_SLOT, REFL_GLOSS_TEX_SLOT, TRANSP_GLOSS_TEX_SLOT, OPACITY_TEX_SLOT, TRANSLUCENCY_TEX_SLOT, NUM_SUBTEX
};

constexpr int PBLOCK_REF      = NUM_SUBTEX;
constexpr int VERSION_CURRENT = 1;

using         hydraStr        = std::wstring;
using         hydraChar       = wchar_t;

//////////////////////////////////////////////////////////////////////////////////

class HydraMtlCatcher: public Mtl
{
public:

  // Parameter block
  IParamBlock2	*pblock;	
  Texmap*        subTex[NUM_SUBTEX]; 

  Color reflectColor;
  BOOL receiveShadowOn, fresnelOn, affectShadowsOn;
  float aoMult, aoDistance, specular_roughness, reflectGloss, reflect_cospower, specular_cospower, ior, specular_ior, reflect_ior, 
    transparency_cospower, fog_multiplier, displacement_height, diffuse_mult, specular_mult, emission_mult, transparency_mult, reflectMult;

  Interval		ivalid;


  ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams* imp)           override;
  void      Update(TimeValue t, Interval& valid)                      override;
  Interval  Validity(TimeValue t)                                     override;
  void      Reset()                                                   override;

  void NotifyChanged();

  // From MtlBase and Mtl
  void  SetAmbient       (Color c, TimeValue t)              noexcept override;
  void  SetDiffuse       (Color c, TimeValue t)              noexcept override;
  void  SetSpecular      (Color c, TimeValue t)              noexcept override;
  void  SetShininess     (float v, TimeValue t)              noexcept override;
  Color GetAmbient       (int mtlNum=0, BOOL backFace=FALSE)          override;
  Color GetDiffuse       (int mtlNum=0, BOOL backFace=FALSE)          override;
  Color GetSpecular      (int mtlNum=0, BOOL backFace=FALSE)          override;
  float GetXParency      (int mtlNum=0, BOOL backFace=FALSE) noexcept override;
  float GetShininess     (int mtlNum=0, BOOL backFace=FALSE) noexcept override;
  float GetShinStr       (int mtlNum=0, BOOL backFace=FALSE) noexcept override;
  float WireSize         (int mtlNum=0, BOOL backFace=FALSE) noexcept override;


  // Shade and displacement calculation
  //
  void     Shade(ShadeContext& sc)                     override;
  float    EvalDisplacement(ShadeContext& sc) noexcept override;
  Interval DisplacementValidity(TimeValue t)           override;

  // SubMaterial access methods
  int      NumSubMtls()                       noexcept override { return 0; }
  Mtl*     GetSubMtl(int i)                   noexcept override { return nullptr; }
  void     SetSubMtl(int i, Mtl *m)                    override;

#ifdef MAX2022
  MSTR     GetSubMtlSlotName(int i, bool localized)    override;
  MSTR     GetSubMtlTVName  (int i, bool localized = true);
#else      
  TSTR     GetSubMtlSlotName(int i)                    override;
  TSTR     GetSubMtlTVName(int i);
#endif // MAX2022

  // SubTexmap access methods
  int      NumSubTexmaps()                    noexcept override { return NUM_SUBTEX; }
  Texmap*  GetSubTexmap(int i)                noexcept override { return subTex[i]; }  
  void     SetSubTexmap(int i, Texmap *m)              override;

#ifdef MAX2022
  MSTR     GetSubTexmapSlotName(int i, bool localized) override;
  MSTR     GetSubTexmapTVName  (int i, bool localized = true);
#else      
  TSTR     GetSubTexmapSlotName(int i)                 override;
  TSTR     GetSubTexmapTVName(int i);
#endif // MAX2022


  BOOL     SetDlgThing(ParamDlg* dlg)         noexcept override;

  HydraMtlCatcher(BOOL loading);
  // ~hydraMaterial(){};

   // For Paramblock2 is not necessary. It saves everything automatically.
   // Loading/Saving
   //IOResult Load(ILoad *iload);
   //IOResult Save(ISave *isave);

   //From Animatable
  Class_ID  ClassID()                           override { return HydraMtlCatcher_CLASS_ID; }
  SClass_ID SuperClassID()             noexcept override { return MATERIAL_CLASS_ID; }

#ifdef MAX2022
  void      GetClassName(MSTR& s, bool localized = true) const override { s = GetString(IDS_CLASS_NAME); }
#else
  void      GetClassName(TSTR& s)                override { s = GetString(IDS_CLASS_NAME); }
#endif // MAX2022

  RefTargetHandle Clone(RemapDir& remap)         override;

  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID,
    RefMessage message, BOOL propagate) override;


  int             NumSubs()             noexcept override { return 1; }
  Animatable*     SubAnim(int i)        noexcept override;


#ifdef MAX2022
  MSTR            SubAnimName(int i, bool localized) override;
#else
  TSTR            SubAnimName(int i)             override;
#endif // MAX2022


  // Maintain the number or references here 
  int             NumRefs()             noexcept override { return 1 + NUM_SUBTEX; }
  RefTargetHandle GetReference(int i)   noexcept override;
  void            SetReference(int i, RefTargetHandle rtarg) override;


  int	            NumParamBlocks()      noexcept override { return 1; }					// return number of ParamBlocks in this instance
  IParamBlock2*   GetParamBlock(int i)  noexcept override { return pblock; } // return i'th ParamBlock
  IParamBlock2*   GetParamBlockByID(BlockID id)  override { return (pblock->ID() == id) ? pblock : nullptr; } // return id'd ParamBlock

  void            DeleteThis()          noexcept override { delete this; }

};



class HydraMtlCatcherClassDesc: public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  int          IsPublic()           noexcept override { return TRUE; }
  void*        Create(BOOL loading = FALSE)  override { return new HydraMtlCatcher(loading); }
  const TCHAR* ClassName()                   override { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  SClass_ID    SuperClassID()       noexcept override { return MATERIAL_CLASS_ID; }
  Class_ID     ClassID()                     override { return HydraMtlCatcher_CLASS_ID; }
  const TCHAR* Category()                    override { return GetString(IDS_CATEGORY); }
  const TCHAR* InternalName()       noexcept override { return _T("HydraMtlCatcher"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE    HInstance()          noexcept override { return hInstance; }					// returns owning module handle

  // For entry category
  FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  const MCHAR* GetEntryName()          const override;
  const MCHAR* GetEntryCategory()      const override;
  Bitmap*      GetEntryThumbnail()     const noexcept override;

#ifdef MAX2022
  const MCHAR* NonLocalizedClassName()       override { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
#endif // MAX2022

};


ClassDesc2* GethydraMaterialDesc();

enum { hydramaterial_params };


// Add enums for various parameters
enum
{
  pb_spin,
  mtl_mat1,
  mtl_mat1_on,
  mtl_cameraMappedBackMap,
  mtl_opacityMult,
  mtl_opacityMap,
  mtl_receiveShadowOn,
  mtl_aoMult,
  mtl_aoDistance,
  mtl_reflectColor,
  mtl_reflectMult,
  mtl_reflectGloss,
  mtl_reflect_ior,
  mtl_fresnelOn

  //mtl_diffuse_color,
 // mtl_diffuse_map,
 // mtl_diffuse_mult_on,
 // mtl_diffuse_mult,
  //mtl_specular_color,
 // mtl_specular_map,
 // mtl_specular_mult_on,
 // mtl_specular_mult,
 // mtl_specular_brdf,
  //mtl_specular_roughness,
  //mtl_specular_cospower,
  //mtl_specular_ior,
 // mtl_reflect_color,
 // mtl_reflect_map,
 // mtl_reflect_mult_on,
 // mtl_reflect_mult,
 // mtl_reflect_brdf,
 // mtl_reflect_cospower,
 // mtl_reflect_roughness,
  //mtl_reflect_ior,
 // mtl_emission_color,
 // mtl_emission_map,
 // mtl_emission_mult_on,
 // mtl_emission_mult,
 // mtl_transparency_color,
 // mtl_transparency_map,
 // mtl_transparency_mult_on,
 // mtl_transparency_mult,
 // mtl_transparency_thin_on,
 // mtl_ior,
 // mtl_transparency_cospower,
 // mtl_fog_color,
 // mtl_fog_multiplier,
 // mtl_exit_color,
 // mtl_displacement_on,
 // mtl_displacement_height,
 // mtl_normal_map,
 // mtl_displacement_invert_height_on,
  //mtl_no_ic_records,
  //mtl_spec_gloss,
  //mtl_spec_gl_map,
  //mtl_spec_gloss_or_cos,
  //mtl_spec_fresnel_on,
  //mtl_refl_gloss,
  //mtl_refl_gl_map,
  //mtl_refl_gloss_or_cos,
  //mtl_refl_fresnel_on,
  //mtl_transp_gloss,
  //mtl_transp_gl_map,
  //mtl_transp_gloss_or_cos,
  //mtl_lock_specular,
  //mtl_bump_amount,
  //mtl_bump_radius,
  //mtl_bump_sigma,
  //mtl_emission_gi,
  //mtl_opacity_map,
  //mtl_shadow_matte,
  //mtl_translucency_color,
  //mtl_translucency_mult,
  //mtl_translucency_map,
  //mtl_translucency_mult_on,
  //mtl_reflect_extrusion,
 // mtl_roughness_mult
};


