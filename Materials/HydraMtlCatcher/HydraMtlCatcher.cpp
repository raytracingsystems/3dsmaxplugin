#include "HydraMtlCatcher.h"
#include "3dsmaxport.h"

//////////////////////////////////////////////////////////////////////////
///
static HydraMtlCatcherClassDesc hydraMaterialDesc;
ClassDesc2* GethydraMaterialDesc()
{
  return &hydraMaterialDesc;
}

static ParamBlockDesc2 hydraMtlCatcher_param_blk(hydramaterial_params, _T("params"), 0, &hydraMaterialDesc,
                                                 P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF,
                                                 //rollout
                                                 IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
                                                 mtl_mat1_on, _T("mtl_mat1_on"), TYPE_BOOL, 0, IDS_MTL1ON,
                                                 p_default, TRUE,
                                                 p_ui, TYPE_SINGLECHEKBOX, IDC_MTLON1,
                                                 p_end,
                                                 mtl_cameraMappedBackMap, _T("cameraMappedBackMap"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_CAMERAMAPPEDBACK_MAP,
                                                 p_refno, 0,
                                                 p_subtexno, 0,
                                                 p_ui, TYPE_TEXMAPBUTTON, IDC_CAMERAMAPPEDBACK_MAP,
                                                 p_end,
                                                 mtl_opacityMult, _T("opacityMult"), TYPE_FLOAT, P_ANIMATABLE, IDS_OPACITY_MULT,
                                                 p_default, 1.0f,
                                                 p_range, 0.0f, 1.0f,
                                                 p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_OPACITY_MULT, IDC_OPACITY_MULT_SPINNER, 0.1f,
                                                 p_end,
                                                 mtl_opacityMap, _T("opacityMap"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_OPACITY_MAP,
                                                 p_refno, 1,
                                                 p_subtexno, 1,
                                                 p_ui, TYPE_TEXMAPBUTTON, IDC_OPACITY_MAP,
                                                 p_end,
                                                 mtl_receiveShadowOn, _T("recieveShadowOn"), TYPE_BOOL, 0, IDS_RECEIVE_SHADOW_ON,
                                                 p_default, FALSE,
                                                 p_ui, TYPE_SINGLECHEKBOX, IDC_RECEIVE_SHADOW_ON,
                                                 p_end,
                                                 mtl_aoMult, _T("aoMult"), TYPE_FLOAT, P_ANIMATABLE, IDS_AO_MULT,
                                                 p_default, 0.0f,
                                                 p_range, 0.0f, 1.0f,
                                                 p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_AO_MULT, IDC_AO_MULT_SPINNER, 0.1f,
                                                 p_end,
                                                 mtl_aoDistance, _T("aoDistance"), TYPE_FLOAT, P_ANIMATABLE, IDS_AO_DISTANCE_MULT,
                                                 p_default, 0.0f,
                                                 p_range, 0.0f, 100.0f,
                                                 p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_AO_DISTANCE_MULT, IDC_AO_DISTANCE_MULT_SPINNER, 0.1f,
                                                 p_end,
                                                 mtl_reflectColor, _T("reflectColor"), TYPE_RGBA, P_ANIMATABLE, IDS_REFLECT_COLOR,
                                                 p_default, Color(1.0, 1.0, 1.0),
                                                 p_ui, TYPE_COLORSWATCH, IDC_REFLECT_COLOR,
                                                 p_end,
                                                 mtl_reflectMult, _T("reflectMult"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_MULT,
                                                 p_default, 0.0f,
                                                 p_range, 0.0f, 1.0f,
                                                 p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_MULT, IDC_REFLECT_MULT_SPINNER, 0.1f,
                                                 p_end,
                                                 mtl_reflectGloss, _T("reflectGloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFL_GLOSSINESS,
                                                 p_default, 1.0f,
                                                 p_range, 0.0f, 1.0f,
                                                 p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_ROUGHNESS, IDC_REFLECT_GLOSSINESS_SPINNER, 0.1f,
                                                 p_end,
                                                 mtl_reflect_ior, _T("reflect_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_IOR,
                                                 p_default, 1.5f,
                                                 p_range, 0.0f, 50.0f,
                                                 p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_IOR, IDC_REFLECT_IOR_SPINNER, 0.5f,
                                                 p_end,
                                                 mtl_fresnelOn, _T("fresnelOn"), TYPE_BOOL, 0, IDS_REFL_FRESNEL_ON,
                                                 p_default, TRUE,
                                                 p_ui, TYPE_SINGLECHEKBOX, IDC_REFL_FRESNEL_ON,
                                                 p_end,
                                                 p_end
);

class HydraMtlCatcherDlgProc: public ParamMap2UserDlgProc
{
public:
  HydraMtlCatcher* hMltC  = nullptr;
  HWND thishWnd           = nullptr;

  bool additional_init;

  HydraMtlCatcherDlgProc(HydraMtlCatcher *cb);

  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
  void DropFileInMapSlot(int mapControlID, hydraChar* filename);
  void DeleteThis() { delete this; }
};

HydraMtlCatcherDlgProc::HydraMtlCatcherDlgProc(HydraMtlCatcher *cb)
  :hMltC(cb)
{
  additional_init = true;
}



void HydraMtlCatcherDlgProc::DropFileInMapSlot(int mapControlID, hydraChar* filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  //hydraChar dir[1024];
  //hydraChar name[256];
  //hydraChar extension[16];

  //SplitFilename(filename, dir, name, extension);

  WStr dir;
  WStr name;
  WStr extension;
  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);


  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap *tex = (Texmap *)btex;

  switch (mapControlID)
  {
    case IDC_CAMERAMAPPEDBACK_MAP:
      hMltC->SetSubTexmap(CAMERAMAPPED_TEX_SLOT, tex);
      hMltC->subTex[CAMERAMAPPED_TEX_SLOT] = tex;
      break;
    case IDC_OPACITY_MAP:
      hMltC->SetSubTexmap(OPACITY_TEX_SLOT, tex);
      hMltC->subTex[OPACITY_TEX_SLOT] = tex;
      break;
    default:
      break;
  }
}

INT_PTR HydraMtlCatcherDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thishWnd = hWnd;
  HydraMtlCatcherDlgProc *dlg = DLGetWindowLongPtr<HydraMtlCatcherDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
    case WM_INITDIALOG:
    {
      DragAcceptFiles(GetDlgItem(hWnd, IDC_CAMERAMAPPEDBACK_MAP), true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_OPACITY_MAP), true);
      return TRUE;
    }
    break;
    case WM_COMMAND:
      switch (LOWORD(wParam))
      {
        case IDC_CAMERAMAPPEDBACK_MAP:
        case IDC_OPACITY_MAP:
        default:
          break;
      }
      break;
    case WM_DESTROY:
      break;
    case WM_DROPFILES:
      POINT pt;
      WORD numFiles;
      HWND dropTarget;
      int dropTargetID;

      hydraChar lpszFile[80];

      DragQueryPoint((HDROP)wParam, &pt);

      numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

      if (numFiles == 0)
      {
        DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
        dropTarget = RealChildWindowFromPoint(hWnd, pt);
        dropTargetID = GetDlgCtrlID(dropTarget);
        DropFileInMapSlot(dropTargetID, lpszFile);
      }

      DragFinish((HDROP)wParam);

      break;
    default:
      return FALSE;

  }
  return TRUE;
}


HydraMtlCatcher::HydraMtlCatcher(BOOL loading)
{
  pblock = nullptr;

  for (int i = 0; i < NUM_SUBTEX ; i++) subTex[i] = nullptr;

  reflectColor = Color(1.0, 1.0, 1.0);

  receiveShadowOn = true;
  fresnelOn = true;

  reflectMult = 0.0f;
  reflectGloss = 1.0f;
  reflect_ior = 1.5f;

  if (!loading)
    GethydraMaterialDesc()->MakeAutoParamBlocks(this);
}


void HydraMtlCatcher::Reset()
{
  ivalid.SetEmpty();

  for (int i = 0; i < NUM_SUBTEX; i++)
  {
    if (subTex[i])
    {
      DeleteReference(i);
      subTex[i] = nullptr;
    }
  }

  GethydraMaterialDesc()->MakeAutoParamBlocks(this);
}


ParamDlg* HydraMtlCatcher::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp)
{
  IAutoMParamDlg* masterDlg = GethydraMaterialDesc()->CreateParamDlgs(hwMtlEdit, imp, this);

  // Set param block user dialog if necessary
  hydraMtlCatcher_param_blk.SetUserDlgProc(new HydraMtlCatcherDlgProc(this));
  return masterDlg;
}

BOOL HydraMtlCatcher::SetDlgThing(ParamDlg* dlg) noexcept
{
  return FALSE;
}

Interval HydraMtlCatcher::Validity(TimeValue t)
{
  Interval valid = FOREVER;

  for (int i = 0; i < NUM_SUBTEX; i++)
  {
    if (subTex[i])
      valid &= subTex[i]->Validity(t);
  }

  return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/

RefTargetHandle HydraMtlCatcher::GetReference(int i) noexcept
{
  if (i < NUM_SUBTEX)
    return subTex[i];
  else
    return pblock;

}

void HydraMtlCatcher::SetReference(int i, RefTargetHandle rtarg)
{
  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NUM_SUBTEX && s != L"ParamBlock2")
    subTex[i] = (Texmap *)rtarg;
  else
    pblock = (IParamBlock2 *)rtarg;
}

#ifdef MAX2022
MSTR HydraMtlCatcher::SubAnimName(int i, bool localized)
{
  return TSTR(_T(""));
}
#else
TSTR HydraMtlCatcher::SubAnimName(int i)
{
  //if (i < NSUBMTL)
  //  return GetSubMtlTVName(i);
  return TSTR(_T(""));
}
#endif

Animatable* HydraMtlCatcher::SubAnim(int i) noexcept
{
 if (i < NUM_SUBTEX)
     return subTex[i];
 else
   return pblock;
}


RefResult HydraMtlCatcher::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
    case REFMSG_CHANGE:
      ivalid.SetEmpty();

      if (hTarget == pblock)
      {
        ParamID changing_param = pblock->LastNotifyParamID();
        hydraMtlCatcher_param_blk.InvalidateUI(changing_param);
      }
      break;
  }
  return REF_SUCCEED;
}


/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/

void HydraMtlCatcher::SetSubMtl(int i, Mtl *m)
{
  ReplaceReference(i, m);
}

#ifdef MAX2022
MSTR HydraMtlCatcher::GetSubMtlSlotName(int i, bool localized)
{
  return _T("");
}

MSTR HydraMtlCatcher::GetSubMtlTVName(int i, bool localized)
{
  return GetSubMtlSlotName(i, false);
}

#else
TSTR HydraMtlCatcher::GetSubMtlSlotName(int i)
{
  // Return i'th sub-material name 
  return _T("");
}

TSTR HydraMtlCatcher::GetSubMtlTVName(int i)
{
  return GetSubMtlSlotName(i);
}
#endif // MAX2022

/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/


void HydraMtlCatcher::SetSubTexmap(int i, Texmap *m)
{
  ReplaceReference(i, m);

  switch (i)
  {
    case 0:
      hydraMtlCatcher_param_blk.InvalidateUI(mtl_cameraMappedBackMap);
      pblock->SetValue(mtl_cameraMappedBackMap, TimeValue(0), m);
      ivalid.SetEmpty();
      break;
    case 1:
      hydraMtlCatcher_param_blk.InvalidateUI(mtl_opacityMap);
      pblock->SetValue(mtl_opacityMap, TimeValue(0), m);
      ivalid.SetEmpty();
      break;

    default:
      break;
  }

  NotifyChanged();
}


#ifdef MAX2022
MSTR HydraMtlCatcher::GetSubTexmapSlotName(int i, bool localized)
{
  switch (i)
  {
  case 0:  return GetString(IDS_CAMERAMAPPEDBACK_MAP);
  case 1:  return GetString(IDS_OPACITY_MAP);

  default: return _T("");
  }
}

MSTR HydraMtlCatcher::GetSubTexmapTVName(int i, bool localized)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i, false);
}

#else
TSTR HydraMtlCatcher::GetSubTexmapSlotName(int i)
{
  switch (i)
  {
    case 0:  return GetString(IDS_CAMERAMAPPEDBACK_MAP);
    case 1:  return GetString(IDS_OPACITY_MAP);

    default: return _T("");
  }
}

TSTR HydraMtlCatcher::GetSubTexmapTVName(int i)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i);
}
#endif // 2022



/*===========================================================================*\
 |	Standard IO
\*===========================================================================*/

//#define MTL_HDR_CHUNK 0x4000
//#define HYDRA_CHUNK 0x5000
//
//IOResult HydraMtlCatcher::Save(ISave *isave)
//{
//  IOResult res;
//  ULONG nb;
//
//  isave->BeginChunk(MTL_HDR_CHUNK);
//  res = MtlBase::Save(isave);
//  if (res != IO_OK) return res;
//  isave->EndChunk();
//
//  isave->BeginChunk(HYDRA_CHUNK);
//  
//  isave->Write(&reflectColor, sizeof(Color), &nb);
//
//  isave->Write(&receiveShadowOn, sizeof(bool), &nb);
//  isave->Write(&fresnelOn, sizeof(bool), &nb);
//
//
//  isave->Write(&aoMult, sizeof(float), &nb);
//  isave->Write(&aoDistance, sizeof(float), &nb);
//  isave->Write(&reflectMult, sizeof(float), &nb);
//  isave->Write(&reflectGloss, sizeof(float), &nb);
//  isave->Write(&reflect_ior, sizeof(float), &nb);
//
//  isave->EndChunk();
//
//
//  return IO_OK;
//}
//
//IOResult HydraMtlCatcher::Load(ILoad *iload)
//{
//  IOResult res;
//  int id;
//  ULONG nb;
//  while (IO_OK == (res = iload->OpenChunk()))
//  {
//    switch (id = iload->CurChunkID())
//    {
//    case MTL_HDR_CHUNK:
//      res = MtlBase::Load(iload);
//      break;
//    case HYDRA_CHUNK:
//      res = iload->Read(&reflectColor, sizeof(Color), &nb);
//
//      res = iload->Read(&receiveShadowOn, sizeof(bool), &nb);
//      res = iload->Read(&fresnelOn, sizeof(bool), &nb);
//
//      res = iload->Read(&aoMult, sizeof(float), &nb);
//      res = iload->Read(&aoDistance, sizeof(float), &nb);
//      res = iload->Read(&reflectMult, sizeof(float), &nb);
//      res = iload->Read(&reflectGloss, sizeof(float), &nb);
//      res = iload->Read(&reflect_ior, sizeof(float), &nb);
//
//      break;
//    }
//    iload->CloseChunk();
//    if (res != IO_OK)
//      return res;
//  }
//
//  return IO_OK;
//}


/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/

RefTargetHandle HydraMtlCatcher::Clone(RemapDir &remap)
{
  HydraMtlCatcher *mnew = new HydraMtlCatcher(FALSE);
  *((MtlBase*)mnew) = *((MtlBase*)this);

  mnew->ReplaceReference(0, remap.CloneRef(pblock));

  mnew->reflectColor = reflectColor;

  mnew->receiveShadowOn = receiveShadowOn;
  mnew->fresnelOn = fresnelOn;

  mnew->aoMult = aoMult;
  mnew->aoDistance = aoDistance;
  mnew->reflectMult = reflectMult;
  mnew->reflectGloss = reflectGloss;
  mnew->reflect_ior = reflect_ior;

  mnew->ivalid.SetEmpty();

  for (int i = 0; i < NUM_SUBTEX; i++)
  {
    mnew->subTex[i] = nullptr;
    if (subTex[i])
      mnew->ReplaceReference(i, remap.CloneRef(subTex[i]));
  }

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}

void HydraMtlCatcher::NotifyChanged()
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void HydraMtlCatcher::Update(TimeValue t, Interval& valid)
{
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();
    pblock->GetValue(mtl_mat1_on, t, affectShadowsOn, ivalid);

    pblock->GetValue(mtl_receiveShadowOn, t, receiveShadowOn, ivalid);
    pblock->GetValue(mtl_aoMult, t, aoMult, ivalid);
    pblock->GetValue(mtl_aoDistance, t, aoDistance, ivalid);
    pblock->GetValue(mtl_reflectColor, t, reflectColor, ivalid);
    pblock->GetValue(mtl_reflectMult, t, reflectMult, ivalid);
    pblock->GetValue(mtl_reflectGloss, t, reflectGloss, ivalid);
    pblock->GetValue(mtl_reflect_ior, t, reflect_ior, ivalid);
    pblock->GetValue(mtl_fresnelOn, t, fresnelOn, ivalid);


    for (int i = 0; i < NUM_SUBTEX; i++)
    {
      if (subTex[i])
        subTex[i]->Update(t, ivalid);
    }
  }
  valid &= ivalid;
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/


void HydraMtlCatcher::SetAmbient(Color c, TimeValue t) noexcept {}
void HydraMtlCatcher::SetDiffuse(Color c, TimeValue t) noexcept
{
  //diffuse_color = c;
  //pblock->SetValue(mtl_diffuse_color, t, diffuse_color);
  //NotifyChanged();
}
void HydraMtlCatcher::SetSpecular(Color c, TimeValue t) noexcept
{
  //specular_color = c;
  //pblock->SetValue(mtl_specular_color, t, specular_color);
  //NotifyChanged();
}
void HydraMtlCatcher::SetShininess(float v, TimeValue t) noexcept
{
  //log(reflect_cospower)/log(100000.0)
  //derp

  //specular_roughness = v;
  //pblock->SetValue(mtl_specular_roughness, t, specular_roughness);
  //NotifyChanged();
}

Color HydraMtlCatcher::GetAmbient(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetAmbient(mtlNum,backFace):Color(0,0,0);
  //return diffuse_color*diffuse_mult;
  Color color(0.2f, 0.2f, 0.2f);
  return color;
}

Color HydraMtlCatcher::GetDiffuse(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetDiffuse(mtlNum,backFace):Color(0,0,0);
  //return diffuse_color*diffuse_mult;
  Color color(1, 1, 1);
  return color;
}

Color HydraMtlCatcher::GetSpecular(int mtlNum, BOOL backFace)           { return { 0.0F, 0.0F, 0.0F }; }
float HydraMtlCatcher::GetXParency (int mtlNum, BOOL backFace) noexcept { return 0.9F; }
float HydraMtlCatcher::GetShininess(int mtlNum, BOOL backFace) noexcept { return 0.0F; }
float HydraMtlCatcher::GetShinStr  (int mtlNum, BOOL backFace) noexcept { return 0.0F; }
float HydraMtlCatcher::WireSize    (int mtlNum, BOOL backFace) noexcept { return 0.0F; }


FPInterface * HydraMtlCatcherClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * HydraMtlCatcherClassDesc::GetEntryName() const
{
  return const_cast<HydraMtlCatcherClassDesc*>(this)->ClassName();

}

const MCHAR * HydraMtlCatcherClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Materials\\Hydra";
  return str;
}

Bitmap * HydraMtlCatcherClassDesc::GetEntryThumbnail() const noexcept
{
  return nullptr;
}
