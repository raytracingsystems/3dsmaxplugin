#include "HydraMtlCatcher.h"
#include "..\..\HydraRenderer\ImportStructs.h"

inline float clamp(float u, float a, float b) { float r = max(a, u); return min(r, b); }


/*===========================================================================*\
 |	Actual shading takes place
\*===========================================================================*/

void HydraMtlCatcher::Shade(ShadeContext& sc) 
{
  if(gbufID) 
    sc.SetGBufferID(gbufID);

  sc.out.c = { 0.5F, 0.5F, 0.5F };
  sc.out.t = { 0.5F, 0.5F, 0.5F };
}

float HydraMtlCatcher::EvalDisplacement(ShadeContext& sc) noexcept
{
  return 0.0F;
}

Interval HydraMtlCatcher::DisplacementValidity(TimeValue t)
{
  return FOREVER;
}

