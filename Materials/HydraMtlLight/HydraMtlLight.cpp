#include "HydraMtlLight.h"
#include "3dsmaxport.h"
#include "stdmat.h"

/////////////////////////////////////////////////////////////////////////////////////

static HydraMtlLightClassDesc hydraMtlLightDesc;
ClassDesc2* GetHydraMtlLightDesc()
{
  return &hydraMtlLightDesc;
}

static ParamBlockDesc2 hydraMtlLight_param_blk(
  HydraMtlLight_params, _T("params"), 0, &hydraMtlLightDesc,
  P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF,
  //rollout
  IDD_PANEL, IDS_PARAMETERS, 0, 0, NULL,
    mtl_light_color_mult, _T("mtl_light_color_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_COLOR_MULT,
      p_default, 1.0F,
      p_range, 0.0F, 10000.0F,
      p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_COLOR_MULT, IDC_COLOR_MULT_SPINNER, 1.0F,
    p_end,
    mtl_light_color_map, _T("mtl_light_color_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_COLOR_MAP,
      p_refno, 0,
      p_subtexno, 0,
      p_ui, TYPE_TEXMAPBUTTON, IDC_COLOR_MAP,
    p_end,
    mtl_light_color, _T("mtl_light_color"), TYPE_RGBA, P_ANIMATABLE, IDS_COLOR,
      p_default, Color(1.0F, 1.0F, 1.0F),
      p_ui, TYPE_COLORSWATCH, IDC_COLOR,
    p_end,
    mtl_light_color_tint_on, _T("mtl_light_color_tint_on"), TYPE_BOOL, 0, IDS_COLOR_TINT_ON,
      p_default, FALSE,
      p_ui, TYPE_SINGLECHEKBOX, IDC_COLOR_TINT_ON,
    p_end,
  p_end
);

class HydraMtlLayerDlgProc: public ParamMap2UserDlgProc
{
public:
  HydraMtlLight* hM;
  HWND thisHwnd                          = nullptr;

  ICustEdit*       gui_color_multEdit    = nullptr;
  ISpinnerControl* gui_color_multSpin    = nullptr;
  ICustButton*     gui_color_map         = nullptr;
  IColorSwatch*    gui_color_colorSwatch = nullptr;

  bool             additional_init       = true;

  explicit HydraMtlLayerDlgProc(HydraMtlLight *cb) noexcept;
  INT_PTR  DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
  void     DeleteThis() noexcept override { delete this; }
  void     texBtnText();
  void     DropFileInMapSlot(int mapControlID, const hydraChar* filename);
};


HydraMtlLayerDlgProc::HydraMtlLayerDlgProc(HydraMtlLight *cb) noexcept
  : hM(cb) 
{
  //additional_init     = true;

  //gui_color_multEdit    = nullptr;
  //gui_color_multSpin    = nullptr;
  //gui_color_map         = nullptr;
  //gui_color_colorSwatch = nullptr;
}

void HydraMtlLayerDlgProc::texBtnText()
{
  if (hM->subTex[0]) gui_color_map->SetText(_M("M")); else gui_color_map->SetText(_M(" "));
}


void HydraMtlLayerDlgProc::DropFileInMapSlot(int mapControlID, const hydraChar* filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  //MaxSDK::AssetManagement::AssetUser u;

  //u = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(filename, MaxSDK::AssetManagement::kBitmapAsset);

  WStr dir;
  WStr name;
  WStr extension;

  const WStr filename_wstr = WStr(filename);

  SplitFilename(filename_wstr, &dir, &name, &extension);

  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  //tex->SetMap(u);
  btex->SetMapName(filename);
  btex->SetName(name);

  Texmap* tex = dynamic_cast<Texmap*>(btex);

  switch (mapControlID)
  {
    case IDC_COLOR_MAP:
      hM->SetSubTexmap(COLOR_TEX_SLOT, tex);
      hM->subTex[COLOR_TEX_SLOT] = tex;
      break;
    default:
      break;
  }
}


INT_PTR HydraMtlLayerDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thisHwnd=hWnd;
  const HydraMtlLayerDlgProc* dlg = DLGetWindowLongPtr<HydraMtlLayerDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG)
    return FALSE;

  switch (msg)
  {
    case WM_INITDIALOG:
    {
      //dlg = (hydraMaterialDlgProc *)lParam;
      //DLSetWindowLongPtr(hWnd, dlg);

      if (!gui_color_multEdit) gui_color_multEdit = GetICustEdit(GetDlgItem(hWnd, IDC_COLOR_MULT));

      if (!gui_color_multSpin) gui_color_multSpin = GetISpinner(GetDlgItem(hWnd, IDC_COLOR_MULT_SPINNER));

      if (!gui_color_map) gui_color_map = GetICustButton(GetDlgItem(hWnd, IDC_COLOR_MAP));

      gui_color_colorSwatch = GetIColorSwatch(GetDlgItem(hWnd, IDC_COLOR));

      DragAcceptFiles(GetDlgItem(hWnd, IDC_COLOR_MAP), true);


      return TRUE;
      //break;
    }
    case WM_COMMAND:
      switch (LOWORD(wParam))
      {
        case IDC_DIFFUSE_MAP:
          break;
      }
      break;
    case WM_DESTROY:

      ReleaseICustEdit(gui_color_multEdit);
      ReleaseISpinner(gui_color_multSpin);
      ReleaseICustButton(gui_color_map);
      ReleaseIColorSwatch(gui_color_colorSwatch);

      break;
    case WM_DROPFILES:
      POINT pt;
      WORD numFiles;
      HWND dropTarget;
      int dropTargetID;

      hydraChar lpszFile[80];

      DragQueryPoint((HDROP)wParam, &pt);

      numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

      if (numFiles == 0)
      {
        DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
        dropTarget = RealChildWindowFromPoint(hWnd, pt);
        dropTargetID = GetDlgCtrlID(dropTarget);
        DropFileInMapSlot(dropTargetID, lpszFile);
      }

      DragFinish((HDROP)wParam);

      break;
    default:
      return FALSE;
  }
  return TRUE;
}


HydraMtlLight::HydraMtlLight(BOOL loading)
{
  pblock = nullptr;
  subTex[0] = nullptr;

  light_color_mult    = 1.0f;
  light_color         = Color(1.0, 1.0, 1.0);
  light_color_tint_on = false;

  if (!loading)
    GetHydraMtlLightDesc()->MakeAutoParamBlocks(this);
}


void HydraMtlLight::Reset()
{
  ivalid.SetEmpty();
    
  if (subTex[0])
  {
    DeleteReference(0);
    subTex[0] = nullptr;
  }
  

  GetHydraMtlLightDesc()->MakeAutoParamBlocks(this);
}


ParamDlg* HydraMtlLight::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp)
{
  IAutoMParamDlg* masterDlg = GetHydraMtlLightDesc()->CreateParamDlgs(hwMtlEdit, imp, this);

  // Set param block user dialog if necessary
  hydraMtlLight_param_blk.SetUserDlgProc(new HydraMtlLayerDlgProc(this));
  return masterDlg;
}

BOOL HydraMtlLight::SetDlgThing(ParamDlg* dlg) noexcept
{
  return FALSE;
}

Interval HydraMtlLight::Validity(TimeValue t)
{
  Interval valid = FOREVER;
  
  if (subTex[0])
    valid &= subTex[0]->Validity(t);

  return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/

RefTargetHandle HydraMtlLight::GetReference(int i) noexcept
{
  if (i < NUM_SUBTEX)
    return subTex[i];
  else
    return pblock;
}

void HydraMtlLight::SetReference(int i, RefTargetHandle rtarg)
{
  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NUM_SUBTEX && s != L"ParamBlock2")
    subTex[i] = (Texmap *)rtarg;
  else
    pblock = (IParamBlock2 *)rtarg;
}

#ifdef MAX2022
MSTR HydraMtlLight::SubAnimName(int i, bool localized)
{
  //if (i < NSUBMTL)
  //  return GetSubMtlTVName(i);
  return TSTR(_T(""));
}
#else
TSTR HydraMtlLight::SubAnimName(int i)
{
  //if (i < NSUBMTL)
  //  return GetSubMtlTVName(i);
  return TSTR(_T(""));
}
#endif // MAX2022

Animatable* HydraMtlLight::SubAnim(int i) noexcept
{
  if (i < NUM_SUBTEX)
    return subTex[i];
  else
    return pblock;
}


RefResult HydraMtlLight::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
    case REFMSG_CHANGE:
      ivalid.SetEmpty();

      if (hTarget == pblock)
      {
        ParamID changing_param = pblock->LastNotifyParamID();
        hydraMtlLight_param_blk.InvalidateUI(changing_param);
      }
      break;
  }
  return REF_SUCCEED;
}


/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/

Mtl* HydraMtlLight::GetSubMtl(int i) noexcept
{
  return nullptr;
}

void HydraMtlLight::SetSubMtl(int i, Mtl *m)
{
  ReplaceReference(i, m);
  // Set the material and update the UI	
}


#ifdef MAX2022
MSTR HydraMtlLight::GetSubMtlSlotName(int i, bool localized)
{
  // Return i'th sub-material name 
  return _T("");
}
MSTR HydraMtlLight::GetSubMtlTVName(int i, bool localized)
{
  return GetSubMtlSlotName(i, false);
}
#else
TSTR HydraMtlLight::GetSubMtlSlotName(int i)
{
  // Return i'th sub-material name 
  return _T("");
}

TSTR HydraMtlLight::GetSubMtlTVName(int i)
{
  return GetSubMtlSlotName(i);
}
#endif // MAX2022




/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/

Texmap* HydraMtlLight::GetSubTexmap(int i) noexcept
{
  return subTex[i];
}

void HydraMtlLight::SetSubTexmap(int i, Texmap *m)
{
  ReplaceReference(i, m);

  switch (i)
  {
    case 0:
      hydraMtlLight_param_blk.InvalidateUI(mtl_light_color_map);
      pblock->SetValue(mtl_light_color_map, TimeValue(0), m);
      ivalid.SetEmpty();
      break;
    default:
      break;
  }

  NotifyChanged();
}

#ifdef MAX2022
MSTR HydraMtlLight::GetSubTexmapSlotName(int i, bool localized)
{
  switch (i)
  {
  case 0:  return GetString(IDS_COLOR_MAP);
  default: return _T("");
  }
}

MSTR HydraMtlLight::GetSubTexmapTVName(int i, bool localized)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i, false);
}
#else
TSTR HydraMtlLight::GetSubTexmapSlotName(int i)
{
  switch (i)
  {
    case 0:  return GetString(IDS_COLOR_MAP);
    default: return _T("");
  }
}

TSTR HydraMtlLight::GetSubTexmapTVName(int i)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i);
}
#endif // MAX2022



/*===========================================================================*\
 |	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

//IOResult HydraMtlLight::Save(ISave *isave) { 
//  IOResult res;
//	ULONG nb;
//
//  isave->BeginChunk(MTL_HDR_CHUNK);
//  res = MtlBase::Save(isave);
//  if (res!=IO_OK) return res;
//  isave->EndChunk();
//
//	isave->BeginChunk(HYDRA_CHUNK);
//  
//	isave->Write(&light_color_mult, sizeof(float), &nb);
//	isave->Write(&light_color, sizeof(Color), &nb);
//	isave->Write(&light_color_tint_on, sizeof(bool), &nb);
//
//	isave->EndChunk();
//
//
//  return IO_OK;
//  }	
//
//IOResult HydraMtlLight::Load(ILoad *iload) { 
//  IOResult res;
//  int id;
//	ULONG nb;
//  while (IO_OK==(res=iload->OpenChunk())) 
//	{
//    switch(id = iload->CurChunkID())  
//		{
//      case MTL_HDR_CHUNK:
//        res = MtlBase::Load(iload);
//        break;
//			case HYDRA_CHUNK:
//
//				res = iload->Read(&light_color_mult, sizeof(float), &nb);
//				res = iload->Read(&light_color, sizeof(Color), &nb);
//				res = iload->Read(&light_color_tint_on, sizeof(bool), &nb);
//
//				break;
//    }
//    iload->CloseChunk();
//    if (res!=IO_OK) 
//      return res;
//  }
//
//  return IO_OK;
//  }


/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/


RefTargetHandle HydraMtlLight::Clone(RemapDir &remap)
{
  HydraMtlLight *mnew = new HydraMtlLight(FALSE);
  *((MtlBase*)mnew) = *((MtlBase*)this);
  mnew->ReplaceReference(0, remap.CloneRef(pblock));

  mnew->light_color_mult = light_color_mult;
  mnew->light_color_tint_on = light_color_tint_on;
  mnew->light_color = light_color;

  mnew->ivalid.SetEmpty();

  mnew->subTex[0] = nullptr;
  if (subTex[0])
    mnew->ReplaceReference(0, remap.CloneRef(subTex[0]));

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}

void HydraMtlLight::NotifyChanged()
{
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void HydraMtlLight::Update(TimeValue t, Interval& valid)
{
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();

    pblock->GetValue(mtl_light_color_mult, t, light_color_mult, ivalid);
    pblock->GetValue(mtl_light_color, t, light_color, ivalid);
    pblock->GetValue(mtl_light_color_tint_on, t, light_color_tint_on, ivalid);


    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    if (subTex[0])
      subTex[0]->Update(t, ivalid);
  }

  valid &= ivalid;
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/

void HydraMtlLight::SetAmbient  (Color c, TimeValue t) noexcept {}
void HydraMtlLight::SetDiffuse  (Color c, TimeValue t) noexcept {}
void HydraMtlLight::SetSpecular (Color c, TimeValue t) noexcept {}
void HydraMtlLight::SetShininess(float v, TimeValue t) noexcept {}

Color HydraMtlLight::GetAmbient(int mtlNum, BOOL backFace)
{
  return { 0, 0, 0 };
}

Color HydraMtlLight::GetDiffuse(int mtlNum, BOOL backFace)
{
  return { 0, 0, 0 };
}

Color HydraMtlLight::GetSpecular(int mtlNum, BOOL backFace)
{
  return { 0, 0, 0 };
}

Color HydraMtlLight::GetSelfIllumColor(int mtlNum, BOOL backFace)
{
  Color res = light_color * light_color_mult;
  
  res.r /= (1.0f + res.r);
  res.g /= (1.0f + res.g);
  res.b /= (1.0f + res.b);

  return res;
}

float HydraMtlLight::GetXParency(int mtlNum, BOOL backFace)  noexcept
{
  return 0;
}

float HydraMtlLight::GetShininess(int mtlNum, BOOL backFace) noexcept
{
  return 0;
}

float HydraMtlLight::GetShinStr(int mtlNum, BOOL backFace) noexcept
{
  return 1.0f;
}

float HydraMtlLight::WireSize(int mtlNum, BOOL backFace) noexcept
{
  return 0.0f;
}


FPInterface * HydraMtlLightClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
  {
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  }
  else
  {
    return ClassDesc2::GetInterface(id);
  }
}

const MCHAR * HydraMtlLightClassDesc::GetEntryName() const
{
  return const_cast<HydraMtlLightClassDesc*>(this)->ClassName();

}

const MCHAR * HydraMtlLightClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Materials\\Hydra";
  return str;
}

Bitmap* HydraMtlLightClassDesc::GetEntryThumbnail() const noexcept
{
  return nullptr;
}
