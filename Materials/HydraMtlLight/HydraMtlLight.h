#pragma once
#include "resource.h"
#include "iparamm2.h"
#include <IMaterialBrowserEntryInfo.h>

/////////////////////////////////////////////////////////////////////////////////////

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define HydraMtlLight_CLASS_ID	Class_ID(0x332b3850, 0x1e7e7dce)

enum { COLOR_TEX_SLOT, NUM_SUBTEX }; // NUM_SUBTEX must be last in list!

constexpr int PBLOCK_REF      = NUM_SUBTEX;
constexpr int VERSION_CURRENT = 1;

using         hydraStr        = std::wstring;
using         hydraChar       = wchar_t;

/////////////////////////////////////////////////////////////////////////////////////

class HydraMtlLight: public Mtl
{
public:

  // Parameter block
  IParamBlock2* pblock;	
  Texmap*       subTex[NUM_SUBTEX];

  float         light_color_mult;
  Color         light_color;
  BOOL          light_color_tint_on;

  Interval		  ivalid;


  ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp)             override;
  void      Update(TimeValue t, Interval& valid)                        override;
  Interval  Validity(TimeValue t)                                       override;
  void      Reset()                                                     override;

  void      NotifyChanged();

  // From MtlBase and Mtl
  void     SetAmbient       (Color c, TimeValue t)              noexcept override;
  void     SetDiffuse       (Color c, TimeValue t)              noexcept override;
  void     SetSpecular      (Color c, TimeValue t)              noexcept override;
  void     SetShininess     (float v, TimeValue t)              noexcept override;
  Color    GetAmbient       (int mtlNum=0, BOOL backFace=FALSE)          override;
  Color    GetDiffuse       (int mtlNum=0, BOOL backFace=FALSE)          override;
  Color    GetSpecular      (int mtlNum=0, BOOL backFace=FALSE)          override;
  Color    GetSelfIllumColor(int mtlNum=0, BOOL backFace=FALSE)          override;
  float    GetXParency      (int mtlNum=0, BOOL backFace=FALSE) noexcept override;
  float    GetShininess     (int mtlNum=0, BOOL backFace=FALSE) noexcept override;
  float    GetShinStr       (int mtlNum=0, BOOL backFace=FALSE) noexcept override;
  float    WireSize         (int mtlNum=0, BOOL backFace=FALSE) noexcept override;


  // Shade and displacement calculation
  //
  void     Shade(ShadeContext& sc)                override;
  float    EvalDisplacement(ShadeContext& sc)     override;
  Interval DisplacementValidity(TimeValue t)      override;

  // SubMaterial access methods
  int      NumSubMtls()                  noexcept override { return 0; }
  Mtl*     GetSubMtl(int i)              noexcept override;
  void     SetSubMtl(int i, Mtl *m)               override;

#ifdef MAX2022
  MSTR     GetSubMtlSlotName(int i, bool localized) override;
  MSTR     GetSubMtlTVName  (int i, bool localized = true);
#else      
  TSTR     GetSubMtlSlotName(int i)                override;
  TSTR     GetSubMtlTVName(int i);
#endif // MAX2022


  // SubTexmap access methods
  int      NumSubTexmaps()                noexcept override { return NUM_SUBTEX; }
  Texmap*  GetSubTexmap(int i)            noexcept override;
  void     SetSubTexmap(int i, Texmap* m)          override;

#ifdef MAX2022
  MSTR     GetSubTexmapSlotName(int i, bool localized) override;
  MSTR     GetSubTexmapTVName  (int i, bool localized = true);
#else      
  TSTR     GetSubTexmapSlotName(int i)             override;
  TSTR     GetSubTexmapTVName(int i);
#endif // MAX2022


  BOOL     SetDlgThing(ParamDlg* dlg)     noexcept override;
  
  explicit HydraMtlLight(BOOL loading);
  // ~hydraMaterial(){ DeleteObject(hBitmap);};

   // For Paramblock2 is not necessary. It saves everything automatically.
   // Loading/Saving
   //IOResult Load(ILoad *iload);
   //IOResult Save(ISave *isave);

   //From Animatable
  Class_ID        ClassID()                      override { return HydraMtlLight_CLASS_ID; }
  SClass_ID       SuperClassID()        noexcept override { return MATERIAL_CLASS_ID; }

#ifdef MAX2022
  void            GetClassName(MSTR& s, bool localized = true) const override { s = GetString(IDS_CLASS_NAME); }
#else             
  void            GetClassName(TSTR& s)          override { s = GetString(IDS_CLASS_NAME); }
#endif // MAX2022


  RefTargetHandle Clone(RemapDir &remap)         override;

  int             NumSubs()             noexcept override { return 1; }
  Animatable*     SubAnim(int i)        noexcept override;

#ifdef MAX2022
  MSTR            SubAnimName(int i, bool localized) override;
#else
  TSTR            SubAnimName(int i)      override;
#endif // MAX2022

  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate) override;

  // Maintain the number or references here 
  int             NumRefs()             noexcept override { return 1 + NUM_SUBTEX; }
  RefTargetHandle GetReference(int i)   noexcept override;
  void            SetReference(int i, RefTargetHandle rtarg)  override;


  int	            NumParamBlocks()      noexcept override { return 1; }					// return number of ParamBlocks in this instance
  IParamBlock2*   GetParamBlock(int i)  noexcept override { return pblock; } // return i'th ParamBlock
  IParamBlock2*   GetParamBlockByID(BlockID id)  override { return (pblock->ID() == id) ? pblock : nullptr; } // return id'd ParamBlock

  void            DeleteThis()          noexcept override { delete this; }

protected:

  Color getShadeEmissiveColor    (ShadeContext &sc);
};



class HydraMtlLightClassDesc: public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  int          IsPublic()           noexcept override { return TRUE; }
  void*        Create(BOOL loading = FALSE)  override { return new HydraMtlLight(loading); }
  const TCHAR* ClassName()                   override { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  SClass_ID    SuperClassID()       noexcept override { return MATERIAL_CLASS_ID; }
  Class_ID     ClassID()                     override { return HydraMtlLight_CLASS_ID; }
  const TCHAR* Category()                    override { return GetString(IDS_CATEGORY); }
  const TCHAR* InternalName()       noexcept override { return _T("HydraMtlLight"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE    HInstance()          noexcept override { return hInstance; }					// returns owning module handle

  // For entry category
  FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  const MCHAR* GetEntryName()          const override;
  const MCHAR* GetEntryCategory()      const override;
  Bitmap*      GetEntryThumbnail()     const noexcept override;

#ifdef MAX2022
  const MCHAR* NonLocalizedClassName()       override { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
#endif // MAX2022

};


ClassDesc2* GetHydraMtlLightDesc();

enum { HydraMtlLight_params };


// Add enums for various parameters
enum
{
  mtl_light_color_mult,
  mtl_light_color_map,
  mtl_light_color,
  mtl_light_color_tint_on
};


