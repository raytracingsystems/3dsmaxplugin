#include "HydraMtlLight.h"
#include "../../HydraRenderer/ImportStructs.h"
#include <math.h> 
#include <fstream> 

inline float clamp(float u, float a, float b) { float r = max(a, u); return min(r, b); }


/*===========================================================================*\
 |	Actual shading takes place
\*===========================================================================*/

void HydraMtlLight::Shade(ShadeContext& sc) 
{
  if(gbufID) 
    sc.SetGBufferID(gbufID);
  
  sc.out.c = getShadeEmissiveColor(sc);
  //sc.out.t = getShadeTransparencyColor(sc);
  //sc.out.ior = ior;
  //sc.SetIOR(ior);
}

float HydraMtlLight::EvalDisplacement(ShadeContext& sc)
{
  return 0.0f;
}

Interval HydraMtlLight::DisplacementValidity(TimeValue t)
{
  return FOREVER;
}


Color HydraMtlLight::getShadeEmissiveColor(ShadeContext &sc)
{
  if(subTex[COLOR_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor(1,1,1);
    texColor = subTex[COLOR_TEX_SLOT]->EvalColor(sc);

    if(light_color_tint_on)
      return texColor * light_color * light_color_mult;
    else
      return texColor * light_color_mult;
  }
  else
    return light_color * light_color_mult;
}
