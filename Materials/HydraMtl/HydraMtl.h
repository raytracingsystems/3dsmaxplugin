#pragma once
////////////////////////////////////////////////////////////////////////////////////////////////
// DESCRIPTION: Hydra material.
// AUTHOR: RAY TRACING SYSTEMS.
////////////////////////////////////////////////////////////////////////////////////////////////
#include "IHydraMtl.h"


extern HINSTANCE hInstance;
extern TCHAR* GetString(int id);

constexpr float M_E          = 2.71828182F;

// The current implementation does not support more than one parameter block, because the reference to it is stored in
// scenes by the number following the enumeration of all texture references. So it developed historically and if to remake,
// the previous scenes will not open.
constexpr BlockID MAIN_PB_ID = 0;

constexpr int NSUBMTL         = 0; // number of sub-materials supported by this plugin 
constexpr int PBLOCK_REF      = NSUBMTL + NUM_SUBTEX;
constexpr int NUM_REFS        = 1 + PBLOCK_REF;
constexpr int VERSION_CURRENT = 1;
constexpr int MAX_SAMPLES     = 256;

const USHORT* getGgxTable();

////////////////////////////////////////////////////////////////////////////////////////////////


class HydraMtl: public IHydraMtl//, public MaxSDK::Graphics::IHLSLMaterialTranslator
{
public:

  // Parameter block
  IParamBlock2* pblock;
  Texmap*       subTexSlots[NUM_SUBTEX];

  Interval ivalid;

  int   m_specularBrdf, m_reflectBrdf, m_specGlossOrCos, m_reflectGlossOrCos, m_transpGlossOrCos, m_reflectExtrusion;  
  Color m_diffuseColor, m_specularColor, m_reflectColor, m_emissionColor, m_transpColor, m_transpDistColor, m_exitColor, m_translucColor;  
  BOOL  m_displacement, m_invertHeight, m_noIcRecords, m_diffuseTint, m_specularTint, m_emissionTint, m_transpTint, m_reflectTint,
    m_transpThin, m_translucTint, m_affectShadows, m_specularFresnel, m_reflectFresnel, m_lockSpecular, m_emissionCastGi, m_opacitySmooth;
  float m_specularRough, m_reflectRough, m_reflectCospow, m_specularCospow, m_transpIor, m_specularIor, m_reflectIor, m_transpCospower,
    m_transpDistMult, m_displaceHeight, m_diffuseMult, m_specularMult, m_emissionMult, m_transpMult, m_reflectMult, m_specularGloss, m_reflectGloss,
    m_transpGloss, m_reliefAmount, m_reliefSmooth, m_bumpSigma, m_translucMult, m_diffuseRoughness, m_reflectAnisotr, m_reflectAnisRotat;


  // Load bitmap for environment map
  static Interface*     ip;
  static bool           hasLoadStaticContent;
  static BitmapTex*     bmapTexEnv;
  static StdUVGen*      uvGenEnv;
  static Texmap*        texmapEnv;
  static HydraLogger    materialLog;

  ParamDlg*              CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) override;
  void                   Update(TimeValue t, Interval& valid)            override;
  Interval               Validity(TimeValue t)                           override;
  void                   Reset()                                         override;

  void NotifyChanged() { NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE); }


  // From MtlBase and Mtl
  void  SetAmbient         (Color c, TimeValue t)                   override;
  void  SetDiffuse         (Color c, TimeValue t)                   override;
  void  SetSpecular        (Color c, TimeValue t)                   override;
  void  SetShininess       (float v, TimeValue t)                   override;
  Color GetAmbient         (int mtlNum = 0, BOOL backFace = FALSE)  override;
  Color GetDiffuse         (int mtlNum = 0, BOOL backFace = FALSE)  override;
  Color GetSpecular        (int mtlNum = 0, BOOL backFace = FALSE)  override;
  Color GetSelfIllumColor  (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float GetXParency        (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float GetShininess       (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float GetShinStr         (int mtlNum = 0, BOOL backFace = FALSE)  override;
  float WireSize           (int mtlNum = 0, BOOL backFace = FALSE)  override;
  BOOL  GetTransparencyHint(TimeValue t, Interval& valid)           override;

  ULONG Requirements(int subMtlNum)                                 override;

  // Shade in viewport
  //void CreateHLSLMaterialHandle(MaxSDK::Graphics::GraphicFeatureLevel featureLevel);
  //virtual bool   UpdateHLSLMaterial(const TimeValue t, MaxSDK::Graphics::GraphicFeatureLevel featureLevel) override;
  //virtual const  MaxSDK::Graphics::HLSLMaterialHandle& GetHLSLMaterialHandle(MaxSDK::Graphics::GraphicFeatureLevel featureLevel) override;
  //virtual BaseInterface* GetInterface(Interface_ID iid) override
  //{
  //  if (IHLSL_MATERIAL_TRANSLATOR_INTERFACE_ID == iid) return static_cast<IHLSLMaterialTranslator*>(this);
  //  else                                               return MtlBase::GetInterface(iid);
  //}

  // Shade and m_displacement calculation
  //
  void     Shade           (ShadeContext& sc)       override;
  float    EvalDisplacement(ShadeContext& sc)       override;
  Interval DisplacementValidity(TimeValue t)        override;

  float    DiffuseOrenNayar         (const Point3 a_l, const Point3 a_v, const Point3 a_normal, const float a_roughness);
  void     PhongSampleAndEvalBRDF   (Color& a_resultReflects, ShadeContext& sc, const Point2 rnd2d, const float cosPower, Point3& a_rayDir, const bool a_refraction);
  void     BlinnSampleAndEvalBRDF   (Color& a_resultReflects, const Point2 rnd2d, const float cosPower, const Point3 a_normal, Point3& a_rayDir);
  void     GGXSampleAndEvalBRDF     (Color& a_resultReflects, const Point2 rnd2d, const float gloss, const Point3 a_normal, Point3& a_rayDir);
  void     GGXSample2AndEvalBRDF    (Color& a_resultReflects, const Point2 rnd2d, const float gloss, const Point3 a_normal, Point3& a_rayDir, const Color a_reflColor);
  void     BeckmannSampleAndEvalBRDF(Color& a_resultReflects, const Point2 rnd2d, const float gloss, const float anisotropy, const float anisRotation, const Point3 a_normal, const Point3 tbBasis[2], Point3& a_rayDir);
  void     TRGGXSampleAndEvalBRDF   (Color& a_resultReflects, const Point2 rnd2d, const float gloss, const float anisotropy, const float anisRotation, const Point3 a_normal, const Point3 tbBasis[2], Point3& a_rayDir);

  // SubMaterial access methods
  int      NumSubMtls()                    noexcept override { return NSUBMTL; }
  Mtl*     GetSubMtl(int i)                noexcept override;
  void     SetSubMtl(int i, Mtl *m)                 override;

#ifdef MAX2022
  MSTR     GetSubMtlSlotName(int i, bool localized) override;
  MSTR     GetSubMtlTVName  (int i, bool localized = true);
#else      
  TSTR     GetSubMtlSlotName(int i)                 override;
  TSTR     GetSubMtlTVName(int i);
#endif // MAX2022

  // SubTexmap access methods
  int      NumSubTexmaps()                 noexcept override { return NUM_SUBTEX; }
  Texmap*  GetSubTexmap(int i)             noexcept override;
  void     SetSubTexmap(int i, Texmap* m)           override;

#ifdef MAX2022
  MSTR     GetSubTexmapSlotName(int i, bool localized) override;
  MSTR     GetSubTexmapTVName  (int i, bool localized = true);
#else      
  TSTR     GetSubTexmapSlotName(int i)               override;
  TSTR     GetSubTexmapTVName(int i);
#endif // MAX2022


  BOOL     SetDlgThing(ParamDlg* dlg)       noexcept override { return FALSE; };

  explicit HydraMtl(BOOL loading);
  // ~HydraMtl(){ DeleteObject(hBitmap);};

   // Loading/Saving
  IOResult          Load(ILoad *iload)      override;
  IOResult          Save(ISave *isave)      override;

  // From Animatable
  Class_ID        ClassID()                 override { return HydraMtl_CLASS_ID; }
  SClass_ID       SuperClassID()   noexcept override { return MATERIAL_CLASS_ID; }

#ifdef MAX2022
  void            GetClassName(MSTR& s, bool localized = true) const override { s = GetString(IDS_CLASS_NAME); }
#else
  void            GetClassName(TSTR& s)     override { s = GetString(IDS_CLASS_NAME); }
#endif // MAX2022

  RefTargetHandle Clone(RemapDir &remap)    override;

  int             NumSubs()        noexcept override { return 1 + NSUBMTL; }
  Animatable*     SubAnim(int i)            override;

#ifdef MAX2022
  MSTR            SubAnimName(int i, bool localized=false) override;
#else
  TSTR            SubAnimName(int i)        override;
#endif // MAX2022

  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate) override;

  // Maintain the number or references here 
  int             NumRefs()            noexcept override { return NUM_REFS; }
  RefTargetHandle GetReference(int i)           override;
  void            SetReference(int i, RefTargetHandle rtarg) override;

  int	            NumParamBlocks()     noexcept override { return 1 + MAIN_PB_ID; }	  // return number of ParamBlocks in this instance
  IParamBlock2*   GetParamBlock(int i)          override;                             // return i'th ParamBlock
  IParamBlock2*   GetParamBlockByID(BlockID id) override;                             // return id'd ParamBlock

  void            DeleteThis()         noexcept override { delete this; }

  // from interface IHydraMtl 
  // Diffuse
  Texmap*         GetDiffuseTexmap()            override;
  float           GetDiffuseMult()              override;
  Color           GetDiffuseColor()             override;
  bool            GetDiffuseTint()              override;
  float           GetDiffuseRoughness()         override;
                                           
  // Reflectivity                          
  Texmap*         GetReflTexmap()               override;
  Texmap*         GetReflGlossTexmap()          override;
  Texmap*         GetReflAnisTexmap()           override;
  Texmap*         GetReflAnisRotatTexmap()      override;
  float           GetReflMult()                 override;
  Color           GetReflColor()                override;
  bool            GetReflTint()                 override;
  float           GetReflGloss()                override;
  int             GetReflBRDF()                 override;
  float           GetReflIor()                  override;
  bool            GetReflFresnel()              override;
  int             GetReflExtrus()               override;
  float           GetReflAnisotr()              override;
  float           GetReflAnisRotat()            override;
                                           
  // Transparency                          
  Texmap*         GetTranspTexmap()             override;
  Texmap*         GetTranspGlossTexmap()        override;
  float           GetTranspMult()               override;
  Color           GetTranspColor()              override;
  bool            GetTranspTint()               override;
  float           GetTranspGloss()              override;
  float           GetTranspIor()                override;
  float           GetTranspDistMult()           override;
  Color           GetTranspDistColor()          override;
  bool            GetTranspThin()               override;
                                           
  // Opacity/Special                       
  Texmap*        GetOpacityTexmap()             override;
  bool           GetAffectShadow()              override;
  bool           GetOpacitySmooth()             override;
  bool           HasOpacity()                   override;
                                           
  // Emission                              
  Texmap*        GetEmissionTexmap()            override;
  float          GetEmissionMult()              override;
  Color          GetEmissionColor()             override;
  bool           GetEmissionTint()              override;
  bool           GetEmissionCastGi()            override;
                                           
  // Translucency                          
  Texmap*        GetTranslucTexmap()            override;
  float          GetTranslucMult()              override;
  Color          GetTranslucColor()             override;
  bool           GetTranslucTint()              override;
                                           
  // Relief                                
  Texmap*        GetReliefTexmap()              override;
  float          GetReliefAmount()              override;
  float          GetReliefSmooth()              override;

private:
    //static MaxSDK::Graphics::TextureHandle m_hlslEnvirMap;
    //MaxSDK::Graphics::HLSLMaterialHandle m_HLSLMaterialHandle;
    //MaxSDK::Graphics::TextureHandle      m_hlslDiffuseMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslReflectMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslReflGlossMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslTranspMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslTranspGlossMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslOpacityMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslTranslucMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslEmissionMap;
    //MaxSDK::Graphics::TextureHandle      m_hlslReliefMap;        

protected:
  Color GetShadeDiffuseColor     (ShadeContext& sc);
  Color GetShadeReflectionColor  (ShadeContext& sc);
  Color GetShadeSpecularColor    (ShadeContext& sc);
  Color GetShadeTransparencyColor(ShadeContext& sc);
  Color GetShadeTranslucencyColor(ShadeContext& sc);
  Color GetShadeEmissiveColor    (ShadeContext& sc);
  float GetShadeOpacity          (ShadeContext& sc);
  float GetShadeSpecularCosPow   (ShadeContext& sc);
  float GetShadeReflectionCosPow (ShadeContext& sc);
  float GetShadeReflectionGloss  (ShadeContext& sc);
  float GetShadeTransparCosPow   (ShadeContext& sc);
  float GetShadeTransparGloss    (ShadeContext& sc);
  float GetShadeAnisotropy       (ShadeContext& sc);
  float GetShadeAnisRotation     (ShadeContext& sc);
};



class hydraMaterialClassDesc: public ClassDesc2, public IMaterialBrowserEntryInfo
{
public: 
  int           IsPublic()          noexcept override { return TRUE; }
  void*         Create(BOOL loading = FALSE) override { return new HydraMtl(loading); }
  const TCHAR *	ClassName()                  override { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  SClass_ID     SuperClassID()      noexcept override { return MATERIAL_CLASS_ID; }
  Class_ID      ClassID()                    override { return HydraMtl_CLASS_ID; }
  const TCHAR*  Category()                   override { return GetString(IDS_CATEGORY); }
  const TCHAR*  InternalName()      noexcept override { return _T("hydraMaterial"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE     HInstance()         noexcept override { return hInstance; }					  // returns owning module handle

  // For entry category
  FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  const MCHAR*  GetEntryName()         const override;
  const MCHAR*  GetEntryCategory()     const override;
  Bitmap*       GetEntryThumbnail()    const noexcept override;

#ifdef MAX2022
  const MCHAR* NonLocalizedClassName()       override { return GetString(IDS_CLASS_NAME); }
#endif // MAX2022

};



class HydraMtlDlgProc: public ParamMap2UserDlgProc
{
public:
  HydraMtl* hM        = nullptr;
  HWND      thishWnd  = nullptr;

  explicit HydraMtlDlgProc(HydraMtl *cb);

  INT_PTR  DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
  void     DropFileInMapSlot(int mapControlID, const std::filesystem::path& a_filename);
  void     DeleteThis() noexcept override { delete this; }
};


ClassDesc2* GetHydraMtlDesc();

enum { hydraMtl_params };

// Add enums for various parameters. Don't delete items, just add!
enum
{
  pb_spin,
  mtl_mat1,
  mtl_affect_shadow,
  mtl_diffuse_color,
  mtl_diffuse_map,
  mtl_diffuse_tint_on,
  mtl_diffuse_mult,
  mtl_specular_color,
  mtl_specular_map,
  mtl_specular_tint_on,
  mtl_specular_mult,
  mtl_specular_brdf,
  mtl_specular_roughness,
  mtl_specular_cospower,
  mtl_specular_ior,
  mtl_refl_color,
  mtl_refl_map,
  mtl_refl_tint_on,
  mtl_refl_mult,
  mtl_refl_brdf,
  mtl_refl_cospower,
  mtl_refl_roughness,
  mtl_refl_ior,
  mtl_emission_color,
  mtl_emission_map,
  mtl_emission_tint_on,
  mtl_emission_mult,
  mtl_transpar_color,
  mtl_transpar_map,
  mtl_transpar_tint_on,
  mtl_transpar_mult,
  mtl_transpar_thin_on,
  mtl_transpar_ior,
  mtl_transpar_cospower,
  mtl_transpar_dist_color,
  mtl_transpar_dist_mult,
  mtl_exit_color,
  mtl_displacement_on,
  mtl_displacement_height,
  mtl_normal_map,
  mtl_displacement_invert_height_on,
  mtl_no_ic_records,
  mtl_spec_gloss,
  mtl_spec_gl_map,
  mtl_spec_gloss_or_cos,
  mtl_spec_fresnel_on,
  mtl_refl_gloss,
  mtl_refl_gl_map,
  mtl_refl_gloss_or_cos,
  mtl_refl_fresnel_on,
  mtl_transpar_gloss,
  mtl_transpar_gl_map,
  mtl_transpar_gloss_or_cos,
  mtl_lock_specular,
  mtl_relief_amount,
  mtl_relief_smooth,
  mtl_bump_sigma,
  mtl_emission_cast_gi,
  mtl_opacity_map,
  mtl_opacity_smooth,
  mtl_transluc_color,
  mtl_transluc_mult,
  mtl_transluc_map,
  mtl_transluc_tint_on,
  mtl_refl_extrusion,
  mtl_roughness_mult,
  mtl_refl_anisotr,
  mtl_refl_rotat,
  mtl_refl_anisotr_map,
  mtl_refl_rotat_map
};

