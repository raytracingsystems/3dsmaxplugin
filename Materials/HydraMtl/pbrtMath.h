#pragma once
#include <random>
#include <math.h> 
#include "Max.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
#define INV_PI        0.31830988f
#define INV_TWOPI     0.15915494f
#define DEPSILON      1e-20F
#define DEPSILON2     1e-30F

static std::random_device rd;
static std::mt19937 engine(rd());
static const std::uniform_real_distribution<> distr(0.0f, 1.0f);


static void Lerp(Color& inData1, const Color& inData2, float coeff) // 0 - data1, 1 - data2
{
  if (coeff > 1.0F) coeff = 1.0F;
  else if (coeff < 0.0F) coeff = 0.0F;
  inData1 = inData1 + (inData2 - inData1) * coeff;
}

static void Lerp(Point3& inData1, const Point3& inData2, float coeff) // 0 - data1, 1 - data2
{
  if (coeff > 1.0F) coeff = 1.0F;
  else if (coeff < 0.0F) coeff = 0.0F;
  inData1 = inData1 + (inData2 - inData1) * coeff;
}

static void Lerp(float& inData1, const float& inData2, float coeff) // 0 - data1, 1 - data2
{
  if (coeff > 1.0F) coeff = 1.0F;
  else if (coeff < 0.0F) coeff = 0.0F;
  inData1 = inData1 + (inData2 - inData1) * coeff;
}



static float Luminance(const Point3& data)                      { return DotProd(data, Point3(0.2126F, 0.7152F, 0.0722F)); }
static void  Clamp1(float &a)                                   noexcept { a = fmin(a, 1.0F); }
static void  Clamp01(float &a)                                  noexcept { const float r = fmax(a, 0.0F); a = fmin(r, 1.0F); }
static float Clamp01_2(const float a)                           noexcept { const float r = fmax(a, 0.0F); return fmin(r, 1.0F); }
static float Clamp(const float u, const float a, const float b) noexcept { const float r = fmax(a, u); return fmin(r, b); }



struct float3
{
  float3() = default;
  float3(float a, float b, float c) : x(a), y(b), z(c) {}
  float3(const float* ptr) : x(ptr[0]), y(ptr[1]), z(ptr[0]) {}

  float x = 0.0F;
  float y = 0.0F;
  float z = 0.0F;
};


struct float4
{
  float4() = default;
  float4(float a, float b, float c, float d) : x(a), y(b), z(c), w(d) {}
  explicit float4(float a[4]) : x(a[0]), y(a[1]), z(a[2]), w(a[3]) {}

  float x = 0.0F;
  float y = 0.0F;
  float z = 0.0F;
  float w = 0.0F;
};

struct float4x4
{
  float4x4() { identity(); }

  float4x4(const float arr[16])
  {
    row[0] = float4(arr[0], arr[1], arr[2], arr[3]);
    row[1] = float4(arr[4], arr[5], arr[6], arr[7]);
    row[2] = float4(arr[8], arr[9], arr[10], arr[11]);
    row[3] = float4(arr[12], arr[13], arr[14], arr[15]);
  }

  void identity()
  {
    row[0] = float4(1, 0, 0, 0);
    row[1] = float4(0, 1, 0, 0);
    row[2] = float4(0, 0, 1, 0);
    row[3] = float4(0, 0, 0, 1);
  }

  float& M(int x, int y) { return ((float*)row)[y * 4 + x]; }
  float  M(int x, int y) const { return ((float*)row)[y * 4 + x]; }

  float* L() { return (float*)row; }
  const float* L() const { return (float*)row; }

  float4 row[4];
};

static float4x4 RotateAroundVector4x4(const Point3& v, float rotAngle)
{
  const float cos_t = cos(rotAngle);
  const float sin_t = sin(rotAngle);

  float4x4 m;

  m.row[0].x = (1.0F - cos_t)*v.x*v.x + cos_t;
  m.row[0].y = (1.0F - cos_t)*v.x*v.y - sin_t * v.z;
  m.row[0].z = (1.0F - cos_t)*v.x*v.z + sin_t * v.y;
  m.row[0].w = 1.0F;

  m.row[1].x = (1.0F - cos_t)*v.y*v.x + sin_t * v.z;
  m.row[1].y = (1.0F - cos_t)*v.y*v.y + cos_t;
  m.row[1].z = (1.0F - cos_t)*v.y*v.z - sin_t * v.x;
  m.row[1].w = 1.0F;

  m.row[2].x = (1.0F - cos_t)*v.x*v.z - sin_t * v.y;
  m.row[2].y = (1.0F - cos_t)*v.z*v.y + sin_t * v.x;
  m.row[2].z = (1.0F - cos_t)*v.z*v.z + cos_t;
  m.row[2].w = 1.0F;

  m.row[3].x = 0.0F;
  m.row[3].y = 0.0F;
  m.row[3].z = 0.0F;
  m.row[3].w = 1.0F;

  return m;
}

static Point3 mul3x3(const float4x4& m, const Point3& v)
{
  Point3 res;
  res.x = m.row[0].x*v.x + m.row[0].y*v.y + m.row[0].z*v.z;
  res.y = m.row[1].x*v.x + m.row[1].y*v.y + m.row[1].z*v.z;
  res.z = m.row[2].x*v.x + m.row[2].y*v.y + m.row[2].z*v.z;
  return res;
}

static void CoordinateSystem(const Point3& v1, Point3& v2, Point3& v3)
{
  float invLen = 1.0F;

  if (fabs(v1.x) > fabs(v1.y))
  {
    invLen     = 1.0F / sqrt(v1.x*v1.x + v1.z*v1.z);
    v2         = Point3(-v1.z * invLen, 0.0F, v1.x * invLen);
  }
  else
  {
    invLen     = 1.0F / sqrt(v1.y*v1.y + v1.z*v1.z);
    v2         = Point3(0.0F, v1.z * invLen, -v1.y * invLen);
  }

  v3           = CrossProd(v1, v2);
}

static inline float sigmoid(float x)
{
  return 1.0F / (1.0F + exp(-1.0F*x));
}


static float BilinearFrom2dTable(const USHORT* a_inData, float a_newPosX, float a_newPosY, const int a_width,
  const int a_height)
{
  // |------|------|
  // | dxy1 | dxy2 |
  // |------|------|
  // | dxy3 | dxy4 |
  // |------|------|

  a_newPosX = Clamp(a_newPosX, 0.0f, a_width - 1.0001f);
  a_newPosY = Clamp(a_newPosY, 0.0f, a_height - 1.0001f);

  const int floorY = floor(a_newPosY);
  const int floorX = floor(a_newPosX);

  const int dxy1 = floorY * a_width + floorX;
  const int dxy2 = dxy1 + 1;
  const int dxy3 = (floorY + 1) * a_width + floorX;
  const int dxy4 = dxy3 + 1;

  const float dx = a_newPosX - floorX;
  const float dy = a_newPosY - floorY;

  const float mult1 = (1.0f - dx) * (1.0f - dy);
  const float mult2 = dx * (1.0f - dy);
  const float mult3 = dy * (1.0f - dx);
  const float mult4 = dx * dy;

  if (floorY >= 0 && floorX >= 0 && floorY <= a_height - 2 && floorX <= a_width - 2)
    return a_inData[dxy1] * mult1 + a_inData[dxy2] * mult2 + a_inData[dxy3] * mult3 + a_inData[dxy4] * mult4;
  else
    return 1.0f;
}

static Color GetMultiscatteringFrom2dTable(const USHORT* msTable, const float roughness, const float dotNV,
  const int widthTable, const int heightTable, const Color color)
{
  const float x = dotNV * (float)(widthTable);
  const float y = roughness * (float)(heightTable);
  const float Ess = BilinearFrom2dTable(msTable, x, y, widthTable, heightTable) * (1.0f / 65535.0f);
  return 1.0f + color * (1.0f - Ess) / fmax(Ess, 1e-6f);
}

static inline float sigmoidShifted(float x)
{
  return sigmoid(20.0F*(x - 0.5F));
}

static float PreDivCosThetaFixMult(const float gloss, const float cosThetaOut)
{
  const float t = sigmoidShifted(2.0F*gloss);
  const float lerpVal = 1.0F + t * (1.0F / fmax(cosThetaOut, 1e-5F) - 1.0F); // mylerp { return u + t * (v - u); }
  return lerpVal;
}


static float SinPhiPBRT(const Point3& w, const float sintheta)
{
  if (sintheta == 0.0F) return 0.0F;
  else                  return Clamp(w.y / sintheta, -1.0F, 1.0F);
}

static float CosPhiPBRT(const Point3& w, const float sintheta)
{
  if (sintheta == 0.0F) return 1.0F;
  else                  return Clamp(w.x / sintheta, -1.0F, 1.0F);
}

static inline Point3 SphericalDirection(const float sintheta, const float costheta, const float phi)
{
  return Point3(sintheta * cos(phi), sintheta * sin(phi), costheta);
}

static inline bool SameHemisphere(const Point3& w, const Point3& wp) { return w.z * wp.z > 0.0F; }

static float TorranceSparrowG1(const Point3& wo, const Point3& wi, const Point3& wh) // in PBRT coord system
{
  const float NdotWh = fabs(wh.z);
  const float NdotWo = fabs(wo.z);
  const float NdotWi = fabs(wi.z);
  const float WOdotWh = fmax(fabs(DotProd(wo, wh)), DEPSILON);

  return fmin(1.0F, fmin((2.0F * NdotWh * NdotWo / WOdotWh), (2.0F * NdotWh * NdotWi / WOdotWh)));
}

static float FrCond(const float cosi, const float eta, const float k)
{
  const float tmp = (eta*eta + k * k) * cosi*cosi;
  const float Rparl2 = (tmp - (2.0F * eta * cosi) + 1.0F) / (tmp + (2.0F * eta * cosi) + 1.0F);
  const float tmp_f = eta * eta + k * k;
  const float Rperp2 = (tmp_f - (2.0F * eta * cosi) + cosi * cosi) / (tmp_f + (2.0F * eta * cosi) + cosi * cosi);
  return fabs(Rparl2 + Rperp2) / 2.0F;
}

static float TorranceSparrowGF1(const Point3& wo, const Point3& wi) // in PBRT coord system
{
  const float cosThetaO = fabs(wo.z); // inline float AbsCosTheta(const Vector &w) { return fabsf(w.z); }
  const float cosThetaI = fabs(wi.z); // inline float AbsCosTheta(const Vector &w) { return fabsf(w.z); }

  if (cosThetaI == 0.0F || cosThetaO == 0.0F)
    return 0.0F;

  Point3 wh = wi + wo;
  if (wh.x == 0.0F && wh.y == 0.0F && wh.z == 0.0F)
    return 0.0F;

  wh = wh.Normalize();

  //const float F = 1.0F;// Fresnel is not needed here, because it is used for the blend with diffusion.

  return fmin(TorranceSparrowG1(wo, wi, wh) /** F*/ / fmax(4.0F * cosThetaI * cosThetaO, DEPSILON), 250.0F);
}


// Random 

static inline float SimpleRand1D()
{  
  return (float)(distr(engine));
  //return (float)((rand() % RAND_MAX)) / (float)RAND_MAX;
}

static inline float CosThetaPBRT(const Point3& w) { return w.z; }
static inline float Cos2ThetaPBRT(const Point3& w) { return w.z * w.z; }
static inline float AbsCosThetaPBRT(const Point3& w) { return fabs(w.z); }
static inline float Sin2ThetaPBRT(const Point3& w) { return fmax(0.0F, 1.0F - Cos2ThetaPBRT(w)); }
 
static inline float SinThetaPBRT(const Point3& w) { return sqrt(Sin2ThetaPBRT(w)); }
static inline float TanThetaPBRT(const Point3& w) { return SinThetaPBRT(w) / CosThetaPBRT(w); }
static inline float Tan2ThetaPBRT(const Point3& w) { return Sin2ThetaPBRT(w) / Cos2ThetaPBRT(w); }

static float CosPhiPBRT(const Point3& w)
{
  const float sinTheta = SinThetaPBRT(w);
  return (sinTheta == 0.0F) ? 1.0F : Clamp(w.x / sinTheta, -1.0F, 1.0F);
}

static float SinPhiPBRT(const Point3& w)
{
  const float sinTheta = SinThetaPBRT(w);
  return (sinTheta == 0.0F) ? 0.0F : Clamp(w.y / sinTheta, -1.0F, 1.0F);
}

static inline float Cos2PhiPBRT(const Point3& w) { return CosPhiPBRT(w) * CosPhiPBRT(w); }
static inline float Sin2PhiPBRT(const Point3& w) { return SinPhiPBRT(w) * SinPhiPBRT(w); }

static float ErfPBRT(float x)
{
  // constants
  const float a1 =  0.254829592F;
  const float a2 = -0.284496736F;
  const float a3 =  1.421413741F;
  const float a4 = -1.453152027F;
  const float a5 =  1.061405429F;
  const float p  =  0.327591100F;
  // Save the sign of x
  int sign = 1;
  if (x < 0)
    sign = -1;
  x = fabs(x);
  // A&S formula 7.1.26
  const float t = 1.0F / (1.0F + p * x);
  const float y = 1.0F - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * exp(-x * x);
  return sign * y;
}

static float ErfInvPBRT(float x)
{
  float w, p;
  x = Clamp(x, -0.99999f, 0.99999f);
  w = -log((1.0F - x) * (1.0F + x));
  if (w < 5.0f)
  {
    w = w - 2.5f;
    p = 2.81022636e-08f;
    p = 3.43273939e-07f + p * w;
    p = -3.5233877e-06f + p * w;
    p = -4.39150654e-06f + p * w;
    p = 0.00021858087f + p * w;
    p = -0.00125372503f + p * w;
    p = -0.00417768164f + p * w;
    p = 0.246640727f + p * w;
    p = 1.50140941f + p * w;
  }
  else
  {
    w = sqrt(w) - 3.0f;
    p = -0.000200214257f;
    p = 0.000100950558f + p * w;
    p = 0.00134934322f + p * w;
    p = -0.00367342844f + p * w;
    p = 0.00573950773f + p * w;
    p = -0.0076224613f + p * w;
    p = 0.00943887047f + p * w;
    p = 1.00167406f + p * w;
    p = 2.83297682f + p * w;
  }
  return p * x;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//   Beckmann
/////////////////////////////////////////////////////////////////////////////////////////////////////

static void BeckmannSample11(float cosThetaI, float U1, float U2, float *slope_x, float *slope_y)
{
  /* Special case (normal incidence) */
  if (cosThetaI > 0.9999f)
  {
    const float r = sqrt(-log(1.0F - U1));
    const float sinPhi = sin(TWOPI * U2);
    const float cosPhi = cos(TWOPI * U2);
    *slope_x = r * cosPhi;
    *slope_y = r * sinPhi;
    return;
  }

  // The original inversion routine from the paper contained
  // discontinuities, which causes issues for QMC integration
  // and techniques like Kelemen-style MLT. The following code
  // performs a numerical inversion with better behavior 
  //
  const float sinThetaI = sqrt(fmax(0.0F, 1.0F - cosThetaI * cosThetaI));
  const float tanThetaI = sinThetaI / cosThetaI;
  const float cotThetaI = 1.0F / tanThetaI;

  // Search interval -- everything is parameterized
  // in the Erf() domain 
  //
  float a = -1, c = ErfPBRT(cotThetaI);
  const float sample_x = fmax(U1, 1e-6F);

  // Start with a good initial guess 
  // Float b = (1-sample_x) * a + sample_x * c;
  // We can do better (inverse of an approximation computed in Mathematica)
  // 
  const float thetaI = acos(cosThetaI);
  const float fit = 1 + thetaI * (-0.876f + thetaI * (0.4265f - 0.0594f * thetaI));
  float b = c - (1 + c) * pow(1 - sample_x, fit);

  // Normalization factor for the CDF 
  //
  const float SQRT_PI_INV = 1.f / sqrt(PI);
  float normalization = 1.0F / (1.0F + c + SQRT_PI_INV * tanThetaI * exp(-cotThetaI * cotThetaI));

  int it = 0;
  while (++it < 10)
  {
    // Bisection criterion -- the oddly-looking
    // Boolean expression are intentional to check
    // for NaNs at little additional cost 
    //
    if (!(b >= a && b <= c))
      b = 0.5f * (a + c);
    // Evaluate the CDF and its derivative
    //   (i.e. the density function) 
    const float invErf = ErfInvPBRT(b);
    const float value = normalization * (1.0F + b + SQRT_PI_INV * tanThetaI * exp(-invErf * invErf)) - sample_x;
    const float derivative = normalization * (1 - invErf * tanThetaI);
    if (fabs(value) < 1e-5f)
      break;
    /* Update bisection intervals */
    if (value > 0)
      c = b;
    else
      a = b;
    b -= value / derivative;
  }

  // Now convert back into a slope value 
  *slope_x = ErfInvPBRT(b);
  // Simulate Y component 
  *slope_y = ErfInvPBRT(2.0F * fmax(U2, 1e-6F) - 1.0F);

  //CHECK(!std::isinf(*slope_x));
  //CHECK(!std::isnan(*slope_x));
  //CHECK(!std::isinf(*slope_y));
  //CHECK(!std::isnan(*slope_y));
}

static Point3 BeckmannSample(const Point3& wi, float alpha_x, float alpha_y, float U1, float U2)
{
  // 1. stretch wi
  const Point3 wiStretched = Normalize(Point3(alpha_x * wi.x, alpha_y * wi.y, wi.z));

  // 2. simulate P22_{wi}(x_slope, y_slope, 1, 1)
  float slope_x, slope_y;
  BeckmannSample11(CosThetaPBRT(wiStretched), U1, U2,
    &slope_x, &slope_y);

  // 3. rotate
  float tmp = CosPhiPBRT(wiStretched) * slope_x - SinPhiPBRT(wiStretched) * slope_y;
  slope_y = SinPhiPBRT(wiStretched) * slope_x + CosPhiPBRT(wiStretched) * slope_y;
  slope_x = tmp;

  // 4. unstretch
  slope_x = alpha_x * slope_x;
  slope_y = alpha_y * slope_y;

  // 5. compute normal
  return Normalize(Point3(-slope_x, -slope_y, 1.f));
}

static float BeckmannDistributionD(const Point3& wh, float alphax, float alphay)
{
  float tan2Theta = Tan2ThetaPBRT(wh);
  if (!isfinite(tan2Theta))
    return 0.0F;
  float cos4Theta = Cos2ThetaPBRT(wh) * Cos2ThetaPBRT(wh);
  return exp(-tan2Theta * (Cos2PhiPBRT(wh) / (alphax * alphax) + Sin2PhiPBRT(wh) / (alphay * alphay))) / (PI * alphax * alphay * cos4Theta);
}

static float BeckmannDistributionLambda(const Point3& w, float alphax, float alphay)
{
  const float absTanTheta = fabs(TanThetaPBRT(w));
  if (!isfinite(absTanTheta))
    return 0.0F;
  // Compute _alpha_ for direction _w_
  const float alpha = sqrt(Cos2PhiPBRT(w) * alphax * alphax + Sin2PhiPBRT(w) * alphay * alphay);
  const float a = 1.0F / (alpha * absTanTheta);
  if (a >= 1.6F)
    return 0.0F;
  return (1.0F - 1.259F * a + 0.396F * a * a) / (3.535F * a + 2.181F * a * a);
}

static Point3 BeckmannDistributionSampleWH(const Point3& wo, const Point2& u, float alphax, float alphay)
{
  Point3 wh;
  const bool flip = (wo.z < 0.0F);
  wh = BeckmannSample(flip ? (-1.0F) * wo : wo, alphax, alphay, u.x, u.y);
  if (flip)
    wh = (-1.0F)*wh;
  return wh;
}

static inline float BeckmannG1(const Point3& w, float alphax, float alphay)
{
  return 1.0F / (1.0F + BeckmannDistributionLambda(w, alphax, alphay));
}

static inline float BeckmannDistributionPdf(const Point3& wo, const Point3& wh, float alphax, float alphay)
{
  return BeckmannDistributionD(wh, alphax, alphay) * BeckmannG1(wo, alphax, alphay) / (4.0F * AbsCosThetaPBRT(wo));
}

static inline float BeckmannG(const Point3& wo, const Point3& wi, float alphax, float alphay)
{
  return 1.0F / (1.0F + BeckmannDistributionLambda(wo, alphax, alphay) + BeckmannDistributionLambda(wi, alphax, alphay));
}

static float BeckmannBRDF_PBRT(const Point3& wo, const Point3& wi, float alphax, float alphay)
{
  const float cosThetaO = AbsCosThetaPBRT(wo);
  const float cosThetaI = AbsCosThetaPBRT(wi);

  Point3 wh = wi + wo;

  // Handle degenerate cases for microfacet reflection
  if (cosThetaI <= 1e-6F || cosThetaO <= 1e-6F)
    return 0.0F;

  if (fabs(wh.x) <= 1e-6F && fabs(wh.y) <= 1e-6F && fabs(wh.z) <= 1e-6F)
    return 0.0F;

  wh = Normalize(wh);
  const float F = 1.0F; // FrCond(dot(wi, wh), 5.0f, 1.25f); // fresnel is used inside FresnelBlend

  return BeckmannDistributionD(wh, alphax, alphay) * BeckmannG(wo, wi, alphax, alphay) * F / fmax(4.0f * cosThetaI * cosThetaO, DEPSILON);
}

static float BeckmannRoughnessToAlpha(float roughness)
{
  const float x = log(fmax(roughness, 1.0e-4f));
  return 1.62142f + 0.819955f * x + 0.1734f * x * x + 0.0171201f * x * x * x + 0.000640711f * x * x * x * x;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//   TRGGX
/////////////////////////////////////////////////////////////////////////////////////////////////////

static float TrowbridgeReitzDistributionD(const Point3& wh, float alphax, float alphay)
{
  float tan2Theta = Tan2ThetaPBRT(wh);
  if (!isfinite(tan2Theta))
    return 0.0F;
  const float cos4Theta = Cos2ThetaPBRT(wh) * Cos2ThetaPBRT(wh);
  const float e = (Cos2PhiPBRT(wh) / (alphax * alphax) + Sin2PhiPBRT(wh) / (alphay * alphay)) * tan2Theta;
  return 1.0F / (PI * alphax * alphay * cos4Theta * (1.0F + e) * (1.0F + e));
}

static float TrowbridgeReitzDistributionLambda(const Point3& w, float alphax, float alphay)
{
  const float absTanTheta = fabs(TanThetaPBRT(w));
  if (!isfinite(absTanTheta))
    return 0.0F;
  // Compute _alpha_ for direction _w_
  const float alpha = sqrt(Cos2PhiPBRT(w) * alphax * alphax + Sin2PhiPBRT(w) * alphay * alphay);
  const float alpha2Tan2Theta = (alpha * absTanTheta) * (alpha * absTanTheta);
  return (-1.0F + sqrt(1.f + alpha2Tan2Theta)) / 2.0F;
}


static void TrowbridgeReitzSample11(float cosTheta, float U1, float U2, float *slope_x, float *slope_y)
{
  // special case (normal incidence)
  if (cosTheta > 0.9999f)
  {
    const float r = sqrt(U1 / (1.0F - U1));
    const float phi = 6.28318530718f * U2;
    *slope_x = r * cos(phi);
    *slope_y = r * sin(phi);
    return;
  }

  const float sinTheta = sqrt(fmax(0.0F, 1.0F - cosTheta * cosTheta));
  const float tanTheta = sinTheta / cosTheta;
  const float a = 1.0F / tanTheta;
  const float G1 = 2.0F / (1.0F + sqrt(1.f + 1.f / (a * a)));

  // sample slope_x
  const float A = 2.0F * U1 / G1 - 1.0F;
  float tmp = 1.f / (A * A - 1.f);
  if (tmp > 1e10f)
    tmp = 1e10f;

  const float B = tanTheta;
  const float D = sqrt(fmax(B * B * tmp * tmp - (A * A - B * B) * tmp, 0.0F));
  const float slope_x_1 = B * tmp - D;
  const float slope_x_2 = B * tmp + D;

  *slope_x = (A < 0.0F || slope_x_2 > 1.f / tanTheta) ? slope_x_1 : slope_x_2;

  // sample slope_y
  float S;
  if (U2 > 0.5f)
  {
    S = 1.f;
    U2 = 2.f * (U2 - .5f);
  }
  else
  {
    S = -1.f;
    U2 = 2.f * (.5f - U2);
  }
  const float z = (U2 * (U2 * (U2 * 0.27385f - 0.73369f) + 0.46341f)) /
    (U2 * (U2 * (U2 * 0.093073f + 0.309420f) - 1.000000f) + 0.597999f);

  *slope_y = S * z * sqrt(1.f + *slope_x * *slope_x);

  //CHECK(!std::isinf(*slope_y));
  //CHECK(!std::isnan(*slope_y));
}

static Point3 TrowbridgeReitzSample(const Point3& wi, float alpha_x, float alpha_y, float U1, float U2)
{
  // 1. stretch wi
  const Point3 wiStretched = Normalize(Point3(alpha_x * wi.x, alpha_y * wi.y, wi.z));

  // 2. simulate P22_{wi}(x_slope, y_slope, 1, 1)
  float slope_x, slope_y;
  TrowbridgeReitzSample11(CosThetaPBRT(wiStretched), U1, U2,
    &slope_x, &slope_y);

  // 3. rotate
  float tmp = CosPhiPBRT(wiStretched) * slope_x - SinPhiPBRT(wiStretched) * slope_y;
  slope_y = SinPhiPBRT(wiStretched) * slope_x + CosPhiPBRT(wiStretched) * slope_y;
  slope_x = tmp;

  // 4. unstretch
  slope_x = alpha_x * slope_x;
  slope_y = alpha_y * slope_y;

  // 5. compute normal
  return Normalize(Point3(-slope_x, -slope_y, 1.0F));
}

static Point3 TrowbridgeReitzDistributionSampleWH(const Point3& wo, const Point2& u, float alphax, float alphay)
{
  Point3 wh;
  bool flip = wo.z < 0.0F;
  wh = TrowbridgeReitzSample(flip ? -wo : wo, alphax, alphay, u.x, u.y);
  if (flip)
    wh = wh * (-1.0F);
  return wh;
}

static inline float TrowbridgeReitzG1(const Point3& w, float alphax, float alphay)
{
  return 1.0F / (1.0F + TrowbridgeReitzDistributionLambda(w, alphax, alphay));
}

static inline float TrowbridgeReitzDistributionPdf(const Point3& wo, const Point3& wh, float alphax, float alphay)
{
  return TrowbridgeReitzDistributionD(wh, alphax, alphay) * TrowbridgeReitzG1(wo, alphax, alphay) / (4.0F*AbsCosThetaPBRT(wo));
}

static float TrowbridgeReitzRoughnessToAlpha(float roughness)
{
  const float x = log(fmax(roughness, 1e-4f));
  return 1.62142f + 0.819955f * x + 0.1734f * x * x + 0.0171201f * x * x * x + 0.000640711f * x * x * x * x;
}

static inline float TrowbridgeReitzG(const Point3& wo, const Point3& wi, float alphax, float alphay)
{
  return 1.0F / (1.0F + TrowbridgeReitzDistributionLambda(wo, alphax, alphay) + TrowbridgeReitzDistributionLambda(wi, alphax, alphay));
}

static float TrowbridgeReitzBRDF_PBRT(const Point3& wo, const Point3& wi, float alphax, float alphay)
{
  const float cosThetaO = AbsCosThetaPBRT(wo);
  const float cosThetaI = AbsCosThetaPBRT(wi);

  Point3 wh = wi + wo;

  // Handle degenerate cases for microfacet reflection
  if (cosThetaI <= 1e-6F || cosThetaO <= 1e-6F)
    return 0.0F;

  if (fabs(wh.x) <= 1e-6F && fabs(wh.y) <= 1e-6F && fabs(wh.z) <= 1e-6F)
    return 0.0F;

  wh = Normalize(wh);
  const float F = 1.0F; //FrCond(dot(wi, wh), 5.0f, 1.25f); //Fresnel is used inside FresnelBlend

  return TrowbridgeReitzDistributionD(wh, alphax, alphay) * TrowbridgeReitzG(wo, wi, alphax, alphay) * F / fmax(4.0f * cosThetaI * cosThetaO, DEPSILON);
}

static Point3 CreateTangent(const Point3& normal, const Point3& ray)
{
  Point3 tangent = CrossProd(normal, ray);
  if (tangent == Point3(0.0F, 0.0F, 0.0F))
  {
    Point3 a = CrossProd(normal, Point3(0.0F, 1.0F, 0.0F));
    Point3 b = CrossProd(normal, Point3(0.0F, 0.0F, 1.0F));
    if (Length(a) > Length(b))
      tangent = a;
    else
      tangent = b;
  }
  return Normalize(tangent);
}

