#pragma once
////////////////////////////////////////////////////////////////////////////////////////////////
// DESCRIPTION: Hydra material
// AUTHOR: RAY TRACING SYSTEMS
////////////////////////////////////////////////////////////////////////////////////////////////
#include "iparamm2.h"
#include "resource.h"
#include "iparamm2.h"
#include <string>
#include "stdmat.h"
#include <IMaterialBrowserEntryInfo.h>
#include <Graphics/IHLSLMaterialTranslator.h>
#include <Graphics/RenderItemHandle.h>

#include "../../HydraRenderer/HydraLogger.h"

////////////////////////////////////////////////////////////////////////////////////////////////

#define HydraMtl_CLASS_ID	Class_ID(0xa3f0e60d, 0x941ba693)

enum extrusion { EXTRUSION_STRONG, EXTRUSION_LUM };
enum brdf { BRDF_PHONG, BRDF_TORRSPARR, BRDF_BECKMANN, BRDF_GGX, BRDF_TRGGX };
enum texSlot
{
  DIFFUSE_TEX_SLOT, SPECULAR_TEX_SLOT, REFLECT_TEX_SLOT, EMISSION_TEX_SLOT, TRANSP_TEX_SLOT, RELIEF_TEX_SLOT,
  SPEC_GLOSS_TEX_SLOT, REFL_GLOSS_TEX_SLOT, TRANSP_GLOSS_TEX_SLOT, OPACITY_TEX_SLOT, TRANSLUCENCY_TEX_SLOT, REFL_ANISOTR_TEX_SLOT,
  REFL_ROTATION_TEX_SLOT, NUM_SUBTEX
}; // NSUBTEX should be last in the list!


class IHydraMtl : public Mtl // interface for HydraMtl compatible materials.
{
public:
  virtual ~IHydraMtl() = default;

  // Export to Hydra render

  // Diffuse
  virtual Texmap* GetDiffuseTexmap()        = 0;
  virtual float   GetDiffuseMult()          = 0;
  virtual Color   GetDiffuseColor()         = 0;
  virtual bool    GetDiffuseTint()          = 0;
  virtual float   GetDiffuseRoughness()     = 0;

  // Reflectivity
  virtual Texmap* GetReflTexmap()           = 0;
  virtual Texmap* GetReflGlossTexmap()      = 0;
  virtual Texmap* GetReflAnisTexmap()       = 0;
  virtual Texmap* GetReflAnisRotatTexmap()  = 0;
  virtual float   GetReflMult()             = 0; 
  virtual Color   GetReflColor()            = 0;
  virtual bool    GetReflTint()             = 0;
  virtual float   GetReflGloss()            = 0; 
  virtual int     GetReflBRDF()             = 0;  
  virtual float   GetReflIor()              = 0;
  virtual bool    GetReflFresnel()          = 0;
  virtual int     GetReflExtrus()           = 0;  
  virtual float   GetReflAnisotr()          = 0;
  virtual float   GetReflAnisRotat()        = 0;

  // Transparency
  virtual Texmap* GetTranspTexmap()         = 0;
  virtual Texmap* GetTranspGlossTexmap()    = 0;
  virtual float   GetTranspMult()           = 0;
  virtual Color   GetTranspColor()          = 0; 
  virtual bool    GetTranspTint()           = 0;
  virtual float   GetTranspGloss()          = 0;
  virtual float   GetTranspIor()            = 0;
  virtual float   GetTranspDistMult()       = 0;
  virtual Color   GetTranspDistColor()      = 0;
  virtual bool    GetTranspThin()           = 0;

  // Opacity/Special
  virtual Texmap* GetOpacityTexmap()        = 0;
  virtual bool    GetAffectShadow()         = 0;
  virtual bool    GetOpacitySmooth()        = 0;
  virtual bool    HasOpacity()              = 0;

  // Emission
  virtual Texmap* GetEmissionTexmap()       = 0;
  virtual float   GetEmissionMult()         = 0;
  virtual Color   GetEmissionColor()        = 0;
  virtual bool    GetEmissionTint()         = 0;
  virtual bool    GetEmissionCastGi()       = 0;

  // Translucency
  virtual Texmap* GetTranslucTexmap()       = 0;
  virtual float   GetTranslucMult()         = 0;
  virtual Color   GetTranslucColor()        = 0;
  virtual bool    GetTranslucTint()         = 0;

  // Relief
  virtual Texmap* GetReliefTexmap()         = 0;  
  virtual float   GetReliefAmount()         = 0;
  virtual float   GetReliefSmooth()         = 0;
};
