#include "HydraMtl.h"
#include "3dsmaxport.h"
#include "pbrtMath.h"

////////////////////////////////////////////////////////////////////////////////////////////////

static hydraMaterialClassDesc hydraMtlDesc;

ClassDesc2* GetHydraMtlDesc()
{
  return &hydraMtlDesc;
}

static ParamBlockDesc2 hydraMtl_param_blk(hydraMtl_params, _T("parameters"), 0, &hydraMtlDesc,
  P_AUTO_CONSTRUCT + P_AUTO_UI, PBLOCK_REF,
  //rollout
  IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
  mtl_affect_shadow, _T("mtl_affect_shadow"), TYPE_BOOL, 0, IDS_AFFECT_SHADOW,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_AFFECT_SHADOW,
    p_end,
  mtl_diffuse_color, _T("mtl_diffuse_color"), TYPE_RGBA, P_ANIMATABLE, IDS_DIFFUSE_COLOR,
    p_default, Color(0.8, 0.8, 0.8),
    p_ui, TYPE_COLORSWATCH, IDC_DIFFUSE_COLOR,
    p_end,
  mtl_diffuse_map, _T("mtl_diffuse_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_DIFFUSE_MAP,
    p_refno, DIFFUSE_TEX_SLOT,
    p_subtexno, DIFFUSE_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_DIFFUSE_MAP,
    p_end,
  mtl_diffuse_tint_on, _T("mtl_diffuse_mult_on"), TYPE_BOOL, 0, IDS_DIFFUSE_MULT,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_DIFFUSE_MULT_ON,
    p_end,
  mtl_diffuse_mult, _T("mtl_diffuse_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_DIFFUSE_MULT,
    p_default, 1.0F,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_DIFFUSE_MULT, IDC_DIFFUSE_MULT_SPINNER, 0.1F,
    p_end,
  mtl_roughness_mult, _T("mtl_roughness_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_DIFFUSE_ROUGHNESS,
    p_default, 0.0F,
    p_range, 0.0F, 1.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_DIFFUSE_ROUGHNESS, IDC_DIFFUSE_ROUGHNESS_SPINNER, 0.1F,
    p_end,
  mtl_specular_color, _T("mtl_specular_color"), TYPE_RGBA, P_ANIMATABLE, IDS_SPECULAR_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_SPECULAR_COLOR,
    p_end,
  mtl_specular_map, _T("mtl_specular_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_SPECULAR_MAP, // deprecated1
    p_refno, SPECULAR_TEX_SLOT,
    p_subtexno, SPECULAR_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_SPECULAR_MAP,
    p_end,
  mtl_specular_tint_on, _T("mtl_specular_mult_on"), TYPE_BOOL, 0, IDS_SPECULAR_MULT,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_SPECULAR_MULT_ON,
    p_end,
  mtl_specular_mult, _T("mtl_specular_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_MULT,
    p_default, 0.0F,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_MULT, IDC_SPECULAR_MULT_SPINNER, 0.1F,
    p_end,
  mtl_specular_brdf, _T("mtl_specular_brdf"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_SPECULAR_BRDF,
    p_default, 3,
    p_ui, TYPE_INT_COMBOBOX, IDC_SPECULAR_BRDF, 5, IDS_BRDF1, IDS_BRDF2, IDS_BRDF3, IDS_BRDF4, IDS_BRDF5,
    p_tooltip, IDS_SPECULAR_BRDF,
    p_end,
  mtl_specular_roughness, _T("mtl_specular_roughness"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_ROUGHNESS,
    p_default, 1.0F,
    p_range, 0.0F, 1.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_ROUGHNESS, IDC_SPECULAR_ROUGHNESS_SPINNER, 0.1F,
    p_end,
  mtl_specular_cospower, _T("mtl_specular_cospower"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_COS,
    p_default, 256.0F,
    p_range, 1.0F, 1000000.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_COS, IDC_SPECULAR_COS_SPINNER, 0.1F,
    p_end,
  mtl_specular_ior, _T("mtl_specular_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPECULAR_IOR,
    p_default, 1.0F,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_IOR, IDC_SPECULAR_IOR_SPINNER, 0.1F,
    p_end,
  mtl_refl_color, _T("mtl_reflect_color"), TYPE_RGBA, P_ANIMATABLE, IDS_REFLECT_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_REFLECT_COLOR,
    p_end,
  mtl_refl_map, _T("mtl_reflect_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_REFLECT_MAP,
    p_refno, REFLECT_TEX_SLOT,
    p_subtexno, REFLECT_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_REFLECT_MAP,
    p_end,
  mtl_refl_tint_on, _T("mtl_reflect_mult_on"), TYPE_BOOL, 0, IDS_REFLECT_MULT,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_REFLECT_MULT_ON,
    p_end,
  mtl_refl_mult, _T("mtl_reflect_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_MULT,
    p_default, 0.0F,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_MULT, IDC_REFLECT_MULT_SPINNER, 0.1F,
    p_end,
  mtl_refl_brdf, _T("mtl_reflect_brdf"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_REFLECT_BRDF,
    p_default, 3,
    p_ui, TYPE_INT_COMBOBOX, IDC_REFLECT_BRDF, 5, IDS_BRDF1, IDS_BRDF2, IDS_BRDF3, IDS_BRDF4, IDS_BRDF5,
    p_tooltip, IDS_REFLECT_BRDF,
    p_end,
  mtl_refl_cospower, _T("mtl_reflect_cospower"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_COS,
    p_default, 256.0F,
    p_range, 1.0F, 1000000.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_COS, IDC_REFLECT_COS_SPINNER, 0.1F,
    p_end,
  mtl_refl_roughness, _T("mtl_reflect_roughness"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_ROUGHNESS,
    p_default, 1.0F,
    p_range, 0.0F, 1.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_ROUGHNESS, IDC_REFLECT_ROUGHNESS_SPINNER, 0.1F,
    p_end,
  mtl_refl_ior, _T("mtl_reflect_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFLECT_IOR,
    p_default, 1.5f,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFLECT_IOR, IDC_REFLECT_IOR_SPINNER, 0.1F,
    p_end,
  mtl_emission_color, _T("mtl_emission_color"), TYPE_RGBA, P_ANIMATABLE, IDS_EMISSION_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_EMISSION_COLOR,
    p_end,
  mtl_emission_map, _T("mtl_emission_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_EMISSION_MAP,
    p_refno, EMISSION_TEX_SLOT,
    p_subtexno, EMISSION_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_EMISSION_MAP,
    p_end,
  mtl_emission_tint_on, _T("mtl_emission_mult_on"), TYPE_BOOL, 0, IDS_EMISSION_MULT,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_EMISSION_MULT_ON,
    p_end,
  mtl_emission_mult, _T("mtl_emission_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_EMISSION_MULT,
    p_default, 0.0F,
    p_range, 0.0F, 1000.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_EMISSION_MULT, IDC_EMISSION_MULT_SPINNER, 0.1F,
    p_end,
  mtl_transpar_color, _T("mtl_transparency_color"), TYPE_RGBA, P_ANIMATABLE, IDS_TRANSP_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_TRANSP_COLOR,
    p_end,
  mtl_transpar_map, _T("mtl_transparency_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_TRANSP_MAP,
    p_refno, TRANSP_TEX_SLOT,
    p_subtexno, TRANSP_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_TRANSP_MAP,
    p_end,
  mtl_transpar_tint_on, _T("mtl_transparency_mult_on"), TYPE_BOOL, 0, IDS_TRANSP_MULT,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_TRANSP_MULT_ON,
    p_end,
  mtl_transpar_mult, _T("mtl_transparency_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSP_MULT,
    p_default, 0.0F,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSP_MULT, IDC_TRANSP_MULT_SPINNER, 0.1F,
    p_end,
  mtl_transpar_ior, _T("mtl_ior"), TYPE_FLOAT, P_ANIMATABLE, IDS_IOR,
    p_default, 1.5f,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_IOR, IDC_IOR_SPINNER, 0.1F,
    p_end,
  mtl_transpar_cospower, _T("mtl_transparency_cospower"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSP_COS,
    p_default, 256.0F,
    p_range, 1.0F, 1000000.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSP_COS, IDC_TRANSP_COS_SPINNER, 0.1F,
    p_end,
  mtl_transpar_thin_on, _T("mtl_transparency_thin_on"), TYPE_BOOL, 0, IDS_TRANSP_THIN,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_TRANSP_THIN_ON,
    p_end,
  mtl_transpar_dist_color, _T("mtl_fog_color"), TYPE_RGBA, P_ANIMATABLE, IDS_FOG_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_FOG_COLOR,
    p_end,
  mtl_transpar_dist_mult, _T("mtl_fog_multiplier"), TYPE_FLOAT, P_ANIMATABLE, IDS_FOG_MULT,
    p_default, 0.0F,
    p_range, 0.0F, 1000.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_FOG_MULT, IDC_FOG_MULT_SPINNER, 1.0F,
    p_end,
  mtl_exit_color, _T("mtl_exit_color"), TYPE_RGBA, P_ANIMATABLE, IDS_EXIT_COLOR,
    p_default, Color(1.0, 1.0, 1.0),
    p_ui, TYPE_COLORSWATCH, IDC_EXIT_COLOR,
    p_end,
  mtl_displacement_on, _T("mtl_displacement_on"), TYPE_BOOL, 0, IDS_DISPLACEMENT_ON,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_DISPLACEMENT_ON,
    p_end,
  mtl_displacement_height, _T("mtl_displacement_height"), TYPE_FLOAT, P_ANIMATABLE, IDS_DISPLACE_HEIGHT,
    p_default, 0.5f,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_DISPLACE_HEIGHT, IDC_DISPLACE_HEIGHT_SPINNER, 0.1F,
    p_end,
  mtl_normal_map, _T("mtl_normal_map"), TYPE_TEXMAP,/*0*/ P_OWNERS_REF, IDS_NORMAL_MAP,
    p_refno, RELIEF_TEX_SLOT,
    p_subtexno, RELIEF_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_NORMAL_MAP,
    p_end,
  mtl_displacement_invert_height_on, _T("mtl_displacement_invert_height_on"), TYPE_BOOL, 0, IDS_DISPLACE_INVERTHEIGHT_ON,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_DISPLACE_INVERTHEIGHT_ON,
    p_end,
  mtl_no_ic_records, _T("mtl_no_ic_records"), TYPE_BOOL, 0, IDS_NO_IC_RECORDS,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_NO_IC_RECORDS,
    p_end,
  mtl_spec_gloss, _T("mtl_spec_gloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_SPEC_GLOSSINESS,
    p_default, 1.0F,
    p_range, 0.0F, 1.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SPECULAR_GL, IDC_SPECULAR_GL_SPINNER, 0.01f,
    p_end,
  mtl_spec_gl_map, _T("mtl_spec_gl_map"), TYPE_TEXMAP, P_OWNERS_REF, IDS_SPEC_GLOSSINESS_MAP, // deprecated2
    p_refno, SPEC_GLOSS_TEX_SLOT,
    p_subtexno, SPEC_GLOSS_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_SPEC_GL_MAP,
    p_end,
  mtl_spec_gloss_or_cos, _T("mtl_spec_gloss_or_cos"), TYPE_INT, 0, IDS_SPEC_GLOSS_OR_COS,
    p_default, 0,
    p_ui, TYPE_RADIO, 2, IDC_SPEC_GL_RAD, IDC_SPEC_COS_RAD,
    p_end,
  mtl_spec_fresnel_on, _T("mtl_spec_fresnel_on"), TYPE_BOOL, 0, IDS_SPEC_FRESNEL_ON,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_SPEC_FRESNEL_ON,
    p_end,
  mtl_refl_gloss, _T("mtl_refl_gloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFL_GLOSSINESS,
    p_default, 1.0F,
    p_range, 0.0F, 10.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFL_GL, IDC_REFL_GL_SPINNER, 0.01f,
    p_end,
  mtl_refl_gl_map, _T("mtl_refl_gl_map"), TYPE_TEXMAP, P_OWNERS_REF, IDS_REFL_GLOSSINESS_MAP,
    p_refno, REFL_GLOSS_TEX_SLOT,
    p_subtexno, REFL_GLOSS_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_REFL_GL_MAP,
    p_end,
  mtl_refl_gloss_or_cos, _T("mtl_refl_gloss_or_cos"), TYPE_INT, 0, IDS_REFL_GLOSS_OR_COS,
    p_default, 0,
    p_ui, TYPE_RADIO, 2, IDC_REFL_GL_RAD, IDC_REFL_COS_RAD,
    p_end,
  mtl_refl_fresnel_on, _T("mtl_refl_fresnel_on,"), TYPE_BOOL, 0, IDS_REFL_FRESNEL_ON,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_REFL_FRESNEL_ON,
    p_end,
  mtl_transpar_gloss, _T("mtl_transp_gloss"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSP_GLOSSINESS,
    p_default, 1.0F,
    p_range, 0.0F, 10.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSP_GL, IDC_TRANSP_GL_SPINNER, 0.01f,
    p_end,
  mtl_transpar_gl_map, _T("mtl_transp_gl_map"), TYPE_TEXMAP, P_OWNERS_REF, IDS_TRANSP_GLOSSINESS_MAP,
    p_refno, TRANSP_GLOSS_TEX_SLOT,
    p_subtexno, TRANSP_GLOSS_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_TRANSP_GL_MAP,
    p_end,
  mtl_transpar_gloss_or_cos, _T("mtl_transp_gloss_or_cos"), TYPE_INT, 0, IDS_TRANSP_GLOSS_OR_COS,
    p_default, 0,
    p_ui, TYPE_RADIO, 2, IDC_TRANSP_GL_RAD, IDC_TRANSP_COS_RAD,
    p_end,
  mtl_lock_specular, _T("mtl_lock_specular,"), TYPE_BOOL, 0, IDS_LOCK_SPECULAR,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_LOCK_SPECULAR,
    p_end,
  mtl_relief_amount, _T("mtl_bump_amount"), TYPE_FLOAT, P_ANIMATABLE, IDS_BUMP_AMT,
    p_default, 0.2F,
    p_range, 0.0F, 5.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BUMP_AMT, IDC_BUMP_AMT_SPINNER, 0.1F,
    p_end,
  mtl_relief_smooth, _T("mtl_bump_radius"), TYPE_FLOAT, P_ANIMATABLE, IDS_BUMP_RADIUS,
    p_default, 0.25F,
    p_range, 0.0F, 1.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BUMP_RADIUS, IDC_BUMP_RADIUS_SPINNER, 0.1F,
    p_end,
  mtl_bump_sigma, _T("mtl_bump_sigma"), TYPE_FLOAT, P_ANIMATABLE, IDS_BUMP_SIGMA,
    p_default, 1.5F,
    p_range, 1.0F, 16.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_BUMP_SIGMA, IDC_BUMP_SIGMA_SPINNER, 0.5f,
    p_end,
  mtl_emission_cast_gi, _T("mtl_emission_gi"), TYPE_BOOL, 0, IDS_EMISSION_GI,
    p_default, TRUE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_EMISSION_GI,
    p_end,
  mtl_opacity_map, _T("mtl_opacity_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_OPACITY_MAP,
    p_refno, OPACITY_TEX_SLOT,
    p_subtexno, OPACITY_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_OPACITY_MAP,
    p_end,
  mtl_opacity_smooth, _T("mtl_opacity_smooth"), TYPE_BOOL, 0, IDS_SMOOTH,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_SMOOTH,
    p_end,
  mtl_transluc_color, _T("mtl_translucency_color"), TYPE_RGBA, P_ANIMATABLE, IDS_TRANSLUCENCY_COLOR,
    p_default, Color(1.0F, 1.0F, 1.0F),
    p_ui, TYPE_COLORSWATCH, IDC_TRANSLUCENCY_COLOR,
    p_end,
  mtl_transluc_mult, _T("mtl_translucency_mult"), TYPE_FLOAT, P_ANIMATABLE, IDS_TRANSLUCENCY_MULT,
    p_default, 0.0F,
    p_range, 0.0F, 100.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_TRANSLUCENCY_MULT, IDC_TRANSLUCENCY_MULT_SPINNER, 0.1F,
    p_end,
  mtl_transluc_map, _T("mtl_translucency_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_TRANSLUCENCY_MAP,
    p_refno, TRANSLUCENCY_TEX_SLOT,
    p_subtexno, TRANSLUCENCY_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_TRANSLUCENCY_MAP,
    p_end,
  mtl_transluc_tint_on, _T("mtl_translucency_mult_on"), TYPE_BOOL, 0, IDS_TRANSLUCENCY_MULT_ON,
    p_default, FALSE,
    p_ui, TYPE_SINGLECHEKBOX, IDC_TRANSLUCENCY_MULT_ON,
    p_end,
  mtl_refl_extrusion, _T("mtl_reflect_extrusion"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_REFLECT_EXTRUSION,
    p_default, 0,
    p_ui, TYPE_INT_COMBOBOX, IDC_REFLECT_EXTRUSION, 2, /*IDS_EXTRUSION_COMP,*/ IDS_EXTRUSION_STRONG, IDS_EXTRUSION_LUM,
    p_tooltip, IDS_REFLECT_EXTRUSION,
    p_end,
  mtl_refl_anisotr, _T("mtl_reflect_anisotropy"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFL_ANISOTROPY,
    p_default, 0.0F,
    p_range, 0.0F, 1.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFL_ANISOTROPY, IDC_REFL_ANISOTROPY_SPINNER, 0.1F,
    p_end,
  mtl_refl_rotat, _T("mtl_reflect_rotation"), TYPE_FLOAT, P_ANIMATABLE, IDS_REFL_ROTATION,
    p_default, 0.0F,
    p_range, 0.0F, 360.0F,
    p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_REFL_ROTATION, IDC_REFL_ROTATION_SPINNER, 5.0F,
    p_end,
  mtl_refl_anisotr_map, _T("mtl_reflect_anisotropy_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_REFL_ANISOTROPY_MAP,
    p_refno, REFL_ANISOTR_TEX_SLOT,
    p_subtexno, REFL_ANISOTR_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_REFL_ANISOTROPY_MAP,
    p_end,
  mtl_refl_rotat_map, _T("mtl_reflect_rotation_map"), TYPE_TEXMAP, /*0*/ P_OWNERS_REF, IDS_REFL_ROTATION_MAP,
    p_refno, REFL_ROTATION_TEX_SLOT,
    p_subtexno, REFL_ROTATION_TEX_SLOT,
    p_ui, TYPE_TEXMAPBUTTON, IDC_REFL_ROTATION_MAP,
    p_end,
  p_end
);


// Make a bitmap texture for environment map.
Interface* HydraMtl::ip             = GetCOREInterface();
BitmapTex* HydraMtl::bmapTexEnv     = NewDefaultBitmapTex();
StdUVGen* HydraMtl::uvGenEnv        = bmapTexEnv->GetUVGen();
Texmap* HydraMtl::texmapEnv         = (Texmap*)NewDefaultBitmapTex();
HydraLogger HydraMtl::materialLog;

//MaxSDK::Graphics::TextureHandle HydraMtl::m_hlslEnvirMap;

bool HydraMtl::hasLoadStaticContent = false;


//////////////////////////////////////////////////////////////////////////
// Param block version system for compatible versions. Called in Load().
// The current version
constexpr int NUMPARAM_V0 = 66;

static ParamBlockDescID descMainV0[NUMPARAM_V0] =
{
  { TYPE_BOOL ,  NULL, FALSE,  mtl_affect_shadow,                            },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_diffuse_color,                            },
  { TYPE_USER ,  NULL, FALSE,  mtl_diffuse_map,                              },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_diffuse_tint_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_diffuse_mult,                             },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_specular_color,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_specular_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_specular_tint_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_mult,                            },
  { TYPE_INT  ,  NULL, FALSE,  mtl_specular_brdf,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_roughness,                       },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_cospower,                        },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_ior,                             },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_refl_color,                               },
  { TYPE_USER ,  NULL, FALSE,  mtl_refl_map,                                 },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_refl_tint_on,                             },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_mult,                                },
  { TYPE_INT  ,  NULL, FALSE,  mtl_refl_brdf,                                },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_cospower,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_roughness,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_ior,                                 },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_emission_color,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_emission_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_emission_tint_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_emission_mult,                            },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_transpar_color,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_transpar_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_transpar_tint_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_mult,                            },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_transpar_thin_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_ior,                             },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_cospower,                        },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_transpar_dist_color,                      },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_dist_mult,                       },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_exit_color,                               },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_displacement_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_displacement_height,                      },
  { TYPE_USER ,  NULL, FALSE,  mtl_normal_map,                               },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_displacement_invert_height_on,            },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_no_ic_records,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_spec_gloss,                               },
  { TYPE_USER ,  NULL, FALSE,  mtl_spec_gl_map,                              },
  { TYPE_INT  ,  NULL, FALSE,  mtl_spec_gloss_or_cos,                        },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_spec_fresnel_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_gloss,                               },
  { TYPE_USER ,  NULL, FALSE,  mtl_refl_gl_map,                              },
  { TYPE_INT  ,  NULL, FALSE,  mtl_refl_gloss_or_cos,                        },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_refl_fresnel_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_gloss,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_transpar_gl_map,                          },
  { TYPE_INT  ,  NULL, FALSE,  mtl_transpar_gloss_or_cos,                    },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_lock_specular,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_relief_amount,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_relief_smooth,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_bump_sigma,                               },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_emission_cast_gi,                         },
  { TYPE_USER ,  NULL, FALSE,  mtl_opacity_map,                              },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_opacity_smooth,                           },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_transluc_color,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transluc_mult,                            },
  { TYPE_USER ,  NULL, FALSE,  mtl_transluc_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_transluc_tint_on,                         },
  { TYPE_INT  ,  NULL, FALSE,  mtl_refl_extrusion,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_roughness_mult,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_anisotr,                             },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_rotat                                },
                                                                             };

constexpr int NUMPARAM_V1 = 68;
static ParamBlockDescID descMainV1[NUMPARAM_V1] =
{
  { TYPE_BOOL ,  NULL, FALSE,  mtl_affect_shadow,                            },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_diffuse_color,                            },
  { TYPE_USER ,  NULL, FALSE,  mtl_diffuse_map,                              },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_diffuse_tint_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_diffuse_mult,                             },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_specular_color,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_specular_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_specular_tint_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_mult,                            },
  { TYPE_INT  ,  NULL, FALSE,  mtl_specular_brdf,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_roughness,                       },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_cospower,                        },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_specular_ior,                             },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_refl_color,                               },
  { TYPE_USER ,  NULL, FALSE,  mtl_refl_map,                                 },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_refl_tint_on,                             },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_mult,                                },
  { TYPE_INT  ,  NULL, FALSE,  mtl_refl_brdf,                                },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_cospower,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_roughness,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_ior,                                 },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_emission_color,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_emission_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_emission_tint_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_emission_mult,                            },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_transpar_color,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_transpar_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_transpar_tint_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_mult,                            },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_transpar_thin_on,                         },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_ior,                             },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_cospower,                        },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_transpar_dist_color,                      },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_dist_mult,                       },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_exit_color,                               },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_displacement_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_displacement_height,                      },
  { TYPE_USER ,  NULL, FALSE,  mtl_normal_map,                               },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_displacement_invert_height_on,            },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_no_ic_records,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_spec_gloss,                               },
  { TYPE_USER ,  NULL, FALSE,  mtl_spec_gl_map,                              },
  { TYPE_INT  ,  NULL, FALSE,  mtl_spec_gloss_or_cos,                        },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_spec_fresnel_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_gloss,                               },
  { TYPE_USER ,  NULL, FALSE,  mtl_refl_gl_map,                              },
  { TYPE_INT  ,  NULL, FALSE,  mtl_refl_gloss_or_cos,                        },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_refl_fresnel_on,                          },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transpar_gloss,                           },
  { TYPE_USER ,  NULL, FALSE,  mtl_transpar_gl_map,                          },
  { TYPE_INT  ,  NULL, FALSE,  mtl_transpar_gloss_or_cos,                    },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_lock_specular,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_relief_amount,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_relief_smooth,                            },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_bump_sigma,                               },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_emission_cast_gi,                         },
  { TYPE_USER ,  NULL, FALSE,  mtl_opacity_map,                              },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_opacity_smooth,                           },
  { TYPE_RGBA ,  NULL, TRUE,   mtl_transluc_color,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_transluc_mult,                            },
  { TYPE_USER ,  NULL, FALSE,  mtl_transluc_map,                             },
  { TYPE_BOOL ,  NULL, FALSE,  mtl_transluc_tint_on,                         },
  { TYPE_INT  ,  NULL, FALSE,  mtl_refl_extrusion,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_roughness_mult,                           },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_anisotr,                             },
  { TYPE_FLOAT,  NULL, TRUE,   mtl_refl_rotat                                },
  { TYPE_USER ,  NULL, FALSE,  mtl_refl_anisotr_map,                         },
  { TYPE_USER ,  NULL, FALSE,  mtl_refl_rotat_map,                           },
                                                                             };

#define NUM_LAST_VERSIONS (VERSION_CURRENT + 1)
static ParamVersionDesc versionsMainPB[NUM_LAST_VERSIONS] =
{
  ParamVersionDesc(descMainV0, NUMPARAM_V0, 0),
  ParamVersionDesc(descMainV1, NUMPARAM_V1, 1)
};


// For update version:
// 1) Add new parameters only to the end of the enum list.
// 2) Do not remove param from the list of enum, just add.
// 2) Add new or delete parameters in ParamBlockDesc2.
// 3) Create new ParamBlockDescID descVxx[] with new params.
// 4) Add new ParamVersionDesc in ParamVersionDesc versions[]
// 7) VERSION_CURRENT += 1

//////////////////////////////////////////////////////////////////////////



HydraMtlDlgProc::HydraMtlDlgProc(HydraMtl *cb)
: hM(cb) {}



void HydraMtlDlgProc::DropFileInMapSlot(int mapControlID, const std::filesystem::path& a_filename)
{
  BitmapTex *btex = NewDefaultBitmapTex();

  if (!btex)  
    return;
  
    WStr name = a_filename.stem().c_str();

  //std::wstring validExtensions[] = { L".bmp", L".jpg", L".jpeg", L".png", L".tga", L".tiff", L".tif", L".psd"};

  btex->SetMapName(a_filename.c_str());
  btex->SetName(name.data());

  Texmap* tex = dynamic_cast<Texmap*>(btex);

  switch (mapControlID)
  {
    case IDC_DIFFUSE_MAP:
      hM->SetSubTexmap(DIFFUSE_TEX_SLOT, tex);
      hM->subTexSlots[DIFFUSE_TEX_SLOT]       = tex;
      break;
    case IDC_REFLECT_MAP:
      hM->SetSubTexmap(REFLECT_TEX_SLOT, tex);
      hM->subTexSlots[REFLECT_TEX_SLOT]       = tex;
      break;
    case IDC_REFL_GL_MAP:
      hM->SetSubTexmap(REFL_GLOSS_TEX_SLOT, tex);
      hM->subTexSlots[REFL_GLOSS_TEX_SLOT]    = tex;
      break;
    case IDC_REFL_ANISOTROPY_MAP:
      hM->SetSubTexmap(REFL_ANISOTR_TEX_SLOT, tex);
      hM->subTexSlots[REFL_ANISOTR_TEX_SLOT]  = tex;
      break;
    case IDC_REFL_ROTATION_MAP:
      hM->SetSubTexmap(REFL_ROTATION_TEX_SLOT, tex);
      hM->subTexSlots[REFL_ROTATION_TEX_SLOT] = tex;
      break;
    case IDC_TRANSP_MAP:
      hM->SetSubTexmap(TRANSP_TEX_SLOT, tex);
      hM->subTexSlots[TRANSP_TEX_SLOT]        = tex;
      break;
    case IDC_TRANSP_GL_MAP:
      hM->SetSubTexmap(TRANSP_GLOSS_TEX_SLOT, tex);
      hM->subTexSlots[TRANSP_GLOSS_TEX_SLOT]  = tex;
      break;
    case IDC_OPACITY_MAP:
      hM->SetSubTexmap(OPACITY_TEX_SLOT, tex);
      hM->subTexSlots[OPACITY_TEX_SLOT]       = tex;
      break;
    case IDC_TRANSLUCENCY_MAP:
      hM->SetSubTexmap(TRANSLUCENCY_TEX_SLOT, tex);
      hM->subTexSlots[TRANSLUCENCY_TEX_SLOT]  = tex;
      break;
    case IDC_EMISSION_MAP:
      hM->SetSubTexmap(EMISSION_TEX_SLOT, tex);
      hM->subTexSlots[EMISSION_TEX_SLOT]      = tex;
      break;
    case IDC_NORMAL_MAP:
      hM->SetSubTexmap(RELIEF_TEX_SLOT, tex);
      hM->subTexSlots[RELIEF_TEX_SLOT]        = tex;
      break;
    default:
      break;
  }
}

void SetEnableParamAnisotropy(IParamMap2* map, HydraMtl* hM)
{
  if (!hM || !map) return;
  
  hM->m_reflectBrdf = hM->GetReflBRDF();

  if (hM->m_reflectBrdf == BRDF_BECKMANN || hM->m_reflectBrdf == BRDF_TRGGX)
  {
    map->Enable(mtl_refl_anisotr, true);
    map->Enable(mtl_refl_rotat, true);
  }
  else
  {
    map->Enable(mtl_refl_anisotr, false);
    map->Enable(mtl_refl_rotat, false);
  }
}

INT_PTR HydraMtlDlgProc::DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  thishWnd=hWnd;
  HydraMtlDlgProc const *dlg = DLGetWindowLongPtr<HydraMtlDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;


  switch (msg)
  {
    case WM_INITDIALOG:
    {
      DragAcceptFiles(GetDlgItem(hWnd, IDC_EMISSION_MAP)        , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_DIFFUSE_MAP)         , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_REFLECT_MAP)         , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_TRANSP_MAP)          , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_NORMAL_MAP)          , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_SPEC_GL_MAP)         , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_REFL_GL_MAP)         , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_TRANSP_GL_MAP)       , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_OPACITY_MAP)         , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_TRANSLUCENCY_MAP)    , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_REFL_ANISOTROPY_MAP) , true);
      DragAcceptFiles(GetDlgItem(hWnd, IDC_REFL_ROTATION_MAP)   , true);

      SetEnableParamAnisotropy(map, hM);

      return TRUE;
    }
    case WM_COMMAND:
      if (LOWORD(wParam) == IDC_REFLECT_BRDF && HIWORD(wParam) == LBN_SELCHANGE)        
        SetEnableParamAnisotropy(map, hM);                 
    case WM_DESTROY:
      break;
    case WM_DROPFILES:
      POINT pt;
      WORD numFiles;
      HWND dropTarget;
      int dropTargetID;
      wchar_t lpszFile[255];

      DragQueryPoint((HDROP)wParam, &pt);
      numFiles = DragQueryFile((HDROP)wParam, 0xFFFF, (LPWSTR)NULL, 0);

      if (numFiles == 0)
      {
        DragQueryFile((HDROP)wParam, 0, lpszFile, sizeof(lpszFile));
        dropTarget = RealChildWindowFromPoint(hWnd, pt);
        dropTargetID = GetDlgCtrlID(dropTarget);
        DropFileInMapSlot(dropTargetID, lpszFile);
      }

      DragFinish((HDROP)wParam);

      break;
    default:
      return FALSE;
  }
  return TRUE;
}


HydraMtl::HydraMtl(BOOL loading)
{
  if (!hasLoadStaticContent)
  {
    hasLoadStaticContent = true;
    uvGenEnv->SetCoordMapping(UVMAP_SPHERE_ENV);
    uvGenEnv->ActivateTexDisplay(false);
    bmapTexEnv->SetFilterType(FILTER_NADA);
    bmapTexEnv->SetMapName(_T("c:\\[Hydra]\\pluginFiles\\material\\EnvMatEdit.hdr"));
    texmapEnv = bmapTexEnv;
  }


  pblock = nullptr;

  for (auto& tex : subTexSlots)
    tex = nullptr;

  const Color white(1.0F, 1.0F, 1.0F);

  m_diffuseColor      = white * 0.8F;
  m_specularColor     = white;
  m_reflectColor      = white;
  m_emissionColor     = white;
  m_transpColor       = white;
  m_transpDistColor   = white;
  m_exitColor         = white;
  m_translucColor     = white;

  m_diffuseTint       = false;
  m_reflectTint       = false;
  m_specularFresnel   = true;
  m_reflectFresnel    = true;
  m_lockSpecular      = true;
  m_specularTint      = false;
  m_transpTint        = false;
  m_transpThin        = false;
  m_noIcRecords       = false;
  m_affectShadows     = true;
  m_opacitySmooth     = false;
  m_translucTint      = false;
  m_emissionTint      = false;
  m_emissionCastGi    = true;
  m_displacement      = false;
  m_invertHeight      = false;

  m_specularRough     = 0.0F;
  m_reflectRough      = 0.0F;
  m_reflectCospow     = 0.0F;
  m_transpIor         = 1.5F;
  m_specularIor       = 0.0F;
  m_reflectIor        = 0.0F;
  m_transpCospower    = 0.0F;
  m_specularCospow    = 0.0F;
  m_transpDistMult    = 0.0F;
  m_displaceHeight    = 0.0F;
  m_diffuseMult       = 1.0F;
  m_specularMult      = 0.0F;
  m_emissionMult      = 0.0F;
  m_transpMult        = 0.0F;
  m_reflectMult       = 0.0F;
  m_specularGloss     = 0.0F;
  m_reflectGloss      = 0.0F;
  m_transpGloss       = 0.0F;
  m_reliefAmount      = 0.0F;
  m_reliefSmooth      = 1.0F;
  m_bumpSigma         = 1.5F;
  m_translucMult      = 0.0F;
  m_diffuseRoughness  = 0.0F;
  m_reflectAnisotr    = 0.0F;
  m_reflectAnisRotat  = 0.0F;

  m_specularBrdf      = BRDF_GGX;
  m_reflectBrdf       = BRDF_GGX;
  m_specGlossOrCos    = 0;
  m_reflectGlossOrCos = 0;
  m_transpGlossOrCos  = 0;
  m_reflectExtrusion  = EXTRUSION_STRONG;

  // ask the ClassDesc to make the P_AUTO_CONSTRUCT paramblocks
  hydraMtlDesc.MakeAutoParamBlocks(this);
}


/*===========================================================================*\
|    Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000
#define HYDRA_CHUNK 0x5000

IOResult HydraMtl::Save(ISave *isave)
{
  IOResult res;
  ULONG nb;

  isave->BeginChunk(MTL_HDR_CHUNK);
  res = MtlBase::Save(isave);

  isave->EndChunk();

  if (res != IO_OK)
    return res;


  isave->BeginChunk(HYDRA_CHUNK);
  isave->Write(&m_diffuseColor      , sizeof(Color), &nb);
  isave->Write(&m_specularColor     , sizeof(Color), &nb);
  isave->Write(&m_reflectColor      , sizeof(Color), &nb);
  isave->Write(&m_emissionColor     , sizeof(Color), &nb);
  isave->Write(&m_transpColor       , sizeof(Color), &nb);
  isave->Write(&m_transpDistColor   , sizeof(Color), &nb);
  isave->Write(&m_exitColor         , sizeof(Color), &nb);
  isave->Write(&m_translucColor     , sizeof(Color), &nb);

  isave->Write(&m_displacement      , sizeof(bool), &nb);
  isave->Write(&m_invertHeight      , sizeof(bool), &nb);
  isave->Write(&m_diffuseTint       , sizeof(bool), &nb);
  isave->Write(&m_specularTint      , sizeof(bool), &nb);
  isave->Write(&m_emissionTint      , sizeof(bool), &nb);
  isave->Write(&m_transpTint        , sizeof(bool), &nb);
  isave->Write(&m_reflectTint       , sizeof(bool), &nb);
  isave->Write(&m_transpThin        , sizeof(bool), &nb);
  isave->Write(&m_affectShadows     , sizeof(bool), &nb);
  isave->Write(&m_noIcRecords       , sizeof(bool), &nb);
  isave->Write(&m_specularFresnel   , sizeof(bool), &nb);
  isave->Write(&m_reflectFresnel    , sizeof(bool), &nb);
  isave->Write(&m_lockSpecular      , sizeof(bool), &nb);
  isave->Write(&m_emissionCastGi    , sizeof(bool), &nb);
  isave->Write(&m_opacitySmooth     , sizeof(bool), &nb);
  isave->Write(&m_translucTint      , sizeof(bool), &nb);

  isave->Write(&m_specularRough     , sizeof(float), &nb);
  isave->Write(&m_reflectRough      , sizeof(float), &nb);
  isave->Write(&m_reflectCospow     , sizeof(float), &nb);
  isave->Write(&m_specularCospow    , sizeof(float), &nb);
  isave->Write(&m_transpIor         , sizeof(float), &nb);
  isave->Write(&m_specularIor       , sizeof(float), &nb);
  isave->Write(&m_reflectIor        , sizeof(float), &nb);
  isave->Write(&m_transpCospower    , sizeof(float), &nb);
  isave->Write(&m_transpDistMult    , sizeof(float), &nb);
  isave->Write(&m_displaceHeight    , sizeof(float), &nb);
  isave->Write(&m_diffuseMult       , sizeof(float), &nb);
  isave->Write(&m_specularMult      , sizeof(float), &nb);
  isave->Write(&m_emissionMult      , sizeof(float), &nb);
  isave->Write(&m_transpMult        , sizeof(float), &nb);
  isave->Write(&m_reflectMult       , sizeof(float), &nb);
  isave->Write(&m_specularGloss     , sizeof(float), &nb);
  isave->Write(&m_reflectGloss      , sizeof(float), &nb);
  isave->Write(&m_transpGloss       , sizeof(float), &nb);
  isave->Write(&m_reliefAmount      , sizeof(float), &nb);
  isave->Write(&m_reliefSmooth      , sizeof(float), &nb);
  isave->Write(&m_bumpSigma         , sizeof(float), &nb);
  isave->Write(&m_translucMult      , sizeof(float), &nb);
  isave->Write(&m_diffuseRoughness  , sizeof(float), &nb);
  isave->Write(&m_reflectAnisotr    , sizeof(float), &nb);
  isave->Write(&m_reflectAnisRotat  , sizeof(float), &nb);

  isave->Write(&m_specularBrdf      , sizeof(int), &nb);
  isave->Write(&m_reflectBrdf       , sizeof(int), &nb);
  isave->Write(&m_specGlossOrCos    , sizeof(int), &nb);
  isave->Write(&m_reflectGlossOrCos , sizeof(int), &nb);
  isave->Write(&m_transpGlossOrCos  , sizeof(int), &nb);
  isave->Write(&m_reflectExtrusion  , sizeof(int), &nb);
  isave->EndChunk();

  return IO_OK;
}


IOResult HydraMtl::Load(ILoad *iload)
{
  IOResult res;
  int id;
  ULONG nb;
  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
      case MTL_HDR_CHUNK:
        res = MtlBase::Load(iload);
        break;
      case HYDRA_CHUNK:
        res = iload->Read(&m_diffuseColor     , sizeof(Color), &nb);
        res = iload->Read(&m_specularColor    , sizeof(Color), &nb);
        res = iload->Read(&m_reflectColor     , sizeof(Color), &nb);
        res = iload->Read(&m_emissionColor    , sizeof(Color), &nb);
        res = iload->Read(&m_transpColor      , sizeof(Color), &nb);
        res = iload->Read(&m_transpDistColor  , sizeof(Color), &nb);
        res = iload->Read(&m_exitColor        , sizeof(Color), &nb);
        res = iload->Read(&m_translucColor    , sizeof(Color), &nb);

        res = iload->Read(&m_displacement     , sizeof(bool), &nb);
        res = iload->Read(&m_invertHeight     , sizeof(bool), &nb);
        res = iload->Read(&m_diffuseTint      , sizeof(bool), &nb);
        res = iload->Read(&m_specularTint     , sizeof(bool), &nb);
        res = iload->Read(&m_emissionTint     , sizeof(bool), &nb);
        res = iload->Read(&m_transpTint       , sizeof(bool), &nb);
        res = iload->Read(&m_reflectTint      , sizeof(bool), &nb);
        res = iload->Read(&m_transpThin       , sizeof(bool), &nb);
        res = iload->Read(&m_affectShadows    , sizeof(bool), &nb);
        res = iload->Read(&m_noIcRecords      , sizeof(bool), &nb);
        res = iload->Read(&m_specularFresnel  , sizeof(bool), &nb);
        res = iload->Read(&m_reflectFresnel   , sizeof(bool), &nb);
        res = iload->Read(&m_lockSpecular     , sizeof(bool), &nb);
        res = iload->Read(&m_emissionCastGi   , sizeof(bool), &nb);
        res = iload->Read(&m_opacitySmooth    , sizeof(bool), &nb);
        res = iload->Read(&m_translucTint     , sizeof(bool), &nb);

        res = iload->Read(&m_specularRough    , sizeof(float), &nb);
        res = iload->Read(&m_reflectRough     , sizeof(float), &nb);
        res = iload->Read(&m_reflectCospow    , sizeof(float), &nb);
        res = iload->Read(&m_specularCospow   , sizeof(float), &nb);
        res = iload->Read(&m_transpIor        , sizeof(float), &nb);
        res = iload->Read(&m_specularIor      , sizeof(float), &nb);
        res = iload->Read(&m_reflectIor       , sizeof(float), &nb);
        res = iload->Read(&m_transpCospower   , sizeof(float), &nb);
        res = iload->Read(&m_transpDistMult   , sizeof(float), &nb);
        res = iload->Read(&m_displaceHeight   , sizeof(float), &nb);
        res = iload->Read(&m_diffuseMult      , sizeof(float), &nb);
        res = iload->Read(&m_specularMult     , sizeof(float), &nb);
        res = iload->Read(&m_emissionMult     , sizeof(float), &nb);
        res = iload->Read(&m_transpMult       , sizeof(float), &nb);
        res = iload->Read(&m_reflectMult      , sizeof(float), &nb);
        res = iload->Read(&m_specularGloss    , sizeof(float), &nb);
        res = iload->Read(&m_reflectGloss     , sizeof(float), &nb);
        res = iload->Read(&m_transpGloss      , sizeof(float), &nb);
        res = iload->Read(&m_reliefAmount     , sizeof(float), &nb);
        res = iload->Read(&m_reliefSmooth     , sizeof(float), &nb);
        res = iload->Read(&m_bumpSigma        , sizeof(float), &nb);
        res = iload->Read(&m_translucMult     , sizeof(float), &nb);
        res = iload->Read(&m_diffuseRoughness , sizeof(float), &nb);
        res = iload->Read(&m_reflectAnisotr   , sizeof(float), &nb);
        res = iload->Read(&m_reflectAnisRotat , sizeof(float), &nb);

        res = iload->Read(&m_specularBrdf     , sizeof(int), &nb);
        res = iload->Read(&m_reflectBrdf      , sizeof(int), &nb);
        res = iload->Read(&m_specGlossOrCos   , sizeof(int), &nb);
        res = iload->Read(&m_reflectGlossOrCos, sizeof(int), &nb);
        res = iload->Read(&m_transpGlossOrCos , sizeof(int), &nb);
        res = iload->Read(&m_reflectExtrusion , sizeof(int), &nb);
        break;
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  iload->RegisterPostLoadCallback(new ParamBlock2PLCB(versionsMainPB, NUM_LAST_VERSIONS, &hydraMtl_param_blk, this, PBLOCK_REF));

  return IO_OK;
}



void HydraMtl::Reset()
{
  ivalid.SetEmpty();

  //for (int i=0; i < NSUBMTL; i++)
  //{
  //   if( submtl[i] ){
  //     DeleteReference(i);
  //     submtl[i] = NULL;
  //   }
  //}

  for (int i = 0; i < NUM_SUBTEX; i++)
  {
    if (subTexSlots[i])
    {
      DeleteReference(i);
      subTexSlots[i] = nullptr;
    }
  }

  GetHydraMtlDesc()->MakeAutoParamBlocks(this);
}


ParamDlg* HydraMtl::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp)
{
  IAutoMParamDlg* masterDlg = GetHydraMtlDesc()->CreateParamDlgs(hwMtlEdit, imp, this);

  // Set param block user dialog if necessary
  hydraMtl_param_blk.SetUserDlgProc(new HydraMtlDlgProc(this));
  return masterDlg;
}


Interval HydraMtl::Validity(TimeValue t)
{
  Interval valid = FOREVER;

  //for (int i=0; i < NSUBMTL; i++)
  //{
  //   if (submtl[i])
  //     valid &= submtl[i]->Validity(t);
  //}

  for (auto i : subTexSlots)
  {
    if (i != nullptr)
      valid &= i->Validity(t);
  }

  return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/

RefTargetHandle HydraMtl::GetReference(int i)
{
  if (i < NUM_SUBTEX)
    return subTexSlots[i];
  else
    return pblock;
}

void HydraMtl::SetReference(int i, RefTargetHandle rtarg)
{
  MSTR s = L"";
  if (rtarg)
    rtarg->GetClassName(s);

  if (i < NUM_SUBTEX && s != L"ParamBlock2") subTexSlots[i] = (Texmap *)rtarg;
  else                                       pblock         = (IParamBlock2 *)rtarg;
}

IParamBlock2 * HydraMtl::GetParamBlock(int i)
{
  if (MAIN_PB_ID == i) return pblock;
  else                 return nullptr;    
}

IParamBlock2 * HydraMtl::GetParamBlockByID(BlockID id)
{
  if (MAIN_PB_ID == id) return pblock;
  else                  return nullptr;  
}

#ifdef MAX2022
MSTR HydraMtl::SubAnimName(int i, bool localized)
{
  if (i < NSUBMTL)
    return GetSubMtlTVName(i);
  else
    return TSTR(_T(""));
}

#else
TSTR HydraMtl::SubAnimName(int i)
{
  if (i < NSUBMTL)
    return GetSubMtlTVName(i);
  else
    return TSTR(_T(""));
}
#endif // MAX2022

Animatable* HydraMtl::SubAnim(int i)
{  
  if (i < NUM_SUBTEX) return subTexSlots[i];
  else                return pblock;
}


RefResult HydraMtl::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
    case REFMSG_CHANGE:
      ivalid.SetEmpty();
      if (hTarget == pblock)
      {
        const ParamID changing_param = pblock->LastNotifyParamID();
        hydraMtl_param_blk.InvalidateUI(changing_param);
      }
      break;
    default:
      break;
  }
  return REF_SUCCEED;
}


/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/

Mtl* HydraMtl::GetSubMtl(int i) noexcept
{
  /*if (i < NSUBMTL )
    return submtl[i];*/
  return nullptr;
}

void HydraMtl::SetSubMtl(int i, Mtl *m)
{
  ReplaceReference(i, m);
  // Set the material and update the UI	
}

#ifdef MAX2022
MSTR HydraMtl::GetSubMtlSlotName(int i, bool localized)
{
  return _T("");
}

MSTR HydraMtl::GetSubMtlTVName(int i, bool localized)
{
  return GetSubMtlSlotName(i, false);
}

#else
TSTR HydraMtl::GetSubMtlSlotName(int i)
{
  // Return i'th sub-material name 
  return _T("");
}

TSTR HydraMtl::GetSubMtlTVName(int i)
{
  return GetSubMtlSlotName(i);
}
#endif // MAX2022


/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/

Texmap* HydraMtl::GetSubTexmap(int i) noexcept
{
  return subTexSlots[i];
}


void HydraMtl::SetSubTexmap(int i, Texmap *m)
{
  switch (i)
  {
    case DIFFUSE_TEX_SLOT:
      ReplaceReference(i, m);      
      pblock->SetValue(mtl_diffuse_map, TimeValue(0), m);
      break;
    case SPECULAR_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_specular_map, TimeValue(0), m);
      break;
    case REFLECT_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_refl_map, TimeValue(0), m);
      break;
    case EMISSION_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_emission_map, TimeValue(0), m);
      break;
    case TRANSP_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_transpar_map, TimeValue(0), m);
      break;
    case RELIEF_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_normal_map, TimeValue(0), m);
      break;
    case SPEC_GLOSS_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_spec_gl_map, TimeValue(0), m);
      break;
    case REFL_GLOSS_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_refl_gl_map, TimeValue(0), m);
      break;
    case TRANSP_GLOSS_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_transpar_gl_map, TimeValue(0), m);
      break;
    case OPACITY_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_opacity_map, TimeValue(0), m);
      break;
    case TRANSLUCENCY_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_transluc_map, TimeValue(0), m);
      break;
    case REFL_ANISOTR_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_refl_anisotr_map, TimeValue(0), m);
      break;
    case REFL_ROTATION_TEX_SLOT:
      ReplaceReference(i, m);
      pblock->SetValue(mtl_refl_rotat_map, TimeValue(0), m);
      break;
    default:
      break;
  }

  NotifyChanged();
}

#ifdef MAX2022
MSTR HydraMtl::GetSubTexmapSlotName(int i, bool localized)
{
  switch (i)
  {
  case DIFFUSE_TEX_SLOT:        return GetString(IDS_DIFFUSE_MAP);
  case SPECULAR_TEX_SLOT:       return GetString(IDS_SPECULAR_MAP);
  case REFLECT_TEX_SLOT:        return GetString(IDS_REFLECT_MAP);
  case EMISSION_TEX_SLOT:       return GetString(IDS_EMISSION_MAP);
  case TRANSP_TEX_SLOT:         return GetString(IDS_TRANSP_MAP);
  case RELIEF_TEX_SLOT:         return GetString(IDS_NORMAL_MAP);
  case SPEC_GLOSS_TEX_SLOT:     return GetString(IDS_SPEC_GLOSSINESS_MAP);
  case REFL_GLOSS_TEX_SLOT:     return GetString(IDS_REFL_GLOSSINESS_MAP);
  case TRANSP_GLOSS_TEX_SLOT:   return GetString(IDS_TRANSP_GLOSSINESS_MAP);
  case OPACITY_TEX_SLOT:        return GetString(IDS_OPACITY_MAP);
  case TRANSLUCENCY_TEX_SLOT:   return GetString(IDS_TRANSLUCENCY_MAP);
  case REFL_ANISOTR_TEX_SLOT:   return GetString(IDS_REFL_ANISOTROPY_MAP);
  case REFL_ROTATION_TEX_SLOT:  return GetString(IDS_REFL_ROTATION_MAP);
  default: return _T("");
  }
}
MSTR HydraMtl::GetSubTexmapTVName(int i, bool localized)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i, false);
}
#else
TSTR HydraMtl::GetSubTexmapSlotName(int i)
{
  switch (i)
  {
    case DIFFUSE_TEX_SLOT:        return GetString(IDS_DIFFUSE_MAP);
    case SPECULAR_TEX_SLOT:       return GetString(IDS_SPECULAR_MAP);
    case REFLECT_TEX_SLOT:        return GetString(IDS_REFLECT_MAP);
    case EMISSION_TEX_SLOT:       return GetString(IDS_EMISSION_MAP);
    case TRANSP_TEX_SLOT:         return GetString(IDS_TRANSP_MAP);
    case RELIEF_TEX_SLOT:         return GetString(IDS_NORMAL_MAP);
    case SPEC_GLOSS_TEX_SLOT:     return GetString(IDS_SPEC_GLOSSINESS_MAP);
    case REFL_GLOSS_TEX_SLOT:     return GetString(IDS_REFL_GLOSSINESS_MAP);
    case TRANSP_GLOSS_TEX_SLOT:   return GetString(IDS_TRANSP_GLOSSINESS_MAP);
    case OPACITY_TEX_SLOT:        return GetString(IDS_OPACITY_MAP);
    case TRANSLUCENCY_TEX_SLOT:   return GetString(IDS_TRANSLUCENCY_MAP);
    case REFL_ANISOTR_TEX_SLOT:   return GetString(IDS_REFL_ANISOTROPY_MAP);
    case REFL_ROTATION_TEX_SLOT:  return GetString(IDS_REFL_ROTATION_MAP);
    default: return _T("");
  }
}

TSTR HydraMtl::GetSubTexmapTVName(int i)
{
  // Return i'th sub-texture name 
  return GetSubTexmapSlotName(i);
}
#endif // MAX2022






/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/

RefTargetHandle HydraMtl::Clone(RemapDir &remap)
{
  HydraMtl *mnew = new HydraMtl(FALSE);
  *((MtlBase*)mnew) = *((MtlBase*)this);
  mnew->ReplaceReference(NSUBMTL, remap.CloneRef(pblock));

  mnew->m_diffuseColor          = m_diffuseColor;
  mnew->m_specularColor         = m_specularColor;
  mnew->m_reflectColor          = m_reflectColor;
  mnew->m_emissionColor         = m_emissionColor;
  mnew->m_transpColor           = m_transpColor;
  mnew->m_transpDistColor       = m_transpDistColor;
  mnew->m_exitColor             = m_exitColor;
  mnew->m_translucColor         = m_translucColor;

  mnew->m_displacement          = m_displacement;
  mnew->m_invertHeight          = m_invertHeight;
  mnew->m_diffuseTint           = m_diffuseTint;
  mnew->m_specularTint          = m_specularTint;
  mnew->m_emissionTint          = m_emissionTint;
  mnew->m_transpTint            = m_transpTint;
  mnew->m_reflectTint           = m_reflectTint;
  mnew->m_transpThin            = m_transpThin;
  mnew->m_affectShadows         = m_affectShadows;
  mnew->m_noIcRecords           = m_noIcRecords;
  mnew->m_specularFresnel       = m_specularFresnel;
  mnew->m_reflectFresnel        = m_reflectFresnel;
  mnew->m_lockSpecular          = m_lockSpecular;
  mnew->m_emissionCastGi        = m_emissionCastGi;
  mnew->m_opacitySmooth         = m_opacitySmooth;

  mnew->m_specularRough         = m_specularRough;
  mnew->m_reflectRough          = m_reflectRough;
  mnew->m_reflectCospow         = m_reflectCospow;
  mnew->m_transpIor             = m_transpIor;
  mnew->m_specularIor           = m_specularIor;
  mnew->m_reflectIor            = m_reflectIor;
  mnew->m_transpCospower        = m_transpCospower;
  mnew->m_specularCospow        = m_specularCospow;
  mnew->m_transpDistMult        = m_transpDistMult;
  mnew->m_displaceHeight        = m_displaceHeight;
  mnew->m_diffuseMult           = m_diffuseMult;
  mnew->m_specularMult          = m_specularMult;
  mnew->m_emissionMult          = m_emissionMult;
  mnew->m_transpMult            = m_transpMult;
  mnew->m_reflectMult           = m_reflectMult;
  mnew->m_specularGloss         = m_specularGloss;
  mnew->m_reflectGloss          = m_reflectGloss;
  mnew->m_transpGloss           = m_transpGloss;
  mnew->m_reliefAmount          = m_reliefAmount;
  mnew->m_reliefSmooth          = m_reliefSmooth;
  mnew->m_bumpSigma             = m_bumpSigma;
  mnew->m_translucMult          = m_translucMult;
  mnew->m_translucTint          = m_translucTint;
  mnew->m_diffuseRoughness      = m_diffuseRoughness;
  mnew->m_reflectAnisotr        = m_reflectAnisotr;
  mnew->m_reflectAnisRotat      = m_reflectAnisRotat;

  mnew->m_specularBrdf          = m_specularBrdf;
  mnew->m_reflectBrdf           = m_reflectBrdf;
  mnew->m_specGlossOrCos        = m_specGlossOrCos;
  mnew->m_reflectGlossOrCos     = m_reflectGlossOrCos;
  mnew->m_transpGlossOrCos      = m_transpGlossOrCos;
  mnew->m_reflectExtrusion      = m_reflectExtrusion;

  mnew->ivalid.SetInfinite();

  //for (int i = 0; i < NSUBMTL; i++)
  //{
  //  mnew->submtl[i] = NULL;
  //  if (submtl[i])
  //    mnew->ReplaceReference(i,remap.CloneRef(submtl[i]));
  //    mnew->mapOn[i] = mapOn[i];
  //}

  for (int i = 0; i < NUM_SUBTEX; i++)
  {
    mnew->subTexSlots[i] = nullptr;
    if (subTexSlots[i])
      mnew->ReplaceReference(i, remap.CloneRef(subTexSlots[i]));
  }

  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}


void HydraMtl::Update(TimeValue t, Interval& valid)
{  
  if (!ivalid.InInterval(t))
  {
    ivalid.SetInfinite();

    pblock->GetValue(mtl_affect_shadow                  , t, m_affectShadows    , ivalid);

    pblock->GetValue(mtl_diffuse_color                  , t, m_diffuseColor     , ivalid);
    pblock->GetValue(mtl_diffuse_tint_on                , t, m_diffuseTint      , ivalid);
    pblock->GetValue(mtl_diffuse_mult                   , t, m_diffuseMult      , ivalid);
    pblock->GetValue(mtl_no_ic_records                  , t, m_noIcRecords      , ivalid);
    pblock->GetValue(mtl_roughness_mult                 , t, m_diffuseRoughness , ivalid);

    pblock->GetValue(mtl_refl_color                     , t, m_reflectColor     , ivalid);
    pblock->GetValue(mtl_refl_tint_on                   , t, m_reflectTint      , ivalid);
    pblock->GetValue(mtl_refl_mult                      , t, m_reflectMult      , ivalid);
    pblock->GetValue(mtl_refl_brdf                      , t, m_reflectBrdf      , ivalid);
    pblock->GetValue(mtl_refl_cospower                  , t, m_reflectCospow    , ivalid);
    pblock->GetValue(mtl_refl_roughness                 , t, m_reflectRough     , ivalid);
    pblock->GetValue(mtl_refl_ior                       , t, m_reflectIor       , ivalid);
    pblock->GetValue(mtl_refl_gloss                     , t, m_reflectGloss     , ivalid);
    pblock->GetValue(mtl_refl_gloss_or_cos              , t, m_reflectGlossOrCos, ivalid);
    pblock->GetValue(mtl_refl_fresnel_on                , t, m_reflectFresnel   , ivalid);
    pblock->GetValue(mtl_refl_extrusion                 , t, m_reflectExtrusion , ivalid);
    pblock->GetValue(mtl_refl_anisotr                   , t, m_reflectAnisotr   , ivalid);
    pblock->GetValue(mtl_refl_rotat                     , t, m_reflectAnisRotat , ivalid);

    pblock->GetValue(mtl_lock_specular                  , t, m_lockSpecular     , ivalid);

    pblock->GetValue(mtl_emission_color                 , t, m_emissionColor    , ivalid);
    pblock->GetValue(mtl_emission_tint_on               , t, m_emissionTint     , ivalid);
    pblock->GetValue(mtl_emission_mult                  , t, m_emissionMult     , ivalid);
    pblock->GetValue(mtl_emission_cast_gi               , t, m_emissionCastGi   , ivalid);

    pblock->GetValue(mtl_transpar_color                 , t, m_transpColor      , ivalid);
    pblock->GetValue(mtl_transpar_tint_on               , t, m_transpTint       , ivalid);
    pblock->GetValue(mtl_transpar_mult                  , t, m_transpMult       , ivalid);
    pblock->GetValue(mtl_transpar_thin_on               , t, m_transpThin       , ivalid);
    pblock->GetValue(mtl_transpar_cospower              , t, m_transpCospower   , ivalid);
    pblock->GetValue(mtl_transpar_ior                   , t, m_transpIor        , ivalid);
    pblock->GetValue(mtl_transpar_dist_color            , t, m_transpDistColor  , ivalid);
    pblock->GetValue(mtl_transpar_dist_mult             , t, m_transpDistMult   , ivalid);
    pblock->GetValue(mtl_exit_color                     , t, m_exitColor        , ivalid);
    pblock->GetValue(mtl_transpar_gloss                 , t, m_transpGloss      , ivalid);
    pblock->GetValue(mtl_transpar_gloss_or_cos          , t, m_transpGlossOrCos , ivalid);
    pblock->GetValue(mtl_opacity_smooth                 , t, m_opacitySmooth    , ivalid);

    pblock->GetValue(mtl_transluc_color                 , t, m_translucColor    , ivalid);
    pblock->GetValue(mtl_transluc_mult                  , t, m_translucMult     , ivalid);
    pblock->GetValue(mtl_transluc_tint_on               , t, m_translucTint     , ivalid);

    pblock->GetValue(mtl_relief_amount                  , t, m_reliefAmount     , ivalid);
    pblock->GetValue(mtl_displacement_on                , t, m_displacement     , ivalid);
    pblock->GetValue(mtl_displacement_height            , t, m_displaceHeight   , ivalid);
    pblock->GetValue(mtl_displacement_invert_height_on  , t, m_invertHeight     , ivalid);
    pblock->GetValue(mtl_relief_smooth                  , t, m_reliefSmooth     , ivalid);
    pblock->GetValue(mtl_bump_sigma                     , t, m_bumpSigma        , ivalid);
    

    NotifyDependents(FOREVER, PART_ALL, REFMSG_DISPLAY_MATERIAL_CHANGE);

    //for (int i=0; i < NSUBMTL; i++)
    //{
    //  if (submtl[i])
    //    submtl[i]->Update(t,ivalid);
    //}

    for (auto& i : subTexSlots)
    {
      if (i != nullptr)
        i->Update(t, ivalid);
    }

  }

  valid &= ivalid;
  //NotifyChanged();
  //ExportMaterialXML();
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/

void HydraMtl::SetAmbient(Color c, TimeValue t) {} // not used.
void HydraMtl::SetDiffuse(Color c, TimeValue t)
{
  m_diffuseColor = c;
  pblock->SetValue(mtl_diffuse_color, t, m_diffuseColor);
  NotifyChanged();
}
void HydraMtl::SetSpecular(Color c, TimeValue t)
{
  m_specularColor = c;
  pblock->SetValue(mtl_specular_color, t, m_specularColor);
  NotifyChanged();
}
void HydraMtl::SetShininess(float v, TimeValue t)
{
  m_specularRough = v;
  pblock->SetValue(mtl_refl_roughness, t, m_reflectRough);
  NotifyChanged();
}

Color HydraMtl::GetAmbient(int mtlNum, BOOL backFace)
{
  return GetDiffuse(mtlNum, backFace);
}

Color HydraMtl::GetDiffuse(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetDiffuse(mtlNum,backFace):Color(0,0,0);
  return m_diffuseColor * m_diffuseMult + m_reflectColor * m_reflectMult * 0.01F;
}

Color HydraMtl::GetSpecular(int mtlNum, BOOL backFace)
{
  //return submtl[0]?submtl[0]->GetSpecular(mtlNum,backFace):Color(0,0,0);  
  return m_reflectColor * m_reflectMult;
}

Color HydraMtl::GetSelfIllumColor(int mtlNum, BOOL backFace)
{
  Color res = m_emissionColor * m_emissionMult;

  res.r /= (1.0F + res.r);  // compress for viewport.
  res.g /= (1.0F + res.g);
  res.b /= (1.0F + res.b);

  return res;
}

float HydraMtl::GetXParency(int mtlNum, BOOL backFace)
{
  if (m_transpMult > 1.0F)
    m_transpMult = 1.0F;
  return m_transpMult * (0.2126F * m_transpColor.r + 0.7152F * m_transpColor.g + 0.0722F * m_transpColor.b) * 0.8F; // 0.8 - for visible obj in viewport with full transparency
}

float HydraMtl::GetShininess(int mtlNum, BOOL backFace) 
{
  return m_reflectGloss;
}

float HydraMtl::GetShinStr(int mtlNum, BOOL backFace)
{
  return 1.0F;
}

float HydraMtl::WireSize(int mtlNum, BOOL backFace)
{
  return 1.0F;
}

BOOL HydraMtl::GetTransparencyHint(TimeValue t, Interval & valid)
{
  const float trans = m_transpMult * (m_transpColor.r + m_transpColor.g + m_transpColor.b);
  return trans > 0.0F ? true : false;
}

ULONG HydraMtl::Requirements(int subMtlNum)
{
  // No two-sided to match Quicksilver, possibly transparent
  ULONG requirements = MTLREQ_TRANSP | MTLREQ_TRANSP_IN_VP | MTLREQ_UV;
  return requirements;
}

// from interface IHydraMtl 
// Diffuse
Texmap* HydraMtl::GetDiffuseTexmap()        { return subTexSlots[DIFFUSE_TEX_SLOT];             } 
Color   HydraMtl::GetDiffuseColor()         { return pblock->GetColor(mtl_diffuse_color);       }
float   HydraMtl::GetDiffuseMult()          { return pblock->GetFloat(mtl_diffuse_mult);        }
bool    HydraMtl::GetDiffuseTint()          { return pblock->GetInt  (mtl_diffuse_tint_on);     }
float   HydraMtl::GetDiffuseRoughness()     { return pblock->GetFloat(mtl_roughness_mult);      }

// Reflectivity
Texmap* HydraMtl::GetReflTexmap()           { return subTexSlots[REFLECT_TEX_SLOT];             }
Texmap* HydraMtl::GetReflGlossTexmap()      { return subTexSlots[REFL_GLOSS_TEX_SLOT];          }
Texmap* HydraMtl::GetReflAnisTexmap()       { return subTexSlots[REFL_ANISOTR_TEX_SLOT];        }
Texmap* HydraMtl::GetReflAnisRotatTexmap()  { return subTexSlots[REFL_ROTATION_TEX_SLOT];       }
Color   HydraMtl::GetReflColor()            { return pblock->GetColor(mtl_refl_color);          }
bool    HydraMtl::GetReflTint()             { return pblock->GetInt  (mtl_refl_tint_on);        }
float   HydraMtl::GetReflMult()             { return pblock->GetFloat(mtl_refl_mult);           }
float   HydraMtl::GetReflGloss()            { return pblock->GetFloat(mtl_refl_gloss);          }
int     HydraMtl::GetReflBRDF()             { return pblock->GetInt  (mtl_refl_brdf);           }
float   HydraMtl::GetReflIor()              { return pblock->GetFloat(mtl_refl_ior);            }
bool    HydraMtl::GetReflFresnel()          { return pblock->GetInt  (mtl_refl_fresnel_on);     }
int     HydraMtl::GetReflExtrus()           { return pblock->GetInt  (mtl_refl_extrusion);      }
float   HydraMtl::GetReflAnisotr()          { return pblock->GetFloat(mtl_refl_anisotr);        }
float   HydraMtl::GetReflAnisRotat()        { return pblock->GetFloat(mtl_refl_rotat);          }

// Transparency
Texmap* HydraMtl::GetTranspTexmap()         { return subTexSlots[TRANSP_TEX_SLOT];              }
Texmap* HydraMtl::GetTranspGlossTexmap()    { return subTexSlots[TRANSP_GLOSS_TEX_SLOT];        }
Color   HydraMtl::GetTranspColor()          { return pblock->GetColor(mtl_transpar_color);      }
float   HydraMtl::GetTranspMult()           { return pblock->GetFloat(mtl_transpar_mult);       }
bool    HydraMtl::GetTranspTint()           { return pblock->GetInt  (mtl_transpar_tint_on);    }
float   HydraMtl::GetTranspGloss()          { return pblock->GetFloat(mtl_transpar_gloss);      }
float   HydraMtl::GetTranspIor()            { return pblock->GetFloat(mtl_transpar_ior);        }
float   HydraMtl::GetTranspDistMult()       { return pblock->GetFloat(mtl_transpar_dist_mult);  }
Color   HydraMtl::GetTranspDistColor()      { return pblock->GetColor(mtl_transpar_dist_color); }
bool    HydraMtl::GetTranspThin()           { return pblock->GetInt  (mtl_transpar_thin_on);    }

// Opacity/Special
Texmap* HydraMtl::GetOpacityTexmap()        { return subTexSlots[OPACITY_TEX_SLOT];             }
bool    HydraMtl::GetAffectShadow()         { return pblock->GetInt  (mtl_affect_shadow);       }
bool    HydraMtl::GetOpacitySmooth()        { return pblock->GetInt  (mtl_opacity_smooth);      }
bool    HydraMtl::HasOpacity()              { return (GetOpacityTexmap() != nullptr);           }

// Emission
Texmap* HydraMtl::GetEmissionTexmap()       { return subTexSlots[EMISSION_TEX_SLOT];            }
Color   HydraMtl::GetEmissionColor()        { return pblock->GetColor(mtl_emission_color);      }
float   HydraMtl::GetEmissionMult()         { return pblock->GetFloat(mtl_emission_mult);       }
bool    HydraMtl::GetEmissionTint()         { return pblock->GetInt  (mtl_emission_tint_on);    }
bool    HydraMtl::GetEmissionCastGi()       { return pblock->GetInt  (mtl_emission_cast_gi);    }

// Translucency
Texmap* HydraMtl::GetTranslucTexmap()       { return subTexSlots[TRANSLUCENCY_TEX_SLOT];        }
float   HydraMtl::GetTranslucMult()         { return pblock->GetFloat(mtl_transluc_mult);       }
Color   HydraMtl::GetTranslucColor()        { return pblock->GetColor(mtl_transluc_color);      }
bool    HydraMtl::GetTranslucTint()         { return pblock->GetInt  (mtl_transluc_tint_on);    }

// Relief
Texmap* HydraMtl::GetReliefTexmap()         { return subTexSlots[RELIEF_TEX_SLOT];              }
float   HydraMtl::GetReliefAmount()         { return pblock->GetFloat(mtl_relief_amount);       }
float   HydraMtl::GetReliefSmooth()         { return pblock->GetFloat(mtl_relief_smooth);       }



FPInterface * hydraMaterialClassDesc::GetInterface(Interface_ID id)
{
  if (id == IMATERIAL_BROWSER_ENTRY_INFO_INTERFACE)
    return static_cast<IMaterialBrowserEntryInfo*>(this);
  else
    return ClassDesc2::GetInterface(id);
}

const MCHAR * hydraMaterialClassDesc::GetEntryName() const
{
  return const_cast<hydraMaterialClassDesc*>(this)->ClassName();  
}

const MCHAR * hydraMaterialClassDesc::GetEntryCategory() const
{
  static const MSTR str = L"Materials\\Hydra";
  return str;
}

Bitmap * hydraMaterialClassDesc::GetEntryThumbnail() const noexcept
{
  return nullptr;
}

