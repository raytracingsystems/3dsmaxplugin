/*****************************************************************************^
Copyright Ray Tracing Systems. Hydra Renderer. 2020.
******************************************************************************/

string ParamID = "0x003"; //use DXSAS compiler

// float Script : STANDARDSGLOBAL <
    // string UIWidget     = "none";
    // string ScriptClass  = "object";
    // string ScriptOrder  = "standard";
    // string ScriptOutput = "color";
    // string Script       = "Technique=T0;";
// > = 0.8;

////////////////// Global constants //////////////////

#define INV_TWOPI  0.15915494f
#define HALF_PI    1.57079632f
#define TWO_PI     6.28318530f
#define PI         3.14159265f
#define INV_PI     0.31830988f
#define INV_HALFPI 0.15915494f

////////////////// UN-TWEAKABLES - AUTOMATICALLY-TRACKED TRANSFORMS //////////////////
cbuffer UpdatePerFrame
{
  float4x4 ViewIXf            : VIEWINVERSE               < string UIWidget = "None"; >;	
	float4x4 view				        : VIEW                      < string UIWidget = "None"; >;
	float4x4 prj				        : PROJECTION                < string UIWidget = "None"; >;
	float4x4 viewPrj			      : VIEWPROJECTION            < string UIWidget = "None"; >;
	float4x4 worldViewInvTrans	: WORLDVIEWINVERSETRANSPOSE < string UIWidget = "None"; >;
  
  float g_masterScaler; // scale system unit 3ds max.
}

cbuffer UpdatePerObject
{	
  float4x4 WorldXf   : WORLD                  < string UIWidget = "None"; >;
	float4x4 WorldView : WORLDVIEW              < string UIWidget = "None"; >;
  float4x4 WorldITXf : WORLDINVERSETRANSPOSE  < string UIWidget = "None"; >;
  float4x4 WvpXf     : WORLDVIEWPROJECTION    < string UIWidget = "None"; >;
} 



////////////////// Texture coordinates //////////////////

int texcoord0 : TEXCOORD
<
	int Texcoord    = 0;
	int MapChannel  = 1;
>;



////////////////// Environment //////////////////   

bool     g_hasEnvirMap;
float3   g_envColor;
float3   g_globLightTint;
float3   g_envAmbColor;
Texture2D <float3> g_envirMap;// : REFLECTIONMAP;
float4x4 g_envUVmatrix44;

////////////////// Lights //////////////////   

//int maxLightSample;

bool   g_lightEnable;

float3 g_lightPos : POSITION;
// <    
  // string Space = "World";
  // int refID = 0; 								//refID for automatic lightcolor input
// > = {0, -100, 100};
     
float3 g_lightDir : DIRECTION;
// < 
  // int LightRef = 0;
// >;

float3 g_lightColor : LIGHTCOLOR;
// <  
  // int LightRef = 0;
// > = float3(0, 0, 0);
     
//float g_lightIntens;
float4 g_lightArea; // { Length, Width, Raduis, intens }
bool   g_lightHasDir;
int    g_lightType;



////////////////// Read from HydraMtl GUI //////////////////

// Diffuse
float     g_diffuseMult;
bool      g_hasDiffuseMap;
Texture2D <float3> g_diffuseMap : DIFFUSEMAP;
float3    g_diffuseColor;
float     g_diffuseRough;

// Reflect
float     g_reflectMult;
bool      g_hasReflectMap;
Texture2D <float3> g_reflectMap : SPECULARMAP;
float3    g_reflectColor;
float     g_reflectIOR;
float     g_reflectGloss;
bool      g_hasReflGlosMap;
Texture2D <float3> g_reflectGlosMap;
int       g_reflectBrdf;
int       g_reflectExtrus;


// Transparency
float     g_transpMult;
bool      g_hasTranspMap;
Texture2D <float3> g_transpMap;
float3    g_transpColor;
float     g_transpGloss;
bool      g_hasTranspGlossMap;
Texture2D <float3> g_transpGlosMap;
float     g_transpIOR;


// Opacity.
bool g_hasOpacityMap;
Texture2D <float3> g_opacityMap : OPACITYMAP;
bool g_opacitySmooth;



// Translucency.
float     g_translucMult;
float3    g_translucColor;


// Emission.
float     g_emissionMult;
bool      g_hasEmissionMap;
Texture2D <float3> g_emissionMap;
float3    g_emissionColor;


// Relief. 
float g_bumpAmount;
bool g_hasReliefMap;
Texture2D <float3> g_ReliefMap : NORMALMAP; // BUMPMAP or NORMALMAP 


////////////////// Samplers //////////////////

SamplerState g_diffuseSampler
{  
  Filter   = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
};

SamplerState g_reflectSampler
{  
  Filter   = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
};

SamplerState g_reflGlossSampler
{  
  Filter   = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
};

SamplerState g_transpSampler
{  
  Filter   = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
};

SamplerState g_reliefSampler
{  
  Filter   = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
};

SamplerState g_envirSampler
{  
  Filter   = MIN_MAG_MIP_LINEAR;
  AddressU = Wrap;
  AddressV = Wrap;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////// Functions //////////////////                                                             

// Random
struct RandomGen
{
  int2 state;
};

uint NextState(inout RandomGen gen)
{
  const uint x = (gen.state).x * 17 + (gen.state).y * 13123;
  (gen.state).x = (x << 13) ^ x;
  (gen.state).y ^= (x << 7);
  return x;
}

float2 rndFloat2_Pseudo(RandomGen gen)
{
  uint x = NextState(gen); 

  const uint x1 = (x * (x * x * 15731 + 74323) + 871483);
  const uint y1 = (x * (x * x * 13734 + 37828) + 234234);

  const float scale     = (1.0f / 4294967296.0f);

  return float2((float)(x1), (float)(y1))*scale;
}


float2 SphereMapTo2DTexCoord(const float3 a_rayDir) 
{ 
  const float theta = acos(a_rayDir.z);               // [0,pi] 
  float phi         = atan2(a_rayDir.y, a_rayDir.x);  // [-pi,pi]
  
  if (phi < 0.0f)
    phi += TWO_PI;                  // [-pi,pi] --> [0, 2*pi];  see PBRT.

  
  const float2 angles = float2(phi, theta);

  const float texX = saturate(angles.x * INV_HALFPI);
  const float texY = saturate(angles.y * INV_PI);
  
  return float2(texX, texY);
}


float Luminance(const float3 data) { return dot(data, float3(0.2126f, 0.7152f, 0.0722f)); }

float glossCoeff[10][4] = {
    { 8.88178419700125e-14f, -1.77635683940025e-14f, 5.0f             , 1.0f    }, // 0-0.1
    { 357.142857142857f    , -35.7142857142857f    , 5.0f             , 1.5f    }, // 0.1-0.2
    { -2142.85714285714f   , 428.571428571429f     , 8.57142857142857f, 2.0f    }, // 0.2-0.3
    { 428.571428571431f    , -42.8571428571432f    , 30.0f            , 5.0f    }, // 0.3-0.4
    { 2095.23809523810f    , -152.380952380952f    , 34.2857142857143f, 8.0f    }, // 0.4-0.5
    { -4761.90476190476f   , 1809.52380952381f     , 66.6666666666667f, 12.0f   }, // 0.5-0.6
    { 9914.71215351811f    , 1151.38592750533f     , 285.714285714286f, 32.0f   }, // 0.6-0.7
    { 45037.7068059246f    , 9161.90096119855f     , 813.432835820895f, 82.0f   }, // 0.7-0.8
    { 167903.678757035f    , 183240.189801913f     , 3996.94423223835f, 300.0f  }, // 0.8-0.9
    { -20281790.7444668f   , 6301358.14889336f     , 45682.0925553320f, 2700.0f }  // 0.9-1.0
};


float CosPowerFromGlosiness(const float glosiness)
{
  const float cMax = 1000000.0f;
  float x          = glosiness;
  int k            = (abs(x - 1.0f) < 1e-5f) ? 10 : (int)(x * 10.0f);
  const float x1   = (x - (float)(k) * 0.1f);

  if (k == 10 || x >= 0.99f) 
	return cMax;
  else             
	return glossCoeff[k][3] + glossCoeff[k][2] * x1 + glossCoeff[k][1] * x1*x1 + glossCoeff[k][0] * x1*x1*x1;
}


float fresnelDielectric(const float cosTheta1, const float cosTheta2, const float etaExt, const float etaInt)
{
  float Rs = (etaExt * cosTheta1 - etaInt * cosTheta2) / (etaExt * cosTheta1 + etaInt * cosTheta2);
  float Rp = (etaInt * cosTheta1 - etaExt * cosTheta2) / (etaInt * cosTheta1 + etaExt * cosTheta2);

  return (Rs * Rs + Rp * Rp) / 2.0f;
}

float fresnelReflectionCoeff(const float cosTheta1, float etaExt, float etaInt)
{
  // Swap the indices of refraction if the interaction starts
  // at the inside of the object
  //
  if (cosTheta1 < 0.0f)
  {
    float temp = etaInt;
    etaInt = etaExt;
    etaExt = temp;
  }

  // Using Snell's law, calculate the sine of the angle
  // between the transmitted ray and the surface normal 
  //
  float sinTheta2 = etaExt / etaInt * sqrt(max(0.0f, 1.0f - cosTheta1*cosTheta1));

  if (sinTheta2 > 1.0f)
    return 1.0f;  // Total internal reflection!

  // Use the sin^2+cos^2=1 identity - max() guards against
  //	numerical imprecision
  //
  float cosTheta2 = sqrt(max(0.0f, 1.0f - sinTheta2*sinTheta2));

  // Finally compute the reflection coefficient
  //
  return fresnelDielectric(abs(cosTheta1), cosTheta2, etaInt, etaExt);
}

float fresnelReflectionCoeffMentalLike(const float cosTheta, const float refractIOR)
{
  return fresnelReflectionCoeff(abs(cosTheta), 1.0f, refractIOR);
}

// BxDFs

void CoordinateSystem(const float3 v1, inout float3 v2, inout float3 v3)
{
  float invLen = 1.0f;

  if (abs(v1.x) > abs(v1.y))
  {
    invLen = 1.0f / sqrt(v1.x*v1.x + v1.z*v1.z);
    v2     = float3(-v1.z * invLen, 0.0f, v1.x * invLen);
  }
  else
  {
    invLen = 1.0f / sqrt(v1.y*v1.y + v1.z*v1.z);
    v2     = float3(0.0f, v1.z * invLen, -v1.y * invLen);
  }

  v3 = cross(v1, v2);
}

float SinPhiPBRT(const float3 w, const float sintheta)
{
  if (sintheta == 0.0f) return 0.0f;
  else                  return clamp(w.y / sintheta, -1.0f, 1.0f);
}

float CosPhiPBRT(const float3 w, const float sintheta)
{
  if (sintheta == 0.0f) return 1.0f;
  else                  return clamp(w.x / sintheta, -1.0f, 1.0f);
}

float OrenNayarBRDF(const float3 a_l, const float3 a_v, const float3 a_n, const float a_roughness)
{
  const float cosTheta_wi = dot(a_l, a_n);
  const float cosTheta_wo = dot(a_v, a_n);

  const float sinTheta_wi = sqrt(max(0.0f, 1.0f - cosTheta_wi * cosTheta_wi));
  const float sinTheta_wo = sqrt(max(0.0f, 1.0f - cosTheta_wo * cosTheta_wo));

  const float sigma       = a_roughness * HALF_PI; //Radians(sig)
  const float sigma2      = sigma * sigma;
  const float A           = 1.0f - (sigma2 / (2.0f * (sigma2 + 0.33f)));
  const float B           = 0.45f * sigma2 / (sigma2 + 0.09f);

  // to PBRT coordinate system
  // wo = a_v = -ray_dir
  // wi = a_l = newDir
  //
  float3 nx, ny = a_n, nz;
  CoordinateSystem(ny, nx, nz);
  
  {
    const float3 temp = ny;
    ny = nz;
    nz = temp;
  }

  const float3 wo = float3(-dot(a_v, nx), -dot(a_v, ny), -dot(a_v, nz));
  const float3 wi = float3(-dot(a_l, nx), -dot(a_l, ny), -dot(a_l, nz));
  //
  // to PBRT coordinate system

  // Compute cosine term of Oren-Nayar model
  float maxcos = 0.0f;

  if (sinTheta_wi > 1e-4 && sinTheta_wo > 1e-4)
  {
    const float sinphii = SinPhiPBRT(wi, sinTheta_wi);
    const float cosphii = CosPhiPBRT(wi, sinTheta_wi);
    const float sinphio = SinPhiPBRT(wo, sinTheta_wo);
    const float cosphio = CosPhiPBRT(wo, sinTheta_wo);
    const float dcos    = cosphii * cosphio + sinphii * sinphio;
    maxcos              = max(0.0f, dcos);
  }

  // Compute sine and tangent terms of Oren-Nayar model
  float sinalpha = 0.0f, tanbeta = 0.0f;

  if (abs(cosTheta_wi) > abs(cosTheta_wo))
  {
    sinalpha = sinTheta_wo;
    tanbeta  = sinTheta_wi / max(abs(cosTheta_wi), 0.0001f);
  }
  else
  {
    sinalpha = sinTheta_wi;
    tanbeta  = sinTheta_wo / max(abs(cosTheta_wo), 0.0001f);
  }

  return (A + B * maxcos * sinalpha * tanbeta);
}


float PhongEvalBRDF(const float a_cosPower, const float3 a_n, const float3 a_v, const float3 a_l, const float a_dotNL)
{  
  const float3 r         = reflect((-1.0f)*a_v, a_n);   
  const float  cosAlpha  = saturate(dot(a_l, r));   
  const float  energyFix = 1.0f / a_dotNL * cosAlpha; // the transfer of geometric cosine in the space of reflection.    
  
  return (a_cosPower + 2.0f) * INV_TWOPI * pow(cosAlpha, a_cosPower) * energyFix; // please see "Using the Modified Phong Reflectance Model for Physically ... 
}


float BlinnEvalBRDF(const float a_cosPower, const float3 a_n, const float3 a_v, const float3 a_l, const float a_dotNV, const float a_dotNL)
{
  const float3 h = normalize(a_v + a_l);    
  return pow(saturate(dot(a_n, h)), a_cosPower);
}


float3 GgxVndf(float3 a_wo, float a_roughness, float a_u1, float a_u2)
{
  // -- Stretch the view vector so we are sampling as though
  // -- a_roughness==1
  const float3 v = normalize(float3(a_wo.x * a_roughness, a_wo.y * a_roughness, a_wo.z));

  // -- Build an orthonormal basis with v, t1, and t2
  const float3 XAxis = float3(1.0f, 0.0f, 0.0f);
  const float3 ZAxis = float3(0.0f, 0.0f, 1.0f);
  const float3 t1 = (v.z < 0.999f) ? normalize(cross(v, ZAxis)) : XAxis;
  const float3 t2 = cross(t1, v);

  // -- Choose a point on a disk with each half of the disk weighted
  // -- proportionally to its projection onto direction v
  const float a = 1.0f / (1.0f + v.z);
  const float r = sqrt(a_u1);
  const float phi = (a_u2 < a) ? (a_u2 / a) * PI : PI + (a_u2 - a) / (1.0f - a) * PI;
  const float p1 = r * cos(phi);
  const float p2 = r * sin(phi) * ((a_u2 < a) ? 1.0f : v.z);

  // -- Calculate the normal in this stretched tangent space
  const float3 n = p1 * t1 + p2 * t2 + sqrt(max(0.0f, 1.0f - p1 * p1 - p2 * p2)) * v;

  // -- unstretch and normalize the normal
  return normalize(float3(a_roughness * n.x, a_roughness * n.y, max(0.0f, n.z)));
}

// GGX 2017
float SmithGGXMasking(const float a_dotNV, float a_roughSqr)
{
  const float denomC = sqrt(a_roughSqr + (1.0f - a_roughSqr) * a_dotNV * a_dotNV) + a_dotNV;
  return 2.0f * a_dotNV / max(denomC, 1e-6f);
}

float SmithGGXMaskingShadowing(const float a_dotNL, const float a_dotNV, float a_roughSqr)
{
  const float denomA = a_dotNV * sqrt(a_roughSqr + (1.0f - a_roughSqr) * a_dotNL * a_dotNL);
  const float denomB = a_dotNL * sqrt(a_roughSqr + (1.0f - a_roughSqr) * a_dotNV * a_dotNV);
  return 2.0f * a_dotNL * a_dotNV / max(denomA + denomB, 1e-6f);
}

float GGXSample2AndEvalBRDF(const float3 a_rayDir, const float3 a_normal, const float a_roughSqr, const float2 a_rands, inout float3 a_newDir)
{
  const float dotNV = dot(a_normal, a_rayDir * (-1.0f));

  float3 nx, ny, nz   = a_normal;
  CoordinateSystem(nz, nx, ny);

  const float3 wo     = float3(-dot(a_rayDir, nx), -dot(a_rayDir, ny), -dot(a_rayDir, nz));
  const float3 wh     = GgxVndf(wo, a_roughSqr, a_rands.x, a_rands.y);
  const float3 wi     = (2.0f * dot(wo, wh) * wh) - wo;                  // Compute incident direction by reflecting about wm  
  a_newDir            = normalize(wi.x * nx + wi.y * ny + wi.z * nz);    // back to a_normal coordinate system

  const float3 l      = a_newDir;
  const float dotNL   = dot(a_normal, l);

  if (dotNL < 1e-6f)
    return 0.0f;
  
  //const float F   = Fresnel is not needed here, because it is used for the blend with diffusion.    
  const float G1    = SmithGGXMasking(dotNV, a_roughSqr);
  const float G2    = SmithGGXMaskingShadowing(dotNL, dotNV, a_roughSqr);
  return G2 / max(G1, 1e-6f);    
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////// data from application vertex buffer //////////////////

struct appdata
{
  float4 Position : POSITION;
  float3 Normal	  : NORMAL;
  float3 Tangent  : TANGENT;
  float3 Binormal : BINORMAL;
  float2 UV0      : TEXCOORD0;	
};

////////////////// data passed from vertex shader to pixel shader //////////////////

struct vertexOutput
{
  float4 HPosition	   : SV_Position;
  float2 UV0		       : TEXCOORD0;
  // The following values are passed in "World" coordinates since
  // it tends to be the most flexible and easy for handling
  // reflections, sky lighting, and other "global" effects.
  float3 LightVec	     : TEXCOORD1;
  float3 WorldNormal   : TEXCOORD2;
  float3 WorldTangent  : TEXCOORD3;
  float3 WorldBinormal : TEXCOORD4;
  float3 WorldView	   : TEXCOORD5;
  float4 wPos		       : TEXCOORD6;
};


////////////////// VERTEX SHADING //////////////////

vertexOutput std_VS(appdata IN)
{
  vertexOutput OUT    = (vertexOutput)0;
					  
  OUT.WorldNormal     = mul(IN.Normal,   WorldITXf).xyz;
  OUT.WorldTangent    = mul(IN.Tangent,  WorldITXf).xyz;
  OUT.WorldBinormal   = mul(IN.Binormal, WorldITXf).xyz;

  // Mikkt support
  // To align with other applications, normalize normal/tangent/bitangent after transform to world space
  if (true)//(useMikkT)
  {
    OUT.WorldNormal   = normalize(OUT.WorldNormal);
    OUT.WorldTangent  = normalize(OUT.WorldTangent);
    OUT.WorldBinormal = normalize(OUT.WorldBinormal);
  }

  float4 Po           = float4(IN.Position.xyz, 1.0f);
  float3 Pw           = mul(Po, WorldXf).xyz;    
  OUT.HPosition       = mul(Po, WvpXf);
  OUT.LightVec        = g_lightPos     - Pw;
  OUT.WorldView       = ViewIXf[3].xyz - Pw;
  OUT.wPos            = mul(IN.Position, WorldXf);
 

// Pass through the UVs
  OUT.UV0 = IN.UV0;
     
  return OUT;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////// PIXEL SHADING //////////////////

float4 std_PS(vertexOutput IN): SV_Target
{  
  const float3 Vn = normalize(IN.WorldView);
  const float3 Ln = normalize(IN.LightVec);
  const float3 Tn = normalize(IN.WorldTangent);
  const float3 Bn = normalize(IN.WorldBinormal);
        float3 Nn = normalize(IN.WorldNormal);
  
  const float dotNLBeforeRelief = saturate(dot(Nn, Ln));
  
  ///// Relief.	
  if (g_hasReliefMap)
  {
    float3 bump = 2.0f * (g_ReliefMap.Sample(g_reliefSampler,IN.UV0) - 0.5f);
    
    // Flip green value because by default green means -y in the normal map generated by 3ds Max.
    bump.g = -bump.g;
    
    // if (g_FlipGreen)
    	// bump.g = -bump.g;
    
    // if (orthogonalizeTangentBitangentPerPixel)
    // {
    	// float3 bitangent = normalize(cross(Nn, Tn));
    	// Tn = normalize(cross(bitangent, Nn));
    	//Bitangent need to be flipped if the map face is flipped. We don't have map face handedness in shader so make
    	//the calculated bitangent point in the same direction as the interpolated bitangent which has considered the flip.
    	// Bn = sign(dot(bitangent, Bn)) * bitangent;
    // }
    
    Nn = (bump.x * Tn + bump.y * Bn) + bump.z * Nn; //g_bumpAmount * (bump.x * Tn + bump.y * Bn) + bump.z * Nn;
  }
  Nn = normalize(Nn);
  
  const float dotNV = saturate(dot(Nn, Vn));	  
  const float dotNL = saturate(dot(Nn, Ln));
  

  float3 outColor = float3(0, 0, 0);
	


  ////////// Diffuse.								
  
  float3 diffuse =  float3(0, 0, 0);  
  const float3 diffuseFinColor = saturate(g_diffuseMult * g_diffuseColor * (g_hasDiffuseMap ? g_diffuseMap.Sample(g_diffuseSampler, IN.UV0) : 1.0f)) * INV_PI;
  
  diffuse = diffuseFinColor;
  
  if (g_diffuseRough > 0.0f)
    diffuse *= OrenNayarBRDF(Ln, Vn, Nn, g_diffuseRough);   
  
  




  ////////// Translucency
  
  // float3 transluc   = saturate(g_translucMult * g_translucColor);
  // float  translucMask = max(transluc.r, max(transluc.g, transluc.b));  
  // float  dotInvNL    = dot(-Nn, Ln);  
      
	
	
  ////////// Reflectivity.
  
  float reflectGloss = 1.0f;
  float fresnel                = 1.0f;
  const float3 reflectFinColor = saturate(g_reflectMult * g_reflectColor * (g_hasReflectMap ? g_reflectMap.Sample(g_reflectSampler, IN.UV0) : 1.0f));
  float reflectCoeff           = Luminance(reflectFinColor);        
  
  if (reflectCoeff > 0.0001f && dotNV > 0.00001f)
  {      	
    const float3 reflectGlossColor = saturate(g_reflectGloss * (g_hasReflGlosMap ? g_reflectGlosMap.Sample(g_reflGlossSampler, IN.UV0) : 1.0f));
    reflectGloss = max(reflectGlossColor.r, max(reflectGlossColor.g, reflectGlossColor.b));
    
    // Extrusions.
    if (g_reflectExtrus == 0) 
      reflectCoeff = max(reflectFinColor.r, max(reflectFinColor.g, reflectFinColor.b));  // Extrusion: strong.
    
    
    // Has Fresnel.	
    if (g_reflectIOR < 101)
      fresnel      = fresnelReflectionCoeffMentalLike(dotNV, g_reflectIOR);             	

    // BRDF.
    float brdfReflects = 0.0f; 
    
    if (dotNLBeforeRelief > 0.00001f)
    {
      float cosPow = 1.0f;
      cosPow = CosPowerFromGlosiness(reflectGloss); // for PHONG.      	        
      
      if (g_reflectBrdf == 0)              
        brdfReflects = PhongEvalBRDF(cosPow, Nn, Vn, Ln, dotNL);
      else
        brdfReflects = BlinnEvalBRDF(cosPow, Nn, Vn, Ln, dotNV, dotNL);
    }	
	
    const float mask = saturate(reflectCoeff * fresnel); 		
    outColor         = diffuse * (1.0f - mask) + brdfReflects * reflectFinColor * fresnel;	
  }
  
  
  
  ////////// Attenuation light.
    
  if(g_lightEnable)
  {
    const float distToLight = length(IN.LightVec) * g_masterScaler;  
    const float attenuate   = 1.0f / (distToLight * distToLight);
	  const float colorLight  = g_lightArea.w * g_lightColor * attenuate;
    
    // Cutoff backsided area light.
    if (g_lightHasDir)        
      outColor *= dot(g_lightDir, Ln);     

    outColor *= step(0.0f, dotNLBeforeRelief);
    outColor *= colorLight * dotNL;
  }
  
  
  
  ////////// Diffuse environment light. 
      
  if (Luminance(diffuse) > 0.0001f)
  {
    if (g_hasEnvirMap)
    {
      float3 newDir      = Nn;
      newDir.xy          = newDir.yx;
      newDir.y          *= -1.0f;
      const float2 envUV = SphereMapTo2DTexCoord(newDir); 
      outColor          += diffuseFinColor * g_globLightTint * g_envirMap.SampleLevel(g_envirSampler, envUV, 8);     
    }
    else
      outColor          += diffuseFinColor * g_globLightTint * g_envColor;
  }
  

     
  
  
  ////////// Reflect environment.  
  
  if (reflectCoeff > 0.0001f)
  {
    if (g_hasEnvirMap)
    {
      const int maxSample  = 4;
      
      const float roughSqr = 1.0f - reflectGloss * reflectGloss;
      
      float3 reflRay       = reflect(Vn, Nn);  
      reflRay.yz           = -reflRay.zy;  
      reflRay              = reflRay.zxy;      
      
      RandomGen gen;
      
      float3 reflEnvColor = float3(0,0,0);
      
      // [fastopt] // not unroll loop.
      // for (int i = 0; i < maxSample; i++)
      // {
        // gen.state           = uint2(i, i + 10);    
        // const float2 rands  = rndFloat2_Pseudo(gen);    
        // float3 newDir       = reflRay;          
        // float brdfVal       = 1.0f;
        
        // if (roughSqr > 1e-6f)
        // {
          // brdfVal           = GGXSample2AndEvalBRDF(-Vn, Nn, roughSqr, rands, newDir);      
          // newDir.xy         = newDir.yx;
          // newDir.y         *= -1.0f;
        // }
        
        // float2 envUV         = SphereMapTo2DTexCoord(newDir);  
        // envUV                = mul(envUV, g_envUVmatrix44);      
        // reflEnvColor        += g_envirMap.Sample(g_envirSampler, envUV) * brdfVal;	
      // }
      
      // reflEnvColor /= (float)maxSample;
      
      // Glossy from mipmap levels.    
      const float2 envUV = SphereMapTo2DTexCoord(reflRay);        
      reflEnvColor       = g_envirMap.SampleLevel(g_envirSampler, envUV, (1.0f - pow(reflectGloss, 2.5)) * 8.75f);	    
      
      outColor          += g_globLightTint * reflEnvColor * reflectFinColor * fresnel;
    }
    else
      outColor          += g_globLightTint * g_envColor * reflectFinColor * fresnel;
  }
  
  

  ////////// Transparency.
      
  const float transp = saturate(g_transpMult * g_transpColor * (g_hasTranspMap ? g_transpMap.Sample(g_transpSampler, IN.UV0) : 1.0f));
    
  
  
  
  ////////// Emission.
  const float3 emission = saturate(g_emissionMult * g_emissionColor);
  
  
  ////////// Finish color.
  outColor = outColor + emission;// + diffuse * g_envAmbColor;    
  return float4(outColor, transp);    
}



////////////////// TECHNIQUES //////////////////

technique11 T0
{
  pass P0
  {	
    //SetBlendState(NULL); 
	
    SetVertexShader(CompileShader(vs_5_0, std_VS()));	
    SetGeometryShader(NULL);
    SetPixelShader(CompileShader(ps_5_0, std_PS()));     
  }
}
