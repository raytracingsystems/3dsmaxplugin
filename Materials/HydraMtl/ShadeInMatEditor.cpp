#include "HydraMtl.h"
#include <math.h> 
#include <cstdlib> // for random
#include "pbrtMath.h"


////////////////////////////////////////////////////////////////////////////////////////////////

float CosPowerFromGlosiness(const float glossiness) noexcept
{
  if (glossiness < 0.5F) return 0.495448F * powf(M_E, (4.865236F * glossiness + 0.780640F));
  else                   return 0.000575F * powf(M_E, 22.425084F * glossiness - 4.147466F) + 11.584066F;
}


void HydraMtl::Shade(ShadeContext& sc)
{  
  if (gbufID)
    sc.SetGBufferID(gbufID);

  sc.shadow                 = true;
  bool bumped               = false;
  Point3 tbnBasis[2]; //The bump basic vectors. [0] - U (tangent). [1] - V (bitangent).
  Point3 light;

  const Point3 geomNormal   = sc.Normal();
  Point3 normal             = geomNormal;

  const Color diffColor     = GetShadeDiffuseColor(sc);
  Color translucColor       = GetShadeTranslucencyColor(sc);
  const float translucCoeff = fmax(translucColor.r, fmax(translucColor.g, translucColor.b));

  Color resultDiffuse(0.0f, 0.0f, 0.0F);
  Color resultTransluc(0.0f, 0.0f, 0.0F);


  // Calculate diffuse and translucence for real lights
  for (int i = 0; i < sc.nLights; i++)
  {
    LightDesc* l = sc.Light(i);

    if (l != nullptr)
    {
      float NdotL     = 0.0F;
      float diffCoeff = 0.0F;
      Color lightColor(1.0f, 1.0f, 1.0F);

      if (subTexSlots[RELIEF_TEX_SLOT] != nullptr && sc.doMaps) // perturb normal if we have any such and move all vectors to tangent space
      {
        bumped = true;

        Point3 reliefNormal = subTexSlots[RELIEF_TEX_SLOT]->EvalNormalPerturb(sc);

        //invert height not supported yet.
        //if (m_invertHeight)
        //{        
        //  reliefNormal.x *= (-1.0F);
        //  reliefNormal.y *= (-1.0F);
        //}      

        const Point3 zero(0.0f, 0.0f, 0.0f);
        Lerp(reliefNormal, zero, 1.0f - m_reliefAmount / 5.0f);
        reliefNormal *= 5.0f; // looks like render and standart mat (bump amount 100).
        normal = Normalize(geomNormal + (sc.backFace ? -reliefNormal : reliefNormal));
        sc.SetNormal(normal); // for mirror reflect bump
      }

      l->Illuminate(sc, normal, lightColor, light, NdotL, diffCoeff);

      float nDotLMinus = 0.0F;

      if (NdotL < 0.0F)
      {
        nDotLMinus     = NdotL;
        NdotL          = 0.0F;
      }
      else
        translucColor = { 0, 0, 0 };

      // Diffuse
      if (m_diffuseRoughness > 0.0F)
        resultDiffuse += lightColor * diffColor * NdotL * DiffuseOrenNayar(light, -sc.OrigView(), normal, m_diffuseRoughness); // Oren-Nayar      
      else
        resultDiffuse += lightColor * diffColor * NdotL; // Lambert

      // Translucence
      resultTransluc += (lightColor * translucColor * abs(nDotLMinus));
      Lerp(resultDiffuse, resultTransluc, translucCoeff);
    }
  }


  //sc.DPdUVW(tbnBasis); //This returns the bump basis vectors (tangent, bitangent, normal) for UVW in camera space. 
  //sc.BumpBasisVectors(tbnBasis, AXIS_UV); 

  // Calculate approximate tangent and binormal from normal and view ray.
  const Point3 xAxis(1.0f, 0.0f, 0.0F);
  const float4x4 mRot             = RotateAroundVector4x4(xAxis, HALFPI);
  const Point3 rayTop             = mul3x3(mRot, sc.OrigView());
  tbnBasis[0]                     = CreateTangent(normal, rayTop);
  tbnBasis[1]                     = CrossProd(normal, tbnBasis[0]);

  sc.SetIOR(m_transpIor);

  Color bg;
  Color bgt;
  sc.GetBGColor(bg, bgt, false);

  const Color transpColor         = GetShadeTransparencyColor(sc);
  const float transparencyCoeff   = fmax(transpColor.r, fmax(transpColor.g, transpColor.b));
  const float opacityCoeff        = GetShadeOpacity(sc);

  const float glossReflect        = GetShadeReflectionGloss(sc);
  const float cosPowerReflect     = GetShadeReflectionCosPow(sc); // power of cosine in the original Phong model
  const float glossTransp         = GetShadeTransparGloss(sc);
  const float cosPowerTransp      = GetShadeTransparCosPow(sc); // power of cosine in the original Phong model

  const float reflectAnisotropy   = GetShadeAnisotropy(sc);
  const float reflectAnisRotate   = GetShadeAnisRotation(sc);

  // Extrusions
  const Color reflColor           = GetShadeReflectionColor(sc);
  float reflectCoeff;
  if (m_reflectExtrusion == 0)
    reflectCoeff = fmax(reflColor.r, fmax(reflColor.g, reflColor.b)); // strong
  else
    reflectCoeff = Luminance(Point3(reflColor));                            // luminance

  // Fresnel
  float fresnel = 1.0F;
  if (m_reflectFresnel)
  {
    const float dotVN = abs(DotProd(sc.OrigView(), sc.Normal()));
    fresnel           = FrCond(dotVN, m_reflectIor, 0.0F);
  }

  // Sampling. 
  int sample                    = 0;
  int differCount               = 0;
  constexpr int maxGoalCount    = 64;
  bool doSample                 = false;
  const bool hasGlossy          = glossReflect < 1.0F || glossTransp < 1.0F;
  float diffSample              = 0.0F;
  float currSample              = 0.0F;
  constexpr float error         = 1.0F / 256.0F;
  Color summColor(0.0F, 0.0F, 0.0F);

  // No need to parallelize here. 3d max apparently parallelize the rendering of the pixels themselves above.
  do
  {
    // Diffuse.
    sc.out.c = resultDiffuse;


    Color brdfReflects = Color(1, 1, 1);
    Color brdfRefracts = Color(1, 1, 1);

    if (texmapEnv != nullptr)
    {
      Point3 rayDir;
      const Point2 rnd2d = { SimpleRand1D(), fmax(SimpleRand1D(), 1e-6F) };

      // Refraction
      if (transparencyCoeff > 0.0F)
      {
        rayDir = sc.RefractVector(m_transpIor);

        if (glossTransp < 1.0F)
          PhongSampleAndEvalBRDF(brdfReflects, sc, rnd2d, cosPowerTransp, rayDir, true);

        brdfRefracts *= sc.EvalEnvironMap(texmapEnv, rayDir);
        Lerp(sc.out.c, (brdfRefracts * transpColor), transparencyCoeff);
      }
    
      // Reflectivity
      if (reflectCoeff > 0.0F)
      {
        rayDir = sc.OrigView();

        if (glossReflect < 1.0F)
        {
          switch (m_reflectBrdf)
          {
          case BRDF_PHONG:
            rayDir = sc.ReflectVector();
            PhongSampleAndEvalBRDF(brdfReflects, sc, rnd2d, cosPowerReflect, rayDir, false);
            break;
          case BRDF_TORRSPARR:
            BlinnSampleAndEvalBRDF(brdfReflects, rnd2d, cosPowerReflect, normal, rayDir);
            break;
          case BRDF_BECKMANN:
            BeckmannSampleAndEvalBRDF(brdfReflects, rnd2d, glossReflect, reflectAnisotropy, reflectAnisRotate, normal, tbnBasis, rayDir);
            break;
          case BRDF_GGX:
            GGXSample2AndEvalBRDF(brdfReflects, rnd2d, glossReflect, normal, rayDir, reflColor);
            break;
          case BRDF_TRGGX:
            TRGGXSampleAndEvalBRDF(brdfReflects, rnd2d, glossReflect, reflectAnisotropy, reflectAnisRotate, normal, tbnBasis, rayDir);
            break;
          default:
            break;
          }
        }
        else
          rayDir = sc.ReflectVector();

        brdfReflects *= sc.EvalEnvironMap(texmapEnv, rayDir);
        Lerp(sc.out.c, brdfReflects * reflColor, reflectCoeff * fresnel);
      }
    }

    
    // Opacity / Cutoff (mix with background). 
    Lerp(sc.out.c, bg, 1.0f - opacityCoeff);


    // calculate final color
    const float alpha = 1.0f / (sample + 1.0f);
    summColor         = summColor * (1.0f - alpha) + sc.out.c * alpha;
        

    // calculate error for stop sampling

    float nextSample     = pow(Luminance(summColor), 1.0f / 2.2f);    

    // simple tone-mapping.
    constexpr float knee    = 5.0f; // from 10 to 1. lower = softer.
    constexpr float invKnee = 1.0f / knee;
    nextSample /= pow((1.0f + pow(nextSample, knee)), invKnee);


    diffSample  = fabs(currSample - nextSample);     
    currSample  = nextSample;
    
    if (diffSample < error)
      differCount++;
    
    if (hasGlossy)
      doSample = differCount < maxGoalCount;

    sample++;
  } while (doSample && sample < MAX_SAMPLES); // end loop sampling.

  sc.out.c = summColor;

  // Emission.
  sc.out.c += GetShadeEmissiveColor(sc);


  // simple tone-mapping.
  constexpr float knee    = 5.0f; // from 10 to 1. lower = softer.
  constexpr float invKnee = 1.0f / knee;
  sc.out.c.r /= pow((1.0f + pow(sc.out.c.r, knee)), invKnee);
  sc.out.c.g /= pow((1.0f + pow(sc.out.c.g, knee)), invKnee);
  sc.out.c.b /= pow((1.0f + pow(sc.out.c.b, knee)), invKnee);
  

  if (bumped) sc.SetNormal(geomNormal); // restore normal
}


//-----------------------------------------------------------------------------------


float HydraMtl::EvalDisplacement(ShadeContext& sc)
{
  if (subTexSlots[RELIEF_TEX_SLOT] != nullptr && sc.doMaps)
  {
    TSTR className;
    subTexSlots[RELIEF_TEX_SLOT]->GetClassName(className);

    if (className == L"Normal Bump")
      return 0.0F;
    else
      return m_reliefAmount * subTexSlots[RELIEF_TEX_SLOT]->EvalMono(sc);
  }

  return 0.0F;
}

Interval HydraMtl::DisplacementValidity(TimeValue t)
{
  //Interval iv; iv.SetInfinite();
  //return iv;	
  return FOREVER;
}



Color HydraMtl::GetShadeDiffuseColor(ShadeContext &sc)
{
  // After being evaluated, if a map or material has a non-zero gbufID, 
  // it should call ShadeContext::SetGBuffer() to store it into 
  // the shade context.
  if (gbufID)
    sc.SetGBufferID(gbufID);

  Color res;

  if (subTexSlots[DIFFUSE_TEX_SLOT] != NULL && sc.doMaps)
  {
    const auto texColor = subTexSlots[DIFFUSE_TEX_SLOT]->EvalColor(sc);

    if (m_diffuseTint)
      res = texColor * m_diffuseColor*m_diffuseMult;
    else
      res = texColor * m_diffuseMult;
  }
  else
    res = m_diffuseColor * m_diffuseMult;

  res.ClampMinMax();
  return res;

}

Color HydraMtl::GetShadeSpecularColor(ShadeContext &sc)
{
  Color res;

  if (subTexSlots[SPECULAR_TEX_SLOT] != NULL && sc.doMaps)
  {
    const auto texColor = subTexSlots[SPECULAR_TEX_SLOT]->EvalColor(sc);

    if (m_specularTint)
      res = texColor * m_specularColor * m_specularMult;
    else
      res = texColor * m_specularMult;
  }
  else
    res = m_specularColor * m_specularMult;

  res.ClampMinMax();
  return res;
}

Color HydraMtl::GetShadeReflectionColor(ShadeContext &sc)
{
  Color res;

  if (subTexSlots[REFLECT_TEX_SLOT] != NULL && sc.doMaps)
  {
    const auto texColor = subTexSlots[REFLECT_TEX_SLOT]->EvalColor(sc);

    if (m_reflectTint)
      res = texColor * m_reflectColor * m_reflectMult;
    else
      res = texColor * m_reflectMult;
  }
  else
    res = m_reflectColor * m_reflectMult;

  res.ClampMinMax();
  return res;
}

Color HydraMtl::GetShadeEmissiveColor(ShadeContext &sc)
{
  Color res;

  if (subTexSlots[EMISSION_TEX_SLOT] != NULL && sc.doMaps)
  {
    const auto texColor = subTexSlots[EMISSION_TEX_SLOT]->EvalColor(sc);

    if (m_emissionTint)
      res = texColor * m_emissionColor * m_emissionMult;
    else
      res = texColor * m_emissionMult;
  }
  else
    res = m_emissionColor * m_emissionMult;

  return res;

}

Color HydraMtl::GetShadeTransparencyColor(ShadeContext &sc)
{
  Color fogTransparency(1, 1, 1);
  Color res;

  if (!m_transpThin)
    Lerp(fogTransparency, m_transpDistColor, (m_transpDistMult * 0.05f));

  if (subTexSlots[TRANSP_TEX_SLOT] != NULL && sc.doMaps)
  {
    Color texColor(1, 1, 1);
    texColor = subTexSlots[TRANSP_TEX_SLOT]->EvalColor(sc);

    if (m_transpTint)
      res = texColor * m_transpColor * m_transpMult * fogTransparency;
    else
      res = texColor * m_transpMult * fogTransparency;
  }
  else
    res = m_transpColor * m_transpMult * fogTransparency;

  res.ClampMinMax();
  return res;
}

float HydraMtl::GetShadeOpacity(ShadeContext & sc)
{
  if (subTexSlots[OPACITY_TEX_SLOT] != NULL && sc.doMaps)
  {
    auto texColor = subTexSlots[OPACITY_TEX_SLOT]->EvalColor(sc);

    texColor.ClampMinMax();

    float result = fmax(texColor.r, fmax(texColor.g, texColor.b));
        
    if (m_opacitySmooth)
      return result;
    else
    {
      if (result < 0.5f)
        return 0.0F;
      else
        return 1.0F;
    }
  }
  else
    return 1.0F;
}

Color HydraMtl::GetShadeTranslucencyColor(ShadeContext & sc)
{
  Color res;

  if (subTexSlots[TRANSLUCENCY_TEX_SLOT] != NULL && sc.doMaps)
  {
    const auto texColor = subTexSlots[TRANSLUCENCY_TEX_SLOT]->EvalColor(sc);

    if (m_translucTint)
      res = texColor * m_translucColor * m_translucMult;
    else
      res = texColor * m_translucMult;
  }
  else
    res = m_translucColor * m_translucMult;

  res.ClampMinMax();
  return res;
}



float HydraMtl::GetShadeSpecularCosPow(ShadeContext &sc)
{
  if (m_specGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[SPEC_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_specularGloss * (subTexSlots[SPEC_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_specularGloss;

    return CosPowerFromGlosiness(glosiness);
  }
  else
    return m_specularCospow;
}

float HydraMtl::GetShadeReflectionCosPow(ShadeContext &sc)
{
  if (m_reflectGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[REFL_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_reflectGloss * (subTexSlots[REFL_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_reflectGloss;

    return CosPowerFromGlosiness(glosiness);
  }
  else
    return m_reflectCospow;
}

float HydraMtl::GetShadeReflectionGloss(ShadeContext &sc)
{
  if (m_reflectGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[REFL_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_reflectGloss * (subTexSlots[REFL_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_reflectGloss;

    Clamp01(glosiness);

    return glosiness;
  }
  else
    return m_reflectCospow;
}

float HydraMtl::GetShadeTransparCosPow(ShadeContext &sc)
{
  if (m_transpGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[TRANSP_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_transpGloss * (subTexSlots[TRANSP_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_transpGloss;

    return CosPowerFromGlosiness(glosiness);
  }
  else
    return m_transpCospower;
}

float HydraMtl::GetShadeTransparGloss(ShadeContext &sc)
{
  if (m_transpGlossOrCos == 0)
  {
    float glosiness;

    if (subTexSlots[TRANSP_GLOSS_TEX_SLOT] != NULL && sc.doMaps)
      glosiness = m_transpGloss * (subTexSlots[TRANSP_GLOSS_TEX_SLOT]->EvalMono(sc));
    else
      glosiness = m_transpGloss;

    Clamp01(glosiness);

    return glosiness;
  }
  else
    return m_transpCospower;
}

float HydraMtl::GetShadeAnisotropy(ShadeContext &sc)
{
  float anisotropy;

  if (subTexSlots[REFL_ANISOTR_TEX_SLOT] != NULL && sc.doMaps)
    anisotropy = m_reflectAnisotr * (subTexSlots[REFL_ANISOTR_TEX_SLOT]->EvalMono(sc));
  else
    anisotropy = m_reflectAnisotr;

  Clamp01(anisotropy);

  return anisotropy;
}

float HydraMtl::GetShadeAnisRotation(ShadeContext &sc)
{
  float anisRot;

  if (subTexSlots[REFL_ROTATION_TEX_SLOT] != NULL && sc.doMaps)
    anisRot = subTexSlots[REFL_ROTATION_TEX_SLOT]->EvalMono(sc);
  else
    anisRot = m_reflectAnisRotat / 360.0f;

  Clamp01(anisRot);

  return anisRot;
}



////////////////////////////////////////////////////////////////////////////////////////////
// BRDFs
////////////////////////////////////////////////////////////////////////////////////////////

// Oren-Nayar
float HydraMtl::DiffuseOrenNayar(const Point3 a_l, const Point3 a_v, const Point3 a_normal, 
  const float a_roughness)
{
  const float cosTheta_wi = DotProd(a_l, a_normal);
  const float cosTheta_wo = DotProd(a_v, a_normal);

  const float sinTheta_wi = sqrt(fmax(0.0f, 1.0f - cosTheta_wi * cosTheta_wi));
  const float sinTheta_wo = sqrt(fmax(0.0f, 1.0f - cosTheta_wo * cosTheta_wo));

  const float sigma       = a_roughness * HALFPI; //Radians(sig)
  const float sigma2      = sigma * sigma;
  const float A           = 1.0f - (sigma2 / (2.0f * (sigma2 + 0.33f)));
  const float B           = 0.45f * sigma2 / (sigma2 + 0.09f);

  ///////////////////////////////////////////////////////////////////////////// to PBRT coordinate system
  // wo = a_v = -rayDir
  // wi = a_l = newDir
  //
  Point3 nx, ny = a_normal, nz;
  CoordinateSystem(ny, nx, nz);
  {
    const Point3 temp = ny;
    ny = nz;
    nz = temp;
  }

  ///////////////////////////////////////////////////////////////////////////// to PBRT coordinate system

  // Compute cosine term of Oren-Nayar model
  float maxcos = 0.0F;

  if (sinTheta_wi > 1e-4 && sinTheta_wo > 1e-4)
  {
    const Point3 wo     = Point3(-DotProd(a_v, nx), -DotProd(a_v, ny), -DotProd(a_v, nz));
    const Point3 wi     = Point3(-DotProd(a_l, nx), -DotProd(a_l, ny), -DotProd(a_l, nz));
    const float sinphii = SinPhiPBRT(wi, sinTheta_wi);
    const float cosphii = CosPhiPBRT(wi, sinTheta_wi);
    const float sinphio = SinPhiPBRT(wo, sinTheta_wo);
    const float cosphio = CosPhiPBRT(wo, sinTheta_wo);
    const float dcos    = cosphii * cosphio + sinphii * sinphio;
    maxcos              = fmax(0.0F, dcos);
  }

  // Compute sine and tangent terms of Oren-Nayar model
  float sinalpha = 0.0F, tanbeta = 0.0F;

  if (fabs(cosTheta_wi) > fabs(cosTheta_wo))
  {
    sinalpha = sinTheta_wo;
    tanbeta  = sinTheta_wi / fmax(fabs(cosTheta_wi), DEPSILON);
  }
  else
  {
    sinalpha = sinTheta_wi;
    tanbeta  = sinTheta_wo / fmax(fabs(cosTheta_wo), DEPSILON);
  }

  return (A + B * maxcos * sinalpha * tanbeta);
}


// Phong
void HydraMtl::PhongSampleAndEvalBRDF(Color& a_resultReflects, ShadeContext& sc, const Point2 rnd2d,
  const float cosPower, Point3& a_rayDir, const bool a_refraction)
{
  const float f         = TWOPI * rnd2d.x;
  const float sinTheta = sqrt(1.0f - pow(rnd2d.y, 2.0f / (cosPower + 1.0F)));

  float3 deviation;
  deviation.x = sinTheta * cos(f);
  deviation.y = sinTheta * sin(f);
  deviation.z = pow(rnd2d.y, 1.0f / (cosPower + 1.0F));

  //Generation of tangent and bitangent spaces to align the hemisphere to normal.      
  //x - horiz, y - vert, z - far
  Point3 nx, nz;
  CoordinateSystem(a_rayDir, nx, nz);

  const Point3 newDir  = Normalize(nx * deviation.x + a_rayDir * deviation.z + nz * deviation.y); // deviation.z <-> deviation.y !!!

  const Point3 normal  = a_refraction ? -sc.Normal() : sc.Normal();
  const Point3 viewDir = a_refraction ? sc.OrigView() : -sc.OrigView();

  const float dotNL    = DotProd(normal, newDir);
  const float dotNV    = DotProd(normal, viewDir);

  if (dotNV < 1e-6f || dotNL < 1e-6f)
    a_resultReflects = { 0.0f, 0.0f, 0.0f };
  else
  {
    const float cosAlpha                  = DotProd(newDir, a_rayDir);
    const float powCosAlphaCosPow         = pow(cosAlpha, cosPower);
    const float powCosAlphaCosPowInvTwoPi = powCosAlphaCosPow * INV_TWOPI;
    const float pdf                       = fmax(powCosAlphaCosPowInvTwoPi * (cosPower + 1.0F), 1e-6F);
    const float brdf                      = powCosAlphaCosPowInvTwoPi * (cosPower + 2.0f);

    a_resultReflects                      = brdf / pdf * cosAlpha * Color(1, 1, 1);
    a_rayDir                              = newDir;
  }
}


// Torrance-Sparrow with Blinn distribution.
void HydraMtl::BlinnSampleAndEvalBRDF(Color& a_resultReflects, const Point2 rnd2d, 
  const float cosPower, const Point3 a_normal, Point3& a_rayDir)
{
  // wo = v = rayDir
  // wi = l = -newDir

  Point3 nx, ny = a_normal, nz;
  CoordinateSystem(ny, nx, nz);
  {
    Point3 temp = ny;
    ny          = nz;
    nz          = temp;
  }
  const Point3 wo      = Point3(-DotProd(a_rayDir, nx), -DotProd(a_rayDir, ny), -DotProd(a_rayDir, nz));

  ///////////////////////////////////////////////////////////////////////////// to PBRT coordinate system

  const float exponent = cosPower;

  const float costheta = pow(rnd2d.y, 1.0f / (exponent + 1.0F));
  const float sintheta = sqrt(fmax(0.0f, 1.0f - costheta * costheta));
  const float phi      = TWOPI * rnd2d.x;

  // Compute sampled half-angle vector wh for Blinn distribution
  const Point3 wh      = SphericalDirection(sintheta, costheta, phi);
  const Point3 wi      = (2.0f * DotProd(wo, wh) * wh) - wo;            // Compute incident direction by reflecting about wh
  const Point3 newDir  = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL    = DotProd(a_normal, newDir);
  const float dotNV    = DotProd(a_normal, -a_rayDir);

  if (dotNV < 1e-6f || dotNL < 1e-6f)
    a_resultReflects = { 0.0f, 0.0f, 0.0f };
  else
  {
    const float D      = ((exponent + 2.0f) * INV_TWOPI * pow(costheta, exponent));
    const float GF1    = TorranceSparrowGF1(wo, wi);
    const float pdf    = ((exponent + 1.0F) * pow(costheta, exponent)) / fmax(TWOPI * 4.0f * DotProd(wo, wh), 1e-6F);

    a_resultReflects     = dotNL * D * GF1 / pdf * Color(1, 1, 1);
    a_rayDir           = newDir;
  }
}


void HydraMtl::BeckmannSampleAndEvalBRDF(Color& a_resultReflects, const Point2 rnd2d,
  const float gloss, const float anisotropy, const float anisRotation, const Point3 a_normal,
  const Point3 tbBasis[2], Point3& a_rayDir)
{
  const float roughness = 0.5f - 0.5f*gloss;
  const float anisoMult = 1.0f - anisotropy;
  const Point2 alpha(BeckmannRoughnessToAlpha(roughness*roughness), BeckmannRoughnessToAlpha(roughness*roughness*anisoMult*anisoMult));

  Point3 nx, ny = a_normal, nz;

  // wo = v = rayDir
  // wi = l = -newDir

  if (fabs(alpha.x - alpha.y) > 1.0e-5f)
  {
    nx = tbBasis[1]; //a_bitan;
    nz = tbBasis[0]; //a_tan;

    const float4x4 mRot = RotateAroundVector4x4(ny, anisRotation * TWOPI);
    nx = mul3x3(mRot, nx);
    nz = mul3x3(mRot, nz);
  }
  else
    CoordinateSystem(ny, nx, nz);

  const Point3 temp    = ny;
  ny                   = nz;
  nz                   = temp;

  const Point3 wo      = Point3(-DotProd(a_rayDir, nx), -DotProd(a_rayDir, ny), -DotProd(a_rayDir, nz));
  const Point3 wh      = BeckmannDistributionSampleWH(wo, Point2(rnd2d.x, rnd2d.y), alpha.x, alpha.y);
  const Point3 wi      = (2.0f * DotProd(wo, wh) * wh) - wo; // Compute incident direction by reflecting about wh
  const Point3 newDir  = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL    = DotProd(a_normal, newDir);
  const float dotNV    = DotProd(a_normal, -a_rayDir);

  if (dotNV < 1e-6f || dotNL < 1e-6f)
    a_resultReflects = { 0.0f, 0.0f, 0.0f };
  else
  {
    const float pdf    = fmaxf(BeckmannDistributionPdf(wo, wh, alpha.x, alpha.y), DEPSILON);
    a_resultReflects   = dotNL * fmin(BeckmannBRDF_PBRT(wo, wi, alpha.x, alpha.y), 500.0f) / pdf * Color(1, 1, 1);
    a_rayDir           = newDir;
  }
}



static float GGX_GeomShadMask(const float cosThetaN, const float alpha)
{
  const float cosTheta_sqr = Clamp01_2(cosThetaN*cosThetaN);
  const float tan2         = (1.0f - cosTheta_sqr) / fmax(cosTheta_sqr, 1e-6F);
  const float GP           = 2.0f / (1.0f + sqrt(1.0f + alpha * alpha * tan2));
  return GP;
}

static float GGX_Distribution(const float cosThetaNH, const float alpha)
{
  const float alpha2 = alpha * alpha;
  const float NH_sqr = Clamp01_2(cosThetaNH * cosThetaNH);
  const float den    = NH_sqr * alpha2 + (1.0f - NH_sqr);
  return alpha2 / fmax(PI * den * den, 1e-6F);
}


void HydraMtl::GGXSampleAndEvalBRDF(Color& a_resultReflects, const Point2 rnd2d, const float gloss,
  const Point3 a_normal, Point3& a_rayDir)
{
  const float  roughness = 1.0f - gloss;
  const float  roughSqr  = roughness * roughness;

  Point3 nx, ny, nz      = a_normal;

  CoordinateSystem(nz, nx, ny);

  // wo = v = rayDir
  // wi = l = -newDir

  // to PBRT coordinate system
  const Point3 wo       = Point3(-DotProd(a_rayDir, nx), -DotProd(a_rayDir, ny), -DotProd(a_rayDir, nz));

  // Compute sampled half-angle vector wh
  const float phi       = rnd2d.x * TWOPI;
  const float cosTheta  = Clamp01_2(sqrt((1.0f - rnd2d.y) / (1.0f + roughSqr * roughSqr * rnd2d.y - rnd2d.y)));
  const float sinTheta  = sqrt(1.0f - cosTheta * cosTheta);
  Point3 wh             = SphericalDirection(sinTheta, cosTheta, phi);
  const Point3 wi       = (2.0f * DotProd(wo, wh) * wh) - wo;              // Compute incident direction by reflecting about wh    
  const Point3 newDir   = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);          // back to normal coordinate system

  const Point3 v        = (-1.0F) * a_rayDir;
  const float dotNL     = DotProd(a_normal, newDir);  
  const float dotNV     = DotProd(a_normal, v);

  if (dotNV < 1e-6f || dotNL < 1e-6f)
    a_resultReflects = { 0.0f, 0.0f, 0.0f };
  else
  {
    const Point3 h      = Normalize(v + newDir); // half vector.
    const float dotNH   = DotProd(a_normal, h);
    const float dotHV   = DotProd(h, v);

    //const float F     = Fresnel is not needed here, because it is used for the blend with diffusion.
    const float D       = GGX_Distribution(dotNH, roughSqr);
    const float G       = GGX_GeomShadMask(dotNV, roughSqr) * GGX_GeomShadMask(dotNL, roughSqr);

    const float pdf     = D * dotNH / fmax(4.0f * dotHV, 1e-6F);
    const float bxdfVal = D /** F*/ * G / fmax(4.0f * dotNV, 1e-6F); // removing dotNL from the denominator (4.0f*dotNV*dotNL), since there is a division by it at the end.

    a_resultReflects    = bxdfVal / pdf * Color(1, 1, 1);
    a_rayDir            = newDir;
  }
}

///////////////////////////////////////////
// GGX sample v.2
// https://hal.archives-ouvertes.fr/hal-01509746/document
// https://schuttejoe.github.io/post/ggximportancesamplingpart2/
///////////////////////////////////////////

float SmithGGXMasking(const float dotNV, float roughSqr)
{
  const float denomC = sqrt(roughSqr + (1.0f - roughSqr) * dotNV * dotNV) + dotNV;
  return 2.0f * dotNV / fmax(denomC, 1e-6F);
}


float SmithGGXMaskingShadowing(const float dotNL, const float dotNV, float roughSqr)
{
  const float denomA = dotNV * sqrt(roughSqr + (1.0f - roughSqr) * dotNL * dotNL);
  const float denomB = dotNL * sqrt(roughSqr + (1.0f - roughSqr) * dotNV * dotNV);
  return 2.0f * dotNL * dotNV / fmax(denomA + denomB, 1e-6F);
}


Point3 GgxVndf(const Point3& wo, float roughness, float u1, float u2)
{
  // -- Stretch the view vector so we are sampling as though
  // -- roughness==1
  const Point3 v = Normalize(Point3(wo.x * roughness, wo.y * roughness, wo.z));

  // -- Build an orthonormal basis with v, t1, and t2
  const Point3 XAxis(1.0f, 0.0f, 0.0F);
  const Point3 ZAxis(0.0f, 0.0f, 1.0F);
  const Point3 t1 = (v.z < 0.999f) ? Normalize(CrossProd(v, ZAxis)) : XAxis;
  const Point3 t2 = CrossProd(t1, v);

  // -- Choose a point on a disk with each half of the disk weighted
  // -- proportionally to its projection onto direction v
  const float a = 1.0f / (1.0f + v.z);
  const float r = sqrt(u1);
  const float phi = (u2 < a) ? (u2 / a) * PI : PI + (u2 - a) / (1.0f - a) * PI;
  const float p1 = r * cos(phi);
  const float p2 = r * sin(phi) * ((u2 < a) ? 1.0f : v.z);

  // -- Calculate the normal in this stretched tangent space
  const Point3 n = p1 * t1 + p2 * t2 + sqrt(fmax(0.0f, 1.0f - p1 * p1 - p2 * p2)) * v;

  // -- unstretch and normalize the normal
  return Normalize(Point3(roughness * n.x, roughness * n.y, fmax(0.0f, n.z)));
}



void HydraMtl::GGXSample2AndEvalBRDF(Color& a_resultReflects, const Point2 rnd2d, 
  const float gloss, const Point3 a_normal, Point3& a_rayDir, const Color a_reflColor)
{
  const float  roughness = 1.0f - gloss;
  const float  roughSqr  = roughness * roughness;

  Point3 nx, ny, nz = a_normal;

  CoordinateSystem(nz, nx, ny);

  // to PBRT coordinate system
  // wo = v = rayDir
  // wi = l = -newDir   

  const Point3 wo       = Point3(-DotProd(a_rayDir, nx), -DotProd(a_rayDir, ny), -DotProd(a_rayDir, nz));
  const Point3 wh       = GgxVndf(wo, roughSqr, rnd2d.x, rnd2d.y);
  const Point3 wi       = (2.0f * DotProd(wo, wh) * wh) - wo;            // Compute incident direction by reflecting about wm    
  const Point3 newDir   = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL     = DotProd(a_normal, newDir);
  const float dotNV     = DotProd(a_normal, -a_rayDir);


  if (dotNV < 1e-6f || dotNL < 1e-6f)
    a_resultReflects    = { 0.0f, 0.0f, 0.0f };
  else
  {
    //const float F     = Fresnel is not needed here, because it is used for the blend with diffusion.
    const float G1      = SmithGGXMasking(dotNV, roughSqr);
    const float G2      = SmithGGXMaskingShadowing(dotNL, dotNV, roughSqr);

    const float Pss     = /*F **/ G2 / G1; // Pass single-scattering.
    
    // Pass multi-scattering.    
    const Color Pms     = GetMultiscatteringFrom2dTable(getGgxTable(), roughness, dotNV, 64, 64, a_reflColor);

    a_resultReflects    = Pss * Pms;
    a_rayDir            = newDir;
  }
}


void HydraMtl::TRGGXSampleAndEvalBRDF(Color& a_resultReflects, const Point2 rnd2d, const float gloss,
  const float anisotropy, const float anisRotation, const Point3 a_normal, const Point3 tbBasis[2],
  Point3& a_rayDir)
{
  const float roughness = 0.5f - 0.5f * gloss;
  const float anisoMult = 1.0f - anisotropy;
  const Point2 alpha(BeckmannRoughnessToAlpha(roughness*roughness), BeckmannRoughnessToAlpha(roughness*roughness*anisoMult*anisoMult));

  Point3 nx, ny = a_normal, nz;

  // wo = v = rayDir
  // wi = l = -newDir
  //
  if (fabs(alpha.x - alpha.y) > 1.0e-5f)
  {
    nx = tbBasis[1]; //a_bitan;
    nz = tbBasis[0]; //a_tan;

    const float4x4 mRot = RotateAroundVector4x4(ny, anisRotation * TWOPI);
    nx = mul3x3(mRot, nx);
    nz = mul3x3(mRot, nz);
  }
  else
    CoordinateSystem(ny, nx, nz);

  const Point3 temp = ny;
  ny = nz;
  nz = temp;

  const Point3 wo      = Point3(-DotProd(a_rayDir, nx), -DotProd(a_rayDir, ny), -DotProd(a_rayDir, nz));
  const Point3 wh      = TrowbridgeReitzDistributionSampleWH(wo, Point2(rnd2d.x, rnd2d.y), alpha.x, alpha.y);
  const Point3 wi      = (2.0f * DotProd(wo, wh) * wh) - wo;                // Compute incident direction by reflecting about wh
  const Point3 newDir  = Normalize(wi.x*nx + wi.y*ny + wi.z*nz);        // back to normal coordinate system

  const float dotNL = DotProd(a_normal, newDir);
  const float dotNV = DotProd(a_normal, -a_rayDir);

  if (dotNV < 1e-6f || dotNL < 1e-6f)
    a_resultReflects = { 0.0f, 0.0f, 0.0f };
  else
  {
    const float pdf  = fmaxf(TrowbridgeReitzDistributionPdf(wo, wh, alpha.x, alpha.y), DEPSILON);
    a_resultReflects = dotNL * TrowbridgeReitzBRDF_PBRT(wo, wi, alpha.x, alpha.y) / pdf * Color(1,1,1);
    a_rayDir         = newDir;
  }
}
