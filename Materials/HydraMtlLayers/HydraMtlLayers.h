#pragma once

#include "resource.h"
#include "stdmat.h"
#include "iparamm2.h"
#include "StdMaterialViewportShading.h"
#include <IMaterialBrowserEntryInfo.h>


/////////////////////////////////////////////////////////////////////////////////////////


extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define hydraMtlLayers_CLASS_ID	Class_ID(0x7a8d0218, 0x6e502df)

#define NSUBMTL		5 // TODO: number of sub-materials supported by this plugin 
#define NSUBTEX   4

#define VERSION_CURRENT 1

enum mtlSlots { MTL1_SLOT, MTL2_SLOT, MTL3_SLOT, MTL4_SLOT, MTL5_SLOT, MASK2_TEX_SLOT, MASK3_TEX_SLOT, MASK4_TEX_SLOT, MASK5_TEX_SLOT, LAST_ELEM }; // LAST_ELEM should be last in the list!

#define PBLOCK_REF	LAST_ELEM

typedef std::wstring hydraStr;
typedef wchar_t hydraChar;

class MixMatDlgProc;


class HydraMtlLayers : public Mtl, public IReshading
{
  private:
    GeneralMaterialViewportShading mVptShadingToggle;

  public:
    IParamBlock2*     pblock;
    Mtl*              subMtl[NSUBMTL];
    Texmap*           subTex[NSUBTEX];

    BOOL              mapOn[3];
    Interval          ivalid;
    BOOL              Param1;
    ReshadeRequirements mReshadeRQ; 

    static MixMatDlgProc *paramDlg;

    HydraMtlLayers(BOOL loading);
    void              NotifyChanged() { NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE); }

    // From MtlBase and Mtl
    void              SetAmbient(Color c, TimeValue t) {}
    void              SetDiffuse(Color c, TimeValue t) {}
    void              SetSpecular(Color c, TimeValue t) {}
    void              SetShininess(float v, TimeValue t) {}

    Color             GetAmbient(int mtlNum = 0, BOOL backFace = FALSE);
    Color             GetDiffuse(int mtlNum = 0, BOOL backFace = FALSE);
    Color             GetSpecular(int mtlNum = 0, BOOL backFace = FALSE);
    float             GetXParency(int mtlNum = 0, BOOL backFace = FALSE);
    float             GetShininess(int mtlNum = 0, BOOL backFace = FALSE);
    float             GetShinStr(int mtlNum = 0, BOOL backFace = FALSE);
    float             GetSelfIllum(int mtlNum = 0, BOOL backFace = FALSE);
    Color             GetSelfIllumColor(int mtlNum = 0, BOOL backFace = FALSE);
    BOOL              GetSelfIllumColorOn(int mtlNum = 0, BOOL backFace = FALSE);

    float             WireSize(int mtlNum = 0, BOOL backFace = FALSE);

    ParamDlg*         CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
    void              EnableStuff();

    void              Shade(ShadeContext& sc);
    float             EvalDisplacement(ShadeContext& sc);
    Interval          DisplacementValidity(TimeValue t);
    void              Update(TimeValue t, Interval& valid);
    void              Init();
    void              Reset();
    Interval          Validity(TimeValue t);

    void              DeleteThis() { delete this; }

    Sampler*          GetPixelSampler(int mtlNum, BOOL backFace);

    // SubMaterial access methods
    int               NumSubMtls() { return NSUBMTL; }
    Mtl*              GetSubMtl(int i);
    void              SetSubMtl(int i, Mtl *m);
    TSTR              GetSubMtlSlotName(int i);
    TSTR              GetSubMtlTVName(int i);
    int               VPDisplaySubMtl();

    // SubTexmap access methods
    int               NumSubTexmaps() { return NSUBTEX; }
    Texmap*           GetSubTexmap(int i);
    void              SetSubTexmap(int i, Texmap *m);
    TSTR              GetSubTexmapSlotName(int i);
    TSTR              GetSubTexmapTVName(int i);    

    // Loading/Saving
    IOResult          Load(ILoad *iload);
    IOResult          Save(ISave *isave);

    // From Animatable
    Class_ID          ClassID() { return hydraMtlLayers_CLASS_ID; }
    SClass_ID         SuperClassID() { return MATERIAL_CLASS_ID; }
    void              GetClassName(TSTR& s) { s = GetString(IDS_CLASS_NAME); }
    RefTargetHandle   Clone(RemapDir &remap);
    int               NumSubs() { return 1 + NSUBMTL; }
    Animatable*       SubAnim(int i);
    TSTR              SubAnimName(int i);
    virtual RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate);

    // Maintain the number or references here 
    RefTargetHandle   GetReference(int i);
    void              SetReference(int i, RefTargetHandle rtarg);
    int               NumRefs() { return 1 + NSUBMTL + NSUBTEX; }
    int	              NumParamBlocks() { return 1; }					   // return number of ParamBlocks in this instance
    IParamBlock2*     GetParamBlock(int i) { return pblock; }       // return i'th ParamBlock
    IParamBlock2*     GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock
    int               SubNumToRefNum(int subNum) { return subNum; }

    BOOL              SetDlgThing(ParamDlg* dlg);

    // begin - ke/mjm - 03.16.00 - merge reshading code
    BOOL SupportsRenderElements() { return FALSE; }
    //		BOOL SupportsReShading(ShadeContext& sc);
    ReshadeRequirements GetReshadeRequirements() { return mReshadeRQ; } // mjm - 06.02.00
    void PreShade(ShadeContext& sc, IReshadeFragment* pFrag);
    void PostShade(ShadeContext& sc, IReshadeFragment* pFrag, int& nextTexIndex, IllumParams* ip);
    // end - ke/mjm - 03.16.00 - merge reshading code

        // From Mtl
    bool IsOutputConst(ShadeContext& sc, int stdID);
    bool EvalColorStdChannel(ShadeContext& sc, int stdID, Color& outClr);
    bool EvalMonoStdChannel(ShadeContext& sc, int stdID, float& outVal);

    void* GetInterface(ULONG id);

    BaseInterface* GetInterface(Interface_ID id);
  };



class hydraMtlLayersClassDesc : public ClassDesc2, public IMaterialBrowserEntryInfo
{
public:
  virtual int IsPublic() 							{ return TRUE; }
  virtual void* Create(BOOL loading = FALSE) 		{ return new HydraMtlLayers(loading); }
  virtual const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); } // Name in Material/Map Browser
  virtual SClass_ID SuperClassID() 				{ return MATERIAL_CLASS_ID; }
  virtual Class_ID ClassID() 						{ return hydraMtlLayers_CLASS_ID; }
  virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }
  virtual const TCHAR* InternalName() 			{ return _T("hydraMtlLayers"); }	// returns fixed parsable name (scripter-visible name)
  virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
  
  // For entry category
  virtual FPInterface* GetInterface(Interface_ID id) override;

  // -- from IMaterialBrowserEntryInfo
  virtual const MCHAR* GetEntryName() const override;
  virtual const MCHAR* GetEntryCategory() const override;
  virtual Bitmap* GetEntryThumbnail() const override;
};


ClassDesc2* GethydraMtlLayersDesc(); 

enum { hydraMtlLayers_params };


//TODO: Add enums for various parameters
enum { 
  pb_baseMtl,
  pb_layer2_mtl,
  pb_layer2_mask,
  pb_layer3_mtl,
  pb_layer3_mask,
  pb_layer4_mtl,
  pb_layer4_mask,
  pb_layer5_mtl,
  pb_layer5_mask
};


