#include "HydraMtlLayers.h"
#include "../../HydraRender mk3/ImportStructs.h"
#include <math.h> 
#include <fstream> 

inline float clamp(float u, float a, float b) { float r = max(a, u); return min(r, b); }


/*===========================================================================*\
 |	Actual shading takes place
\*===========================================================================*/

// my version 12.4.00,  kae need to save mix & get it
void HydraMtlLayers::PreShade(ShadeContext& sc, IReshadeFragment* pFrag)
{
  IReshading* pReshading = NULL;

  int numChan = pFrag->NTextures(); // indx of len channel
  pFrag->AddIntChannel(0);//placeholder

  int subMtlLength[NSUBTEX] = { 0, 0, 0, 0 };
  int subTexLength = 0;

  // save mix
  float mix = 0.0f;
  for (size_t i = 0; i < NSUBTEX; i++)
  {
    if (subTex[i])
    {
      mix = subTex[i]->EvalMono(sc);
      pFrag->AddFloatChannel(mix);
      subTexLength++;
    }
  }

  for (size_t i = 0; i < NSUBMTL; i++)
  {
    if (subMtl[i]) 
    {
      pReshading = (IReshading*)(subMtl[i]->GetInterface(IID_IReshading));
      if (pReshading) 
      {
        pReshading->PreShade(sc, pFrag);
        int prevI = i - 1;
        if (prevI < 0) prevI = 0;
        subMtlLength[i] = pFrag->NTextures() - subMtlLength[prevI] - numChan - 1;
      }
    }
  }

  // code the two lengths into the int channel
  pFrag->SetIntChannel(numChan, (subMtlLength[0] | (subMtlLength[1] << 8) | (subTexLength << 31)));
}

void HydraMtlLayers::PostShade(ShadeContext& sc, IReshadeFragment* pFrag, int& nextTexIndex, IllumParams*)
{
  IReshading* pReshading  = nullptr;
  Mtl*        sm[NSUBMTL];
  Texmap*     mp[NSUBTEX];

  for (size_t i = 0; i < NSUBMTL; i++) sm[i] = mapOn[i] ? subMtl[i] : NULL;
  for (size_t i = 0; i < NSUBTEX; i++) mp[i] = mapOn[i] ? subTex[i] : NULL;
  

  int sm1Length = pFrag->GetIntChannel(nextTexIndex++);
  int sm2Length = (sm1Length >> 8) & 0xff;
  int mpLength = sm1Length >> 31; // top bit
  sm1Length &= 0xff; // get rid of other stuff


  for (size_t i = 0; i < NSUBMTL; i++)
  {
    if (sm[i] && mp[i])
    {
      sc.ResetOutput();
      pReshading = (IReshading*)(sm[i]->GetInterface(IID_IReshading));
      if (pReshading)
        pReshading->PostShade(sc, pFrag, nextTexIndex);

      if (mpLength)
      {
        const float mix = pFrag->GetFloatChannel(nextTexIndex++);

        //out1 = sc.out;
        sc.out.MixIn(sc.out, mix);
      }
      nextTexIndex++;
    }
  }
}



