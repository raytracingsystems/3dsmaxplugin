#include <ctime>
#include <mutex>
#include <future>
#include <chrono> 

#include "../../HydraAPI/hydra_api/HR_HDRImage.h"
#include "../../HydraAPI/hydra_api/HydraRenderDriverAPI.h"
#include "HydraRenderer.h"

#include "3dsmaxport.h"
#include "gamma.h"
#include "IFileResolutionManager.h"
#include "IPathConfigMgr.h"




////////////////////////////////////////////////////////////////////////////////////
IHRRenderDriver* CreateDriverRTE(const wchar_t* a_cfg) { return nullptr; }
HRplugResourceManager g_hrPlugResMangr;
extern HINSTANCE hInstance;


HydraRenderParamDlg* HydraRenderPlugin::m_pLastParamDialog  = nullptr;
HydraRenderParams    HydraRenderPlugin::m_lastRendParams;


HRplugResourceManager::HRplugResourceManager()
{  
  m_trackerManager = std::make_shared<Hydra::TrackerManager>();
}


HRplugResourceManager::~HRplugResourceManager()
{
  //hrSceneLibraryClose();
}



void HydraRenderPlugin::HydraInit(bool& a_hydraInit)
{
  m_rendParams.renderer = this;

  if (!a_hydraInit)
  {
    hrSceneLibraryClose();
    ClearTempFolder();

    m_rendParams.SetDefaults();
    m_hasCalculateGBuffer = false;

    omp_set_num_threads(omp_get_num_procs()); // use all threads 

    m_paramTrace.open(HYDRA_PLUGIN_PATH / L"paramTrace.txt");

    g_hrPlugResMangr.m_trackerManager->ClearNode();
    g_hrPlugResMangr.m_trackerManager->ClearTexmap();

    envTex = texInfo(HRTextureNodeRef(), nullptr, 1.0F, L"alpha");

    g_hrPlugResMangr.m_geoNodesPools.clear();
    g_hrPlugResMangr.m_lightNodesPools.clear();

    g_hrPlugResMangr.m_meshRefLib.clear();
    g_hrPlugResMangr.m_lightsRefLib.clear();
    g_hrPlugResMangr.m_materialsRefLib.clear();

    g_hrPlugResMangr.m_texmapLib.clear();
    g_hrPlugResMangr.m_procTexLib.clear();

    hrSceneLibraryOpen(HYDRA_SCENELIB_PATH.c_str(), HR_WRITE_DISCARD);
    _hrInitPostProcess();
    g_hrPlugResMangr.m_rendRef           = hrRenderCreate(L"HydraModern");
    g_hrPlugResMangr.m_placeholderMatRef = hrMaterialCreate(L"hydra_placeholder_material");
    g_hrPlugResMangr.m_grayMat           = hrMaterialCreate(L"grayOverrideMat");
    g_hrPlugResMangr.ExtractPlaceholderMaterial();
    g_hrPlugResMangr.CreateOverrideGrayMat();

    g_hrPlugResMangr.m_hydraRawRender    = hrFBICreate(L"framebuffer", 1, 1, 16);
    g_hrPlugResMangr.m_hydraOutRender    = hrFBICreate(L"out", 1, 1, 16);

#ifdef SILENT_DEVICE_ID
    m_lastRendParams.device_id.push_back(3); // your device ID
    m_lastRendParams.engine_type = 1;        // 1 - OpenCL; 0 - CUDA
    m_lastRendParams.liteMode = false;
#else
    if (!readAndCheckDeviceSettingsFile(m_lastRendParams.engine_type, m_lastRendParams.device_id))
      UserSelectDeviceDialog(nullptr);
    else
      m_rendParams.rememberDevice = true;
#endif 

    a_hydraInit = true;
  }
}



HydraRenderPlugin::HydraRenderPlugin()
{  
  HydraInit(g_hrPlugResMangr.m_hydraInit);
}



extern bool g_materialProcessStart;



HydraRenderPlugin::~HydraRenderPlugin()
{
  // The destructor is called every time the render settings are opened. 
  // See DllEntry.cpp, int LibShutdown(void).
  // 
  //hrSceneLibraryClose();
}


bool HydraRenderPlugin::IsStopSupported() const
{
  return false;
}

void HydraRenderPlugin::StopRendering()
{
  // Do nothing
}

Renderer::PauseSupport HydraRenderPlugin::IsPauseSupported() const
{
  return PauseSupport::None;
}

void HydraRenderPlugin::PauseRendering()
{
  // Do nothing
}

void HydraRenderPlugin::ResumeRendering()
{
  // Do nothing
}

bool HydraRenderPlugin::HasRequirement(const Requirement requirement)
{
  switch (requirement)
  {
    case kRequirement_Wants32bitFPOutput:          return true;
    case kRequirement_NoGBufferForToneOpPreview:   return true;
    case kRequirement_SupportsConcurrentRendering: return true;
    default: return false;  // Don't care about other requirements
  }
}

IInteractiveRender* HydraRenderPlugin::GetIInteractiveRender()
{
  return nullptr;
}

void HydraRenderPlugin::GetVendorInformation(MSTR& info) const
{
  info = ClassName();
}

void HydraRenderPlugin::GetPlatformInformation(MSTR& info) const
{
  // nothing
}


//RendProgressCallback* g_pCallBack = nullptr;
//
//void FilterProgressBar(const char* message, float a_progress)
//{
//  if (g_pCallBack == nullptr)
//    return;
//  else
//  {
//#ifdef MAX2012
//    g_pCallBack->SetTitle(message);
//#else
//    std::wstring myStr = strConverter::ToWideString(message);
//    g_pCallBack->SetTitle(myStr.c_str());
//#endif
//    g_pCallBack->Progress(int(a_progress*100.0f), 100);
//  }
//}



size_t blocksST(size_t elems, int threadsPerBlock)
{
  if (elems % threadsPerBlock == 0 && elems >= threadsPerBlock)
    return elems / threadsPerBlock;
  else
    return (elems / threadsPerBlock) + 1;
}

size_t RoundBlocks(size_t elems, int threadsPerBlock)
{
  if (elems < threadsPerBlock)
    return static_cast<size_t>(threadsPerBlock);
  else
    return blocksST(elems, threadsPerBlock) * threadsPerBlock;
}


void HydraRenderPlugin::CopyRawImageToBitmap(Bitmap* a_bmapOut, const float* a_imageSrc, const size_t a_widthSrc, const size_t a_heightSrc, const bool a_hasAlpha) const
{
  if (!a_imageSrc) 
    return;

  //ULONG ctype;
  //auto* pGbufRealPix = static_cast<RealPixel*>(a_bmapOut->GetChannel(BMM_FLOAT_RGBA_32, ctype));

  //if (!pGbufRealPix)
  //{
  //  a_bmapOut->CreateChannels(BMM_FLOAT_RGBA_32);
  //  pGbufRealPix = static_cast<RealPixel*>(a_bmapOut->GetChannel(BMM_FLOAT_RGBA_32, ctype));
  //}

  const size_t w     = a_widthSrc;
  const size_t h     = a_heightSrc;
  const auto   black = BMM_Color_fl(0.0F, 0.0F, 0.0F, 0.0F);

  a_bmapOut->Fill(black);

  if (a_bmapOut->Width() != w || a_bmapOut->Height() != h)
    a_bmapOut->CropImage((int)w, (int)h, black);

  const int numChannel = a_hasAlpha ? 4 : 3;
    
//#pragma omp parallel for num_threads(omp_get_num_procs())
//  for (int y = 0; y < h; ++y)
//    a_bmapOut->PutPixels(0, h - y - 1, w, (BMM_Color_fl*)(a_imageSrc + y * w * numChannel));

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int y = 0; y < (int)h; ++y)
  {
    std::vector<BMM_Color_fl> line(w);

    for (size_t x = 0; x < w; ++x)
    {
      const size_t indexSrc = ((size_t)y * w + x) * numChannel;

      const float r = a_imageSrc[indexSrc + 0];
      const float g = a_imageSrc[indexSrc + 1];
      const float b = a_imageSrc[indexSrc + 2];
      float a       = 1.0F;
      
      if (a_hasAlpha)
        a = a_imageSrc[indexSrc + 3];

      //if (pGbufRealPix)
      //{
      //  const int gbufIndex     = (h - y - 1) * w + x;                 
      //  pGbufRealPix[gbufIndex] = MakeRealPixel(r, g, b);
      //}

      line[x] = BMM_Color_fl(r, g, b, a);      
    }
    a_bmapOut->PutPixels(0, (int)h - y - 1, (int)w, line.data());
  }
}

void HydraRenderPlugin::CopyImageFromHRRenderToBitmap(Bitmap* bmap_out, int src_w, int src_h)
{
  if (!bmap_out)
    return;

  ULONG ctype;
  RealPixel* pGbufRealPix = static_cast<RealPixel*>(bmap_out->GetChannel(BMM_FLOAT_RGBA_32/*BMM_CHAN_REALPIX*/, ctype));

  if (pGbufRealPix == nullptr)
  {
    bmap_out->CreateChannels(BMM_FLOAT_RGBA_32/*BMM_CHAN_REALPIX*/);
    pGbufRealPix = static_cast<RealPixel*>(bmap_out->GetChannel(BMM_FLOAT_RGBA_32/*BMM_CHAN_REALPIX*/, ctype));
  }

  const int w = bmap_out->Width();
  const int h = bmap_out->Height();

  if (w != src_w || h != src_h)
  {
    auto black = BMM_Color_fl(0.0F, 0.0F, 0.0F, 0.0F);
    bmap_out->Fill(black);
    bmap_out->CropImage(w, h, black);
  }

  hrRenderLockFrameBufferUpdate(g_hrPlugResMangr.m_rendRef);

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int y = 0; y < h; ++y)
  {
    std::vector<float> line(w * 4); // line[x*4 + 0] - red; line[x*4 + 3] - alpha; x from 0 to w-1; 
    hrRenderGetFrameBufferLineHDR4f(g_hrPlugResMangr.m_rendRef, 0, w, y, line.data());
    bmap_out->PutPixels(0, h - y - 1, w, (BMM_Color_fl*)line.data());
  }
  hrRenderUnlockFrameBufferUpdate(g_hrPlugResMangr.m_rendRef);
}



bool HydraRenderPlugin::FrameBufferUpdateLoop(Bitmap* a_bmapOut, const int a_renderWidth, const int a_renderHeight,
  RendProgressCallback* a_prog, const bool a_forMatEditor)
{
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"FrameBufferUpdateLoop.");

  if (!a_bmapOut)
    return false;

  const auto timeStart        = std::chrono::system_clock::now();
  auto timeStart2             = timeStart;
  bool saveFileEveryN_munutes = true;
  float saveImageNext         = m_rendParams.saveRendImagEveryMin * 60.0F; // we will double next image time each save  

  bool hadAnyUpdates          = false;

  // Create an Intel Open Image Denoise device
  oidn::DeviceRef device      = oidn::newDevice();
  device.commit();
  oidn::FilterRef filter      = device.newFilter("RT"); // generic ray tracing filter
  m_albedoPass.clear();
  m_normalPass.clear();
  
  // update loop frame buffer 3ds max

  while (true)
  {
    const auto loopTimeStart = std::chrono::system_clock::now();

    if (saveFileEveryN_munutes)
    {
      timeStart2             = loopTimeStart;
      saveFileEveryN_munutes = false;
    }

    HRRenderUpdateInfo info = hrRenderHaveUpdate(g_hrPlugResMangr.m_rendRef);

    if (info.haveUpdateFB)
    {
      hadAnyUpdates = true;

      m_hasCalculateGBuffer = m_rendParams.denoiserAlbedoPassOn;
      
      if (m_rendParams.denoiserOn)
        DoIntelDenoiseLoop(a_bmapOut, device, filter); // normal need with bump
      else
        CopyImageFromHRRenderToBitmap(a_bmapOut, a_renderWidth, a_renderHeight);

      if (m_rendParams.postProcOn) 
        DoPostProc(a_bmapOut, !m_rendParams.denoiserOn);

      CopyAlphaFromGbuffer(a_bmapOut);
      a_bmapOut->RefreshWindow();
      m_pLastRender = a_bmapOut;
    }

    const auto  timeCurr             = std::chrono::system_clock::now();
    const float renderElapsedSeconds = std::chrono::duration<float>(timeCurr - timeStart).count();
    const float renderElapsedMin     = std::chrono::duration<float>(renderElapsedSeconds).count() / 60.0F;

    if (!UpdateRenderProgressBar(a_prog, info, renderElapsedMin))
      break;

    if (m_rendParams.saveRendImagEveryOn)
      SaveRenderImagesEveryNminutes(a_prog, timeStart, saveImageNext, renderElapsedMin, saveFileEveryN_munutes);


    if (info.finalUpdate || (a_forMatEditor && hadAnyUpdates && info.progress < 0.00001F))
    {
      g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"Render finished.");
      //hrRenderSaveFrameBufferHDR(g_hrPlugResMangr.m_rendRef, HYDRA_TEMP_PATH + L"output.exr");
      break;
    }

    if (m_rendParams.timeLimitOn && (int)renderElapsedMin >= m_rendParams.timeLimit)
      break;
    
    MinimumPauseLoop(loopTimeStart, 1000.0F);
  }

  return true;
}


void HydraRenderPlugin::DoIntelDenoiseLoop(Bitmap* a_bmapOut, oidn::DeviceRef& device, oidn::FilterRef& filter)
{
  const bool useAlbedo = m_rendParams.denoiserAlbedoPassOn;
  const bool useNormal = m_rendParams.denoiserNormalPassOn;

  hrRenderCopyFrameBufferToFBI(g_hrPlugResMangr.m_rendRef, L"color", g_hrPlugResMangr.m_hydraRawRender);

  int width  = 0;
  int height = 0;
  int bpp    = 0;

  auto const image   = (float*)hrFBIGetData(g_hrPlugResMangr.m_hydraRawRender, &width, &height, &bpp);
  
  const int sizeImg4f = width * height * 4;
  std::vector<float> imageRaw(sizeImg4f);

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int i = 0; i < sizeImg4f; ++i)
    imageRaw[(size_t)i] = image[(size_t)i];
  

  // Init filter

  int byteRowStride = bpp * width;
  filter.setImage("color",  image, oidn::Format::Float3, (size_t)width, (size_t)height, 0, (size_t)bpp, (size_t)byteRowStride);  
  filter.setImage("output", image, oidn::Format::Float3, (size_t)width, (size_t)height, 0, (size_t)bpp, (size_t)byteRowStride);  

  if (useAlbedo && m_albedoPass.empty())  
    GetAlbedoAndNormalFromGbuffer(width, height); 
  else
  {
    bpp           = sizeof(float) * 3;
    byteRowStride = bpp * width;
    if (useAlbedo && !m_albedoPass.empty())              filter.setImage("albedo", (float*)m_albedoPass.data(), oidn::Format::Float3, (size_t)width, (size_t)height, 0, (size_t)bpp, (size_t)byteRowStride); // optional      
    if (useAlbedo && useNormal && !m_normalPass.empty()) filter.setImage("normal", (float*)m_normalPass.data(), oidn::Format::Float3, (size_t)width, (size_t)height, 0, (size_t)bpp, (size_t)byteRowStride); // optional
  }
  
  filter.set("hdr", true); 
  filter.set("cleanAux", true);
  filter.commit();
  filter.execute();
  
  // Check for errors
  const char* errorMessage;
  if (device.getError(errorMessage) != oidn::Error::None)
  {
    std::wstring log = L"Intel denoise. " + s2ws(errorMessage);
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, log);
  }

  // Blend with ammount from GUI

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int i = 0; i < sizeImg4f; ++i)
  {
    const auto  a          = (size_t)i;
    const float imgDenoise = image[a];

    const float fixRaw = std::isnan(imageRaw[a]) ? imgDenoise : fmin(imageRaw[a], imgDenoise * 1.25F); // delete nun and compress firefly    
    image[a]          = fixRaw + m_rendParams.denoiseLvl * (imgDenoise - fixRaw);                      // blend
  }

  CopyRawImageToBitmap(a_bmapOut, image, (size_t)width, (size_t)height, true);
}


bool HydraRenderPlugin::UpdateRenderProgressBar(RendProgressCallback* m_prog, HRRenderUpdateInfo& info, const float elapsedMin) const
{  
  if (m_prog && info.progress != 0)
  {
    auto res = m_prog->Progress(unsigned int(info.progress), 100);

    if (res == RENDPROG_ABORT)
      return false;

    std::wstringstream strOut;
    unsigned int remainedMin = 0;
    remainedMin              = (int)(elapsedMin / info.progress * 100.0F - elapsedMin + 1.0F);
	  
    strOut << L"Rendering: " << (unsigned int)info.progress << L"   |   Elapsed: " << (unsigned int)elapsedMin << L" min" << L"   |   Remained: " << remainedMin << L" min";
    
    auto progstr             = strOut.str();
    m_prog->SetTitle(progstr.c_str());          
  }

  return true;
}


Point2 HydraView::ViewToScreen(const Point3 p)
{
  return pRendParams->MapToScreen(p);
}





//***************************************************************************
// Enumerator to call RenderBegin() on all objects
//***************************************************************************

BeginEnum::BeginEnum(TimeValue startTime)
{
  t = startTime;
}

int BeginEnum::proc(ReferenceMaker *rm)
{
  rm->RenderBegin(t);
  return REF_ENUM_CONTINUE;
}

//***************************************************************************
// Enumerator to call RenderEnd() on all objects
//***************************************************************************

EndEnum::EndEnum(TimeValue endTime)
{
  t = endTime;
}

int EndEnum::proc(ReferenceMaker *rm)
{
  rm->RenderEnd(t);
  return REF_ENUM_CONTINUE;
}
