#pragma once

// Based on NodeInstancingPool class from 2017 MaxSDK::RenderingAPI::TranslationHelpers

// max sdk
#include <Noncopyable.h>
#include <RenderingAPI/Translator/Helpers/INodeInstancingPool.h>
#include <NotificationAPI/NotificationAPI_Subscription.h>

#include "object.h"
#include <unordered_set>
#include <unordered_map>
#include <set>

////////////////////////////////////////////////////////////////////////////////////////////////

using namespace MaxSDK::NotificationAPI;

namespace Hydra
{
  class TrackerManager : public MaxSDK::Util::Noncopyable, public INotificationCallback
  {
  public:
    TrackerManager();
    TrackerManager(INode* initial_node, const NotifierType a_notification_type, TimeValue t);
    ~TrackerManager() override;

    void NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* userData) override;

    // Nodes

    INode*  GetRepresentativeNode() const;
    Object* GetReferencedObject() const;
    bool    IsNodeCompatible(INode* node, TimeValue t) const;

    void    AddNode(INode* a_node, TimeValue t);
    void    RemoveNode(INode* a_node);
    void    ClearNode();

    bool needExport = true;         ///< reexport if geometry change.
    std::set<INode*> instancesPool; ///< all instance for curent node

    // Textures
    std::unordered_map<Texmap*, std::pair<bool, Mtl*>> texmaps; // list of trackered textures and their materials

    void AddTexmap(Texmap* tex, Mtl* parentMtl);
    void RemoveTexmap(Texmap* tex);
    void ClearTexmap();

  private:
    Object*            m_refObject              = nullptr;
    INode*             m_representativeNode     = nullptr;    
    const NotifierType m_node_notification_type = NotifierType::NotifierType_Material;
    std::unique_ptr<IImmediateNotificationClient> m_notification_client;
    bool isGeoPool;
  };

}