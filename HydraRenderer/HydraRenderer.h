#pragma once
////////////////////////////////////////////////////////////////////////////////////////////////
// DESCRIPTION: Hydra Renderer plugin for 3ds Max.
// AUTHOR: RAY TRACING SYSTEMS.
////////////////////////////////////////////////////////////////////////////////////////////////

// std
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <unordered_map>  
#include <thread>
#include <memory>
#include <omp.h>
#include <filesystem>

// 3ds Max SDK
#include "3dsmaxsdk_preinclude.h"
#include "resource.h"
#include "icurvctl.h"
#include "lslights.h"
#include "iparamb2.h"
#include "ITabDialog.h"
#include "IFileResolutionManager.h"
#include "INodeBakeProperties.h"
#include "triobj.h"
#include "units.h"
#include "trig.h"
#include "Noncopyable.h"

// Hydra
#include "ImportStructs.h"
#include "HydraLogger.h"
#include "change_trackers.h"
#include "../Materials/HydraMtl/IHydraMtl.h"
#include "../Lights/HydraLight/IHydraLight.h"

#include "../../HydraAPI/hydra_api/HydraAPI.h"
#include "../../HydraAPI/hydra_api/HydraXMLHelpers.h"
#include "../../HydraAPI/hydra_api/HydraLegacyUtils.h"

#include "../../HydraPP/hydra_pp/HydraPostProcessAPI.h"

#include "oidn.hpp" // Intel denoise

/////////////////////////////////////////////////////////////////////////////////////////////////
// 
// CLSP
//
#define BUILD_FOR_EXPORT_COMPRESS_MESHES false // "true" for 3ds Max script export all models in vsgfc, for database CLSP project.
//
// Func with tags parameters in export_materials.cpp
// void HydraRenderPlugin::OverrideNameFromHydraMtlTags(Mtl* mtl, pugi::xml_node& mat)
/////////////////////////////////////////////////////////////////////////////////////////////////


#define HydraRender_CLASS_ID		Class_ID(0x7da90dd1, 0x758b0286)

//Mtl
#define HydraMtlLight_CLASS_ID		Class_ID(0x332b3850, 0x1e7e7dce)
#define hydraMtlLayers_CLASS_ID		Class_ID(0x7a8d0218, 0x6e502df)
#define hydraMtlCatcher_CLASS_ID	Class_ID(0x3cd640a9, 0x36af3dcd)
#define hydraMtlTags_CLASS_ID	    Class_ID(0x341d33e9, 0x491a1bbc)

//Map
#define HydraAO_CLASS_ID	        Class_ID(0x423c85a3, 0x83d993c6)
#define HydraBackEnvir_CLASS_ID		Class_ID(0xe954bddf, 0x4a0184fd)
#define HydraBlendedBox_CLASS_ID	Class_ID(0x39199e3c, 0x762cf297) 


extern TCHAR* GetString(int id);
extern HINSTANCE hInstance;

const std::filesystem::path HYDRA_INSTALL_PATH      = L"C:\\[Hydra]";
const std::filesystem::path HYDRA_ENGINE_PATH       = HYDRA_INSTALL_PATH  / L"bin2";
const std::filesystem::path HYDRA_SHADERS_PATH      = HYDRA_ENGINE_PATH   /     L"shaders";
const std::filesystem::path HYDRA_LOGS_PATH         = HYDRA_INSTALL_PATH  / L"logs";
const std::filesystem::path HYDRA_PLUGIN_PATH       = HYDRA_INSTALL_PATH  / L"pluginFiles";
const std::filesystem::path DEVICE_FILE             = HYDRA_PLUGIN_PATH   /     L"device.txt";
const std::filesystem::path HYDRA_SCENELIB_PATH     = HYDRA_PLUGIN_PATH   /     L"scenelib";
const std::filesystem::path HYDRA_SCENELIBDATA_PATH = HYDRA_SCENELIB_PATH /         L"data";
const std::filesystem::path HYDRA_TEMP_PATH         = HYDRA_INSTALL_PATH  / L"temp";

using texInfo                         = std::tuple <HRTextureNodeRef, StdUVGen*, float, std::wstring>;

enum rendMethod     { RENDER_METHOD_PT, RENDER_METHOD_LT, RENDER_METHOD_IBPT, RENDER_METHOD_MMLT };
enum preset         { PRESET_LOW, PRESET_MEDIUM, PRESET_HIGH, PRESET_VHIGH };
enum texOutputType  { TEX_OUT_COLOR = 0, TEX_OUT_MONO = 1, TEX_OUT_ALPHA = 2 };



namespace strConverter
{
  std::wstring s2ws(const std::string& s);
  std::string ws2s(const std::wstring& s);
  std::wstring s2ws(const std::wstring& s);
  std::string ws2s(const std::string& s);
  std::wstring c2ws(const char* s);
  std::wstring c2ws(const wchar_t* s);
  std::wstring ToWideString(const std::string& rhs);
  std::string ToNarrowString(const std::wstring& rhs);
  std::wstring ToWideString(const std::wstring& rhs);
  std::string ToNarrowString(const std::string& rhs);
  std::wstring colorToWideString(const Color& color);
  std::wstring Point3ToWideString(const Point3& point);
  std::wstring Point4ToWideString(const Point4& point);
  std::string  WstringToUtf8(const std::wstring& str);
  std::wstring Utf8ToWstring(const std::string& str);
  LPCWSTR WstrToLPCWSTR(const std::wstring& str);
}


////////////////////////////////////////////////////////////////////



class HydraRenderParams: public RenderGlobalContext
{
public:
  RendType	rendType;
  int		  	nMinx;
  int		  	nMiny;
  int		  	nMaxx;
  int		  	nMaxy;
  int		  	nNumDefLights;			// The default lights passed into the renderer
  Point2		scrDUV;

  DefaultLight*	        pDefaultLights = nullptr;
  FrameRendParams*	    pFrp           = nullptr;			// Frame specific members
  RendProgressCallback*	progCallback   = nullptr;	// Render progress callback


  // Standard options
  // These options are configurable for all plugin renderers
  BOOL		bVideoColorCheck;
  BOOL		bForce2Sided;
  BOOL		bRenderHidden;
  BOOL		bSuperBlack;
  BOOL		bRenderFields;
  BOOL		bNetRender;

  // Render effects
  Effect*		effect = nullptr;

  int rendTimeType;
  TimeValue rendStart, rendEnd;
  int nthFrame;
  bool rendSaveFile;

  //Rendering parameters
  bool  infQuality;
  float renderQuality;
  long  timeLimit;
  int   primary, secondary, tertiary, manualMode, engine_type, preset;
  std::unordered_map<int, bool> device_id; //device id -> on/off


  //int devices_num;  
  bool useHydraGUI, enableDOF, noRandomLightSel, enableLog, debugOn, allSecondary, allTertiary, timeLimitOn;
  bool primaryMLFilter, secondaryMLFilter, writeToDisk, liteMode, useSeparateSwap, hydraFrameBuf, specNoiseFilterOn, saveRendImagEveryOn;
  bool rememberDevice, enableCaustics, enableTextureResize, offlinePT;
  float focalplane, lensradius, specNoiseFilter;

  //path tracing
  int   icBounce;
  int   seed;
  int   minrays, maxrays, raybounce, diffbounce, useRR, causticRays;
  float relative_error;
  bool  guided, qmcLights, qmcMaterials;
  float env_clamp;
  float bsdf_clamp;
  bool  bsdf_clamp_on;
  bool  estimateLDR;

  //SPPM Caustics
  int maxphotons_c, retrace_c;
  float initial_radius_c, caustic_power, alpha_c;
  bool visibility_c;

  //SPPM Diffuse
  int maxphotons_d, retrace_d;
  float initial_radius_d, alpha_d;
  bool visibility_d, irr_map;

  //Irradiance cache
  int ic_eval;
  int maxpass, fixed_rays;
  float ws_err, ss_err4, ss_err2, ss_err1, ic_relative_error;

  //Multi-Layer
  bool filterPrimary;
  float r_sigma, s_sigma;
  int spp;
  int layerNum;
  //bool saveComposeLayers;

  // Denoise
  float denoiseLvl;
  bool denoiserOn, denoiserAlbedoPassOn, denoiserNormalPassOn;

  //Post processing
  int filter_id;
  float gamma = 2.2F;
  float bloom_radius, bloom_str;
  int numThreads;
  Color whitePointColor;
  float exposure, compress, contrast, saturation, vibrance, whiteBalance, uniformContrast, normalize,
    chromAberr, vignette, sharpness, sizeStar, randomAngle, sprayRay;
  int numRay, rotateRay;
  bool bloom, mlaa, postProcOn;

  //Override
  float env_mult;
  bool overrideGrayMatOn;

  //MLT
  float mlt_plarge;
  int   mlt_iters_mult;
  int   mlt_burn_iters;
  bool  mlt_enable_median_filter;
  float mlt_median_filter_threshold;

  //MMLT  
  int   mmlt_estim_time, mmlt_burn_iters, mmlt_stepSize, mmlt_threads;
  float mmlt_median_filter_threshold, mmlt_multBrightness;
  bool  mmlt_enable_median_filter;

  //Debug
  std::map<std::wstring, bool> debugCheckBoxes;
  float saveRendImagEveryMin;

  // Render elements
  bool hasCalculateRenderElements = false, renElemAlphaOn = true, renElemColorOn = false, renElemCoordOn = false, renElemCoverOn = false, renElemDepthOn = false,
    renElemInstIdOn = false, renElemMatIdOn = false, renElemNormalsOn = false, renElemObjIdOn = false, renElemShadowOn = false;


  HydraRenderParams();
  void		SetDefaults();
  void		ComputeViewParams(const ViewParams&vp);
  Point3		RayDirection(float sx, float sy);

  int				NumRenderInstances() override;
  RenderInstance*	GetRenderInstance(int i) override;
};


class HydraView: public View
{
public:
  HydraRenderParams*	pRendParams;
  Point2		ViewToScreen(const Point3 p) override;
};


class HRplugResourceManager
{
public:
  HRplugResourceManager();
  ~HRplugResourceManager();

  std::wstring        m_hydraVersion    = L"2.6.2";

  bool                m_hydraInit       = false; 
  HRRenderRef         m_rendRef;
  HydraLogger         m_pluginLog;
  HRFBIRef            m_hydraRawRender;
  HRFBIRef            m_hydraOutRender;
    

  std::vector<std::shared_ptr<Hydra::TrackerManager>> m_geoNodesPools;
  std::vector<std::shared_ptr<Hydra::TrackerManager>> m_lightNodesPools;
  std::unordered_map<INode*,  int>                    m_nodeInstancesLib;

  std::unordered_map<Object*, HRMeshRef>              m_meshRefLib;
  std::unordered_map<Object*, HRLightRef>             m_lightsRefLib;
  std::unordered_map<Mtl*,    HRMaterialRef>          m_materialsRefLib;
  std::unordered_map<Texmap*, texInfo>                m_texmapLib; //map from max texture to hydraAPI reference, uvgen and gamma
  std::unordered_map<Texmap*, std::vector<texInfo>>   m_procTexLib; // procedural textures lib
  std::shared_ptr<Hydra::TrackerManager>              m_trackerManager;


  /**
  \brief Gray material for override all materials from GUI render setup
  */
  HRMaterialRef m_grayMat;
  void CreateOverrideGrayMat();

  /**
  \brief exports placeholder material for use in blend
  */
  HRMaterialRef m_placeholderMatRef; //for use in blend material
  void ExtractPlaceholderMaterial();

};


class HydraRenderParamDlg;

class HydraRenderPlugin: public Renderer, public ITabDialogObject
{
public:  
  static HydraRenderParamDlg* m_pLastParamDialog;

  HydraRenderParams	          m_rendParams;
  static HydraRenderParams    m_lastRendParams;
  static int                  m_matRenderTimes;
  HydraView		                m_theView;
  ViewParams	                m_view;

  [[nodiscard]] inline int	     GetMeshFrameStep() const      { return m_nMeshFrameStep; }
  [[nodiscard]] inline int	     GetKeyFrameStep()  const      { return m_nKeyFrameStep; }
  [[nodiscard]] inline TimeValue GetStaticFrame()   const      { return m_nStaticFrame; }
  inline void SetMeshFrameStep(int val)                        { m_nMeshFrameStep = val; }
  inline void SetKeyFrameStep(int val)                         { m_nKeyFrameStep  = val; }
  inline void SetStaticFrame(TimeValue val)                    { m_nStaticFrame   = val; }

  void		    BeginThings();		// Called before rendering has started
  void		    EndThings();			// Called after rendering has finished

  IOResult    Load(ILoad *iload)         override;
  IOResult    Save(ISave *isave)         override;
                                         
  //From Animatable                      
  Class_ID    ClassID()                  override { return HydraRender_CLASS_ID; }
  SClass_ID   SuperClassID()             override { return RENDERER_CLASS_ID; }
                                         
#ifdef MAX2022                           
#else                                    
  void        GetClassName(TSTR& s)      override { s = GetString(IDS_CLASS_NAME); }
#endif // MAX2022


  RefTargetHandle Clone(RemapDir &remap) override;

  RefResult   NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate) override;

  int         NumSubs()                  override { return 1; }

#ifdef MAX2022
#else
  TSTR        SubAnimName(int i)         override { return GetString(IDS_PARAMS); }
#endif // MAX2022


  int  Open(INode *scene, INode *vnode, ViewParams *viewPar, RendParams &rp, HWND hwnd, DefaultLight *defaultLights=nullptr, int numDefLights=0, RendProgressCallback *m_prog=nullptr) override;
  int  Render(TimeValue t, Bitmap *tobm, FrameRendParams &frp, HWND hwnd, RendProgressCallback *m_prog=nullptr, ViewParams *viewPar=nullptr) override;
  void Close(HWND hwnd, RendProgressCallback *m_prog=nullptr) override;
  
  bool ApplyRenderEffects(TimeValue t, Bitmap *pBitmap, bool updateDisplay=true) override { return false; }
  RendParamDlg* CreateParamDialog(IRendParams *m_iRendParam, BOOL m_prog=FALSE) override;

  bool IsStopSupported()                    const override;
  void StopRendering()                            override;
  Renderer::PauseSupport IsPauseSupported() const override;
  void PauseRendering()                           override;
  void ResumeRendering()                          override;
  bool HasRequirement(Requirement requirement)    override;
  IInteractiveRender* GetIInteractiveRender()     override;
  void GetVendorInformation(MSTR& info)     const override;
  void GetPlatformInformation(MSTR& info)   const override;
  bool CompatibleWithAnyRenderElement()     const override { return false; }
  bool CompatibleWithRenderElement(IRenderElement& pIRenderElement) const override { return false; }

  bool SupportsTexureBaking()                     override { return false; } // ### not yet 

  using Renderer::GetInterface;
  BaseInterface* GetInterface(Interface_ID id)    override;

  void AddTabToDialog(ITabbedDialog* dialog, ITabDialogPluginTab* tab) override;
  int  AcceptTab(ITabDialogPluginTab* tab)        override;

  void ResetParams()                              override;

  int	CheckAbort(int done, int total);
  void SetProgTitle(const TCHAR *title);

  void DeleteThis()                               override { delete this; }

  void HydraInit(bool& a_hydraInit);

  HydraRenderPlugin();
  ~HydraRenderPlugin()                            override;

  /////// utility variables////////////////////////////
  //IScanRenderer*                m_pScanlineRender;
  std::wofstream                  m_paramTrace;
  int		                          m_nMeshFrameStep   = 5;
  int		                          m_nKeyFrameStep    = 5;
  TimeValue	                      m_nStaticFrame     = TimeValue(0);
  INode*                          m_pScene           = nullptr;
  INode*                          m_pVnode           = nullptr;
  std::vector<HydraRenderDevice>  m_devList;
  ///////////////////////////////////////////////////

  HRSceneInstRef                  m_currentScene;
  bool                            m_compressMeshOnExport = false;
  int                             m_largestMaterialId = -1; //for rendering in material editor
  HRLightRef                      m_envRef;
  HRLightRef                      m_sunRef;
  HRCameraRef                     m_camRef;
  HRRenderRef                     m_rendRefMatEditor;

  /**
  \brief copies image from renderDriver to 3ds max default framebuffer
  */
  bool FrameBufferUpdateLoop(Bitmap * a_bmapOut, const int a_renderWidth, const int a_renderHeight, RendProgressCallback * a_prog, const bool a_forMatEditor);
  bool UpdateRenderProgressBar(RendProgressCallback* m_prog, HRRenderUpdateInfo& info, const float elapsedMin) const;
  void CopyRawImageToBitmap(Bitmap * a_bmapOut, const float* a_imageSrc, const size_t a_widthSrc, const size_t a_heightSrc, const bool a_hasAlpha) const;
  /**
  \brief entry point to scene export
  */
  int  DoExport(INode* root, INode *vnode, TimeValue t);

  /**
  \brief walks the scene node graph and fill geometry and light instance pools, also find all materials used in the scene and add
  them in mtlTracker
  */
  void PreProcess(INode* node, TimeValue t);

  /**
  \brief creates xml file with scene graph
  */
  void ExportSceneGraph(INode* root);

  /**
  \brief creates scene and instance meshes and lights
  */
  int  ExtractAndDumpSceneData(TimeValue t);

  /**
  \brief exports geometry for new/changed meshes and create instances
  */
  void ExtractAndInstanceMeshes(int& geomObjNum, int& vertWritten, TimeValue t);

  bool CheckSupportRemapList(Mtl * repMat, const bool a_printWarning);

  /**
  \brief exports geometry data for a single geometry/shape object
  * supports vertex indexing
  */
  //int  ExtractMesh(INode* node, TimeValue t, const HRMeshRef &meshRef);

  /**
  \brief exports geometry data for a single geometry/shape object
  * does not support vertex indexing
  */
  int  ExtractMeshNoIndexing(INode * a_node, HRMeshRef & a_meshRef, const bool a_supportRemapList, TimeValue t);

  void AddMatIdFromFaceMatId(Mtl* const a_nodeMtl,



 const bool a_isMultiMat,
 const bool a_notSuppRemapList, const int a_faceMatID, std::vector<int> &a_materialIDs, const size_t a_faceIndex);

  ///////////////////////////////////////////////////////////////////
  // LIGHTS 
  ///////////////////////////////////////////////////////////////////

  /**
  \brief exports parameters for new/changed lights and create instances
  */
  void ExtractAndInstanceLights(int& lightsNum, TimeValue t);

  /**
  \brief exports parameters of a single light
  */
  void ExtractLightObject(INode* node, TimeValue t, HRLightRef lightRef, bool isNew);

  /**
  \brief exports environment parameters
  */
  void ExtractEnvironmentLight(TimeValue t);

  /**
  \brief create light from mesh light
  */
  void CreateLightFromMeshLight(HRLightRef& meshLightRef, Mtl* instMat, std::shared_ptr<Hydra::TrackerManager> geoNode);

  texInfo envTex = texInfo(HRTextureNodeRef(), nullptr, 1.0F, L"alpha");

  //need this to reference sun from sky

  std::string  m_sunSkyName;
  std::wstring m_sunSkyNameW;

  bool         m_hasSky = false;
  struct PerezSky
  {
    float skyMult;
    Color skyTint;
    float skyHaze;
  };

  PerezSky m_perezSky;

  //Exporting different light types
  bool IsHydraLight                 (LightObject* a_node);
  bool IsHydraLight2                (LightObject* a_node); // hydra light script plugin.
  bool IsHydraSky                   (LightObject* a_node);
  bool IsPhotometricLight           (LightObject* a_node);

  void ExtractHydraLightParams      (LightObject* light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval& valid);
  void ExtractHydraLight2Params     (LightObject* light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval& valid);
  void ExtractStdLightParams        (LightObject* light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval& valid);
  void ExtractPhotometricLightParams(LightObject* light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval& valid);
  void ExtractHydraSunSkyParams     (LightObject* light, HRLightRef lightRef, INode* node, pugi::xml_node lightNode, TimeValue t, Interval& valid);
  void ExtractIESDataFromScriptLight(LightObject* light, Matrix3* lightTransform, pugi::xml_node lightNode);
  void ExtractIESData               (LightObject* light, pugi::xml_node lightNode) const;
  void ExtractSpectrFromScriptLight (LightObject* light, pugi::xml_node lightNode);

  ///////////////////////////////////////////////////////////////////
  // MATERIALS   
  ///////////////////////////////////////////////////////////////////


  /**
  \brief Override gray materials
  */
  void AddMatIDToInstanceMatIDs(Mtl* mtl, std::vector<int>& instanceMatIDs);


  /**
  \brief extracts all new/changed materials
  */
  void ExtractMaterialList();

  /**
  \brief recursice all nodes
  */
  void NodeRecursive(INode* pNode);

  /**
  \brief checks material class and call appropriate export function
  */
  int  ExtractMaterial(Mtl* a_mtl);

  /**
  \brief extracts submaterials of a given material
  */
  void ExtractMaterialRecursive(Mtl* mtl);

  /**
  \brief determines open mode for material
  */
  HRMaterialRef GetHRMatRefFromMtl(Mtl* mtl, HR_OPEN_MODE &mode);


  /**
  \brief exports 3ds max standard blend material
  */
  void ExtractBlendMaterial(Mtl* mtl, std::vector<HRMaterialRef> &subMatRefs);

  /**
  \brief exports 3ds max standard material
  */
  void ExtractStdMaterial       (Mtl* mtl);
  void ExtractStdMatDiffuse     (StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
  void ExtractStdMatReflect     (StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
  void ExtractStdMatTransparency(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
  void ExtractStdMatOpacity     (StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
  void ExtractStdMatEmission    (StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);
  void ExtractStdMatDisplacement(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t);

  /**
  \brief exports hydra render material
  */
  void ExtractHydraMaterial           (Mtl* mtl);
  void ExtractHydraDiffuse            (IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport);
  void ExtractHydraReflect            (IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport);
  void ExtractHydraTransparency       (IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport);
  void ExtractHydraOpacity            (IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport);
  void ExtractHydraEmission           (IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport);
  void ExtractHydraTranslucency       (IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport);
  void ExtractHydraRelief             (IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef);

  void ExtractHydraMtlCatcher         (Mtl* a_mtl);
  void ExtractHydraMtlCatcherOpacity  (Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
  void ExtractHydraMtlCatcherAO       (Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);
  void ExtractHydraMtlCatcherReflect  (Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef);

  void ExtractHydraMtlLayers          (Mtl* mtl);

  void OverrideNameFromHydraMtlTags   (Mtl* mtl, pugi::xml_node &mat);


  ///////////////////////////////////////////////////////////////////
  // Textures
  ///////////////////////////////////////////////////////////////////
  
  Texmap* GetTextureFromSlot(Mtl* mtl, const std::wstring& reqSlotName);
  
  /**
  \brief gets names of hydra material texture slots that changed and need to be exported
  */
  bool HydraMtlGetChangedTexturesSlotNames(const std::unordered_set<std::wstring>& changedParams, std::vector<std::wstring>& slotNames) const;

  /**
  \brief gets names of all hydra material texture slots
  */
  void HydraMtlGetAllTexturesSlotNames(std::vector<std::wstring>& slotNames) const;

  /**
  \brief exports texture matrix, addressing mode and gamma
  */
  void ExtractSampler(pugi::xml_node& texNode, const texInfo& info, Texmap* tex);

  /**
  \brief add all textures for given material to m_texmapLib and get uvGen pointers to export samplers later
  \param mtl - material;
  Returns a map: [slot name] -> tuple(texture reference in hydra API, StdUVGen pointer for max texmap object, gamma)
  */
  std::unordered_map<std::wstring, texInfo> ExtractHydraTextures(Mtl* mtl, const std::vector<std::wstring> &mapNames);

  void GetOneTexture(Mtl* a_mtl, Texmap* a_subTex, texInfo& a_oneTex, const std::wstring& a_slotName);

  /**
  \brief export Texmap object, calls BakeBitmap, BakeNormalMap or BakeOtherTexmap
  Returns tuple(texture reference in hydra API, StdUVGen pointer for max texmap object, gamma)
  */
  texInfo BakeTexture(Mtl* a_mtl, Texmap* a_tex, const std::wstring& a_slotName, HRTextureNodeRef a_currentRef, const std::wstring& a_mtlName, const bool a_dontUpdate);
  /**
  \brief bake bitmap (apply output, crop, etc.)
  */
  texInfo BakeBitmap(Texmap* tex, HRTextureNodeRef currentRef, const bool invertHeightMap, const float bumpHeight, const std::wstring& a_mtlName, bool dontupdate = false);

  /**
  \brief export normalmap parameters
  */
  void ExtractNormalBump(Mtl* mtl, bool &isNormalMap, bool &flipRed, bool &flipGreen, bool &swap);

  texInfo BakeNormalMap(Texmap* tex, HRTextureNodeRef currentRef, const bool invertHeightMap, const float bumpHeight); //unused


  /**
  \brief export procedural textures
  */
  HRTextureNodeRef m_texProcHexaplanar;
  HRTextureNodeRef m_texProcFalloff;
  HRTextureNodeRef CreateFalloff();
  HRTextureNodeRef CreateHexaplanar();
  HRTextureNodeRef CreateHexaplanarScalers();
  HRTextureNodeRef CreateAO();

  void BindingTextures(Texmap* tex, const pugi::xml_node& matNode, const HRTextureNodeRef& texRef,
 const std::unordered_map<std::wstring,texInfo>& a_texturesSlotToRef,const std::wstring& a_slotName, const wchar_t* nameChild);

  void BindingFalloff(pugi::xml_node materialNode, const wchar_t* nameChild, const Color& color1, const Color& color2 /*,HRTextureNodeRef currentRef, HRTextureNodeRef texBitmap1, HRTextureNodeRef texBitmap2*/);
  void BindingHexaplanar(pugi::xml_node materialNode, const HRTextureNodeRef& texX, const HRTextureNodeRef& texY, const HRTextureNodeRef& texZ, const HRTextureNodeRef& texX2, const HRTextureNodeRef& texY2, const HRTextureNodeRef& texZ2, const float blendSize, const float mapScale);
  //void BindingHexaplanarScalers(pugi::xml_node materialNode, HRTextureNodeRef texX, HRTextureNodeRef texY, HRTextureNodeRef texZ, 
  //  HRTextureNodeRef texX2, HRTextureNodeRef texY2, HRTextureNodeRef texZ2, LiteMath::float3 tex_scale, LiteMath::float3 minXYZ, LiteMath::float3 maxXYZ);
  
  enum aoCalculateDir { CONCAVE, CONVEX, BOTH };
  void BindingAO(pugi::xml_node materialNode, const Color& color1, const Color& color2, const float distance,
 const float falloff, const int dir, const bool local/*, HRTextureNodeRef occludedBitmap, HRTextureNodeRef unoccludedBitmap, HRTextureNodeRef distance*/);


  texInfo BakeOtherTexmap(Texmap* tex, const HRTextureNodeRef currentRef);

  ///////// CAMERA //////////////////////   

  /**
  \brief export active viewport/camera settings
  */
  HRCameraRef ExtractCameraFromActiveViewport(INode *vnode, TimeValue t);


  ///////// HYDRA RENDER SETINGS ////////

  /**
  \brief export render settings and create render driver
  */
  HRRenderRef  SetupRender(FrameRendParams &frp);

  /**
  \brief export render settings and create render driver for material editor
  */
  HRRenderRef  SetupRenderForMatEditor(FrameRendParams &frp);


  /////// helper functions for geometry data export

  BOOL         TMNegParity(const Matrix3 & m);
  TriObject*   GetTriObjectFromNode(INode *node, TimeValue t, int &deleteIt);
  void	       MakeFaceUV(const Face * f, Point3 *tv) const;

  void         CalculateNormals(const Mesh * a_mesh, std::vector<VertexNormal>& a_normals) const;
  const Point3 GetVertexNormal(Mesh& mesh, MeshNormalSpec* mesh_normal_spec, const int face_index, const int face_vertex_index);
  const Point3 GetVertexNormalUnchecked(Mesh& mesh, MeshNormalSpec* mesh_normal_spec, const int face_index, const int face_vertex_index);
  Point3       ComputeSmGroupNormal(const std::vector<int>& faceIndeces, int faceNum, const std::vector<int> &face_smoothgroups, float *face_normals);
  void         CalcMaxNormalsAsInExample2(Mesh* mesh, GeometryObj* geom, const Matrix3& tm) const;
  void         ExtractUserDefinedNormals(Mesh* mesh, GeometryObj* geom, const Matrix3& tm);

  /////// helper functions for bitmaps and texmaps

  void         CropToFileHDR(BitmapTex* pBitMap, Bitmap *bmap, const std::filesystem::path& mapPath, TimeValue t);
  void         ExtractTextureMatrixFromUVGen(StdUVGen* a_uvGen, std::vector<float>& a_matrixData, int& a_pTexTilingFlags, bool & a_hasScreenMapping);
  void         SetRenderParamsImageSizeFromBitMap(HydraRenderParams* pParams, Bitmap* tobm);
  void         CopyImageFromHRRenderToBitmap(Bitmap* bmap_out, int src_w, int src_h);

  /////// post process ////////////////////////////

  Bitmap*   m_pLastRender = nullptr;


  void      CopyBitmap(Bitmap* from, Bitmap* to) const;
  void      DisplayLastRender(bool processed = true);

  void      DoDenoise(Bitmap* a_maxFrameBuff);
  void      DoIntelDenoiseLoop(Bitmap* a_bmapOut, oidn::DeviceRef& device, oidn::FilterRef& filter);   // Intel� Open Image Denoise

  void      DoPostProc(Bitmap* a_bmap, const bool a_getRawImg) const;
  void      UpdatePostProc(HydraRenderParamDlg* a_dlg);
  void      ResetPostProc (HydraRenderParamDlg* a_dlg) const;
  void      DisplaySource (HydraRenderParamDlg* a_dlg);

  /////// exploring IParamBlock2 /////////////////////

  IParamBlock2* FindBlockOfParameter(const std::wstring& a_paramName, ReferenceTarget* a_node);
  IParamBlock2* FindBlockOfParameter(const std::wstring& a_paramName, IParamBlock2* a_paramRec);  

  bool          FindBool  (const std::wstring& a_paramName, ReferenceTarget* a_node);
  int           FindInt   (const std::wstring& a_paramName, ReferenceTarget* a_node);
  float         FindFloat (const std::wstring& a_paramName, ReferenceTarget* a_node);
  Color         FindColor (const std::wstring& a_paramName, ReferenceTarget* a_node);
  AColor        FindAColor(const std::wstring& a_paramName, ReferenceTarget* a_node);
  std::wstring  FindString(const std::wstring& a_paramName, ReferenceTarget* a_node);
  Texmap*       FindTex   (const std::wstring& a_paramName, ReferenceTarget* a_node);

  void          TraceNodeCustomProperties(const std::wstring& a_objectName, ReferenceTarget* a_node,  int deep = 0);
  void          TraceIParamBlock2        (const std::wstring& a_objectName, IParamBlock2*    a_block, int deep = 0);
  void          TraceIParamBlock         (const std::wstring& a_objectName, IParamBlock*     a_block, int deep = 0);

  /////// render elements 
  bool               m_hasCalculateGBuffer = false;
  std::vector<float> m_albedoPass;
  std::vector<float> m_normalPass;

  void CopyAlphaFromGbuffer    (Bitmap* tobm) const;  
  void SaveRenderElementsToDisk(Bitmap* tobm) const;
  void GetAlbedoAndNormalFromGbuffer(const int a_width, const int a_height);

  ///// various utility functions

  std::wstring GetHydraVersionW(const std::wstring& ver) const;

  void    SaveRenderImagesEveryNminutes(RendProgressCallback* m_prog, const std::chrono::system_clock::time_point& timeStart,
    float& saveImageNext, const float elapsedMin, bool& saveFileEveryN_munutes) const;

  std::wstring getSceneName();
  void UserSelectDeviceDialog(HWND hwnd);
  bool readAndCheckDeviceSettingsFile(int &mode, std::unordered_map<int, bool> &deviceIDs);

  bool VerifyThings(int renderWidth, int renderHeight);
  static void ClearTempFolder();


  [[nodiscard]] float ToMeters(const float a_val) const
  {
#ifdef MAX2022
    const auto sysScale = static_cast<float>(GetSystemUnitScale(UNITS_METERS));
#else
    const auto sysScale = static_cast<float>(GetMasterScale(UNITS_METERS));
#endif // MAX2022

    return sysScale * a_val;
  }


  [[nodiscard]] Point3 ToMetersP3(Point3 p)
  {
    p.x = ToMeters(p.x);
    p.y = ToMeters(p.y);
    p.z = ToMeters(p.z);
    return p;
  }

  [[nodiscard]] Matrix3 ToMetersM3(Matrix3 m)
  {
    Point3 translate = m.GetTrans();
    m.SetTrans(ToMetersP3(translate));
    return m;
  }

  Matrix3 GetConversionMatrix()
  {
    Matrix3 m;
    m.SetRow(0, Point3(1.0F, 0.0F,  0.0F));
    m.SetRow(1, Point3(0.0F, 0.0F, -1.0F));
    m.SetRow(2, Point3(0.0F, 1.0F,  0.0F));
    m.SetRow(3, Point3(0.0F, 0.0F,  0.0F));
    return m;
  }


  inline int FindTextureSlot(const std::wstring* a_slotNames, const std::vector<std::wstring>& textureSlotNames, int a_size) const
  {
    for (size_t i = 0; i < textureSlotNames.size(); ++i)
    {
      for (int j = 0; j < a_size; ++j)
      {
        const std::wstring& a_slotName = a_slotNames[j];
        if (a_slotName == textureSlotNames[i])
          return (int)i;
      }
    }
    return -1;
  }
};



class hydraRenderClassDesc: public ClassDesc2
{
public:
  int          IsPublic()                       override { return TRUE; }
  void*        Create(BOOL loading = FALSE)     override { return new HydraRenderPlugin;  }
  const TCHAR* ClassName()                      override { return GetString(IDS_CLASS_NAME); }
  SClass_ID    SuperClassID()                   override { return RENDERER_CLASS_ID; }
  Class_ID     ClassID()                        override { return HydraRender_CLASS_ID; }
  const TCHAR* Category()                       override { return GetString(IDS_CATEGORY); }

  const TCHAR* InternalName()                   override { return _T("HydraRenderPlugin"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE    HInstance()                      override { return hInstance; }					// returns owning module handle

#ifdef MAX2022
  const MCHAR* NonLocalizedClassName()          override { return ClassName(); }
#endif // MAX2022

};

static hydraRenderClassDesc hydraRenderDesc;



class HydraRenderParamDlg: public RendParamDlg
{
public:
  HydraRenderPlugin* m_pRend      = nullptr;
  IRendParams*       m_iRendParam = nullptr;

  HWND m_hMain                      = nullptr;
  HWND m_hPathTracing               = nullptr;
  //HWND m_hSPPMC                   = nullptr;
  //HWND m_hSPPMD                   = nullptr;
  HWND m_hPostProc                  = nullptr;
  HWND m_hOverr                     = nullptr;
  HWND m_hDebug                     = nullptr;
  //HWND m_hMLT                     = nullptr;
  HWND m_hMMLT                      = nullptr;
  HWND m_hTools                     = nullptr;
  HWND m_hRendElem                  = nullptr;

  BOOL m_prog;

  enum rollUps {/*SPPMC, SPPMD, */PATH, POSTPROC, MAIN, OVERRIDE, DEBUG_ROLL, /*MLT,*/ MMLT, TOOLS, RENDELEM, NUM_ROLLUP };
  
  std::vector<int> m_rollupIndices;

  //********UI controls***********
  //main tab
  ICustEdit*       m_lensRadius_edit              = nullptr;
  ICustEdit*       m_timeLimit_edit               = nullptr;
  ICustEdit*       m_specNoiseFilter_edit         = nullptr; //deprecated
  ICustEdit*       m_maxRays_edit                 = nullptr;
  ICustEdit*       m_rayBounce_edit               = nullptr;
  ICustEdit*       m_diffBounce_edit              = nullptr;
                                                
  ISpinnerControl* m_rayBounce_spin               = nullptr;
  ISpinnerControl* m_diffBounce_spin              = nullptr;
                                               
  ISpinnerControl* m_lensRadius_spin              = nullptr;
  ISpinnerControl* m_timeLimit_spin               = nullptr;
  ISpinnerControl* m_specNoiseFilter_spin         = nullptr; //deprecated
  ISpinnerControl* m_maxRays_spin                 = nullptr;
                                                
  ISliderControl*  m_renderQuality_slider         = nullptr;


  //path tracing tab
  ICustEdit*       m_minRays_edit                 = nullptr; //deprecated
  ICustEdit*       m_relativeError_edit           = nullptr; //deprecated
  ICustEdit*       m_envClamp_edit                = nullptr;           
  ICustEdit*       m_bsdfClamp_edit               = nullptr; 

  ISpinnerControl* m_minRays_spin                 = nullptr; //deprecated                                                
  ISpinnerControl* m_relativeError_spin           = nullptr; //deprecated
  ISpinnerControl* m_envClamp_spin                = nullptr; 
  ISpinnerControl* m_bsdfClamp_spin               = nullptr; 

  //SPPM Caustics tab
  ICustEdit*       m_maxPhotonsCaust_edit         = nullptr;
  ICustEdit*       m_initialRadiusCaust_edit      = nullptr;
  ICustEdit*       m_powerCaust_edit              = nullptr;
  ICustEdit*       m_retraceCaust_edit            = nullptr;
  ICustEdit*       m_alphaCaust_edit              = nullptr;

  ISpinnerControl* m_maxPhotonsCaust_spin         = nullptr;
  ISpinnerControl* m_initialRadiusCaust_spin      = nullptr;
  ISpinnerControl* m_powerCaust_spin              = nullptr;
  ISpinnerControl* retrace_c_spin                 = nullptr;
  ISpinnerControl* alpha_c_spin                   = nullptr;

  //SPPM Diffuse tab
  ICustEdit*       m_maxPhotonsDiffuse_edit       = nullptr;
  ICustEdit*       m_initialRadiusDiffuse_edit    = nullptr;
  ICustEdit*       m_retraceDiffuse_edit          = nullptr;
  ICustEdit*       m_alphaDiffuse_edit            = nullptr;
                                                
  ISpinnerControl* m_maxPhotonsDiffuse_spin       = nullptr;
  ISpinnerControl* m_initialRadiusDiffuse_spin    = nullptr;
  ISpinnerControl* m_retraceDiffuse_spin          = nullptr;
  ISpinnerControl* m_alphaDiffuse_spin            = nullptr;


  //Post processing tab
  ICustButton*     m_denoise_btn                  = nullptr;
                                                
  ICustEdit*       m_exposure_edit                = nullptr;
  ICustEdit*       m_compress_edit                = nullptr;
  ICustEdit*       m_contrast_edit                = nullptr;
  ICustEdit*       m_saturation_edit              = nullptr;
  ICustEdit*       m_vibrance_edit                = nullptr;
  ICustEdit*       m_whiteBalance_edit            = nullptr;
  IColorSwatch*    m_whitePoint_selector          = nullptr;
  ICustEdit*       m_uniformContrast_edit         = nullptr;
  ICustEdit*       m_normalize_edit               = nullptr;
  ICustEdit*       m_chromAberr_edit              = nullptr;
  ICustEdit*       m_vignette_edit                = nullptr;
  ICustEdit*       m_sharpness_edit               = nullptr;
  ICustEdit*       m_sizeStar_edit                = nullptr;
  ICustEdit*       m_numRay_edit                  = nullptr;
  ICustEdit*       m_rotateRay_edit               = nullptr;
  ICustEdit*       m_randomAngle_edit             = nullptr;
  ICustEdit*       m_sprayRay_edit                = nullptr;
                                                
  ICustEdit*       m_denoiseLvl_edit              = nullptr;
  ICustEdit*       m_bloomRadius_edit             = nullptr;
  ICustEdit*       m_bloomStr_edit                = nullptr;
                                                
  ISliderControl*  m_exposure_slider              = nullptr;
  ISliderControl*  m_compress_slider              = nullptr;
  ISliderControl*  m_contrast_slider              = nullptr;
  ISliderControl*  m_saturation_slider            = nullptr;
  ISliderControl*  m_vibrance_slider              = nullptr;
  ISliderControl*  m_whiteBalance_slider          = nullptr;
  ISliderControl*  m_uniformContrast_slider       = nullptr;
  ISliderControl*  m_normalize_slider             = nullptr;
  ISliderControl*  m_chromAberr_slider            = nullptr;
  ISliderControl*  m_vignette_slider              = nullptr;
  ISliderControl*  m_sharpness_slider             = nullptr;
  ISliderControl*  m_sizeStar_slider              = nullptr;
  ISliderControl*  m_numRay_slider                = nullptr;
  ISliderControl*  m_rotateRay_slider             = nullptr;
  ISliderControl*  m_randomAngle_slider           = nullptr;
  ISliderControl*  m_sprayRay_slider              = nullptr;
                                                
  ISliderControl*  m_denoiseLvl_slider            = nullptr;
  ISpinnerControl* m_bloomRadius_spin             = nullptr;
  ISpinnerControl* m_bloomStr_spin                = nullptr;
                                                 
  ICustButton*     m_resetPostProc                = nullptr;
                                                 
  //Environment                                  
  ICustEdit*       m_envMult_edit                 = nullptr;
  ISpinnerControl* m_envMult_spin                 = nullptr;

  //MLT
  ICustEdit*       m_mlt_medianThreshld_edit      = nullptr;
  ICustEdit*       m_mlt_burnIters_edit           = nullptr;
  ICustEdit*       m_mlt_plarge_edit              = nullptr;
  ICustEdit*       m_mlt_itersMult_edit           = nullptr;
                                                  
  ISliderControl*  m_mlt_burnIters_slider         = nullptr;
  ISliderControl*  m_mlt_plarge_slider            = nullptr;
  ISliderControl*  m_mlt_itersMult_slider         = nullptr;
                                                     
  ISpinnerControl* m_mlt_medianThreshld_spin      = nullptr;
                                                       
  //MMLT                                               
  ICustEdit*       m_mmlt_estimTime_edit          = nullptr;
  ICustEdit*       m_mmlt_burning_edit            = nullptr;
  ICustEdit*       m_mmlt_stepSize_edit           = nullptr;
  ICustEdit*       m_mmlt_threads_edit            = nullptr;
  ICustEdit*       m_mmlt_medianThreshld_edit     = nullptr;
  ICustEdit*       m_mmlt_multBrightness_edit     = nullptr;
                                                 
  ISliderControl*  m_mmlt_estimTime_slider        = nullptr;
  ISliderControl*  m_mmlt_burning_slider          = nullptr;
  ISliderControl*  m_mmlt_stepSize_slider         = nullptr;
  ISliderControl*  m_mmlt_threads_slider          = nullptr;
  ISpinnerControl* m_mmlt_medianThreshld_spin     = nullptr;
  ISpinnerControl* m_mmlt_multBrightness_spin     = nullptr;
                                                       
  //Tools                                              
  ICustButton*     m_runConvertToHydraScript_btn   = nullptr;
  ICustButton*     m_runConvertFromHydraScript_btn = nullptr;
  ICustButton*     m_runOverrideHydraMatScript_btn = nullptr;
                                                       
  //Debug                                              
  ICustEdit*       m_saveRendImagEvery_edit        = nullptr;
  ISpinnerControl* m_saveRendImagEvery_spin        = nullptr;


  //********************

  HydraRenderParamDlg(HydraRenderPlugin* a_rendr, IRendParams* a_iRendPar, BOOL a_prog);
  ~HydraRenderParamDlg() override;

  void AcceptParams()    override;
  void RejectParams()    override;
  void DeleteThis()      override { delete this; }

  void ReadGuiToRendParams(HydraRenderParams* pRendParams);

  void SetPTParamsFromSlider();
  void SetSliderFromPTParams();

  void LoadPreset(int preset_id);
  void SavePreset();
  void LoadPresetToGUI();

  void InitMainHydraDialog  (HWND hWnd);
  void InitPathTracingDialog(HWND hWnd);
  void InitSPPMCDialog      (HWND hWnd);
  void InitSPPMDDialog      (HWND hWnd);
  void InitPostProcDialog   (HWND hWnd);
  void InitOverrideDialog   (HWND hWnd);
  void InitDebugDialog      (HWND hWnd);
  void InitMLTDialog        (HWND hWnd);
  void InitMMLTDialog       (HWND hWnd);
  void InitToolsDialog      (HWND hWnd);
  void InitRendElemDialog   (HWND hWnd);
  void InitProgDialog       ();

  void RemovePage           (HWND &hPanel);
  void AddPage              (HWND &hPanel, int IDD);
};



////////////////////////////////////////////////////////////////////
// Enumerators to manage the RenderBegin()/RenderEnd() calls
////////////////////////////////////////////////////////////////////

class BeginEnum: public RefEnumProc
{
public:
  TimeValue t = 0;
  explicit BeginEnum(TimeValue startTime);
  int proc(ReferenceMaker *rm) override;
};

class EndEnum: public RefEnumProc
{
public:
  TimeValue t = 0;
  explicit EndEnum(TimeValue endTime);
  int proc(ReferenceMaker *rm) override;
};


////////////////////////////////////////////////////////////////////

void    MaxMatrix3ToFloat16(const Matrix3& pivot, float* m);
void    MaxMatrix3ToFloat16V2(const Matrix3& pivot, float* m);
float   scaleWithMatrix(float a_val, const Matrix3& a_externTransform);
void    MaxMatrix3ToFloat16MagicSwap(const Matrix3& pivot, float* m);
Matrix3 Float16ToMaxMatrix3(const float* a_m);
Matrix3 TransformMatrixFromMax(const Matrix3& m);
void    ExtractRawTransformAndAddXmlNode(pugi::xml_node& instNode, const Matrix3 pivot);

[[nodiscard]] float   MyClamp(float u, float a, float b);
[[nodiscard]] int     RealColor3ToUint32(const float real_color[3]);
[[nodiscard]] float   Luminance(const Point3& data);

float   ReadFloatParam(const char* message, const char* paramName);

std::string  MethodName(int a_name);
std::string  GetMethodPresets(int methodId);
std::filesystem::path GetCorrectTexPath (const wchar_t* a_name, IFileResolutionManager* pFRM);
std::filesystem::path GetCorrectTexPath2(const wchar_t* a_name, IFileResolutionManager* pFRM);
std::wstring AlterSavedName(const std::wstring& a_name, const std::wstring& a_alter);

size_t RoundBlocks(size_t elems, int threadsPerBlock);
void   CreateDeviceSettingsFile(HRRenderRef a_ref);
void   CreateDeviceSettingsFile(int engineType, const std::unordered_map<int,bool>& deviceIDs, HRRenderRef a_ref);
Color  EvalComponentColorAPI(const Color& color, float mult, bool multOn, bool hasTexture);
void   MinimumPauseLoop(const std::chrono::system_clock::time_point& a_timeStart, const float a_minMillsecForSleep);

std::vector<HydraRenderDevice> InitDeviceList(HWND devices_list, int mode, bool init, HRRenderRef a_renderRef);
std::vector<HydraRenderDevice> InitDeviceListInternal(HRRenderRef a_renderRef);

INT_PTR CALLBACK SelectDeviceProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam);

