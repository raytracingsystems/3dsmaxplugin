#pragma once

#include "IFilter2D.h"
#include "zimage.h"
#include "HDRImage.h"

#include <vector>
#include <string>
#include <sstream>
#include <memory>

#include "bitmap.h" // 3ds max bitmap

#include <mutex>

///// free Image functions

enum FREE_IMAGE_TYPE 
{
    FIT_UNKNOWN = 0,	// unknown type
    FIT_BITMAP = 1,	  // standard image			: 1-, 4-, 8-, 16-, 24-, 32-bit
    FIT_UINT16 = 2,	  // array of unsigned short	: unsigned 16-bit
    FIT_INT16 = 3,	  // array of short			: signed 16-bit
    FIT_UINT32 = 4,	  // array of unsigned long	: unsigned 32-bit
    FIT_INT32 = 5,	  // array of long			: signed 32-bit
    FIT_FLOAT = 6,	  // array of float			: 32-bit IEEE floating point
    FIT_DOUBLE = 7,	  // array of double			: 64-bit IEEE floating point
    FIT_COMPLEX = 8,	// array of FICOMPLEX		: 2 x 64-bit IEEE floating point
    FIT_RGB16 = 9,	  // 48-bit RGB image			: 3 x 16-bit
    FIT_RGBA16 = 10,	// 64-bit RGBA image		: 4 x 16-bit
    FIT_RGBF = 11,	  // 96-bit RGB float image	: 3 x 32-bit IEEE floating point
    FIT_RGBAF = 12	  // 128-bit RGBA float image	: 4 x 32-bit IEEE floating point
};


enum FREE_IMAGE_FORMAT 
{
    FIF_UNKNOWN = -1,
    FIF_BMP = 0,
    FIF_ICO = 1,
    FIF_JPEG = 2,
    FIF_JNG = 3,
    FIF_KOALA = 4,
    FIF_LBM = 5,
    FIF_IFF = FIF_LBM,
    FIF_MNG = 6,
    FIF_PBM = 7,
    FIF_PBMRAW = 8,
    FIF_PCD = 9,
    FIF_PCX = 10,
    FIF_PGM = 11,
    FIF_PGMRAW = 12,
    FIF_PNG = 13,
    FIF_PPM = 14,
    FIF_PPMRAW = 15,
    FIF_RAS = 16,
    FIF_TARGA = 17,
    FIF_TIFF = 18,
    FIF_WBMP = 19,
    FIF_PSD = 20,
    FIF_CUT = 21,
    FIF_XBM = 22,
    FIF_XPM = 23,
    FIF_DDS = 24,
    FIF_GIF = 25,
    FIF_HDR = 26,
    FIF_FAXG3 = 27,
    FIF_SGI = 28,
    FIF_EXR = 29,
    FIF_J2K = 30,
    FIF_JP2 = 31,
    FIF_PFM = 32,
    FIF_PICT = 33,
    FIF_RAW = 34
};

struct FIBITMAP { void* data; };

typedef FIBITMAP*      (__stdcall *P_FreeImage_AllocateT)(FREE_IMAGE_TYPE type, int width, int height, int bpp, unsigned red_mask, unsigned green_mask, unsigned blue_mask);
typedef unsigned char* (__stdcall *P_FreeImage_GetBits)(FIBITMAP* dib);
typedef int            (__stdcall *P_FreeImage_SaveU)(FREE_IMAGE_FORMAT fif, FIBITMAP *dib, const wchar_t *filename, int flags);
typedef void           (__stdcall *P_FreeImage_Unload)(FIBITMAP *dib);

// \\


struct FilterChainPresets
{
  FilterChainPresets() : enableNLM(false), enableMLAA(true), enableMLFilter(false), enableBloom(false), enableTiles(false), toneMapFilterId(0), enableMedian(false), enableMLT(false) {}
  bool enableNLM;         // if yes add NLM  to the last stage, before MLAA (MLAA should be disabled?)
  bool enableMLAA;        // if yes add MLAA to the last stage
  bool enableMLFilter;
  bool enableBloom;
  bool enableTiles;
  bool enableMedian;
  bool enableMLT;
  bool hdrFrameBuff;
  int  toneMapFilterId;   // 0 - naive, 1 - intel, 2 - andrew, 3 - ... 
};


struct FilterChainCustom
{
  float exposure;
  float compress;
  float contrast;
  float saturation;
  float whiteBalance;
  float uniformContrast;
  float normalize;
  float chromAberr;
  float vignette;

  
  FilterChainCustom() : exposure(1.0f), compress(0.0f), contrast(1.0f), saturation(1.0f), whiteBalance(0.0f), 
                        uniformContrast(0.0f), normalize(0.0f), vignette(0.0f), chromAberr(0.0f), mlRadius(10), mlSigma(2.5), 
                        mlFiterPrimary(false), finalPass(false), medianThreshold(1.0f), mlt_enableMedianFilter(false),
                        mlt_medianFilterThreshold(0.4f), noiseLevel(0.15f), pProgressBar(nullptr) {}


  int   mlRadius;
  float mlSigma;
  bool  mlFiterPrimary;

  int   bloomRadius;
  float bloomStrength;
  bool  finalPass;

  bool  medianThreshold;
  bool  mlt_enableMedianFilter;
  float mlt_medianFilterThreshold;
  float noiseLevel;
  FILTER2D_PROGRESSBAR_CALLBACK pProgressBar;
};


class PostProcessPipeline
{
public:

  PostProcessPipeline(FilterChainPresets a_presets, ImageZ* a_pImageA, Bitmap* a_bmap_out, std::ostream* pLog = nullptr);
  virtual ~PostProcessPipeline();

  void resize(int a_width, int a_height);
  void runFilterChain(FilterChainCustom a_presets);

  void freeAuxMemory();

  const float* getHDRData() const { return m_image.data(); }
  const HDRImage4f& getHDRImage() const { return m_image;  }
  void SaveHDR(const std::wstring& folderSceneName, Bitmap* from_bm);

  bool GBufferLoaded() const { return m_layersWasLoaded; }
  void LoadGBuffer(const std::string& a_folderPath, Bitmap* tobm);

  void EstimateProgressLDR(ImageZ* pImageA, float a_errorOk, int a_minRaysPerPixel);

  void AccumMLTAvgBrightness(float a_brightness);
  void InitInteractivePPFilterChain(Bitmap* a_bmap_out, FilterChainPresets a_presets);

protected:

  void MakeCommonChain(FilterChainPresets a_presets, Bitmap* a_bmap_out);

  HDRImage4f m_image;
  HDRImage4f m_imageHDRCopy;
  bool       m_layersWasLoaded;

  std::vector<float>   m_tempData;
  std::vector<uint8_t> m_tempLDR1;
  std::vector<uint8_t> m_tempLDR2;
  std::vector<uint8_t> m_alphaImage;
  int m_ldrPassNumber;

  float m_avgB;
  int   m_avgBCounter;

  //
  //
  std::vector< std::shared_ptr<IFilter2D> > m_filtersList; // IFilter2D* m_filtersList[10];

  std::shared_ptr<IFilter2D> m_pCopyFromZFilter;
  std::shared_ptr<IFilter2D> m_pToneMapFilter;
  std::shared_ptr<IFilter2D> m_pAAFilter;  // IFilter2D* 
  std::shared_ptr<IFilter2D> m_pNLMFilterLDR;

  std::shared_ptr<IFilter2D> m_pBloomPrePass;
  std::shared_ptr<IFilter2D> m_pBloomFinPass;
  std::shared_ptr<IFilter2D> m_pMedianFilter;

  std::shared_ptr<IFilter2D> m_pShowError;
  std::shared_ptr<IFilter2D> m_pDrawTiles;
  std::shared_ptr<IFilter2D> m_pAlphaFromGBuff;

  PostProcessPipeline(const PostProcessPipeline& a_rhs) {}
  PostProcessPipeline& operator=(const PostProcessPipeline& a_rhs) { return *this; }

  FilterChainPresets m_lastPresets;

  std::ostream* pErrLog;

  static HMODULE      mlaa_dll_handle;
  static PCREATEFUN_T mlaa_create_fun_ptr;

  static HMODULE      free_img_dll_handle;

  static P_FreeImage_AllocateT freeImage_AllocateT;
  static P_FreeImage_GetBits   freeImage_GetBits;
  static P_FreeImage_SaveU     freeImage_SaveU;
  static P_FreeImage_Unload    freeImage_Unload;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct MedianFilter : public FilterCommon
{
  MedianFilter();
  ~MedianFilter();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

  void SetPresets(const char* a_presetsStr);

protected:

  int m_width;
  int m_height;
  float* m_data;

  float m_threshold;

};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct CopyFromZFilter : public IFilter2D
{
  CopyFromZFilter(ImageZ* a_pInput);
  ~CopyFromZFilter();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

protected:

  int m_width;
  int m_height;
  float* m_data;

  ImageZ* m_pInput;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct CopyFromZFilterMLT : public IFilter2D
{
  CopyFromZFilterMLT(ImageZ* a_pInput, std::ostream* pLog);
  ~CopyFromZFilterMLT();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

  void SetPresets(const char* a_presetsStr);

protected:

  int m_width;
  int m_height;
  float* m_data;

  float m_avgB;
  HDRImage4f m_tempImage;

  bool  m_enableMedianFilter;
  float m_medianthreshold;

  ImageZ* m_pInput;
  std::ostream* m_pLog;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct PutAlphaChannelFilter : public IFilter2D
{
  PutAlphaChannelFilter(std::vector<uint8_t>* a_pInputImage) { m_pInputImage = a_pInputImage; }
  ~PutAlphaChannelFilter(){}
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

protected:

  std::vector<uint8_t>* m_pInputImage;

  int m_width;
  int m_height;
  float* m_data;

};

struct NaiveToneMappingFilter : public IFilter2D
{
  NaiveToneMappingFilter();
  ~NaiveToneMappingFilter();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

  void SetPresets(const char* a_presetsStr) {}

protected:

  int m_width;
  int m_height;
  float* m_data;

};

struct TestToneMapping : public IFilter2D
{
  TestToneMapping();
  ~TestToneMapping();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

  void SetPresets(const char* a_presetsStr);

protected:

  int m_width;
  int m_height;
  float* m_data;

  float m_exposure;
  float m_compress;
  float m_contrast;
  float m_saturation;
  float m_whiteBalance;
  float m_uniformContrast;
  float m_normalize;
  float m_chromAberr;
  float m_vignette;
};


typedef struct
{
  float fPowKLow;                             //fPowKLow = pow( 2.0f, kLow)
  float fPowKHigh;                            //fPowKHigh = pow( 2.0f, kHigh)
  float fPow35;                               //fPow35 = pow( 2.0f, 3.5f)
  float fFStops;                              //F stops
  float fFStopsInv;                           //Invesrse fFStops value
  float fPowExposure;                         //fPowExposure = pow( 2.0f, exposure +  2.47393f )
  float fGamma;                               //Gamma correction parameter
  float fPowGamma;                            //Scale factor
  float fDefog;                               //Defog value

} CHDRData;

void EvaluateRaw(float* inputArray, float* outputArray, CHDRData *pData, int arrayWidth, int iRow);
void ExecuteToneMappingReference(float* p_input, float* p_output, CHDRData *pData, unsigned int width, unsigned int height);
float resetFStopsParameter(float powKLow, float kHigh);

CHDRData calcPresets(float kLow = -3.0f, float kHigh = 7.5f, float exposure = 3.0f, float defog = 0.0f);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct CopyHDRToMaxBitMapFilter : public FilterCommon
{
  CopyHDRToMaxBitMapFilter(Bitmap* a_bmap_out);
  ~CopyHDRToMaxBitMapFilter();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

protected:

  int m_width;
  int m_height;
  float* m_data;

  Bitmap* bmap_out;

};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct BloomGetBrightPixels : public IFilter2D
{
  BloomGetBrightPixels();
  ~BloomGetBrightPixels();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

protected:

  int m_width;
  int m_height;

  const float* m_dataIn;
  float*       m_dataOut;

  bool m_inputAddrisOk;

};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct BloomBlurAndBlend : public IFilter2D
{
  BloomBlurAndBlend();
  ~BloomBlurAndBlend();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

  void SetPresets(const char* a_presetsStr);

protected:

  int m_width;
  int m_height;

  HDRImage4f m_brightPixelsImg;

  float m_strength;
  int   m_radius;

  const float* m_dataIn;
  float*       m_dataOut;

  bool m_inputAddrisOk;

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct BloomBlurAndBlend2 : public BloomBlurAndBlend
{
  bool Eval();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct ShowErrorFromZFilter : public IFilter2D
{
  ShowErrorFromZFilter(ImageZ* a_pInput, Bitmap* a_bmap_out);
  ~ShowErrorFromZFilter();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

protected:

  int m_width;
  int m_height;

  ImageZ* m_pInput;
  Bitmap* bmap_out;
};


struct ShowTiles : public IFilter2D
{
  ShowTiles(ImageZ* a_pInput, Bitmap* a_bmap_out);
  ~ShowTiles();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

protected:

  int m_width;
  int m_height;
  float* m_data;

  ImageZ* m_pInput;
  Bitmap* bmap_out;
};



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct NonLocalMeansLDR : public FilterCommon
{
  NonLocalMeansLDR(Bitmap* a_bmap_out);
  ~NonLocalMeansLDR();
  void Release(); // com like API

  bool Eval();    // do filter; return true if success.

  void SetInput_f4(const float* a_input, int w, int h, const char* a_slotName);
  void SetOutput_f4(float* a_output, int w, int h);

  void SetPresets(const char* a_presetsStr);

protected:

  int m_width;
  int m_height;
  float* m_data;

  bool runNow;
  float m_noiseLvl;
  Bitmap* m_bmapInLayers;
};

