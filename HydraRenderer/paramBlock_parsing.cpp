#include "HydraRenderer.h"
#include <control.h>
#include <sstream>




IParamBlock2* HydraRenderPlugin::FindBlockOfParameter(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  for (unsigned int i = 0; i < a_node->NumRefs(); ++i)
  {
    ReferenceTarget *pObjRef = a_node->GetReference(i);
    if (!pObjRef)
      continue;

    if (pObjRef->SuperClassID() == PARAMETER_BLOCK2_CLASS_ID)
    {
      IParamBlock2* foundBlock = FindBlockOfParameter(a_paramName, (IParamBlock2 *)pObjRef);
      if (foundBlock != NULL)
        return foundBlock;
    }
  }

  return NULL;
}


IParamBlock2* HydraRenderPlugin::FindBlockOfParameter(const std::wstring& a_paramName, IParamBlock2* pBlock)
{
  if (pBlock == NULL)
    return NULL;

  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = pBlock->IndextoID(p);
    const MSTR paramName = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());

    if (strData == a_paramName)
      return pBlock;

    ParamType2 typeId = pBlock->GetParameterType(pid);

    if (typeId == TYPE_PBLOCK2)
    {
      IParamBlock2* internalNode = pBlock->GetParamBlock2(pid);
      IParamBlock2* result = FindBlockOfParameter(a_paramName, internalNode);
      if (result != NULL)
        return result;
    }
  }

  return NULL;
}

float HydraRenderPlugin::FindFloat(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  IParamBlock2* pBlock = FindBlockOfParameter(a_paramName, a_node);

  if (pBlock == nullptr)
    return 0.0F;

  // must check that we do have this parameter with float type
  
  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid          = pBlock->IndextoID(p);
    const MSTR paramName       = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());

    if (strData == a_paramName)
    {
      const ParamType2 typeId = pBlock->GetParameterType(pid);

      switch (typeId)
      {
        case TYPE_ANGLE:
        case TYPE_PCNT_FRAC:
        case TYPE_WORLD:
        case TYPE_FLOAT: //-V556
        return pBlock->GetFloat(pid);
        break;
        default:
        break;
      };
    }
  }

  return 0.0F;
}

AColor HydraRenderPlugin::FindAColor(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  IParamBlock2* pBlock = FindBlockOfParameter(a_paramName, a_node);
  if (pBlock == NULL)
    return AColor(0, 0, 0);

  // must check that we do have this parameter with float type
  
  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = pBlock->IndextoID(p);
    const MSTR paramName = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());

    if (strData == a_paramName)
    {
      const ParamType2 typeId = pBlock->GetParameterType(pid);
      if (typeId == TYPE_FRGBA) return pBlock->GetAColor(pid);
      else                      return AColor(0, 0, 0);
    }
  }
  return AColor(0, 0, 0);
}

Color HydraRenderPlugin::FindColor(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  IParamBlock2* pBlock = FindBlockOfParameter(a_paramName, a_node);
  if (pBlock == NULL)
    return Color(0, 0, 0);

  // must check that we do have this parameter with float type
  //
  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = pBlock->IndextoID(p);
    const MSTR paramName = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());

    if (strData == a_paramName)
    {
      const ParamType2 typeId = pBlock->GetParameterType(pid);
      if (typeId == TYPE_RGBA) return pBlock->GetColor(pid);
      else                     return Color(0, 0, 0);        
    }
  }

  return Color(0, 0, 0);
}

int HydraRenderPlugin::FindInt(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  IParamBlock2* pPB2 = FindBlockOfParameter(a_paramName, a_node);

  if (pPB2 == NULL)
    return 0;

  // must check that we do have this parameter with float type
  for (int p = 0; p < pPB2->NumParams(); ++p)
  {
    const ParamID pid          = pPB2->IndextoID(p);
    const MSTR paramName       = pPB2->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());

    if (strData == a_paramName)
    {
      const ParamType2 typeId = pPB2->GetParameterType(pid);
      if (typeId == TYPE_INT) return pPB2->GetInt(pid);
      else                    return 0;
    }
  }

  return 0;
}

std::wstring HydraRenderPlugin::FindString(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  IParamBlock2* pBlock = FindBlockOfParameter(a_paramName, a_node);
  if (pBlock == NULL)
    return std::wstring(L"");

  // must check that we do have this parameter with float type
  //
  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = pBlock->IndextoID(p);
    const MSTR paramName = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());
    if (strData == a_paramName)
    {
      const ParamType2 typeId = pBlock->GetParameterType(pid);      
      switch (typeId)
      {
        case TYPE_FILENAME:
        case TYPE_STRING:
        {
          std::wstring pstrdata;
          if (pBlock->GetStr(pid) == NULL) pstrdata.clear();
          else                             pstrdata = strConverter::c2ws(pBlock->GetStr(pid));
          return pstrdata;
        }
        break;
        default: 
          return std::wstring();
        break;
      };
    }
  }
  return std::wstring(L"");
}

Texmap* HydraRenderPlugin::FindTex(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  IParamBlock2* pBlock = FindBlockOfParameter(a_paramName, a_node);
  if (pBlock == NULL)
    return 0;

  // must check that we do have this parameter with float type
  //
  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = pBlock->IndextoID(p);
    const MSTR paramName = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());

    if (strData == a_paramName)
    {
      const ParamType2 typeId = pBlock->GetParameterType(pid);
      if (typeId == TYPE_TEXMAP) return pBlock->GetTexmap(pid);
      else                       return 0;
    }
  }

  return 0;
}

bool HydraRenderPlugin::FindBool(const std::wstring& a_paramName, ReferenceTarget* a_node)
{
  if (a_node == nullptr) return false;

  IParamBlock2* pBlock = FindBlockOfParameter(a_paramName, a_node);
  if (pBlock == NULL)
    return false;

  // must check that we do have this parameter with bool type
  //
  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = pBlock->IndextoID(p);
    const MSTR paramName = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::c2ws(paramName.data());

    if (strData == a_paramName)
    {
      const ParamType2 typeId = pBlock->GetParameterType(pid);
      if (typeId == TYPE_BOOL) return bool(pBlock->GetInt(pid));
      else                     return false;    
    }
  }

  return false;
}



void HydraRenderPlugin::TraceNodeCustomProperties(const std::wstring& a_objectName, ReferenceTarget* a_node, int deep)
{
  if (!a_node)
    return;
  
  const unsigned int uNumReferences = a_node->NumRefs();

  if (uNumReferences == 0)
    return;

  m_paramTrace << a_objectName.c_str() << L" is " << std::endl;

  for (unsigned int i = 0; i < uNumReferences; ++i)
  {
    ReferenceTarget *pObjRef = a_node->GetReference(i);

    if (!pObjRef)
      continue;

    const ULONG classID1 = pObjRef->SuperClassID();

    if (classID1 == PARAMETER_BLOCK2_CLASS_ID)
    {
      m_paramTrace << "ClassID : " << pObjRef->ClassID().PartA() << " : " << pObjRef->ClassID().PartB() << std::endl;
      m_paramTrace << "SClassID : " << classID1 << std::endl;
      m_paramTrace << "GetReference(" << i << ")[IParamBlock2] = { " << std::endl;
      TraceIParamBlock2(a_objectName, (IParamBlock2 *)pObjRef, deep);
      m_paramTrace << "} " << std::endl;
    }
    else //if(classID1 == CUST_ATTRIB_CLASS_ID)
    {
      MSTR className;
      pObjRef->GetClassName(className);

      const std::string tempStr = strConverter::ToNarrowString(className.data());
      m_paramTrace << "unknown className =" << tempStr.c_str() << std::endl;
    }
  }

  m_paramTrace << L"end " << a_objectName.c_str() << std::endl;
}


void HydraRenderPlugin::TraceIParamBlock2(const std::wstring& a_objectName, IParamBlock2* pBlock, int deep)
{
  if (pBlock == NULL)
    return;

  ParamBlockDesc2 *desc = pBlock->GetDesc();
  const unsigned int uNumParams = desc->Count();
  const unsigned int uNumParams2 = pBlock->NumParams();

  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = pBlock->IndextoID(p);
    //ParamDef& paramDef = pBlock->GetParamDef(pid);

    const Interval valid = FOREVER;

    Color col;
    AColor acol;

    hydraStr paramName = pBlock->GetLocalName(pid);
    const std::wstring strData = strConverter::ToWideString(paramName.data());

    for (int i = 0; i < deep; i++)
      m_paramTrace << L"  ";
    m_paramTrace << strData;

    const ParamType2 typeId = pBlock->GetParameterType(pid);
    switch (typeId)
    {
      case TYPE_ANGLE:
      case TYPE_PCNT_FRAC:
      case TYPE_WORLD:
      case TYPE_FLOAT: m_paramTrace << L"\t\t: float   = " << pBlock->GetFloat(pid) << std::endl; break; //-V556
      case TYPE_TIMEVALUE:
      case TYPE_INT:   m_paramTrace << L"\t\t: integer = " << pBlock->GetInt(pid)   << std::endl; break; //-V556
      case TYPE_BOOL:  m_paramTrace << L"\t\t: boolean = " << pBlock->GetInt(pid)   << std::endl; break; 
      case TYPE_FILENAME:
      case TYPE_STRING:
      {
        const hydraChar* pstrdata = pBlock->GetStr(pid);
        if (pstrdata == NULL)
          pstrdata = _M("");
        m_paramTrace << L"\t\t: string  = " << pstrdata << std::endl;
      }
      break;

      case TYPE_RGBA: //-V556
        col = pBlock->GetColor(pid);
        m_paramTrace << L"\t\t: color   = " << col.r << " " << col.g << " " << col.b << " " << std::endl;
      break;

      case TYPE_FRGBA:
        acol = pBlock->GetAColor(pid);
        m_paramTrace << L"\t\t: colorf  = " << acol.r << " " << acol.g << " " << acol.b << " " << std::endl;
      break;

      case TYPE_PBLOCK2:
      {
        IParamBlock2* internalNode = pBlock->GetParamBlock2(pid);
        m_paramTrace << L"\t\t: IParamBlock2";
        TraceIParamBlock2(L"", internalNode, deep + 1);
      }
      break;
      case TYPE_REFTARG:
      {
        const INode* node = pBlock->GetINode(pid);
        ReferenceTarget* pRefTarget = pBlock->GetReferenceTarget(pid);
        IParamBlock2* node2 = pBlock->GetParamBlock2(pid);

        //ULONG classID1 = pObjRef->SuperClassID();
        //const unsigned int uNumReferences = pBlock->NumRefs();

        if (node != NULL)
          m_paramTrace << L"\t\t: INode(geometry) " << std::endl;
        else if (pRefTarget != NULL)
        {
          //const ULONG classId1 = pRefTarget->SuperClassID();
          const ULONG classId2 = pRefTarget->ClassID().PartA();
          m_paramTrace << L"\t\t: TYPE_REFTARG; calsssId = " << classId2;
        }
        else if (node2 != NULL)
        {
          m_paramTrace << L"\t\t: IParamBlock2(TYPE_REFTARG)";
          TraceIParamBlock2(L"", node2, deep + 1);
        }
        else
          m_paramTrace << L"\t\t: EmptyRefTarget" << std::endl;
      }
      break;

      case TYPE_TEXMAP: m_paramTrace << L"\t\t: some texmap " << std::endl; break;
      default:          m_paramTrace << " : unknown "         << std::endl; break;
    }
  }
}


void HydraRenderPlugin::TraceIParamBlock(const std::wstring& a_objectName, IParamBlock* pBlock, int deep)
{
  if (pBlock == NULL)
    return;

  for (int p = 0; p < pBlock->NumParams(); p++)
  {
    const ParamID pid = p; //pBlock->IndextoID(p);    //  AnimNumToParamNum 	

    const Interval valid = FOREVER;

    Color col;
    Point3 acol;

    //MSTR paramName  = pBlock->GetLocalName(pid); 
    //const wchar_t* strData = paramName.data();

    std::wstringstream outStr;
    outStr << p;
    const std::wstring tempStr = std::wstring(L"parameter ") + outStr.str() + std::wstring(L" ");
    const wchar_t* strData = tempStr.c_str();

    for (int i = 0; i < deep; i++)
      m_paramTrace << L"  ";
    m_paramTrace << strData;

    const ParamType typeId = pBlock->GetParameterType(pid);
    switch (typeId)
    {
      case TYPE_FLOAT: m_paramTrace << L"\t\t: float   = " << pBlock->GetFloat(pid) << std::endl; break;
      case TYPE_INT:   m_paramTrace << L"\t\t: integer = " << pBlock->GetInt(pid)   << std::endl; break;
      case TYPE_BOOL:  m_paramTrace << L"\t\t: boolean = " << pBlock->GetInt(pid)   << std::endl; break;
      case TYPE_RGBA:  
        col = pBlock->GetColor(pid);
        m_paramTrace << L"\t\t: color   = " << col.r << col.g << col.b << std::endl;
      break;
      case TYPE_POINT3:
        acol = pBlock->GetPoint3(pid);
        m_paramTrace << L"\t\t: colorf  = " << acol.x << acol.y << acol.z << std::endl;
      break;
      default:
        m_paramTrace << " : unknown " << std::endl;
      break;
    }
  }
}
