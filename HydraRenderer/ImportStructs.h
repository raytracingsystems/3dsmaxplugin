#pragma once

#include <vector>
#include <string>
#include <map>


using hydraStr  = std::wstring;
using hydraChar = wchar_t;

#include <VertexNormal.h>

using MaxSDK::VertexNormal;


struct GeometryObj
{
  GeometryObj() noexcept
  {
    clear();
  }

  float m[16];
  int n_verts;
  int n_faces;
  hydraStr mesh_id;

  std::vector<int>          material_id;
  std::vector<float>        positions;
  std::vector<int>          pos_indeces;
  std::vector<int>          face_smoothgroups;
  std::vector<VertexNormal> vnorms2;
  std::vector<int>          faceV;
  std::vector<float>        normsSpecified; // like in Open Collada plugin
  std::vector<int>          normsSpecIndices;                                                        
  std::vector<float>        tex_coords;
  std::vector<int>          tex_coords_indices;

  bool hasNegativeScale;

  float bbox[6];

  template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & m;
    ar & n_verts;
    ar & n_faces;
    ar & mesh_id;
    ar & material_id;
    ar & positions;
    ar & pos_indeces;
    ar & face_smoothgroups;
    ar & tex_coords;
    ar & tex_coords_indices;
    ar & bbox;
  }

  void clear() noexcept
  {
    n_verts          = 0;
    n_faces          = 0;

    material_id       .clear();
    positions         .clear();
    pos_indeces       .clear();
    face_smoothgroups .clear();
    tex_coords        .clear();
    tex_coords_indices.clear();

    faceV             .clear();
    normsSpecified    .clear();
    normsSpecIndices  .clear();
    vnorms2           .clear();

    hasNegativeScale = false;

    for (auto& i : bbox) i = 0.0F;
    for (auto& i : m)    i = 0.0F;    
  }

  void reserveMemory(size_t n)
  {
    n_verts           = 0;
    n_faces           = 0;

    pos_indeces       .reserve(n * 3);
    positions         .reserve(n);
    vnorms2           .reserve(n);
    material_id       .reserve(n / 10);
    tex_coords_indices.reserve(n * 3);
    tex_coords        .reserve(n);
  }
};


struct CameraObj
{
  float fov;
  float farClipPlane;
  float nearClipPlane;
  float zoom;
  float tdist;

  float direction[3];
  float upVector[3];

  float tiltX;
  float tiltY;
  float tiltShiftX;
  float tiltShiftY;

  float dofFocalDist;
  float dofStrength;
  bool  dofEnabled;

  bool  isOrtho;
  bool  isTargeted;

  float posMatrix[16];
  float targetMatrix[16];
  float worldViewMatrix[16];

  bool isAnimated;
  int frames;
  std::vector <std::string> perFrame;
  std::wstring camName;

  CameraObj() noexcept  
  {
    fov           = 45;
    nearClipPlane = 0.001F;
    farClipPlane  = 10000.0F;
    zoom          = 1.0F;
    tdist         = 1.0F;

    direction[0]  = 0.0F;
    direction[1]  = 0.0F;
    direction[2]  = 1.0F;

    upVector[0]   = 0.0F;
    upVector[1]   = 1.0F;
    upVector[2]   = 0.0F;

    tiltX         = 0.0F;
    tiltY         = 0.0F;
    tiltShiftX    = 0.0F;
    tiltShiftY    = 0.0F;

    dofFocalDist  = 1.0F;
    dofStrength   = 0.0F;
    dofEnabled    = false;

    isOrtho       = false;
    isTargeted    = false;

    for (size_t i = 0; i < 16; ++i)
    {
      posMatrix[i]       = 0;
      targetMatrix[i]    = 0;
      worldViewMatrix[i] = 0;
    }

    isAnimated = false;
    frames = 1;
  }
};


struct LightObj
{
  enum LDISTRIBTYPES { LD_UNIFORM = 0, LD_DIFFUSE = 1, LD_SPOT = 2, LD_IES = 3 };

  hydraStr     lightName;
  std::wstring lightType;
  std::wstring shadowType;
  std::wstring spotShape;
  std::wstring iesXmlData;

  std::filesystem::path envTexturePath;
  std::string  envTextureSampler;

  std::wstring skyPortalEnvMode;
  std::wstring skyPortalEnvTex;
  std::string  skyPortalEnvTexSampler;
  std::wstring shadowTexStr;
  std::string  shadowTexSamplerStr;

  ///////////////////////////////////// for skylight only
  // this will influence only on diffuse surfaces
  //
  std::filesystem::path envTexturePathSecondary;
  std::string   envTextureSamplerSecondary;
  float         colorSecondary[3];
  bool          useSeparateDiffuseLight;
  ///////////////////////////////////// for skylight only

  int lightDitributionType;

  bool useGlobal;
  bool absMapBias;
  bool overshoot;
  bool on;
  bool computeShadow;
  bool isTargeted;
  bool isVisiable;
  bool causticLight;

  float position[3];
  float direction[3];
  float color[3];

  float intensity;
  float aspect;
  float hotsize;
  float fallsize;
  float attenStart;
  float attenEnd;
  float TDist;
  float kc, kl, kq;
  float directLightStart;
  float shadowSoftness;
  float envTexAmt;
  float skyTurbidity;

  float mapBias;
  float mapRange;
  float mapSize;
  float rayBias;
  float photonMaxRadius;

  float sizeX;
  float sizeY;

  float m[16];
  float targetMatrix[16];
  float iesMatrixRot[16];

  LightObj()
  {    
    lightName                  = _M("default_name");
    lightType                  .clear();
    shadowType                 .clear();
    spotShape                  .clear();
    iesXmlData                 .clear();
                               
    envTexturePath             .clear();
    envTextureSampler          .clear();
        
    skyPortalEnvMode           = L"environment";
    skyPortalEnvTex            .clear();
    skyPortalEnvTexSampler     .clear();
    shadowTexStr               .clear();
    shadowTexSamplerStr        .clear();

    envTexturePathSecondary    .clear();
    envTextureSamplerSecondary .clear();
        
    useSeparateDiffuseLight    = false;
    
    lightDitributionType       = LD_UNIFORM;

    useGlobal                  = false;
    absMapBias                 = false;
    overshoot                  = false;
    on                         = true;
    computeShadow              = true;
    isTargeted                 = false;
    isVisiable                 = true;
    causticLight               = true;

    for (int i = 0; i < 3; i++)
    {
      position[i]              = 0;
      direction[i]             = 0;
      color[i]                 = 0;
      colorSecondary[i]        = 0;
    }

    intensity                  = 1.0F;
    aspect                     = 1.0F;
    hotsize                    = 90.0F;
    fallsize                   = 120.0F;
    attenStart                 = 1.0F;
    attenEnd                   = 1000.0F;
    TDist                      = 100.0F;
    kc                         = 1.0F;
    kl                         = 0.0F;
    kq                         = 0.0F;
    directLightStart           = 0.0F;
    shadowSoftness             = 0.0F;
    envTexAmt                  = 1.0F;
    skyTurbidity               = 1.0F;

    mapBias                    = 1.0F;
    mapRange                   = 1.0F;
    mapSize                    = 1.0F;
    rayBias                    = 1.0F;
    photonMaxRadius            = 1e6F;
                             
    sizeX                      = 1.0F;
    sizeY                      = 1.0F;


    for (int k = 0; k < 4; ++k)
    {
      for (int j = 0; j < 4; ++j)
      {
        const float val = (k == j) ? 1.0F : 0.0F;

        m           [j * 4 + k] = val;
        targetMatrix[j * 4 + k] = val;
        iesMatrixRot[j * 4 + k] = val;
      }
    }
  }
};

struct TextureObj
{
  std::wstring texName;
  std::wstring texClass;
  std::wstring mapName;
  std::wstring texFilter;
  std::wstring mapType;

  std::filesystem::path normalMapPath;
  std::filesystem::path displacementMapPath;

  std::wstring points;
  std::wstring tangents;
  std::wstring alphaSource;
  float displacementAmount;

  bool  texInvert;
  bool  crop;

  bool  useRealWorldScale;
  bool  isBump;
  bool  hasNormals;
  bool  hasDisplacement;
  bool  nmFlipRed;
  bool  nmFlipGreen;
  bool  nmSwapRedAndGreen;
  bool  needHDR;

  bool  isFalloff;
  bool  isFaloffFresnel;
  bool  isFaloffFresnelOverride;
  float faloffFresnelIOR;


  std::map<std::string, TextureObj> subTextures;

  Color col_1;
  Color col_2;


  float uOffset;
  float vOffset;
  float uTiling;
  float vTiling;

  float cporuoffs;
  float cporvoffs;
  float cropuscale;
  float cropvscale;
  float customGamma;

  float angle;
  float angleUVW[3];
  float blur;
  float blurOffset;
  float noiseAmt;
  float noiseSize;
  int   noiseLevel;
  int   texTilingFlags;
  float noisePhase;
  float texAmount;


  TextureObj()
  {
    alphaSource             = L"rgb";
    texInvert               = false;
    isBump                  = false;
    hasNormals              = false;
    hasDisplacement         = false;
    nmFlipRed               = false;
    nmFlipGreen             = false;
    nmSwapRedAndGreen       = false;
    useRealWorldScale       = false;
    crop                    = false;
    needHDR                 = false;

    isFalloff               = false;
    isFaloffFresnel         = false;
    isFaloffFresnelOverride = false;
    faloffFresnelIOR        = 1.6f;

    uOffset                 = 0.0F;
    vOffset                 = 0.0F;
    uTiling                 = 1.0F;
    vTiling                 = 1.0F;

    cporuoffs               = 0.0F;
    cporvoffs               = 0.0F;
    cropuscale              = 1.0F;
    cropvscale              = 1.0F;

    angle                   = 0;
    angleUVW[0]             = 0;
    angleUVW[1]             = 0;
    angleUVW[2]             = 0;
    blur                    = 0;
    blurOffset              = 0;
    noiseAmt                = 0;
    noiseSize               = 0;
    noiseLevel              = 0;
    texTilingFlags          = 0;
    noisePhase              = 0;
    displacementAmount      = 0.0F;
    customGamma             = 2.2f;
    texAmount               = 1.0F;
  }
};

struct MaterialObj
{
  bool sub;
  bool isHydraNative;
  bool isVrayMtl;
  bool isStringMat;
  bool isBlend;

  std::wstring materialBodyXML;
  std::wstring auxMaterialBodyXML;
  std::wstring opacityBodyXML = L"";

  int id;
  std::wstring name;
  float ambient_color[3];
  float diffuseColor[3];
  float specularColor[3];
  float filter_color[3];
  float emissionColor[3];
  float reflectColor[3];
  float m_transpColor[3];
  float fogColor[3];
  float exitColor[3];
  float m_translucColor[3];


  float shininess;
  float shine_strength;
  float transparency;
  //float wire_size;
  std::wstring shading;
  float opacity;
  float IOR;
  float opacity_falloff;
  float self_illumination;
  bool twosided;
  bool affectShadows;
  /*bool wire;
  bool wire_units; //true - units, false - pixels*/
  bool falloff; //true - out, false - in
  bool facemap;
  bool soften;
  bool noIcRecords;
  std::wstring transparency_type;
  int num_subMat;

  bool lockSpecular, hilight_gloss_tex, refr_gloss_tex, reflect_gloss_tex;

  bool displacement, displacement_invert_height_on, diffuse_mult_on, specular_mult_on, emission_mult_on, transparency_mult_on, reflect_mult_on, transpThin;
  bool specularFresnel, reflectFresnel, emissionGI, shadow_matte, translucency_mult_on;
  int specularGlossOrCos, reflectGlossOrCos, transparency_gloss_or_cos;
  float specularIor, reflectIor, specularRoughness, reflectRoughness, reflectCospower, transpCospower, specularCospower, fogMult, displaceHeight, diffuseMult, specularMult, emissionMult, transpMult, reflectMult;
  float specular_glossiness, reflect_glossiness, transparency_glossiness, bumpAmount, bumpRadius, bumpSigma, translucMult;
  int specularBrdf, reflectBrdf, reflectExtrusion;

  float diffuseRoughness;

  std::vector<TextureObj>   textures;
  std::vector<std::wstring> textureSlotNames;
  std::vector<float>        textureAmount;

  std::map<std::wstring, bool> falloffSlots;

  MaterialObj()
  {
    for (int i = 0; i < 3; i++)
    {
      ambient_color[i]            = 0;
      diffuseColor[i]             = 0;
      specularColor[i]            = 0;
      reflectColor[i]             = 0;
      m_transpColor[i]            = 0;
      filter_color[i]             = 0;
      fogColor[i]                 = 0;
      exitColor[i]                = 0;
      emissionColor[i]            = 0;
      m_translucColor[i]          = 0;
    }

    isHydraNative                 = false;
    isVrayMtl                     = false;
    isStringMat                   = false;
    displacement                  = false;
    isBlend                       = false;
    displacement_invert_height_on = false;
    diffuse_mult_on               = false;
    specular_mult_on              = false;
    emission_mult_on              = false;
    transparency_mult_on          = false;
    reflect_mult_on               = false;
    transpThin                    = false;
    noIcRecords                   = false;
    lockSpecular                  = true;
    hilight_gloss_tex             = false;
    refr_gloss_tex                = false;
    reflect_gloss_tex             = false;
    translucency_mult_on          = false;
    affectShadows                 = true;


    specularIor                   = 0;
    reflectIor                    = 0;
    reflectRoughness              = 0;
    specularRoughness             = 0;
    reflectCospower               = 0;
    transpCospower                = 0;
    specularCospower              = 0;
    fogMult                       = 0;
    displaceHeight                = 0;
    diffuseMult                   = 0;
    specularMult                  = 0;
    emissionMult                  = 0;
    transpMult                    = 0;
    reflectMult                   = 0;
    translucMult                  = 0;
    diffuseRoughness              = 0;

    specularBrdf                  = 0;
    reflectBrdf                   = 0;
    reflectExtrusion              = 0;
    shininess                     = 0;
    shine_strength                = 0;
    transparency                  = 0;
    shading.clear();;
    opacity                       = 0;
    IOR                           = 0;
    opacity_falloff               = 0;
    self_illumination             = 0;
    twosided                      = false;
    falloff                       = false;
    facemap                       = false;
    soften                        = false;
    sub                           = false;
    transparency_type.clear();

    specularGlossOrCos            = 1;
    reflectGlossOrCos             = 1;
    transparency_gloss_or_cos     = 1;
    specular_glossiness           = 1;
    reflect_glossiness            = 1;
    transparency_glossiness       = 1;
    specularFresnel               = false;
    reflectFresnel                = false;
    bumpAmount                    = 0.0F;

    shadow_matte                  = false;
    emissionGI                    = true;
    bumpRadius                    = 1.0F;
    bumpSigma                     = 1.5f;

    id                            = 0;
    name                          = L"hydra_material_null";
    num_subMat                    = 0;

    materialBodyXML   .clear();
    auxMaterialBodyXML.clear();
    falloffSlots      .clear();
  }

  inline size_t FindTextureSlot(const std::wstring* a_slotNames, size_t a_size) const
  {
    for (size_t i = 0; i < textureSlotNames.size(); ++i)
    {
      for (size_t j = 0; j < a_size; ++j)
      {
        const std::wstring& a_slotName = a_slotNames[j];
        if (a_slotName == textureSlotNames[i])
          return i;
      }
    }

    return -1;
  }

};

struct TransferContents
{
  enum { SCENE, TEXTURE_FILES, VRSCENE_FILE, CONFIG, RENDER_SETTINGS, HEADER };
  int contents;

  template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & contents;
  }

};

struct Header
{
  enum { COLLADA_PROFILE, TEXTURE_FILE };
  int contents;
  std::wstring file_name;

  template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & contents;
    ar & file_name;
  }

};

struct Included
{
  bool geometry;
  bool lights;
  bool materials;
  bool tree;
  bool spheres;

  std::string sceneDumpName;

  std::wstring render_type;

  int geomObjNum;
  int imgObjNum;

  std::wstring AccType;
  std::wstring ConstrMode;

  void IncludeAdd()
  {
    geometry = true;
    spheres = true;
    lights = false;
    materials = false;
    tree = false;

    sceneDumpName = "c:/[hydra]/pluginFiles/test.dump";
  }

  template <class Archive>
  void serialize(Archive& ar, unsigned int version)
  {
    ar & geometry;
    ar & lights;
    ar & materials;
    ar & tree;
    ar & spheres;

    ar & sceneDumpName;

    ar & geomObjNum;
    ar & imgObjNum;

    ar & AccType;
    ar & ConstrMode;
  }
};

inline static int find_interval(float x)
{
  if (fabs(x - 1.0f) < 1e-5f)
    return 10;
  else
    return (int)(x * 10);
}

static float GetCosPowerFromMaxShiness(float glosiness)
{
  float cMin = 1.0F;
  float cMax = 1000000.0F;

  float x    = glosiness;
  int k      = find_interval(x);

  if (k == 10 || x >= 0.99F)
    return cMax;
  else
  {
    double coeff[10][4] = {
        { 8.88178419700125e-14, -1.77635683940025e-14, 5, 1 }, //0-0.1
        { 357.142857142857, -35.7142857142857, 5, 1.5 }, //0.1-0.2
        { -2142.85714285714, 428.571428571429, 8.57142857142857, 2 }, //0.2-0.3
        { 428.571428571431, -42.8571428571432, 30, 5 }, //0.3-0.4
        { 2095.23809523810, -152.380952380952, 34.2857142857143, 8 }, //0.4-0.5
        { -4761.90476190476, 1809.52380952381, 66.6666666666667, 12 },//0.5-0.6
        { 9914.71215351811, 1151.38592750533, 285.714285714286, 32 }, //0.6-0.7
        { 45037.7068059246, 9161.90096119855, 813.432835820895, 82 }, //0.7-0.8
        { 167903.678757035, 183240.189801913, 3996.94423223835, 300 }, //0.8-0.9
        { -20281790.7444668, 6301358.14889336, 45682.0925553320, 2700 } //0.9-1.0
    };
    return float(coeff[k][3] + coeff[k][2] * (x - k * 0.1) + coeff[k][1] * pow((x - k * 0.1), 2) + coeff[k][0] * pow((x - k * 0.1), 3));
  }
}


