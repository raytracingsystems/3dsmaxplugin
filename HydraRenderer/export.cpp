﻿#include "HydraRenderer.h"

#include <MeshNormalSpec.h>
//#include <mentalray/imrPhysicalSkyEnvironmentMap.h>

#include <unordered_map>
#include <map>
#include <time.h>
#include <polyobj.h>
#include <NotificationAPI/NotificationAPI_Events.h>

//#include <control.h>
//#include "ISTDPLUG.H"
#include <decomp.h>
//#include "modstack.h"

////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;

#define HydraMtlLight_CLASS_ID	Class_ID(0x332b3850, 0x1e7e7dce)

static BOOL exportSelected;


int	HydraRenderPlugin::DoExport(INode* root, INode *vnode, TimeValue t)
{
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"DoExport.");
  
  clock_t start;
  clock_t finish;
  start = clock();

  PreProcess(root, t);	///recursively to the content of the nodes and place them in mtlList. Also count all renderable nodes.
  ExtractMaterialList(); ///export materials in mtlList to hydraAPI   

  hrSceneOpen(m_currentScene, HR_WRITE_DISCARD);
  ExtractAndDumpSceneData(t); ///instance geometry nodes in hydraAPI scene
  ExtractCameraFromActiveViewport(vnode, t);
  hrSceneClose(m_currentScene);

  if (this->m_rendParams.debugCheckBoxes[L"EXPORT_SCENEGRAPH"])
  {
    hrSceneOpen(m_currentScene, HR_OPEN_EXISTING);
    ExportSceneGraph(root);
    hrSceneClose(m_currentScene);
  }

  finish = clock();
  const auto exportTime  = (float)((finish - start) / (float)CLOCKS_PER_SEC);
  g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Geometry export time (sec.)", exportTime);

  return 1;
}

HRMeshRef createCube(int mat_id)
{
  //uint32_t numberVertices = 24;
  uint32_t numberIndices = 36;

  float cubeVertices[] =
  {
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, -1.0f, -1.0f, +1.0f,
    -1.0f, -1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, +1.0f, +1.0f,
    -1.0f, +1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, -1.0f, +1.0f,
    +1.0f, -1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, +1.0f, +1.0f,
    +1.0f, +1.0f, -1.0f, +1.0f
  };

  float cubeNormals[] =
  {
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, -1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, +1.0f, 0.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, -1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    0.0f, 0.0f, +1.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    -1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f,
    +1.0f, 0.0f, 0.0f, +1.0f
  };

  float cubeTexCoords[] =
  {
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    1.0f, 1.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
  };

  uint32_t cubeIndices[] =
  {
    0, 2, 1,
    0, 3, 2,
    4, 5, 6,
    4, 6, 7,
    8, 9, 10,
    8, 10, 11,
    12, 15, 14,
    12, 14, 13,
    16, 17, 18,
    16, 18, 19,
    20, 23, 22,
    20, 22, 21
  };

  auto cubeRef = hrMeshCreate(L"cube");

  hrMeshOpen(cubeRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);  
  hrMeshVertexAttribPointer4f(cubeRef, L"pos", cubeVertices);
  hrMeshVertexAttribPointer4f(cubeRef, L"norm", cubeNormals);
  hrMeshVertexAttribPointer2f(cubeRef, L"texcoord", cubeTexCoords);
  hrMeshMaterialId(cubeRef, mat_id);
  hrMeshAppendTriangles3(cubeRef, numberIndices, (int*)cubeIndices);  
  hrMeshClose(cubeRef);

  return cubeRef;
}

HRMeshRef createSphere(int mat_id)
{
  int i;
  int j;
  int numberSlices    = 64;
  float radius        = 1.0f;
  int numberParallels = numberSlices;
  int numberVertices  = (numberParallels + 1) * (numberSlices + 1);
  int numberIndices   = numberParallels * numberSlices * 3;

  float angleStep = 2.0F * pi / ((float)numberSlices);

  std::vector<float> vPos;
  std::vector<float> vNorm;
  std::vector<float> vTexCoord;
  std::vector<int>   triIndices;
  std::vector<int>   matIndices;

  vPos.resize(numberVertices * 4);
  vNorm.resize(numberVertices * 4);
  vTexCoord.resize(numberVertices * 2);

  triIndices.resize(numberIndices);
  matIndices.resize(numberIndices / 3);

  for (i = 0; i < numberParallels + 1; i++)
  {
    for (j = 0; j < numberSlices + 1; j++)
    {
      int vertexIndex = (i * (numberSlices + 1) + j) * 4;
      int normalIndex = (i * (numberSlices + 1) + j) * 4;
      int texCoordsIndex = (i * (numberSlices + 1) + j) * 2;

      vPos[vertexIndex + 0] = radius * sinf(angleStep * (float)i) * sinf(angleStep * (float)j);
      vPos[vertexIndex + 1] = radius * cosf(angleStep * (float)i);
      vPos[vertexIndex + 2] = radius * sinf(angleStep * (float)i) * cosf(angleStep * (float)j);
      vPos[vertexIndex + 3] = 1.0f;

      vNorm[normalIndex + 0] = vPos[vertexIndex + 0] / radius;
      vNorm[normalIndex + 1] = vPos[vertexIndex + 1] / radius;
      vNorm[normalIndex + 2] = vPos[vertexIndex + 2] / radius;
      vNorm[normalIndex + 3] = 1.0f;

      vTexCoord[texCoordsIndex + 0] = (float)j / (float)numberSlices;
      vTexCoord[texCoordsIndex + 1] = (1.0f - (float)i) / (float)(numberParallels - 1);
    }
  }

  int* indexBuf = triIndices.data();

  for (i = 0; i < numberParallels; i++)
  {
    for (j = 0; j < numberSlices; j++)
    {
      *indexBuf++ = i * (numberSlices + 1) + j;
      *indexBuf++ = (i + 1) * (numberSlices + 1) + j;
      *indexBuf++ = (i + 1) * (numberSlices + 1) + (j + 1);

      *indexBuf++ = i * (numberSlices + 1) + j;
      *indexBuf++ = (i + 1) * (numberSlices + 1) + (j + 1);
      *indexBuf++ = i * (numberSlices + 1) + (j + 1);

      int diff = int(indexBuf - triIndices.data());
      if (diff >= numberIndices)
        break;
    }

    int diff = int(indexBuf - triIndices.data());
    if (diff >= numberIndices)
      break;
  }

  HRMeshRef meshRef = hrMeshCreate(L"sphere");

  hrMeshOpen(meshRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);  
  hrMeshVertexAttribPointer4f(meshRef, L"pos", vPos.data());
  hrMeshVertexAttribPointer4f(meshRef, L"norm", vNorm.data());
  hrMeshVertexAttribPointer2f(meshRef, L"texcoord", vTexCoord.data());
  hrMeshMaterialId(meshRef, mat_id);
  hrMeshAppendTriangles3(meshRef, int(triIndices.size()), triIndices.data());  
  hrMeshClose(meshRef);

  return meshRef;
}



//////////////////// Export scene graph //////////////////////

static inline void WriteBox3(pugi::xml_node a_node, const Box3 &a_bbox)
{
  std::wstringstream outStream;
  outStream << std::setw(6)
    << a_bbox.pmin.x << L" " << a_bbox.pmax.x << L" "
    << a_bbox.pmin.y << L" " << a_bbox.pmax.y << L" "
    << a_bbox.pmin.z << L" " << a_bbox.pmax.z;

  a_node.force_attribute(L"bbox").set_value(outStream.str().c_str());
}

static inline void WriteMatrix43to44(pugi::xml_node a_node, const wchar_t* a_attrib_name, const Matrix3& m)
{
  std::wstringstream outStream;
  //outStream << m.GetRow(0).x << L" " << m.GetRow(0).y << L" " << m.GetRow(0).z << L" " << "0" << L" "
  //          << m.GetRow(1).x << L" " << m.GetRow(1).y << L" " << m.GetRow(1).z << L" " << "0" << L" "
  //          << m.GetRow(2).x << L" " << m.GetRow(2).y << L" " << m.GetRow(2).z << L" " << "0" << L" "
  //          << m.GetRow(3).x << L" " << m.GetRow(3).y << L" " << m.GetRow(3).z << L" " << "1";

  //transpose matrix
  outStream << m.GetRow(0).x << L" " << m.GetRow(1).x << L" " << m.GetRow(2).x << L" " << m.GetRow(3).x << L" "
            << m.GetRow(0).z << L" " << m.GetRow(1).z << L" " << m.GetRow(2).z << L" " << m.GetRow(3).z << L" "
            << m.GetRow(0).y << L" " << m.GetRow(1).y << L" " << m.GetRow(2).y << L" " << m.GetRow(3).y << L" "
            << "0"           << L" " << "0"           << L" " << "0"           << L" " << "1";

  a_node.append_attribute(a_attrib_name).set_value(outStream.str().c_str());
}


static inline void ScaleMaxToHydra(float *m)
{
  if (m[3]  != 1.0f) m[3]   /= 100.0f; //-V550
  if (m[7]  != 1.0f) m[7]   /= 100.0f; //-V550
  if (m[11] != 1.0f) m[11]  /= 100.0f; //-V550
  if (m[15] != 1.0f) m[15]  /= 100.0f; //-V550
}


void CreateSceneGraph(INode *node, pugi::xml_node xmlNode, std::unordered_map<INode*, int>& m_nodeInstancesLib, int nLevel)
{
  if (nLevel < 256)
  {
    const pugi::char_t* nodeName = node->GetName();
    pugi::xml_node child_xml = xmlNode.append_child(nodeName);


    // get instance id from node       
    int inst_id = -1;
    if (m_nodeInstancesLib.find(node) != m_nodeInstancesLib.end())
      inst_id = m_nodeInstancesLib[node];

    child_xml.append_attribute(L"inst_id").set_value(inst_id);


    // add transform matrix
    float samplerMatrix[16];
    Matrix3 matrixFinal;
    Matrix3 mt = node->GetObjectTM(TimeValue(0));
    matrixFinal = TransformMatrixFromMax(mt);
    MaxMatrix3ToFloat16V2(matrixFinal, samplerMatrix);
    child_xml.force_attribute(L"matrix");
    ScaleMaxToHydra(samplerMatrix);
    HydraXMLHelpers::WriteMatrix4x4(child_xml, L"matrix", samplerMatrix);


    // add bounding box
    //Object *ob = node->GetObjectRef();
    //ViewExp& vp = ip->GetViewExp(hWnd);
    //Box3 bbox;
    //ob->GetLocalBoundBox(TimeValue(0), node, vp, bbox);
    //WriteBox3(child_xml, bbox);


    // get this node's children and recurse it
    int nNodes = node->NumberOfChildren();
    for (int i = 0; i < nNodes; i++)
    {
      INode *child_node = node->GetChildNode(i);
      CreateSceneGraph(child_node, child_xml, g_hrPlugResMangr.m_nodeInstancesLib, nLevel + 1);
    }
  }
}

void HydraRenderPlugin::ExportSceneGraph(INode* root)
{
  pugi::xml_document doc;
  pugi::xml_node rootNode = doc.append_child(L"scene_graph");
  //auto sceneNode = hrSceneParamNode(m_currentScene);
  //auto rootNode  = sceneNode.force_child(L"scene_graph");
  //rootNode.force_attribute(L"id") = sceneNode.attribute(L"id").as_string();
  CreateSceneGraph(root, rootNode, g_hrPlugResMangr.m_nodeInstancesLib, 0);
  doc.save_file("C:\\[Hydra]\\pluginFiles\\scenelib\\00_last_scene_graph.xml", L"  ");
}


////////////////////////////////////////////////////////////


int HydraRenderPlugin::ExtractAndDumpSceneData(TimeValue t)
{
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"ExtractAndDumpSceneData.");
  m_rendParams.progCallback->SetTitle(_T("Extract and dump scene data..."));

  int geomObjNum  = 0;
  int lightsNum   = 0;
  int vertWritten = 0;

  ExtractAndInstanceMeshes(geomObjNum, vertWritten, t);
  ExtractAndInstanceLights(lightsNum, t);

  return geomObjNum;
}


void MaxMatrix3ToFloat16(const Matrix3& pivot, float* m)
{
  Point3 row1;
  Point3 row2;
  Point3 row3;
  Point3 row4;

  row1 = pivot.GetRow(0);
  row2 = pivot.GetRow(1);
  row3 = pivot.GetRow(2);
  row4 = pivot.GetRow(3);

  m[0] = row1[0];
  m[1] = row1[1];
  m[2] = row1[2];
  m[3] = 0;

  m[4] = row2[0];
  m[5] = row2[1];
  m[6] = row2[2];
  m[7] = 0;

  m[8] = row3[0];
  m[9] = row3[1];
  m[10] = row3[2];
  m[11] = 0;

  m[12] = row4[0];
  m[13] = row4[1];
  m[14] = row4[2];
  m[15] = 1;
}

void MaxMatrix3ToFloat16V2(const Matrix3& pivot, float* m)
{
  Point4 col1;
  Point4 col2;
  Point4 col3;

  col1 = pivot.GetColumn(0);
  col2 = pivot.GetColumn(1);
  col3 = pivot.GetColumn(2);

  m[0] = col1.x;
  m[1] = col1.y;
  m[2] = col1.z;
  m[3] = col1.w;

  m[4] = col2.x;
  m[5] = col2.y;
  m[6] = col2.z;
  m[7] = col2.w;

  m[8] = col3.x;
  m[9] = col3.y;
  m[10] = col3.z;
  m[11] = col3.w;

  m[12] = 0;
  m[13] = 0;
  m[14] = 0;
  m[15] = 1;
}

void MaxMatrix3ToFloat16MagicSwap(const Matrix3& pivot, float* m)
{
  Point3 row1;
  Point3 row2;
  Point3 row3; 
  Point3 row4;

  row1 = pivot.GetRow(0);
  row2 = pivot.GetRow(1);
  row3 = pivot.GetRow(2);
  row4 = pivot.GetRow(3);

  m[0] = row1[0];
  m[1] = row1[2];
  m[2] = row1[1];
  m[3] = row4[0];

  m[4] = row3[0];
  m[5] = row3[2];
  m[6] = row3[1];
  m[7] = row4[2];

  m[8] = row2[0];
  m[9] = row2[2];
  m[10] = row2[1];
  m[11] = -row4[1];

  m[12] = 0;
  m[13] = 0;
  m[14] = 0;
  m[15] = 1;
}

Matrix3 TransformMatrixFromMax(const Matrix3 &m)
{
  Matrix3 swapAxis;
  swapAxis.SetRow(0, Point3(1, 0, 0));
  swapAxis.SetRow(1, Point3(0, 0, -1));
  swapAxis.SetRow(2, Point3(0, 1, 0));
  swapAxis.SetRow(3, Point3(0, 0, 0));

  Matrix3 swapAxisInv;
  swapAxisInv.SetRow(0, Point3(1, 0, 0));
  swapAxisInv.SetRow(1, Point3(0, 0, 1));
  swapAxisInv.SetRow(2, Point3(0, -1, 0));
  swapAxisInv.SetRow(3, Point3(0, 0, 0));

  Matrix3 matrixFinal;
  matrixFinal = swapAxisInv * m*swapAxis;

  return matrixFinal;
}


Matrix3 Float16ToMaxMatrix3(const float* a_m)
{
  Matrix3 m;
  m.SetRow(0, Point3(a_m[0 * 4 + 0], a_m[0 * 4 + 1], a_m[0 * 4 + 2]));
  m.SetRow(1, Point3(a_m[1 * 4 + 0], a_m[1 * 4 + 1], a_m[1 * 4 + 2]));
  m.SetRow(2, Point3(a_m[2 * 4 + 0], a_m[2 * 4 + 1], a_m[2 * 4 + 2]));
  m.SetRow(3, Point3(a_m[3 * 4 + 0], a_m[3 * 4 + 1], a_m[3 * 4 + 2]));
  return m;
}


float scaleWithMatrix(float a_val, const Matrix3& a_externTransform)
{
  Point3 tmpVec(0.0f, a_val, 0.0f);

  Matrix3 extTransform = a_externTransform;
  extTransform.SetRow(3, Point3(0, 0, 0));

  return (extTransform*tmpVec).Length();
}

[[nodiscard]] float MyClamp(float u, float a, float b) { float r = fmax(a, u); return fmin(r, b); }

int RealColor3ToUint32(const float real_color[3])
{
  float  r = MyClamp(real_color[0] * 255.0F, 0.0F, 255.0F);
  float  g = MyClamp(real_color[1] * 255.0F, 0.0F, 255.0F);
  float  b = MyClamp(real_color[2] * 255.0F, 0.0F, 255.0F);
  //float  a = myclamp(real_color[3] * 255.0F, 0.0F, 255.0F);

  auto red     = (unsigned char)r;
  auto green   = (unsigned char)g;
  auto blue    = (unsigned char)b;
  //auto alpha = (unsigned char)a;

  //return red | (green << 8) | (blue << 16) | (alpha << 24);
  return red | (green << 8) | (blue << 16);
}


std::wstring MatrixFromAffineDecomp(const AffineParts& a_parts)
{
  Matrix3 srtm, rtm, ptm, stm, ftm, matrixFinal2;
  float matrixT2[4][4];

  ptm.IdentityMatrix();
  ptm.SetTrans(a_parts.t);
  a_parts.q.MakeMatrix(rtm);
  a_parts.u.MakeMatrix(srtm);
  stm               = ScaleMatrix(a_parts.k);
  ftm               = ScaleMatrix(Point3(a_parts.f, a_parts.f, a_parts.f));
  const Matrix3 mat = Inverse(srtm) * stm * srtm * rtm * ftm * ptm;
  matrixFinal2      = TransformMatrixFromMax(mat);
  MaxMatrix3ToFloat16V2(matrixFinal2, &matrixT2[0][0]);

  float matrx[16];
  memcpy(matrx, matrixT2, 16 * sizeof(float));

  std::wstringstream outMat;
  for (int j = 0; j < 16; j++)
    outMat << matrx[j] << L" ";

  return outMat.str();
}


void ExtractRawTransformAndAddXmlNode(pugi::xml_node& instNode, const Matrix3 pivot)
{
  float euler[3];
  AffineParts parts;
  decomp_affine(pivot, &parts);
  QuatToEuler(parts.q, euler);
  const Point3 trans  = parts.t;
  const Point3 rotate = { RadToDeg(euler[0]), RadToDeg(euler[1]), RadToDeg(euler[2]) };
  const Point3 scale  = parts.k;

  instNode.force_child(L"position").force_attribute(L"val") = strConverter::Point3ToWideString(trans).c_str();
  instNode.force_child(L"rotation").force_attribute(L"val") = strConverter::Point3ToWideString(rotate).c_str();
  instNode.force_child(L"scale").force_attribute(L"val")    = strConverter::Point3ToWideString(scale).c_str();

#ifdef DEBUG  
  // Test matrix for reconstruct transform.   
  instNode.force_child(L"matrix_check").force_attribute(L"val") = MatrixFromAffineDecomp(parts).c_str();
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


BOOL HydraRenderPlugin::TMNegParity(const Matrix3 & m)
{
  return (DotProd(CrossProd(m.GetRow(0), m.GetRow(1)), m.GetRow(2)) < 0.0) ? 1 : 0;
}


TriObject* HydraRenderPlugin::GetTriObjectFromNode(INode* node, TimeValue t, int &deleteIt)
{
  deleteIt = FALSE;
  Object *obj = node->EvalWorldState(t).obj;

  if (obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
  {
    TriObject *tri = (TriObject *)obj->ConvertToType(t, Class_ID(TRIOBJ_CLASS_ID, 0));
    // Note that the TriObject should only be deleted
    // if the pointer to it is not equal to the object
    // pointer that called ConvertToType()
    if (obj != tri) deleteIt = TRUE;
    return tri;
  }
  else
    return nullptr;
}


static const Point3 basic_tva[3] = { Point3(0.0F, 0.0F, 0.0F), Point3(1.0F, 0.0F, 0.0F), Point3(1.0F, 1.0F, 0.0F) };
static const Point3 basic_tvb[3] = { Point3(1.0F, 1.0F, 0.0F), Point3(0.0F, 1.0F, 0.0F), Point3(0.0F, 0.0F, 0.0F) };
static const int nextpt[3]       = { 1, 2, 0 };
static const int prevpt[3]       = { 2, 0, 1 };

void HydraRenderPlugin::MakeFaceUV(const Face* f, Point3 *tv) const
{
  int na;
  int nhid;
  const Point3* basetv;

  // make the invisible edge be 2->0 

  nhid = 2; //!(f->flags&EDGE_C)
  if      (!(f->flags&EDGE_A)) nhid = 0;
  else if (!(f->flags&EDGE_B)) nhid = 1;
  //else if (!(f->flags&EDGE_C)) nhid = 2;

  na = 2 - nhid;
  basetv = (f->v[prevpt[nhid]] < f->v[nhid]) ? basic_tva : basic_tvb;
  for (int i = 0; i < 3; i++)
  {
    tv[i] = basetv[na];
    na = nextpt[na];
  }
}


void AddMtlsRecursive(Mtl* a_material, std::shared_ptr<Hydra::TrackerManager> a_tracker)
{
  if (a_material->ClassID().PartA() == MULTI_CLASS_ID)
  {
    const int subMats = a_material->NumSubMtls();
    if (subMats != 0)
    {
      for (int j = 0; j < subMats; j++)
      {
        Mtl* subMtl = a_material->GetSubMtl(j);
        if (subMtl != nullptr)
          AddMtlsRecursive(subMtl, a_tracker);
      }
    }
  }
}


void HydraRenderPlugin::PreProcess(INode* node, TimeValue t)
{
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"PreProcess.");
  m_rendParams.progCallback->SetTitle(_T("Pre process..."));

  bool rendThisNode = (GetCOREInterface13()->GetRendHidden() || !node->IsNodeHidden());

  const ObjectState os = node->EvalWorldState(t);

  if (os.obj != nullptr)
  {
    switch (os.obj->SuperClassID())
    {
      case SHAPE_CLASS_ID:
        rendThisNode = rendThisNode && static_cast<ShapeObject*>(os.obj)->GetRenderable();
      case GEOMOBJECT_CLASS_ID:
      {
        if (!node->Renderable() && !rendThisNode) break;

        const Object* object = node->GetObjOrWSMRef();
        bool objectAlreadyIn = false;

        const auto nodeMtl = node->GetMtl();
        if (nodeMtl != nullptr)
          AddMtlsRecursive(nodeMtl, g_hrPlugResMangr.m_trackerManager);

        for (const auto& it : g_hrPlugResMangr.m_geoNodesPools)
        {
          if (it->GetReferencedObject() == object && it->IsNodeCompatible(node, t))
          {
            objectAlreadyIn = true;
            it->AddNode(node, t);
            break;
          }
        }

        if (!objectAlreadyIn)
        {
          const auto newPool = std::make_shared<Hydra::TrackerManager>(node, MaxSDK::NotificationAPI::NotifierType_Node_Geom, t);
          g_hrPlugResMangr.m_geoNodesPools.push_back(newPool);
        }

        break;
      }
      case LIGHT_CLASS_ID:
      {
        const Object* object      = node->GetObjOrWSMRef();
        bool objectAlreadyIn      = false;

        LightObject* light_object = static_cast<LightObject*>(os.obj);

        
        //if (light_object != nullptr) //another check just in case
        //{
        if (IsHydraSky(light_object))
          m_sunSkyNameW = node->GetName();

        for (const auto& it : g_hrPlugResMangr.m_lightNodesPools)
        {
          if (it->GetReferencedObject() == object && it->IsNodeCompatible(node, t))
          {
            objectAlreadyIn = true;
            it->AddNode(node, t);
            break;
          }
        }


        if (!objectAlreadyIn)
        {
          const auto newPool = std::make_shared<Hydra::TrackerManager>(node, MaxSDK::NotificationAPI::NotifierType_Node_Light, t);
          g_hrPlugResMangr.m_lightNodesPools.push_back(newPool);
        }
        //}
        break;
      }
      default:
        break;
    }
  }

  for (int c = 0; c < node->NumberOfChildren(); c++)
    PreProcess(node->GetChildNode(c), t);
}


std::vector<int32_t> createRemapList(const std::vector<int32_t> &repMatIDs, const std::vector<int32_t> &instanceMatIDs)
{
  std::vector<int32_t> remap_list;
  
  for (int i = 0; i < repMatIDs.size(); ++i)
  {
    auto id_to_replace = repMatIDs[i];
    int id_replacement;

    if ((instanceMatIDs.size() != repMatIDs.size()) && instanceMatIDs.size() == 1) //multi material -> material
      id_replacement = instanceMatIDs[0];
    else
      id_replacement = instanceMatIDs[std::max(std::min(i, (int)(instanceMatIDs.size())-1), 0)];

    remap_list.push_back(id_to_replace);
    remap_list.push_back(id_replacement);
  }

  return remap_list;
}

bool HasTextureInAllSubmat(const std::wstring& a_slotName, Mtl* a_mtl)
{
  if (a_mtl == nullptr) return false;

  bool hasTexture = false;

  const int subMats = a_mtl->NumSubMtls();
  if (subMats == 0)
  {
    const int numSubTex = a_mtl->NumSubTexmaps();
    for (int i = 0; i < numSubTex; ++i) // all slots in material: diffuse, reflect and etc.
    {
      const std::wstring slotName = strConverter::c2ws(a_mtl->GetSubTexmapSlotName(i).data());
      if ((slotName == a_slotName) && a_mtl->GetSubTexmap(i) != nullptr) // get texture from current slot
      {
        hasTexture = true;
        break;
      }
    }
  }
  else
  {
    for (int j = 0; j < subMats; j++)
    {
      Mtl* subMtl = a_mtl->GetSubMtl(j);
      if (subMtl)
      {
        const int numSubTex = subMtl->NumSubTexmaps();
        for (int i = 0; i < numSubTex; ++i) // all slots in material: diffuse, reflect and etc.
        {
          const std::wstring slotName = strConverter::c2ws(subMtl->GetSubTexmapSlotName(i).data());
          if ((slotName == a_slotName) && subMtl->GetSubTexmap(i) != nullptr) // get texture from current slot
          {
            hasTexture = true;
            break;
          }
        }
      }
    }
  }
  return hasTexture;
}


void HydraRenderPlugin::CreateLightFromMeshLight(HRLightRef& meshLightRef, Mtl* instMat, std::shared_ptr<Hydra::TrackerManager> geoNode)
{
  meshLightRef               = hrLightCreate(L"mesh_light");

  float mult                 = FindFloat(L"ColorMultiplier", instMat);
  Color color                = FindColor(L"Color", instMat);
  bool tint                  = FindBool(L"TextureTint", instMat);
  std::wstring colorSlotName = L"ColorMap";
  bool hasColorTexture       = false;

  std::vector<std::wstring> hydraMapNames;
  HydraMtlGetAllTexturesSlotNames(hydraMapNames);
  auto texturesSlotToRef = ExtractHydraTextures(instMat, hydraMapNames);

  if (texturesSlotToRef.find(colorSlotName) != texturesSlotToRef.end())
    hasColorTexture = true;

  Color res_color = EvalComponentColorAPI(color, mult, tint, hasColorTexture);

  hrLightOpen(meshLightRef, HR_WRITE_DISCARD);
  {
    pugi::xml_node lightNode = hrLightParamNode(meshLightRef);

    lightNode.attribute(L"type").set_value(L"area");
    lightNode.attribute(L"shape").set_value(L"mesh");
    lightNode.attribute(L"distribution").set_value(L"diffuse");
    lightNode.append_child(L"mesh").append_attribute(L"id") = g_hrPlugResMangr.m_meshRefLib[geoNode->GetReferencedObject()].id;

    pugi::xml_node intensityNode = lightNode.append_child(L"intensity");
    auto colorNode = intensityNode.append_child(L"color");
    intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(1.0f);
    intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

    if (hasColorTexture)
    {
      auto texRef  = std::get<0>(texturesSlotToRef[colorSlotName]);
      auto texNode = hrTextureBind(texRef, colorNode);

      Texmap* tex  = GetTextureFromSlot(instMat, colorSlotName);
      ExtractSampler(texNode, texturesSlotToRef[colorSlotName], tex);
    }
    else if (colorNode.child(L"texture") != nullptr)
      colorNode.remove_child(L"texture");

    if (tint)
      colorNode.force_attribute(L"tex_apply_mode").set_value(L"multiply");
  }
  hrLightClose(meshLightRef);
}


void HydraRenderPlugin::AddMatIDToInstanceMatIDs(Mtl* mtl, std::vector<int>& instanceMatIDs)
{  
  if (!mtl)
  {
    instanceMatIDs.push_back(0);
    return;
  }

  auto matId = g_hrPlugResMangr.m_materialsRefLib[mtl].id;
  if (matId == -1)
    matId = 0;

  if (m_rendParams.overrideGrayMatOn && mtl->ClassID() == HydraMtl_CLASS_ID)
  {
    IHydraMtl* const hydraMtl = dynamic_cast<IHydraMtl*>(mtl);
    if (hydraMtl && hydraMtl->GetTranspMult() < 0.5F) // exclude transparency materials
      matId = g_hrPlugResMangr.m_grayMat.id;
  }
  
  instanceMatIDs.push_back(matId);
}




void HydraRenderPlugin::ExtractAndInstanceMeshes(int& geomObjNum, int& vertWritten, TimeValue t)
{
  vertWritten = 0;
  geomObjNum  = 0;
  int exports = 0;

  g_hrPlugResMangr.m_nodeInstancesLib.clear();

  for (const auto& geoNode : g_hrPlugResMangr.m_geoNodesPools) /// all geometry node
  {
    INode* representativeNode = geoNode->GetRepresentativeNode();

    if (!representativeNode || geoNode->instancesPool.empty())
      continue;

    const ObjectState os = representativeNode->EvalWorldState(t);

    if (!os.obj || os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0))
      continue;

    const bool infInterv = os.obj->ObjectValidity(t).IsInfinite();

    ////////////////////////////////////////////////////////////////////////////////////////
    /// Extract mesh and create hydra mesh from representative (root for instance) node.
    ////////////////////////////////////////////////////////////////////////////////////////

    bool supportRemapList = true;
    for (auto& nodeInstance : geoNode->instancesPool)
    {
      if (supportRemapList)
        supportRemapList = CheckSupportRemapList(nodeInstance->GetMtl(), true);
    }

    if (!supportRemapList || geoNode->needExport || !infInterv) // if not an infinite validity interval, then there is animation in the modifiers (noise, bend, skin etc.)
    {
      HRMeshRef meshRef;
      vertWritten += ExtractMeshNoIndexing(representativeNode, meshRef, supportRemapList, t); /// create hydra mesh
          
      if (meshRef.id != -1)
      {      
        g_hrPlugResMangr.m_meshRefLib[geoNode->GetReferencedObject()] = meshRef;               /// map(3ds max Object*, HRMeshRef*) 
        exports++;
      }
    }

    ////////////////////////////////////////////////////////////
    /// Collect all materials from geo node in representMatIDs.
    ////////////////////////////////////////////////////////////
    
    std::vector<int32_t> representMatIDs;
    auto repMat = representativeNode->GetMtl();

    if (repMat)                   
    {
      if (repMat->ClassID() == MULTI_MATERIAL_CLASS_ID)
      {
        for (int i = 0; i < repMat->NumSubMtls(); ++i)
          representMatIDs.push_back(i);
      }
      else
      {
        int matId = g_hrPlugResMangr.m_materialsRefLib[repMat].id;
        if (matId == -1)
          matId = 0;
        representMatIDs.push_back(matId);
      }
    }
    else
      representMatIDs.push_back(0);


    ////////////////////////////////////////////////////////////
    /// Create all instance for representative geometry node.
    ////////////////////////////////////////////////////////////    
    
    for (auto nodeInstance : geoNode->instancesPool) 
    {      
      //////////////////////////////////////////////////////////
      /// Collect all instance material to instanceMatIDs.
      //////////////////////////////////////////////////////////

      bool hasHydraMtlLight = false;
      const auto instMat    = nodeInstance->GetMtl();
      std::vector<int32_t> instanceMatIDs;


      if (instMat != nullptr) 
      {
        if (instMat->ClassID() == MULTI_MATERIAL_CLASS_ID)
        {                 
          for (int i = 0; i < instMat->NumSubMtls(); i++)            
            AddMatIDToInstanceMatIDs(instMat->GetSubMtl(i), instanceMatIDs);
        }
        else
          AddMatIDToInstanceMatIDs(instMat, instanceMatIDs);

        // Mesh light material in scene?
        if (instMat->ClassID() == HydraMtlLight_CLASS_ID)     
          hasHydraMtlLight = true;       
      }
      else
      {
        if (m_rendParams.overrideGrayMatOn)
          instanceMatIDs.push_back(g_hrPlugResMangr.m_grayMat.id);
        else
          instanceMatIDs.push_back(0);
      }

      /////////////////////////////////////
      /// Create light for mesh light.
      /////////////////////////////////////

      HRLightRef meshLightRef;
      if (hasHydraMtlLight)              
        CreateLightFromMeshLight(meshLightRef, instMat, geoNode); 

      /////////////////////////////////////
      /// Create hydra instances.
      /////////////////////////////////////

      if (GetCOREInterface13()->GetRendHidden() || !nodeInstance->IsNodeHidden())
      {
        float matrixT[4][4];
        Matrix3 matrixFinal;
        const Matrix3 pivot = ToMetersM3(nodeInstance->GetObjectTM(t));
        matrixFinal         = TransformMatrixFromMax(pivot);
        MaxMatrix3ToFloat16V2(matrixFinal, &matrixT[0][0]);


        if (hasHydraMtlLight)        
          hrLightInstance(m_currentScene, meshLightRef, &matrixT[0][0]);        
        else
        {
          int instId = -1;

          if (!supportRemapList || m_compressMeshOnExport || instanceMatIDs == representMatIDs) // without remap list.
            instId = hrMeshInstance(m_currentScene, g_hrPlugResMangr.m_meshRefLib[geoNode->GetReferencedObject()], &matrixT[0][0]);
          
          else //with remap list.
          {
            std::vector<int32_t> remap_list = createRemapList(representMatIDs, instanceMatIDs);
            instId = hrMeshInstance(m_currentScene, g_hrPlugResMangr.m_meshRefLib[geoNode->GetReferencedObject()], &matrixT[0][0], remap_list.data(), (int32_t)remap_list.size());
          }
          g_hrPlugResMangr.m_nodeInstancesLib[nodeInstance] = instId;                    

          auto instNode = hrGetMeshInstanceNode(m_currentScene, instId);
          ExtractRawTransformAndAddXmlNode(instNode, pivot);
        }
      }

      geomObjNum += 1;
    }

    geoNode->needExport = false;
  }

  g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Exported unique meshes number", exports);
}





int HydraRenderPlugin::ExtractMeshNoIndexing(INode * a_node, HRMeshRef & a_meshRef, const bool a_supportRemapList, TimeValue t)
{
  Matrix3 tm;
  if (a_node->GetObjTMAfterWSM(t).IsIdentity())
    tm = a_node->GetObjTMAfterWSM(t);
  else
    tm = a_node->GetObjTMBeforeWSM(t);// ->GetObjTMAfterWSM(t);


  ObjectState os = a_node->EvalWorldState(t);

  if (os.obj == nullptr || (os.obj->SuperClassID() != SHAPE_CLASS_ID && os.obj->SuperClassID() != GEOMOBJECT_CLASS_ID))
  {
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, L"Calling extract mesh on invalid object.");
    return 0; // Safety net. This shouldn't happen.
  }

  // Order of the vertices. Get 'em counter clockwise if the objects is negatively scaled.
  int vx1 = 0;
  int vx2 = 1;
  int vx3 = 2;

  //BOOL negScale = TMNegParity(tm);

  BOOL  delMesh = false;
  Mesh* mesh    = nullptr;

  if (os.obj->SuperClassID() == GEOMOBJECT_CLASS_ID)
  {
    auto geomObject = dynamic_cast<GeomObject*>(os.obj);
    if(geomObject)
      mesh = geomObject->GetRenderMesh(t, a_node, m_theView, delMesh);
  }
  else if (os.obj->SuperClassID() == SHAPE_CLASS_ID)
  {
    auto shape = dynamic_cast<ShapeObject*>(os.obj);
    if (shape && shape->GetRenderable())
    {
      shape->GenerateMesh(t, GENMESH_RENDER, mesh);
      mesh = shape->GetRenderMesh(t, a_node, m_theView, delMesh);
    }
  }
  else if (os.obj->IsParticleSystem())
  {
    ParticleObject* particles = GetParticleInterface(os.obj);
    if (particles)
      mesh = particles->GetRenderMesh(t, a_node, m_theView, delMesh);
  }


  if (!mesh)
    return 0;


  const size_t nVerts = (size_t)mesh->numFaces * 3;

  if (nVerts == 0 && a_node->SuperClassID() == GEOMOBJECT_CLASS_ID)
  {
    const std::wstring nodeName = a_node->GetName();
    const std::wstring message = L"Empty node: " + nodeName;
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Warning, message);    
    return 0;
  }

  std::vector<float> positions;
  std::vector<float> normals;
  std::vector<float> texcoords;
  std::vector<int>   indices;

  const size_t nVerts2 = nVerts * 2;
  const size_t nVerts4 = nVerts * 4;

  positions.reserve(nVerts4);
  normals.  reserve(nVerts4);
  texcoords.reserve(nVerts2);
  indices.  reserve(nVerts);
  std::vector<int> materialIDs(mesh->numFaces);
  
  //const MeshNormalSpec* meshNormalSpec = mesh->GetSpecifiedNormals();
  //if (meshNormalSpec && (meshNormalSpec->GetNumFaces() <= 0 || meshNormalSpec->GetNumNormals() <= 0)) // Are any normals specified?
  //  meshNormalSpec = nullptr;     // Don't use specified normals
  
  
  Mtl* const nodeMtl = a_node->GetMtl();

  bool isMultiMat       = false;
  if (nodeMtl)
    isMultiMat = (nodeMtl->ClassID() == MULTI_MATERIAL_CLASS_ID);
        

  std::vector<VertexNormal> calcNormals;
  CalculateNormals(mesh, calcNormals);

  for (size_t faceIndex = 0; faceIndex < (size_t)mesh->numFaces; ++faceIndex)
  {
    Face& face = mesh->faces[faceIndex];
    const int faceMatID = face.getMatID();

    // Material IDs

    AddMatIdFromFaceMatId(nodeMtl, isMultiMat, a_supportRemapList, faceMatID, materialIDs, faceIndex);

    // Indices

    const auto index0 = (size_t)mesh->faces[faceIndex].v[vx1];
    const auto index1 = (size_t)mesh->faces[faceIndex].v[vx2];
    const auto index2 = (size_t)mesh->faces[faceIndex].v[vx3];

    indices.push_back((int)faceIndex * 3 + 0);
    indices.push_back((int)faceIndex * 3 + 1);
    indices.push_back((int)faceIndex * 3 + 2);

    const Matrix3 conv = GetConversionMatrix();
    Matrix3 normM      = conv;
    normM.Invert();

    // Normals

    Point3 vertexNormal0;
    Point3 vertexNormal1;
    Point3 vertexNormal2;

    if (face.smGroup != 0)
    {
      vertexNormal0           = calcNormals.at(index0).GetNormal(face.smGroup).Normalize() * conv;
      vertexNormal1           = calcNormals.at(index1).GetNormal(face.smGroup).Normalize() * conv;
      vertexNormal2           = calcNormals.at(index2).GetNormal(face.smGroup).Normalize() * conv;
    }
    else
    {
      const Point3& v0        = mesh->verts[(size_t)face.v[0]];
      const Point3& v1        = mesh->verts[(size_t)face.v[1]];
      const Point3& v2        = mesh->verts[(size_t)face.v[2]];
      const Point3 faceNormal = ((v1 - v0) ^ (v2 - v1)).Normalize();

      vertexNormal0           = faceNormal * conv;
      vertexNormal1           = vertexNormal0;
      vertexNormal2           = vertexNormal0;
    }

    normals.push_back(vertexNormal0.x);
    normals.push_back(vertexNormal0.y);
    normals.push_back(vertexNormal0.z);
    normals.push_back(1.0F);

    normals.push_back(vertexNormal1.x);
    normals.push_back(vertexNormal1.y);
    normals.push_back(vertexNormal1.z);
    normals.push_back(1.0F);

    normals.push_back(vertexNormal2.x);
    normals.push_back(vertexNormal2.y);
    normals.push_back(vertexNormal2.z);
    normals.push_back(1.0F);
    


    //Positions

    const Point3 position0 = ToMetersP3(mesh->verts[index0]) * conv;
    const Point3 position1 = ToMetersP3(mesh->verts[index1]) * conv;
    const Point3 position2 = ToMetersP3(mesh->verts[index2]) * conv;

    positions.push_back(position0.x);
    positions.push_back(position0.y);
    positions.push_back(position0.z);
    positions.push_back(1.0F);

    positions.push_back(position1.x);
    positions.push_back(position1.y);
    positions.push_back(position1.z);
    positions.push_back(1.0F);

    positions.push_back(position2.x);
    positions.push_back(position2.y);
    positions.push_back(position2.z);
    positions.push_back(1.0F);


    //Texture coordinates

    const UVVert* const mapVerts = mesh->tVerts;
    const TVFace* const mapFaces = mesh->tvFace;

    if (mapVerts && mapFaces)
    {
      const auto& uvw0           = mapVerts[(size_t)mapFaces[faceIndex].t[0]];
      const auto& uvw1           = mapVerts[(size_t)mapFaces[faceIndex].t[1]];
      const auto& uvw2           = mapVerts[(size_t)mapFaces[faceIndex].t[2]];

      texcoords.push_back(MyClamp(uvw0.x, -1e5F, 1e5F));  // the texture coordinates must not be huge, 
      texcoords.push_back(MyClamp(uvw0.y, -1e5F, 1e5F));  // otherwise there will be further problems.                                    
      texcoords.push_back(MyClamp(uvw1.x, -1e5F, 1e5F));
      texcoords.push_back(MyClamp(uvw1.y, -1e5F, 1e5F));                                        
      texcoords.push_back(MyClamp(uvw2.x, -1e5F, 1e5F));
      texcoords.push_back(MyClamp(uvw2.y, -1e5F, 1e5F));
    }
  }

  //g_hrPlugResMangr.m_pluginLog.Print     (IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Mesh        : " + std::wstring(node->GetName()));
  //g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Positions   ", positions.size());
  //g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Normals     ", normals.size());
  //g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"TexCoords   ", texcoords.size());
  //g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Indices     ", indices.size());
  //g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Material IDs", materialIDs.size());

  const bool weldVertices = m_rendParams.debugCheckBoxes[L"WELD_VERTICES(SLOW_EXPORT)"]; // this enable very heavy method, slows down about 3 times the export of geometry!

  a_meshRef = hrMeshCreate(a_node->GetName());

  hrMeshOpen(a_meshRef, HR_TRIANGLE_IND3, HR_WRITE_DISCARD);

  hrMeshVertexAttribPointer4f   (a_meshRef, L"pos",              positions.data());
  hrMeshVertexAttribPointer4f   (a_meshRef, L"norm",             normals.data());
  hrMeshVertexAttribPointer2f   (a_meshRef, L"texcoord",         texcoords.data());  
  hrMeshPrimitiveAttribPointer1i(a_meshRef, L"mind",             materialIDs.data());
  hrMeshAppendTriangles3        (a_meshRef, int(indices.size()), indices.data(), weldVertices);

  hrMeshClose(a_meshRef, m_compressMeshOnExport, m_compressMeshOnExport);

  if (delMesh)
  {
    mesh->DeleteThis();
    mesh = nullptr;
  }

  return (int)nVerts;
}


void HydraRenderPlugin::AddMatIdFromFaceMatId(Mtl* const a_nodeMtl, const bool a_isMultiMat, const bool a_supportRemapList, 
  const int a_faceMatID, std::vector<int>& a_materialIDs, const size_t a_faceIndex)
{
  if (!a_nodeMtl)
    return;
    
  int matId = 0;

  // For opacity and not cast shadow need write in polygons mat ID from lib.
  // Because this options not support remap list and need read mat ID from polygons.
  
  // The remap list itself can be used, but these parameters will be the same for all instances? 
  // It should work with remap lists, but it doesn't.


  if (a_isMultiMat && (!a_supportRemapList || m_compressMeshOnExport)) 
  {
    Mtl* const faceMat = a_nodeMtl->GetSubMtl(a_faceMatID);
    if (faceMat)
      matId = g_hrPlugResMangr.m_materialsRefLib[faceMat].id;
  }
  else if (a_isMultiMat)
    matId = a_faceMatID;  
  else
    matId = g_hrPlugResMangr.m_materialsRefLib[a_nodeMtl].id;

  if (matId == -1)
    matId = 0;

  a_materialIDs[a_faceIndex] = matId;
}

bool HydraRenderPlugin::CheckSupportRemapList(Mtl* a_mtl, const bool a_printWarning)
{
  if (!a_mtl)
    return true;

  const int subMats = a_mtl->NumSubMtls();

  if (subMats == 0)
    return true;

  bool hasAffectShadows = true;
  bool hasOpacity       = false;
  bool supportRemapList = true;

  std::wstring matName  = L"";

  for (int i = 0; i < subMats; i++)
  {
    Mtl* subMtl = a_mtl->GetSubMtl(i);

    if (!subMtl)
      continue;

    bool findMat     = false;
    Class_ID classID = subMtl->ClassID();   

    if (classID == HydraMtl_CLASS_ID)
    {
      const auto hydraMtl = dynamic_cast<IHydraMtl*>(subMtl);
      if (hydraMtl)
      {
        if (hasAffectShadows) hasAffectShadows = hydraMtl->GetAffectShadow();
        if (!hasOpacity)      hasOpacity       = hydraMtl->HasOpacity();
      }    
      findMat = true;
    }  
    else if (classID == hydraMtlTags_CLASS_ID)
    {      
      if (hasAffectShadows) hasAffectShadows = FindBool(L"AffectShadows", subMtl);
      if (!hasOpacity)      hasOpacity       = GetTextureFromSlot(subMtl, L"OpacityMap");
      findMat = true;
    }

    if (findMat)
      matName += std::wstring(subMtl->GetName()) + L", ";
  }

  if (!hasAffectShadows || hasOpacity)
  {
    supportRemapList = false;

    if (a_printWarning)
    {
      std::wstring log         = L"In the current implementation, opacity and disabling shadows for a multimaterial will work the same for all instances. ";
      std::wstring multMatName = L"Multi/Sub material name: " + std::wstring(a_mtl->GetName()) + L". ";
      log                     += multMatName + L"Sub.mat. names: " + matName + L".";
      g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Warning, log);
    }
  }

  return supportRemapList;
}