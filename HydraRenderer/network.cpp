#include "hydraRender mk3.h"
#include <unordered_map>
#include <streambuf>

#include "../HydraAppLib/HydraNetwork.h" 
#include "zimage.h"

bool g_materialProcessStart = false;
PROCESS_INFORMATION g_materialProcessInfo;

struct PluginShmemPipe : public IHydraNetPluginAPI
{
  PluginShmemPipe(const char* imageFileName, const char* messageFileName, const char* guiFile, int width, int height, 
                  const char* connectionType = "main", 
                  const std::vector<int>& a_devIdList = std::vector<int>(), 
                  std::ostream* m_pLog = nullptr);

  virtual ~PluginShmemPipe();

  void updatePresets(const char* a_presetsXML);
  bool hasConnection() const;
  bool isStatic() { return m_staticConnection; }

  void runAllRenderProcesses(RenderProcessRunParams a_params, const std::vector<HydaRenderDevice>& a_devList);
  void stopAllRenderProcesses();

  ImageZ* imageA() { return pImageA; }
  ImageZ* imageB() { return pImageB; }

  HANDLE getMtlRenderHProcess() const { return g_materialProcessInfo.hProcess; }

protected:

  ImageZ* pImageA;
  ImageZ* pImageB;

  int imageWidth();
  int imageHeight();

  void CreateConnectionMainType(const char* imageFileName, const char* messageFileName, const char* guiFile, int width, int height);
  void CreateConnectionMaterialType(const char* imageFileName, const char* messageFileName, const char* guiFile, int width, int height);

  HANDLE m_hFile, m_hImageFile, m_hGuiFile;
  HANDLE m_bvhMutex;
  char* m_shmem;
  char* m_imageShmem;
  char* m_guiShmem;

  STARTUPINFOA m_hydraStartupInfo;
  PROCESS_INFORMATION m_hydraProcessInfo;
  bool m_hydraServerStarted;
  bool m_staticConnection;

  // when multi-device mode is used
  bool m_multiDevMode;
  std::vector<PROCESS_INFORMATION> m_mdProcessList;
  std::vector<int>                 m_mdDeviceList;
  // \\

  unsigned int m_lastImageType;

  enum {MESSAGE_BUFF_SIZE = 1024};

  struct SharedBufferDataInfo* bufferInfo() { return (struct SharedBufferDataInfo*)m_shmem; }
  char* bufferData() { return m_shmem + sizeof(struct SharedBufferDataInfo); }

  struct SharedBufferDataInfo* imageBufferInfo() { return (struct SharedBufferDataInfo*)m_imageShmem; }
  char* imageBufferDara() { return m_imageShmem + sizeof(struct SharedBufferDataInfo); }

  // params that we have to remember after connection created
  //
  std::string m_connectionType;

  std::string m_guiFileName;
  std::string m_messageFileName;
  std::string m_imageFileName;

  int m_width;
  int m_height;

  std::ostream* m_pLog;
};


IHydraNetPluginAPI* CreateHydraServerConnection(int renderWidth, int renderHeight, bool inMatEditor, const std::vector<int>& a_devList)
{
  static int m_matRenderTimes = 0;

  IHydraNetPluginAPI* pImpl = nullptr;
  DWORD ticks = GetTickCount();

  std::stringstream ss;
  ss << ticks;

  std::string imageName   = std::string("HydraHDRImage_")     + ss.str();
  std::string messageName = std::string("HydraMessageShmem_") + ss.str();
  std::string guiName     = std::string("HydraGuiShmem_")     + ss.str();

  std::ostream* logPtr = &hydraRender_mk3::plugin_log.Stream();
  
	if (!inMatEditor)
  {  
    pImpl = new PluginShmemPipe(imageName.c_str(), messageName.c_str(), guiName.c_str(), renderWidth, renderHeight, "main", a_devList, logPtr);
  }
  else // if in matEditor
  {
	  if ((m_matRenderTimes != 0) && (m_matRenderTimes % 40 == 0)) // restart mtl render process each 32 render to prevent mem leaks
    {
      //hydraRender_mk3::plugin_log.Print("restart material render");
      if (hydraRender_mk3::m_pMaterialRenderConnect != nullptr)
        hydraRender_mk3::m_pMaterialRenderConnect->stopAllRenderProcesses();
      
      g_materialProcessStart = false;
    }

    pImpl = hydraRender_mk3::m_pMaterialRenderConnect;

    if (pImpl == nullptr)
    {
      hydraRender_mk3::m_pMaterialRenderConnect = new PluginShmemPipe(imageName.c_str(), messageName.c_str(), guiName.c_str(), MTL_WINDOW_SIZE, MTL_WINDOW_SIZE, "material", a_devList, logPtr);
      pImpl = hydraRender_mk3::m_pMaterialRenderConnect;
    }
  
    m_matRenderTimes++;
  }
  
  if(pImpl->hasConnection())
    return pImpl;
  else
  {
    delete pImpl;
    return nullptr;
  }

}

struct SharedBufferInfo2
{
  enum LAST_WRITER { WRITER_HYDRA_GUI = 0, WRITER_HYDRA_SERVER = 1 };

  unsigned int xmlBytesize;
  unsigned int lastWriter;
  unsigned int writerCounter;
  unsigned int dummy;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool isFileExist(const char *fileName)
{
  std::ifstream infile(fileName);
  return infile.good();
}

const int GUI_FILE_SIZE = 32768;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// PluginShmemPipe ///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PluginShmemPipe::PluginShmemPipe(const char* imageFileName, const char* messageFileName, const char* guiFileName, int width, int height, const char* connectionType, const std::vector<int>& a_devIdList, std::ostream* a_pLog) : m_hydraServerStarted(false), m_staticConnection(false), m_pLog(a_pLog)
{
  pImageA = nullptr;
  pImageB = nullptr;
  m_bvhMutex = NULL;
	m_mdDeviceList.clear();
	m_mdProcessList.clear();

  m_connectionType = connectionType;
  m_mdDeviceList   = a_devIdList;

  if (m_connectionType == "main")
  {
    m_multiDevMode = true; // to enable specular filter

    m_mdProcessList.resize(m_mdDeviceList.size());
    m_bvhMutex = CreateMutexA(NULL, FALSE, "hydraBVHMutex");
    
    CreateConnectionMainType(imageFileName, messageFileName, guiFileName, width, height);
  }
  else
  {
    m_multiDevMode = false;
    CreateConnectionMaterialType(imageFileName, messageFileName, guiFileName, width, height);
  }

}

void PluginShmemPipe::CreateConnectionMainType(const char* imageFileName, const char* messageFileName, const char* guiFileName, int width, int height)
{
  m_staticConnection = false;

  m_hImageFile = NULL; // CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, (width + 16)*(height + 16)*sizeof(float) * 4 + 1024, imageFileName);
  m_hFile      = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024 + sizeof(int) * 4, messageFileName);
  m_hGuiFile   = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, GUI_FILE_SIZE, guiFileName);

  m_shmem      = (char*)MapViewOfFile(m_hFile,      FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_imageShmem = NULL; // (char*)MapViewOfFile(m_hImageFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_guiShmem   = (char*)MapViewOfFile(m_hGuiFile,   FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);

  if (m_pLog != nullptr)
  {
    if (m_shmem == NULL || m_guiShmem == NULL)
    {
      (*m_pLog) << "[syscall failed]: CreateConnectionMainType() -> CreateFileMappingA or MapViewOfFile: " << std::endl;
      (*m_pLog) << "m_shmem    = " << m_shmem    << std::endl;
      (*m_pLog) << "m_guiShmem = " << m_guiShmem << std::endl;
    }
  }

  // init gui mem
  //
  SharedBufferInfo2* pInfo2 = (SharedBufferInfo2*)m_guiShmem;

  pInfo2->xmlBytesize   = 0;
  pInfo2->lastWriter    = 0;
  pInfo2->writerCounter = 0;
  pInfo2->dummy         = 0xFAFAFAFA; // magic end of the structure in the buffer
 
  // remember params
  //
  m_guiFileName     = guiFileName;
  m_messageFileName = messageFileName;
  m_imageFileName   = imageFileName;

  m_width  = width;
  m_height = height;
}

void PluginShmemPipe::CreateConnectionMaterialType(const char* imageFileName, const char* messageFileName, const char* guiFileName, int width, int height)
{
  m_staticConnection = true;

  m_hImageFile = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024, imageFileName);
  m_hFile      = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 1024 + sizeof(int) * 4, messageFileName);
  m_hGuiFile   = CreateFileMappingA(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, GUI_FILE_SIZE, guiFileName);


  if (m_hImageFile == NULL || m_hFile == NULL || m_hGuiFile == NULL)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "CreateConnectionMaterialType(): CreateFileMappingA failed" << std::endl;
  }

  m_imageShmem = (char*)MapViewOfFile(m_hImageFile, FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_shmem      = (char*)MapViewOfFile(m_hFile,      FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);
  m_guiShmem   = (char*)MapViewOfFile(m_hGuiFile,   FILE_MAP_WRITE | FILE_MAP_READ, 0, 0, 0);

  if (m_imageShmem == NULL || m_shmem == NULL || m_guiShmem == NULL)
  {
    if (m_pLog != nullptr)
      (*m_pLog) << "CreateConnectionMaterialType(): MapViewOfFile failed" << std::endl;
  }

  // put initial data
  //
  SharedBufferDataInfo* pInfo      = bufferInfo();
  SharedBufferDataInfo* pImageInfo = imageBufferInfo();

  pImageInfo->read    = 1;
  pImageInfo->width   = 0; // write width
  pImageInfo->height  = 0; // write height
  pImageInfo->written = 0;

  if (pInfo->written == HYDRA_SERVER_ID || pInfo->written == HYDRA_PLUGIN_ID)
  {
    memset(bufferData(), 0, 1024);
    pInfo->written = HYDRA_PLUGIN_ID;
    pInfo->read = 1;
  }
  
  SharedBufferInfo2* pInfo2 = (SharedBufferInfo2*)m_guiShmem;
  
  pInfo2->xmlBytesize   = 0;
  pInfo2->lastWriter    = 0;
  pInfo2->writerCounter = 0;
  pInfo2->dummy = 0xFAFAFAFA; // magic end of the structure in the buffer
  
  // load
  //
  std::ifstream fin("C:\\[Hydra]\\pluginFiles\\material\\settings.xml");
  std::string str((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());

  char* data = (char*)(m_guiShmem)+sizeof(SharedBufferInfo2);
  memset(data, 0, GUI_FILE_SIZE - sizeof(SharedBufferInfo2));
  memcpy(data, str.c_str(), str.length());

  // remember params
  //
  m_guiFileName     = guiFileName;
  m_messageFileName = messageFileName;
  m_imageFileName   = imageFileName;

  m_width  = width;
  m_height = height;
}


void PluginShmemPipe::runAllRenderProcesses(RenderProcessRunParams a_params, const std::vector<HydaRenderDevice>& a_devList)
{
  std::ostream* outp = m_pLog;

  // restore params
  //
  bool a_showCmd           = a_params.showConsole;
  bool a_normalPriorityCPU = a_params.normalPriorityCPU;
  bool a_debug             = a_params.debug;

  const char* guiFileName     = m_guiFileName.c_str();
  const char* messageFileName = m_messageFileName.c_str();
  const char* imageFileName   = m_imageFileName.c_str();

  int width  = m_width;
  int height = m_height;

  m_mdProcessList.resize(a_devList.size());

  if (m_connectionType == "main")
  {
    ZeroMemory(&m_hydraStartupInfo, sizeof(STARTUPINFOA));
    ZeroMemory(&m_hydraProcessInfo, sizeof(PROCESS_INFORMATION));

    m_hydraStartupInfo.cb          = sizeof(STARTUPINFO);
    m_hydraStartupInfo.dwFlags     = STARTF_USESHOWWINDOW;
    m_hydraStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;

    const char* hydraPath = "C:\\[Hydra]\\bin\\hydra.exe";

    if (!isFileExist(hydraPath))
    {
      m_hydraServerStarted = false;
    }
    else
    {
      std::stringstream ss;
      ss << "xxx yyy zzz " << " -gui_buffer_name " << guiFileName << " -image_buffer_name " << imageFileName << " -message_buffer_name " << messageFileName;

      std::stringstream ss2;
      ss2 << GetTickCount();

      std::string imgA = "hydraImageA_" + ss2.str();
      std::string imgB = "hydraImageB_" + ss2.str();

      std::string mutexA = "hydraImageAMutex_" + ss2.str();
      std::string mutexB = "hydraImageBMutex_" + ss2.str();

      const int layersNum = a_params.enableMLT ? 2 : 1;

      pImageA = new ImageZ(imgA.c_str(), mutexA.c_str(), width, height, outp, layersNum);
      pImageB = new ImageZ(imgB.c_str(), mutexB.c_str(), width, height, outp, layersNum);

      std::string tempStr = std::string(" -multidevicemode 1 -imageB ") + imgB + " -mutexB " + mutexB + " -imageA " + imgA + " -mutexA " + mutexA;

      ss << tempStr.c_str();
      ss << " -nowindow 1 ";

      if (pImageA != nullptr)
        pImageA->SendMsg("-node_t A -sid 0 -layer wait -action wait");

      int deviceId   = m_mdDeviceList.size() == 0 ? -1 : m_mdDeviceList[0];
      int engineType = 1;
      int liteMode   = false;

      std::string basicCmd = ss.str();

      m_hydraServerStarted = true;
      std::ofstream fout("C:\\[Hydra]\\pluginFiles\\zcmd.txt");

      for (size_t i = 0; i < m_mdDeviceList.size(); i++)
      {
        DWORD dwCreationFlags = a_showCmd ? 0 : CREATE_NO_WINDOW;

        int devId = m_mdDeviceList[i];
        if (isTargetDevIdACPU(devId, a_devList))
        {
          if (isTargetDevIdAHydraCPU(devId, a_devList))
          {
            devId *= -1;
            if (m_mdDeviceList.size() != 1 && !a_normalPriorityCPU)
              dwCreationFlags |= BELOW_NORMAL_PRIORITY_CLASS; // is CPU is the only one device, use normal priority, else use background mode
          }
        }
        
        std::stringstream ss3;

        if (engineType == 1)
          ss3 << " -cl_enable 1 -cl_device_id " << devId << " -cl_lite_core " << liteMode;
        else
          ss3 << " -cl_enable 0 -cl_device_id " << devId;

        std::string cmdFull = basicCmd + ss3.str();

        ZeroMemory(&m_mdProcessList[i], sizeof(PROCESS_INFORMATION));
        if (!a_debug)
        {
          m_hydraServerStarted = m_hydraServerStarted && CreateProcessA(hydraPath, (LPSTR)cmdFull.c_str(), NULL, NULL, FALSE, dwCreationFlags, NULL, NULL, &m_hydraStartupInfo, &m_mdProcessList[i]);
          if (!m_hydraServerStarted && outp != nullptr)
          {
            (*outp) << "[syscall failed]: runAllRenderProcesses->(m_connectionType == 'main')->CreateProcessA " << std::endl;
          }
        }

        fout << cmdFull.c_str() << std::endl;
      }

      fout.close();

    }
  }
  else if (!g_materialProcessStart)
  {
    ZeroMemory(&m_hydraStartupInfo, sizeof(STARTUPINFOA));
    ZeroMemory(&m_hydraProcessInfo, sizeof(PROCESS_INFORMATION));

    m_hydraStartupInfo.cb          = sizeof(STARTUPINFO);
    m_hydraStartupInfo.dwFlags     = STARTF_USESHOWWINDOW;
    m_hydraStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;


    const char* hydraPath = "C:\\[Hydra]\\bin\\hydra.exe";
    const char* matScenePath = "C:\\[Hydra]\\pluginFiles\\material\\";

    if (!isFileExist(hydraPath))
    {
      m_hydraServerStarted = false;
    }
    else
    {
      ZeroMemory(&m_hydraStartupInfo, sizeof(STARTUPINFOA));
      ZeroMemory(&m_hydraProcessInfo, sizeof(PROCESS_INFORMATION));

      m_hydraStartupInfo.cb = sizeof(STARTUPINFO);
      m_hydraStartupInfo.dwFlags = STARTF_USESHOWWINDOW;
      m_hydraStartupInfo.wShowWindow = SW_SHOWMINNOACTIVE;

      std::stringstream ss;
      ss << hydraPath << " " << matScenePath << "scene.vsgf " << matScenePath << "hydra_profile_generated.xml " << matScenePath << "settings.xml -out " << matScenePath << "z_material.imagef4 -outimagef4 1 -nobvhcache 1 -inmaterialditor 1 -wait 1" << " -gui_buffer_name " << guiFileName << " -image_buffer_name " << imageFileName << " -message_buffer_name " << messageFileName;

      if (1)
      {
        std::stringstream ss2;
        ss2 << GetTickCount();

        std::string imgA = "hydraImageA_" + ss2.str();
        std::string imgB = "hydraImageB_" + ss2.str();

        std::string mutexA = "hydraImageAMutex_" + ss2.str();
        std::string mutexB = "hydraImageBMutex_" + ss2.str();

        pImageA = new ImageZ(imgA.c_str(), mutexA.c_str(), width, height, outp);
        pImageB = new ImageZ(imgB.c_str(), mutexB.c_str(), width, height, outp);

        std::string tempStr = std::string(" -multidevicemode 1 -imageB ") + imgB + " -mutexB " + mutexB + " -imageA " + imgA + " -mutexA " + mutexA;
        ss << tempStr.c_str();
      }

      ss << " -nowindow 1 ";

      int deviceId   =(hydraRender_mk3::m_lastRendParams.device_id.size() == 0) ? -1 : hydraRender_mk3::m_lastRendParams.device_id.at(0); // pRendParams->
      int engineType = hydraRender_mk3::m_lastRendParams.engine_type;
      int liteMode   = hydraRender_mk3::m_lastRendParams.liteMode;

      DWORD dwCreationFlags = a_showCmd ? 0 : CREATE_NO_WINDOW;

      if (isTargetDevIdACPU(deviceId, a_devList))
      {
        if (isTargetDevIdAHydraCPU(deviceId, a_devList))
          deviceId *= -1;
        //dwCreationFlags |= BELOW_NORMAL_PRIORITY_CLASS;
      }

      hydraRender_mk3::m_engineTypeMatRender = deviceId;
      hydraRender_mk3::m_deviceIdMatRender   = engineType;
      hydraRender_mk3::m_liteCoreMatRender   = liteMode;

      if (engineType == 1)
        ss << " -cl_enable 1 -cl_device_id " << deviceId << " -cl_lite_core " << liteMode;
      else
        ss << " -cl_enable 0 -cl_device_id " << deviceId << " -cl_lite_core ";

      if (a_params.compileShaders)
        ss << " -compile_shaders 1 ";

      m_hydraServerStarted = CreateProcessA("C:\\[Hydra]\\bin\\hydra.exe", (LPSTR)ss.str().c_str(), NULL, NULL, FALSE, dwCreationFlags, NULL, NULL, &m_hydraStartupInfo, &m_hydraProcessInfo); // CREATE_NEW_CONSOLE, CREATE_NO_WINDOW
      if (!m_hydraServerStarted && outp != nullptr)
        (*outp) << "[syscall failed]: runAllRenderProcesses->(m_connectionType == 'material')->CreateProcessA " << std::endl;

      std::ofstream fout("C:\\[Hydra]\\pluginFiles\\material\\cmd.txt");
      fout << ss.str().c_str() << std::endl;
      fout.close();
    }

    if (a_params.compileShaders)
      g_materialProcessStart = false;
    else
      g_materialProcessStart = true;

    g_materialProcessInfo  = m_hydraProcessInfo;
  }

}


void PluginShmemPipe::stopAllRenderProcesses()
{

  if (m_multiDevMode && m_hydraServerStarted)
  {
    for (auto i = 0; i < m_mdProcessList.size(); i++)
    {
      if (m_mdProcessList[i].hProcess == 0 || m_mdProcessList[i].hProcess == INVALID_HANDLE_VALUE)
        continue;

      DWORD exitCode;
      GetExitCodeProcess(m_mdProcessList[i].hProcess, &exitCode);

      if (exitCode == STILL_ACTIVE)
        Sleep(100);

      GetExitCodeProcess(m_mdProcessList[i].hProcess, &exitCode);

      if (exitCode == STILL_ACTIVE)
        TerminateProcess(m_mdProcessList[i].hProcess, NO_ERROR);
    }
  }
  else if (m_hydraServerStarted && m_hydraProcessInfo.hProcess != 0 && m_hydraProcessInfo.hProcess != INVALID_HANDLE_VALUE)
  {
    // ask process for exit
    //
    DWORD exitCode;
    GetExitCodeProcess(m_hydraProcessInfo.hProcess, &exitCode);

    if (exitCode == STILL_ACTIVE)
      Sleep(100);

    GetExitCodeProcess(m_hydraProcessInfo.hProcess, &exitCode);

    if (exitCode == STILL_ACTIVE)
      TerminateProcess(m_hydraProcessInfo.hProcess, NO_ERROR);
  }


}

PluginShmemPipe::~PluginShmemPipe()
{
  stopAllRenderProcesses();

  if (m_bvhMutex != NULL)
    CloseHandle(m_bvhMutex);

  delete pImageA; pImageA = nullptr;
  delete pImageB; pImageB = nullptr;

  UnmapViewOfFile(m_hImageFile); CloseHandle(m_hImageFile); m_hImageFile = INVALID_HANDLE_VALUE;
  UnmapViewOfFile(m_hFile);      CloseHandle(m_hFile);      m_hFile      = INVALID_HANDLE_VALUE;
  UnmapViewOfFile(m_hGuiFile);   CloseHandle(m_hGuiFile);   m_hGuiFile   = INVALID_HANDLE_VALUE;
}


bool  PluginShmemPipe::hasConnection() const 
{ 
  return (m_hFile != NULL) && (m_hGuiFile != NULL);
}



void PluginShmemPipe::updatePresets(const char* a_presetsXML)
{
  SharedBufferInfo2* pInfo = (SharedBufferInfo2*)m_guiShmem;
  char* data = m_guiShmem + sizeof(SharedBufferInfo2);

  //memset(m_guiShmem, 0, GUI_FILE_SIZE);
  strcpy(data, a_presetsXML);

  pInfo->xmlBytesize   = strlen(a_presetsXML);
  pInfo->lastWriter    = 0; 
  pInfo->dummy         = 0xFAFAFAFA; // magic end of the structure in the buffer
  pInfo->writerCounter = pInfo->writerCounter+2;

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void hydraRender_mk3::PutImageLDR(HydraRenderParams rendParams, const HDRImage4f& a_image, Bitmap *bmap_out)
{
  int m_width  = bmap_out->Width();
  int m_height = bmap_out->Height();

  const float fhInv = 1.0f / float(m_height);
  const float fwInv = 1.0f / float(m_width);

  if (a_image.width() == 0 || a_image.height() == 0)
  {
    plugin_log.Print("[error]: hydraRender_mk3::PutImageLDR, a_image have zero size");
    return;
  }

  std::vector<BMM_Color_fl> line(m_width);

  for (int y = 0; y < m_height; y++)
  {
    float texCoordY = (float(y) + 0.5f)*fhInv;

    for (int x = 0; x < m_width; x++)
    {
      int   indexSrc  = y*m_width + x;
      float texCoordX = (float(x) + 0.5f)*fwInv;

      float color[4];
      a_image.sample(texCoordX, texCoordY, color);

      float r = color[0];
      float g = color[1];
      float b = color[2];
      float a = color[3];

      line[x] = BMM_Color_fl(r, g, b, a);
    }

    bmap_out->PutPixels(0, m_height - y - 1, m_width, &line[0]);
  }

  
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace ZImageUtils
{
  uint32_t IndexZB(int x, int y, int pitch);
};

bool hydraRender_mk3::CopyFromZImageToMaxBitMap(ImageZ* self, Bitmap* tobm, const HydraRenderParams& rendParams)
{
  if (tobm == nullptr || self == nullptr)
    return false;

  //
  ULONG ctype;
  auto chan = tobm->ChannelsPresent();
  RealPixel* pGbufRealPix = (RealPixel*)tobm->GetChannel(BMM_CHAN_REALPIX, ctype);
  // \\

  int renderWidth  = rendParams.nMaxx - rendParams.nMinx;
  int renderHeight = rendParams.nMaxy - rendParams.nMiny;

  int zimageWidth  = self->Width();
  int zimageHeight = self->Height();

  float* color = self->ColorPtr();

  std::vector<BMM_Color_fl> line(renderWidth);

  for (int y = rendParams.nMiny; y < rendParams.nMaxy; y++)
  {
    for (int x = 0; x < renderWidth; x++)       
    {
      auto indexSrc = ZImageUtils::IndexZB(x, zimageHeight - y - 1, zimageWidth);

      float r = color[indexSrc * 4 + 0];
      float g = color[indexSrc * 4 + 1];
      float b = color[indexSrc * 4 + 2];
      float a = color[indexSrc * 4 + 3];

      if (pGbufRealPix != nullptr)
      {
        int gbufIndex = y*rendParams.devWidth + x;

        RealPixel rpix;
        rpix = MakeRealPixel(r, g, b);
        pGbufRealPix[gbufIndex] = rpix;
      }

      line[x] = BMM_Color_fl(r, g, b, a);
    }

    tobm->PutPixels(0, y, renderWidth, &line[0]);
  }

  return true;
}

bool hydraRender_mk3::CopyFromZImageToHdrBuff(ImageZ* self, float* pOut)
{
  if (pOut == nullptr || self == nullptr)
    return false;

  int zimageWidth  = self->Width();
  int zimageHeight = self->Height();
  float* color     = self->ColorPtr();

  for (int y = 0; y < zimageHeight; y++)
  {
    for (int x = 0; x < zimageWidth; x++)
    {
      auto indexSrc = ZImageUtils::IndexZB(x, zimageHeight - y - 1, zimageWidth);

      float r = color[indexSrc * 4 + 0];
      float g = color[indexSrc * 4 + 1];
      float b = color[indexSrc * 4 + 2];
      float a = color[indexSrc * 4 + 3];

      int index2 = (zimageHeight - y - 1)*zimageWidth + x;

      pOut[index2 * 4 + 0] = r;
      pOut[index2 * 4 + 1] = g;
      pOut[index2 * 4 + 2] = b;
      pOut[index2 * 4 + 3] = a;
    }
  }

  return true;
}



bool isTargetDevIdACPU(int a_devId, const std::vector<HydaRenderDevice>& a_devList)
{
  bool res = false;

  for (size_t i = 0; i < a_devList.size(); i++)
  {
    if (a_devList[i].id == a_devId)
    {
      res = a_devList[i].isCPU;
      break;
    }
  }

  return res;
}


bool isTargetDevIdAHydraCPU(int a_devId, const std::vector<HydaRenderDevice>& a_devList)
{
  bool res = false;

  for (size_t i = 0; i < a_devList.size(); i++)
  {
    if (a_devList[i].id == a_devId)
    {
      res = (a_devList[i].name == L"Hydra CPU");
      break;
    }
  }

  return res;
}

