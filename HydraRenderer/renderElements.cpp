#include "HydraRenderer.h"
#include "qmc_sobol_niederreiter.h"

////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;


void HydraRenderPlugin::CopyAlphaFromGbuffer(Bitmap* tobm) const
{
  if (!tobm)
    return;

  const int w = tobm->Width();
  const int h = tobm->Height();

  hrRenderLockFrameBufferUpdate(g_hrPlugResMangr.m_rendRef);

  if (m_rendParams.renElemAlphaOn)
  {
#pragma omp parallel for num_threads(omp_get_num_procs())
    for (int y = 0; y < h; ++y)
    {
      std::vector<float>          line(w * 4); // line[x*4 + 0] - red; line[x*4 + 3] - alpha; x from 0 to w-1; 
      std::vector<HRGBufferPixel> lineGBuff(w);
      tobm->GetPixels(0, h - y - 1, w, (BMM_Color_fl*)line.data());
      hrRenderGetGBufferLine(g_hrPlugResMangr.m_rendRef, y, lineGBuff.data(), 0, w);

      for (int x = 0; x < w; ++x)
        line[x * 4 + 3] = lineGBuff[x].rgba[3];

      tobm->PutPixels(0, h - y - 1, w, (BMM_Color_fl*)line.data());
    }
  }
  else
  {
#pragma omp parallel for num_threads(omp_get_num_procs())
    for (int y = 0; y < h; ++y)
    {
      std::vector<float> line(w * 4); // line[x*4 + 0] - red; line[x*4 + 3] - alpha; x from 0 to w-1; 
      tobm->GetPixels(0, h - y - 1, w, (BMM_Color_fl*)line.data());

      for (int x = 0; x < w; ++x)
        line[x * 4 + 3] = 1.0F;

      tobm->PutPixels(0, h - y - 1, w, (BMM_Color_fl*)line.data());
    }
  }
  hrRenderUnlockFrameBufferUpdate(g_hrPlugResMangr.m_rendRef);
}


void HydraRenderPlugin::SaveRenderElementsToDisk(Bitmap* tobm) const
{
  if (m_rendParams.rendSaveFile)
  {
    //const int w = tobm->Width();
    //std::vector<float>          line(w * 4); // line[x*4 + 0] - red; line[x*4 + 3] - alpha; x from 0 to w-1; 
    //std::vector<HRGBufferPixel> lineGBuff(w);

    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"SaveRenderElementsToDisk.");
    
    BitmapInfo bi = GetCOREInterface()->GetRendFileBI();
    
    const std::filesystem::path biFilePath = bi.Name();
    const std::filesystem::path perantPath = biFilePath.parent_path();
    const std::wstring stemFileName        = biFilePath.stem().generic_wstring();
    const std::wstring ext                 = biFilePath.extension().generic_wstring();


    const int currFrameInt = bi.CurrentFrame() + 1;
    if (GetCOREInterface()->GetRendTimeType() == REND_TIMESINGLE) BMMCreateNumberedFilename(stemFileName.c_str(), BMM_SINGLEFRAME, const_cast<wchar_t*>(stemFileName.c_str()));
    else                                                          BMMCreateNumberedFilename(stemFileName.c_str(), currFrameInt, const_cast<wchar_t*>(stemFileName.c_str()));

    std::filesystem::path fullPathSaveResult;

    if (m_rendParams.renElemColorOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_diffcolor.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"diffcolor", nullptr, 0);
    }
    if (m_rendParams.renElemCoordOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_texcoord.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"texcoord", nullptr, 0);
    }
    if (m_rendParams.renElemCoverOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_coverage.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"coverage", nullptr, 0);
    }
    if (m_rendParams.renElemDepthOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_depth.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"depth", nullptr, 0);
    }

    int palette[256];
    float colorPoint[3];
    const int paletteSize = 256;
    unsigned int table[QRNG_DIMENSIONS][QRNG_RESOLUTION];

    initQuasirandomGenerator(table);

    for (int i = 1; i < paletteSize; i++)
    {
      colorPoint[0] = rndQmcSobolN(i, 0, &table[0][0]);
      colorPoint[1] = rndQmcSobolN(i, 1, &table[0][0]);
      colorPoint[2] = rndQmcSobolN(i, 2, &table[0][0]);

      palette[i] = RealColor3ToUint32(colorPoint);
    }

    if (m_rendParams.renElemInstIdOn)
    {
      fullPathSaveResult = perantPath / (stemFileName +  L"_instid.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"instid", palette, paletteSize);
    }
    if (m_rendParams.renElemMatIdOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_matid.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"matid", palette, paletteSize);
    }
    if (m_rendParams.renElemNormalsOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_normals.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"normals", nullptr, 0);
    }
    if (m_rendParams.renElemObjIdOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_objid.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"objid", palette, paletteSize);
    }
    if (m_rendParams.renElemShadowOn)
    {
      fullPathSaveResult = perantPath / (stemFileName + L"_shadow.png");
      hrRenderSaveGBufferLayerLDR(g_hrPlugResMangr.m_rendRef, fullPathSaveResult.c_str(), L"shadow", nullptr, 0);
    }
  }
}


 void HydraRenderPlugin::GetAlbedoAndNormalFromGbuffer(const int a_width, const int a_height)
{
   if (!m_hasCalculateGBuffer)
     return;
      

  hrRenderLockFrameBufferUpdate(g_hrPlugResMangr.m_rendRef);

  const int size       = a_width * a_height * 3;

  m_albedoPass.resize(size);
  m_normalPass.resize(size);

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int y = 0; y < a_height; ++y)
  {
    std::vector<HRGBufferPixel> lineGBuff(a_width);
    hrRenderGetGBufferLine(g_hrPlugResMangr.m_rendRef, y, lineGBuff.data(), 0, a_width);

    for (int x = 0; x < a_width; ++x)
    {
      const int offsetX         = (y * a_width + x) * 3;

      m_albedoPass[offsetX + 0] = lineGBuff[x].rgba[0];
      m_albedoPass[offsetX + 1] = lineGBuff[x].rgba[1];
      m_albedoPass[offsetX + 2] = lineGBuff[x].rgba[2];

      m_normalPass[offsetX + 0] = lineGBuff[x].norm[0];
      m_normalPass[offsetX + 1] = lineGBuff[x].norm[1];
      m_normalPass[offsetX + 2] = lineGBuff[x].norm[2];
    }
  }

  hrRenderUnlockFrameBufferUpdate(g_hrPlugResMangr.m_rendRef);
 }


