#include "HydraRenderer.h"
#include "3dsmaxport.h"
#include "gamma.h"


HydraRenderParams::HydraRenderParams()
{
  SetDefaults();

  progCallback      = nullptr;
  effect            = nullptr;
  hydraFrameBuf     = false;
  useRR             = 0;

  envMap            = NULL;
  atmos             = NULL;
  rendType          = RENDTYPE_NORMAL;
  nMinx             = 0;
  nMiny             = 0;
  nMaxx             = 0;
  nMaxy             = 0;
  nNumDefLights     = 0;
  scrDUV            = Point2(0.0f, 0.0f);
  pDefaultLights    = NULL;
  pFrp              = NULL;
  bVideoColorCheck  = 0;
  bForce2Sided      = FALSE;
  bRenderHidden     = FALSE;
  bSuperBlack       = FALSE;
  bRenderFields     = FALSE;
  bNetRender        = FALSE;

  renderer          = NULL;
  projType          = PROJ_PERSPECTIVE;
  devWidth          = 0;
  devHeight         = 0;
  xscale            = 0;
  yscale            = 0;
  xc                = 0;
  yc                = 0;
  antialias         = FALSE;
  nearRange         = 0;
  farRange          = 0;
  devAspect         = 0;
  frameDur          = 0;
  time              = 0;
  wireMode          = FALSE;
  inMtlEdit         = FALSE;
  fieldRender       = FALSE;
  first_field       = FALSE;
  field_order       = FALSE;
  objMotBlur        = FALSE;
  nBlurFrames       = 0;

  rendTimeType      = REND_TIMESINGLE;
  rendStart         = GetCOREInterface()->GetAnimRange().Start()  / GetTicksPerFrame();
  rendEnd           = GetCOREInterface()->GetAnimRange().End()    / GetTicksPerFrame();
  nthFrame          = 1;
  rendSaveFile      = FALSE;

}

void HydraRenderParams::SetDefaults()
{
  // Main
  infQuality                    = false;
  renderQuality                 = 50.0F;
  primary                       = 0;
  secondary                     = 0;
  tertiary                      = 0;
  manualMode                    = 0;
  enableCaustics                = true;
  enableTextureResize           = false;
  offlinePT                     = false;
  timeLimitOn                   = false;
  timeLimit                     = 1;
  useHydraGUI                   = false;
  enableDOF                     = false;
  noRandomLightSel              = false;
  focalplane                    = 8;
  lensradius                    = 1.0F;
  allSecondary                  = false;
  allTertiary                   = false;
  primaryMLFilter               = false;
  secondaryMLFilter             = false;
  writeToDisk                   = false;
  engine_type                   = 0;
  device_id.clear();
  liteMode                      = false;
  specNoiseFilterOn             = false;
  specNoiseFilter               = 2.0F;
	preset                        = 4; //*******
	rememberDevice                = false;

  // Path tracing
  seed                          = 1;
  causticRays                   = 1;
  minrays                       = 64;
  maxrays                       = 12560; // 50% quality slider
  raybounce                     = 10;
  diffbounce                    = 8;
  relative_error                = 2.5F;
  guided                        = false;
  bsdf_clamp_on                 = false;
  bsdf_clamp                    = 5.0F;
  env_clamp                     = 5.0F;
  estimateLDR                   = false;
  qmcLights                     = false;
  qmcMaterials                  = false;

  // MLT
  mlt_burn_iters                = 256;
  mlt_iters_mult                = 2;
  mlt_plarge                    = 0.5F;
  mlt_enable_median_filter      = false;
  mlt_median_filter_threshold   = 0.4F;

  // MMLT
  mmlt_estim_time               = 1;
  mmlt_burn_iters               = 2;
  mmlt_stepSize                 = 2;
  mmlt_threads                  = 524288;
  mmlt_enable_median_filter     = false;
  mmlt_median_filter_threshold  = 0.4F;
  mmlt_multBrightness           = 1.0F;

  // SPPM Caustics
  maxphotons_c                  = 1000;
  caustic_power                 = 1;
  retrace_c                     = 4;
  initial_radius_c              = 4;
  visibility_c                  = false;
  alpha_c                       = 0.67F;

  // SPPM Diffuse
  maxphotons_d                  = 1000;
  retrace_d                     = 10;
  initial_radius_d              = 10;
  visibility_d                  = false;
  alpha_d                       = 0.67F;
  irr_map                       = false;

  // Irradiance cache
  ic_eval                       = 0;
  maxpass                       = 16;
  fixed_rays                    = 1024;
  ws_err                        = 40;
  ss_err4                       = 16;
  ss_err2                       = 4;
  ss_err1                       = 1;
  ic_relative_error             = 10;

  // Multi-Layer
  filterPrimary                 = false;
  r_sigma                       = 2.5;
  s_sigma                       = 10.0F;
  layerNum                      = 1;
	spp                           = 256;
  //saveComposeLayers           = false;

  // Override
  env_mult                      = 1.0F;
  overrideGrayMatOn             = false;

  // Post process
  postProcOn                    = false;
  numThreads                    = omp_get_num_procs();
  exposure                      = 1.0F;
  compress                      = 0.0F;
  contrast                      = 1.0F;
  saturation                    = 1.0F;
  vibrance                      = 1.0F;
  whiteBalance                  = 0.0F;
  whitePointColor               = Color(0.0F, 0.0F, 0.0F);
  uniformContrast               = 0.0F;
  normalize                     = 0.0F;
  chromAberr                    = 0.0F;
  vignette                      = 0.0F;
  sharpness                     = 0.0F;
  sizeStar                      = 0.0F;
  numRay                        = 8;
  rotateRay                     = 10;
  randomAngle                   = 0.1F;
  sprayRay                      = 0.0F;

  bloom                         = false;
	bloom_radius                  = 7.0F;
	bloom_str                     = 0.5F;
  icBounce                      = 0;
  //hydraFrameBuf               = false;
  filter_id                     = 0;
	mlaa                          = true;

  // Denoise
  denoiserOn                    = false;
  denoiserAlbedoPassOn          = false;
  denoiserNormalPassOn          = false;
  denoiseLvl                    = 0.5F;

	// Debug
  enableLog                                         = false;
  useSeparateSwap                                   = false;
  debugOn                                           = false;
  debugCheckBoxes[L"BLACK_IMAGE"]                   = false;
  debugCheckBoxes[L"COMPRESS_MESHES"]               = false;
  debugCheckBoxes[L"DEBUG"]                         = false;
  debugCheckBoxes[L"DISABLE_ENERGY_FIX"]            = false;
  debugCheckBoxes[L"EXPORT_IN_ORB"]                 = false;
  debugCheckBoxes[L"EXPORT_MAT_LIB"]                = false;
  debugCheckBoxes[L"EXPORT_SCENEGRAPH"]             = false;
  debugCheckBoxes[L"FORCE_GPU_FRAMEBUFFER"]         = false;
	debugCheckBoxes[L"NORMAL_PRIORITY_FOR_CPU_TRACE"] = false;
  debugCheckBoxes[L"WELD_VERTICES(SLOW_EXPORT)"]    = false;


  saveRendImagEveryOn   = false;
  saveRendImagEveryMin  = 5.0F;

  // Render elements
  hasCalculateRenderElements = false;
  renElemAlphaOn             = true;
  renElemColorOn             = false;
  renElemCoordOn             = false;
  renElemCoverOn             = false;
  renElemDepthOn             = false;
  renElemInstIdOn            = false;
  renElemMatIdOn             = false;
  renElemNormalsOn           = false;
  renElemObjIdOn             = false;
  renElemShadowOn            = false;


  rendTimeType          = REND_TIMESINGLE;
  rendStart             = GetCOREInterface()->GetAnimRange().Start()  / GetTicksPerFrame();
  rendEnd               = GetCOREInterface()->GetAnimRange().End()    / GetTicksPerFrame();
  nthFrame              = 1;
  rendSaveFile          = FALSE;
}

#define VIEW_DEFAULT_WIDTH ((float)400.0)

void HydraRenderParams::ComputeViewParams(const ViewParams&vp)
{
	worldToCam = vp.affineTM;
	camToWorld = Inverse(worldToCam);

	xc = devWidth / 2.0f;
	yc = devHeight / 2.0f;

	scrDUV.x = 1.0f / (float)devWidth;
	scrDUV.y = 1.0f / (float)devHeight;

	projType = vp.projType;

	if (projType == PROJ_PERSPECTIVE) {
		float fac = -(float)(1.0 / tan(0.5*(double)vp.fov));
		xscale = fac*xc;
		yscale = -devAspect*xscale;
	}
	else {
		xscale = (float)devWidth / (VIEW_DEFAULT_WIDTH*vp.zoom);
		yscale = -devAspect*xscale;
	}
}

Point3 HydraRenderParams::RayDirection(float sx, float sy)
{
  Point3 p;
  p.x = -(sx - xc) / xscale;
  p.y = -(sy - yc) / yscale;
  p.z = -1.0f;
  return Normalize(p);
}

int HydraRenderParams::NumRenderInstances()
{
  return 0;
}

RenderInstance* HydraRenderParams::GetRenderInstance(int i)
{
  return NULL;
}
