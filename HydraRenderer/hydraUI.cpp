#include "3dsmaxsdk_preinclude.h"
#include "resource.h"
#include "HydraRenderer.h"

#include "3dsmaxport.h"
#include "maxscript/maxscript.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;
extern HINSTANCE hInstance;
extern bool g_materialProcessStart;

static const Class_ID kTabClassID(0x71f46739, 0x266d64bc); // main
static const Class_ID kTabClassID2(0x7d367fe5, 0x6a8575ca); // post process
static const Class_ID kTabClassID3(0x603a7f38, 0x3f215d2e); // tools
static const Class_ID kTabClassID4(0x184c20a8, 0x283b3a7f); // render elements


static INT_PTR CALLBACK HydraRenderParamDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK PathTracingDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK SPPMCDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK SPPMDDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK PostProcessingDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK OverrideDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK DebugDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK MLTDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK MMLTDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK ToolsDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK RendElemDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

#include <icolorman.h>

bool IsDarkTheme()
{
  bool darkScheme = true;
  IColorManager* pColorManager = GetColorManager();

  if (pColorManager != nullptr)
  {
    auto theme = pColorManager->GetAppFrameColorTheme();
    if (theme == IColorManager::kLightTheme)
      darkScheme = false;
  }

  return darkScheme;
}




HydraRenderParamDlg::HydraRenderParamDlg(HydraRenderPlugin *a_rendr, IRendParams *a_iRendPar, BOOL a_prog) :
  m_pRend(a_rendr),
  m_iRendParam(a_iRendPar),
  m_prog(a_prog)
{
  m_rollupIndices.resize(NUM_ROLLUP);
  for (auto& a : m_rollupIndices)
    a = -1;


  if (!a_prog)
  {
    const auto tab1           = m_iRendParam->GetTabIRollup(kTabClassID);
    m_rollupIndices[MAIN]     = tab1->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_HYDRADIALOG1), HydraRenderParamDlgProc, _T("Rendering parameters"), (LPARAM)this);
    m_hMain                   = tab1->GetPanelDlg(m_rollupIndices[MAIN]);

    m_rollupIndices[PATH]     = tab1->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_PATHTRACING), PathTracingDlgProc, _M("Path tracing"), (LPARAM)this, APPENDROLL_CLOSED);
    m_hPathTracing            = tab1->GetPanelDlg(m_rollupIndices[PATH]);

    //m_rollupIndices[MLT]    = tab1->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_MLT), MLTDlgProc, _M("MLT"), (LPARAM)this, APPENDROLL_CLOSED);
    //hMLT                    = tab1->GetPanelDlg(m_rollupIndices[MLT]);
                              
    m_rollupIndices[MMLT]     = tab1->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_MMLT), MMLTDlgProc, _M("MMLT"), (LPARAM)this, APPENDROLL_CLOSED);
    m_hMMLT                   = tab1->GetPanelDlg(m_rollupIndices[MMLT]);
                              
    m_rollupIndices[OVERRIDE] = tab1->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_OVERRIDE), OverrideDlgProc, _M("Override"), (LPARAM)this, APPENDROLL_CLOSED);
    m_hOverr                  = tab1->GetPanelDlg(m_rollupIndices[OVERRIDE]);

    //m_rollupIndices[SPPMC]  = tab1->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_SPPM_CAUSTIC), SPPMCDlgProc, _M("SPPM (Caustics)"), (LPARAM)this, APPENDROLL_CLOSED);
    //hSPPMC                  = tab1->GetPanelDlg(m_rollupIndices[SPPMC]);

    //m_rollupIndices[SPPMD]  = tab1->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_SPPM_DIFF), SPPMDDlgProc, _M("SPPM (Diffuse)"), (LPARAM)this, APPENDROLL_CLOSED);
    //hSPPMD                  = tab1->GetPanelDlg(m_rollupIndices[SPPMD]);

    const auto tab2           = m_iRendParam->GetTabIRollup(kTabClassID2);
    m_rollupIndices[POSTPROC] = tab2->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_POSTPROC), PostProcessingDlgProc, _M("Post processing"), (LPARAM)this);
    m_hPostProc               = tab2->GetPanelDlg(m_rollupIndices[POSTPROC]);

    const auto tab3           = m_iRendParam->GetTabIRollup(kTabClassID3);
    m_rollupIndices[TOOLS]    = tab3->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_TOOLS), ToolsDlgProc, _M("Tools"), (LPARAM)this);
    m_hTools                  = tab3->GetPanelDlg(m_rollupIndices[TOOLS]);        

    m_rollupIndices[DEBUG_ROLL]    = tab3->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_DEBUG), DebugDlgProc, _M("Debug"), (LPARAM)this);
    m_hDebug                  = tab3->GetPanelDlg(m_rollupIndices[DEBUG_ROLL]);

    const auto tab4           = m_iRendParam->GetTabIRollup(kTabClassID4);
    m_rollupIndices[RENDELEM] = tab4->AppendRollup(hInstance, MAKEINTRESOURCE(IDD_RENDER_ELEMENTS), RendElemDlgProc, _M("Render elements"), (LPARAM)this);
    m_hRendElem                 = tab4->GetPanelDlg(m_rollupIndices[RENDELEM]);
  }
}

HydraRenderParamDlg::~HydraRenderParamDlg()
{
  if (m_hMain)        m_iRendParam->DeleteTabRollupPage(kTabClassID, m_hMain);
  if (m_hPathTracing) m_iRendParam->DeleteTabRollupPage(kTabClassID, m_hPathTracing);
  //if (m_hMLT)         m_iRendParam->DeleteTabRollupPage(kTabClassID, m_hMLT);
  if (m_hMMLT)        m_iRendParam->DeleteTabRollupPage(kTabClassID, m_hMMLT);
  if (m_hOverr)       m_iRendParam->DeleteTabRollupPage(kTabClassID, m_hOverr);
  if (m_hPostProc)    m_iRendParam->DeleteTabRollupPage(kTabClassID2, m_hPostProc);
  if (m_hTools)       m_iRendParam->DeleteTabRollupPage(kTabClassID3, m_hTools);
  if (m_hDebug)       m_iRendParam->DeleteTabRollupPage(kTabClassID3, m_hDebug);
  if (m_hRendElem)    m_iRendParam->DeleteTabRollupPage(kTabClassID4, m_hRendElem);

  //if(m_hSPPMC) m_iRendParam->DeleteTabRollupPage(kTabClassID, m_hSPPMC);
  //if(m_hSPPMD) m_iRendParam->DeleteTabRollupPage(kTabClassID, m_hSPPMD);

  for (int i = 0; i < NUM_ROLLUP; ++i)
  {
    if (m_rollupIndices[i] != -1)
    {
      auto currTabClassID = kTabClassID;

      switch (i)
      {
      case POSTPROC: currTabClassID = kTabClassID2; 
        break;
      case TOOLS:
      case DEBUG_ROLL:    currTabClassID = kTabClassID3; 
        break;
      case RENDELEM: currTabClassID = kTabClassID4; 
        break;
      default:
        break;
      }
      m_iRendParam->GetTabIRollup(currTabClassID)->DeleteRollup(m_rollupIndices[i], 1);
      m_rollupIndices[i] = -1;
    }
  }


  m_hMain        = nullptr;
  m_hPathTracing = nullptr;
  //m_hSPPMC     = nullptr;
  //m_hSPPMD     = nullptr;
  m_hOverr       = nullptr;
  //m_hMLT       = nullptr;
  m_hMMLT        = nullptr;
  m_hPostProc    = nullptr;
  m_hTools       = nullptr;
  m_hDebug       = nullptr;
  m_hRendElem    = nullptr;
}


void HydraRenderParamDlg::InitProgDialog()
{
  m_hMain        = nullptr;
  m_hPathTracing = nullptr;
  //m_hSPPMC     = nullptr;
  //m_hSPPMD     = nullptr;
  m_hPostProc    = nullptr;
  m_hOverr       = nullptr;
  m_hDebug       = nullptr;
  //m_hMLT       = nullptr;
  m_hMMLT        = nullptr;
  m_hTools       = nullptr;
  m_hRendElem    = nullptr;
}

std::vector<HydraRenderDevice> InitDeviceListInternal(HRRenderRef a_renderRef)
{
  std::vector<HydraRenderDevice> res;

  auto pList = hrRenderGetDeviceList(a_renderRef);

  while (pList != nullptr)
  {
    HydraRenderDevice dev;
    dev.id         = pList->id;
    dev.name       = pList->name;
    dev.driverName = pList->driver;
    dev.isCPU      = pList->isCPU;
    res.push_back(dev);
    pList = pList->next;
  }

  return res;
}

std::vector<HydraRenderDevice> InitDeviceList(HWND devices_list, int mode, bool init, HRRenderRef a_renderRef)
{
  std::vector<HydraRenderDevice> result = InitDeviceListInternal(a_renderRef); // InitDeviceList(mode);

#ifdef SILENT_DEVICE_ID
  return result;
#endif

  if (devices_list == nullptr)
    return result;

  if (!init)
    ListView_DeleteAllItems(devices_list);

  LVITEM lvI;
  lvI.mask = LVIF_TEXT | LVIF_STATE;
  lvI.stateMask = 0;
  lvI.iSubItem = 0;
  lvI.state = 0;
  lvI.cColumns = 3;

  for (auto p = result.begin(); p != result.end(); ++p)
  {
    auto str        = p->name;
    auto dev_name   = p->name;
    auto dev_id     = p->id;
    auto dev_driver = p->driverName;

    std::wstringstream devIdStrOut;
    lvI.pszText = (LPWSTR)str.c_str();

    lvI.iItem   = dev_id;
    ListView_InsertItem(devices_list, &lvI);

    devIdStrOut << dev_id;
    auto dev_id_str = devIdStrOut.str();

    ListView_SetItemText(devices_list, lvI.iItem, 0, (LPWSTR)dev_id_str.c_str());
    ListView_SetItemText(devices_list, lvI.iItem, 1, (LPWSTR)dev_name.c_str());
    ListView_SetItemText(devices_list, lvI.iItem, 2, (LPWSTR)dev_driver.c_str());

    ListView_SetCheckState(devices_list, 0, true);
  }

  return result;
}

void HydraRenderParamDlg::LoadPreset(int preset_id)
{
  float tmp = 0.0F;
  if (false)
  {
    switch (preset_id + 1)
    {
      case PRESET_LOW:
        break;
      case PRESET_MEDIUM:
        break;
      case PRESET_HIGH:
        break;
      case PRESET_VHIGH:
        break;
      default:
        break;
    }
  }
  else
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, L"Failed to load presets.xml.");
}

void HydraRenderParamDlg::SavePreset()
{
  if (false)
  {
    // not implemented.
  }
  else
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, L"Failed to load presets.xml.");
}

void HydraRenderParamDlg::LoadPresetToGUI()
{
  float f_tmp = 0.0F;
  int i_tmp   = 0;


  m_minRays_edit->SetText(i_tmp);
  m_minRays_spin->SetValue(i_tmp, FALSE);

  m_maxRays_edit->SetText(i_tmp);
  m_maxRays_spin->SetValue(i_tmp, FALSE);

  m_relativeError_edit->SetText(f_tmp);
  m_relativeError_spin->SetValue(f_tmp, FALSE);
}

void HydraRenderParamDlg::InitMainHydraDialog(HWND hWnd)
{
  HydraRenderPlugin::m_pLastParamDialog = this;

  CheckDlgButton(hWnd, IDC_INF_QUALITY_ON, m_pRend->m_rendParams.infQuality);

  //PRIMARY BOUNCE
  SendDlgItemMessage(hWnd, IDC_PRIMARY, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));
  // SendDlgItemMessage(hWnd, IDC_PRIMARY, CB_ADDSTRING, 0, (LPARAM)(_M("SPPM")));

   //SECONDARY BOUNCE
  SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));
  SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("Light Tracing")));
  SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("IBPT")));
  SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_ADDSTRING, 0, (LPARAM)(_M("MMLT")));

  //TERTIARY BOUNCE
  /*SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));
  SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_ADDSTRING, 0, (LPARAM)(_M("SPPM")));
  SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_ADDSTRING, 0, (LPARAM)(_M("MLT")));*/

  SendDlgItemMessage(hWnd, IDC_PRIMARY, CB_SETCURSEL, (WPARAM)m_pRend->m_rendParams.primary, 0);
  SendDlgItemMessage(hWnd, IDC_SECONDARY, CB_SETCURSEL, (WPARAM)m_pRend->m_rendParams.secondary, 0);
  //SendDlgItemMessage(hWnd, IDC_TERTIARY, CB_SETCURSEL,  m_pRend->m_rendParams.tertiary, 0);

  //CAUSTICS
  CheckDlgButton(hWnd, IDC_CAUSTICSON, m_pRend->m_rendParams.enableCaustics);
  /*SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("None")));
  SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("SPPM")));
  SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("MLT")));
  SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_ADDSTRING, 0, (LPARAM)(_M("Path Tracing")));*/

  //SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_SELECTSTRING, -1, (LPARAM)(L"None"));
  //SendDlgItemMessage(hWnd, IDC_CAUSTICS, CB_SETCURSEL, m_pRend->m_rendParams.causticsMode, 0);

  CheckDlgButton(hWnd, IDC_ENABLE_TEXTURE_RESIZE, m_pRend->m_rendParams.enableTextureResize);
  CheckDlgButton(hWnd, IDC_PRODUCTION_MODE, m_pRend->m_rendParams.offlinePT);


  m_renderQuality_slider = GetISlider(GetDlgItem(hWnd, IDC_RENDERQUALITY_SLIDER));
  m_renderQuality_slider->SetLimits(1.0f, 100.0f, FALSE);
  m_renderQuality_slider->SetResetValue(50.0f);
  m_renderQuality_slider->SetValue(m_pRend->m_rendParams.renderQuality, FALSE);
  m_renderQuality_slider->SetNumSegs(4);


  //DEVICE ID
  /*for (int i = 0; i <= 10; ++i)
  {
    SendDlgItemMessage(hWnd, IDC_DEVICE, CB_ADDSTRING, 0, (LPARAM)std::to_wstring(i).c_str());
  }
  SendDlgItemMessage(hWnd, IDC_DEVICE, CB_SETCURSEL, m_pRend->m_rendParams.device_id, 0);*/


  m_lensRadius_edit = GetICustEdit(GetDlgItem(hWnd, IDC_LENS));
  m_lensRadius_edit->SetText(m_pRend->m_rendParams.lensradius);

  m_lensRadius_spin = GetISpinner(GetDlgItem(hWnd, IDC_LENS_SPIN));
  m_lensRadius_spin->SetLimits(0.0f, 1000.0f, FALSE);
  m_lensRadius_spin->SetResetValue(1.0f);
  m_lensRadius_spin->SetValue(m_pRend->m_rendParams.lensradius, FALSE);
  m_lensRadius_spin->SetScale(0.1f);      // value change step
  m_lensRadius_spin->LinkToEdit(GetDlgItem(hWnd, IDC_LENS), EDITTYPE_FLOAT);


  m_timeLimit_edit = GetICustEdit(GetDlgItem(hWnd, IDC_TIMELIMIT_EDIT));
  m_timeLimit_edit->SetText(m_pRend->m_rendParams.timeLimit);

  m_timeLimit_spin = GetISpinner(GetDlgItem(hWnd, IDC_TIMELIMIT_SPIN));
  m_timeLimit_spin->SetLimits(1, 60000, FALSE);
  m_timeLimit_spin->SetResetValue(1);
  m_timeLimit_spin->SetValue(m_pRend->m_rendParams.timeLimit, FALSE);
  m_timeLimit_spin->SetScale(1);      // value change step
  m_timeLimit_spin->LinkToEdit(GetDlgItem(hWnd, IDC_TIMELIMIT_EDIT), EDITTYPE_INT);


  CheckDlgButton(hWnd, IDC_DOF_ON, m_pRend->m_rendParams.enableDOF);
  CheckDlgButton(hWnd, IDC_TIMELIMIT, m_pRend->m_rendParams.timeLimitOn);


  m_maxRays_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MAXRAY));
  m_maxRays_edit->SetText(m_pRend->m_rendParams.maxrays);

  m_maxRays_spin = GetISpinner(GetDlgItem(hWnd, IDC_MAXRAY_SPIN));
  m_maxRays_spin->SetLimits(64, 1000000, FALSE);
  m_maxRays_spin->SetResetValue(12560); // 12560 = 50% quality slider.
  m_maxRays_spin->SetValue(m_pRend->m_rendParams.maxrays, FALSE);
  m_maxRays_spin->SetScale(64); //value change step
  m_maxRays_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MAXRAY), EDITTYPE_INT);


  m_rayBounce_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RAYBOUNCE));
  m_rayBounce_edit->SetText(m_pRend->m_rendParams.raybounce);

  m_rayBounce_spin = GetISpinner(GetDlgItem(hWnd, IDC_RAYBOUNCE_SPIN));
  m_rayBounce_spin->SetLimits(0, 255, FALSE);
  m_rayBounce_spin->SetResetValue(10);
  m_rayBounce_spin->SetValue(m_pRend->m_rendParams.raybounce, FALSE);
  m_rayBounce_spin->SetScale(1); //value change step
  m_rayBounce_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RAYBOUNCE), EDITTYPE_INT);


  m_diffBounce_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFBOUNCE));
  m_diffBounce_edit->SetText(m_pRend->m_rendParams.diffbounce);

  m_diffBounce_spin = GetISpinner(GetDlgItem(hWnd, IDC_DIFFBOUNCE_SPIN));
  m_diffBounce_spin->SetLimits(0, 16, FALSE);
  m_diffBounce_spin->SetResetValue(8);
  m_diffBounce_spin->SetValue(m_pRend->m_rendParams.diffbounce, FALSE);
  m_diffBounce_spin->SetScale(1); //value change step
  m_diffBounce_spin->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFBOUNCE), EDITTYPE_INT);

  //CheckDlgButton(hWnd, IDC_ALL_SECONDARY, 0); 


  SetWindowTextW(GetDlgItem(hWnd, IDC_VERSION), m_pRend->GetHydraVersionW(g_hrPlugResMangr.m_hydraVersion).c_str());


  HWND devices_list = GetDlgItem(hWnd, IDC_DEVICES_LIST);

  ListView_SetExtendedListViewStyleEx(devices_list, 0, LVS_EX_CHECKBOXES);
  //ListView_SetView(devices_list, LV_VIEW_DETAILS);
  ListView_SetBkColor(devices_list, CLR_NONE);
  ListView_SetTextBkColor(devices_list, CLR_NONE);

  if (IsDarkTheme())
    ListView_SetTextColor(devices_list, RGB(228, 228, 228));
  else
    ListView_SetTextColor(devices_list, RGB(20, 20, 20));

  LVCOLUMN lvc;
  int iCol;
  int nCol = 3;
  lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
  hydraStr cols[] ={ _M("ID"), _M("Device Name"), _M("Driver/CC") };

  for (iCol = 0; iCol < nCol; iCol++)
  {
    lvc.iSubItem = iCol;
    lvc.pszText = (LPWSTR)cols[iCol].c_str();
    lvc.fmt     = LVCFMT_LEFT;

    if (iCol < 1) lvc.cx = 40;
    else lvc.cx = 150;

    ListView_InsertColumn(devices_list, iCol, &lvc);
  }


  bool res = false;
  std::unordered_map<int, bool> deviceIDs;
  int mode = 1;

  res = m_pRend->readAndCheckDeviceSettingsFile(mode, deviceIDs);

  if (res && !deviceIDs.empty())
  {
    m_pRend->m_rendParams.rememberDevice = true;
    m_pRend->m_devList = InitDeviceList(devices_list, mode, true, g_hrPlugResMangr.m_rendRef);
    ListView_SetCheckState(devices_list, 0, false);
    for (auto it = deviceIDs.begin(); it != deviceIDs.end(); ++it)
      ListView_SetCheckState(devices_list, it->first, it->second);

    m_pRend->m_rendParams.engine_type = 1;
  }
  else if (!HydraRenderPlugin::m_lastRendParams.device_id.empty())
  {
    m_pRend->m_devList = InitDeviceList(devices_list, HydraRenderPlugin::m_lastRendParams.engine_type, true, g_hrPlugResMangr.m_rendRef);
    ListView_SetCheckState(devices_list, 0, false);
    for (auto it = HydraRenderPlugin::m_lastRendParams.device_id.begin(); it != HydraRenderPlugin::m_lastRendParams.device_id.end(); ++it)
      ListView_SetCheckState(devices_list, it->first, it->second);

    m_pRend->m_rendParams.engine_type = HydraRenderPlugin::m_lastRendParams.engine_type;
    m_pRend->m_rendParams.rememberDevice = false;
  }
  else //should not happen
  {
    m_pRend->m_devList = InitDeviceList(devices_list, m_pRend->m_rendParams.engine_type, true, g_hrPlugResMangr.m_rendRef);
    m_pRend->m_rendParams.rememberDevice = false;
  }

  CheckDlgButton(hWnd, IDC_REMEMBER_MDLG, m_pRend->m_rendParams.rememberDevice);

  UpdateWindow(hWnd);
}



void HydraRenderParamDlg::InitPathTracingDialog(HWND hWnd)
{
  //PRESETS
  //SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(L"Custom"));
  SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("Ultra")));
  SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("High")));
  SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("Medium")));
  SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("Low")));
  SendDlgItemMessage(hWnd, IDC_PRESETS, CB_ADDSTRING, 0, (LPARAM)(_M("******")));
  SendDlgItemMessage(hWnd, IDC_PRESETS, CB_SETCURSEL, (WPARAM)m_pRend->m_rendParams.preset, 4);

  CheckDlgButton(hWnd, IDC_QMC_LIGHTS, m_pRend->m_rendParams.qmcLights);
  CheckDlgButton(hWnd, IDC_QMC_MATERIALS, m_pRend->m_rendParams.qmcMaterials);

  m_minRays_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MINRAY));
  m_minRays_edit->SetText(m_pRend->m_rendParams.minrays);

  m_minRays_spin = GetISpinner(GetDlgItem(hWnd, IDC_MINRAY_SPIN));
  m_minRays_spin->SetLimits(1, 65536, FALSE);
  m_minRays_spin->SetResetValue(16);
  m_minRays_spin->SetValue(m_pRend->m_rendParams.minrays, FALSE);
  m_minRays_spin->SetScale(16); //value change step
  m_minRays_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MINRAY), EDITTYPE_INT);


  m_relativeError_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RELATIVE_ERR));
  m_relativeError_edit->SetText(m_pRend->m_rendParams.relative_error);

  m_relativeError_spin = GetISpinner(GetDlgItem(hWnd, IDC_RELATIVE_ERR_SPIN));
  m_relativeError_spin->SetLimits(0.0f, 100.0f, FALSE);
  m_relativeError_spin->SetResetValue(2.0f);                              // Right click reset value
  m_relativeError_spin->SetValue(m_pRend->m_rendParams.relative_error, FALSE); // FALSE = don't send notify yet.
  m_relativeError_spin->SetScale(0.1f);                                   // value change step
  m_relativeError_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RELATIVE_ERR), EDITTYPE_FLOAT);

  m_bsdfClamp_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BSDF_CLAMP_EDIT));
  m_bsdfClamp_edit->SetText(m_pRend->m_rendParams.bsdf_clamp);

  m_bsdfClamp_spin = GetISpinner(GetDlgItem(hWnd, IDC_BSDF_CLAMP_SPIN));
  m_bsdfClamp_spin->SetLimits(1.0f, 1000000.0f, FALSE);
  m_bsdfClamp_spin->SetResetValue(5.0f);                              // Right click reset value
  m_bsdfClamp_spin->SetValue(m_pRend->m_rendParams.bsdf_clamp, FALSE);      // FALSE = don't send notify yet.
  m_bsdfClamp_spin->SetScale(1.0f);                                    // value change step
  m_bsdfClamp_spin->LinkToEdit(GetDlgItem(hWnd, IDC_BSDF_CLAMP_EDIT), EDITTYPE_FLOAT);

  m_envClamp_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ENV_CLAMP));
  m_envClamp_edit->SetText(m_pRend->m_rendParams.env_clamp);

  m_envClamp_spin = GetISpinner(GetDlgItem(hWnd, IDC_ENV_CLAMP_SPIN));
  m_envClamp_spin->SetLimits(1.0f, 1000000.00f, FALSE);
  m_envClamp_spin->SetResetValue(5.0f);   // Right click reset value
  m_envClamp_spin->SetValue(m_pRend->m_rendParams.env_clamp, FALSE); // FALSE = don't send notify yet.
  m_envClamp_spin->SetScale(1.0f);       // value change step
  m_envClamp_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ENV_CLAMP), EDITTYPE_FLOAT);


  SendDlgItemMessage(hWnd, IDC_SEED, CB_ADDSTRING, 0, (LPARAM)(_M("None")));
  SendDlgItemMessage(hWnd, IDC_SEED, CB_ADDSTRING, 0, (LPARAM)(_M("Per warp")));
  SendDlgItemMessage(hWnd, IDC_SEED, CB_ADDSTRING, 0, (LPARAM)(_M("All")));

  //SendDlgItemMessage(hWnd, IDC_SEED, CB_SELECTSTRING, -1, (LPARAM)(L"None"));
  SendDlgItemMessage(hWnd, IDC_SEED, CB_SETCURSEL, (WPARAM)m_pRend->m_rendParams.seed, 0);

  CheckDlgButton(hWnd, IDC_CAUSTICS_ON, m_pRend->m_rendParams.causticRays);
  CheckDlgButton(hWnd, IDC_RR_ON, m_pRend->m_rendParams.useRR);
  CheckDlgButton(hWnd, IDC_GUIDED, m_pRend->m_rendParams.guided);
  CheckDlgButton(hWnd, IDC_BSDF_CLAMP_ON, m_pRend->m_rendParams.bsdf_clamp_on);

}

void HydraRenderParamDlg::InitMLTDialog(HWND hWnd)
{
  m_mlt_plarge_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_LSTEP_EDIT));
  m_mlt_plarge_edit->SetText(m_pRend->m_rendParams.mlt_plarge);

  m_mlt_plarge_slider = GetISlider(GetDlgItem(hWnd, IDC_MLT_LSTEP_SLIDER));
  m_mlt_plarge_slider->SetLimits(10, 50, FALSE);
  m_mlt_plarge_slider->SetResetValue(50);
  m_mlt_plarge_slider->SetValue((int)(100.0f*m_pRend->m_rendParams.mlt_plarge), FALSE);
  m_mlt_plarge_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_LSTEP_EDIT), EDITTYPE_INT);
  m_mlt_plarge_slider->SetNumSegs(5);
  m_mlt_plarge_slider->SetTooltip(true, TEXT("Large step sample image uniformly. Make it larger to sample dark regions more often. Make it smaller to sample caustics more often."));

  m_mlt_itersMult_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_ITERMULT_EDIT));
  m_mlt_itersMult_edit->SetText(m_pRend->m_rendParams.mlt_iters_mult);

  m_mlt_itersMult_slider = GetISlider(GetDlgItem(hWnd, IDC_MLT_ITERMULT_SLIDER));
  m_mlt_itersMult_slider->SetLimits(1, 4, FALSE);
  m_mlt_itersMult_slider->SetResetValue(2);
  m_mlt_itersMult_slider->SetValue(m_pRend->m_rendParams.mlt_iters_mult, FALSE); //1/(m_pRend->m_rendParams.mlt_iters_mult + 1)
  m_mlt_itersMult_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_ITERMULT_EDIT), EDITTYPE_INT);
  m_mlt_itersMult_slider->SetNumSegs(3);
  m_mlt_itersMult_slider->SetTooltip(true, TEXT("This is balance between direct and indirect light. Make it larger if you have strong indirect light. Make it smaller if you have complex direct light. Value 1 means 1/2 for direct and 1/2 for indirect. Value 2 means 2/3 for direct light and 1/3 for indirect. Value 16 means 1/17 for direct light 16/17 for indirect."));


  m_mlt_burnIters_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_BURN_ITERS_EDIT));
  m_mlt_burnIters_edit->SetText(m_pRend->m_rendParams.mlt_burn_iters / 64);

  m_mlt_burnIters_slider = GetISlider(GetDlgItem(hWnd, IDC_MLT_BURN_ITERS_SLIDER));
  m_mlt_burnIters_slider->SetLimits(1, 16, FALSE);
  m_mlt_burnIters_slider->SetResetValue(2);
  m_mlt_burnIters_slider->SetValue(m_pRend->m_rendParams.mlt_burn_iters / 64, FALSE);
  m_mlt_burnIters_slider->SetNumSegs(3);
  m_mlt_burnIters_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_BURN_ITERS_EDIT), EDITTYPE_INT);
  m_mlt_burnIters_slider->SetTooltip(true, TEXT("Normally you should not change this preset. Make it greater if your lighting is very complex. Make it smaller if your burn-in period is too long."));

  CheckDlgButton(hWnd, IDC_MLT_MEDIAN_ON, m_pRend->m_rendParams.mlt_enable_median_filter);

  m_mlt_medianThreshld_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MLT_MED_THRES_EDIT));
  m_mlt_medianThreshld_edit->SetText(m_pRend->m_rendParams.mlt_median_filter_threshold);

  m_mlt_medianThreshld_spin = GetISpinner(GetDlgItem(hWnd, IDC_MLT_MED_THRES_SPIN));
  m_mlt_medianThreshld_spin->SetLimits(0.1f, 2.0f, FALSE);
  m_mlt_medianThreshld_spin->SetResetValue(0.1f);
  m_mlt_medianThreshld_spin->SetValue(m_pRend->m_rendParams.mlt_median_filter_threshold, FALSE);
  m_mlt_medianThreshld_spin->SetScale(0.05f);
  m_mlt_medianThreshld_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MLT_MED_THRES_EDIT), EDITTYPE_FLOAT);
}

void HydraRenderParamDlg::InitMMLTDialog(HWND hWnd)
{
  m_mmlt_estimTime_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MMLT_ESTIM_TIME_EDIT));
  m_mmlt_estimTime_edit->SetText(m_pRend->m_rendParams.mmlt_estim_time);
  m_mmlt_estimTime_slider = GetISlider(GetDlgItem(hWnd, IDC_MMLT_ESTIM_TIME_SLIDER));
  m_mmlt_estimTime_slider->SetLimits(0, 3, FALSE);
  m_mmlt_estimTime_slider->SetResetValue(1);
  m_mmlt_estimTime_slider->SetValue(m_pRend->m_rendParams.mmlt_estim_time, FALSE);
  m_mmlt_estimTime_slider->SetNumSegs(4);
  m_mmlt_estimTime_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MMLT_ESTIM_TIME_EDIT), EDITTYPE_INT);
  m_mmlt_estimTime_slider->SetTooltip(true, TEXT("Up to 1 hours. From 2 - 5 hours. From 5 - 10 hours. More than 10 hours."));

  m_mmlt_burning_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MMLT_BURN_EDIT));
  m_mmlt_burning_edit->SetText(m_pRend->m_rendParams.mmlt_burn_iters);
  m_mmlt_burning_slider = GetISlider(GetDlgItem(hWnd, IDC_MMLT_BURN_SLIDER));
  m_mmlt_burning_slider->SetLimits(128, 4096, FALSE);
  m_mmlt_burning_slider->SetResetValue(1024);
  m_mmlt_burning_slider->SetValue(m_pRend->m_rendParams.mmlt_burn_iters, FALSE);
  m_mmlt_burning_slider->SetNumSegs(4);
  m_mmlt_burning_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MMLT_BURN_EDIT), EDITTYPE_INT);
  m_mmlt_burning_slider->SetTooltip(true, TEXT("Increase this setting if there is a lot of uneven secondary lighting in the scene. In samples per pixels."));

  m_mmlt_stepSize_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MMLT_STEP_SIZE_EDIT));
  m_mmlt_stepSize_edit->SetText(m_pRend->m_rendParams.mmlt_stepSize);
  m_mmlt_stepSize_slider = GetISlider(GetDlgItem(hWnd, IDC_MMLT_STEP_SIZE_SLIDER));
  m_mmlt_stepSize_slider->SetLimits(0, 4, FALSE);
  m_mmlt_stepSize_slider->SetResetValue(2);
  m_mmlt_stepSize_slider->SetValue(m_pRend->m_rendParams.mmlt_stepSize, FALSE);
  m_mmlt_stepSize_slider->SetNumSegs(4);
  m_mmlt_stepSize_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MMLT_STEP_SIZE_EDIT), EDITTYPE_INT);
  m_mmlt_stepSize_slider->SetTooltip(true, TEXT("[0.25, 0.5, 1.0, 1.5, 2.0]. The greater parameter is, the smaller step we gain."));

  m_mmlt_threads_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MMLT_THREADS_EDIT));
  m_mmlt_threads_edit->SetText(m_pRend->m_rendParams.mmlt_threads);
  m_mmlt_threads_slider = GetISlider(GetDlgItem(hWnd, IDC_MMLT_THREADS_SLIDER));
  m_mmlt_threads_slider->SetLimits(0, 2, FALSE);
  m_mmlt_threads_slider->SetResetValue(1);
  m_mmlt_threads_slider->SetValue(m_pRend->m_rendParams.mmlt_threads, FALSE);
  m_mmlt_threads_slider->SetNumSegs(3);
  m_mmlt_threads_slider->LinkToEdit(GetDlgItem(hWnd, IDC_MMLT_THREADS_EDIT), EDITTYPE_INT);
  m_mmlt_threads_slider->SetTooltip(true, TEXT("Set the approximate classic of your video card: low = 0, medium = 1, top = 2"));

  CheckDlgButton(hWnd, IDC_MMLT_MEDIAN_ON, m_pRend->m_rendParams.mlt_enable_median_filter);

  m_mmlt_medianThreshld_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MMLT_MED_THRES_EDIT));
  m_mmlt_medianThreshld_edit->SetText(m_pRend->m_rendParams.mmlt_median_filter_threshold);

  m_mmlt_medianThreshld_spin = GetISpinner(GetDlgItem(hWnd, IDC_MMLT_MED_THRES_SPIN));
  m_mmlt_medianThreshld_spin->SetLimits(0.1f, 2.0f, FALSE);
  m_mmlt_medianThreshld_spin->SetResetValue(0.5f);
  m_mmlt_medianThreshld_spin->SetValue(m_pRend->m_rendParams.mmlt_median_filter_threshold, FALSE);
  m_mmlt_medianThreshld_spin->SetScale(0.1f);
  m_mmlt_medianThreshld_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MMLT_MED_THRES_EDIT), EDITTYPE_FLOAT);
  m_mmlt_medianThreshld_spin->SetTooltip(true, TEXT("The lower the threshold, the stronger the filter works."));

  m_mmlt_multBrightness_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MMLT_MULT_BRIGHTNESS_EDIT));
  m_mmlt_multBrightness_edit->SetText(m_pRend->m_rendParams.mmlt_multBrightness);

  m_mmlt_multBrightness_spin = GetISpinner(GetDlgItem(hWnd, IDC_MMLT_MULT_BRIGHTNESS_SPIN));
  m_mmlt_multBrightness_spin->SetLimits(0.0f, 4.0f, FALSE);
  m_mmlt_multBrightness_spin->SetResetValue(1.0f);
  m_mmlt_multBrightness_spin->SetValue(m_pRend->m_rendParams.mmlt_multBrightness, FALSE);
  m_mmlt_multBrightness_spin->SetScale(0.1f);
  m_mmlt_multBrightness_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MMLT_MULT_BRIGHTNESS_EDIT), EDITTYPE_FLOAT);
}

void HydraRenderParamDlg::InitSPPMCDialog(HWND hWnd)
{
  m_maxPhotonsCaust_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_C));
  m_maxPhotonsCaust_edit->SetText(m_pRend->m_rendParams.maxphotons_c);

  m_maxPhotonsCaust_spin = GetISpinner(GetDlgItem(hWnd, IDC_MAXPHOTONS_C_SPIN));
  m_maxPhotonsCaust_spin->SetLimits(1, 2000, FALSE);
  m_maxPhotonsCaust_spin->SetResetValue(1000);   // Right click reset value
  m_maxPhotonsCaust_spin->SetValue(m_pRend->m_rendParams.maxphotons_c, FALSE); // FALSE = don't send notify yet.
  m_maxPhotonsCaust_spin->SetScale(50);       // value change step
  m_maxPhotonsCaust_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_C), EDITTYPE_INT);


  m_initialRadiusCaust_edit = GetICustEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_C));
  m_initialRadiusCaust_edit->SetText(m_pRend->m_rendParams.initial_radius_c);
  m_initialRadiusCaust_edit->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));

  m_initialRadiusCaust_spin = GetISpinner(GetDlgItem(hWnd, IDC_INIT_RADIUS_C_SPIN));
  m_initialRadiusCaust_spin->SetLimits(0.5, 32.0f, FALSE);
  m_initialRadiusCaust_spin->SetResetValue(4);   // Right click reset value
  m_initialRadiusCaust_spin->SetValue(m_pRend->m_rendParams.initial_radius_c, FALSE); // FALSE = don't send notify yet.
  m_initialRadiusCaust_spin->SetScale(0.5f);       // value change step
  m_initialRadiusCaust_spin->LinkToEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_C), EDITTYPE_FLOAT);

  m_initialRadiusCaust_spin->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));

  m_powerCaust_edit = GetICustEdit(GetDlgItem(hWnd, IDC_CAUSTICPOW));
  m_powerCaust_edit->SetText(m_pRend->m_rendParams.caustic_power);

  m_powerCaust_spin = GetISpinner(GetDlgItem(hWnd, IDC_CAUSTICPOW_SPIN));
  m_powerCaust_spin->SetLimits(0, 10, FALSE);
  m_powerCaust_spin->SetResetValue(1);   // Right click reset value
  m_powerCaust_spin->SetValue(m_pRend->m_rendParams.caustic_power, FALSE); // FALSE = don't send notify yet.
  m_powerCaust_spin->SetScale(0.5);       // value change step
  m_powerCaust_spin->LinkToEdit(GetDlgItem(hWnd, IDC_CAUSTICPOW), EDITTYPE_FLOAT);

  m_retraceCaust_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RETRACE_C));
  m_retraceCaust_edit->SetText(m_pRend->m_rendParams.retrace_c);
  m_retraceCaust_edit->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

  retrace_c_spin = GetISpinner(GetDlgItem(hWnd, IDC_RETRACE_C_SPIN));
  retrace_c_spin->SetLimits(1, 16, FALSE);
  retrace_c_spin->SetResetValue(m_pRend->m_rendParams.retrace_c);   // Right click reset value
  retrace_c_spin->SetValue(m_pRend->m_rendParams.retrace_c, FALSE); // FALSE = don't send notify yet.
  retrace_c_spin->SetScale(1);                 // value change step
  retrace_c_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RETRACE_C), EDITTYPE_INT);
  retrace_c_spin->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

  m_alphaCaust_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_C));
  m_alphaCaust_edit->SetText(m_pRend->m_rendParams.alpha_c);

  alpha_c_spin = GetISpinner(GetDlgItem(hWnd, IDC_ALPHA_SPIN_C));
  alpha_c_spin->SetLimits(0.66f, 1.0f, FALSE);
  alpha_c_spin->SetResetValue(m_pRend->m_rendParams.alpha_c);   // Right click reset value
  alpha_c_spin->SetValue(m_pRend->m_rendParams.alpha_c, FALSE); // FALSE = don't send notify yet.
  alpha_c_spin->SetScale(0.05f);                 // value change step
  alpha_c_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_C), EDITTYPE_FLOAT);


  CheckDlgButton(hWnd, IDC_VISIBILITY_C_ON, m_pRend->m_rendParams.visibility_c);
}

void HydraRenderParamDlg::InitSPPMDDialog(HWND hWnd)
{
  m_maxPhotonsDiffuse_edit = GetICustEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_D));
  m_maxPhotonsDiffuse_edit->SetText(m_pRend->m_rendParams.maxphotons_d);

  m_maxPhotonsDiffuse_spin = GetISpinner(GetDlgItem(hWnd, IDC_MAXPHOTONS_D_SPIN));
  m_maxPhotonsDiffuse_spin->SetLimits(1, 2000, FALSE);
  m_maxPhotonsDiffuse_spin->SetResetValue(1000);   // Right click reset value
  m_maxPhotonsDiffuse_spin->SetValue(m_pRend->m_rendParams.maxphotons_d, FALSE); // FALSE = don't send notify yet.
  m_maxPhotonsDiffuse_spin->SetScale(50);       // value change step
  m_maxPhotonsDiffuse_spin->LinkToEdit(GetDlgItem(hWnd, IDC_MAXPHOTONS_D), EDITTYPE_INT);


  m_initialRadiusDiffuse_edit = GetICustEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_D));
  m_initialRadiusDiffuse_edit->SetText(m_pRend->m_rendParams.initial_radius_d);
  m_initialRadiusDiffuse_edit->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));

  m_initialRadiusDiffuse_spin = GetISpinner(GetDlgItem(hWnd, IDC_INIT_RADIUS_D_SPIN));
  m_initialRadiusDiffuse_spin->SetLimits(1, 64, FALSE);
  m_initialRadiusDiffuse_spin->SetResetValue(10);   // Right click reset value
  m_initialRadiusDiffuse_spin->SetValue(m_pRend->m_rendParams.initial_radius_d, FALSE); // FALSE = don't send notify yet.
  m_initialRadiusDiffuse_spin->SetScale(0.5f);     // value change step
  m_initialRadiusDiffuse_spin->LinkToEdit(GetDlgItem(hWnd, IDC_INIT_RADIUS_D), EDITTYPE_FLOAT);

  m_initialRadiusDiffuse_spin->SetTooltip(true, TEXT("Approximate Initial radius in pixels. Radius became smaller in time."));


  m_retraceDiffuse_edit = GetICustEdit(GetDlgItem(hWnd, IDC_RETRACE_D));
  m_retraceDiffuse_edit->SetText(m_pRend->m_rendParams.retrace_d);
  m_retraceDiffuse_edit->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

  m_retraceDiffuse_spin = GetISpinner(GetDlgItem(hWnd, IDC_RETRACE_D_SPIN));
  m_retraceDiffuse_spin->SetLimits(1, 16, FALSE);
  m_retraceDiffuse_spin->SetResetValue(m_pRend->m_rendParams.retrace_d);   // Right click reset value
  m_retraceDiffuse_spin->SetValue(m_pRend->m_rendParams.retrace_d, FALSE); // FALSE = don't send notify yet.
  m_retraceDiffuse_spin->SetScale(1);                 // value change step
  m_retraceDiffuse_spin->LinkToEdit(GetDlgItem(hWnd, IDC_RETRACE_D), EDITTYPE_INT);
  m_retraceDiffuse_spin->SetTooltip(true, TEXT("This number controll gather pass amount per one photon trace pass. For example 2 means you trace 1 portion of photons and then gather this portion 2 times with path tracing. "));

  m_alphaDiffuse_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_D));
  m_alphaDiffuse_edit->SetText(m_pRend->m_rendParams.alpha_d);

  m_alphaDiffuse_spin = GetISpinner(GetDlgItem(hWnd, IDC_ALPHA_SPIN_D));
  m_alphaDiffuse_spin->SetLimits(0.66f, 1.0f, FALSE);
  m_alphaDiffuse_spin->SetResetValue(m_pRend->m_rendParams.alpha_d);   // Right click reset value
  m_alphaDiffuse_spin->SetValue(m_pRend->m_rendParams.alpha_d, FALSE); // FALSE = don't send notify yet.
  m_alphaDiffuse_spin->SetScale(0.05f);                 // value change step
  m_alphaDiffuse_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ALPHA_EDIT_D), EDITTYPE_FLOAT);

  CheckDlgButton(hWnd, IDC_VISIBILITY_D_ON, m_pRend->m_rendParams.visibility_d);
  CheckDlgButton(hWnd, IDC_IRRMAP, m_pRend->m_rendParams.irr_map);
}

void HydraRenderParamDlg::InitPostProcDialog(HWND hWnd)
{
  CheckDlgButton(hWnd, IDC_DENOISER,               m_pRend->m_rendParams.denoiserOn);
  CheckDlgButton(hWnd, IDC_DENOISE_COLOR_PASS_ON,  m_pRend->m_rendParams.denoiserAlbedoPassOn);
  CheckDlgButton(hWnd, IDC_DENOISE_NORMAL_PASS_ON, m_pRend->m_rendParams.denoiserNormalPassOn);

  m_denoise_btn           = GetICustButton(GetDlgItem(hWnd, IDC_DO_DENOISE));
  m_denoise_btn->SetType(CBT_PUSH);

  m_denoiseLvl_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DENOISELVL_EDIT));
  m_denoiseLvl_edit->SetText(m_pRend->m_rendParams.denoiseLvl);

  m_denoiseLvl_slider = GetISlider(GetDlgItem(hWnd, IDC_DENOISELVL_SLIDER));
  m_denoiseLvl_slider->SetLimits(0.0F, 1.0F, FALSE);
  m_denoiseLvl_slider->SetResetValue(0.5F);
  m_denoiseLvl_slider->SetValue(m_pRend->m_rendParams.denoiseLvl, FALSE);
  m_denoiseLvl_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DENOISELVL_EDIT), EDITTYPE_FLOAT);
  m_denoiseLvl_slider->SetNumSegs(4);

  CheckDlgButton(hWnd, IDC_POSTPROC_ON, m_pRend->m_rendParams.postProcOn);

  m_resetPostProc   = GetICustButton(GetDlgItem(hWnd, IDC_RESET_POSTPROC));
  m_resetPostProc->SetType(CBT_PUSH);

  m_exposure_edit   = GetICustEdit(GetDlgItem(hWnd, IDC_EXPOSURE_EDIT));
  m_exposure_edit->SetText(m_pRend->m_rendParams.exposure);

  m_exposure_slider = GetISlider(GetDlgItem(hWnd, IDC_EXPOSURE_SLIDER));
  m_exposure_slider->SetLimits(0.0F, 10.0F, FALSE);
  m_exposure_slider->SetResetValue(1.0F);
  m_exposure_slider->SetValue(m_pRend->m_rendParams.exposure, FALSE);
  m_exposure_slider->LinkToEdit(GetDlgItem(hWnd, IDC_EXPOSURE_EDIT), EDITTYPE_FLOAT);
  m_exposure_slider->SetNumSegs(4);

  m_compress_edit = GetICustEdit(GetDlgItem(hWnd, IDC_COMPRESS_EDIT));
  m_compress_edit->SetText(m_pRend->m_rendParams.compress);

  m_compress_slider = GetISlider(GetDlgItem(hWnd, IDC_COMPRESS_SLIDER));
  m_compress_slider->SetLimits(0.0F, 1.0F, FALSE);
  m_compress_slider->SetResetValue(0.0F);
  m_compress_slider->SetValue(m_pRend->m_rendParams.compress, FALSE);
  m_compress_slider->LinkToEdit(GetDlgItem(hWnd, IDC_COMPRESS_EDIT), EDITTYPE_FLOAT);
  m_compress_slider->SetNumSegs(4);

  m_contrast_edit = GetICustEdit(GetDlgItem(hWnd, IDC_CONTRAST_EDIT));
  m_contrast_edit->SetText(m_pRend->m_rendParams.contrast);

  m_contrast_slider = GetISlider(GetDlgItem(hWnd, IDC_CONTRAST_SLIDER));
  m_contrast_slider->SetLimits(1.0F, 2.0F, FALSE);
  m_contrast_slider->SetResetValue(1.0F);
  m_contrast_slider->SetValue(m_pRend->m_rendParams.contrast, FALSE);
  m_contrast_slider->LinkToEdit(GetDlgItem(hWnd, IDC_CONTRAST_EDIT), EDITTYPE_FLOAT);
  m_contrast_slider->SetNumSegs(4);

  m_saturation_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SATURATION_EDIT));
  m_saturation_edit->SetText(m_pRend->m_rendParams.saturation);

  m_saturation_slider = GetISlider(GetDlgItem(hWnd, IDC_SATURATION_SLIDER));
  m_saturation_slider->SetLimits(0.0F, 2.0F, FALSE);
  m_saturation_slider->SetResetValue(1.0F);
  m_saturation_slider->SetValue(m_pRend->m_rendParams.saturation, FALSE);
  m_saturation_slider->LinkToEdit(GetDlgItem(hWnd, IDC_SATURATION_EDIT), EDITTYPE_FLOAT);
  m_saturation_slider->SetNumSegs(4);

  m_vibrance_edit = GetICustEdit(GetDlgItem(hWnd, IDC_VIBRANCE_EDIT));
  m_vibrance_edit->SetText(m_pRend->m_rendParams.vibrance);

  m_vibrance_slider = GetISlider(GetDlgItem(hWnd, IDC_VIBRANCE_SLIDER));
  m_vibrance_slider->SetLimits(0.0F, 2.0F, FALSE);
  m_vibrance_slider->SetResetValue(1.0F);
  m_vibrance_slider->SetValue(m_pRend->m_rendParams.vibrance, FALSE);
  m_vibrance_slider->LinkToEdit(GetDlgItem(hWnd, IDC_VIBRANCE_EDIT), EDITTYPE_FLOAT);
  m_vibrance_slider->SetNumSegs(4);

  m_whiteBalance_edit = GetICustEdit(GetDlgItem(hWnd, IDC_WHITE_BALANCE_EDIT));
  m_whiteBalance_edit->SetText(m_pRend->m_rendParams.whiteBalance);

  m_whiteBalance_slider = GetISlider(GetDlgItem(hWnd, IDC_WHITE_BALANCE_SLIDER));
  m_whiteBalance_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_whiteBalance_slider->SetResetValue(0.0f);
  m_whiteBalance_slider->SetValue(m_pRend->m_rendParams.whiteBalance, FALSE);
  m_whiteBalance_slider->LinkToEdit(GetDlgItem(hWnd, IDC_WHITE_BALANCE_EDIT), EDITTYPE_FLOAT);
  m_whiteBalance_slider->SetNumSegs(4);

  m_whitePoint_selector = GetIColorSwatch(GetDlgItem(hWnd, IDC_WHITEPOINT_COLOR), RGB(0, 0, 0), _M("Select white point. Black color - auto white balance"));
  m_whitePoint_selector->SetColor(m_pRend->m_rendParams.whitePointColor);

  m_uniformContrast_edit = GetICustEdit(GetDlgItem(hWnd, IDC_UNIFORM_CONTRAST_EDIT));
  m_uniformContrast_edit->SetText(m_pRend->m_rendParams.uniformContrast);

  m_uniformContrast_slider = GetISlider(GetDlgItem(hWnd, IDC_UNIFORM_CONTRAST_SLIDER));
  m_uniformContrast_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_uniformContrast_slider->SetResetValue(0.0f);
  m_uniformContrast_slider->SetValue(m_pRend->m_rendParams.uniformContrast, FALSE);
  m_uniformContrast_slider->LinkToEdit(GetDlgItem(hWnd, IDC_UNIFORM_CONTRAST_EDIT), EDITTYPE_FLOAT);
  m_uniformContrast_slider->SetNumSegs(4);

  m_normalize_edit = GetICustEdit(GetDlgItem(hWnd, IDC_NORMALIZE_EDIT));
  m_normalize_edit->SetText(m_pRend->m_rendParams.normalize);

  m_normalize_slider = GetISlider(GetDlgItem(hWnd, IDC_NORMALIZE_SLIDER));
  m_normalize_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_normalize_slider->SetResetValue(0.0f);
  m_normalize_slider->SetValue(m_pRend->m_rendParams.normalize, FALSE);
  m_normalize_slider->LinkToEdit(GetDlgItem(hWnd, IDC_NORMALIZE_EDIT), EDITTYPE_FLOAT);
  m_normalize_slider->SetNumSegs(4);

  m_chromAberr_edit = GetICustEdit(GetDlgItem(hWnd, IDC_CHROM_ABERR_EDIT));
  m_chromAberr_edit->SetText(m_pRend->m_rendParams.chromAberr);

  m_chromAberr_slider = GetISlider(GetDlgItem(hWnd, IDC_CHROM_ABERR_SLIDER));
  m_chromAberr_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_chromAberr_slider->SetResetValue(0.0f);
  m_chromAberr_slider->SetValue(m_pRend->m_rendParams.chromAberr, FALSE);
  m_chromAberr_slider->LinkToEdit(GetDlgItem(hWnd, IDC_CHROM_ABERR_EDIT), EDITTYPE_FLOAT);
  m_chromAberr_slider->SetNumSegs(4);

  m_vignette_edit = GetICustEdit(GetDlgItem(hWnd, IDC_VIGNETTE_EDIT));
  m_vignette_edit->SetText(m_pRend->m_rendParams.vignette);

  m_vignette_slider = GetISlider(GetDlgItem(hWnd, IDC_VIGNETTE_SLIDER));
  m_vignette_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_vignette_slider->SetResetValue(0.0f);
  m_vignette_slider->SetValue(m_pRend->m_rendParams.vignette, FALSE);
  m_vignette_slider->LinkToEdit(GetDlgItem(hWnd, IDC_VIGNETTE_EDIT), EDITTYPE_FLOAT);
  m_vignette_slider->SetNumSegs(4);

  m_sharpness_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SHARPNESS_EDIT));
  m_sharpness_edit->SetText(m_pRend->m_rendParams.sharpness);

  m_sharpness_slider = GetISlider(GetDlgItem(hWnd, IDC_SHARPNESS_SLIDER));
  m_sharpness_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_sharpness_slider->SetResetValue(0.0f);
  m_sharpness_slider->SetValue(m_pRend->m_rendParams.sharpness, FALSE);
  m_sharpness_slider->LinkToEdit(GetDlgItem(hWnd, IDC_SHARPNESS_EDIT), EDITTYPE_FLOAT);
  m_sharpness_slider->SetNumSegs(4);

  m_sizeStar_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SIZE_EDIT));
  m_sizeStar_edit->SetText(m_pRend->m_rendParams.sizeStar);

  m_sizeStar_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_SIZE_SLIDER));
  m_sizeStar_slider->SetLimits(0.0f, 100.0f, FALSE);
  m_sizeStar_slider->SetResetValue(0.0f);
  m_sizeStar_slider->SetValue(m_pRend->m_rendParams.sizeStar, FALSE);
  m_sizeStar_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SIZE_EDIT), EDITTYPE_FLOAT);
  m_sizeStar_slider->SetNumSegs(4);

  m_numRay_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_NUMRAYS_EDIT));
  m_numRay_edit->SetText(m_pRend->m_rendParams.numRay);

  m_numRay_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_NUMRAYS_SLIDER));
  m_numRay_slider->SetLimits(2, 16, FALSE);
  m_numRay_slider->SetResetValue(8);
  m_numRay_slider->SetValue(m_pRend->m_rendParams.numRay, FALSE);
  m_numRay_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_NUMRAYS_EDIT), EDITTYPE_INT);
  m_numRay_slider->SetNumSegs(4);

  m_rotateRay_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_ROTRAYS_EDIT));
  m_rotateRay_edit->SetText(m_pRend->m_rendParams.rotateRay);

  m_rotateRay_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_ROTRAYS_SLIDER));
  m_rotateRay_slider->SetLimits(0, 360, FALSE);
  m_rotateRay_slider->SetResetValue(10);
  m_rotateRay_slider->SetValue(m_pRend->m_rendParams.rotateRay, FALSE);
  m_rotateRay_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_ROTRAYS_EDIT), EDITTYPE_INT);
  m_rotateRay_slider->SetNumSegs(4);

  m_randomAngle_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_RANDANGLE_EDIT));
  m_randomAngle_edit->SetText(m_pRend->m_rendParams.randomAngle);

  m_randomAngle_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_RANDANGLE_SLIDER));
  m_randomAngle_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_randomAngle_slider->SetResetValue(0.0f);
  m_randomAngle_slider->SetValue(m_pRend->m_rendParams.randomAngle, FALSE);
  m_randomAngle_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_RANDANGLE_EDIT), EDITTYPE_FLOAT);
  m_randomAngle_slider->SetNumSegs(4);

  m_sprayRay_edit = GetICustEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SPRAYRAY_EDIT));
  m_sprayRay_edit->SetText(m_pRend->m_rendParams.sprayRay);

  m_sprayRay_slider = GetISlider(GetDlgItem(hWnd, IDC_DIFFSTAR_SPRAYRAY_SLIDER));
  m_sprayRay_slider->SetLimits(0.0f, 1.0f, FALSE);
  m_sprayRay_slider->SetResetValue(0.0f);
  m_sprayRay_slider->SetValue(m_pRend->m_rendParams.sprayRay, FALSE);
  m_sprayRay_slider->LinkToEdit(GetDlgItem(hWnd, IDC_DIFFSTAR_SPRAYRAY_EDIT), EDITTYPE_FLOAT);
  m_sprayRay_slider->SetNumSegs(4);


  m_bloomRadius_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLOOM_RADIUS_EDIT));
  m_bloomRadius_edit->SetText(m_pRend->m_rendParams.bloom_radius);

  m_bloomRadius_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLOOM_RADIUS_SPIN));
  m_bloomRadius_spin->SetLimits(0.00f, 100.00f, FALSE);
  m_bloomRadius_spin->SetResetValue(7.0f);   // Right click reset value
  m_bloomRadius_spin->SetValue(m_pRend->m_rendParams.bloom_radius, FALSE); // FALSE = don't send notify yet.
  m_bloomRadius_spin->SetScale(1.0f);       // value change step
  m_bloomRadius_spin->LinkToEdit(GetDlgItem(hWnd, IDC_BLOOM_RADIUS_EDIT), EDITTYPE_FLOAT);

  CheckDlgButton(hWnd, IDC_BLOOM_ON, m_pRend->m_rendParams.bloom);

  m_bloomStr_edit = GetICustEdit(GetDlgItem(hWnd, IDC_BLOOM_STR_EDIT));
  m_bloomStr_edit->SetText(m_pRend->m_rendParams.bloom_radius);

  m_bloomStr_spin = GetISpinner(GetDlgItem(hWnd, IDC_BLOOM_STR_SPIN));
  m_bloomStr_spin->SetLimits(0.00f, 5.00f, FALSE);
  m_bloomStr_spin->SetResetValue(0.5f);   // Right click reset value
  m_bloomStr_spin->SetValue(m_pRend->m_rendParams.bloom_str, FALSE); // FALSE = don't send notify yet.
  m_bloomStr_spin->SetScale(0.1f);       // value change step
  m_bloomStr_spin->LinkToEdit(GetDlgItem(hWnd, IDC_BLOOM_STR_EDIT), EDITTYPE_FLOAT);

  CheckDlgButton(hWnd, IDC_MLAA, m_pRend->m_rendParams.mlaa);
}

void HydraRenderParamDlg::InitToolsDialog(HWND hWnd)
{
  m_runConvertToHydraScript_btn   = GetICustButton(GetDlgItem(hWnd, IDC_CONVERT_TO_HYDRA));
  m_runConvertFromHydraScript_btn = GetICustButton(GetDlgItem(hWnd, IDC_CONVERT_FROM_HYDRA));
  m_runOverrideHydraMatScript_btn = GetICustButton(GetDlgItem(hWnd, IDC_OVERR_PARAM_MAT));

  m_runConvertToHydraScript_btn->SetType(CBT_PUSH);
  m_runConvertFromHydraScript_btn->SetType(CBT_PUSH);
  m_runOverrideHydraMatScript_btn->SetType(CBT_PUSH);
}

void HydraRenderParamDlg::InitRendElemDialog(HWND hWnd)
{
  CheckDlgButton(hWnd, IDC_REND_ELEM_ALPHA, m_pRend->m_rendParams.renElemAlphaOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_COLOR, m_pRend->m_rendParams.renElemColorOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_COORD, m_pRend->m_rendParams.renElemCoordOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_COVERAGE, m_pRend->m_rendParams.renElemCoverOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_DEPTH, m_pRend->m_rendParams.renElemDepthOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_INSTID, m_pRend->m_rendParams.renElemInstIdOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_MATID, m_pRend->m_rendParams.renElemMatIdOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_NORMALS, m_pRend->m_rendParams.renElemNormalsOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_OBJID, m_pRend->m_rendParams.renElemObjIdOn);
  CheckDlgButton(hWnd, IDC_REND_ELEM_SHADOW, m_pRend->m_rendParams.renElemShadowOn);

}

void HydraRenderParamDlg::InitOverrideDialog(HWND hWnd)
{
  m_envMult_edit = GetICustEdit(GetDlgItem(hWnd, IDC_ENV_MULT));
  m_envMult_edit->SetText(m_pRend->m_rendParams.env_mult);

  m_envMult_spin = GetISpinner(GetDlgItem(hWnd, IDC_ENV_MULT_SPIN));
  m_envMult_spin->SetLimits(0.0f, 100.00f, FALSE);
  m_envMult_spin->SetResetValue(1.0f);   // Right click reset value
  m_envMult_spin->SetValue(m_pRend->m_rendParams.env_mult, FALSE); // FALSE = don't send notify yet.
  m_envMult_spin->SetScale(0.1f);       // value change step
  m_envMult_spin->LinkToEdit(GetDlgItem(hWnd, IDC_ENV_MULT), EDITTYPE_FLOAT);

  CheckDlgButton(hWnd, IDC_GRAYMAT_ON, m_pRend->m_rendParams.overrideGrayMatOn);
}


void HydraRenderParamDlg::InitDebugDialog(HWND hWnd)
{

  CheckDlgButton(hWnd, IDC_LOG,     m_pRend->m_rendParams.enableLog);
  CheckDlgButton(hWnd, IDC_SEPSWAP, m_pRend->m_rendParams.useSeparateSwap);

  HWND debug_list = GetDlgItem(hWnd, IDC_DEBUG_LIST);

  ListView_SetExtendedListViewStyleEx(debug_list, 0, LVS_EX_CHECKBOXES);
  //ListView_SetView(devices_list, LV_VIEW_DETAILS);
  ListView_SetBkColor(debug_list, CLR_NONE);
  ListView_SetTextBkColor(debug_list, CLR_NONE);

  if (IsDarkTheme())
    ListView_SetTextColor(debug_list, RGB(228, 228, 228));
  else
    ListView_SetTextColor(debug_list, RGB(20, 20, 20));

  LVCOLUMN lvc;
  int iCol;
  int nCol = 1;
  lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
  hydraStr cols[] ={ _M("Name") };

  for (iCol = 0; iCol < nCol; iCol++) //-V1008
  {
    lvc.iSubItem = iCol;
    lvc.pszText  = (LPWSTR)cols[iCol].c_str();
    lvc.fmt      = LVCFMT_LEFT;

    lvc.cx = 250;

    ListView_InsertColumn(debug_list, iCol, &lvc);
  }

  LVITEM lvI;
  lvI.mask = LVIF_TEXT | LVIF_STATE;
  lvI.stateMask = 0;
  lvI.iSubItem = 0;
  lvI.state = 0;
  lvI.cColumns = 1;

  std::wstring str;

  int i = 0;

  for (auto it = m_pRend->m_rendParams.debugCheckBoxes.begin(); it != m_pRend->m_rendParams.debugCheckBoxes.end(); it++)
  {

    str = it->first;

    lvI.pszText = (LPWSTR)str.c_str();

    lvI.iItem = i;
    ListView_InsertItem(debug_list, &lvI);
    ListView_SetItemText(debug_list, lvI.iItem, 0, (LPWSTR)str.c_str());
    ListView_SetCheckState(debug_list, 0, it->second);

    i++;
  }

  UpdateWindow(hWnd);
  CheckDlgButton(hWnd, IDC_SAVE_REND_IMAGE_EVERY_ON, m_pRend->m_rendParams.saveRendImagEveryOn);

  m_saveRendImagEvery_edit = GetICustEdit(GetDlgItem(hWnd, IDC_SAVE_REND_IMAGE_EVERY_EDIT));
  m_saveRendImagEvery_edit->SetText(m_pRend->m_rendParams.saveRendImagEveryMin);

  m_saveRendImagEvery_spin = GetISpinner(GetDlgItem(hWnd, IDC_SAVE_REND_IMAGE_EVERY_SPIN));
  m_saveRendImagEvery_spin->SetLimits(0.5F, 240.0F, FALSE);
  m_saveRendImagEvery_spin->SetResetValue(5.0F);   // Right click reset value
  m_saveRendImagEvery_spin->SetValue(m_pRend->m_rendParams.saveRendImagEveryMin, FALSE); // FALSE = don't send notify yet.
  m_saveRendImagEvery_spin->SetScale(0.5F);  // value change step
  m_saveRendImagEvery_spin->LinkToEdit(GetDlgItem(hWnd, IDC_SAVE_REND_IMAGE_EVERY_EDIT), EDITTYPE_FLOAT);
}


void HydraRenderParamDlg::RemovePage(HWND &hPanel)
{
  if (hPanel != NULL)
  {
    m_iRendParam->DeleteTabRollupPage(kTabClassID, hPanel);
    m_iRendParam->DeleteTabRollupPage(kTabClassID2, hPanel);
    m_iRendParam->DeleteTabRollupPage(kTabClassID3, hPanel);
    m_iRendParam->DeleteTabRollupPage(kTabClassID4, hPanel);

    hPanel = NULL;
  }
}

void HydraRenderParamDlg::AddPage(HWND &hPanel, int IDD)
{
  if (hPanel == nullptr)
  {
    switch (IDD)
    {
      case IDD_HYDRADIALOG1:
        m_hMain = m_iRendParam->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_HYDRADIALOG1), HydraRenderParamDlgProc, _M("Hydra Renderer"), (LPARAM)this);
        break;
      case IDD_PATHTRACING:
        m_hPathTracing = m_iRendParam->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_PATHTRACING), PathTracingDlgProc, _M("Path tracing"), (LPARAM)this);
        break;
        /*case IDD_SPPM_CAUSTIC:
          hSPPMC = m_iRendParam->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_SPPM_CAUSTIC), SPPMCDlgProc, _M("SPPM (Caustics)"), (LPARAM)this);
          break;
        case IDD_SPPM_DIFF:
          hSPPMD = m_iRendParam->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_SPPM_DIFF), SPPMDDlgProc, _M("SPPM (Diffuse)"), (LPARAM)this);
          break;*/
        //case IDD_MLT:
        //	hMLT = m_iRendParam->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_MLT), MLTDlgProc, _M("MLT"), (LPARAM)this);
        //	break;
      case IDD_MMLT:
        m_hMMLT = m_iRendParam->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_MMLT), MLTDlgProc, _M("MMLT"), (LPARAM)this);
        break;
      case IDD_OVERRIDE:
        m_hOverr = m_iRendParam->AddTabRollupPage(kTabClassID, hInstance, MAKEINTRESOURCE(IDD_OVERRIDE), OverrideDlgProc, _M("Override"), (LPARAM)this);
        break;
      case IDD_POSTPROC:
        m_hPostProc = m_iRendParam->AddTabRollupPage(kTabClassID2, hInstance, MAKEINTRESOURCE(IDD_POSTPROC), PostProcessingDlgProc, _M("Post Processing"), (LPARAM)this);
        break;
      case IDD_TOOLS:
        m_hTools = m_iRendParam->AddTabRollupPage(kTabClassID3, hInstance, MAKEINTRESOURCE(IDD_TOOLS), ToolsDlgProc, _M("Tools"), (LPARAM)this);
        break;
      case IDD_DEBUG:
        m_hDebug = m_iRendParam->AddTabRollupPage(kTabClassID3, hInstance, MAKEINTRESOURCE(IDD_DEBUG), DebugDlgProc, _M("Debug"), (LPARAM)this);
        break;
      case IDD_RENDER_ELEMENTS:
        m_hRendElem = m_iRendParam->AddTabRollupPage(kTabClassID4, hInstance, MAKEINTRESOURCE(IDD_RENDER_ELEMENTS), RendElemDlgProc, _M("Render elements"), (LPARAM)this);
        break;
      default:
        break;
    }
  }
}


void HydraRenderParamDlg::ReadGuiToRendParams(HydraRenderParams* pRendParams)
{
  if (!pRendParams)
    return;

  //MessageBoxA(NULL, "HydraRenderParamDlg::ReadGuiToRendParams", "", 0);

  pRendParams->infQuality          = IsDlgButtonChecked(m_hMain, IDC_INF_QUALITY_ON);

  pRendParams->renderQuality       = m_renderQuality_slider->GetFVal();
  pRendParams->primary             = (int)SendDlgItemMessage(m_hMain, IDC_PRIMARY, CB_GETCURSEL, 0, 0);
  pRendParams->secondary           = (int)SendDlgItemMessage(m_hMain, IDC_SECONDARY, CB_GETCURSEL, 0, 0);

  pRendParams->manualMode          = 1;
  pRendParams->enableCaustics      = IsDlgButtonChecked(m_hMain, IDC_CAUSTICSON);
  pRendParams->enableTextureResize = IsDlgButtonChecked(m_hMain, IDC_ENABLE_TEXTURE_RESIZE);
  pRendParams->offlinePT           = IsDlgButtonChecked(m_hMain, IDC_PRODUCTION_MODE);

  pRendParams->device_id.clear();
  HWND devices_list                = GetDlgItem(m_hMain, IDC_DEVICES_LIST);
  int numItems                     = ListView_GetItemCount(devices_list);

  for (auto i = 0; i < numItems; ++i)
  {
    hydraChar bufText[4];
    ListView_GetItemText(devices_list, i, 0, &bufText[0], sizeof(bufText));
    if (ListView_GetCheckState(devices_list, i)) pRendParams->device_id[std::stoi(bufText)] = true;
    else                                         pRendParams->device_id[std::stoi(bufText)] = false;
  }

  pRendParams->timeLimitOn       = IsDlgButtonChecked(m_hMain, IDC_TIMELIMIT);
  pRendParams->timeLimit         = m_timeLimit_spin->GetIVal();

  pRendParams->lensradius        = m_lensRadius_spin->GetFVal();
  pRendParams->enableDOF         = IsDlgButtonChecked(m_hMain, IDC_DOF_ON);

  pRendParams->rememberDevice    = IsDlgButtonChecked(m_hMain, IDC_REMEMBER_MDLG);

  
  //if (pRendParams->rememberDevice)
  //  CreateDeviceSettingsFile(pRendParams->engine_type, pRendParams->device_id, HydraRenderPlugin::g_hrPlugResMangr.m_rendRef);
  //else
  //  CreateDeviceSettingsFile(-1, m_pRend->m_rendParams.device_id, HydraRenderPlugin::g_hrPlugResMangr.m_rendRef);
  

  //path tracing
  pRendParams->seed           = (int)SendDlgItemMessage(m_hPathTracing, IDC_SEED, CB_GETCURSEL, 0, 0);
  pRendParams->preset         = (int)SendDlgItemMessage(m_hPathTracing, IDC_PRESETS, CB_GETCURSEL, 0, 0);
  pRendParams->minrays        = m_minRays_spin->GetIVal();
  pRendParams->maxrays        = m_maxRays_spin->GetIVal();
  pRendParams->raybounce      = m_rayBounce_spin->GetIVal();
  pRendParams->diffbounce     = m_diffBounce_spin->GetIVal();
  pRendParams->relative_error = m_relativeError_spin->GetFVal();
  pRendParams->env_clamp      = m_envClamp_spin->GetFVal();
  pRendParams->bsdf_clamp     = m_bsdfClamp_spin->GetFVal();
  pRendParams->bsdf_clamp_on  = IsDlgButtonChecked(m_hPathTracing, IDC_BSDF_CLAMP_ON);
  pRendParams->useRR          = IsDlgButtonChecked(m_hPathTracing, IDC_RR_ON);
  pRendParams->causticRays    = IsDlgButtonChecked(m_hPathTracing, IDC_CAUSTICS_ON);
  pRendParams->guided         = IsDlgButtonChecked(m_hPathTracing, IDC_GUIDED);
  pRendParams->qmcLights      = IsDlgButtonChecked(m_hPathTracing, IDC_QMC_LIGHTS);
  pRendParams->qmcMaterials   = IsDlgButtonChecked(m_hPathTracing, IDC_QMC_MATERIALS);


  //SPPM Caustics
  //pRendParams->maxphotons_c   = m_maxPhotonsCaust_spin->GetIVal();
  //pRendParams->caustic_power    = m_powerCaust_spin->GetFVal();
  //pRendParams->retrace_c        = retrace_c_spin->GetIVal();
  //pRendParams->initial_radius_c = m_initialRadiusCaust_spin->GetFVal();
  //pRendParams->visibility_c     = IsDlgButtonChecked(hSPPMC, IDC_VISIBILITY_C_ON);
  //pRendParams->alpha_c          = alpha_c_spin->GetFVal();


  //SPPM Diffuse
  //pRendParams->maxphotons_d     = m_maxPhotonsDiffuse_spin->GetIVal();
  //pRendParams->retrace_d        = m_retraceDiffuse_spin->GetIVal();
  //pRendParams->initial_radius_d = m_initialRadiusDiffuse_spin->GetFVal();
  //pRendParams->visibility_d     = IsDlgButtonChecked(hSPPMD, IDC_VISIBILITY_D_ON);
  //pRendParams->irr_map          = IsDlgButtonChecked(hSPPMD, IDC_IRRMAP);
  //pRendParams->alpha_d          = m_alphaDiffuse_spin->GetFVal();

  //Denoise
  pRendParams->denoiserOn           = IsDlgButtonChecked(m_hPostProc, IDC_DENOISER);
  pRendParams->denoiserAlbedoPassOn = IsDlgButtonChecked(m_hPostProc, IDC_DENOISE_COLOR_PASS_ON);
  pRendParams->denoiserNormalPassOn = IsDlgButtonChecked(m_hPostProc, IDC_DENOISE_NORMAL_PASS_ON);
  pRendParams->denoiseLvl           = m_denoiseLvl_slider->GetFVal();

  //Post process

  pRendParams->postProcOn      = IsDlgButtonChecked(m_hPostProc, IDC_POSTPROC_ON);
  pRendParams->exposure        = m_exposure_slider->       GetFVal();
  pRendParams->compress        = m_compress_slider->       GetFVal();
  pRendParams->contrast        = m_contrast_slider->       GetFVal();
  pRendParams->saturation      = m_saturation_slider->     GetFVal();
  pRendParams->vibrance        = m_vibrance_slider->       GetFVal();
  pRendParams->whiteBalance    = m_whiteBalance_slider->   GetFVal();
  pRendParams->whitePointColor = m_whitePoint_selector->   GetAColor();
  pRendParams->uniformContrast = m_uniformContrast_slider->GetFVal();
  pRendParams->normalize       = m_normalize_slider->      GetFVal();
  pRendParams->chromAberr      = m_chromAberr_slider->     GetFVal();
  pRendParams->vignette        = m_vignette_slider->       GetFVal();
  pRendParams->sharpness       = m_sharpness_slider->      GetFVal();
  pRendParams->sizeStar        = m_sizeStar_slider->       GetFVal();
  pRendParams->numRay          = m_numRay_slider->         GetIVal();
  pRendParams->rotateRay       = m_rotateRay_slider->      GetIVal();
  pRendParams->randomAngle     = m_randomAngle_slider->    GetFVal();
  pRendParams->sprayRay        = m_sprayRay_slider->       GetFVal();

  pRendParams->bloom           = IsDlgButtonChecked(m_hPostProc, IDC_BLOOM_ON);
  pRendParams->bloom_radius    = m_bloomRadius_spin->GetFVal();
  pRendParams->bloom_str       = m_bloomStr_spin->   GetFVal();
  //pRendParams->hydraFrameBuf   = IsDlgButtonChecked(m_hPostProc, IDC_FRAMEBUFFER);
  pRendParams->mlaa            = IsDlgButtonChecked(m_hPostProc, IDC_MLAA);


  //Override
  pRendParams->env_mult          = m_envMult_spin->GetFVal();
  pRendParams->overrideGrayMatOn = IsDlgButtonChecked(m_hOverr, IDC_GRAYMAT_ON);


  //MLT
 // pRendParams->mlt_plarge     = 0.5f; // (float)(m_mlt_plarge_slider->GetIVal()) / 100.0f;
  //pRendParams->mlt_iters_mult = m_mlt_itersMult_slider->GetIVal();
  //pRendParams->mlt_burn_iters = m_mlt_burnIters_slider->GetIVal()*64;
  //pRendParams->mlt_enable_median_filter = IsDlgButtonChecked(hMLT, IDC_MLT_MEDIAN_ON);
  //pRendParams->mlt_median_filter_threshold = m_mlt_medianThreshld_spin->GetFVal();


  //MMLT
  pRendParams->mmlt_estim_time              = m_mmlt_estimTime_slider->GetIVal();
  pRendParams->mmlt_burn_iters              = m_mmlt_burning_slider->GetIVal();
  pRendParams->mmlt_stepSize                = m_mmlt_stepSize_slider->GetIVal();
  pRendParams->mmlt_threads                 = m_mmlt_threads_slider->GetIVal();
  pRendParams->mmlt_enable_median_filter    = IsDlgButtonChecked(m_hMMLT, IDC_MMLT_MEDIAN_ON);
  pRendParams->mmlt_median_filter_threshold = m_mmlt_medianThreshld_spin->GetFVal();
  pRendParams->mmlt_multBrightness          = m_mmlt_multBrightness_spin->GetFVal();


  //Debug
  HWND debug_list = GetDlgItem(m_hDebug, IDC_DEBUG_LIST);
  numItems        = ListView_GetItemCount(debug_list);
  for (auto i     = 0; i < numItems; ++i)
  {
    hydraChar bufText[64];
    ListView_GetItemText(debug_list, i, 0, &bufText[0], sizeof(bufText))

    std::wstring debugParamName = strConverter::c2ws(bufText);
    pRendParams->debugCheckBoxes[debugParamName] = ListView_GetCheckState(debug_list, i);
  }

  pRendParams->enableLog                = IsDlgButtonChecked(m_hDebug, IDC_LOG);
  pRendParams->saveRendImagEveryOn      = IsDlgButtonChecked(m_hDebug, IDC_SAVE_REND_IMAGE_EVERY_ON);
  pRendParams->saveRendImagEveryMin     = m_saveRendImagEvery_spin->GetFVal();

  // Render elements
  pRendParams->renElemAlphaOn   = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_ALPHA);
  pRendParams->renElemColorOn   = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_COLOR);
  pRendParams->renElemCoordOn   = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_COORD);
  pRendParams->renElemCoverOn   = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_COVERAGE);
  pRendParams->renElemDepthOn   = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_DEPTH);
  pRendParams->renElemInstIdOn  = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_INSTID);
  pRendParams->renElemMatIdOn   = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_MATID);
  pRendParams->renElemNormalsOn = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_NORMALS);
  pRendParams->renElemObjIdOn   = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_OBJID);
  pRendParams->renElemShadowOn  = IsDlgButtonChecked(m_hRendElem, IDC_REND_ELEM_SHADOW);
}

void HydraRenderParamDlg::AcceptParams()
{
  //MessageBoxA(NULL, "HydraRenderParamDlg::AcceptParams", "", 0);

  if (m_pRend == nullptr)
    return;

  ReadGuiToRendParams(&m_pRend->m_rendParams);
}

void HydraRenderParamDlg::RejectParams()
{
  //MessageBoxA(NULL, "HydraRenderParamDlg::RejectParams", "", 0);
}


void HydraRenderParamDlg::SetPTParamsFromSlider()
{
  const float qualitySliderVal = m_renderQuality_slider->GetFVal() / 100.0f;

  const float min_maxRaysPP = 64.0f;
  const float max_maxRaysPP = 200000.0f;

  const float maxRaysPP     = (max_maxRaysPP - min_maxRaysPP)  * (qualitySliderVal*qualitySliderVal*qualitySliderVal*qualitySliderVal) + min_maxRaysPP;

  m_maxRays_spin->SetValue(maxRaysPP, TRUE);
  m_maxRays_edit->SetText(int(maxRaysPP));
}

void HydraRenderParamDlg::SetSliderFromPTParams()
{
  const float currRaysPP = m_maxRays_spin->GetIVal();

  const float min_maxRaysPP = 64.0f;
  const float max_maxRaysPP = 200000.0f;

  const float qualitySliderVal = pow((currRaysPP - min_maxRaysPP) / (max_maxRaysPP - min_maxRaysPP), 0.25f);

  m_renderQuality_slider->SetValue(qualitySliderVal * 100.0f, TRUE);
}

static INT_PTR CALLBACK HydraRenderParamDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitMainHydraDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseISlider(dlg->m_renderQuality_slider);

        ReleaseICustEdit(dlg->m_maxRays_edit);
        ReleaseICustEdit(dlg->m_rayBounce_edit);
        ReleaseICustEdit(dlg->m_diffBounce_edit);
        ReleaseICustEdit(dlg->m_timeLimit_edit);
        ReleaseICustEdit(dlg->m_lensRadius_edit);

        ReleaseISpinner(dlg->m_maxRays_spin);
        ReleaseISpinner(dlg->m_rayBounce_spin);
        ReleaseISpinner(dlg->m_diffBounce_spin);
        ReleaseISpinner(dlg->m_timeLimit_spin);
        ReleaseISpinner(dlg->m_lensRadius_spin);
      }
      break;

    case WM_COMMAND:
      switch (HIWORD(wParam))
      {
        case LBN_SELCHANGE:
        {
          if (LOWORD(wParam) == IDC_PRIMARY)
          {
          }
          //else if (LOWORD(wParam) == IDC_SECONDARY)
          //{
            //// If not Path Tracing, disable DOF and diffuse bounce, because it is not supported yet anywhere. 

            //int rendMeth = SendDlgItemMessage(dlg->m_hMain, IDC_SECONDARY, CB_GETCURSEL, 0, 0);
            //HWND checkBoxDof = GetDlgItem(hWnd, IDC_DOF_ON);
            //HWND diffBounce  = GetDlgItem(hWnd, IDC_DIFFBOUNCE_SPIN);

            //switch (rendMeth)
            //{
            //case RENDER_METHOD_PT:
            //  EnableWindow(checkBoxDof, true);
            //  EnableWindow(diffBounce, true);
            //  break;
            //case RENDER_METHOD_LT:
            //case RENDER_METHOD_IBPT:
            //  EnableWindow(diffBounce, true);
            //  SendMessage(checkBoxDof, BM_SETCHECK, BST_UNCHECKED, 0);
            //  EnableWindow(checkBoxDof, false);
            //  break;
            //case RENDER_METHOD_MMLT:
            //  EnableWindow(diffBounce, false);
            //  SendMessage(checkBoxDof, BM_SETCHECK, BST_UNCHECKED, 0);
            //  EnableWindow(checkBoxDof, false);
            //  break;
            //default:
            //  break;
            //}
          //}
        }
        break;
        case BN_CLICKED:
          if (LOWORD(wParam) == IDC_INF_QUALITY_ON)
          {
            LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
            HWND winQual     = GetDlgItem(hWnd, IDC_RENDERQUALITY_SLIDER);
            HWND winMaxray   = GetDlgItem(hWnd, IDC_MAXRAY_SPIN);
            HWND winDraft    = GetDlgItem(hWnd, IDC_QUAL_DRAFT_STATIC);
            HWND winLow      = GetDlgItem(hWnd, IDC_QUAL_LOW_STATIC);
            HWND winMed      = GetDlgItem(hWnd, IDC_QUAL_MEDIUM_STATIC);
            HWND winHigh     = GetDlgItem(hWnd, IDC_QUAL_HIGH_STATIC);
            HWND winUltra    = GetDlgItem(hWnd, IDC_QUAL_ULTRA_STATIC);
            HWND winSpp      = GetDlgItem(hWnd, IDC_SPP_STATIC);

            if (chkState == BST_CHECKED)
            {
              EnableWindow(winQual, false);
              EnableWindow(winMaxray, false);
              EnableWindow(winDraft, false);
              EnableWindow(winLow, false);
              EnableWindow(winMed, false);
              EnableWindow(winHigh, false);
              EnableWindow(winUltra, false);
              EnableWindow(winSpp, false);
            }
            else
            {
              EnableWindow(winQual, true);
              EnableWindow(winMaxray, true);
              EnableWindow(winDraft, true);
              EnableWindow(winLow, true);
              EnableWindow(winMed, true);
              EnableWindow(winHigh, true);
              EnableWindow(winUltra, true);
              EnableWindow(winSpp, true);
            }
          }
          else if (LOWORD(wParam) == IDC_TIMELIMIT)
          {
            LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
            HWND winTimeLim = GetDlgItem(hWnd, IDC_TIMELIMIT_SPIN);

            if (chkState == BST_CHECKED) EnableWindow(winTimeLim, true);
            else                         EnableWindow(winTimeLim, false);
          }
          else if (LOWORD(wParam) == IDC_REMEMBER_MDLG)
          {
            LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);

            if (chkState == BST_UNCHECKED)
              CreateDeviceSettingsFile(-1, dlg->m_pRend->m_rendParams.device_id, g_hrPlugResMangr.m_rendRef);
            else
            {
              int mode = (int)SendDlgItemMessage(hWnd, IDC_ENGINE, CB_GETCURSEL, 0, 0);
              HWND devices_list = GetDlgItem(hWnd, IDC_DEVICES_LIST);
              int numItems = ListView_GetItemCount(devices_list);
              std::unordered_map<int, bool> deviceIDs;

              for (auto i = 0; i < numItems; ++i)
              {
                hydraChar bufText[4];
                ListView_GetItemText(devices_list, i, 0, &bufText[0], sizeof(bufText));
                if (ListView_GetCheckState(devices_list, i))
                  deviceIDs[std::stoi(bufText)] = true;
                else
                  deviceIDs[std::stoi(bufText)] = false;
              }

              CreateDeviceSettingsFile(mode, deviceIDs, g_hrPlugResMangr.m_rendRef);
            }
          }
          break;
      }
      break;
    case CC_SLIDER_CHANGE:
      if (LOWORD(wParam) == IDC_RENDERQUALITY_SLIDER)
        dlg->SetPTParamsFromSlider();
      break;
    case CC_SPINNER_CHANGE:
      if (LOWORD(wParam) == IDC_MAXRAY_SPIN)
        dlg->SetSliderFromPTParams();
      else if (LOWORD(wParam) == IDC_RAYBOUNCE_SPIN)
      {
        const float rayBounce = dlg->m_rayBounce_spin->GetFVal();
        const float diffBounce = dlg->m_diffBounce_spin->GetFVal();
        if (diffBounce > rayBounce - 2)
          dlg->m_diffBounce_spin->SetValue(rayBounce - 2, TRUE);
      }
      else if (LOWORD(wParam) == IDC_DIFFBOUNCE_SPIN)
      {
        const float rayBounce = dlg->m_rayBounce_spin->GetFVal();
        const float diffBounce = dlg->m_diffBounce_spin->GetFVal();
        if (diffBounce > rayBounce - 2)
          dlg->m_rayBounce_spin->SetValue(diffBounce + 2, TRUE);
      }
      break;
    case WM_NOTIFY:
      switch (((LPNMHDR)lParam)->code)
      {
        case LVN_ITEMCHANGED:
          if (((LPNMHDR)lParam)->idFrom == IDC_DEVICES_LIST)
          {
            HWND checkBoxRememberDevice = GetDlgItem(hWnd, IDC_REMEMBER_MDLG);
            SendMessage(checkBoxRememberDevice, BM_SETCHECK, BST_UNCHECKED, 0);
          }
          break;
      }
      break;
    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK PathTracingDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:
      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitPathTracingDialog(hWnd);
      }
      break;
    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_minRays_edit);
        ReleaseICustEdit(dlg->m_relativeError_edit);
        ReleaseICustEdit(dlg->m_bsdfClamp_edit);
        ReleaseICustEdit(dlg->m_envClamp_edit);

        ReleaseISpinner(dlg->m_minRays_spin);
        ReleaseISpinner(dlg->m_relativeError_spin);
        ReleaseISpinner(dlg->m_bsdfClamp_spin);
        ReleaseISpinner(dlg->m_envClamp_spin);
      }
      break;
    case WM_COMMAND:
      switch (HIWORD(wParam))
      {
        case BN_CLICKED:
          if (LOWORD(wParam) == IDC_BSDF_CLAMP_ON)
          {
            LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
            if (chkState == BST_UNCHECKED)
            {
              dlg->m_pRend->m_rendParams.bsdf_clamp_on = false;
              dlg->m_pRend->m_rendParams.bsdf_clamp = 1000000.0F;
              dlg->m_bsdfClamp_edit->SetText(1000000.0F);
              dlg->m_bsdfClamp_edit->Disable();
              dlg->m_bsdfClamp_spin->Disable();
            }
            else
            {
              dlg->m_pRend->m_rendParams.bsdf_clamp_on = true;
              dlg->m_pRend->m_rendParams.bsdf_clamp = 5.0F;
              dlg->m_bsdfClamp_edit->SetText(5.0F);
              dlg->m_bsdfClamp_edit->Enable();
              dlg->m_bsdfClamp_spin->Enable();
            }
          }
          break;
        case LBN_SELCHANGE:
          if (LOWORD(wParam) == IDC_PRESETS)
          {
            int pr = (int)SendDlgItemMessage(dlg->m_hPathTracing, IDC_PRESETS, CB_GETCURSEL, 0, 0);
            dlg->LoadPreset(pr);
          }
          break;
      }
      break;
    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
      break;
  }
  return TRUE;
}

static INT_PTR CALLBACK MLTDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitMLTDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_mlt_medianThreshld_edit);
        ReleaseICustEdit(dlg->m_mlt_burnIters_edit);
        ReleaseICustEdit(dlg->m_mlt_plarge_edit);
        ReleaseICustEdit(dlg->m_mlt_itersMult_edit);

        ReleaseISpinner(dlg->m_mlt_medianThreshld_spin);

        ReleaseISlider(dlg->m_mlt_burnIters_slider);
        ReleaseISlider(dlg->m_mlt_plarge_slider);
        ReleaseISlider(dlg->m_mlt_itersMult_slider);
      }
      break;

    case WM_COMMAND:
      break;

    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK MMLTDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:
    {
      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitMMLTDialog(hWnd);
      }

      break;
    }
    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_mmlt_burning_edit);
        ReleaseICustEdit(dlg->m_mmlt_threads_edit);
        ReleaseICustEdit(dlg->m_mmlt_medianThreshld_edit);
        ReleaseICustEdit(dlg->m_mmlt_multBrightness_edit);

        ReleaseISlider(dlg->m_mmlt_burning_slider);
        ReleaseISlider(dlg->m_mmlt_threads_slider);
        ReleaseISpinner(dlg->m_mmlt_medianThreshld_spin);
        ReleaseISpinner(dlg->m_mmlt_multBrightness_spin);
      }
      break;

    case WM_COMMAND:
      break;

    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
    {
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    }
    default:
      return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK SPPMCDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitSPPMCDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_maxPhotonsCaust_edit);
        ReleaseICustEdit(dlg->m_initialRadiusCaust_edit);
        ReleaseICustEdit(dlg->m_powerCaust_edit);
        ReleaseICustEdit(dlg->m_retraceCaust_edit);
        ReleaseICustEdit(dlg->m_alphaCaust_edit);

        ReleaseISpinner(dlg->m_maxPhotonsCaust_spin);
        ReleaseISpinner(dlg->m_initialRadiusCaust_spin);
        ReleaseISpinner(dlg->m_powerCaust_spin);
        ReleaseISpinner(dlg->retrace_c_spin);
        ReleaseISpinner(dlg->alpha_c_spin);
      }
      break;

    case WM_COMMAND:
      // We don't care about the UI controls.
      // We take the value in AcceptParams() instead.
      break;

    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK SPPMDDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitSPPMDDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_maxPhotonsDiffuse_edit);
        ReleaseICustEdit(dlg->m_initialRadiusDiffuse_edit);
        ReleaseICustEdit(dlg->m_retraceDiffuse_edit);
        ReleaseICustEdit(dlg->m_alphaDiffuse_edit);

        ReleaseISpinner(dlg->m_maxPhotonsDiffuse_spin);
        ReleaseISpinner(dlg->m_initialRadiusDiffuse_spin);
        ReleaseISpinner(dlg->m_retraceDiffuse_spin);
        ReleaseISpinner(dlg->m_alphaDiffuse_spin);
      }
      break;

    case WM_COMMAND:
      // We don't care about the UI controls.
      // We take the value in AcceptParams() instead.
      break;

    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }
  return TRUE;
}



static INT_PTR CALLBACK PostProcessingDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  bool postProcessUpdate = false;

  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);

  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitPostProcDialog(hWnd);
      }
      //dlg->m_iRendParam->GetTabIRollup(kTabClassID)->Hide(dlg->m_rollupIndices[dlg->POSTPROC]);
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_exposure_edit);
        ReleaseICustEdit(dlg->m_compress_edit);
        ReleaseICustEdit(dlg->m_contrast_edit);
        ReleaseICustEdit(dlg->m_saturation_edit);
        ReleaseICustEdit(dlg->m_vibrance_edit);
        ReleaseICustEdit(dlg->m_whiteBalance_edit);
        ReleaseIColorSwatch(dlg->m_whitePoint_selector);
        ReleaseICustEdit(dlg->m_uniformContrast_edit);
        ReleaseICustEdit(dlg->m_normalize_edit);
        ReleaseICustEdit(dlg->m_chromAberr_edit);
        ReleaseICustEdit(dlg->m_vignette_edit);
        ReleaseICustEdit(dlg->m_sharpness_edit);
        ReleaseICustEdit(dlg->m_sizeStar_edit);
        ReleaseICustEdit(dlg->m_numRay_edit);
        ReleaseICustEdit(dlg->m_rotateRay_edit);
        ReleaseICustEdit(dlg->m_randomAngle_edit);
        ReleaseICustEdit(dlg->m_sprayRay_edit);

        ReleaseICustEdit(dlg->m_bloomRadius_edit);
        ReleaseICustEdit(dlg->m_bloomStr_edit);


        ReleaseISlider(dlg->m_exposure_slider);
        ReleaseISlider(dlg->m_compress_slider);
        ReleaseISlider(dlg->m_contrast_slider);
        ReleaseISlider(dlg->m_saturation_slider);
        ReleaseISlider(dlg->m_vibrance_slider);
        ReleaseISlider(dlg->m_whiteBalance_slider);
        ReleaseISlider(dlg->m_uniformContrast_slider);
        ReleaseISlider(dlg->m_normalize_slider);
        ReleaseISlider(dlg->m_chromAberr_slider);
        ReleaseISlider(dlg->m_vignette_slider);
        ReleaseISlider(dlg->m_sharpness_slider);
        ReleaseISlider(dlg->m_sizeStar_slider);
        ReleaseISlider(dlg->m_numRay_slider);
        ReleaseISlider(dlg->m_rotateRay_slider);
        ReleaseISlider(dlg->m_randomAngle_slider);
        ReleaseISlider(dlg->m_sprayRay_slider);

        ReleaseISpinner(dlg->m_bloomRadius_spin);
        ReleaseISpinner(dlg->m_bloomStr_spin);

        ReleaseICustButton(dlg->m_denoise_btn);
        ReleaseICustButton(dlg->m_resetPostProc);

        ReleaseICustEdit(dlg->m_denoiseLvl_edit);
        ReleaseISlider(dlg->m_denoiseLvl_slider);

      }
      break;

    case WM_COMMAND:
      if (HIWORD(wParam) == BN_CLICKED)
      {
        switch (LOWORD(wParam))
        {
        case IDC_DENOISER:
          postProcessUpdate = true;
          break;
        case IDC_DENOISE_COLOR_PASS_ON:
        {
          postProcessUpdate        = true;
          const LRESULT chkState   = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
          const HWND winNormPass = GetDlgItem(hWnd, IDC_DENOISE_NORMAL_PASS_ON);

          if (chkState == BST_CHECKED)
            EnableWindow(winNormPass, true);
          else                     
          {
            SendMessage(winNormPass, BM_SETCHECK, BST_UNCHECKED, 0);
            EnableWindow(winNormPass, false);            
            dlg->m_pRend->m_rendParams.denoiserNormalPassOn = false;          
          }
        }
        break;
        case IDC_DENOISE_NORMAL_PASS_ON:
          postProcessUpdate = true;
          //const LRESULT chkState = SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);                        
          //if (chkState == BST_CHECKED) 
          break;
        case IDC_POSTPROC_ON:
          postProcessUpdate = true;
          break;
        case IDC_RESET_POSTPROC:
          dlg->m_pRend->ResetPostProc(dlg);
          postProcessUpdate = true;
          break;
        default:
          break;
        }
      }
      break;
    case CC_SLIDER_BUTTONUP:
    case CC_COLOR_CLOSE:
    case WM_CUSTEDIT_ENTER:
      postProcessUpdate = true;
      break;
    case CC_COLOR_SEL:
      if (dlg->m_pRend->m_rendParams.postProcOn)
      {
        if (dlg->m_pRend->m_rendParams.denoiserOn)
        {
          dlg->m_pRend->DoDenoise(dlg->m_pRend->m_pLastRender );
          dlg->m_pRend->DisplayLastRender();
        }
        else
          dlg->m_pRend->DisplaySource(dlg);
      }
      break;
    case WM_LBUTTONDOWN:      
    case WM_MOUSEMOVE:
      break;
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }

  if (postProcessUpdate && dlg && dlg->m_pRend && dlg->m_pRend->m_pLastRender)
    dlg->m_pRend->UpdatePostProc(dlg);

  return TRUE;
}

static INT_PTR CALLBACK OverrideDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitOverrideDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_envMult_edit);
        ReleaseISpinner(dlg->m_envMult_spin);
      }
      break;

    case WM_COMMAND:
      // We don't care about the UI controls.
      // We take the value in AcceptParams() instead.
      break;

    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK ToolsDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitToolsDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustButton(dlg->m_runConvertToHydraScript_btn);
        ReleaseICustButton(dlg->m_runConvertFromHydraScript_btn);
        ReleaseICustButton(dlg->m_runOverrideHydraMatScript_btn);
      }
      break;

    case WM_COMMAND:
      switch (HIWORD(wParam))
      {
        case BN_CLICKED:
          if      (LOWORD(wParam) == IDC_CONVERT_TO_HYDRA)   filein_script((HYDRA_PLUGIN_PATH / L"ConvertToHydra.ms").c_str());
          else if (LOWORD(wParam) == IDC_CONVERT_FROM_HYDRA) filein_script((HYDRA_PLUGIN_PATH / L"ConvertFromHydra.ms").c_str());
          else if (LOWORD(wParam) == IDC_OVERR_PARAM_MAT)    filein_script((HYDRA_PLUGIN_PATH / L"OverrideParamHydraMat.ms").c_str());
      }
      break;

    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK RendElemDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:
      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitRendElemDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      break;

    case WM_COMMAND:
      switch (HIWORD(wParam))
      {
        case BN_CLICKED:
          if (LOWORD(wParam) == IDC_GENERATE_RENDER_ELEMENTS)
          {
            // Press button for generate pass.
          }
          break;
      }
      break;

    case WM_LBUTTONDOWN:
      break;
    case WM_MOUSEMOVE:
      break;
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;

    default:
      return FALSE;
  }
  return TRUE;
}

static INT_PTR CALLBACK DebugDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraRenderParamDlg *dlg = DLGetWindowLongPtr<HydraRenderParamDlg*>(hWnd);
  switch (msg)
  {
    case WM_INITDIALOG:

      dlg = (HydraRenderParamDlg*)lParam;
      DLSetWindowLongPtr(hWnd, lParam);
      if (dlg)
      {
        if (dlg->m_prog)
          dlg->InitProgDialog();
        else
          dlg->InitDebugDialog(hWnd);
      }
      break;

    case WM_DESTROY:
      if (!dlg->m_prog)
      {
        ReleaseICustEdit(dlg->m_saveRendImagEvery_edit);
        ReleaseISpinner(dlg->m_saveRendImagEvery_spin);
      }
      break;

    case WM_COMMAND:
      break;

    case WM_LBUTTONDOWN:
    case WM_MOUSEMOVE:
    case WM_LBUTTONUP:
      dlg->m_iRendParam->RollupMouseMessage(hWnd, msg, wParam, lParam);
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

RendParamDlg *HydraRenderPlugin::CreateParamDialog(IRendParams *m_iRendParam, BOOL m_prog)
{
  if (!m_prog)
  {
    HydraRenderPlugin::m_pLastParamDialog = new HydraRenderParamDlg(this, m_iRendParam, m_prog);
    return HydraRenderPlugin::m_pLastParamDialog;
  }
  else
  {
    return nullptr;
  }
}



//static const Class_ID blurRaytrace ( 0x4fa95e9b, 0x9a26e66 );


BaseInterface* HydraRenderPlugin::GetInterface(Interface_ID id)
{
  if (id == TAB_DIALOG_OBJECT_INTERFACE_ID)
  {
    ITabDialogObject* r = this;
    return r;
  }
  /*else if (id == IREND_ELEM_MGR_INTERFACE)
  {
    return this;
  }*/
  else
  {
    return Renderer::GetInterface(id);
  }
}

// ITabDialogObject
// Add the pages you want to the dialog. Use tab as the plugin
// associated with the pages. This will allows the manager
// to remove and add the correct pages to the dialog.
void HydraRenderPlugin::AddTabToDialog(ITabbedDialog* dialog, ITabDialogPluginTab* tab)
{
  //dialog->AddRollout ( GetString( IDS_CLASS_NAME ), NULL,
  //   kTabClassID, tab, -1, kRendRollupWidth, 0,
  //   0, ITabbedDialog::kSystemPage );
  static const int kRendRollupWidth = 222;

  dialog->AddRollout(L"Engine", NULL,
                     kTabClassID, tab, -1, kRendRollupWidth, 0,
                     0, ITabbedDialog::kSystemPage);

  dialog->AddRollout(L"Post process", NULL,
                     kTabClassID2, tab, -1, kRendRollupWidth, 0,
                     0, ITabbedDialog::kSystemPage);

  dialog->AddRollout(L"Tools", NULL,
                     kTabClassID3, tab, -1, kRendRollupWidth, 0,
                     0, ITabbedDialog::kSystemPage);

  dialog->AddRollout(L"Render elements", NULL,
                     kTabClassID4, tab, -1, kRendRollupWidth, 0,
                     0, ITabbedDialog::kSystemPage);

}

int HydraRenderPlugin::AcceptTab(ITabDialogPluginTab* tab)
{
  Class_ID id = tab->GetClassID();

  /*switch (tab->GetSuperClassID())
  {
  case RENDER_ELEMENT_CLASS_ID:
    return TAB_DIALOG_ADD_TAB;
  }*/

  if (id == HydraRender_CLASS_ID)
    return TAB_DIALOG_ADD_TAB;
  else
    return 0;

  /*

  switch ( tab->GetSuperClassID ( ) ) {
   case RADIOSITY_CLASS_ID:
      return 0;         // Don't show the advanced lighting tab
   }
   */
   // Accept all other tabs
   //return TAB_DIALOG_ADD_TAB;
}