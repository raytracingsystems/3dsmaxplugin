#include "HydraRenderer.h"

#include <unordered_map>
#include <string>
#include <sstream>

#include <sys/stat.h>
#include <time.h>
#include <sstream>
#include <thread>
#include <codecvt>
#include <mutex>
#include <icurvctl.h>

#include "gamma.h"

////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;




std::string escape(const std::string& data)
{
  std::string buffer;
  buffer.reserve(data.size() + 10);

  for (size_t pos = 0; pos != data.size(); ++pos)
  {
    switch (data[pos])
    {
      case '&':  buffer.append("&amp;");       break;
      case '\"': buffer.append("&quot;");      break;
      case '\'': buffer.append("&apos;");      break;
      case '<':  buffer.append("&lt;");        break;
      case '>':  buffer.append("&gt;");        break;
      default:   buffer.append(&data[pos], 1); break;
    }
  }

  return buffer;
}





inline bool file_exists(const std::string& name)
{
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}


void evalComponentColorFaloff(const float mat_color[3], const Color& falloff_1, const Color& falloff_2, float mult, bool multOn,
                              Color* f1, Color* f2)
{
  if (multOn)
  {
    f1->r = mat_color[0] * mult * falloff_1.r;
    f1->g = mat_color[1] * mult * falloff_1.g;
    f1->b = mat_color[2] * mult * falloff_1.b;

    f2->r = mat_color[0] * mult * falloff_2.r;
    f2->g = mat_color[1] * mult * falloff_2.g;
    f2->b = mat_color[2] * mult * falloff_2.b;
  }
  else
  {
    f1->r = mult * falloff_1.r;
    f1->g = mult * falloff_1.g;
    f1->b = mult * falloff_1.b;

    f2->r = mult * falloff_2.r;
    f2->g = mult * falloff_2.g;
    f2->b = mult * falloff_2.b;
  }

}

Color EvalComponentColorAPI(const Color& color, const float mult, const bool multOn, const bool hasTexture)
{
  Color result(0.0F, 0.0F, 0.0F);

  if (hasTexture)
  {
    if (multOn)
    {
      result.r = color.r * mult;
      result.g = color.g * mult;
      result.b = color.b * mult;
    }
    else
    {
      result.r = mult;
      result.g = mult;
      result.b = mult;
    }
  }
  else
  {
    result.r = color.r * mult;
    result.g = color.g * mult;
    result.b = color.b * mult;
  }
  return result;
}


Texmap* HydraRenderPlugin::GetTextureFromSlot(Mtl* mtl, const std::wstring& reqSlotName)
{
  const int numSubTex = mtl->NumSubTexmaps();

  for (int i = 0; i < numSubTex; ++i) // all slots in material: diffuse, reflect and etc.
  {
    const std::wstring matSlotName = strConverter::c2ws(mtl->GetSubTexmapSlotName(i).data());

    if (matSlotName == reqSlotName)
      return mtl->GetSubTexmap(i); // get texture from current slot  
  }

  return nullptr;
}

bool HydraRenderPlugin::HydraMtlGetChangedTexturesSlotNames(const std::unordered_set<std::wstring> &changedParams, std::vector<std::wstring> &slotNames) const
{
  if (changedParams.empty())
    return false;

  if ((changedParams.find(L"mtl_diffuse_map") != changedParams.end()))
    slotNames.push_back(L"DiffuseMap");

  if ((changedParams.find(L"mtl_reflect_map") != changedParams.end()))
    slotNames.push_back(L"ReflectivityMap");

  if ((changedParams.find(L"mtl_emission_map") != changedParams.end()))
    slotNames.push_back(L"EmissionMap");

  if ((changedParams.find(L"mtl_transparency_map") != changedParams.end()))
    slotNames.push_back(L"TransparencyMap");

  if ((changedParams.find(L"mtl_normal_map") != changedParams.end()))
    slotNames.push_back(L"NormalMap");

  if ((changedParams.find(L"mtl_refl_gl_map") != changedParams.end()))
    slotNames.push_back(L"ReflectGlossinessMap");

  if ((changedParams.find(L"mtl_transp_gl_map") != changedParams.end()))
    slotNames.push_back(L"TranspGlossinessMap");

  if ((changedParams.find(L"mtl_opacity_map") != changedParams.end()))
    slotNames.push_back(L"OpacityMap");

  if ((changedParams.find(L"mtl_translucency_map") != changedParams.end()))
    slotNames.push_back(L"TranslucencyMap");

  return true;
}

void HydraRenderPlugin::HydraMtlGetAllTexturesSlotNames(std::vector<std::wstring> &slotNames) const
{
  std::vector<std::wstring> names = { L"DiffuseMap", L"SpecularMap", L"SpecularGlossinessMap", L"ReflectivityMap", L"ReflectGlossinessMap",
    L"ReflectivityAnisotropyMap", L"ReflectivityRotationMap", L"TransparencyMap", L"TranspGlossinessMap", L"OpacityMap", L"TranslucencyMap",
    L"EmissionMap", L"NormalMap", L"ColorMap" }; // L"ColorMap" for HydraMtlLight
  slotNames = names;
}


void HydraRenderPlugin::ExtractMaterialRecursive(Mtl *mtl)
{
  if (!mtl)  
    return;
  
  int subMats = mtl->NumSubMtls();

  if (subMats == 0)
    ExtractMaterial(mtl);
  else
  {
    std::vector <HRMaterialRef> subMatRefs;
    for (int j = 0; j < subMats; j++)
    {
      Mtl* subMtl = mtl->GetSubMtl(j);
      if (subMtl)
      {
        ExtractMaterialRecursive(subMtl);
        subMatRefs.insert(subMatRefs.begin(), g_hrPlugResMangr.m_materialsRefLib[subMtl]);
      }
      else
        subMatRefs.emplace(subMatRefs.begin(), HRMaterialRef());
    }

    if (mtl->ClassID().PartA() == MIXMAT_CLASS_ID) //standard Blend material      
      ExtractBlendMaterial(mtl, subMatRefs);      
  }
}


void HydraRenderPlugin::NodeRecursive(INode* pNode)
{
  ExtractMaterialRecursive(pNode->GetMtl());

  for (int i = 0; i < pNode->NumberOfChildren(); ++i)
    NodeRecursive(pNode->GetChildNode(i));  
}


void HydraRenderPlugin::ExtractMaterialList()
{
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"ExtractMaterialList.");
  m_rendParams.progCallback->SetTitle(_T("Extract material list..."));

  clock_t start;
  clock_t finish;
  start = clock();

  INode* pRootNode = GetCOREInterface()->GetRootNode();
  NodeRecursive(pRootNode);

  finish = clock();
  const auto exportTime = (finish - start) / CLOCKS_PER_SEC;
  g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Materials export time (sec.)", exportTime);
}


void HRplugResourceManager::ExtractPlaceholderMaterial()
{
  hrMaterialOpen(m_placeholderMatRef, HR_WRITE_DISCARD);  
  auto matNode = hrMaterialParamNode(m_placeholderMatRef);
  auto diff    = matNode.force_child(L"diffuse");
  diff.force_attribute(L"brdf_type").set_value(L"lambert");
  diff.force_child(L"color").append_attribute(L"val") = L"0.078 0.0 0.156";
  hrMaterialClose(m_placeholderMatRef);
}

void HRplugResourceManager::CreateOverrideGrayMat()
{
  // Create gray material for override all materials from GUI render setup
  hrMaterialOpen(m_grayMat, HR_WRITE_DISCARD);
  auto matNode = hrMaterialParamNode(m_grayMat);
  auto diff    = matNode.force_child(L"diffuse");
  diff.force_attribute(L"brdf_type").set_value(L"lambert");
  diff.force_child(L"color").append_attribute(L"val") = L"0.5 0.5 0.5";
  hrMaterialClose(m_grayMat);
}

void HydraRenderPlugin::ExtractSampler(pugi::xml_node &texNode, const texInfo &info, Texmap* tex)
{
  std::vector<float> samplerMatrix(16);
  int   filter            = 0;
  int   texTilingFlags    = 0;
  bool  hasScreenMapping  = false;

  if (tex != nullptr && tex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
    filter = dynamic_cast<BitmapTex*>(tex)->GetFilterType(); //-V522

  //auto texRef             = std::get<0>(info);
  auto uvgen              = std::get<1>(info);
  auto gamma              = std::get<2>(info);
  auto alphaSrc           = std::get<3>(info);

  if (uvgen != nullptr)
  {
    ExtractTextureMatrixFromUVGen(uvgen, samplerMatrix, texTilingFlags, hasScreenMapping);

    texNode.force_attribute(L"matrix");

    if      (texTilingFlags & U_WRAP)   texNode.force_attribute(L"addressing_mode_u").set_value(L"wrap");
    else if (texTilingFlags & U_MIRROR) texNode.force_attribute(L"addressing_mode_u").set_value(L"mirror");
    else                                texNode.force_attribute(L"addressing_mode_u").set_value(L"clamp");

    if      (texTilingFlags & V_WRAP)   texNode.force_attribute(L"addressing_mode_v").set_value(L"wrap");
    else if (texTilingFlags & V_MIRROR) texNode.force_attribute(L"addressing_mode_v").set_value(L"mirror");
    else                                texNode.force_attribute(L"addressing_mode_v").set_value(L"clamp");

    if      (hasScreenMapping)          texNode.force_attribute(L"channel").set_value(L"camera_mapped");
    else                                texNode.remove_attribute(L"channel");

    if      (filter == 2)               texNode.force_attribute(L"filter").set_value(L"point"); // no filtering
    else                                texNode.remove_attribute(L"filter");
      
    
    HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", &samplerMatrix[0]);
  }
  else //write some default sampler attributes if there's no uvGen
  {
    texNode.force_attribute(L"matrix");
    std::vector<float> samplerMatrixDefault = { 1.0F, 0.0F, 0.0F, 0.0F,
                                                0.0F, 1.0F, 0.0F, 0.0F,
                                                0.0F, 0.0F, 1.0F, 0.0F,
                                                0.0F, 0.0F, 0.0F, 1.0F };
    texNode.force_attribute(L"addressing_mode_u").set_value(L"wrap");
    texNode.force_attribute(L"addressing_mode_v").set_value(L"wrap");

    HydraXMLHelpers::WriteMatrix4x4(texNode, L"matrix", &samplerMatrixDefault[0]);
  }

  texNode.force_attribute(L"input_gamma").set_value(gamma);
  texNode.force_attribute(L"input_alpha").set_value(alphaSrc.c_str());
}

void HydraRenderPlugin::BindingTextures(Texmap* const tex, const pugi::xml_node& matNode, const HRTextureNodeRef& texRef, 
  const std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const std::wstring& a_slotName, const wchar_t* nameChild)
{
  if (tex == nullptr) return;

  if (tex->ClassID() == HydraBlendedBox_CLASS_ID)
  {
    const float blendSize = FindFloat(L"BlendSize", tex);
    const float mapScale  = FindFloat(L"MapScale", tex);
    BindingHexaplanar(matNode, texRef, texRef, texRef, texRef, texRef, texRef, 20.0F / blendSize + 1.0F, mapScale);
  }
  else if (tex->ClassName() == L"Falloff")
  {
    //color1(Color_1) : RGB color
    //map1Amount(Map_Amount_1) : float
    //map1(Map_1) : texturemap
    //map1On(Map_1_Enable) : boolean
    //color2(Color_2) : RGB color
    //map2Amount(Map_Amount_2) : float
    //map2(Map_2) : texturemap
    //map2On(Map_2_Enable) : boolean
    //type(Falloff_Type) : integer
    //direction(Falloff_Direction) : integer
    //node(Falloff_Direction_Object) : node
    //mtlIOROverride(Mtl_IOR_Override_Enable) : boolean
    //ior(Index_of_Refraction) : float
    //extrapolateOn(Distance_Blend_Extrapolate) : boolean
    //nearDistance(Near_Distance) : float
    //farDistance(Far_Distance) : float

    const Color color1   = FindColor(L"Color 1", tex);
    const Color color2   = FindColor(L"Color 2", tex);
    BindingFalloff(matNode, nameChild, color1, color2);
  }
  else if (tex->ClassID() == HydraAO_CLASS_ID)
  {
    const Color color1   = FindColor(L"OccludedColor"    , tex);
    const Color color2   = FindColor(L"UnoccludedColor"  , tex);
    const float distance = FindFloat(L"Distance"         , tex);
    const float falloff  = FindFloat(L"Falloff"          , tex);
    const int   dir      = FindInt  (L"Distribution"     , tex);
    const bool  local    = FindBool (L"OnlyForThisObject", tex);
    BindingAO(matNode, color1, color2, distance, falloff, dir, local/*, occludedTex, unoccludedTex, distanceTex*/);
  }
  else
  {
    auto texNode = hrTextureBind(texRef, matNode, nameChild);
    ExtractSampler(texNode, a_texturesSlotToRef.at(a_slotName), tex);
  }
}


/////////////////////////////


void HydraRenderPlugin::ExtractHydraDiffuse(IHydraMtl* a_mtl, pugi::xml_node& a_mat,
  std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport)
{
  if (!a_mtl)
    return;

  const std::wstring  diffuseSlotName = L"DiffuseMap";

  const Color         color           = a_mtl->GetDiffuseColor();
  Texmap* const       diffTexture     = a_mtl->GetDiffuseTexmap();
  const float         mult            = a_mtl->GetDiffuseMult();
  const bool          hasTint         = a_mtl->GetDiffuseTint();
  const float         roughness       = a_mtl->GetDiffuseRoughness();

  const bool          hasDiffTexture  = (diffTexture != nullptr);
  const Color         resColor        = EvalComponentColorAPI(color, mult, hasTint, hasDiffTexture);

  // XML export

  auto diff = a_mat.force_child(L"diffuse");

  if (a_cleanExport && (Luminance(Point3(resColor)) < FLT_EPSILON))
    return;


  if (roughness < 0.0001F)  diff.force_attribute(L"brdf_type").set_value(L"lambert");
  else                      diff.force_attribute(L"brdf_type").set_value(L"orennayar");

  auto colorNode = diff.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(resColor).c_str());

  if (hasDiffTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[diffuseSlotName]);
    BindingTextures(diffTexture, colorNode, texRef, a_texturesSlotToRef, diffuseSlotName, L"texture");
  }
  else if (colorNode.child(L"texture"))
    colorNode.remove_child(L"texture");

  diff.force_child(L"roughness").force_attribute(L"val").set_value(roughness);

  if (hasTint)
    colorNode.force_attribute(L"tex_apply_mode").set_value(L"multiply");

}


void HydraRenderPlugin::ExtractHydraReflect(IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport)
{
  const std::wstring  reflectSlotName      = L"ReflectivityMap";
  const std::wstring  reflectGlossName     = L"ReflectGlossinessMap";
  const std::wstring  reflectAnisName      = L"ReflectivityAnisotropyMap";
  const std::wstring  reflectAnisRotName   = L"ReflectivityRotationMap";

  const Color         color                = a_mtl->GetReflColor();
  Texmap* const       reflTexture          = a_mtl->GetReflTexmap();
  const bool          hasTint              = a_mtl->GetReflTint();
  const float         mult                 = a_mtl->GetReflMult();
  const float         glossiness           = a_mtl->GetReflGloss();
  Texmap* const       reflGlossTexture     = a_mtl->GetReflGlossTexmap();
  const int           reflBrdf             = a_mtl->GetReflBRDF();
  const float         ior                  = a_mtl->GetReflIor();
  const bool          fresnel              = a_mtl->GetReflFresnel();
  const int           extrusion            = a_mtl->GetReflExtrus();
  const float         anisotr              = a_mtl->GetReflAnisotr();
  Texmap* const       reflAnisTexture      = a_mtl->GetReflAnisTexmap();
  const float         anisRotat            = a_mtl->GetReflAnisRotat();
  Texmap* const       reflAnisRotatTexture = a_mtl->GetReflAnisRotatTexmap();

  const bool          hasReflTexture       = (reflTexture != nullptr);
  const Color         resColor             = EvalComponentColorAPI(color, mult, hasTint, hasReflTexture);

  // XML export

  if (a_cleanExport && (Luminance(Point3(resColor)) < FLT_EPSILON))
    return;

  auto refl = a_mat.force_child(L"reflectivity");

  switch (reflBrdf)
  {
  case BRDF_PHONG:      refl.force_attribute(L"brdf_type").set_value(L"phong");             break;
  case BRDF_TORRSPARR:  refl.force_attribute(L"brdf_type").set_value(L"torranse_sparrow");  break;
  case BRDF_BECKMANN:   refl.force_attribute(L"brdf_type").set_value(L"beckmann");          break;
  case BRDF_GGX:        refl.force_attribute(L"brdf_type").set_value(L"ggx");               break;
  case BRDF_TRGGX:      refl.force_attribute(L"brdf_type").set_value(L"trggx");             break;
  default:              refl.force_attribute(L"brdf_type").set_value(L"ggx");               break;
  }

  switch (extrusion)
  {
  case EXTRUSION_STRONG: refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"maxcolor");  break;
  case EXTRUSION_LUM:    refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"luminance"); break;
  default:               refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"maxcolor");  break;
  }

  auto colorNode = refl.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(resColor).c_str());

  if (hasTint)
    colorNode.force_attribute(L"tex_apply_mode").set_value(L"multiply");

  if (reflTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[reflectSlotName]);
    BindingTextures(reflTexture, colorNode, texRef, a_texturesSlotToRef, reflectSlotName, L"texture");
  }
  else if (colorNode.child(L"texture"))
    colorNode.remove_child(L"texture");

  auto glossNode = refl.force_child(L"glossiness");
  glossNode.force_attribute(L"val").set_value(glossiness);

  if (reflGlossTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[reflectGlossName]);
    BindingTextures(reflGlossTexture, glossNode, texRef, a_texturesSlotToRef, reflectGlossName, L"texture");
  }
  else if (glossNode.child(L"texture"))
    glossNode.remove_child(L"texture");

  refl.force_child(L"energy_fix").force_attribute(L"val") = (int)!this->m_rendParams.debugCheckBoxes[L"DISABLE_ENERGY_FIX"];

  refl.force_child(L"fresnel").force_attribute(L"val").set_value(int(fresnel));
  refl.force_child(L"fresnel_ior").force_attribute(L"val").set_value(ior);

  // Anisotropy

  auto anisNode = refl.force_child(L"anisotropy");

  anisNode.force_attribute(L"val").set_value(anisotr);
  anisNode.force_attribute(L"rot").set_value(MyClamp(anisRotat / 360.0F, 0.0F, 1.0F));
  anisNode.force_attribute(L"flip_axis").set_value(1);

  if (reflAnisTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[reflectAnisName]);
    BindingTextures(reflAnisTexture, anisNode, texRef, a_texturesSlotToRef, reflectAnisName, L"texture");
  }
  else if (anisNode.child(L"texture"))
    anisNode.remove_child(L"texture");


  if (reflAnisRotatTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[reflectAnisRotName]);
    BindingTextures(reflAnisRotatTexture, anisNode, texRef, a_texturesSlotToRef, reflectAnisRotName, L"texture_rot");
    anisNode.force_attribute(L"rot").set_value(1);
  }
  else if (anisNode.child(L"texture_rot"))
    anisNode.remove_child(L"texture_rot");
}


void HydraRenderPlugin::ExtractHydraTransparency(IHydraMtl* a_mtl, pugi::xml_node& a_mat,
  std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport)
{
  const std::wstring  transpSlotName      = L"TransparencyMap";
  const std::wstring  transpGlossSlotName = L"TranspGlossinessMap";

  const Color         transpColor         = a_mtl->GetTranspColor();
  Texmap* const       transpTexture       = a_mtl->GetTranspTexmap();
  const float         transpMult          = a_mtl->GetTranspMult();
  const bool          transpTint          = a_mtl->GetTranspTint();
  const float         transpGloss         = a_mtl->GetTranspGloss();
  Texmap* const       transpGlossTexture  = a_mtl->GetTranspGlossTexmap();
  const float         transpIor           = a_mtl->GetTranspIor();
  const float         transpDistMult      = a_mtl->GetTranspDistMult();
  const Color         transpDistColor     = a_mtl->GetTranspDistColor();
  const bool          transpThin          = a_mtl->GetTranspThin();

  const bool          hasTranspTexture    = (transpTexture != nullptr);
  const Color         resColor            = EvalComponentColorAPI(transpColor, transpMult, transpTint, hasTranspTexture);

  // XML export

  if (a_cleanExport && (Luminance(Point3(resColor)) < FLT_EPSILON))
    return;

  auto transp = a_mat.force_child(L"transparency");
  transp.force_attribute(L"brdf_type").set_value(L"phong");

  auto colorNode = transp.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(resColor).c_str());

  if (transpTint)
    colorNode.force_attribute(L"tex_apply_mode").set_value(L"multiply");

  if (hasTranspTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[transpSlotName]);
    BindingTextures(transpTexture, colorNode, texRef, a_texturesSlotToRef, transpSlotName, L"texture");
  }
  else if (colorNode.child(L"texture"))
    colorNode.remove_child(L"texture");

  auto transpGlossNode = transp.force_child(L"glossiness");
  transpGlossNode.force_attribute(L"val").set_value(transpGloss);

  if (transpGlossTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[transpGlossSlotName]);
    BindingTextures(transpGlossTexture, transpGlossNode, texRef, a_texturesSlotToRef, transpGlossSlotName, L"texture");
  }
  else if (transpGlossNode.child(L"texture"))
    transpGlossNode.remove_child(L"texture");

  transp.force_child(L"thin_walled").force_attribute(L"val").set_value((int)transpThin);
  transp.force_child(L"fog_color").force_attribute(L"val").set_value(strConverter::colorToWideString(transpDistColor).c_str());
  transp.force_child(L"fog_multiplier").force_attribute(L"val").set_value(transpDistMult);
  transp.force_child(L"ior").force_attribute(L"val").set_value(transpIor);
}


void HydraRenderPlugin::ExtractHydraOpacity(IHydraMtl* a_mtl, pugi::xml_node& a_mat, 
  std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport)
{
  const std::wstring  opacitySlotName = L"OpacityMap";

  Texmap* const       opacityTexture  = a_mtl->GetOpacityTexmap();
  const bool          affectShadows   = a_mtl->GetAffectShadow();
  const bool          opacitySmooth   = a_mtl->GetOpacitySmooth();

  // XML export
    

  auto opacityNode = a_mat.force_child(L"opacity");

  if (!affectShadows)
    opacityNode.force_child(L"skip_shadow").force_attribute(L"val").set_value(1);
  else
    opacityNode.remove_child(L"skip_shadow");

  if (a_cleanExport && !opacityTexture)
    return;

  if (opacityTexture)
  {
    opacityNode.force_attribute(L"smooth").set_value(int(opacitySmooth));
    auto texRef = std::get<0>(a_texturesSlotToRef[opacitySlotName]);
    BindingTextures(opacityTexture, opacityNode, texRef, a_texturesSlotToRef, opacitySlotName, L"texture");
  }
  else if (opacityNode && opacityNode.child(L"texture"))
    opacityNode.remove_child(L"texture");
}


void HydraRenderPlugin::ExtractHydraEmission(IHydraMtl* a_mtl, pugi::xml_node& a_mat,
  std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport)
{
  const std::wstring  emissionSlotName = L"EmissionMap";

  const Color         emissColor       = a_mtl->GetEmissionColor();
  Texmap* const       emissionTexture  = a_mtl->GetEmissionTexmap();
  const float         emissMult        = a_mtl->GetEmissionMult();
  const bool          emissTint        = a_mtl->GetEmissionTint();
  const bool          emissCastGi      = a_mtl->GetEmissionCastGi();

  const bool          hasEmissTexture  = (emissionTexture != nullptr);
  const Color         resColor         = EvalComponentColorAPI(emissColor, emissMult, emissTint, hasEmissTexture);

  // XML export

  if (a_cleanExport && (Luminance(Point3(resColor)) < FLT_EPSILON))
    return;

  auto emission  = a_mat.force_child(L"emission");
  auto colorNode = emission.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(resColor).c_str());

  if (hasEmissTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[emissionSlotName]);
    BindingTextures(emissionTexture, colorNode, texRef, a_texturesSlotToRef, emissionSlotName, L"texture");
  }
  else if (colorNode.child(L"texture") != nullptr)
    colorNode.remove_child(L"texture");

  emission.force_child(L"cast_gi").force_attribute(L"val").set_value(int(emissCastGi));

  if (emissTint)
    colorNode.force_attribute(L"tex_apply_mode").set_value(L"multiply");
}


void HydraRenderPlugin::ExtractHydraTranslucency(IHydraMtl* a_mtl, pugi::xml_node& a_mat,
  std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef, const bool a_cleanExport)
{
  const std::wstring  translucSlotName = L"TranslucencyMap";

  const float         translucMult     = a_mtl->GetTranslucMult();
  Texmap* const       translucTexture  = a_mtl->GetTranslucTexmap();
  const Color         translucColor    = a_mtl->GetTranslucColor();
  const bool          translucTint     = a_mtl->GetTranslucTint();

  const bool          hasTranslTexture = (translucTexture != nullptr);
  const Color         resColor         = EvalComponentColorAPI(translucColor, translucMult, translucTint, hasTranslTexture);

  // XML export

  if (a_cleanExport && (Luminance(Point3(resColor)) < FLT_EPSILON))
    return;

  auto translucencyNode = a_mat.force_child(L"translucency");
  translucencyNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(resColor).c_str());

  if (hasTranslTexture)
  {
    const auto texRef = std::get<0>(a_texturesSlotToRef[translucSlotName]);
    BindingTextures(translucTexture, translucencyNode, texRef, a_texturesSlotToRef, translucSlotName, L"texture");
  }
  else if (translucencyNode.child(L"texture"))
    translucencyNode.remove_child(L"texture");

  translucencyNode.force_child(L"multiplier").force_attribute(L"val").set_value(translucMult);
}


void HydraRenderPlugin::ExtractHydraRelief(IHydraMtl* a_mtl, pugi::xml_node& a_mat, std::unordered_map<std::wstring, texInfo>& a_texturesSlotToRef)
{
  const std::wstring  displSlotName = L"NormalMap";
  
  const float         reliefAmount  = a_mtl->GetReliefAmount();
  Texmap*             reliefTexture = a_mtl->GetReliefTexmap();  
  const float         reliefSmooth  = a_mtl->GetReliefSmooth();


  if (reliefTexture)
  {
    bool isNormalMap  = false;
    bool flipRed      = false;
    bool flipGreen    = false;
    bool swap         = false;

    auto texRef       = std::get<0>(a_texturesSlotToRef[displSlotName]);
    
    auto displaceNode = a_mat.force_child(L"displacement");
    pugi::xml_node heightNode;

    ExtractNormalBump(a_mtl, isNormalMap, flipRed, flipGreen, swap);

    if (isNormalMap)
    {
      heightNode      = displaceNode.force_child(L"normal_map");
      auto invert     = heightNode.force_child(L"invert");
      displaceNode.force_attribute(L"type").set_value(L"normal_bump");
      invert.force_attribute(L"x").set_value(int(flipRed));
      invert.force_attribute(L"y").set_value(int(!flipGreen));
      invert.force_attribute(L"swap_xy").set_value(int(swap));
     
      Texmap* const subTex = reliefTexture->GetSubTexmap(0);
      if (subTex && subTex->ClassID() == HydraBlendedBox_CLASS_ID) // if in normal map place HydraBlendedBox map.
        reliefTexture = subTex;
    }
    else
    {      
      heightNode = displaceNode.force_child(L"height_map");
      displaceNode.force_attribute(L"type").set_value(L"height_bump");
    }

    heightNode.force_attribute(L"amount").set_value(reliefAmount);
    heightNode.force_attribute(L"smooth_lvl").set_value(reliefSmooth);

    BindingTextures(reliefTexture, heightNode, texRef, a_texturesSlotToRef, displSlotName, L"texture");
  }
  else if (a_mat.child(L"displacement"))
    a_mat.remove_child(L"displacement");
}


void HydraRenderPlugin::ExtractHydraMtlCatcher(Mtl* a_mtl)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(a_mtl, mode));
  std::vector<std::wstring> hydraMapNames;

  /* if (mode == HR_OPEN_EXISTING)
    HydraMtlGetChangedTexturesSlotNames(changedParams, hydraMapNames); //only export textures that changed since last export
  else*/  
    HydraMtlGetAllTexturesSlotNames(hydraMapNames);  

  auto texturesSlotToRef = ExtractHydraTextures(a_mtl, hydraMapNames);

  hrMaterialOpen(mat, mode);
  
  auto matNode = hrMaterialParamNode(mat);
  matNode.attribute(L"type").set_value(L"shadow_catcher");

  auto back    = matNode.append_child(L"back");
  back.append_attribute(L"reflection") = 1;
        
  ExtractHydraMtlCatcherOpacity  (a_mtl, matNode, texturesSlotToRef);
  ExtractHydraMtlCatcherAO       (a_mtl, matNode, texturesSlotToRef);
  ExtractHydraMtlCatcherReflect  (a_mtl, matNode, texturesSlotToRef);
  
  hrMaterialClose(mat);
}


void HydraRenderPlugin::ExtractHydraMtlCatcherOpacity(Mtl* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  const std::wstring opacitySlotName  = L"OpacityMap";
  const bool shadows                  = FindBool(L"receiveShadowOn", mtl);
  const bool smooth                   = true;

  if (texturesSlotToRef.find(opacitySlotName) != texturesSlotToRef.end())
  {
    auto opacity = mat.force_child(L"opacity");

    opacity.force_child(L"skip_shadow").force_attribute(L"val").set_value(int(!shadows));
    opacity.force_attribute(L"smooth").set_value(int(smooth));

    auto texRef = std::get<0>(texturesSlotToRef[opacitySlotName]);
    auto texNode = hrTextureBind(texRef, opacity);

    Texmap* tex = GetTextureFromSlot(mtl, opacitySlotName);
    ExtractSampler(texNode, texturesSlotToRef[opacitySlotName], tex);
  }
  else if (mat.child(L"opacity") && mat.child(L"opacity").child(L"texture"))
    mat.child(L"opacity").remove_child(L"texture");
}

void HydraRenderPlugin::ExtractHydraMtlCatcherAO(Mtl * mtl, pugi::xml_node & mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{}

void HydraRenderPlugin::ExtractHydraMtlCatcherReflect(Mtl * mtl, pugi::xml_node & mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef)
{
  const Color color      = FindColor(L"ReflectivityColor", mtl);
  const float mult       = FindFloat(L"ReflectColorMultiplier", mtl);
  const float glossiness = FindFloat(L"ReflectGlossiness", mtl);
  const float ior        = FindFloat(L"ReflectivityIOR", mtl);
  const bool  fresnel    = FindBool(L"ReflectFresnelOn", mtl);

  auto refl              = mat.force_child(L"reflectivity");
  refl.force_attribute(L"brdf_type").set_value(L"phong");
  refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"luminance");

  const Color res_color = EvalComponentColorAPI(color, mult, 1, 0);

  refl.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());
  refl.child(L"color").force_attribute(L"tex_apply_mode").set_value(L"multiply");
  refl.force_child(L"glossiness").force_attribute(L"val").set_value(glossiness);
  refl.force_child(L"fresnel").force_attribute(L"val").set_value(int(fresnel));
  refl.force_child(L"fresnel_ior").force_attribute(L"val").set_value(ior);
}


void HydraRenderPlugin::ExtractHydraMtlLayers(Mtl * mtl)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));
  std::vector<std::wstring> hydraMapNames;

  HydraMtlGetAllTexturesSlotNames(hydraMapNames);


  hrMaterialOpen(mat, mode);
  {  }
  hrMaterialClose(mat);
}






HRMaterialRef HydraRenderPlugin::GetHRMatRefFromMtl(Mtl* mtl, HR_OPEN_MODE &mode)
{
  HRMaterialRef mat;
  auto findMatRef = g_hrPlugResMangr.m_materialsRefLib.find(mtl);

  if (findMatRef == g_hrPlugResMangr.m_materialsRefLib.end())
  {
    mat = hrMaterialCreate(strConverter::c2ws(mtl->GetName().data()).c_str());
    g_hrPlugResMangr.m_materialsRefLib[mtl] = mat;
    m_largestMaterialId = mat.id;
  }
  else
  {
    mat = findMatRef->second;
    mode = HR_OPEN_EXISTING;
  }

  return mat;
}


void HydraRenderPlugin::ExtractBlendMaterial(Mtl* mtl, std::vector<HRMaterialRef> &subMatRefs)
{
  const std::wstring slotName = L"Mask";
  /*
  MixAmount		: float   = 1
  Lower		: float   = 0
  Upper		: float   = 1
  UseCurve		: boolean = 1
  Interactive		: integer = 0
  Map 1 : unknown
  Map 2 : unknown
  Mask		: some texmap
  Map 1 Enable		: boolean = 1
  Map 2 Enable		: boolean = 1
  MaskEnable		: boolean = 1
  */

  Texmap* mask = FindTex(slotName, mtl);

  bool mask_on      = FindBool(L"MaskEnable", mtl);
  bool use_curve    = FindBool(L"UseCurve", mtl);
  float mask_mult   = FindFloat(L"MixAmount", mtl);
  bool texSupported = false;

  HR_OPEN_MODE mode = HR_WRITE_DISCARD;

  HRMaterialRef matBlend, mat1, mat2;
  auto findMatRef = g_hrPlugResMangr.m_materialsRefLib.find(mtl);

  if (FindBool(L"Map 1 Enable", mtl)) mat1 = subMatRefs.at(0);
  else                                mat1 = g_hrPlugResMangr.m_placeholderMatRef;

  if (FindBool(L"Map 2 Enable", mtl)) mat2 = subMatRefs.at(1);
  else                                mat2 = g_hrPlugResMangr.m_placeholderMatRef;

  if (findMatRef == g_hrPlugResMangr.m_materialsRefLib.end())
  {
    matBlend = hrMaterialCreateBlend(strConverter::c2ws(mtl->GetName().data()).c_str(), mat1, mat2);
    g_hrPlugResMangr.m_materialsRefLib[mtl] = matBlend;
    m_largestMaterialId = matBlend.id;
  }
  else
  {
    matBlend = findMatRef->second;
    mode = HR_OPEN_EXISTING;
  }

  const std::vector<std::wstring> blendMapNames ={ slotName };

  auto texturesSlotToRef = ExtractHydraTextures(mtl, blendMapNames);

  if (!texturesSlotToRef.empty())
  {
    auto texRef = std::get<0>(texturesSlotToRef.at(slotName));

    if (!texturesSlotToRef.empty() && texRef.id != -1)
      texSupported = true;
  }

  hrMaterialOpen(matBlend, mode);
  {
    auto matNode = hrMaterialParamNode(matBlend);

    matNode.force_attribute(L"node_top").set_value(mat1.id);
    matNode.force_attribute(L"node_bottom").set_value(mat2.id);

    auto blend = matNode.force_child(L"blend");
    if (use_curve)
      blend.force_attribute(L"type").set_value(L"sigmoid_blend");
    else
      blend.force_attribute(L"type").set_value(L"mask_blend");


    auto maskNode = blend.force_child(L"mask");

    if (texSupported && mask_on)
    {
      auto texRef = std::get<0>(texturesSlotToRef.at(slotName));
      Texmap* tex = mtl->GetSubTexmap(0);
      BindingTextures(tex, maskNode, texRef, texturesSlotToRef, slotName, L"texture");
      mask_mult = 1.0f;
    }
    else if (maskNode.child(L"texture") != nullptr)
      maskNode.remove_child(L"texture");

    maskNode.force_attribute(L"val").set_value(mask_mult);

    if (use_curve)
    {
      float upper = FindFloat(L"Upper", mtl);
      float lower = FindFloat(L"Lower", mtl);
      blend.force_child(L"exponent").force_attribute(L"val").set_value(0.75f / (upper - lower));
    }
  }
  hrMaterialClose(matBlend);
}

void HydraRenderPlugin::OverrideNameFromHydraMtlTags(Mtl* mtl, pugi::xml_node& mat)
{
  if (mtl->ClassID() != hydraMtlTags_CLASS_ID)
    return;


  // add "Target"
  const int target = FindInt(L"TexTarget", mtl);
  const std::wstring targetList[] ={ L"Undefined", L"PCscreen", L"PCkeyboard", L"MacKeyboard", L"SmartphoneScreen",
  L"TabletScreen", L"RulerStraight", L"RulerAngular", L"RulerProtractor", L"Notepad", L"BoxPaperclips", L"BoxStaples",
  L"PictureHorizontal", L"PictureVertical", L"ClockCircle", L"ClockSquare", L"ClockRecrangular", L"GlossyMetal", L"GlossyMetalHole",
  L"GlossyPlasticDark", L"GlossyPlasticHole", L"Diffuse", L"GlassClear", L"Chrome", L"CalendarOrPosterHoriz", L"Book", L"Whiteboard",
    L"StorageBox", L"CalendarOrPosterVert", L"ChairLeg", L"CasePC", L"BlackPlastic", L"ChairFabric", L"GlossyPlasticLight", L"Lampshade" };

  const std::wstring categoryList[] ={ L"Ceramic", L"Concrete", L"Emission", L"Fabric", L"Glass", L"Leather", L"Liquid", L"Marble", L"Metal", L"Organic",
    L"Paint", L"Paper", L"Plastic", L"Rubber", L"Wood" };

  std::wstring category;

  // add category materials
  const int sizeCategoryList = sizeof(categoryList) / sizeof(categoryList[0]);
  for (int i = 0; i < sizeCategoryList; i++)
  {
    //if (FindBool(L"MaterialsAny", mtl)) category += L"any_";
    if (FindBool(categoryList[i], mtl)) category += categoryList[i] + L"_";
  }


  // Export compress mesh for database. Rename materials from HydraMtlTags randomize parameters.
  if (m_compressMeshOnExport)
  {
    std::wstring targetName;// = mtl->GetName() + L"__";
    targetName += targetList[target] + L"__";
    std::wstring name = targetName + category;
    mat.attribute(L"name").set_value(name.c_str());

    //if (FindBool(L"RndOverrideColor", mtl))      
    //  metadata.force_attribute(L"overrideColor").set_value(FindColor(L"RndColor", mtl));
  }


  // Export material library for database. 
  if (m_rendParams.debugCheckBoxes[L"EXPORT_MAT_LIB"])
  {
    auto metadata = mat.force_child(L"metadata");
    metadata.force_attribute(L"category").set_value(category.c_str());
    metadata.force_attribute(L"target").set_value(targetList[target].c_str());

  }
}


void HydraRenderPlugin::ExtractHydraMaterial(Mtl* mtl)
{
  if (!mtl)
    return;

  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));

  hrMaterialOpen(mat, mode);  

  if (!m_compressMeshOnExport) // not export materials parameters for "mesh_export" mode, only his name.
  {
    auto matNode            = hrMaterialParamNode(mat);
    OverrideNameFromHydraMtlTags(mtl, matNode); // for material randomizer by material type and other tags.    

    std::vector<std::wstring> hydraMapNames;
    HydraMtlGetAllTexturesSlotNames(hydraMapNames);
    auto texturesSlotToRef  = ExtractHydraTextures(mtl, hydraMapNames);

    const bool cleanExport  = this->m_rendParams.debugCheckBoxes[L"BLACK_IMAGE"] || this->m_rendParams.debugCheckBoxes[L"EXPORT_MAT_LIB"] ||
      this->m_rendParams.debugCheckBoxes[L"EXPORT_IN_ORB"] || m_compressMeshOnExport;

    auto hydraMtl           = dynamic_cast<IHydraMtl*>(mtl);
    if (hydraMtl)
    {
      ExtractHydraDiffuse     (hydraMtl, matNode, texturesSlotToRef, cleanExport);
      ExtractHydraReflect     (hydraMtl, matNode, texturesSlotToRef, cleanExport);
      ExtractHydraTransparency(hydraMtl, matNode, texturesSlotToRef, cleanExport);
      ExtractHydraOpacity     (hydraMtl, matNode, texturesSlotToRef, cleanExport);
      ExtractHydraEmission    (hydraMtl, matNode, texturesSlotToRef, cleanExport);
      ExtractHydraTranslucency(hydraMtl, matNode, texturesSlotToRef, cleanExport);
      ExtractHydraRelief      (hydraMtl, matNode, texturesSlotToRef);
    }
  }
  
  hrMaterialClose(mat);
}

void HydraRenderPlugin::ExtractStdMaterial(Mtl* mtl)
{
  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  HRMaterialRef mat(GetHRMatRefFromMtl(mtl, mode));

  const std::vector<std::wstring> stdMapNames ={ L"Self-Illumination", L"Diffuse Color", L"Specular Color", L"Glossiness",
                                                  L"Opacity", L"Filter Color", L"Bump", L"Reflection", L"Refraction",
                                                  L"Displacement" };

  auto texturesSlotToRef = ExtractHydraTextures(mtl, stdMapNames);

  StdMat2* std = static_cast<StdMat2*>(mtl);
  TimeValue t = GetStaticFrame();

  hrMaterialOpen(mat, mode);
  {
    auto matNode = hrMaterialParamNode(mat);

    ExtractStdMatEmission(std, matNode, texturesSlotToRef, t);
    ExtractStdMatDiffuse(std, matNode, texturesSlotToRef, t);
    ExtractStdMatReflect(std, matNode, texturesSlotToRef, t);
    ExtractStdMatTransparency(std, matNode, texturesSlotToRef, t);
    ExtractStdMatOpacity(std, matNode, texturesSlotToRef, t);
    ExtractStdMatDisplacement(std, matNode, texturesSlotToRef, t);

  }
  hrMaterialClose(mat);
}

void HydraRenderPlugin::ExtractStdMatDiffuse(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring diffuseSlotName = L"Diffuse Color";
  bool hasColorTexture = false;

  Color color = mtl->GetDiffuse(t);

  auto diff = mat.force_child(L"diffuse");

  auto mult = mtl->GetTexmapAmt(ID_DI, t);

  if (texturesSlotToRef.find(diffuseSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = EvalComponentColorAPI(color, mult, false, hasColorTexture);

  diff.force_attribute(L"brdf_type").set_value(L"lambert");

  auto colorNode = diff.force_child(L"color");
  colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if (texturesSlotToRef.find(diffuseSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[diffuseSlotName]);
    auto texNode = hrTextureBind(texRef, colorNode);

    Texmap* tex = GetTextureFromSlot(mtl, diffuseSlotName);
    ExtractSampler(texNode, texturesSlotToRef[diffuseSlotName], tex);
  }
  else if (colorNode.child(L"texture") != nullptr)
  {
    colorNode.remove_child(L"texture");
  }

  diff.force_child(L"roughness").force_attribute(L"val").set_value(0.0f);
}

void HydraRenderPlugin::ExtractStdMatReflect(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring reflSlotName = L"Specular Color";
  std::wstring reflectGlossName = L"Glossiness";
  bool hasColorTexture = false;

  Color color = mtl->GetSpecular(t) * mtl->GetShinStr(t);

  auto mult = mtl->GetTexmapAmt(ID_SP, t);

  auto refl = mat.force_child(L"reflectivity");
  refl.force_attribute(L"brdf_type").set_value(L"phong");

  refl.force_child(L"extrusion").force_attribute(L"val").set_value(L"maxcolor");


  if (texturesSlotToRef.find(reflSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = EvalComponentColorAPI(color, mult, false, hasColorTexture);

  refl.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());


  if (texturesSlotToRef.find(reflSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[reflSlotName]);
    auto texNode = hrTextureBind(texRef, refl.child(L"color"));

    Texmap* tex = GetTextureFromSlot(mtl, reflSlotName);
    ExtractSampler(texNode, texturesSlotToRef[reflSlotName], tex);
  }
  else if (refl.child(L"color").child(L"texture") != nullptr)
  {
    refl.child(L"color").remove_child(L"texture");
  }

  refl.force_child(L"glossiness").force_attribute(L"val").set_value(mtl->GetShininess(t));

  if (texturesSlotToRef.find(reflectGlossName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[reflectGlossName]);
    auto texNode = hrTextureBind(texRef, refl.child(L"color"));
    Texmap* tex = GetTextureFromSlot(mtl, reflectGlossName);
    ExtractSampler(texNode, texturesSlotToRef[reflectGlossName], tex);
  }
  else if (refl.child(L"glossiness").child(L"texture") != nullptr)
  {
    refl.child(L"glossiness").remove_child(L"texture");
  }

  refl.force_child(L"fresnel").force_attribute(L"val").set_value(1);
  refl.force_child(L"fresnel_ior").force_attribute(L"val").set_value(mtl->GetIOR(t));
}

void HydraRenderPlugin::ExtractStdMatTransparency(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring transpSlotName = L"Refraction";
  std::wstring transpSlotName2 = L"Filter Color";
  std::wstring transpGlossSlotName = L"TranspGlossinessMap";
  bool hasColorTexture = false;

  auto mult = mtl->GetTexmapAmt(ID_FI, t); //Filter color, ID_RR for refraction... 

  if (texturesSlotToRef.find(transpSlotName) != texturesSlotToRef.end() || texturesSlotToRef.find(transpSlotName2) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  bool isTransparent = (mtl->GetXParency(t) > 0.001f || hasColorTexture);

  if (isTransparent)
  {
    Color color = mtl->GetFilter(t);

    auto transp = mat.force_child(L"transparency");

    Color res_color = EvalComponentColorAPI(color, mult, false, hasColorTexture);

    transp.force_attribute(L"brdf_type").set_value(L"phong");

    auto colorNode = transp.force_child(L"color");
    colorNode.force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());;

    if (texturesSlotToRef.find(transpSlotName) != texturesSlotToRef.end())
    {
      auto texRef = std::get<0>(texturesSlotToRef[transpSlotName]);
      auto texNode = hrTextureBind(texRef, transp.child(L"color"));

      Texmap* tex = GetTextureFromSlot(mtl, transpSlotName);
      ExtractSampler(texNode, texturesSlotToRef[transpSlotName], tex);
    }
    else if (texturesSlotToRef.find(transpSlotName2) != texturesSlotToRef.end())
    {
      auto texRef = std::get<0>(texturesSlotToRef[transpSlotName2]);
      auto texNode = hrTextureBind(texRef, transp.child(L"color"));

      Texmap* tex = GetTextureFromSlot(mtl, transpSlotName2);
      ExtractSampler(texNode, texturesSlotToRef[transpSlotName2], tex);
    }
    else if (transp.child(L"color").child(L"texture") != nullptr)
    {
      transp.child(L"color").remove_child(L"texture");
    }

    transp.force_child(L"glossiness").force_attribute(L"val").set_value(FindFloat(L"TranspGlossiness", mtl));


    if (texturesSlotToRef.find(transpGlossSlotName) != texturesSlotToRef.end())
    {
      auto texRef = std::get<0>(texturesSlotToRef[transpGlossSlotName]);
      auto texNode = hrTextureBind(texRef, transp.child(L"glossiness"));

      Texmap* tex = GetTextureFromSlot(mtl, transpGlossSlotName);
      ExtractSampler(texNode, texturesSlotToRef[transpGlossSlotName], tex);
    }
    else if (transp.child(L"glossiness").child(L"texture") != nullptr)
    {
      transp.child(L"glossiness").remove_child(L"texture");
    }

    transp.force_child(L"thin_walled").force_attribute(L"val").set_value(0);
    transp.force_child(L"fog_color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());
    transp.force_child(L"fog_multiplier").force_attribute(L"val").set_value(10.0f*mtl->GetOpacFalloff(t));
    transp.force_child(L"ior").force_attribute(L"val").set_value(mtl->GetIOR(t));
  }
}

void HydraRenderPlugin::ExtractStdMatOpacity(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring opacitySlotName = L"Opacity";

  if (texturesSlotToRef.find(opacitySlotName) != texturesSlotToRef.end())
  {
    auto opacity = mat.force_child(L"opacity");
    opacity.force_child(L"skip_shadow").force_attribute(L"val").set_value(0);

    auto texRef = std::get<0>(texturesSlotToRef[opacitySlotName]);
    auto texNode = hrTextureBind(texRef, opacity);

    Texmap* tex = GetTextureFromSlot(mtl, opacitySlotName);
    ExtractSampler(texNode, texturesSlotToRef[opacitySlotName], tex);
  }
  else if (mat.child(L"opacity") != nullptr && mat.child(L"opacity").child(L"texture") != nullptr)
  {
    mat.child(L"opacity").remove_child(L"texture");
  }
}

void HydraRenderPlugin::ExtractStdMatEmission(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  auto emission = mat.force_child(L"emission");

  Color color = mtl->GetSelfIllumColor(t);

  auto mult = mtl->GetTexmapAmt(ID_SI, t);

  std::wstring emissionSlotName = L"Self-Illumination";
  bool hasColorTexture = false;

  if (texturesSlotToRef.find(emissionSlotName) != texturesSlotToRef.end())
  {
    hasColorTexture = true;
  }

  Color res_color = EvalComponentColorAPI(color, mult, false, hasColorTexture);

  emission.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(res_color).c_str());

  if (texturesSlotToRef.find(emissionSlotName) != texturesSlotToRef.end())
  {
    auto texRef = std::get<0>(texturesSlotToRef[emissionSlotName]);
    auto texNode = hrTextureBind(texRef, emission.child(L"color"));

    Texmap* tex = GetTextureFromSlot(mtl, emissionSlotName);
    ExtractSampler(texNode, texturesSlotToRef[emissionSlotName], tex);
  }
  else if (emission.child(L"color").child(L"texture") != nullptr)
  {
    emission.child(L"color").remove_child(L"texture");
  }

  emission.force_child(L"cast_gi").force_attribute(L"val").set_value(1);
  emission.force_child(L"multiplier").force_attribute(L"val").set_value(1.0f);
}


void HydraRenderPlugin::ExtractStdMatDisplacement(StdMat2* mtl, pugi::xml_node &mat, std::unordered_map<std::wstring, texInfo> texturesSlotToRef, TimeValue t)
{
  std::wstring displSlotName1 = L"Bump";
  std::wstring displSlotName2 = L"Displacement";
  bool hasBumpTexture = false;
  bool hasDisplTexture = false;

  if (texturesSlotToRef.find(displSlotName1) != texturesSlotToRef.end())
  {
    hasBumpTexture = true;
  }
  if (texturesSlotToRef.find(displSlotName2) != texturesSlotToRef.end())
  {
    hasDisplTexture = true;
  }

  if (hasBumpTexture || hasDisplTexture)
  {
    auto displace = mat.force_child(L"displacement");

    auto heightNode = displace.force_child(L"height_map");
    heightNode.force_attribute(L"smooth_lvl").set_value(0.1f);

    if (hasBumpTexture)
    {
      auto texRef = std::get<0>(texturesSlotToRef[displSlotName1]);

      heightNode.force_attribute(L"amount").set_value(mtl->GetTexmapAmt(ID_BU, t));
      auto texNode = hrTextureBind(texRef, displace.child(L"height_map"));

      Texmap* tex = GetTextureFromSlot(mtl, displSlotName1);
      ExtractSampler(texNode, texturesSlotToRef[displSlotName1], tex);
    }
    else if (hasDisplTexture)
    {
      auto texRef = std::get<0>(texturesSlotToRef[displSlotName2]);

      heightNode.force_attribute(L"amount").set_value(mtl->GetTexmapAmt(ID_DP, t));
      auto texNode = hrTextureBind(texRef, displace.child(L"height_map"));

      Texmap* tex = GetTextureFromSlot(mtl, displSlotName2);
      ExtractSampler(texNode, texturesSlotToRef[displSlotName2], tex);
    }
  }
  else if (mat.child(L"displacement") != nullptr && mat.child(L"displacement").child(L"height_map") != nullptr)
  {
    mat.child(L"displacement").remove_child(L"texture");
  }
}


int HydraRenderPlugin::ExtractMaterial(Mtl* a_mtl)
{
  if (!a_mtl || a_mtl->NumSubMtls() != 0) return -1;
  

  TSTR className;
  a_mtl->GetClassName(className);

  if (a_mtl->ClassID() == HydraMtl_CLASS_ID || a_mtl->ClassID() == hydraMtlTags_CLASS_ID)
  {
    ExtractHydraMaterial(a_mtl);
    return 1;
  }
  else if (a_mtl->ClassID() == HydraMtlLight_CLASS_ID)
    return 1;
  else if (a_mtl->ClassID() == hydraMtlCatcher_CLASS_ID)
  {
    ExtractHydraMtlCatcher(a_mtl);
    return 1;
  }
  else if (a_mtl->ClassID() == hydraMtlLayers_CLASS_ID)
  {
    ExtractHydraMtlLayers(a_mtl);
    return 1;
  }
  else if (a_mtl->ClassID().PartA() == DMTL_CLASS_ID)
  {
    ExtractStdMaterial(a_mtl);
    return 1;
  }

  //TraceNodeCustomProperties(mat->name, mtl, 1);

  const std::wstring message = L"Unsupported material: " + std::wstring(a_mtl->GetName().ToMCHAR());
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Warning, message);

  return 0;
}






