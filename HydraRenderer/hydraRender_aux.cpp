#include <ctime>
#include <mutex>
#include <future>
#include <chrono> 
#include <time.h>
#include <cwctype>
#include <clocale>

#include "HydraRenderer.h"
#include "3dsmaxport.h"
#include "gamma.h"
#include "IFileResolutionManager.h"

//////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;
extern HINSTANCE hInstance;


namespace strConverter
{
  std::wstring s2ws(const std::string& s)
  {
    size_t len;
    int slength = (int)s.length() + 1;
    len         = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, nullptr, 0);
    auto buf    = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, (int)len);
    std::wstring r(buf);
    delete[] buf;
    return r;
  }

  std::wstring s2ws(const std::wstring& s)
  {
    return s;
  }

  std::wstring c2ws(const char* s)
  {
    std::string str(s);
    return strConverter::s2ws(str);
  }

  std::wstring c2ws(const wchar_t* s)
  {
    std::wstring str(s);
    return str;
  }


  std::string ws2s(const std::wstring& s)
  {
    size_t len;
    int slength = (int)s.length() + 1;
    len         = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, nullptr, 0, nullptr, nullptr);
    auto buf    = new char[len];
    WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, buf, (int)len, nullptr, nullptr);
    std::string r(buf);
    delete[] buf;
    return r;
  }

  std::string ws2s(const std::string& s)
  {
    return s;
  }

  std::wstring ToWideString(const std::string& rhs)
  {
    return strConverter::s2ws(rhs);
    //std::wstring res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::wstring ToWideString(const std::wstring& rhs)
  {
    return rhs;
    //std::wstring res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::string ToNarrowString(const std::wstring& rhs)
  {
    return strConverter::ws2s(rhs);
    //std::string res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::string ToNarrowString(const std::string& rhs)
  {
    return rhs;
    //std::string res; res.resize(rhs.size());
    //std::copy(rhs.begin(), rhs.end(), res.begin());
    //return res;
  }

  std::wstring colorToWideString(const Color& color)
  {
    std::wstringstream tmpSS;

    tmpSS << color.r << " " << color.g << " " << color.b;

    return tmpSS.str();
  }

  std::wstring Point3ToWideString(const Point3& point)
  {
    std::wstringstream tmpSS;

    tmpSS << point.x << " " << point.y << " " << point.z;

    return tmpSS.str();
  }

  std::wstring Point4ToWideString(const Point4& point)
  {
    std::wstringstream tmpSS;

    tmpSS << point.x << " " << point.y << " " << point.z << " " << point.w;

    return tmpSS.str();
  }

  //char IntegerToChar(const int i)
  //{
  //  char str[20];
  //  sprintf_s(str, 20, "%d", i);
  //  return str;
  //}

  std::wstring Utf8ToWstring(const std::string& str)
  {
    //std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
    //return myconv.from_bytes(str);

    if (str.empty()) return std::wstring();
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo(size_needed, 0);
    MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
  }

  LPCWSTR WstrToLPCWSTR(const std::wstring& str)
  {
    const std::wstring stemp = std::wstring(str.begin(), str.end());    
    return stemp.c_str();
  }


  std::string WstringToUtf8(const std::wstring& wstr)
  {
    //std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
    //return escape(myconv.to_bytes(wstr));

    if (wstr.empty()) return std::string();
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
    std::string strTo(size_needed, 0);
    WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
    return strTo;
  }


}



float ReadFloatParam(const char* message, const char* paramName)
{
  std::istringstream iss(message);

  float res = 0.0F;

  do
  {
    std::string name, val;
    iss >> name >> val;

    if (name == paramName)
    {
      res = atof(val.c_str());
      break;
    }

  } while (iss);

  return res;
}

float Luminance(const Point3& data) { return DotProd(data, Point3(0.2126F, 0.7152F, 0.0722F)); }



RefResult HydraRenderPlugin::NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message, BOOL propagate)
{
  return REF_DONTCARE;
}


RefTargetHandle HydraRenderPlugin::Clone(RemapDir &remap)
{
  HydraRenderPlugin *mnew = new HydraRenderPlugin();

  //main
  mnew->m_rendParams.infQuality           = m_rendParams.infQuality;
  mnew->m_rendParams.renderQuality        = m_rendParams.renderQuality;
  mnew->m_rendParams.primary              = m_rendParams.primary;
  mnew->m_rendParams.secondary            = m_rendParams.secondary;
  mnew->m_rendParams.tertiary             = m_rendParams.tertiary;
  mnew->m_rendParams.enableCaustics       = m_rendParams.enableCaustics;
  mnew->m_rendParams.enableTextureResize  = m_rendParams.enableTextureResize;
  mnew->m_rendParams.offlinePT            = m_rendParams.offlinePT;
  mnew->m_rendParams.timeLimitOn          = m_rendParams.timeLimitOn;
  mnew->m_rendParams.timeLimit            = m_rendParams.timeLimit;
  mnew->m_rendParams.useHydraGUI          = m_rendParams.useHydraGUI;
  mnew->m_rendParams.enableDOF            = m_rendParams.enableDOF;
  mnew->m_rendParams.noRandomLightSel     = m_rendParams.noRandomLightSel;
  mnew->m_rendParams.enableLog            = m_rendParams.enableLog;
  mnew->m_rendParams.focalplane           = m_rendParams.focalplane;
  mnew->m_rendParams.lensradius           = m_rendParams.lensradius;
  mnew->m_rendParams.debugOn              = m_rendParams.debugOn;
  mnew->m_rendParams.allSecondary         = m_rendParams.allSecondary;
  mnew->m_rendParams.allTertiary          = m_rendParams.allTertiary;
  mnew->m_rendParams.primaryMLFilter      = m_rendParams.primaryMLFilter;
  mnew->m_rendParams.secondaryMLFilter    = m_rendParams.secondaryMLFilter;
  mnew->m_rendParams.writeToDisk          = m_rendParams.writeToDisk;
  mnew->m_rendParams.engine_type          = m_rendParams.engine_type;
  //mnew->m_rendParams.device_id          = m_rendParams.device_id;
  mnew->m_rendParams.liteMode             = m_rendParams.liteMode;
  mnew->m_rendParams.useSeparateSwap      = m_rendParams.useSeparateSwap;
  mnew->m_rendParams.specNoiseFilterOn    = m_rendParams.specNoiseFilterOn;
  mnew->m_rendParams.specNoiseFilter      = m_rendParams.specNoiseFilter;
  mnew->m_rendParams.preset               = m_rendParams.preset;
  mnew->m_rendParams.rememberDevice       = m_rendParams.rememberDevice;

  //path tracing
  mnew->m_rendParams.seed                 = m_rendParams.seed;
  mnew->m_rendParams.causticRays          = m_rendParams.causticRays;
  mnew->m_rendParams.minrays              = m_rendParams.minrays;
  mnew->m_rendParams.maxrays              = m_rendParams.maxrays;
  mnew->m_rendParams.raybounce            = m_rendParams.raybounce;
  mnew->m_rendParams.diffbounce           = m_rendParams.diffbounce;
  mnew->m_rendParams.relative_error       = m_rendParams.relative_error;
  mnew->m_rendParams.guided               = m_rendParams.guided;
  mnew->m_rendParams.bsdf_clamp_on        = m_rendParams.bsdf_clamp_on;
  mnew->m_rendParams.bsdf_clamp           = m_rendParams.bsdf_clamp;
  mnew->m_rendParams.env_clamp            = m_rendParams.env_clamp;
  //mnew->m_rendParams.estimateLDR        = m_rendParams.estimateLDR;
  mnew->m_rendParams.denoiseLvl           = m_rendParams.denoiseLvl;
  mnew->m_rendParams.qmcLights            = m_rendParams.qmcLights;
  mnew->m_rendParams.qmcMaterials         = m_rendParams.qmcMaterials;



  //SPPM Caustics
  mnew->m_rendParams.maxphotons_c         = m_rendParams.maxphotons_c;
  mnew->m_rendParams.caustic_power        = m_rendParams.caustic_power;
  mnew->m_rendParams.retrace_c            = m_rendParams.retrace_c;
  mnew->m_rendParams.initial_radius_c     = m_rendParams.initial_radius_c;
  mnew->m_rendParams.visibility_c         = m_rendParams.visibility_c;
  mnew->m_rendParams.alpha_c              = m_rendParams.alpha_c;

  //SPPM Diffuse
  mnew->m_rendParams.maxphotons_d         = m_rendParams.maxphotons_d;
  mnew->m_rendParams.retrace_d            = m_rendParams.retrace_d;
  mnew->m_rendParams.initial_radius_d     = m_rendParams.initial_radius_d;
  mnew->m_rendParams.visibility_d         = m_rendParams.visibility_d;
  mnew->m_rendParams.alpha_d              = m_rendParams.alpha_d;
  mnew->m_rendParams.irr_map              = m_rendParams.irr_map;

  //Irradiance cache
  mnew->m_rendParams.ic_eval              = m_rendParams.ic_eval;
  mnew->m_rendParams.maxpass              = m_rendParams.maxpass;
  mnew->m_rendParams.fixed_rays           = m_rendParams.fixed_rays;
  mnew->m_rendParams.ws_err               = m_rendParams.ws_err;
  mnew->m_rendParams.ss_err4              = m_rendParams.ss_err4;
  mnew->m_rendParams.ss_err2              = m_rendParams.ss_err2;
  mnew->m_rendParams.ss_err1              = m_rendParams.ss_err1;
  mnew->m_rendParams.ic_relative_error    = m_rendParams.ic_relative_error;

  //Multi-Layer
  mnew->m_rendParams.filterPrimary        = m_rendParams.filterPrimary;
  mnew->m_rendParams.r_sigma              = m_rendParams.r_sigma;
  mnew->m_rendParams.s_sigma              = m_rendParams.s_sigma;
  mnew->m_rendParams.layerNum             = m_rendParams.layerNum;
  mnew->m_rendParams.spp                  = m_rendParams.spp;
  //mnew->m_rendParams.saveComposeLayers  = m_rendParams.saveComposeLayers;

  //Denoise
  mnew->m_rendParams.denoiserOn           = m_rendParams.denoiserOn;
  mnew->m_rendParams.denoiserAlbedoPassOn = m_rendParams.denoiserAlbedoPassOn;
  mnew->m_rendParams.denoiserNormalPassOn = m_rendParams.denoiserNormalPassOn;

  //Post process
  mnew->m_rendParams.postProcOn           = m_rendParams.postProcOn;
  mnew->m_rendParams.numThreads           = m_rendParams.numThreads;
  mnew->m_rendParams.exposure             = m_rendParams.exposure;
  mnew->m_rendParams.compress             = m_rendParams.compress;
  mnew->m_rendParams.contrast             = m_rendParams.contrast;
  mnew->m_rendParams.saturation           = m_rendParams.saturation;
  mnew->m_rendParams.vibrance             = m_rendParams.vibrance;
  mnew->m_rendParams.whiteBalance         = m_rendParams.whiteBalance;
  mnew->m_rendParams.whitePointColor      = m_rendParams.whitePointColor;
  mnew->m_rendParams.uniformContrast      = m_rendParams.uniformContrast;
  mnew->m_rendParams.normalize            = m_rendParams.normalize;
  mnew->m_rendParams.chromAberr           = m_rendParams.chromAberr;
  mnew->m_rendParams.vignette             = m_rendParams.vignette;
  mnew->m_rendParams.sharpness            = m_rendParams.sharpness;
  mnew->m_rendParams.sizeStar             = m_rendParams.sizeStar;
  mnew->m_rendParams.numRay               = m_rendParams.numRay;
  mnew->m_rendParams.rotateRay            = m_rendParams.rotateRay;
  mnew->m_rendParams.randomAngle          = m_rendParams.randomAngle;
  mnew->m_rendParams.sprayRay             = m_rendParams.sprayRay;


  mnew->m_rendParams.bloom                = m_rendParams.bloom;
  mnew->m_rendParams.icBounce             = m_rendParams.icBounce;
  //mnew->m_rendParams.hydraFrameBuf      = m_rendParams.hydraFrameBuf;
  mnew->m_rendParams.filter_id            = m_rendParams.filter_id;
  mnew->m_rendParams.mlaa                 = m_rendParams.mlaa;
  mnew->m_rendParams.bloom_radius         = m_rendParams.bloom_radius;
  mnew->m_rendParams.bloom_str            = m_rendParams.bloom_str;

  //Override
  mnew->m_rendParams.env_mult             = m_rendParams.env_mult;
  mnew->m_rendParams.overrideGrayMatOn    = m_rendParams.overrideGrayMatOn;

  //MLT
  mnew->m_rendParams.mlt_plarge                   = m_rendParams.mlt_plarge;
  mnew->m_rendParams.mlt_iters_mult               = m_rendParams.mlt_iters_mult;
  mnew->m_rendParams.mlt_burn_iters               = m_rendParams.mlt_burn_iters;
  mnew->m_rendParams.mlt_enable_median_filter     = m_rendParams.mlt_enable_median_filter;
  mnew->m_rendParams.mlt_median_filter_threshold  = m_rendParams.mlt_median_filter_threshold;

  //MMLT
  mnew->m_rendParams.mmlt_estim_time              = m_rendParams.mmlt_estim_time;
  mnew->m_rendParams.mmlt_enable_median_filter    = m_rendParams.mmlt_enable_median_filter;
  mnew->m_rendParams.mmlt_median_filter_threshold = m_rendParams.mmlt_median_filter_threshold;

  //Debug
  mnew->m_rendParams.enableLog            = m_rendParams.enableLog;
  mnew->m_rendParams.useSeparateSwap      = m_rendParams.useSeparateSwap;
  mnew->m_rendParams.saveRendImagEveryOn  = m_rendParams.saveRendImagEveryOn;
  mnew->m_rendParams.saveRendImagEveryMin = m_rendParams.saveRendImagEveryMin;

  // Render elements
  mnew->m_rendParams.renElemAlphaOn       = m_rendParams.renElemAlphaOn;
  mnew->m_rendParams.renElemColorOn       = m_rendParams.renElemColorOn;
  mnew->m_rendParams.renElemCoordOn       = m_rendParams.renElemCoordOn;
  mnew->m_rendParams.renElemCoverOn       = m_rendParams.renElemCoverOn;
  mnew->m_rendParams.renElemDepthOn       = m_rendParams.renElemDepthOn;
  mnew->m_rendParams.renElemInstIdOn      = m_rendParams.renElemInstIdOn;
  mnew->m_rendParams.renElemMatIdOn       = m_rendParams.renElemMatIdOn;
  mnew->m_rendParams.renElemNormalsOn     = m_rendParams.renElemNormalsOn;
  mnew->m_rendParams.renElemObjIdOn       = m_rendParams.renElemObjIdOn;
  mnew->m_rendParams.renElemShadowOn      = m_rendParams.renElemShadowOn;


  BaseClone(this, mnew, remap);
  return (RefTargetHandle)mnew;
}

void HydraRenderPlugin::ResetParams()
{
  DebugPrint(_T("**** Resetting scene\n"));

  //This function is called when user does File -> New All (at least in 2018 max)
  //So we need to reset everything and create new Scene library and new scene
  //
  g_hrPlugResMangr.m_hydraInit = false;
  HydraInit(g_hrPlugResMangr.m_hydraInit);
}


//Chunk ID's for loading and saving render data

#define MAIN_CHUNK		    0x120
#define PATHTRACING_CHUNK	0x130
#define IRRCACHE_CHUNK		0x140
#define MULTILAYER_CHUNK	0x150
#define SPPM_C_CHUNK	  	0x160
#define SPPM_D_CHUNK		0x170
#define POSTPROC_CHUNK		0x180
#define OVERRIDE_CHUNK		0x190
#define MLT_CHUNK		      0x200
#define MMLT_CHUNK		    0x210
#define DENOISE_CHUNK	  	0x220
#define REND_ELEM_CHUNK		0x230

IOResult HydraRenderPlugin::Save(ISave *isave)
{
  ULONG nb;

  isave->BeginChunk(MAIN_CHUNK);
  isave->Write(&m_rendParams.infQuality, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renderQuality, sizeof(float), &nb);
  isave->Write(&m_rendParams.primary, sizeof(int), &nb);
  isave->Write(&m_rendParams.secondary, sizeof(int), &nb);
  isave->Write(&m_rendParams.tertiary, sizeof(int), &nb);
  isave->Write(&m_rendParams.enableCaustics, sizeof(bool), &nb);
  isave->Write(&m_rendParams.enableTextureResize, sizeof(bool), &nb);
  isave->Write(&m_rendParams.offlinePT, sizeof(bool), &nb);
  isave->Write(&m_rendParams.allSecondary, sizeof(bool), &nb);
  isave->Write(&m_rendParams.allTertiary, sizeof(bool), &nb);
  isave->Write(&m_rendParams.timeLimitOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.useHydraGUI, sizeof(bool), &nb);
  isave->Write(&m_rendParams.noRandomLightSel, sizeof(bool), &nb);
  isave->Write(&m_rendParams.debugOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.enableLog, sizeof(bool), &nb);
  isave->Write(&m_rendParams.enableDOF, sizeof(bool), &nb);
  isave->Write(&m_rendParams.primaryMLFilter, sizeof(bool), &nb);
  isave->Write(&m_rendParams.secondaryMLFilter, sizeof(bool), &nb);
  isave->Write(&m_rendParams.writeToDisk, sizeof(bool), &nb);
  isave->Write(&m_rendParams.timeLimit, sizeof(long), &nb);
  isave->Write(&m_rendParams.focalplane, sizeof(float), &nb);
  isave->Write(&m_rendParams.lensradius, sizeof(float), &nb);
  isave->Write(&m_rendParams.engine_type, sizeof(int), &nb);
  /*isave->Write(&m_rendParams.devices_num, sizeof(int), &nb);
  isave->Write(m_rendParams.device_id.data(), m_rendParams.devices_num*sizeof(int), &nb);*/
  isave->Write(&m_rendParams.liteMode, sizeof(bool), &nb);
  isave->Write(&m_rendParams.useSeparateSwap, sizeof(bool), &nb);
  isave->Write(&m_rendParams.specNoiseFilterOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.specNoiseFilter, sizeof(float), &nb);
  isave->Write(&m_rendParams.preset, sizeof(int), &nb);
  //isave->Write(&m_rendParams.rememberDevice, sizeof(bool), &nb);

  isave->EndChunk();

  isave->BeginChunk(PATHTRACING_CHUNK);
  isave->Write(&m_rendParams.icBounce, sizeof(int), &nb);
  isave->Write(&m_rendParams.seed, sizeof(int), &nb);
  isave->Write(&m_rendParams.minrays, sizeof(int), &nb);
  isave->Write(&m_rendParams.maxrays, sizeof(int), &nb);
  isave->Write(&m_rendParams.raybounce, sizeof(int), &nb);
  isave->Write(&m_rendParams.diffbounce, sizeof(int), &nb);
  isave->Write(&m_rendParams.useRR, sizeof(int), &nb);
  isave->Write(&m_rendParams.causticRays, sizeof(int), &nb);
  isave->Write(&m_rendParams.relative_error, sizeof(float), &nb);
  isave->Write(&m_rendParams.guided, sizeof(bool), &nb);
  isave->Write(&m_rendParams.bsdf_clamp_on, sizeof(bool), &nb);
  isave->Write(&m_rendParams.bsdf_clamp, sizeof(float), &nb);
  isave->Write(&m_rendParams.env_clamp, sizeof(float), &nb);
  //isave->Write(&m_rendParams.estimateLDR, sizeof(bool), &nb);
  isave->Write(&m_rendParams.qmcLights, sizeof(bool), &nb);
  isave->Write(&m_rendParams.qmcMaterials, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(IRRCACHE_CHUNK);
  isave->Write(&m_rendParams.ic_eval, sizeof(int), &nb);
  isave->Write(&m_rendParams.maxpass, sizeof(int), &nb);
  isave->Write(&m_rendParams.fixed_rays, sizeof(int), &nb);
  isave->Write(&m_rendParams.ws_err, sizeof(float), &nb);
  isave->Write(&m_rendParams.ss_err4, sizeof(float), &nb);
  isave->Write(&m_rendParams.ss_err2, sizeof(float), &nb);
  isave->Write(&m_rendParams.ss_err1, sizeof(float), &nb);
  isave->Write(&m_rendParams.ic_relative_error, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(MULTILAYER_CHUNK);
  isave->Write(&m_rendParams.layerNum, sizeof(int), &nb);
  isave->Write(&m_rendParams.r_sigma, sizeof(float), &nb);
  isave->Write(&m_rendParams.s_sigma, sizeof(float), &nb);
  isave->Write(&m_rendParams.spp, sizeof(int), &nb);
  isave->Write(&m_rendParams.filterPrimary, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(SPPM_C_CHUNK);
  isave->Write(&m_rendParams.maxphotons_c, sizeof(int), &nb);
  isave->Write(&m_rendParams.retrace_c, sizeof(int), &nb);
  isave->Write(&m_rendParams.initial_radius_c, sizeof(float), &nb);
  isave->Write(&m_rendParams.caustic_power, sizeof(float), &nb);
  isave->Write(&m_rendParams.visibility_c, sizeof(bool), &nb);
  isave->Write(&m_rendParams.alpha_c, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(SPPM_D_CHUNK);
  isave->Write(&m_rendParams.maxphotons_d, sizeof(int), &nb);
  isave->Write(&m_rendParams.retrace_d, sizeof(int), &nb);
  isave->Write(&m_rendParams.initial_radius_d, sizeof(float), &nb);
  isave->Write(&m_rendParams.visibility_d, sizeof(bool), &nb);
  isave->Write(&m_rendParams.alpha_d, sizeof(float), &nb);
  isave->Write(&m_rendParams.irr_map, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(POSTPROC_CHUNK);
  isave->Write(&m_rendParams.postProcOn, sizeof(float), &nb);
  isave->Write(&m_rendParams.exposure, sizeof(float), &nb);
  isave->Write(&m_rendParams.compress, sizeof(float), &nb);
  isave->Write(&m_rendParams.contrast, sizeof(float), &nb);
  isave->Write(&m_rendParams.saturation, sizeof(float), &nb);
  isave->Write(&m_rendParams.vibrance, sizeof(float), &nb);
  isave->Write(&m_rendParams.whiteBalance, sizeof(float), &nb);
  isave->Write(&m_rendParams.whitePointColor, sizeof(Color), &nb);
  isave->Write(&m_rendParams.uniformContrast, sizeof(float), &nb);
  isave->Write(&m_rendParams.normalize, sizeof(float), &nb);
  isave->Write(&m_rendParams.chromAberr, sizeof(float), &nb);
  isave->Write(&m_rendParams.vignette, sizeof(float), &nb);
  isave->Write(&m_rendParams.sharpness, sizeof(float), &nb);
  isave->Write(&m_rendParams.sizeStar, sizeof(float), &nb);
  isave->Write(&m_rendParams.numRay, sizeof(int), &nb);
  isave->Write(&m_rendParams.rotateRay, sizeof(int), &nb);
  isave->Write(&m_rendParams.randomAngle, sizeof(float), &nb);
  isave->Write(&m_rendParams.sprayRay, sizeof(float), &nb);

  isave->Write(&m_rendParams.filter_id, sizeof(int), &nb);
  isave->Write(&m_rendParams.bloom, sizeof(bool), &nb);
  //isave->Write(&m_rendParams.hydraFrameBuf, sizeof(bool), &nb);
  isave->Write(&m_rendParams.mlaa, sizeof(bool), &nb);
  isave->Write(&m_rendParams.bloom_radius, sizeof(float), &nb);
  isave->Write(&m_rendParams.bloom_str, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(OVERRIDE_CHUNK);
  isave->Write(&m_rendParams.env_mult, sizeof(float), &nb);
  isave->Write(&m_rendParams.overrideGrayMatOn, sizeof(bool), &nb);
  isave->EndChunk();

  isave->BeginChunk(MLT_CHUNK);
  isave->Write(&m_rendParams.mlt_plarge, sizeof(float), &nb);
  isave->Write(&m_rendParams.mlt_iters_mult, sizeof(int), &nb);
  isave->Write(&m_rendParams.mlt_burn_iters, sizeof(int), &nb);
  isave->Write(&m_rendParams.mlt_enable_median_filter, sizeof(bool), &nb);
  isave->Write(&m_rendParams.mlt_median_filter_threshold, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(MMLT_CHUNK);
  isave->Write(&m_rendParams.mmlt_estim_time, sizeof(int), &nb);
  isave->Write(&m_rendParams.mmlt_enable_median_filter, sizeof(bool), &nb);
  isave->Write(&m_rendParams.mmlt_median_filter_threshold, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(DENOISE_CHUNK);
  isave->Write(&m_rendParams.denoiserOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.denoiserAlbedoPassOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.denoiserNormalPassOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.denoiseLvl, sizeof(float), &nb);
  isave->EndChunk();

  isave->BeginChunk(REND_ELEM_CHUNK);
  isave->Write(&m_rendParams.renElemAlphaOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemColorOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemCoordOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemCoverOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemDepthOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemInstIdOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemMatIdOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemNormalsOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemObjIdOn, sizeof(bool), &nb);
  isave->Write(&m_rendParams.renElemShadowOn, sizeof(bool), &nb);

  isave->EndChunk();


  return IO_OK;
}



void HydraRenderPlugin::BeginThings()
{
  INode* root = GetCOREInterface()->GetRootNode();
  for (int i = 0; i < root->NumberOfChildren(); i++)
  {
    BeginEnum beginEnum(m_rendParams.time);
    beginEnum.BeginEnumeration();
    ReferenceMaker* rm = root->GetChildNode(i);
    if (rm) rm->EnumRefHierarchy(beginEnum);
  }
}

void HydraRenderPlugin::EndThings()
{
  INode* root = GetCOREInterface()->GetRootNode();
  for (int i = 0; i < root->NumberOfChildren(); i++)
  {
    EndEnum endEnum(m_rendParams.time);
    endEnum.BeginEnumeration();
    ReferenceMaker* rm = root->GetChildNode(i);
    if (rm) rm->EnumRefHierarchy(endEnum);
  }
}

IOResult HydraRenderPlugin::Load(ILoad *iload)
{
  ULONG nb;
  int id = 0;
  IOResult res;
  while (IO_OK == (res = iload->OpenChunk()))
  {
    switch (id = iload->CurChunkID())
    {
      case MAIN_CHUNK:
        res = iload->Read(&m_rendParams.infQuality, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renderQuality, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.primary, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.secondary, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.tertiary, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.enableCaustics, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.enableTextureResize, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.offlinePT, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.allSecondary, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.allTertiary, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.timeLimitOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.useHydraGUI, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.noRandomLightSel, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.debugOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.enableLog, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.enableDOF, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.primaryMLFilter, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.secondaryMLFilter, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.writeToDisk, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.timeLimit, sizeof(long), &nb);
        res = iload->Read(&m_rendParams.focalplane, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.lensradius, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.engine_type, sizeof(int), &nb);
        /*res = iload->Read(&m_rendParams.devices_num, sizeof(int), &nb);

        int *temp_dev;
        temp_dev = new int[m_rendParams.devices_num];
        res = iload->Read(temp_dev, m_rendParams.devices_num*sizeof(int), &nb);
        m_rendParams.device_id.resize(m_rendParams.device_id.size() + m_rendParams.devices_num);
        memcpy(&m_rendParams.device_id[m_rendParams.device_id.size() - m_rendParams.devices_num], &temp_dev[0], m_rendParams.devices_num * sizeof(int));
        delete temp_dev;
        */
        res = iload->Read(&m_rendParams.liteMode, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.useSeparateSwap, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.specNoiseFilterOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.specNoiseFilter, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.preset, sizeof(int), &nb);

        //res = iload->Read(&m_rendParams.rememberDevice, sizeof(bool), &nb);
        break;

      case PATHTRACING_CHUNK:
        res = iload->Read(&m_rendParams.icBounce, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.seed, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.minrays, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.maxrays, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.raybounce, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.diffbounce, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.useRR, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.causticRays, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.relative_error, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.guided, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.bsdf_clamp_on, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.bsdf_clamp, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.env_clamp, sizeof(float), &nb);
        //res = iload->Read(&m_rendParams.estimateLDR, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.qmcLights, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.qmcMaterials, sizeof(float), &nb);
        break;

      case IRRCACHE_CHUNK:
        res = iload->Read(&m_rendParams.ic_eval, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.maxpass, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.fixed_rays, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.ws_err, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.ss_err4, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.ss_err2, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.ss_err1, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.ic_relative_error, sizeof(float), &nb);
        break;
      case MULTILAYER_CHUNK:
        res = iload->Read(&m_rendParams.layerNum, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.r_sigma, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.s_sigma, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.spp, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.filterPrimary, sizeof(bool), &nb);
        break;
      case SPPM_C_CHUNK:
        res = iload->Read(&m_rendParams.maxphotons_c, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.retrace_c, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.initial_radius_c, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.caustic_power, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.visibility_c, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.alpha_c, sizeof(float), &nb);
        break;
      case SPPM_D_CHUNK:
        res = iload->Read(&m_rendParams.maxphotons_d, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.retrace_d, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.initial_radius_d, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.visibility_d, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.alpha_d, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.irr_map, sizeof(bool), &nb);
        break;
      case OVERRIDE_CHUNK:
        res = iload->Read(&m_rendParams.env_mult, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.overrideGrayMatOn, sizeof(bool), &nb);
        break;
      case MLT_CHUNK:
        res = iload->Read(&m_rendParams.mlt_plarge, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.mlt_iters_mult, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.mlt_burn_iters, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.mlt_enable_median_filter, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.mlt_median_filter_threshold, sizeof(float), &nb);
        break;
      case MMLT_CHUNK:
        res = iload->Read(&m_rendParams.mmlt_estim_time, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.mmlt_enable_median_filter, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.mmlt_median_filter_threshold, sizeof(float), &nb);
        break;
      case DENOISE_CHUNK:
        res = iload->Read(&m_rendParams.denoiserOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.denoiserAlbedoPassOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.denoiserNormalPassOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.denoiseLvl, sizeof(float), &nb);
        break;
      case POSTPROC_CHUNK:
        res = iload->Read(&m_rendParams.postProcOn, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.exposure, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.compress, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.contrast, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.saturation, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.vibrance, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.whiteBalance, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.whitePointColor, sizeof(Color), &nb);
        res = iload->Read(&m_rendParams.uniformContrast, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.normalize, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.chromAberr, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.vignette, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.sharpness, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.sizeStar, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.numRay, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.rotateRay, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.randomAngle, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.sprayRay, sizeof(float), &nb);

        res = iload->Read(&m_rendParams.filter_id, sizeof(int), &nb);
        res = iload->Read(&m_rendParams.bloom, sizeof(bool), &nb);
        //res = iload->Read(&m_rendParams.hydraFrameBuf, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.mlaa, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.bloom_radius, sizeof(float), &nb);
        res = iload->Read(&m_rendParams.bloom_str, sizeof(float), &nb);
        break;
      case REND_ELEM_CHUNK:
        res = iload->Read(&m_rendParams.renElemAlphaOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemColorOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemCoordOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemCoverOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemDepthOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemInstIdOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemMatIdOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemNormalsOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemObjIdOn, sizeof(bool), &nb);
        res = iload->Read(&m_rendParams.renElemShadowOn, sizeof(bool), &nb);
    }
    iload->CloseChunk();
    if (res != IO_OK)
      return res;
  }

  auto savedDevId   = HydraRenderPlugin::m_lastRendParams.device_id;
  auto saveEnType   = HydraRenderPlugin::m_lastRendParams.engine_type;
  auto saveLiteMode = HydraRenderPlugin::m_lastRendParams.liteMode;

  HydraRenderPlugin::m_lastRendParams = m_rendParams;

  HydraRenderPlugin::m_lastRendParams.device_id   = savedDevId;
  HydraRenderPlugin::m_lastRendParams.engine_type = saveEnType;
  HydraRenderPlugin::m_lastRendParams.liteMode    = saveLiteMode;

  return IO_OK;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


std::string MethodName(int a_name)
{
  if (a_name == RENDER_METHOD_PT)
    return "pathtracing";
  else if (a_name == RENDER_METHOD_MMLT)
    return "mmlt";
  else if (a_name == RENDER_METHOD_LT)
    return "lighttracing";
  else if (a_name == RENDER_METHOD_IBPT)
    return "IBPT";
  else
    return "none";
}

std::wstring MethodNameW(int a_name)
{
  if (a_name == RENDER_METHOD_PT)
    return L"pathtracing";
  else if (a_name == RENDER_METHOD_LT)
    return L"lighttracing";
  else if (a_name == RENDER_METHOD_IBPT)
    return L"IBPT";
  else if (a_name == RENDER_METHOD_MMLT)
    return L"mmlt";
  else
    return L"none";
}

std::wstring MethodNamePrimaryW(int a_name)
{
  if (a_name == RENDER_METHOD_PT || a_name == RENDER_METHOD_MMLT)
    return L"pathtracing";
  else if (a_name == RENDER_METHOD_LT)
    return L"lighttracing";
  else if (a_name == RENDER_METHOD_IBPT)
    return L"IBPT";
  else
    return L"none";
}

std::vector<std::string> mlGetRenderLayersStrs(const HydraRenderParams* pRenderParams);

void HydraRenderPlugin::SetRenderParamsImageSizeFromBitMap(HydraRenderParams* pParams, Bitmap* tobm)
{
  pParams->devWidth     = tobm->Width();
  pParams->devHeight    = tobm->Height();
  pParams->devAspect    = tobm->Aspect();

  pParams->nMinx = 0;
  pParams->nMiny = 0;
  pParams->nMaxx = pParams->devWidth;
  pParams->nMaxy = pParams->devHeight;
}

HRRenderRef HydraRenderPlugin::SetupRender(FrameRendParams &frp)
{
  //set devices  
  for (const auto& [key, value] : m_rendParams.device_id)
    hrRenderEnableDevice(g_hrPlugResMangr.m_rendRef, key, value);

  const auto renderWidth  = m_rendParams.nMaxx - m_rendParams.nMinx;
  const auto renderHeight = m_rendParams.nMaxy - m_rendParams.nMiny;

  hrRenderOpen(g_hrPlugResMangr.m_rendRef, HR_OPEN_EXISTING);
  {
    auto node = hrRenderParamNode(g_hrPlugResMangr.m_rendRef);

    node.force_child(L"width") .text()  = renderWidth;
    node.force_child(L"height").text() = renderHeight;

    auto GI_method = m_rendParams.secondary;
    const bool GI_methodCorrect = GI_method >= RENDER_METHOD_PT && GI_method <= RENDER_METHOD_MMLT;

    if (!GI_methodCorrect) GI_method = RENDER_METHOD_PT; // If the GI method is not defined, then PT.

    const auto directLight                       = MethodNamePrimaryW(GI_method);
    const auto indirectLight                     = MethodNameW(GI_method);

    node.force_child(L"method_primary")  .text() = directLight.c_str();
    node.force_child(L"method_secondary").text() = indirectLight.c_str();
    node.force_child(L"method_tertiary") .text() = indirectLight.c_str();

    if (m_rendParams.enableCaustics) node.force_child(L"method_caustic").text() = indirectLight.c_str();
    else                             node.force_child(L"method_caustic").text() = L"none";

    node.force_child(L"shadows").text() = L"1";

    // path tracing

    node.force_child(L"trace_depth")     .text()  = m_rendParams.raybounce;
    node.force_child(L"diff_trace_depth").text()  = m_rendParams.diffbounce;

    node.force_child(L"pt_error")       .text()   = m_rendParams.relative_error;
    node.force_child(L"minRaysPerPixel").text()   = 64;// m_rendParams.minrays;

    // Many test scenes in 3D max, got the option "infinite samples" and incorrect GI method,
    // after adding new options to the GUI. Therefore, you need to check for the correctness of GI,
    // then you can disable infinite samples.

    int maxRays = m_rendParams.maxrays;
    if (m_rendParams.infQuality && GI_methodCorrect) maxRays = 2000000;
    if (indirectLight == L"IBPT")
      maxRays /= 2;

    node.force_child(L"maxRaysPerPixel").text() = maxRays;

    // QMC
    const int    QMC_DOF_FLAG = 1;
    const int    QMC_MTL_FLAG = 2;
    const int    QMC_LGT_FLAG = 4;
    unsigned int qmcVar       = QMC_DOF_FLAG;

    if (m_rendParams.qmcMaterials) qmcVar |= QMC_MTL_FLAG;
    if (m_rendParams.qmcLights)    qmcVar |= QMC_LGT_FLAG;
    node.force_child(L"qmc_variant").text() = qmcVar;

    // MMLT
    //const int burn[]       = { 256, 1024, 4069, 16384 };
    //const float stepSize[] = { 0.25, 0.5, 1.0, 1.5, 2.0 };

    int       burn      = 64;
    float     stepSize  = 0.25F;
    
    switch (m_rendParams.mmlt_estim_time)
    {
      case 0:
        burn     = 256;
        stepSize = 0.25F;
        break;
      case 1: // default
        burn     = 1024;
        stepSize = 0.5F;
        break;
      case 2:
        burn     = 4096;
        stepSize = 1.0F;
        break;
      case 3:
        burn     = 16384;
        stepSize = 2.0F;
        break;
      default:
        break;
    }

    node.force_child(L"mmlt_burn_iters").text()     = burn;// this->m_rendParams.mmlt_burn_iters;
    node.force_child(L"mmlt_step_power").text()     = L"normal";
    node.force_child(L"mmlt_step_size").text()      = stepSize; // this->m_rendParams.mmlt_stepSize;
    node.force_child(L"mmlt_threads").text()        = 524288;
    node.force_child(L"mmlt_multBrightness").text() = 1.0F;     // this->m_rendParams.mmlt_multBrightness;
    node.force_child(L"mlt_med_enable").text()      = m_rendParams.mmlt_enable_median_filter;
    node.force_child(L"mlt_med_threshold").text()   = m_rendParams.mmlt_median_filter_threshold;

    //clamping
    const float envClamp   = m_rendParams.env_clamp;
    const float bsdfClamp  = (m_rendParams.bsdf_clamp_on) ? m_rendParams.bsdf_clamp : 1e6F;

    node.force_child(L"envclamp").text()      = envClamp;
    node.force_child(L"clamping").text()      = bsdfClamp;
    node.force_child(L"separate_swap").text() = int(m_rendParams.useSeparateSwap);

    //region render && blowUp
    if (&frp != nullptr)
    {
      if (m_rendParams.rendType == RENDTYPE_REGION || RENDTYPE_REGION_SEL == RENDTYPE_REGION)
      {
        node.force_child(L"regxmin").text() = frp.regxmin;
        node.force_child(L"regxmax").text() = frp.regxmax;
        node.force_child(L"regymin").text() = frp.regymin;
        node.force_child(L"regymax").text() = frp.regymax;
      }
      if (m_rendParams.rendType == RENDTYPE_BLOWUP || m_rendParams.rendType == RENDTYPE_BLOWUP_SEL)
      {
        const float centerX = float(renderWidth)  * 0.5F;
        const float centerY = float(renderHeight) * 0.5F;
              
        const float offsetX = (frp.blowupCenter.x - centerX) / float(renderWidth);
        const float offsetY = (frp.blowupCenter.y - centerY) / float(renderHeight);
              
        const float scaleX  = 1.0F / frp.blowupFactor.x;
        const float scaleY  = 1.0F / frp.blowupFactor.y;

        node.force_child(L"blowupOffsetX").text() = offsetX;
        node.force_child(L"blowupOffsetY").text() = offsetY;
        node.force_child(L"blowupScaleX").text()  = scaleX;
        node.force_child(L"blowupScaleY").text()  = scaleY;
      }
    }

    if (m_rendParams.debugCheckBoxes[L"FORCE_GPU_FRAMEBUFFER"]) node.force_child(L"forceGPUFrameBuffer").text() = 1;
    else                                                     node.force_child(L"forceGPUFrameBuffer").text() = 0;

    if (m_rendParams.renElemAlphaOn || m_rendParams.renElemColorOn || m_rendParams.renElemCoordOn ||
        m_rendParams.renElemCoverOn || m_rendParams.renElemDepthOn || m_rendParams.renElemInstIdOn ||
        m_rendParams.renElemMatIdOn || m_rendParams.renElemNormalsOn || m_rendParams.renElemObjIdOn ||
        m_rendParams.renElemShadowOn || m_rendParams.denoiserAlbedoPassOn)
    {
      m_rendParams.hasCalculateRenderElements = true;
      node.force_child(L"evalgbuffer").text() = 1; // Enable gbuffer
    }
    else
      node.force_child(L"evalgbuffer").text() = 0; // Disable gbuffer


    if (m_rendParams.debugCheckBoxes[L"DEBUG"]) node.force_child(L"dont_run").text()      = 1;
    else                                        node.force_child(L"dont_run").text()      = 0;

    if (m_rendParams.enableTextureResize)       node.force_child(L"scenePrepass").text()  = 1;
    else                                        node.force_child(L"scenePrepass").text()  = 0;

    if (m_rendParams.offlinePT)                 node.force_child(L"offline_pt").text()    = 1;
    else                                        node.force_child(L"offline_pt").text()    = 0;

    if (BUILD_FOR_EXPORT_COMPRESS_MESHES)
      m_compressMeshOnExport = true; // always true for special build
    else
      m_compressMeshOnExport = m_rendParams.debugCheckBoxes[L"COMPRESS_MESHES"];

    node.force_child(L"resources_path").text() = HYDRA_SHADERS_PATH.c_str();


    //gamma
    if (gammaMgr.IsEnabled())
      m_rendParams.gamma = gammaMgr.GetDisplayGamma();

    node.force_child(L"tmGamma").text() = m_rendParams.gamma;
    //node.force_child(L"texInputGamma").text() = gammaMgr.GetFileInGamma(); // don't use this anymore!


    //other
    if (m_rendParams.enableLog)
    {
      node.force_child(L"outputRedirect").text() = int(m_rendParams.enableLog);
      hrRenderLogDir(g_hrPlugResMangr.m_rendRef, HYDRA_LOGS_PATH.c_str(), false);
    }
    else
    {
      node.force_child(L"outputRedirect").text() = int(m_rendParams.enableLog);
      hrRenderLogDir(g_hrPlugResMangr.m_rendRef, L"", false);
    }

  }
  hrRenderClose(g_hrPlugResMangr.m_rendRef);

  return g_hrPlugResMangr.m_rendRef;
}

HRRenderRef HydraRenderPlugin::SetupRenderForMatEditor(FrameRendParams &frp)
{
  HR_OPEN_MODE mode;
  //if (m_rendRefMatEditor.id == -1)
  //{
  mode = HR_WRITE_DISCARD;
  m_rendRefMatEditor = hrRenderCreate(L"HydraModern");
  //}
  //else
  //{
  //  mode = HR_OPEN_EXISTING;
  //}

  //set devices
  for (int i = 0; i < (int)m_rendParams.device_id.size(); ++i)
    hrRenderEnableDevice(m_rendRefMatEditor, m_rendParams.device_id.at(i), true);

  auto renderWidth = m_rendParams.nMaxx - m_rendParams.nMinx; 
  auto renderHeight = m_rendParams.nMaxy - m_rendParams.nMiny;

  hrRenderOpen(m_rendRefMatEditor, mode);
  {
    auto node = hrRenderParamNode(m_rendRefMatEditor);

    node.force_child(L"width").text()            = renderWidth;
    node.force_child(L"height").text()           = renderHeight;

    //methods setup
    node.force_child(L"method_primary").text()   = L"pathtracing";
    node.force_child(L"method_secondary").text() = L"pathtracing";
    node.force_child(L"method_tertiary").text()  = L"pathtracing";
    node.force_child(L"shadows").text()          = L"1";

    // path tracing
    node.force_child(L"trace_depth").text()      = 8;
    node.force_child(L"diff_trace_depth").text() = 2;

    node.force_child(L"pt_error").text()         = 2.0f;
    node.force_child(L"minRaysPerPixel").text()  = 64;
    node.force_child(L"maxRaysPerPixel").text()  = 256;

    node.force_child(L"envclamp").text()         = 1e6F;
    node.force_child(L"bsdfclamp").text()        = 1e6F;
    node.force_child(L"separate_swap").text()    = 0;

    //gamma
    if (gammaMgr.IsEnabled())
      this->m_rendParams.gamma = gammaMgr.GetDisplayGamma();

    node.force_child(L"tmGamma").text() = this->m_rendParams.gamma;
    node.force_child(L"texInputGamma").text() = gammaMgr.GetFileInGamma();

    //other
    node.force_child(L"outputRedirect").text() = this->m_rendParams.enableLog;
  }
  hrRenderClose(m_rendRefMatEditor);

  return m_rendRefMatEditor;
}


bool HydraRenderPlugin::VerifyThings(int renderWidth, int renderHeight)
{  
  std::filesystem::path engineFile = HYDRA_ENGINE_PATH / L"hydra.exe";
  
  if (!std::filesystem::exists(engineFile))
  {
    std::wstring message = L"Can't find " + std::wstring(engineFile) + L", perhaps forgot copy[Hydra] folder to 'C:\\' ? ";
    MessageBoxW(NULL, message.c_str(), L"Critical error", MB_OK);
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Fatal, message);

    return false;
  }
  /*
  if (!m_rendParams.inMtlEditor && (incl.geomObjNum == 0))
  {
    g_hrPlugResMangr.m_pluginLog.Print("Empty scene.");
    return false;
  }*/

  return true;
}


void HydraRenderPlugin::ClearTempFolder()
{
  std::vector<std::filesystem::path> folders = {
    HYDRA_SCENELIB_PATH,
    HYDRA_SCENELIBDATA_PATH,    
    HYDRA_TEMP_PATH };

  //for (const auto& folder : folders)
  //{
  //  std::wstring tempName = folder / L"*";
  //  WIN32_FIND_DATAW fd;
  //  HANDLE hFind = ::FindFirstFile(tempName.c_str(), &fd);
  //  if (hFind != INVALID_HANDLE_VALUE)
  //  {
  //    do
  //    {
  //      std::filesystem::path tempName2 = folder / fd.cFileName;

  //      if (tempName2.has_extension())
  //        remove(tempName2);        

  //    } while (::FindNextFile(hFind, &fd));

  //    ::FindClose(hFind);
  //  }
  //}
  
  for (const auto& folder : folders)
  {
    // loop over all the "directory_entry"'s in folder:
    for (auto& dir : std::filesystem::directory_iterator(folder))
    {
      if (dir.path().has_extension())
        std::filesystem::remove(dir.path());

      //std::filesystem::remove_all(dir.path());
    }
  }
}


void HydraRenderPlugin::CopyBitmap(Bitmap* from, Bitmap* to) const
{
  int renderWidth  = m_rendParams.nMaxx - m_rendParams.nMinx;

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int y = m_rendParams.nMiny; y < m_rendParams.nMaxy; ++y)
  {
    std::vector<BMM_Color_fl> line(renderWidth);
    from->GetPixels(0, y, renderWidth, line.data());
    to->PutPixels(0, y, renderWidth, line.data());
  }
}

void HydraRenderPlugin::DisplayLastRender(bool processed)
{
  if (!processed)
  {
    const int renderWidth  = m_rendParams.nMaxx - m_rendParams.nMinx;
    const int renderHeight = m_rendParams.nMaxy - m_rendParams.nMiny;
    // m_pLastRender->CopyImage(raw_render, COPY_IMAGE_CROP, 0);
    CopyImageFromHRRenderToBitmap(m_pLastRender, renderWidth, renderHeight);
  }

  if (m_pLastRender && m_pLastRender->Storage())
  {
    m_pLastRender->RefreshWindow();
    //const auto win = m_pLastRender->GetWindow(); 
    //if (win)
    //  UpdateWindow(win);
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

HWND g_hProgBar = NULL;

void DenoiseProgressBar(const char* message, float a_progress)
{
  if (g_hProgBar == NULL)
    return;
  //SendMessage(g_hProgBar, PBM_STEPIT, 0, 0);
  //SendMessageA(g_hProgBar, PBM_SETSTEP, (WPARAM)(a_progress*100.0f), 0);
  SendMessage(g_hProgBar, PBM_SETPOS, (WPARAM)(a_progress*100.0f), 0);
}

void InitDenoiseProgressBar()
{
  HWND hwndParent = GetDesktopWindow();

  g_hProgBar = CreateWindowExA(WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE, PROGRESS_CLASSA, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, 512, 512, 500, 50, hwndParent, (HMENU)0, hInstance, NULL);
  BringWindowToTop(g_hProgBar);

  SendMessageA(g_hProgBar, PBM_SETRANGE, 0, MAKELPARAM(0, 100));
  SendMessage(g_hProgBar, PBM_SETPOS, 0, 0);
  SendMessage(g_hProgBar, PBM_SETSTEP, (WPARAM)1, 0);
}

void DestroyDenoiseProgressBar()
{
  DestroyWindow(g_hProgBar);
  g_hProgBar = NULL;
}

void HydraRenderPlugin::DoDenoise(Bitmap* a_maxFrameBuff)
{
  if (!a_maxFrameBuff)
    return;

  //InitDenoiseProgressBar();

  // Create an Intel Open Image Denoise device
  oidn::DeviceRef device = oidn::newDevice();
  device.commit();
  oidn::FilterRef filter = device.newFilter("RT"); // generic ray tracing filter
  DoIntelDenoiseLoop(a_maxFrameBuff, device, filter);

  //DestroyDenoiseProgressBar();
}


void HydraRenderPlugin::DoPostProc(Bitmap* a_bmap, const bool a_getRawImg) const
{
  if (g_hrPlugResMangr.m_hydraRawRender.id == -1)
    return;

  int w   = m_rendParams.nMaxx - m_rendParams.nMinx;
  int h   = m_rendParams.nMaxy - m_rendParams.nMiny;
  int bpp = 16;

  //hrRenderSaveFrameBufferHDR(g_hrPlugResMangr.m_rendRef, L"c:/[Hydra]/temp/test.hdr");

  if (a_getRawImg)
    hrRenderCopyFrameBufferToFBI(g_hrPlugResMangr.m_rendRef, L"color", g_hrPlugResMangr.m_hydraRawRender);

  pugi::xml_document docSettings;
  pugi::xml_node settings = docSettings.append_child(L"settings");

  settings.append_attribute(L"numThreads")            = m_rendParams.numThreads;
  settings.append_attribute(L"exposure")              = m_rendParams.exposure;
  settings.append_attribute(L"compress")              = m_rendParams.compress;
  settings.append_attribute(L"contrast")              = m_rendParams.contrast;
  settings.append_attribute(L"saturation")            = m_rendParams.saturation;
  settings.append_attribute(L"vibrance")              = m_rendParams.vibrance;
  settings.append_attribute(L"whiteBalance")          = m_rendParams.whiteBalance;
  settings.append_attribute(L"whitePointColor");
  float colorArr[3]                                   = { m_rendParams.whitePointColor.r, m_rendParams.whitePointColor.g, m_rendParams.whitePointColor.b };
  HydraXMLHelpers::WriteFloat3(settings.attribute(L"whitePointColor"), colorArr);
  settings.append_attribute(L"uniformContrast")       = m_rendParams.uniformContrast;
  settings.append_attribute(L"normalize")             = m_rendParams.normalize;
  settings.append_attribute(L"chromAberr")            = m_rendParams.chromAberr;
  settings.append_attribute(L"vignette")              = m_rendParams.vignette;
  settings.append_attribute(L"sharpness")             = m_rendParams.sharpness;

  settings.append_attribute(L"diffStars_sizeStar")    = m_rendParams.sizeStar;
  settings.append_attribute(L"diffStars_numRay")      = m_rendParams.numRay;
  settings.append_attribute(L"diffStars_rotateRay")   = m_rendParams.rotateRay;
  settings.append_attribute(L"diffStars_randomAngle") = m_rendParams.randomAngle;
  settings.append_attribute(L"diffStars_sprayRay")    = m_rendParams.sprayRay;

  hrFilterApply(L"post_process_hydra1", settings, HRRenderRef(), L"in_color", g_hrPlugResMangr.m_hydraRawRender, L"out_color", g_hrPlugResMangr.m_hydraOutRender);

  //hrFBISaveToFile(image2, L"tests_images/test_306/z_out.png");

  auto image = (float*)hrFBIGetData(g_hrPlugResMangr.m_hydraOutRender, &w, &h, &bpp);

  CopyRawImageToBitmap(a_bmap, image, (size_t)w, (size_t)h, true);
}

void HydraRenderPlugin::UpdatePostProc(HydraRenderParamDlg* a_dlg)
{
  const int renderWidth  = a_dlg->m_pRend->m_rendParams.devWidth;//a_dlg->m_pRend->m_rendParams.nMaxx - a_dlg->m_pRend->m_rendParams.nMinx;
  const int renderHeight = a_dlg->m_pRend->m_rendParams.devHeight;//a_dlg->m_pRend->m_rendParams.nMaxy - a_dlg->m_pRend->m_rendParams.nMiny;

  a_dlg->ReadGuiToRendParams(&a_dlg->m_pRend->m_rendParams);
  
  if (!a_dlg->m_pRend->m_rendParams.denoiserOn && !a_dlg->m_pRend->m_rendParams.postProcOn)
  {
    a_dlg->m_pRend->DisplaySource(a_dlg);
    return;
  }

  a_dlg->m_pRend->m_rendParams.numThreads = 0; // all threads

  hrFBIResize(g_hrPlugResMangr.m_hydraOutRender, renderWidth, renderHeight);
  hrFBIResize(g_hrPlugResMangr.m_hydraRawRender, renderWidth, renderHeight);

  if (a_dlg->m_pRend->m_rendParams.denoiserOn)
    DoDenoise(m_pLastRender);

  if (a_dlg->m_pRend->m_rendParams.postProcOn)
    DoPostProc(m_pLastRender, !a_dlg->m_pRend->m_rendParams.denoiserOn);

  CopyAlphaFromGbuffer(m_pLastRender);
  DisplayLastRender();
}

void HydraRenderPlugin::ResetPostProc(HydraRenderParamDlg* a_dlg) const
{
  a_dlg->m_exposure_slider->       SetValue(1, FALSE);
  a_dlg->m_compress_slider->       SetValue(0, FALSE);
  a_dlg->m_contrast_slider->       SetValue(0, FALSE);
  a_dlg->m_saturation_slider->     SetValue(1, FALSE);
  a_dlg->m_vibrance_slider->       SetValue(1, FALSE);
  a_dlg->m_whiteBalance_slider->   SetValue(0, FALSE);
  a_dlg->m_whitePoint_selector->   SetColor({ 0, 0, 0 });
  a_dlg->m_uniformContrast_slider->SetValue(0, FALSE);
  a_dlg->m_normalize_slider->      SetValue(0, FALSE);
  a_dlg->m_chromAberr_slider->     SetValue(0, FALSE);
  a_dlg->m_vignette_slider->       SetValue(0, FALSE);
  a_dlg->m_sharpness_slider->      SetValue(0, FALSE);
  a_dlg->m_sizeStar_slider->       SetValue(0, FALSE);
  a_dlg->m_numRay_slider->         SetValue(8, FALSE);
  a_dlg->m_rotateRay_slider->      SetValue(10, FALSE);
  a_dlg->m_randomAngle_slider->    SetValue(0, FALSE);
  a_dlg->m_sprayRay_slider->       SetValue(0, FALSE);
}


void HydraRenderPlugin::DisplaySource(HydraRenderParamDlg* a_dlg)
{
  if (a_dlg && a_dlg->m_pRend && a_dlg->m_pRend->m_pLastRender)
  {
    a_dlg->m_pRend->DisplayLastRender(false);
    CopyAlphaFromGbuffer(m_pLastRender);
  }
}


// This is used as a callback for aborting a potentially lengthy operation.
class MyCallback : public CheckAbortCallback 
{
  HydraRenderPlugin* hr;
public:
  MyCallback(HydraRenderPlugin* r) { hr = r; }
  BOOL Check()                        override { return hr->CheckAbort(0, 0); }
  BOOL Progress(int done, int total)  override { return hr->CheckAbort(done, total); }
  void SetTitle(const TCHAR* title)   override { hr->SetProgTitle(title); }
};


int HydraRenderPlugin::CheckAbort(int done, int total) 
{
  if (m_rendParams.progCallback) 
    if (m_rendParams.progCallback->Progress(done, total) == RENDPROG_ABORT)     
      return 1;
    
  return 0;
}


void HydraRenderPlugin::SetProgTitle(const TCHAR *title)
{
  if (m_rendParams.progCallback)
    m_rendParams.progCallback->SetTitle(title);
}


std::wstring HydraRenderPlugin::GetHydraVersionW(const std::wstring& ver) const
{  
  std::string str(__DATE__);
  std::wstring res = L"v" + ver + L", build " + std::wstring(str.begin(), str.end());
  return res;
}



std::wstring& replace(std::wstring& s, const std::wstring& from, const std::wstring& to)
{
  if (!from.empty())
    for (size_t pos = 0; (pos = s.find(from, pos)) != std::string::npos; pos += to.size())
      s.replace(pos, from.size(), to);
  return s;
}

std::string current_time_and_date()
{
  time_t now = time(0);
  tm tstruct;
  char buf[80];
  localtime_s(&tstruct, &now);

  strftime(buf, sizeof(buf), "__%Y_%m_%d_%H_%M_%S", &tstruct);
  return buf;
}

void HydraRenderPlugin::SaveRenderImagesEveryNminutes(RendProgressCallback* m_prog, const std::chrono::system_clock::time_point& timeStart,
  float& saveImageNext, const float elapsedMin, bool& saveFileEveryN_munutes) const
{
  auto timeCurr = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsedSecondsFromLastSave = timeCurr - timeStart; // timeStart2;

  if (elapsedSecondsFromLastSave.count() > saveImageNext)
  {
    std::wstringstream strOut;
    strOut << L"Save the image for " << (unsigned int)elapsedMin << L"minutes";
    auto progstr = strOut.str();
    m_prog->SetTitle(progstr.c_str());

    std::wstring modeGI;
    switch (m_rendParams.secondary)
    {
    case 0:
      modeGI = L"pt";
      break;
    case 1:
      modeGI = L"lt";
      break;
    case 2:
      modeGI = L"ibpt";
      break;
    case 3:
      modeGI = L"mmlt";
      break;
    default:
      break;
    }

    std::wstringstream strOut2;
    std::wstringstream strOut3;
    strOut2 << "C:\\[Hydra]\\rendered_images\\ldr_" << modeGI.c_str() << "_" << (unsigned int)elapsedMin << "min.png";
    strOut3 << "C:\\[Hydra]\\rendered_images\\hdr_" << modeGI.c_str() << "_" << (unsigned int)elapsedMin << "min.hdr";
    hrRenderSaveFrameBufferLDR(g_hrPlugResMangr.m_rendRef, strOut2.str().c_str());
    hrRenderSaveFrameBufferHDR(g_hrPlugResMangr.m_rendRef, strOut3.str().c_str());

    g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Image saved at", saveImageNext);

    saveFileEveryN_munutes = true;
    saveImageNext = saveImageNext * 2.0f; // - (m_rendParams.saveRendImagEveryMin * 60.0f);
  }
}

std::wstring HydraRenderPlugin::getSceneName()
{
  std::wstring sceneName = strConverter::c2ws(GetCOREInterface()->GetCurFileName().data());
  std::wstring sceneName2 = replace(sceneName, L".max", L"");

  return sceneName2 + strConverter::ToWideString(current_time_and_date());
}

std::wstring AlterSavedName(const std::wstring& a_name, const std::wstring& a_alter)
{
  if (a_name == L"")
    return std::wstring(L"bad_name.hdr");

  std::wstring fname = a_name.substr(0, a_name.size() - 4);
  //std::wstring fres  = a_name.substr(a_name.size() - 4, 4);

  return fname + a_alter + L".hdr"; //fres;
}


void MinimumPauseLoop(const std::chrono::system_clock::time_point& a_timeStart, const float a_minMillsecForSleep)
{
  const auto  timeCurr           = std::chrono::system_clock::now();
  const float loopElapsedSeconds = std::chrono::duration<float>(timeCurr - a_timeStart).count();

  Sleep((int)(fmax(a_minMillsecForSleep - loopElapsedSeconds * 1000.0F, 0.0F)));
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


template<typename T>
std::string ToString(const T& a_rhs)
{
  std::stringstream outStr;
  outStr << a_rhs;
  return outStr.str();
}


ClassDesc2* GetHydraRenderClassDesc()
{
  return &hydraRenderDesc;
}


std::unordered_map<int, bool> firstTimeDeviceID;
int firstTimeEngineType;
bool rememberDevDlg = false;
bool IsDarkTheme();


void CreateDeviceSettingsFile(HRRenderRef a_ref)
{  
  if (std::string str = strConverter::ws2s(DEVICE_FILE); isFileExist(str.c_str()))
    DeleteFileW(DEVICE_FILE.c_str()); 

  std::vector<HydraRenderDevice> devList = InitDeviceListInternal(a_ref);

  std::wofstream out;
  out.open(DEVICE_FILE);
  out << firstTimeEngineType << "\n";

  for (const auto& [devId, enable] : firstTimeDeviceID)
  {
    out << devId  << "\n";
    out << enable << "\n";

    for (const auto& j : devList)
    {
      if (j.id == devId)
      {
        out << j.name << "\n";
        break;
      }
    }
  }

  out.flush();
  out.close();
}


void CreateDeviceSettingsFile(int engineType, const std::unordered_map<int,bool>& deviceIDs, HRRenderRef a_ref)
{  
  if (std::string str = strConverter::ws2s(DEVICE_FILE); isFileExist(str.c_str()))
    DeleteFileW(DEVICE_FILE.c_str()); 

  if (engineType == -1)
    return;

  std::vector<HydraRenderDevice> devList = InitDeviceListInternal(a_ref);

  std::wofstream out;
  out.open(DEVICE_FILE);
  out << engineType << "\n";

  for (const auto& [devId, enable] : deviceIDs)
  {
    out << devId  << "\n";
    out << enable << "\n";

    for (const auto& j : devList)
    {
      if (j.id == devId)
      {
        out << j.name << "\n";
        break;
      }
    }
  }

  out.flush();
  out.close();
}

bool HydraRenderPlugin::readAndCheckDeviceSettingsFile(int &mode, std::unordered_map<int, bool> &deviceIDs)
{
  std::wifstream in;

  in.open(DEVICE_FILE);

  if (!in)
    return false;

  std::wstring tmp;
  std::getline(in, tmp);

  if (tmp.size() == 0 || !std::iswdigit(tmp[0]))
    return false;

  mode = std::stoi(tmp);

  std::vector<HydraRenderDevice> devList = InitDeviceListInternal(g_hrPlugResMangr.m_rendRef);

  while (!in.eof()) //-V1024
  {
    int id;
    int onOff;
    std::wstring name;
    tmp.clear();

    std::getline(in, tmp);
    if (tmp.empty())
      break;

    if (tmp.size() == 0 || !std::iswdigit(tmp[0]))
      return false;

    id = std::stoi(tmp);

    tmp.clear();
    std::getline(in, tmp);
    if (tmp.empty())
      tmp = L"1";

    if (tmp.size() == 0 || !std::iswdigit(tmp[0]))
      return false;

    onOff = std::stoi(tmp);

    std::getline(in, name);

    bool found = false;

    for (const auto& i : devList)
    {
      if (i.id == id && i.name == name)
      {
        found = true;
        break;
      }
    }

    if (!found)
    {
      MessageBoxW(NULL, L"One or more previously selected rendering devices could not be found or its name has changed.", L"Warning", 0);
      return false;
    }

    deviceIDs[id] = onOff;
  }
  in.close();

  if (deviceIDs.empty())
    return false;

  m_rendParams.rememberDevice = true;

  return true;
}

INT_PTR CALLBACK SelectDeviceProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

  HWND hwndOwner;
  RECT rc, rcDlg, rcOwner;

  switch (message)
  {
    case WM_INITDIALOG:
    {
      GetCOREInterface()->RegisterDlgWnd(hwndDlg);
      InitCommonControls();

      HWND devices_list = GetDlgItem(hwndDlg, IDC_DEVICES_LIST);

      ListView_SetExtendedListViewStyleEx(devices_list, 0, LVS_EX_CHECKBOXES);
      //ListView_SetView(devices_list, LV_VIEW_DETAILS);
      ListView_SetBkColor(devices_list, CLR_NONE);
      ListView_SetTextBkColor(devices_list, CLR_NONE);

      if (IsDarkTheme())
        ListView_SetTextColor(devices_list, RGB(228, 228, 228));
      else
        ListView_SetTextColor(devices_list, RGB(20, 20, 20));

      LVCOLUMN lvc;
      int iCol;
      int nCol = 3;
      lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
      hydraStr cols[] ={ _M("ID"), _M("Device Name"), _M("Driver/CC") };

      for (iCol = 0; iCol < nCol; iCol++)
      {
        lvc.iSubItem = iCol;
        lvc.pszText = (LPWSTR)cols[iCol].c_str();
        lvc.fmt = LVCFMT_LEFT;

        if (iCol < 1) lvc.cx = 40;
        else lvc.cx = 150;

        ListView_InsertColumn(devices_list, iCol, &lvc);
      }

      //int mode = SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_GETCURSEL, 0, 0);
      SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_ADDSTRING, 0, (LPARAM)(_M("CUDA")));
      SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_ADDSTRING, 0, (LPARAM)(_M("OpenCL")));
      SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_SETCURSEL, 1, 1);
      InitDeviceList(devices_list, 1, false, g_hrPlugResMangr.m_rendRef);

      if ((hwndOwner = GetParent(hwndDlg)) == NULL)
      {
        hwndOwner = GetDesktopWindow();
      }

      GetWindowRect(hwndOwner, &rcOwner);
      GetWindowRect(hwndDlg, &rcDlg);
      CopyRect(&rc, &rcOwner);

      // Offset the owner and dialog box rectangles so that right and bottom 
      // values represent the width and height, and then offset the owner again 
      // to discard space taken up by the dialog box. 

      OffsetRect(&rcDlg, -rcDlg.left, -rcDlg.top);
      OffsetRect(&rc, -rc.left, -rc.top);
      OffsetRect(&rc, -rcDlg.right, -rcDlg.bottom);

      // The new position is the sum of half the remaining space and the owner's 
      // original position. 

      SetWindowPos(hwndDlg,
                   HWND_TOP,
                   rcOwner.left + (rc.right / 2),
                   rcOwner.top + (rc.bottom / 2),
                   0, 0,          // Ignores size arguments. 
                   SWP_NOSIZE);

      return TRUE;
    }
    case WM_COMMAND:
    {
      switch (LOWORD(wParam))
      {
        case IDOK:
        {
          HWND devices_list = GetDlgItem(hwndDlg, IDC_DEVICES_LIST);
          int numItems = ListView_GetItemCount(devices_list);

          for (auto i = 0; i < numItems; ++i)
          {
            hydraChar bufText[4];
            ListView_GetItemText(devices_list, i, 0, &bufText[0], sizeof(bufText));

            if (ListView_GetCheckState(devices_list, i))
            {
              firstTimeDeviceID[std::stoi(bufText)] = 1;
            }
            else
            {
              firstTimeDeviceID[std::stoi(bufText)] = 0;
            }
          }

          auto mode = (int)SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_GETCURSEL, 0, 0);
          firstTimeEngineType = mode;

          if (IsDlgButtonChecked(hwndDlg, IDC_REMEMBER))
          {
            CreateDeviceSettingsFile(g_hrPlugResMangr.m_rendRef);
            rememberDevDlg = true;
          }


          EndDialog(hwndDlg, IDOK);
          break;
        }
        case IDC_ENGINE:
        {
          if (HIWORD(wParam) == LBN_SELCHANGE)
          {
            HWND devices_list = GetDlgItem(hwndDlg, IDC_DEVICES_LIST);
            auto mode = (int)SendDlgItemMessage(hwndDlg, IDC_ENGINE, CB_GETCURSEL, 0, 0);
            firstTimeEngineType = mode;
            InitDeviceList(devices_list, mode, false, g_hrPlugResMangr.m_rendRef);
          }
          break;
        }
      }
      break;
    }
    default:
      return FALSE;
  }
  return TRUE;
}


void HydraRenderPlugin::UserSelectDeviceDialog(HWND hwnd)
{
  auto ret = DialogBoxW(hInstance, MAKEINTRESOURCE(IDD_DEVICE_SETUP), hwnd, SelectDeviceProc);

  if (ret == IDOK)
  {
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"First time device selection dialog is OK.");
    m_rendParams.rememberDevice = rememberDevDlg;
  }
  else if (ret == -1)
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, L"First time device selection dialog failed.");


  if (firstTimeDeviceID.empty())
  {
    firstTimeDeviceID[-1] = true;
    firstTimeEngineType = 1;
  }
  else
  {
    m_devList = InitDeviceListInternal(g_hrPlugResMangr.m_rendRef);

    int devId = firstTimeDeviceID[0];
    if (isTargetDevIdAHydraCPU(devId, m_devList))
      firstTimeDeviceID[-1] = true;
  }

  HydraRenderPlugin::m_lastRendParams.device_id   = firstTimeDeviceID;
  HydraRenderPlugin::m_lastRendParams.engine_type = firstTimeEngineType;
  HydraRenderPlugin::m_lastRendParams.liteMode    = false;

  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Devices: ");
  for (auto id : HydraRenderPlugin::m_lastRendParams.device_id)
    if (id.second) g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, std::to_wstring(id.first));

}
