#include "change_trackers.h"

///////////////////////////////////////////////////////////////////////////////

namespace Hydra
{
  void TrackerManager::NotificationCallback_NotifyEvent(const IGenericEvent& genericEvent, void* userData)
  {
    const auto* const nodeEvent = dynamic_cast<const INodeEvent*>(&genericEvent);
    //DbgAssert(nodeEvent != nullptr);
    INode* const changedNode = (nodeEvent != nullptr) ? nodeEvent->GetNode() : nullptr;

    const auto eventType = genericEvent.GetEventType();

    // Node tracking

    if (changedNode)
    {
      switch (eventType)
      {
      case EventType_Node_Deleted:
        RemoveNode(changedNode);
        if (changedNode == m_representativeNode)
        {
          bool foundMultimat = false;
          if (!instancesPool.empty())
          {
            for (auto& n : instancesPool)
            {
              if (n->GetMtl() != nullptr && n->GetMtl()->ClassID().PartA() == MULTI_CLASS_ID)
              {
                foundMultimat        = true;
                m_representativeNode = n;
                break;
              }
            }
            if (!foundMultimat)
              m_representativeNode = *instancesPool.begin();
          }
        }
        break;
      case EventType_Node_Transform:
      {
        const ObjectState os = changedNode->EvalWorldState(0);
        const auto sClassID  = os.obj->SuperClassID();
        if (sClassID == LIGHT_CLASS_ID)
          needExport = true;                                   // for recalculate intensity in photomitrics unit
        break;
      }
      case EventType_Node_ParamBlock:
      case EventType_Node_Uncategorized:
      case EventType_Mesh_Vertices:
      case EventType_Mesh_Faces:
      case EventType_Mesh_UVs:
      case EventType_Node_RenderProperty:
        //   case EventType_Node_Reference:
        needExport = true;
        break;
      case EventType_Node_Material_Replaced:
      case EventType_Node_Material_Updated:
      {
        const auto repMat     = m_representativeNode->GetMtl();
        const auto newNodeMat = changedNode->GetMtl();
        if (newNodeMat != nullptr && repMat != nullptr && newNodeMat->ClassID().PartA() == MULTI_CLASS_ID && repMat->ClassID().PartA() != MULTI_CLASS_ID)
          m_representativeNode = changedNode; //the node with multi material should be representative for correct material remap lists

        needExport = true;
      }
      break;
      /*case EventType_Node_Reference:
        needExport = true;*/
        /*   case EventType_Node_Hide:
             bool rendThisNode = (GetCOREInterface13()->GetRendHidden() || !changed_node->IsNodeHidden());*/
      default:
        break;
      }
    }


    // Texture tracking

    const auto* const texmapEvent = dynamic_cast<const ITexmapEvent*>(&genericEvent);
    //DbgAssert(texmapEvent != nullptr);
    Texmap* const changedTex = (texmapEvent != nullptr) ? texmapEvent->GetTexmap() : nullptr;

    if (changedTex)
    {
      Mtl* parent;
      MSTR parentName;
      bool found = false;

      switch (eventType)
      {
      case EventType_Texmap_Deleted:
        RemoveTexmap(changedTex);
        break;
      case EventType_Texmap_ParamBlock:
      case EventType_Material_Reference:
      case EventType_Material_Uncategorized:
        texmaps[changedTex].first = true;

        //if texmap has changed need to re-export material too
        parent     = texmaps[changedTex].second;
        parentName = parent->GetName();

        if (!found)
          RemoveTexmap(changedTex);        
        break;
      default:
        break;
      }
    }

  }

                  


  TrackerManager::TrackerManager()
  {
    m_notification_client = std::unique_ptr<IImmediateNotificationClient>(INotificationManager::GetManager()->RegisterNewImmediateClient());

    instancesPool.clear();
    texmaps.clear();    
  }

  TrackerManager::TrackerManager(INode* initial_node, const NotifierType a_notification_type, TimeValue t) :
    m_refObject(initial_node->GetObjOrWSMRef()),
    m_representativeNode(initial_node),
    m_node_notification_type(a_notification_type)
  {
    m_notification_client = std::unique_ptr<IImmediateNotificationClient>(INotificationManager::GetManager()->RegisterNewImmediateClient());

    instancesPool.clear();
    texmaps.clear();

    const ObjectState os = initial_node->EvalWorldState(t);
    isGeoPool = (os.obj->SuperClassID() == SHAPE_CLASS_ID) || (os.obj->SuperClassID() == GEOMOBJECT_CLASS_ID);

    AddNode(initial_node, t);
  }


  TrackerManager::~TrackerManager()
  {
    // Unregister all notifications
    ClearNode();
    //ClearMtl();
    ClearTexmap();
  }


  INode* TrackerManager::GetRepresentativeNode() const
  {
    return m_representativeNode;
  }


  Object* TrackerManager::GetReferencedObject() const
  {
    return m_refObject;
  }


  bool TrackerManager::IsNodeCompatible(INode* node, TimeValue t) const
  {
    if (node->GetObjOrWSMRef() == m_refObject)
    {
      const ObjectState os = node->EvalWorldState(t);
      //SClass_ID SClassID = node->GetObjOrWSMRef()->SuperClassID();
      const auto SClassID  = os.obj->SuperClassID();

      if (SClassID == SHAPE_CLASS_ID || SClassID == GEOMOBJECT_CLASS_ID)
      {
        INode* const this_node         = GetRepresentativeNode();
        const Mtl* const this_material = (this_node != nullptr) ? this_node->GetMtl() : nullptr;
        const Mtl* const new_material  = node->GetMtl();

        if (this_material != new_material)
          return true; 

        auto geom_object = static_cast<GeomObject*>(os.obj);

        if (geom_object != nullptr)
          return !geom_object->IsInstanceDependent();
        else
          return false;
      }
      else if(SClassID == LIGHT_CLASS_ID)
        return true;
      else
        return false;
    }
    else
      return false;
  }


  void TrackerManager::AddNode(INode* a_node, TimeValue t)
  {
    const auto insert_result = instancesPool.insert(a_node);

    // Upon insertion, monitor the node for changes
    if (insert_result.second && (m_notification_client != nullptr))
      m_notification_client->MonitorNode(*a_node, m_node_notification_type, ~size_t(0), *this, nullptr);

    if (isGeoPool)
    {
      const auto repMat = m_representativeNode->GetMtl();
      const auto newNodeMat = a_node->GetMtl();
      if (newNodeMat != nullptr && repMat != nullptr && newNodeMat->ClassID().PartA() == MULTI_CLASS_ID && repMat->ClassID().PartA() != MULTI_CLASS_ID)
      {
        m_representativeNode = a_node; //the node with multi material should be representative for correct material remap lists
        needExport = true;
      }
    }
  }


  void TrackerManager::RemoveNode(INode* a_node)
  {
    const auto found_it = instancesPool.find(a_node);
    if (found_it != instancesPool.end())
    {
      if (m_notification_client != nullptr) // Stop monitoring the node
        m_notification_client->StopMonitoringNode(*a_node, ~size_t(0), *this, nullptr);

      instancesPool.erase(found_it);
    }
  }


  void TrackerManager::ClearNode()
  {
    if (m_notification_client != nullptr)
    {
      for (INode* node : instancesPool)
        m_notification_client->StopMonitoringNode(*node, ~size_t(0), *this, nullptr);
    }
    instancesPool.clear();
  }


  void TrackerManager::AddTexmap(Texmap* tex, Mtl* parentMtl)
  {
    if (tex != nullptr)
    {
      const auto found_it = texmaps.find(tex);
      if (found_it == texmaps.end())
      {
        texmaps[tex].first = true;
        texmaps[tex].second = parentMtl;
        m_notification_client->MonitorTexmap(*tex, ~size_t(0), *this, nullptr);
      }
    }
  }


  void TrackerManager::RemoveTexmap(Texmap* tex)
  {
    const auto found_it = texmaps.find(tex);
    if (found_it != texmaps.end())
    {
      if (m_notification_client != nullptr)
        m_notification_client->StopMonitoringTexmap(*tex, ~size_t(0), *this, nullptr);

      texmaps.erase(found_it);
    }
  }


  void TrackerManager::ClearTexmap()
  {
    for (auto& tex : texmaps)
    {
      if (m_notification_client != nullptr)      
        m_notification_client->StopMonitoringTexmap(*tex.first, ~size_t(0), *this, nullptr);      
    }
    texmaps.clear();
  }
}
