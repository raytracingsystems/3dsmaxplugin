#pragma once

#include <time.h>
#include <fstream>
#include <string>
#include <sstream>
#include <filesystem>
#include <Rendering/IRenderMessageManager.h>

////////////////////////////////////////////////////////////////////////////

using IRMM_S = IRenderMessageManager::MessageSource;
using IRMM_T = IRenderMessageManager::MessageType;

class HydraLogger
{
  public:
    HydraLogger(): isMtlRender(false) { m_3dsMaxRenderLog = GetRenderMessageManager(); }
    ~HydraLogger()                    { outp.close(); outm.close(); }
 
    void OpenLogFile(const std::filesystem::path& file)
    {
      if (outp.is_open())
        return;

      outp.open(file.c_str()); // std::ios::app

      time_t rawtime;
      tm     timeinfo;

      time(&rawtime);
      localtime_s(&timeinfo, &rawtime);      
      char buff[32];
      asctime_s(buff, &timeinfo);

      outp << buff << "------------------------" << std::endl;
    }

    void OpenLogFileM(const std::filesystem::path& file)
    {
      if (outm.is_open())
        return;

      outm.open(file.c_str()); // std::ios::app

      time_t rawtime;
      tm     timeinfo;

      time(&rawtime);
      localtime_s(&timeinfo, &rawtime);
      char buff[32];
      asctime_s(buff, &timeinfo);

      outm << buff << "||----------------------" << std::endl;
    }
    
    bool isMtlRender;


    void Print (IRMM_S a_messageSource, IRMM_T a_messageType, const std::wstring& a_message)
    {
      std::wstring resMessage = L"";

      switch (a_messageType)
      {
        case IRMM_T::kType_Fatal    : resMessage = L"FATAL: "   ; m_3dsMaxRenderLog->OpenMessageWindow(); break;
        case IRMM_T::kType_Error		:	resMessage = L"ERROR: "   ; m_3dsMaxRenderLog->OpenMessageWindow(); break;
        case IRMM_T::kType_Warning	:	resMessage = L"WARNING: " ; m_3dsMaxRenderLog->OpenMessageWindow(); break;
        case IRMM_T::kType_Info			: resMessage = L"INFO: "    ;                                         break;
        case IRMM_T::kType_Progress : resMessage = L"PROGRESS: ";                                         break;
        case IRMM_T::kType_Debug		:	resMessage = L"DEBUG: "   ;                                         break;
        case IRMM_T::kType_System		: resMessage = L"SYSTEM: "  ;                                         break;
        default:                                                                                                         break;
      }

      resMessage += a_message;

      if (isMtlRender) outm << resMessage.c_str() << std::endl;
      else             outp << resMessage.c_str() << std::endl;
    
      m_3dsMaxRenderLog->LogMessage(a_messageSource, a_messageType, 0, resMessage.c_str());
    }

    template <class T>
    void PrintValue (IRMM_S a_messageSource, IRMM_T a_messageType, std::wstring a_message, const T& a_x)
    {       
      std::wstring resMessage = a_message + L": " + std::to_wstring(a_x);
      Print(a_messageSource, a_messageType, resMessage);
    }

    std::wofstream& Stream() 
    {
      if (isMtlRender)
        return outm;
      else
        return outp; 
    }

		void CloseLog()
		{
			outp.close();
      outm.close();
		}

    void flush() { outp.flush(); outm.flush(); }
    void ClearDisplayedMessages() { m_3dsMaxRenderLog->ClearDisplayedMessages(); }

private:
  std::wofstream outp;
  std::wofstream outm;
  IRenderMessageManager* m_3dsMaxRenderLog;
};

