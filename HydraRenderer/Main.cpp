#include "HydraRenderer.h"
#include <notify.h>

///////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;


int HydraRenderPlugin::Open(INode *scene, INode *vnode, ViewParams* viewPar, RendParams& rpar, HWND hwnd, DefaultLight* defaultLights, int numDefLights, RendProgressCallback* m_prog)
{
  g_hrPlugResMangr.m_pluginLog.OpenLogFile(HYDRA_LOGS_PATH / L"plugin_log.txt");
  g_hrPlugResMangr.m_pluginLog.OpenLogFileM(HYDRA_LOGS_PATH / L"material_log.txt");

  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"Open.");

  m_nStaticFrame = GetCOREInterface()->GetTime();

  // Important!! This has to be done here in MAX Release 2!
  // Also enable it again in Renderer::Close()
  GetCOREInterface()->DisableSceneRedraw();

  if (!rpar.inMtlEdit)
    BroadcastNotification(NOTIFY_PRE_RENDER, static_cast<void*>(&rpar));	// 
  else
  {
    GetCOREInterface()->SetMEditRendererLocked(false);
    GetCOREInterface()->AssignMEditRenderer(GetCOREInterface()->CreateDefaultScanlineRenderer());
  }

  // Get options from RenderParams
  // These are the options that are common to all renderers
  m_rendParams.bVideoColorCheck = rpar.colorCheck;
  m_rendParams.bForce2Sided     = rpar.force2Side;
  m_rendParams.bRenderHidden    = rpar.rendHidden;
  m_rendParams.bSuperBlack      = rpar.superBlack;
  m_rendParams.bRenderFields    = rpar.fieldRender;
  m_rendParams.bNetRender       = rpar.isNetRender;
  m_rendParams.rendType         = rpar.rendType;

  m_rendParams.inMtlEdit = rpar.inMtlEdit;

  // Default lights
  m_rendParams.nNumDefLights  = numDefLights;
  m_rendParams.pDefaultLights = defaultLights;

  // Support Render effects
  m_rendParams.effect = rpar.effect;

  m_rendParams.devWidth  = rpar.width;
  m_rendParams.devHeight = rpar.height;

  m_rendParams.nMinx     = 0;
  m_rendParams.nMiny     = 0;
  m_rendParams.nMaxx     = m_rendParams.devWidth;
  m_rendParams.nMaxy     = m_rendParams.devHeight;

  m_pScene = scene;
  m_pVnode = vnode;

  if (viewPar)
    m_view = *viewPar;


  //Animation
  m_rendParams.rendTimeType = GetCOREInterface()->GetRendTimeType();
  m_rendParams.rendStart    = GetCOREInterface()->GetRendStart();
  m_rendParams.rendEnd      = GetCOREInterface()->GetRendEnd();
  m_rendParams.nthFrame     = GetCOREInterface()->GetRendNThFrame();
  m_rendParams.rendSaveFile = GetCOREInterface()->GetRendSaveFile();


  m_theView.pRendParams = &m_rendParams;

  BeginThings();  /// call RenderBegin() to switch all node and modifiers to render mode.


  //if (m_rendParams.rendSaveFile)
  //{
  //  BitmapInfo bi = GetCOREInterface()->GetRendFileBI();
  //  g_hrPlugResMangr.m_pluginLog.PrintValue(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Info, L"Rendered Image path", bi.Filename());
  //}

  return 1;
}



int HydraRenderPlugin::Render(TimeValue t, Bitmap* tobm, FrameRendParams& frp, HWND hwnd, RendProgressCallback* m_prog, ViewParams* viewPar)
{
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"Render.");

  if (m_prog) // strange case of 3ds max when it call Render multiple times and immediately abort it.
  {
    auto res = m_prog->Progress(0, 100);
    m_prog->SetTitle(_T("Preparing to render..."));
    if (res == RENDPROG_ABORT)
      return 1;
  }

  if (m_currentScene.id < 0)  
    m_currentScene = hrSceneCreate(L"my scene");
    
  g_hrPlugResMangr.m_pluginLog.isMtlRender = m_rendParams.inMtlEdit;
  m_rendParams.estimateLDR                 = m_rendParams.debugCheckBoxes[L"ESTIMATE_IN_LDR"];

  if (!tobm)
  {
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, L"No output bitmap.");
    return 0;
  }
  
  // Select device

  if (m_rendParams.device_id.empty())
  {
    m_rendParams.device_id     = HydraRenderPlugin::m_lastRendParams.device_id;
    m_rendParams.engine_type   = HydraRenderPlugin::m_lastRendParams.engine_type;
    m_rendParams.liteMode      = HydraRenderPlugin::m_lastRendParams.liteMode;

    if (m_rendParams.device_id.empty())
    {
      UserSelectDeviceDialog(hwnd);
      m_rendParams.device_id   = HydraRenderPlugin::m_lastRendParams.device_id;
      m_rendParams.engine_type = HydraRenderPlugin::m_lastRendParams.engine_type;
      m_rendParams.liteMode    = HydraRenderPlugin::m_lastRendParams.liteMode;
    }

    if (m_rendParams.device_id.empty())
    {
      MessageBoxA(nullptr, "Please select at least one rendering device before render.", "Error", MB_OK);
      return 0;
    }
  }

  // Pre-eval notification needs to be sent before scene nodes are evaluated, and called again whenever the time changes.    
  TimeValue eval_time = t;
  BroadcastNotification(NOTIFY_RENDER_PREEVAL, &eval_time);

  SetRenderParamsImageSizeFromBitMap(&m_rendParams, tobm);

  m_rendParams.progCallback = m_prog;
  m_rendParams.time         = t;
  m_rendParams.pFrp         = &frp;

  if (viewPar)
    m_view = *viewPar;

  ////////////////////////////////

  g_hrPlugResMangr.m_rendRef = SetupRender(frp);

  ////////////////////////////////
  // Export data

  DoExport(m_pScene, m_pVnode, t);

  ////////////////////////////////


  if (this->m_rendParams.debugCheckBoxes[L"BLACK_IMAGE"] || m_compressMeshOnExport)
  {
    hrFlush(m_currentScene); // export scene, dont render it actually
    //g_hrPlugResMangr.m_firstRender = false;
    return 1;
  }

  const int renderWidth  = m_rendParams.nMaxx - m_rendParams.nMinx;
  const int renderHeight = m_rendParams.nMaxy - m_rendParams.nMiny;

  if (!VerifyThings(renderWidth, renderHeight))
    return 0;

  if (!m_rendParams.inMtlEdit && m_prog)
  {
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"Waiting for hydra.exe to start rendering ...");
    m_prog->SetTitle(_M("Waiting for hydra.exe to start rendering ..."));
  }

  BroadcastNotification(NOTIFY_PRE_RENDERFRAME, (void*)(RenderGlobalContext*)&m_rendParams);

  hrFlush(m_currentScene, g_hrPlugResMangr.m_rendRef);

  if (m_rendParams.postProcOn || m_rendParams.denoiserOn)
  {
    hrFBIResize(g_hrPlugResMangr.m_hydraRawRender, renderWidth, renderHeight);
    hrFBIResize(g_hrPlugResMangr.m_hydraOutRender, renderWidth, renderHeight);
  }

  //Bitmap* bmapOut = tobm;
  FrameBufferUpdateLoop(tobm /*bmapOut*/, renderWidth, renderHeight, m_prog, false);

  SaveRenderElementsToDisk(m_pLastRender);

  hrRenderCommand(g_hrPlugResMangr.m_rendRef, L"exitnow");

  // please don't clear device id list! it is needed further when material editor is opened. 
  // m_rendParams.device_id.clear(); 

  BroadcastNotification(NOTIFY_POST_RENDERFRAME, (void*)(RenderGlobalContext*)&m_rendParams);

  return 1;
}



void HydraRenderPlugin::Close(HWND hwnd, RendProgressCallback* m_prog)
{
  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"Close.");

  EndThings(); /// call RenderEnd() to switch all node and modifiers to viewport mode.


  if (m_pLastRender != nullptr) // final update framebuffer image if cancel render.
  {
    if (m_rendParams.denoiserOn) DoDenoise(m_pLastRender );
    if (m_rendParams.postProcOn) DoPostProc(m_pLastRender, !m_rendParams.denoiserOn);
    m_pLastRender->RefreshWindow();
  }

  if (!m_rendParams.inMtlEdit)
    BroadcastNotification(NOTIFY_POST_RENDER);

  GetCOREInterface()->EnableSceneRedraw();

  if (m_rendParams.enableLog)
    ShellExecuteW(NULL, NULL, HYDRA_LOGS_PATH.c_str(), NULL, NULL, SW_SHOWMINNOACTIVE);  

  g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Progress, L"----------------------------------\n");
  g_hrPlugResMangr.m_pluginLog.flush();
  g_hrPlugResMangr.m_pluginLog.CloseLog();
}
