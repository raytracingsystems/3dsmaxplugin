#include "HydraRenderer.h"
#include <maxapi.h>
#include <Scene/IPhysicalCamera.h>
#include <icustattribcontainer.h>
#include <maxscript/mxsplugin/mxsCustomAttributes.h>

////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;

using MaxSDK::IPhysicalCamera;


inline float hordToSectorScale(float fov, float aspect)
{
  float angle        = fov;
  float sectorLength = angle*PI/180.0F;
  float hordLength   = 2.0f*sin(DEG_TO_RAD*angle*0.5f);
  float hordToSector = hordLength/sectorLength;
  float magicScale   = aspect/hordToSector;
  return magicScale*fov;
}


inline float tanScale(float fov, float aspect)
{
  //float x = tan(fov*DEG_TO_RAD);
  //return atan(x*aspect)*RAD_TO_DEG;

  return 2.0f*atan(tan(fov*DEG_TO_RAD / 2.0f)*aspect)*RAD_TO_DEG;
}

HRCameraRef HydraRenderPlugin::ExtractCameraFromActiveViewport(INode *vnode, TimeValue t)
{  
  Point3 camUpVector(0.0F, 1.0F, 0.0F); //Oh Gods, purge 3ds max from existence

#ifdef MAX2022
  Matrix3 posMatrix, targetMatrix;
#else
  Matrix3 posMatrix(1), targetMatrix(1);  
#endif // MAX2022

  Point3 camDirPoint(0.0F, 0.0F, 0.0F);
  std::wstring camName = L"myCamera";

  float fov            = 30.0F;
  float nearClipPlane  = 0.01F;
  float farClipPlane   = 100.0F;
  //float tdist        = 0.0F;
  bool isTargeted      = false;

  Point2 tilt(0.0F, 0.0F);
  Point2 shift(0.0F, 0.0F);

  Matrix3 mConv;
  mConv = GetConversionMatrix();

  //Samsung custom parameters init

  bool  onSmsngCustParam           = false;
  std::filesystem::path lensFile   = L"";
  std::filesystem::path sensorFile = L"";
  int   useGpu                     = -1;
  int   lensFlare                  = -1;
  int   skipSensor                 = -1;
  int   threadsNum                 = -1;
  int   irisNumber                 = -1;
  int   irisRotation               = -1;
  float exposureMult               = -1;
  Color rgbWavelengths;


  if (vnode != nullptr)
  {
    ObjectState os = vnode->EvalWorldState(t);

    if (os.obj != nullptr && os.obj->SuperClassID() == CAMERA_CLASS_ID)
    {   
      Interval ivalid;
      ivalid.SetInfinite();
      auto* const camera = static_cast<CameraObject*>(os.obj);

      //INode* targetNode = (camera->ClassID().PartA() == LOOKAT_CAM_CLASS_ID) ? vnode->GetTarget() : NULL;
      INode* targetNode = vnode->GetTarget();
      camName           = strConverter::c2ws(vnode->GetName());

      Matrix3 debug     = vnode->GetNodeTM(t);

      if (targetNode != nullptr)
      {
        targetMatrix = ToMetersM3(targetNode->GetObjectTM(t));
        isTargeted   = true;
      }
      else
        camDirPoint = - vnode->GetNodeTM(t).GetRow(2);

      camUpVector = ::Normalize(vnode->GetNodeTM(t).GetRow(1)*mConv);

      if (::Length(camUpVector) < 1e-5f)
        camUpVector = Point3(0.0F, 1.0F, 0.0F);


      posMatrix = ToMetersM3(vnode->GetObjectTM(t));
      fov       = camera->GetFOV(t, ivalid)*RAD_TO_DEG; 
      // tdist  = ToMeters(camera->GetTDist(t, ivalid));


      //Samsung custom parameters
      const auto custAtrCont = camera->GetCustAttribContainer();
      if (custAtrCont)
      {
        for (int i = 0; i < custAtrCont->GetNumCustAttribs(); ++i)
        {
          const auto ca = custAtrCont->GetCustAttrib(i);

          if (ca == NULL)
            continue;

          const auto pb2 = ca->GetParamBlock(0);

          if (pb2 == NULL)
            continue;

          onSmsngCustParam = FindInt(L"onSmsngCustParam", ca);

          if (onSmsngCustParam)
          {
            lensFile       = FindString (L"lensFilePath"  , ca);
            sensorFile     = FindString (L"sensorFilePath", ca);
            useGpu         = FindInt    (L"useGpu"        , ca);
            lensFlare      = FindInt    (L"lensFlare"     , ca);
            skipSensor     = FindInt    (L"skipSensor"    , ca);
            threadsNum     = FindInt    (L"threadsNum"    , ca);
            irisNumber     = FindInt    (L"irisNumber"    , ca);
            irisRotation   = FindInt    (L"irisRotation"  , ca);
            exposureMult   = FindFloat  (L"exposureMult"  , ca); 
            rgbWavelengths = { FindFloat(L"rWavelengths"  , ca),
                               FindFloat(L"gWavelengths"  , ca),
                               FindFloat(L"bWavelengths"  , ca) };

            if (lensFile.empty())
              g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Warning, L"The Lens File is not specified in the user parameters of the camera.");

            if (sensorFile.empty())
              g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Warning, L"The Sensor File is not specified in the user parameters of the camera.");

          }
        }
      }


      // export PhysicalCamera parameters if such camera is used
      const IPhysicalCamera* const physicalCamera = dynamic_cast<IPhysicalCamera*>(os.obj);
      if (physicalCamera != nullptr)
      {
        tilt      = physicalCamera->GetTiltCorrection(t, ivalid);
        shift     = physicalCamera->GetFilmPlaneOffset(t, ivalid);
      }      
    }
  }
  else
  {
    ViewExp& viewExp = *GetCOREInterface16()->GetCurrentRenderView(); // get "m_view to render" from render settings, for support lock m_view/camera to render.

    camName          = L"maxViewPort2";
    fov              = viewExp.GetFOV()*RAD_TO_DEG;
    Matrix3 aTM, coordSysTM;
    viewExp.GetAffineTM(aTM);
    coordSysTM       = Inverse(aTM);

    camUpVector      = ::Normalize(coordSysTM.GetRow(1)*mConv);

    if (::Length(camUpVector) < 1e-5F)
      camUpVector    = Point3(0.0F, 1.0F, 0.0F);

    Point3 viewDir, viewPos;
    viewDir          = ::Normalize(coordSysTM.GetRow(2));
    viewPos          = ToMetersP3(coordSysTM.GetRow(3));
    camDirPoint      = -viewDir;
    isTargeted       = false;

    posMatrix.SetRow(3, viewPos);
  }

  const int renderWidth  = m_rendParams.nMaxx - m_rendParams.nMinx;
  const int renderHeight = m_rendParams.nMaxy - m_rendParams.nMiny;

  if (renderWidth != renderHeight) // it seems that when this is happend 3ds max assume spherical screen, not planar
  {
    float aspect = float(renderHeight) / float(renderWidth);
    //fov = hordToSectorScale(cam->fov, aspect); 
    fov = tanScale(fov, aspect);
  }

  HR_OPEN_MODE mode;
  if (m_camRef.id < 0)//g_hrPlugResMangr.m_firstRender)
  {
    mode = HR_WRITE_DISCARD;
    m_camRef = hrCameraCreate(camName.c_str());
  }
  else
    mode = HR_OPEN_EXISTING;

  hrCameraOpen(m_camRef, mode);
  {
    auto camNode = hrCameraParamNode(m_camRef);

    // Samsung custom parameters export
    if (onSmsngCustParam)
    {       
      camNode.force_attribute(L"integrator_iters").set_value(16);
      camNode.force_attribute(L"cpu_plugin"      ).set_value(2);
      camNode.force_attribute(L"cpu_plugin_dll"  ).set_value(L"VirtualCamera.dll");
      camNode.force_child    (L"lensFile"        ).text() = lensFile.c_str();
      camNode.force_child    (L"sensorFile"      ).text() = sensorFile.c_str();//The sensor file. From here the resolution and diagonal are taken, the rest is ignored for now.
      camNode.force_child    (L"useGpu"          ).text() = useGpu;
      camNode.force_child    (L"lensFlare"       ).text() = lensFlare;
      camNode.force_child    (L"skipSensor"      ).text() = skipSensor; //while we skip image generation through the sensor, there are a lot of parameters... and it's not necessary yet.
      camNode.force_child    (L"threadsNum"      ).text() = threadsNum;
      camNode.force_child    (L"irisNumber"      ).text() = irisNumber;
      camNode.force_child    (L"irisRotation"    ).text() = irisRotation;      
      camNode.force_child    (L"exposureMult"    ).text() = exposureMult;
      camNode.force_child    (L"rgbWavelengths"  ).text() = strConverter::colorToWideString(rgbWavelengths).c_str();
      camNode.force_child    (L"apertureDiameter").text() = 12; //the diameter of the circumscribed circle around the aperture overrides the value from the lensFile only in the decreasing direction (hello PBRT).
    }
    else
    {
      if (camNode.attribute(L"integrator_iters")) camNode.remove_attribute(L"integrator_iters");
      if (camNode.attribute(L"cpu_plugin"      )) camNode.remove_attribute(L"cpu_plugin"      );
      if (camNode.attribute(L"cpu_plugin_dll"  )) camNode.remove_attribute(L"cpu_plugin_dll"  );
      if (camNode.child    (L"lensFile"        )) camNode.remove_child    (L"lensFile"        );
      if (camNode.child    (L"sensorFile"      )) camNode.remove_child    (L"sensorFile"      );
      if (camNode.child    (L"useGpu"          )) camNode.remove_child    (L"useGpu"          );
      if (camNode.child    (L"lensFlare"       )) camNode.remove_child    (L"lensFlare"       );
      if (camNode.child    (L"skipSensor"      )) camNode.remove_child    (L"skipSensor"      );
      if (camNode.child    (L"threadsNum"      )) camNode.remove_child    (L"threadsNum"      );
      if (camNode.child    (L"irisNumber"      )) camNode.remove_child    (L"irisNumber"      );
      if (camNode.child    (L"irisRotation"    )) camNode.remove_child    (L"irisRotation"    );
      if (camNode.child    (L"rgbWavelengths"  )) camNode.remove_child    (L"rgbWavelengths"  );
      if (camNode.child    (L"apertureDiameter")) camNode.remove_child    (L"apertureDiameter");
    }

    camNode.force_child(L"fov"            ).text().set(fov);
    camNode.force_child(L"nearClipPlane"  ).text().set(nearClipPlane);
    camNode.force_child(L"farClipPlane"   ).text().set(farClipPlane);
    camNode.force_child(L"enable_dof"     ).text() = int(m_rendParams.enableDOF);
    camNode.force_child(L"dof_lens_radius").text() = m_rendParams.lensradius*0.01F;

    std::wstringstream ss;

    ss << camUpVector.x << " " << camUpVector.y << " " << camUpVector.z;
    camNode.force_child(L"up").text().set(ss.str().c_str());
    ss.str(std::wstring());
    ss.clear();

    Point3 maxPos, maxPosT;
    maxPos  = Point3(0, 0, 0) * posMatrix;
    maxPosT = Point3(0, 0, 0) * targetMatrix;

    Point3 pos, target, dir;

    pos     = maxPos  * mConv;
    target  = maxPosT * mConv;
    dir     = (camDirPoint.Normalize())*mConv;

    if (!isTargeted)
      target = pos + 100.0f*dir;

    ss << pos.x << " " << pos.y << " " << pos.z;
    camNode.force_child(L"position").text().set(ss.str().c_str());
    ss.str(std::wstring());
    ss.clear();

    ss << target.x << " " << target.y << " " << target.z;
    camNode.force_child(L"look_at").text().set(ss.str().c_str());
    ss.str(std::wstring());
    ss.clear();

    camNode.force_child(L"tiltRotX"  ).text().set(tilt.x);
    camNode.force_child(L"tiltRotY"  ).text().set(tilt.y);
    camNode.force_child(L"tiltShiftX").text().set(shift.x);
    camNode.force_child(L"tiltShiftY").text().set(shift.y);
  }
  hrCameraClose(m_camRef);

//  TraceNodeCustomProperties(L"mycamera", vnode, 1);

  return m_camRef;
}

