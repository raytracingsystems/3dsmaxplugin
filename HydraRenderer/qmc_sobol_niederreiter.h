#pragma once
#include <basetsd.h> // for INT64

#define QRNG_DIMENSIONS 3
#define QRNG_RESOLUTION 31
#define INT_SCALE (1.0f / (float)0x80000001U)


extern "C" void initQuasirandomGenerator(unsigned int table[QRNG_DIMENSIONS][QRNG_RESOLUTION]);

float rndQmcSobolN(unsigned int pos, int dim, const unsigned int *c_Table);
