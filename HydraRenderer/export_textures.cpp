#include "HydraRenderer.h"

#include <unordered_map>
#include <thread>
#include <codecvt>
#include <mutex>
#include "gamma.h"

/////////////////////////////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;


struct translateTexmap
{
  Texmap* texmap;
};

std::mutex mtx;

bool g_inMatEditor = false;


void hermiteSplinePart(Point2 start, Point2 end, Point2 tangent1, Point2 tangent2, std::vector<Point2> &f)
{
  int steps = 10;
  for (int t = 0; t < steps; t++)
  {
    float s = (float)t / (float)steps;    // scale s to go from 0 to 1
    float h1 = 2 * pow(s, 3) - 3 * pow(s, 2) + 1;          // calculate basis function 1
    float h2 = -2 * pow(s, 3) + 3 * pow(s, 2);              // calculate basis function 2
    float h3 = pow(s, 3) - 2 * pow(s, 2) + s;         // calculate basis function 3
    float h4 = pow(s, 3) - pow(s, 2);              // calculate basis function 4

    Point2 res;
    res.y = h1 * start.y + h2 * end.y + h3 * tangent1.y + h4 * tangent2.y;
    res.x = h1 * start.x + h2 * end.x + h3 * tangent1.x + h4 * tangent2.x;
    f.push_back(res);
    /*
    vector p = h1*P1 +                    // multiply and sum all funtions
    h2*P2 +                    // together to build the interpolated
    h3*T1 +                    // point along the curve.
    h4*T2;
    */
  }
}

bool OutputNodeDoesNothing(BitmapTex* pBitMap, TimeValue t)
{
  if (pBitMap == nullptr)
    return true;

  TextureOutput* texout = pBitMap->GetTexout();

  if (texout == nullptr)
    return true;

  float outpAmt = texout->GetOutputLevel(t);

  StdTexoutGen* ptexOutGen = dynamic_cast<StdTexoutGen*>(texout);
  if (ptexOutGen != nullptr)
    outpAmt *= ptexOutGen->GetOutAmt(t);

  AColor colorFromOutput;
  float  monoFromOutput;
  bool colorChanged = false;

  for (float val = 0.0F; val <= 1.0F; val += 0.1F) //-V1034
  {
    colorFromOutput = texout->Filter(AColor(val, val, val));
    monoFromOutput  = texout->Filter(val);

    if ((fabs(colorFromOutput.r - val) > 1e-5f) ||
        (fabs(colorFromOutput.g - val) > 1e-5f) ||
        (fabs(colorFromOutput.b - val) > 1e-5f) ||
        (fabs(monoFromOutput - val) > 1e-5f))
    {
      colorChanged = true;
      break;
    }
  }

  return (fabs(outpAmt - 1.0F) < 1e-5F) && !texout->GetInvert() && !colorChanged;
}

std::filesystem::path AlteredTexPath(Texmap* tex, bool needHDR, const std::wstring& mtl_name)
{
  if (tex == nullptr)
    return std::wstring(L"");

  MSTR mapName1 = tex->GetName();
  std::wstring mapName_w = strConverter::c2ws(mapName1.data());

  if (needHDR) return HYDRA_TEMP_PATH / (mapName_w + L"-" + mtl_name + L".hdr");
  else         return HYDRA_TEMP_PATH / (mapName_w + L"-" + mtl_name + L".png");
}

Bitmap* CreateBitMapCopyWithAppliedAnyFilter(BitmapTex* a_pBitMap, const std::filesystem::path& a_mapPath, const bool a_invertHeightMap,
  const bool a_alphaAsGray, const TimeValue a_t)
{
  if (a_pBitMap == nullptr)
    return nullptr;

  Bitmap* bmap_src = a_pBitMap->GetBitmap(a_t);
  if (bmap_src == nullptr)
    return nullptr;

  TextureOutput* texout = a_pBitMap->GetTexout();
  if (texout == nullptr)
    return nullptr;

  const auto src_w = (WORD)bmap_src->Width();
  const auto src_h = (WORD)bmap_src->Height();

  //std::lock_guard<std::mutex> lck(mtx);

  BitmapInfo bi_out;
  bi_out.SetType(BMM_FLOAT_RGBA_32);
  bi_out.SetWidth(src_w);
  bi_out.SetHeight(src_h);
  bi_out.SetPath(a_mapPath.c_str());

  bi_out.SetCustomGamma(bmap_src->Gamma());
  bi_out.SetGamma(bmap_src->Gamma());

  Bitmap* bmap_out = nullptr;
  bmap_out = TheManager->Create(&bi_out);
  bmap_out->Fill(0, 0, 0, 0);


  // parallelization of this block is not effective, perhaps because the text->Filter() function is single-threaded.
  for (int i = 0; i < src_h; ++i)
  {
    std::vector<BMM_Color_fl> line(src_w);
    std::vector<BMM_Color_fl> line_filtered(src_w);

    bmap_src->GetPixels(0, i, src_w, &line[0]);

    for (int j = 0; j < src_w; j++)
    {
      line_filtered[j] = texout->Filter(line[j]);

      if (a_alphaAsGray)
      {
        line_filtered[j].r = line_filtered[j].a;
        line_filtered[j].g = line_filtered[j].a;
        line_filtered[j].b = line_filtered[j].a;
      }

      if (a_invertHeightMap)
      {
        line_filtered[j].r = line_filtered[j].r * (-1.0F) + 1.0F;
        line_filtered[j].g = line_filtered[j].g * (-1.0F) + 1.0F;
        line_filtered[j].b = line_filtered[j].b * (-1.0F) + 1.0F;
      }
    }
    bmap_out->PutPixels(0, i, src_w, &line_filtered[0]);
  }

  bmap_out->OpenOutput(&bi_out);
  bmap_out->Write(&bi_out);
  bmap_out->Close(&bi_out);

  return bmap_out;
}


void HydraRenderPlugin::CropToFileHDR(BitmapTex* pBitMap, Bitmap *bmap, const std::filesystem::path& mapPath, TimeValue t)
{
  if (bmap == nullptr || pBitMap == nullptr)
    return;

  bool applyTexCrop = FindBool(L"Apply", pBitMap);
  bool place        = FindInt(L"CropPlace", pBitMap);

  float clipu       = FindFloat(L"Clip U Offset", pBitMap);
  float clipv       = FindFloat(L"Clip V Offset", pBitMap);
  float clipw       = FindFloat(L"Clip U Width", pBitMap);
  float cliph       = FindFloat(L"Clip V Width", pBitMap);

  Bitmap *bmap2;
  BitmapInfo bi;

  int w = bmap->Width();
  int h = bmap->Height();

  int x0, y0, nw, nh;
  nw = int(float(w)*clipw);
  nh = int(float(h)*cliph);
  x0 = int(float(w - 1)*clipu);
  y0 = int(float(h - 1)*clipv);

  if (place)
  {
    std::lock_guard<std::mutex> lck(mtx);
    bi.SetType(BMM_FLOAT_RGBA_32);
    bi.SetWidth(w);
    bi.SetHeight(h);
    bi.SetPath(mapPath.c_str());
    bmap2 = TheManager->Create(&bi);

    if (bmap2 != nullptr)
    {
      bmap2->Fill(0, 0, 0, 0);

      if (nw < 1) nw = 1;
      if (nh < 1) nh = 1;

      Bitmap* tmpBM = nullptr;
      BitmapInfo bi2;
      bi2.SetName(_T("temp"));
      bi.SetType(BMM_FLOAT_RGBA_32);
      bi2.SetWidth(nw);
      bi2.SetHeight(nh);
      tmpBM = TheManager->Create(&bi2);
      if (tmpBM != nullptr)
      {
        tmpBM->CopyImage(bmap, COPY_IMAGE_RESIZE_HI_QUALITY, 0);

#pragma omp parallel for num_threads(omp_get_num_procs())
        for (int y = 0; y < nh; y++)
        {
          PixelBufFloat row(nw);
          BMM_Color_fl*  p1 = row.Ptr();
          tmpBM->GetLinearPixels(0, y, nw, p1);
          bmap2->PutPixels(x0, y + y0, nw, p1);
        }
        tmpBM->DeleteThis();
      }
    }
  }
  else
  {
    if (nw < 1) nw = 1;
    if (nh < 1) nh = 1;
    {
      std::lock_guard<std::mutex> lck(mtx);
      bi.SetType(BMM_FLOAT_RGBA_32);
      bi.SetWidth(nw);
      bi.SetHeight(nh);
      bi.SetPath(mapPath.c_str());

      bmap2 = TheManager->Create(&bi);

      if (bmap2 != nullptr)
      {
#pragma omp parallel for num_threads(omp_get_num_procs())
        for (int y = 0; y < nh; y++)
        {
          PixelBufFloat row(nw);
          BMM_Color_fl*  p1 = row.Ptr();
          bmap->GetLinearPixels(x0, y + y0, nw, p1);
          bmap2->PutPixels(0, y, nw, p1);
        }
      }
    }
  }

  bmap2->OpenOutput(&bi);
  bmap2->Write(&bi); 
  bmap2->Close(&bi);
  bmap2->DeleteThis();
}

void SaveTexureInFileWithLowRes(const std::filesystem::path& mapPath, Bitmap* a_img)
{
  if (a_img == nullptr)
    return;

  int w = a_img->Width();
  int h = a_img->Height();

  do
  {
    w = w / 2;
    h = h / 2;

  } while (w > 1024 || h > 1024);

  BitmapInfo bi;

  if (a_img->IsHighDynamicRange())
    bi.SetType(BMM_REALPIX_32);
  else
    bi.SetType(BMM_TRUE_64);

  bi.SetFlags(a_img->Flags());


  bi.SetName(mapPath.c_str());
  bi.SetPath(mapPath.c_str());
  bi.SetWidth(w);
  bi.SetHeight(h);

  Bitmap *bmap2;
  bmap2 = TheManager->Create(&bi);

  if (bmap2 != nullptr)
  {
    bmap2->CopyImage(a_img, COPY_IMAGE_RESIZE_HI_QUALITY, BMM_Color_fl(0, 0, 0, 0), &bi);
    bmap2->OpenOutput(&bi);
    bmap2->Write(&bi);
    bmap2->Close(&bi);
    bmap2->DeleteThis();
  }

}

void HydraRenderPlugin::ExtractTextureMatrixFromUVGen(StdUVGen* a_uvGen, std::vector<float>& a_matrixData, int& a_pTexTilingFlags, bool & a_hasScreenMapping)
{
  TimeValue t = GetStaticFrame();

  float uOffset;
  float vOffset;
  float uTiling;
  float vTiling;
  //float angle;
  float angleUVW[3];

#ifdef MAX2022
  Matrix3 rotU, rotV, rotW;
  Matrix3 scale, trans;
  Matrix3 moveToCenter, moveToCenterInv;
#else
  Matrix3 rotU(1), rotV(1), rotW(1);
  Matrix3 scale(1), trans(1);
  Matrix3 moveToCenter(1), moveToCenterInv(1);
#endif // MAX2022


  if (a_uvGen != nullptr && !a_uvGen->GetUseRealWorldScale())
  {
    moveToCenter.SetTrans(Point3(0.5F, 0.5F, 0.0F));
    moveToCenterInv.SetTrans(Point3(-0.5F, -0.5F, 0.0F));
  }

  if (a_uvGen != nullptr)
  {
    uOffset = a_uvGen->GetUOffs(t);
    vOffset = a_uvGen->GetVOffs(t);
    uTiling = a_uvGen->GetUScl(t);
    vTiling = a_uvGen->GetVScl(t);
    //angle   = a_uvGen->GetAng(t);

    angleUVW[0] = a_uvGen->GetUAng(t);
    angleUVW[1] = a_uvGen->GetVAng(t);
    angleUVW[2] = a_uvGen->GetWAng(t);

    a_pTexTilingFlags = a_uvGen->GetTextureTiling();

    if (a_uvGen->GetSlotType() == MAPSLOT_ENVIRON)
      a_hasScreenMapping = (a_uvGen->GetCoordMapping(0) == UVMAP_SCREEN_ENV);
  }
  else
  {
    uOffset = 0;
    vOffset = 0;
    uTiling = 1;
    vTiling = 1;
    //angle = 0;

    angleUVW[0] = 0;
    angleUVW[1] = 0;
    angleUVW[2] = 0;

    a_pTexTilingFlags = U_WRAP | V_WRAP;
  }

  rotU.RotateX(angleUVW[0]);
  rotV.RotateY(angleUVW[1]);
  rotW.RotateZ(angleUVW[2]);

  scale.SetScale(Point3(uTiling, vTiling, 1.0F));

  if (uOffset < 0.0F || uOffset > 1.0F)
    uOffset -= (int)uOffset;

  trans.SetTrans(Point3(-uOffset, -vOffset, 0.0F));

  scale = moveToCenterInv * (rotU*rotV*rotW*scale)*moveToCenter;

  Matrix3 texMatrix;
  texMatrix = trans * scale;

  MaxMatrix3ToFloat16V2(texMatrix, &a_matrixData[0]);
}

std::filesystem::path GetCorrectTexPath(const wchar_t* a_name, IFileResolutionManager* pFRM)
{
  std::string texPath;

  if (pFRM != nullptr)
  {
    MaxSDK::Util::Path path(a_name);

    if (pFRM->GetFullFilePath(path, MaxSDK::AssetManagement::kBitmapAsset))
      texPath = strConverter::WstringToUtf8(strConverter::c2ws(path.GetCStr()));
    else
      texPath = strConverter::WstringToUtf8(a_name);
  }
  else
    texPath = strConverter::WstringToUtf8(a_name);

  return texPath;
}

std::filesystem::path GetCorrectTexPath2(const wchar_t* a_name, IFileResolutionManager* pFRM)
{
  std::filesystem::path texPath;

  if (pFRM != nullptr)
  {
    MaxSDK::Util::Path path(a_name);

    if (pFRM->GetFullFilePath(path, MaxSDK::AssetManagement::kBitmapAsset))
      texPath = strConverter::c2ws(path.GetCStr());
    else
      texPath = a_name;
  }
  else
    texPath = a_name;

  return texPath;
}


//////////////////////

texInfo HydraRenderPlugin::BakeBitmap(Texmap* tex, HRTextureNodeRef currentRef, const bool invertHeightMap, const float bumpHeight, const std::wstring& a_mtlName = std::wstring(L""), bool dontupdate)
{
  TimeValue t = GetStaticFrame();

  BitmapTex* pBitmapTex = dynamic_cast<BitmapTex*>(tex);
  if (pBitmapTex == nullptr)
    return texInfo(HRTextureNodeRef(), nullptr, 1.0F, L"alpha");


  IFileResolutionManager* pFRM  = IFileResolutionManager::GetInstance();
  std::wstring tempStr          = strConverter::c2ws(pBitmapTex->GetMapName());
  std::filesystem::path mapPath = GetCorrectTexPath2(tempStr.c_str(), pFRM);

  //const TextureOutput* texout   = pBitmapTex->GetTexout();
  Bitmap *bmap_src              = pBitmapTex->GetBitmap(t);
  Bitmap *bmap_out              = nullptr;


  if (bmap_src != nullptr)
  {
    bool copyWasCreated         = false;
    const bool isHDR            = bmap_src->IsHighDynamicRange();
    const bool applyTexCrop     = FindBool(L"Apply", pBitmapTex);
    const bool alphaAsGray      = (FindInt(L"rgbOutput", pBitmapTex) == 1);


    if (!OutputNodeDoesNothing(pBitmapTex, t) || invertHeightMap || alphaAsGray)
    {
      mapPath                   = AlteredTexPath(tex, isHDR, a_mtlName);
      bmap_out                  = CreateBitMapCopyWithAppliedAnyFilter(pBitmapTex, mapPath, invertHeightMap, alphaAsGray, t);
      copyWasCreated            = true;
    }
    else
    {
      bmap_out                  = bmap_src;
      copyWasCreated            = false;
    }

    if (applyTexCrop)
    {
      mapPath                   = AlteredTexPath(tex, isHDR, a_mtlName);
      CropToFileHDR(pBitmapTex, bmap_out, mapPath, t);
    }


    // go faster for big textures in material editor with Hydra render in mat editor. In the current implementation there is a scanline.
    //if (bmap_src != nullptr && m_inMtlEditorNow) 
    //{
    //  if (bmap_src->Width() > 1024 || bmap_src->Height() > 1024)
    //  {
    //    mapPath = AlteredTexPath(tex, isHDR, mtl_name);
    //    SaveTexureInFileWithLowRes(mapPath, bmap_out);
    //  }
    //}    

    if (bmap_out != nullptr && copyWasCreated)
      bmap_out->DeleteThis();


    Bitmap* pBitMap2     = pBitmapTex->GetBitmap(t);
    float gamma          = 2.2F;

    if (pBitMap2 != nullptr)
      gamma              = pBitMap2->Gamma();
    else
      gamma              = gammaMgr.GetFileInGamma();

    if (isHDR && copyWasCreated) // if HDR texture was transformed with output node, it gamma is always one because it's linear HDR texture
      gamma              = 1.0F;

    int alphaSourceInt   = FindInt(L"AlphaSource", pBitmapTex);
    //int rgbSourceInt   = FindInt(L"rgbOutput", pBitmapTex);
    //int monoOutput     = FindInt(L"MonoOutput", pBitmapTex);
        

    // if (outColorType == TEX_OUT_ALPHA)
    std::wstring alphaSource = (alphaSourceInt == 0) ? L"alpha" : L"rgb";
    /* else if (outColorType == TEX_OUT_MONO)
       alphaSource = (monoOutput == 1) ? L"alpha" : L"rgb";
     else
       alphaSource = (rgbSourceInt == 1) ? L"alpha" : L"rgb";
       */
    

    const auto mapExtension = mapPath.extension();
    if (mapExtension == L".jpg" || mapExtension == L".bmp" || mapExtension == L".JPG" || mapExtension == L".BMP")
      alphaSource = L"rgb";

    
    HRTextureNodeRef texRef;

    if (pBitMap2->Width() * pBitMap2->Height() > 100000000) // for texture > 10x10K
      texRef = hrTexture2DCreateFromFileDL(mapPath.c_str());
    else
    {
      if (currentRef.id == -1)    
        texRef = hrTexture2DCreateFromFile(mapPath.c_str());    
      else if (!dontupdate)
        texRef = hrTexture2DUpdateFromFile(currentRef, mapPath.c_str());
    }

    return texInfo(texRef, static_cast<StdUVGen*>(tex->GetTheUVGen()), gamma, alphaSource);
  }
  else
    return texInfo(HRTextureNodeRef(), nullptr, 2.2F, L"alpha");
}


texInfo HydraRenderPlugin::BakeNormalMap(Texmap* tex, HRTextureNodeRef currentRef, const bool invertHeightMap, const float bumpHeight)
{
  //int numSubTex = tex->NumSubTexmaps();
  Texmap* subTex = tex->GetSubTexmap(0); //we only support "Normal" slot and ignore "Additional bump"

  if (subTex->ClassID() == HydraBlendedBox_CLASS_ID) // Hydra Blended box map
    subTex = subTex->GetSubTexmap(0);

  return BakeBitmap(subTex, currentRef, invertHeightMap, bumpHeight);
}

void HydraRenderPlugin::ExtractNormalBump(Mtl* mtl, bool &isNormalMap, bool &flipRed, bool &flipGreen, bool &swap)
{
  const int numSubTex = mtl->NumSubTexmaps();
  Texmap* subTex = nullptr;

  const bool invertHeight = FindBool(L"DisplacementInvertHeightState", mtl);


  for (int i = 0; i < numSubTex; ++i)
  {
    std::wstring slotName = strConverter::c2ws(mtl->GetSubTexmapSlotName(i).data());

    if (slotName == L"NormalMap")
    {
      subTex = mtl->GetSubTexmap(i);
      break;
    }
  }

  if (subTex)
  {
    Texmap* subSubTex = subTex->GetSubTexmap(0);
    if (subSubTex != nullptr)
    {
      const MSTR slotName = subTex->GetSubTexmapSlotName(0);
      const std::wstring slotNameData = strConverter::c2ws(slotName.data());

      if (slotNameData == L"Normal")
      {
        isNormalMap = true;
        flipRed = FindBool(L"flip red", subTex);
        flipGreen = FindBool(L"flip green", subTex);
        swap = FindBool(L"swap red green", subTex);

        if (invertHeight)
        {
          flipRed = !flipRed;
          flipGreen = !flipGreen;
        }
      }
    }
  }
}

//////////////////////// Procedural textures ////////////////////////

HRTextureNodeRef HydraRenderPlugin::CreateFalloff()
{
  HRTextureNodeRef texProc = hrTextureCreateAdvanced(L"proc", L"falloff");

  hrTextureNodeOpen(texProc, HR_WRITE_DISCARD);
  {
    pugi::xml_node texNode = hrTextureParamNode(texProc);

    pugi::xml_node code_node = texNode.force_child(L"code");
    code_node.force_attribute(L"file") = L"c:/[Hydra]/pluginFiles/ProceduralTextures/falloff.c";
    code_node.force_attribute(L"main") = L"main";
  }
  hrTextureNodeClose(texProc);

  return texProc;
}

void HydraRenderPlugin::BindingFalloff(pugi::xml_node materialNode, const wchar_t* nameChild, const Color& color1, const Color& color2
/*,HRTextureNodeRef currentRef, HRTextureNodeRef texBitmap1, HRTextureNodeRef texBitmap2*/)
{
  HRTextureNodeRef texProc = CreateFalloff();

  auto texNode             = hrTextureBind(texProc, materialNode, nameChild);

  const int numArg         = 2;
  pugi::xml_node p[numArg];

  for (int i = 0; i < numArg; i++)        texNode.remove_child(L"arg");
  for (int i = 0; i < numArg; i++) p[i] = texNode.append_child(L"arg");

  p[0].force_attribute(L"id")   = 0;
  p[0].force_attribute(L"name") = L"color1";
  p[0].force_attribute(L"type") = L"float3";
  p[0].force_attribute(L"size") = 1;
  p[0].force_attribute(L"val")  = strConverter::colorToWideString(color1).c_str();

  p[1].force_attribute(L"id")   = 1;
  p[1].force_attribute(L"name") = L"color2";
  p[1].force_attribute(L"type") = L"float3";
  p[1].force_attribute(L"size") = 1;
  p[1].force_attribute(L"val")  = strConverter::colorToWideString(color2).c_str();
}

HRTextureNodeRef HydraRenderPlugin::CreateHexaplanar()
{
  HRTextureNodeRef texProc = hrTextureCreateAdvanced(L"proc", L"hexaplanar");

  hrTextureNodeOpen(texProc, HR_WRITE_DISCARD);
  {
    pugi::xml_node texNode = hrTextureParamNode(texProc);

    pugi::xml_node code_node = texNode.force_child(L"code");

    code_node.force_attribute(L"file") = L"c:/[Hydra]/pluginFiles/ProceduralTextures/hexaplanar.c";

    code_node.force_attribute(L"main") = L"main";
  }
  hrTextureNodeClose(texProc);

  return texProc;
}

HRTextureNodeRef HydraRenderPlugin::CreateHexaplanarScalers()
{
  HRTextureNodeRef texProc = hrTextureCreateAdvanced(L"proc", L"hexaplanarScalers");

  hrTextureNodeOpen(texProc, HR_WRITE_DISCARD);
  {
    pugi::xml_node texNode = hrTextureParamNode(texProc);

    pugi::xml_node code_node = texNode.force_child(L"code");
    code_node.force_attribute(L"file") = L"c:/[Hydra]/pluginFiles/ProceduralTextures/hexaplanarScalers.c";
    code_node.force_attribute(L"main") = L"main";
  }
  hrTextureNodeClose(texProc);

  return texProc;
}


void HydraRenderPlugin::BindingHexaplanar(pugi::xml_node materialNode, const HRTextureNodeRef& texX, const HRTextureNodeRef& texY, 
  const HRTextureNodeRef& texZ, const HRTextureNodeRef& texX2, const HRTextureNodeRef& texY2, const HRTextureNodeRef& texZ2, 
  const float blendSize, const float mapScale)
{
  HRTextureNodeRef texProc = CreateHexaplanar();

  auto texNode = hrTextureBind(texProc, materialNode);

  const int numArg = 8;
  pugi::xml_node p[numArg];

  for (int i = 0; i < numArg; i++)        texNode.remove_child(L"arg");
  for (int i = 0; i < numArg; i++) p[i] = texNode.append_child(L"arg");

  p[0].force_attribute(L"id")   = 0;
  p[0].force_attribute(L"name") = L"texX1";
  p[0].force_attribute(L"type") = L"sampler2D";
  p[0].force_attribute(L"size") = 1;
  p[0].force_attribute(L"val")  = texX.id;

  p[1].force_attribute(L"id")   = 1;
  p[1].force_attribute(L"name") = L"texY1";
  p[1].force_attribute(L"type") = L"sampler2D";
  p[1].force_attribute(L"size") = 1;
  p[1].force_attribute(L"val")  = texY.id;

  p[2].force_attribute(L"id")   = 2;
  p[2].force_attribute(L"name") = L"texZ1";
  p[2].force_attribute(L"type") = L"sampler2D";
  p[2].force_attribute(L"size") = 1;
  p[2].force_attribute(L"val")  = texZ.id;

  p[3].force_attribute(L"id")   = 3;
  p[3].force_attribute(L"name") = L"texX2";
  p[3].force_attribute(L"type") = L"sampler2D";
  p[3].force_attribute(L"size") = 1;
  p[3].force_attribute(L"val")  = texX2.id;

  p[4].force_attribute(L"id")   = 4;
  p[4].force_attribute(L"name") = L"texY2";
  p[4].force_attribute(L"type") = L"sampler2D";
  p[4].force_attribute(L"size") = 1;
  p[4].force_attribute(L"val")  = texY2.id;

  p[5].force_attribute(L"id")   = 5;
  p[5].force_attribute(L"name") = L"texZ2";
  p[5].force_attribute(L"type") = L"sampler2D";
  p[5].force_attribute(L"size") = 1;
  p[5].force_attribute(L"val")  = texZ2.id;

  p[6].force_attribute(L"id")   = 6;
  p[6].force_attribute(L"name") = L"blendSize";
  p[6].force_attribute(L"type") = L"float";
  p[6].force_attribute(L"size") = 1;
  p[6].force_attribute(L"val")  = blendSize;

  p[7].force_attribute(L"id")   = 7;
  p[7].force_attribute(L"name") = L"mapScale";
  p[7].force_attribute(L"type") = L"float";
  p[7].force_attribute(L"size") = 1;
  p[7].force_attribute(L"val") = mapScale;
}

HRTextureNodeRef HydraRenderPlugin::CreateAO()
{
  HRTextureNodeRef texProc = hrTextureCreateAdvanced(L"proc", L"AmbOcc");

  hrTextureNodeOpen(texProc, HR_WRITE_DISCARD);
  {
    pugi::xml_node texNode = hrTextureParamNode(texProc);

    pugi::xml_node code_node = texNode.force_child(L"code");
    code_node.force_attribute(L"file") = L"c:/[Hydra]/pluginFiles/ProceduralTextures/AmbOcc.c";
    code_node.force_attribute(L"main") = L"main";
  }
  hrTextureNodeClose(texProc);

  return texProc;
}

void HydraRenderPlugin::BindingAO(pugi::xml_node materialNode, const Color& color1, const Color& color2, const float distance,
 const float falloff,
                                const int dir, const bool local/*, HRTextureNodeRef occludedTex, HRTextureNodeRef unoccludedTex, HRTextureNodeRef distanceTex*/)
{
  HRTextureNodeRef texProc = CreateAO();

  auto texNode = hrTextureBind(texProc, materialNode);
    
  const int numArg = 6;
  pugi::xml_node p[numArg];

  for (int i = 0; i < numArg; i++)        texNode.remove_child(L"arg");
  for (int i = 0; i < numArg; i++) p[i] = texNode.append_child(L"arg");

  p[0].force_attribute(L"id")    = 0;
  p[0].force_attribute(L"name")  = L"colorHit";
  p[0].force_attribute(L"type")  = L"float3";
  p[0].force_attribute(L"size")  = 1;
  p[0].force_attribute(L"val")   = strConverter::colorToWideString(color1).c_str();

  p[1].force_attribute(L"id")    = 1;
  p[1].force_attribute(L"name")  = L"texHit";
  p[1].force_attribute(L"type")  = L"sampler2D";
  p[1].force_attribute(L"size")  = 1;
  p[1].force_attribute(L"val")   = -1;

  p[2].force_attribute(L"id")    = 2;
  p[2].force_attribute(L"name")  = L"colorMiss";
  p[2].force_attribute(L"type")  = L"float3";
  p[2].force_attribute(L"size")  = 1;
  p[2].force_attribute(L"val")   = strConverter::colorToWideString(color2).c_str();

  p[3].force_attribute(L"id")    = 3;
  p[3].force_attribute(L"name")  = L"texMiss";
  p[3].force_attribute(L"type")  = L"sampler2D";
  p[3].force_attribute(L"size")  = 1;
  p[3].force_attribute(L"val")   = -1;

  p[4].force_attribute(L"id")    = 4;
  p[4].force_attribute(L"name")  = L"falloffPower";
  p[4].force_attribute(L"type")  = L"float";
  p[4].force_attribute(L"size")  = 1;
  p[4].force_attribute(L"val")   = falloff;

  p[5].force_attribute(L"id")    = 5;
  p[5].force_attribute(L"name")  = L"numNode";
  p[5].force_attribute(L"type")  = L"int";
  p[5].force_attribute(L"size")  = 1;
  p[5].force_attribute(L"val")   = dir;

  texNode.remove_child(L"ao");
  pugi::xml_node aoNode = texNode.append_child(L"ao");

  aoNode.force_attribute(L"length") = distance;
  aoNode.force_attribute(L"local") = local;

  switch (dir)
  {
    case CONCAVE: aoNode.force_attribute(L"hemisphere") = L"up";   break;
    case CONVEX:  aoNode.force_attribute(L"hemisphere") = L"down"; break;
    default:
      break;
  }
}

//void HydraRenderPlugin::BindingHexaplanarScalers(pugi::xml_node materialNode, HRTextureNodeRef texX, HRTextureNodeRef texY, HRTextureNodeRef texZ,
//  HRTextureNodeRef texX2, HRTextureNodeRef texY2, HRTextureNodeRef texZ2, LiteMath::float3 tex_scale, LiteMath::float3 minXYZ, LiteMath::float3 maxXYZ)
//{
//  HRTextureNodeRef texProc = CreateHexaplanarScalers();
//
//  auto texNode = hrTextureBind(texProc, materialNode);
//  
//  pugi::xml_node p1 = texNode.append_child(L"arg");
//  pugi::xml_node p2 = texNode.append_child(L"arg");
//  pugi::xml_node p3 = texNode.append_child(L"arg");
//  pugi::xml_node p4 = texNode.append_child(L"arg");
//  pugi::xml_node p5 = texNode.append_child(L"arg");
//  pugi::xml_node p6 = texNode.append_child(L"arg");
//  pugi::xml_node p7 = texNode.append_child(L"arg");
//  pugi::xml_node p8 = texNode.append_child(L"arg");
//  pugi::xml_node p9 = texNode.append_child(L"arg");
//
//  p1.append_attribute(L"id") = 0;
//  p1.append_attribute(L"name") = L"texX1";
//  p1.append_attribute(L"type") = L"sampler2D";
//  p1.append_attribute(L"size") = 1;
//  p1.append_attribute(L"val") = texX.id;
//  
//  p2.append_attribute(L"id") = 1;
//  p2.append_attribute(L"name") = L"texY1";
//  p2.append_attribute(L"type") = L"sampler2D";
//  p2.append_attribute(L"size") = 1;
//  p2.append_attribute(L"val") = texY.id;
//
//  p3.append_attribute(L"id") = 2;
//  p3.append_attribute(L"name") = L"texZ1";
//  p3.append_attribute(L"type") = L"sampler2D";
//  p3.append_attribute(L"size") = 1;
//  p3.append_attribute(L"val") = texZ.id;
//
//  p4.append_attribute(L"id") = 3;
//  p4.append_attribute(L"name") = L"texX2";
//  p4.append_attribute(L"type") = L"sampler2D";
//  p4.append_attribute(L"size") = 1;
//  p4.append_attribute(L"val") = texX2.id;
//  
//  p5.append_attribute(L"id") = 4;
//  p5.append_attribute(L"name") = L"texY2";
//  p5.append_attribute(L"type") = L"sampler2D";
//  p5.append_attribute(L"size") = 1;
//  p5.append_attribute(L"val") = texY2.id;
//
//  p6.append_attribute(L"id") = 5;
//  p6.append_attribute(L"name") = L"texZ2";
//  p6.append_attribute(L"type") = L"sampler2D";
//  p6.append_attribute(L"size") = 1;
//  p6.append_attribute(L"val") = texZ2.id;
//
//  p7.append_attribute(L"id") = 6;
//  p7.append_attribute(L"name") = L"tex_scale";
//  p7.append_attribute(L"type") = L"float3";
//  p7.append_attribute(L"size") = 1;
//  HydraXMLHelpers::WriteFloat3(p7.append_attribute(L"val"), tex_scale);
//
//  p8.append_attribute(L"id") = 7;
//  p8.append_attribute(L"name") = L"minXYZ";
//  p8.append_attribute(L"type") = L"float3";
//  p8.append_attribute(L"size") = 1;
//  HydraXMLHelpers::WriteFloat3(p8.append_attribute(L"val"), minXYZ);
//
//  p9.append_attribute(L"id") = 8;
//  p9.append_attribute(L"name") = L"maxXYZ";
//  p9.append_attribute(L"type") = L"float3";
//  p9.append_attribute(L"size") = 1;
//  HydraXMLHelpers::WriteFloat3(p9.append_attribute(L"val"), maxXYZ);
//}

Bitmap* getRenderedBitmapFromTexmap(BitmapInfo &bi, Texmap* tex)
{
  MSTR className;
  tex->GetClassName(className);
  std::wstring wClassName = strConverter::c2ws(className.data());
  int numSubs             = tex->NumSubTexmaps();

  Bitmap* bmap            = TheManager->Create(&bi);
  bmap->Fill(0, 0, 0, 0);

  TimeValue t             = 0;
  UVGen* uvGen            = nullptr;
  float uOffset, vOffset, uTiling, vTiling, angle, angleUVW[3];


  // For the correct operation of the other nodes, you need to make all the corrections at the engine level, 
  // so that each texture has its own transformation matrix. That's what we're planning.
  if (wClassName == L"Color Correction")
  {
    if (numSubs > 0)
    {
      Texmap* subTex = nullptr;
      subTex = tex->GetSubTexmap(0);

      if (subTex)
      {
        MSTR className2;
        subTex->GetClassName(className2);

        if (std::wstring(className2) == L"Bitmap")  // check if subtex-bitmap exists
        {
          UVGen* uvGenSub = nullptr;
          uvGenSub = subTex->GetTheUVGen();
          uvGen = uvGenSub;
        }
      }
    }
  }
  else
  {
    uvGen = tex->GetTheUVGen();
  }


  if (uvGen) //absolute bullshit
  {
    StdUVGen* stdUVgen = static_cast<StdUVGen*>(uvGen);
    uOffset            = stdUVgen->GetUOffs(t);
    vOffset            = stdUVgen->GetVOffs(t);
    uTiling            = stdUVgen->GetUScl(t);
    vTiling            = stdUVgen->GetVScl(t);
    angle              = stdUVgen->GetAng(t);
    angleUVW[0]        = stdUVgen->GetUAng(t);
    angleUVW[1]        = stdUVgen->GetVAng(t);
    angleUVW[2]        = stdUVgen->GetWAng(t);

    if (!g_inMatEditor)
    {
      stdUVgen->SetUOffs(0.0f, t);
      stdUVgen->SetVOffs(0.0f, t);
      stdUVgen->SetUScl(1.0f, t);
      stdUVgen->SetVScl(1.0f, t);
      stdUVgen->SetAng(0.0f, t);

      stdUVgen->SetUAng(0.0f, t);
      stdUVgen->SetVAng(0.0f, t);
      stdUVgen->SetWAng(0.0f, t);
    }

    tex->RenderBitmap(0, bmap, 1.0f, true);

    /* bmap->OpenOutput(&bi);
     bmap->Write(&bi);
     bmap->Close(&bi);*/

    if (!g_inMatEditor)
    {
      stdUVgen->SetUOffs(uOffset, t);
      stdUVgen->SetVOffs(vOffset, t);
      stdUVgen->SetUScl(uTiling, t);
      stdUVgen->SetVScl(vTiling, t);
      stdUVgen->SetAng(angle, t);

      stdUVgen->SetUAng(angleUVW[0], t);
      stdUVgen->SetVAng(angleUVW[1], t);
      stdUVgen->SetWAng(angleUVW[2], t);
    }

  }
  else
  {
    if (className != TEXT("Falloff"))    
      tex->RenderBitmap(t, bmap, 1.0F, true);    
  }

  return bmap;
}


void computeMaxProcTexHDR(float* a_buffer, int w, int h, void* texVoid)
{
  if (texVoid == nullptr)
    return;

  Texmap* tex = ((translateTexmap*)(texVoid))->texmap; //static_cast<Texmap*>(texVoid);

  BitmapInfo bi;

  bi.SetType(BMM_REALPIX_32);

  bi.SetWidth((WORD)w);
  bi.SetHeight((WORD)h);

  bi.SetFlags(MAP_HAS_ALPHA);
  bi.SetCustomFlag(0);

  Bitmap* bmap            = getRenderedBitmapFromTexmap(bi, tex);
  const auto W            = (size_t)w;
  const size_t numChannel = 4;

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int y = 0; y < h; ++y)
  {
    std::vector<BMM_Color_fl> line;
    line.resize(W);

    bmap->GetPixels(0, h - y - 1, w, line.data());
    for (size_t x = 0; x < W; ++x)
    {
      a_buffer[((size_t)y*W + x) * numChannel + 0] = line[x].r;
      a_buffer[((size_t)y*W + x) * numChannel + 1] = line[x].g;
      a_buffer[((size_t)y*W + x) * numChannel + 2] = line[x].b;
      a_buffer[((size_t)y*W + x) * numChannel + 3] = line[x].a;
    }
  }
  bmap->DeleteThis();
}

void computeMaxProcTexLDR(unsigned char* a_buffer, int w, int h, void* texVoid)
{
  if (texVoid == nullptr)
    return;

  Texmap* tex = static_cast<Texmap*>(texVoid);//((translateTexmap*)(texVoid))->texmap; //static_cast<Texmap*>(texVoid);

  BitmapInfo bi;

  bi.SetType(BMM_TRUE_64);

  bi.SetWidth((WORD)w);
  bi.SetHeight((WORD)h);

  bi.SetFlags(MAP_HAS_ALPHA);
  bi.SetCustomFlag(0);
  std::filesystem::path mapPath = HYDRA_TEMP_PATH / L"temp.bmp";
  bi.SetPath(mapPath.c_str());
  Bitmap* bmap         = getRenderedBitmapFromTexmap(bi, tex);

  const auto W            = (size_t)w;
  const size_t numChannel = 4;

#pragma omp parallel for num_threads(omp_get_num_procs())
  for (int y = 0; y < h; ++y)
  {
    std::vector<BMM_Color_64> line;
    line.resize(W);

    bmap->GetPixels(0, h - y - 1, w, line.data());

    for (size_t x = 0; x < W; ++x)
    {
      a_buffer[((size_t)y*W + x) * numChannel + 0] = 255 * line[x].r / 65535;
      a_buffer[((size_t)y*W + x) * numChannel + 1] = 255 * line[x].g / 65535;
      a_buffer[((size_t)y*W + x) * numChannel + 2] = 255 * line[x].b / 65535;
      a_buffer[((size_t)y*W + x) * numChannel + 3] = 255 * line[x].a / 65535;
    }
  }
  bmap->DeleteThis();
}

void GetMaxTextureSize(Texmap* tex, float w, float h, float& maxSizeW, float& maxSizeH, int currDepth)
{
  currDepth++;

  const int numSubs = tex->NumSubTexmaps();

  for (int i = 0; i < numSubs; ++i)
  {
    Texmap* subTex = nullptr;
    subTex = tex->GetSubTexmap(i);

    if (subTex)
    {
      MSTR className2;
      subTex->GetClassName(className2);

      if (strConverter::c2ws(className2.data()) == L"Bitmap")
      {
        auto pBitMap = static_cast<BitmapTex*>(subTex);
        auto bm = pBitMap->GetBitmap(TimeValue(0));

        if (bm)
        {
          w = bm->Width();
          h = bm->Height();

          if (w > maxSizeW || h > maxSizeH)
          {
            maxSizeW = w;
            maxSizeH = h;
          }
        }
        else
        {
          maxSizeW = 2048;
          maxSizeH = 2048;
        }
      }
      else
      {
        if (currDepth < 10)
          GetMaxTextureSize(subTex, w, h, maxSizeW, maxSizeH, currDepth);
      }
    }
  }
}

texInfo HydraRenderPlugin::BakeOtherTexmap(Texmap* tex, HRTextureNodeRef currentRef)
{
  MSTR className;
  tex->GetClassName(className);
  bool isHDR = false;
  auto t = GetStaticFrame();

  std::wstring wClassName = strConverter::c2ws(className.data());

  const int notSuppNum = 11;
  std::wstring notSupported[] ={ L"Cellular", L"Dent", L"Noise", L"Marble", L"Perlin Marble", L"Smoke", L"Speckle", L"Splat", L"Stucco", L"Waves", L"Wood" };
  bool supported = true;

  for (int i = 0; i < notSuppNum; ++i)
  {
    int channelSrc = tex->GetUVWSource();

    if (wClassName == notSupported[i] && (channelSrc != UVWSRC_EXPLICIT))
    {
      supported = false;
    }
  }

  if (supported)
  {
    //float gamma = gammaMgr.GetFileInGamma();
    g_inMatEditor = m_rendParams.inMtlEdit;

    auto uvgen = static_cast<StdUVGen*>(tex->GetTheUVGen());
    std::wstring alphaSource = L"rgb";

    float maxSizeW = 256;
    float maxSizeH = 256;
    int currDepth = 0;

    GetMaxTextureSize(tex, 256, 256, maxSizeW, maxSizeH, currDepth);


    int numSubs = tex->NumSubTexmaps();
    for (int i = 0; i < numSubs; ++i)
    {
      Texmap* subTex = nullptr;
      subTex = tex->GetSubTexmap(i);

      if (subTex)
      {
        MSTR className2;
        subTex->GetClassName(className2);
        if (strConverter::c2ws(className2.data()) == L"Bitmap")
        {
          auto pBitMap = static_cast<BitmapTex*>(subTex);
          isHDR = pBitMap->IsHighDynamicRange();
          auto bm = pBitMap->GetBitmap(t);

          if (bm)
          {
            auto stdUVgen = static_cast<StdUVGen*>(pBitMap->GetTheUVGen());

            int alphaSourceInt = FindInt(L"AlphaSource", pBitMap);
            alphaSource = (alphaSourceInt == 0) ? L"alpha" : L"rgb";

            if (wClassName == L"Color Correction" || wClassName == L"Mask" || wClassName == L"Noise" /*|| wClassName == L"Composite" ||*/
                /*wClassName == L"Mix" || wClassName == L"Output" || wClassName == L"RGB Multiply" || wClassName == L"RGB Tint"*/)
            {
              uvgen = stdUVgen; // for tile this node
            }
          }
        }
      }
    }


    HRTextureNodeRef texRef;
    if (isHDR)
    {
      translateTexmap texmapStruct;
      texmapStruct.texmap = tex;

      if (currentRef.id == -1)
        texRef = hrTexture2DCreateBakedHDR(&computeMaxProcTexHDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
      else
        texRef = hrTexture2DUpdateBakedHDR(currentRef, &computeMaxProcTexHDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
    }
    else
    {
      translateTexmap texmapStruct;
      texmapStruct.texmap = tex;

      if (currentRef.id == -1)
        texRef = hrTexture2DCreateBakedLDR(&computeMaxProcTexLDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
      else
        texRef = hrTexture2DUpdateBakedLDR(currentRef, &computeMaxProcTexLDR, texmapStruct.texmap, sizeof(translateTexmap), maxSizeW, maxSizeH);
    }

    return texInfo(texRef, uvgen, 1.0f, alphaSource); // gamma for this type maps (composite, checker and other) must have = 1.0f
  }
  else
  {
    return texInfo(HRTextureNodeRef(), nullptr, 1.0f, L"alpha");
  }
}

texInfo HydraRenderPlugin::BakeTexture(Mtl* a_mtl, Texmap* a_tex, const std::wstring& a_slotName, HRTextureNodeRef a_currentRef = HRTextureNodeRef(), const std::wstring& a_mtlName = std::wstring(L""), const bool a_dontUpdate = false)
{
  bool isBumpSlot       = (a_slotName == L"NormalMap");
  bool invertHeightMap  = false;
  float bumpHeight      = 1.0F;

  if (a_mtl != nullptr)
    invertHeightMap = (isBumpSlot && FindBool(L"DisplacementInvertHeightState", a_mtl));

  texInfo res(HRTextureNodeRef(), nullptr, 1.0F, L"alpha");

  MSTR className;

  if (a_tex == nullptr) 
    return res;

  a_tex->GetClassName(className);

  //std::wstring texName  = strConverter::c2ws(a_tex->GetName().data());
  std::wstring texClass = strConverter::c2ws(className.data());

  if (a_tex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))         //bitmap
    res = BakeBitmap(a_tex, a_currentRef, invertHeightMap, bumpHeight, a_mtlName, a_dontUpdate);
  else if (texClass == L"Normal Bump")
    res = BakeNormalMap(a_tex, a_currentRef, false, bumpHeight);  // this is normalmap based bump texture
  else                                          
    res = BakeOtherTexmap(a_tex, a_currentRef);                   //procedural texture or smth. else

  return res;
}


void HydraRenderPlugin::GetOneTexture(Mtl* a_mtl, Texmap* a_subTex, texInfo& a_oneTex, const std::wstring& a_slotName)
{
  const auto found = g_hrPlugResMangr.m_texmapLib.find(a_subTex);

  if (found == g_hrPlugResMangr.m_texmapLib.end()) // first add texture in lib
  {
    g_hrPlugResMangr.m_trackerManager->AddTexmap(a_subTex, a_mtl);
    a_oneTex = BakeTexture(a_mtl, a_subTex, a_slotName, HRTextureNodeRef(), a_mtl->GetName().data());
    if (std::get<0>(a_oneTex).id != -1)
      g_hrPlugResMangr.m_texmapLib[a_subTex] = a_oneTex;

    g_hrPlugResMangr.m_trackerManager->texmaps[a_subTex].first = false; // need export = false
  }
  else if (g_hrPlugResMangr.m_trackerManager->texmaps[a_subTex].first) // removed the head texture, but remained its instance, which becomes the head.
  {
    //refAndGamma = found->second;
    a_oneTex = BakeTexture(a_mtl, a_subTex, a_slotName, std::get<0>(found->second));
    g_hrPlugResMangr.m_trackerManager->texmaps[a_subTex].first = false;

    if (std::get<0>(a_oneTex).id != -1)
      g_hrPlugResMangr.m_texmapLib[a_subTex] = a_oneTex;
  }
  else // the texture has not changed and is taken from the library.
    a_oneTex = found->second;
}


std::unordered_map<std::wstring, texInfo> HydraRenderPlugin::ExtractHydraTextures(Mtl* mtl, const std::vector<std::wstring> &mapNames)
{
  std::unordered_map<std::wstring, texInfo> result;

  texInfo oneTex;
  Texmap* subTex = nullptr;

  const int numSubTex = mtl->NumSubTexmaps();

  for (int i = 0; i < numSubTex; ++i) // all slots in material: diffuse, reflect and etc.
  {
    const std::wstring slotName = strConverter::c2ws(mtl->GetSubTexmapSlotName(i).data());

    for (auto& j : mapNames)
    {
      if (slotName == j)
      {
        subTex = mtl->GetSubTexmap(i); // get texture from current slot
        break;
      }
    }


    if (subTex) // first map in material slot
    {
      //std::wstring slotNameStr = strConverter::c2ws(mtl->GetSubTexmapSlotName(i).data());
      //auto isOpacityMap = (slotName == L"OpacityMap") ? TEX_OUT_COLOR : TEX_OUT_ALPHA;


      if (subTex->ClassID() == HydraBlendedBox_CLASS_ID) // Hydra Blended box map
      {
        //const int numSubTex = subTex->NumSubTexmaps();
        //Texmap* map = nullptr;

        // get all sub texmap
        //for (int i = 0; i < numSubTex; i++)
        //{
        //  map = subTex->GetSubTexmap(i); 
        //  if (map)
        //    GetOneTexture(oneTex, map, mtl);
        //}

        Texmap* map = subTex->GetSubTexmap(0);    // "Top/All" slot         

        if (map)
          GetOneTexture(mtl, map, oneTex, slotName);

        if (std::get<0>(oneTex).id != -1)
          g_hrPlugResMangr.m_procTexLib[subTex].push_back(oneTex);
      }
      //else if (subTex->ClassName() == L"Falloff")
      //{
      //  Texmap* map = subTex->GetSubTexmap(0);    // Color1 slot 

      //  if (map)
      //    GetOneTexture(oneTex, map, mtl, slotName);

      //  if (std::get<0>(oneTex).id != -1)
      //    m_procTexLib[subTex].push_back(oneTex);
      //}
      else
        GetOneTexture(mtl, subTex, oneTex, slotName);


      // auto uvGen = static_cast<StdUVGen*>(subTex->GetTheUVGen());
      result[slotName] = oneTex;
    }
  }
  return result;
}




/////////////////////////////////////////////////////////////////
// Old unused functions left for reference for the time being  //
/////////////////////////////////////////////////////////////////

/*
bool HydraRenderPlugin::MaxOtherMapToTextureObj(Texmap* tex, TextureObj* texture)
{
  TimeValue t = GetStaticFrame();

  MSTR mapName = tex->GetName();
  texture->mapName = strConverter::c2ws(mapName.data());

  std::pair<std::set<std::wstring>::iterator, bool> ret = procTexNames.insert(texture->mapName);

  std::wstring mapPath;

  mapPath = HYDRA_TEMP_PATH + texture->mapName + L".bmp";

  UVGen* uvGen = tex->GetTheUVGen();
  Matrix3 uvTrans;

  MSTR className;
  tex->GetClassName(className);
  int numSubs = tex->NumSubTexmaps();

  std::wstring wClassName = strConverter::c2ws(className.data());


  // find textures that are functins of world position or object space position. Can't render them.
  //
  const int notSuppNum = 11;
  std::wstring notSupported[] = { L"Cellular", L"Dent", L"Noise", L"Marble", L"Perlin Marble", L"Smoke", L"Speckle", L"Splat", L"Stucco", L"Waves", L"Wood" };
  bool supported = true;
  for (int i = 0; i < notSuppNum; ++i)
  {
    int channelSrc = tex->GetUVWSource();

    if (wClassName == notSupported[i] && (channelSrc != UVWSRC_EXPLICIT))
      supported = false;
  }

  //TraceNodeCustomProperties(texture->mapName, tex, 1);

  if (supported)
  {
    if (uvGen)
      DumpUVGen((StdUVGen*)uvGen, texture);
    else if ((wClassName == L"Color Correction" || wClassName == L"Output" || wClassName == L"RGB Multiply" || wClassName == L"RGB Tint") && numSubs > 0)
    {
      // find DumpUVGen inside one of texmaps
      //
      for (int subTexId = 0; subTexId < tex->NumSubTexmaps(); subTexId++)
      {
        Texmap* subTex = tex->GetSubTexmap(subTexId);
        if (subTex)
        {
          MSTR className2;
          subTex->GetClassName(className2);

          UVGen* uvGenSub = subTex->GetTheUVGen();
          if (uvGenSub)
          {
            DumpUVGen((StdUVGen*)uvGenSub, texture);
            break;
          }
        }
      }

    }
    else
    {
      texture->uOffset = 0;
      texture->vOffset = 0;
      texture->uTiling = 1;
      texture->vTiling = 1;
      texture->angle = 0;

      texture->angleUVW[0] = 0;
      texture->angleUVW[1] = 0;
      texture->angleUVW[2] = 0;
    }

    //procTexs[procTexToCheck] = tex;
    //g_hrPlugResMangr.m_pluginLog.PrintValue("proc tex", strConverter::ws2s(mapPath));

    if (ret.second)
      procTexsV.push_back(tex);

    //procTexToCheck++;
    //curTex++;
    //TraceNodeCustomProperties(texture->mapName, tex, 1);

    bool isHDR = false;

    if (strConverter::c2ws(className.data()) == L"Color Correction")
    {
      int numSubs = tex->NumSubTexmaps();

      for (int i = 0; i < numSubs; ++i)
      {
        Texmap* subTex = nullptr;
        std::wstring slotName = strConverter::c2ws(tex->GetSubTexmapSlotName(i).data());

        if (slotName == L"Map")
        {
          subTex = tex->GetSubTexmap(i);

          if (subTex)
          {
            MSTR className2;
            subTex->GetClassName(className2);
            if (strConverter::c2ws(className2.data()) == L"Bitmap")
            {
              BitmapTex* pBitmapTex = (BitmapTex*)subTex;
              isHDR = pBitmapTex->IsHighDynamicRange();
            }
          }
        }

      }
    }
    if (isHDR)
    {
      mapPath = HYDRA_TEMP_PATH + texture->mapName + L".hdr";
      texture->mapName = mapPath;
      texture->displacementMapPath = mapPath;
    }
    else
    {
      texture->mapName = mapPath;
      texture->displacementMapPath = mapPath;
    }
    texture->hasDisplacement = true;

    if (strConverter::c2ws(className.data()) == L"Falloff")
    {
      texture->isFalloff = true;

      int numSubs = tex->NumSubTexmaps();

      for (int i = 0; i < numSubs; ++i)
      {
        Texmap* subTex = nullptr;
        std::wstring slotName = strConverter::c2ws(tex->GetSubTexmapSlotName(i).data());

        if (slotName == L"Map 1" || slotName == L"Map 2")
        {
          subTex = tex->GetSubTexmap(i);


          float amt = 1.0f;
          Color col_1(0, 0, 0);
          Color col_2(0, 0, 0);

          if (slotName == L"Map 1")
          {
            if (!FindBool(L"Map 1 Enable", tex)) continue;
            amt = FindFloat(L"Map Amount 1", tex);
            if (subTex)
            {
              texture->col_1.r = 1;
              texture->col_1.g = 1;
              texture->col_1.b = 1;
            }
            else
            {
              col_1 = FindColor(L"Color 1", tex);
              texture->col_1 = col_1;
            }
          }

          else if (slotName == L"Map 2")
          {
            if (!FindBool(L"Map 2 Enable", tex)) continue;
            amt = FindFloat(L"Map Amount 2", tex);
            if (subTex)
            {
              texture->col_2.r = 1;
              texture->col_2.g = 1;
              texture->col_2.b = 1;
            }
            else
            {
              col_2 = FindColor(L"Color 2", tex);
              texture->col_2 = col_2;
            }
          }

          if (subTex)
          {
            TextureObj tex_temp;
            bool supported = DumpTexture(subTex, tex->ClassID(), amt, &tex_temp);
            if (supported) texture->subTextures[strConverter::ws2s(slotName.data())] = tex_temp;
          }

        }
      }

      // find curve
      //
      ICurveCtl *curCtrl = nullptr;
      for (auto q = 0; q < tex->NumRefs(); ++q)
      {
        MSTR className;
        if (tex->GetReference(q))
        {
          tex->GetReference(q)->GetClassName(className);
          if (strConverter::c2ws(className.data()) == L"CurveControl")
          {
            curCtrl = (ICurveCtl *)tex->GetReference(q);
            break;
          }
        }
      }

      if (curCtrl)
      {
        ICurve * curve = curCtrl->GetControlCurve(0);

        int num_points = curve->GetNumPts();
        std::wstringstream tmp, tmp2;
        tmp << "          <points> ";
        tmp2 << "          <tangents> ";
        for (auto j = 0; j < num_points; ++j)
        {
          CurvePoint p = curve->GetPoint(t, j);
          tmp << p.p.x << " " << p.p.y << " ";
          tmp2 << p.out.x << " " << p.out.y << " ";
        }
        tmp << "</points>" << std::endl;
        tmp2 << "</tangents>" << std::endl;
        texture->points = tmp.str();
        texture->tangents = tmp2.str();
      }

      // if fresnel ?
      //
      int faloffType = FindInt(L"Falloff Type", tex);
      if (faloffType == 2) // 0 - default Perpendicular/Planar; 2 - Fresnel;
      {
        float fresnelIOR = FindFloat(L"Index of Refraction", tex);
        bool  overrideIOR = FindBool(L"Mtl IOR Override Enable", tex);

        texture->isFaloffFresnel = true;
        texture->isFaloffFresnelOverride = overrideIOR;
        texture->faloffFresnelIOR = fresnelIOR;
      }
      else
        texture->isFaloffFresnel = false;

      // TraceNodeCustomProperties(texture->mapName, tex, 1);
    }
    return true;
  }
  else
    return false;


}


void HydraRenderPlugin::MaxNormalMapToTextureObj(Texmap* tex, TextureObj* texture)
{
  bool hasNormalmap = false;
  bool hasDisplacementMap = false;

  TextureObj texNormal;
  TextureObj texDisplacement;

  for (int i = 0; i<tex->NumSubTexmaps(); i++)
  {
    Texmap* subTex = tex->GetSubTexmap(i);

    if (subTex != nullptr)
    {
      MSTR slotName = tex->GetSubTexmapSlotName(i);
      std::wstring slotNameData = strConverter::c2ws(slotName.data());

      TextureObj* pResult = nullptr;

      if (slotNameData == L"Normal")
      {
        pResult = &texNormal;
        pResult->nmFlipRed = FindBool(L"flip red", tex);
        pResult->nmFlipGreen = FindBool(L"flip green", tex);
        pResult->nmSwapRedAndGreen = FindBool(L"swap red green", tex);
        hasNormalmap = true;
      }
      else if (slotNameData == L"Additional Bump")
      {
        pResult = &texDisplacement;
        hasDisplacementMap = true;
        pResult->displacementAmount = FindFloat(L"Bump Multiplier", tex);
      }
      else
      {
        std::string slotName = strConverter::ws2s(slotNameData);
        g_hrPlugResMangr.m_pluginLog.PrintValue("(error) MaxNormalMapToTextureObj(); Unknown normalBump slot name", slotName.c_str());
      }

      if (subTex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00) && pResult != nullptr)
      {
        MSTR className, texName;

        subTex->GetClassName(className);
        texName = subTex->GetName();

        pResult->texName = strConverter::c2ws(texName);
        pResult->texClass = strConverter::c2ws(className);
        MaxBitMapDataToTextureObj(subTex, pResult, TEX_OUT_COLOR);
      }
    }
  }

  if (hasNormalmap)
    *texture = texNormal;
  else if (hasDisplacementMap)
    *texture = texDisplacement;

  texture->normalMapPath = texNormal.mapName;
  texture->displacementMapPath = texDisplacement.mapName;

  texture->isBump = true;
  texture->hasNormals = hasNormalmap;
  texture->hasDisplacement = hasDisplacementMap;

  texture->displacementAmount = texDisplacement.displacementAmount;
  texture->nmFlipRed = texNormal.nmFlipRed;
  texture->nmFlipGreen = texNormal.nmFlipGreen;
  texture->nmSwapRedAndGreen = texNormal.nmSwapRedAndGreen;

  //TraceNodeCustomProperties(texture->mapName, tex, 1);
}*/