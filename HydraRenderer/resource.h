//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HydraRenderer.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_METHODS                     5
#define IDS_QUALITY                     6
#define IDS_GUI                         7
#define IDS_PATHTRACING                 8
#define IDS_IRRADIANCE                  9
#define IDS_HIGH                        10
#define IDS_MEDIUM                      11
#define IDS_LOW                         12
#define IDR_STRING1                     101
#define IDR_STRING2                     102
#define IDR_STRING3                     103
#define IDR_STRING4                     104
#define IDR_STRING5                     105
#define IDR_STRING6                     106
#define IDR_STRING7                     107
#define IDD_HYDRADIALOG_old             201
#define IDD_PATHTRACING                 202
#define IDD_POSTPROC                    203
#define IDD_SPPM_CAUSTIC                204
#define IDD_SPPM_DIFF                   205
#define IDD_ENVIRONMENT                 206
#define IDD_OVERRIDE                    207
#define IDD_DEBUG                       208
#define IDD_DEVICE_SETUP                209
#define IDD_MLT                         210
#define IDD_HYDRADIALOG2                211
#define IDD_TOOLS                       212
#define IDD_HYDRADIALOG1                213
#define IDD_RENDER_ELEMENTS             214
#define IDD_HYDRADIALOG3                215
#define IDD_MMLT                        216
#define IDC_SEPSWAP                     1000
#define IDC_GUI                         1001
#define IDC_RR_ON                       1002
#define IDC_POSTPROC                    1003
#define IDC_DOF_ON                      1004
#define IDC_RANDOMLIGHT_ON              1005
#define IDC_LOG                         1006
#define IDC_RANDOMLIGHT_ON2             1007
#define IDC_DOF_ON2                     1008
#define IDC_LOG2                        1009
#define IDC_MINRAY                      1010
#define IDC_MAXRAY                      1011
#define IDC_MINRAY_SPIN                 1012
#define IDC_MAXRAY2                     1013
#define IDC_RAYBOUNCE                   1014
#define IDC_MAXRAY_SPIN                 1015
#define IDC_RAYBOUNCE_SPIN              1016
#define IDC_DIFFBOUNCE                  1017
#define IDC_FOCAL                       1018
#define IDC_DIFFBOUNCE_SPIN             1019
#define IDC_FOCAL_SPIN                  1020
#define IDC_LENS                        1021
#define IDC_LENS_SPIN                   1022
#define IDC_SEED                        1023
#define IDC_TIMELIMIT_EDIT              1024
#define IDC_CAUSTICS_ON                 1025
#define IDC_TIMELIMIT_SPIN              1026
#define IDC_WHITE                       1027
#define IDC_GUIDED                      1028
#define IDC_SPEC_NOISEFILTER_SPIN       1029
#define IDC_WHITE_SPIN                  1030
#define IDC_RELATIVE_ERR                1031
#define IDC_SPEC_NOISEFILTER_EDIT       1032
#define IDC_STRENGTH                    1033
#define IDC_RELATIVE_ERR_SPIN           1034
#define IDC_STRENGTH_SPIN               1035
#define IDC_BSDF_CLAMP_ON               1036
#define IDC_GAMMA                       1037
#define IDC_BSDF_CLAMP_EDIT             1038
#define IDC_GAMMA_SPIN                  1039
#define IDC_BSDF_CLAMP_SPIN             1040
#define IDC_PHI                         1041
#define IDC_PHI_SPIN                    1042
#define IDC_MAXPHOTONS_C                1043
#define IDC_LOW_EDIT                    1044
#define IDC_MAXPHOTONS_C_SPIN           1045
#define IDC_LOW_SPIN                    1046
#define IDC_CAUSTICPOW                  1047
#define IDC_HIGH_EDIT                   1048
#define IDC_CAUSTICPOW_SPIN             1049
#define IDC_HIGH_SPIN                   1050
#define IDC_INIT_RADIUS_C               1051
#define IDC_EXP_EDIT                    1052
#define IDC_INIT_RADIUS_C_SPIN          1053
#define IDC_EXP_SPIN                    1054
#define IDC_RETRACE_D                   1055
#define IDC_DEFOG_EDIT                  1056
#define IDC_RETRACE_D_SPIN              1057
#define IDC_WHITE_SPIN5                 1058
#define IDC_DEFOG_SPIN                  1059
#define IDC_VISIBILITY_C_ON             1060
#define IDC_ALPHA_EDIT_D                1061
#define IDC_BLOOM_RADIUS_EDIT           1062
#define IDC_BLOOM_ON                    1063
#define IDC_ALPHA_SPIN_D                1064
#define IDC_ALPHA_EDIT_C                1065
#define IDC_MAXPHOTONS_D                1066
#define IDC_ALPHA_SPIN_C                1067
#define IDC_BLOOM_SPIN                  1068
#define IDC_BLOOM_RADIUS_SPIN           1069
#define IDC_MAXPHOTONS_D_SPIN           1070
#define IDC_BLOOM_STR_EDIT              1071
#define IDC_INIT_RADIUS_D               1072
#define IDC_BLOOM_RADIUS_SPIN2          1073
#define IDC_BLOOM_STR_SPIN              1074
#define IDC_INIT_RADIUS_D_SPIN          1075
#define IDC_STRENGTH2                   1076
#define IDC_COMPRESS                    1077
#define IDC_COMPRESS_EDIT               1078
#define IDC_STRENGTH_SPIN2              1079
#define IDC_COMPRESS_SPIN2              1080
#define IDC_COMPRESS_SPIN               1081
#define IDC_STRENGTH3                   1082
#define IDC_CONTRAST                    1083
#define IDC_CONTRAST_EDIT               1084
#define IDC_STRENGTH_SPIN3              1085
#define IDC_CONTRAST_SPIN               1086
#define IDC_STRENGTH4                   1087
#define IDC_COLOR_ADAPT                 1088
#define IDC_WHITE_BALANCE               1089
#define IDC_WHITE_BALANCE_EDIT          1090
#define IDC_RETRACE_C                   1091
#define IDC_STRENGTH_SPIN4              1092
#define IDC_COLOR_ADAPT_SPIN            1093
#define IDC_WHITE_BALANCE_SPIN          1094
#define IDC_RETRACE_C_SPIN              1095
#define IDC_STRENGTH5                   1096
#define IDC_LIGHT_ADAPT                 1097
#define IDC_UNIFORM_CONTRAST_EDIT       1098
#define IDC_VISIBILITY_D_ON             1099
#define IDC_STRENGTH_SPIN5              1100
#define IDC_LIGHT_ADAPT_SPIN            1101
#define IDC_UNIFORM_CONTRAST_SPIN       1102
#define IDC_STRENGTH6                   1103
#define IDC_CHROM_ABERR                 1104
#define IDC_CHROM_ABERR_EDIT            1105
#define IDC_STRENGTH_SPIN6              1106
#define IDC_CHROM_ABERR_SPIN            1107
#define IDC_STRENGTH7                   1108
#define IDC_VIGNETTE                    1109
#define IDC_VIGNETTE_EDIT               1110
#define IDC_WHITE_SPIN2                 1111
#define IDC_STRENGTH_SPIN7              1112
#define IDC_VIGNETTE_SPIN               1113
#define IDC_DIFFSTAR_SIZE_EDIT          1114
#define IDC_SATURATION                  1115
#define IDC_SATURATION_EDIT             1116
#define IDC_SAVE_COLOR_SPIN             1117
#define IDC_SATURATION_EDIT2            1117
#define IDC_VIBRANCE_EDIT               1117
#define IDC_SATURATION_SPIN             1118
#define IDC_DIFFSTAR_NUMRAYS_EDIT       1119
#define IDC_NORMALIZE_EDIT              1120
#define IDC_SHARPNESS_EDIT              1121
#define IDC_DIFFSTAR_ROTRAYS_EDIT       1122
#define IDC_DIFFSTAR_RANDANGLE_EDIT     1123
#define IDC_DIFFSTAR_SPRAYRAY_EDIT      1124
#define IDC_DEBUG                       1125
#define IDC_SPEC_NOISEFILTER_ON         1126
#define IDC_PRIMARY                     1127
#define IDC_SECONDARY                   1128
#define IDC_TERTIARY                    1129
#define IDC_ENGINE                      1130
#define IDC_PRESETS                     1131
#define IDC_ALL_SECONDARY               1132
#define IDC_ALL_TERTIARY                1133
#define IDC_CAUSTICS                    1134
#define IDC_TIMELIMIT                   1135
#define IDC_PRINT_TIME_IN_IMAGE_ON      1136
#define IDC_FILTER                      1137
#define IDC_QMC_MATERIALS               1138
#define IDC_UPDATE_DENOISE              1139
#define IDC_QMC_LIGHTS                  1140
#define IDC_PRIMARY_ML                  1141
#define IDC_RESET_POSTPROC              1142
#define IDC_SECONDARY_ML                1143
#define IDC_IRRMAP                      1144
#define IDC_WRITE_TO_DISK               1145
#define IDC_FRAMEBUFFER                 1146
#define IDC_VERSION                     1147
#define IDC_MLAA                        1148
#define IDC_ENV_MULT                    1149
#define IDC_LITE                        1150
#define IDC_USEHDRFRAMEBUFFER           1151
#define IDC_USEHDR_FBUF                 1152
#define IDC_ENV_MULT_SPIN               1153
#define IDC_DENOISER                    1154
#define IDC_ENV_CLAMP                   1155
#define IDC_ENV_CLAMP_SPIN              1156
#define IDC_DEVICES_LIST                1157
#define IDC_CLAMP                       1158
#define IDC_FAST_EXR                    1159
#define IDC_DEBUG_LIST                  1160
#define IDC_RADIO3                      1161
#define IDC_LEGACY                      1162
#define IDC_MAT_DEVICE_BTN              1163
#define IDC_MLT_BURN_ITERS_EDIT         1164
#define IDC_MLT_BURN_ITERS_SPIN         1165
#define IDC_MLT_BURN_ITERS_SLIDER       1166
#define IDC_MLT_MED_THRES_EDIT          1167
#define IDC_MLT_MED_THRES_SPIN          1168
#define IDC_MLT_BURN_ITERS_EDIT2        1169
#define IDC_MLT_LSTEP_EDIT              1170
#define IDC_MLT_MEDIAN_ON               1171
#define IDC_MLT_LSTEP_SLIDER            1172
#define IDC_MLT_ITERMULT_SLIDER         1173
#define IDC_CHECK1                      1174
#define IDC_REMEMBER                    1175
#define IDC_REMEMBER_MDLG               1176
#define IDC_POSTPROC_WHILE_RENDER       1177
#define IDC_POSTPROC_ON                 1178
#define IDC_MLT_ITERMULT_EDIT           1179
#define IDC_DENOISE_COLOR_PASS_ON       1179
#define IDC_NOISELVL_SLIDER             1180
#define IDC_DENOISELVL_SLIDER           1180
#define IDC_REND_ELEM_NORMALS           1181
#define IDC_DENOISE_NORMAL_PASS_ON      1181
#define IDC_NOISELVL_EDIT               1182
#define IDC_DENOISELVL_EDIT             1182
#define IDC_REND_ELEM_COLOR             1183
#define IDC_DO_DENOISE                  1184
#define IDC_REND_ELEM_COORD             1185
#define IDC_CONVERT_MAT                 1186
#define IDC_RENDERQUALITY_SLIDER        1187
#define IDC_REND_ELEM_COVERAGE          1188
#define IDC_CONVERT_TO_HYDRA            1189
#define IDC_EXPOSURE                    1190
#define IDC_CONVERT_MAT2                1191
#define IDC_CONVERT_LIGHTS              1192
#define IDC_EXPOSURE_EDIT               1193
#define IDC_REND_ELEM_OBJID             1194
#define IDC_CONVERT_FROM_HYDRA          1195
#define IDC_EXPOSURE_SPIN               1196
#define IDC_REND_ELEM_INSTID            1197
#define IDC_CONVERT_TO_HYDRA2           1198
#define IDC_OVERR_PARAM_MAT             1199
#define IDC_CURVE                       1200
#define IDC_REND_ELEM_MATID             1201
#define IDC_REND_ELEM_ALPHA             1202
#define IDC_WHITE_BALANCE_SLIDER2       1203
#define IDC_REND_ELEM_ALPHA2            1204
#define IDC_GEN_REND_ELEM_AFTER_RENDER  1205
#define IDC_REND_ELEM_OBJID2            1206
#define IDC_REND_ELEM_SHADOW            1207
#define IDC_EXPOSURE_SLIDER             1208
#define IDC_COMPRESS_SLIDER             1209
#define IDC_CHECK2                      1210
#define IDC_CAUSTICSON                  1211
#define IDC_CONTRAST_SLIDER             1212
#define IDC_CAUSTICSON2                 1213
#define IDC_GRAYMAT_ON                  1214
#define IDC_ENABLE_TEXTURE_RESIZE       1215
#define IDC_SATURATION_SLIDER           1216
#define IDC_ENABLE_TEXTURE_RESIZE2      1217
#define IDC_VIBRANCE_SLIDER             1217
#define IDC_PRODUCTION_MODE             1218
#define IDC_WHITE_BALANCE_SLIDER        1219
#define IDC_INF_QUALITY_ON              1220
#define IDC_UNIFORM_CONTRAST_SLIDER     1221
#define IDC_CHROM_ABERR_SLIDER          1222
#define IDC_VIGNETTE_SLIDER             1223
#define IDC_NORMALIZE_SLIDER            1224
#define IDC_WHITEPOINT_COLOR            1225
#define IDC_GENERATE_RENDER_ELEMENTS    1226
#define IDC_SHARPNESS_SLIDER            1227
#define IDC_REND_ELEM_DEPTH             1228
#define IDC_DIFFSTAR_SIZE_SLIDER        1229
#define IDC_DIFFSTAR_NUMRAYS_SLIDER     1230
#define IDC_DIFFSTAR_ROTRAYS_SLIDER     1231
#define IDC_DIFFSTAR_RANDANGLE_SLIDER   1232
#define IDC_MMLT_SDS_SLIDER             1233
#define IDC_MMLT_SDS_FIXED_PROB_SLIDER  1234
#define IDC_DIFFSTAR_SPRAYRAY_SLIDER    1235
#define IDC_MMLT_MEDIAN_ON              1236
#define IDC_MMLT_MED_THRES_EDIT         1237
#define IDC_MMLT_MED_THRES_SPIN         1238
#define IDC_MMLT_BURN_SLIDER            1239
#define IDC_MMLT_SDS_FIXED_PROB_EDIT    1240
#define IDC_MMLT_STEP_POWER_SLIDER      1241
#define IDC_MMLT_ESTIM_TIME_SLIDER      1242
#define IDC_MMLT_BURN_EDIT              1243
#define IDC_SAVE_REND_IMAGE_EVERY_EDIT  1244
#define IDC_MMLT_THREADS_EDIT           1245
#define IDC_SAVE_REND_IMAGE_EVERY_SPIN  1246
#define IDC_MMLT_THREADS_SLIDER         1247
#define IDC_REND_IMAGE_EVERY_ON         1248
#define IDC_SAVE_REND_IMAGE_EVERY_ON    1249
#define IDC_MMLT_MULT_BRIGHTNESS_EDIT   1250
#define IDC_QUAL_DRAFT_STATIC           1251
#define IDC_MMLT_MULT_BRIGHTNESS_SPIN   1252
#define IDC_QMC_MODE_SPIN               1253
#define IDC_QUAL_LOW_STATIC             1254
#define IDC_QUAL_MEDIUM_STATIC          1255
#define IDC_MMLT_STEP_POWER_EDIT        1256
#define IDC_MMLT_BURN_EDIT2             1257
#define IDC_MMLT_ESTIM_TIME_EDIT        1258
#define IDC_QUAL_HIGH_STATIC            1259
#define IDC_MMLT_STEP_POWER_EDIT2       1260
#define IDC_MMLT_STEP_SIZE_EDIT         1261
#define IDC_QUAL_ULTRA_STATIC           1262
#define IDC_MMLT_STEP_SIZE_SLIDER       1263
#define IDC_SPP_STATIC                  1264

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         10000
#define _APS_NEXT_CONTROL_VALUE         1265
#define _APS_NEXT_SYMED_VALUE           5000
#endif
#endif
