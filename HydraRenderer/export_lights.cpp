#include "HydraRenderer.h"
#include <sstream>

#include <genlight.h>
#include <lslights.h>
#include <icustattribcontainer.h>

#include <INodeMentalRayProperties.h>
#include <DaylightSimulation/IPhysicalSunSky.h>
#include <shadgen.h>
#include <decomp.h>

////////////////////////////////////////////////////////////////////////
extern HRplugResourceManager g_hrPlugResMangr;

#define HydraLight_CLASS_ID	        Class_ID(0x69729747, 0x8c40aaf7) // hydra light C++ plugin.
#define HydraLight2_CLASS_ID        Class_ID(0x6601ed90, 0x35f9b299) // hydra light 3ds max script plugin.
#define HydraSunSky_CLASS_ID	      Class_ID(0x3e362b95, 0x4bb75481)


void HydraRenderPlugin::ExtractIESData(LightObject *light, pugi::xml_node lightNode) const
{
  std::filesystem::path iesPath = static_cast<LightscapeLight*>(light)->GetFullWebFileName();
  IFileResolutionManager* pFRM  = IFileResolutionManager::GetInstance();


  if (!iesPath.empty())
    iesPath = GetCorrectTexPath2(iesPath.c_str(), pFRM);

  const float iesRotX = static_cast<LightscapeLight*>(light)->GetWebRotateX();
  const float iesRotY = static_cast<LightscapeLight*>(light)->GetWebRotateY();
  const float iesRotZ = static_cast<LightscapeLight*>(light)->GetWebRotateZ();

#ifdef MAX2022
  Matrix3 rotU, rotV, rotW, mRot;
#else
  Matrix3 rotU(1), rotV(1), rotW(1), mRot(1);
#endif // MAX2022

  rotU.RotateX( iesRotY * DEG_TO_RAD);
  rotV.RotateZ(-iesRotX * DEG_TO_RAD);
  rotW.RotateY( iesRotZ * DEG_TO_RAD);

  mRot = rotU * rotV * rotW;

  // mRot = (*lightTransform) * mRot;

  float samplerMatrix[16];
  MaxMatrix3ToFloat16V2(mRot, samplerMatrix);

  auto distribution = lightNode.force_child(L"ies");
  distribution.force_attribute(L"data").set_value(iesPath.c_str());

  distribution.force_attribute(L"matrix");
  HydraXMLHelpers::WriteMatrix4x4(distribution, L"matrix", samplerMatrix);
}


bool IsPhotometricVisible(LightObject* light, TimeValue t, Interval valid)
{
  bool isVisible = true;

  auto* pMRLight = static_cast<LightscapeLight*>(light);
  ICustAttribContainer* pAttribs = light->GetCustAttribContainer();

  if (pAttribs != nullptr)
  {
    int n = pAttribs->GetNumCustAttribs();
    for (int i = 0; i < n; i++)
    {
      CustAttrib* pAttr = pAttribs->GetCustAttrib(i);
      if (pAttr != nullptr)
      {
        hydraStr name = pAttr->GetName();
        if (name.find(_M("Area Light Sampling Custom Attribute")) != hydraStr::npos)
        {
          LightscapeLight::AreaLightCustAttrib* pAreaCustomAttribs = pMRLight->GetAreaLightCustAttrib(pAttr);
          isVisible = pAreaCustomAttribs->IsLightShapeRenderingEnabled(t, &valid);
        }
      }
    }
  }

  return isVisible;
}

float GetDirectLightAreaShadowsSize(LightObject* light, TimeValue t)
{
  GenLight* pGenLight = static_cast<GenLight*>(light);
  if (pGenLight != nullptr)
  {
    ShadowType* pShadowGen = pGenLight->GetShadowGenerator();
    if (pShadowGen != nullptr)
    {
      IAreaShadowType* area = pShadowGen->GetAreaShadowType();
      if (area != nullptr)
        return 0.1f*0.5f*(area->GetLength(t) + area->GetWidth(t));
    }
  }

  return 0.0F;
}


std::wstring ReadIESLightToString(const std::filesystem::path& iesPath)
{
  std::wifstream fin(iesPath.c_str());
  std::wostringstream fout;

  if (!fin.is_open())
    return std::wstring(L"");

  std::wstring temp;

  while (!fin.eof()) //-V1024
  {
    std::getline(fin, temp);
    fout << temp.c_str() << std::endl;
  }

  return fout.str();
}

void FixEnvironmentTexOffset(pugi::xml_node a_texNode)
{
  pugi::xml_attribute matrixAttr = a_texNode.attribute(L"matrix");
  if (matrixAttr != nullptr)
  {
    std::wstringstream strIn(matrixAttr.as_string());
    std::wstringstream strOut;
    float matrixData[16];

    for (int i = 0; i < 16; i++)
      strIn >> matrixData[i];

    matrixData[3] -= 0.5f;

    for (int i = 0; i < 16; i++)
      strOut << matrixData[i] << L" ";

    const std::wstring mstr = strOut.str();
    matrixAttr.set_value(mstr.c_str());
  }
}

void HydraRenderPlugin::ExtractStdLightParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  ULONG classId = light->ClassID().PartA();
  GenLight* glight = static_cast<GenLight*>(light);
  constexpr ULONG SKY_LIGHT_CLASS_ID_PART_A = 0x7bf61478;

  bool isSpot = false;
  bool isDirectional = false;
  bool isPoint = false;
  //bool isSky = false;
  bool isExtra = false;

  struct LightState ls;
  glight->EvalLightState(t, valid, &ls);

  lightNode.force_attribute(L"shape").set_value(L"point");
  auto intensityNode = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(ls.color).c_str());

  const float shadowRadius = GetDirectLightAreaShadowsSize(light, t);
  float intensity = ls.intens * PI; // intens*PI - because in physical BRDF, diffuse = colorDiff/PI.

  switch (classId)
  {
    case FSPOT_LIGHT_CLASS_ID:
    case SPOT_LIGHT_CLASS_ID:
      isSpot = true;
      lightNode.force_attribute(L"type").set_value(L"point");
      lightNode.force_attribute(L"distribution").set_value(L"spot");
      break;

    case DIR_LIGHT_CLASS_ID:
    case TDIR_LIGHT_CLASS_ID:
      isDirectional = true;
      lightNode.force_attribute(L"type").set_value(L"directional");
      lightNode.force_attribute(L"distribution").set_value(L"directional");
      lightNode.force_child(L"shadow_softness").force_attribute(L"val").set_value(1.0f);
      lightNode.force_child(L"angle_radius").force_attribute(L"val").set_value(shadowRadius);
      break;

      //case SKY_LIGHT_CLASS_ID_PART_A:
      //  isSky = true;
      //  lightNode.force_attribute(L"type").set_value(L"sky");
      //  break;

    case OMNI_LIGHT_CLASS_ID:
      isPoint = true;
      lightNode.force_attribute(L"type").set_value(L"point");
      lightNode.force_attribute(L"distribution").set_value(L"uniform");
      intensity /= PI;
      break;

    default:
      isExtra = true;
      break;
  }

  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(intensity);

  bool isTargeted = (classId == SPOT_LIGHT_CLASS_ID || classId == TDIR_LIGHT_CLASS_ID);

  if (isSpot || isDirectional)
  {
    auto sizeNode = lightNode.force_child(L"size");
    sizeNode.force_attribute(L"inner_radius").set_value(ToMeters(ls.hotsize));
    sizeNode.force_attribute(L"outer_radius").set_value(ToMeters(ls.fallsize));
  }

  //if (isSky)
  //{  
  // // const unsigned int PBLOCK_REF = 0;		 // This is a IParamBlock
  //  const unsigned int PBLOCK_REF_SKY = 0; // This is a IParamBlock2
  //  auto parametersSky = static_cast<IParamBlock2*>(light->GetReference(PBLOCK_REF_SKY));
  //  if (parametersSky != nullptr)
  //  {
  //    Texmap* colorMap = FindTex(L"Sky Color Map", light);
  //    float  texAmt = FindFloat(L"Sky Color Map Amt", light);
  //    int useSeparateSkyLight = FindInt(L"Skylight Mode", light);
  //    bool useMap = FindBool(L"Sky Color Map On", light);

  //    if (colorMap != nullptr && useMap && colorMap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
  //    {
  //      texInfo oneTex = BakeTexture(colorMap);
  //      if (std::get<0>(oneTex).id != -1)
  //      {
  //        intensityNode.force_child(L"color").force_attribute(L"val").set_value(L"1 1 1");
  //        lightNode.force_attribute(L"distribution").set_value(L"map");

  //        auto texNode = hrTextureBind(std::get<0>(oneTex), intensityNode.child(L"color"));
  //        if (std::get<1>(oneTex))
  //        {
  //          ExtractSampler(texNode, oneTex);
  //          FixEnvironmentTexOffset(texNode);
  //        }
  //      }
  //      else
  //      {
  //        lightNode.force_attribute(L"distribution").set_value(L"uniform");
  //      }
  //    }
  //  }
  //}

  if (isSpot)
  {
    lightNode.force_child(L"falloff_angle").force_attribute(L"val").set_value(ls.fallsize);
    lightNode.force_child(L"falloff_angle2").force_attribute(L"val").set_value(ls.hotsize);
  }

}



void HydraRenderPlugin::ExtractHydraLightParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  auto pLight               = dynamic_cast<IHydraLight*>(light);
  if (!pLight)
    return;

  const bool  isVisible     = FindBool(L"Visible in render", light);
  const int   typeLight     = pLight->HlType();
  const Color color         = pLight->GetRGBColor(t, valid);
  float intensityImageUnit  = pLight->GetResultingIntensity(t, valid);

  float sizeX               = ToMeters(pLight->GetLength(t, valid));
  float sizeY               = ToMeters(pLight->GetWidth(t, valid));
  float radius              = ToMeters(pLight->GetRadius(t, valid));

  const float hotspot       = pLight->GetHotspot(t, valid);
  const float fallsize      = pLight->GetFallsize(t, valid);

  const LightscapeLight::DistTypes lightDistributionType = pLight->GetDistribution();

  std::wstring type = L"";
  std::wstring distribution = L"";
  std::wstring shape = L"";


  switch (typeLight)
  {
    case HL_TYPE_POINT:
      type   = L"point";
      shape  = L"point";

      if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)      
        distribution = L"spot";      
      else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
      {
        distribution = L"ies";
        intensityImageUnit /= PI;
      }
      else
      {
        distribution = L"diffuse";
        intensityImageUnit /= PI;
      }
      break;
    case HL_TYPE_RECTANGLE:
      type = L"area";
      shape = L"rect";
      sizeX = sizeX * 0.5F;
      sizeY = sizeY * 0.5F;

      if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
        distribution = L"spot";
      else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
        distribution = L"ies";
      else
        distribution = L"diffuse";
      break;

    case HL_TYPE_DISK:
      type = L"area";
      shape = L"disk";
      sizeX = radius;
      sizeY = radius;

      if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
        distribution = L"spot";
      else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
        distribution = L"ies";
      else
        distribution = L"diffuse";
      break;

    case HL_TYPE_SPHERE:
      type = L"area";
      shape = L"sphere";
      sizeX = radius;
      sizeY = radius;
      distribution = L"diffuse";
      break;

    case HL_TYPE_CYLINDER:
      type = L"area";
      shape = L"cylinder";
      sizeY = radius;
      distribution = L"diffuse";

      break;

    case HL_TYPE_PORTAL:
      type = L"area";
      shape = L"rect";
      sizeX = sizeX * 0.5F;
      sizeY = sizeY * 0.5F;
      distribution = L"diffuse";
      break;

    default:
      type = L"point";
      shape = L"point";
      sizeX = 1.0F;
      sizeY = 1.0F;

      if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
        distribution = L"ies";
      else
        distribution = L"diffuse";
      break;
  }

  lightNode.force_attribute(L"type").set_value(type.c_str());
  lightNode.force_attribute(L"shape").set_value(shape.c_str());
  lightNode.force_attribute(L"distribution").set_value(distribution.c_str());

  auto intensityNode = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(intensityImageUnit);

  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(color).c_str());

  auto sizeNode = lightNode.force_child(L"size");
  if (shape == L"cylinder")
  {
    sizeNode.force_attribute(L"radius").set_value(radius);
    sizeNode.force_attribute(L"height").set_value(sizeX);
    sizeNode.force_attribute(L"angle").set_value(360.0F);
  }
  else if (shape == L"sphere")
    sizeNode.force_attribute(L"radius").set_value(radius);
  else
  {
    sizeNode.force_attribute(L"radius").set_value(radius);
    sizeNode.force_attribute(L"half_length").set_value(sizeY);
    sizeNode.force_attribute(L"half_width").set_value(sizeX);
  }
  lightNode.attribute(L"visible").set_value(isVisible ? 1 : 0);

  if (distribution == L"ies")
  {
    Matrix3 lightTransform = ToMetersM3(node->GetObjectTM(t));
    lightTransform.SetRow(3, Point3(0.0F, 0.0F, 0.0F)); // kill translate
    ExtractIESData(light, lightNode);
  }
  if (distribution == L"spot")
  {
    lightNode.force_child(L"falloff_angle").force_attribute(L"val").set_value(fallsize);
    lightNode.force_child(L"falloff_angle2").force_attribute(L"val").set_value(hotspot);
  }

  if (typeLight == HL_TYPE_PORTAL)
  {
    auto portalNode = lightNode.force_child(L"sky_portal");
    portalNode.force_attribute(L"val").set_value(1);
    portalNode.force_attribute(L"source_id").set_value(m_envRef.id);
  }
  else
    lightNode.remove_child(L"sky_portal");
}


void HydraRenderPlugin::ExtractHydraLight2Params(LightObject* a_light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval& valid)
{
  if (!a_light)
    return;

//.lightOn : boolean
//.targeted : boolean
//.visInRen : boolean
//.type : integer
//.intensityGUI : float
//.intensityCd : float
//.intensityImage : float
//.intensityType : integer
//.colorRgb : RGB color
//.light_length : worldUnits
//.light_Width : worldUnits
//.light_Radius : worldUnits
//.iesOn : boolean
//.iesFile : filename
//.iesFilename : string
//.iesRotX : float
//.iesRotY : float
//.iesRotZ : float
//.spotOn : boolean
//.showCone : boolean
//.hotspot : float
//.fallSize : float
//.spectrOn : boolean
//.spectrFile : filename
//.spectrFilename : string


  const bool  isVisible     = FindBool(L"visInRen", a_light);
  const int   typeLight     = FindInt(L"type", a_light) - 1; // in plugin start from 1
  const Color color         = FindColor(L"colorRgb", a_light);
  const float intensImage   = FindFloat(L"intensityImage", a_light);
  const float intensCd      = FindFloat(L"intensityCd", a_light);
  const int intensityType   = FindInt(L"intensityType", a_light);

  float sizeX               = ToMeters(FindFloat(L"light_length", a_light));
  float sizeY               = ToMeters(FindFloat(L"light_Width", a_light));
  float radius              = ToMeters(FindFloat(L"light_Radius", a_light));

  const float hotspot       = FindFloat(L"hotspot", a_light);
  const float fallsize      = FindFloat(L"fallSize", a_light);

  const int distribType     = FindInt(L"distribType", a_light);

  std::wstring type         = L"point";
  std::wstring shape        = L"point";
  std::wstring distribution = L"diffuse";

  Matrix3 lightTransform = ToMetersM3(node->GetObjectTM(t));

  float area = 1.0F;

  // Scaling the light source affects its area.
  lightTransform.SetRow(3, Point3(0.0F, 0.0F, 0.0F)); // kill translate
  float kf             = scaleWithMatrix(1.0F, lightTransform);
  float scaleIntensity = kf * kf;


  switch (typeLight)
  {
  case HL_TYPE_POINT:
    sizeX          = 1.0F;
    sizeY          = 1.0F;
    scaleIntensity = 1.0F;

    if (distribType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
      distribution = L"spot";
    else if (distribType == LightscapeLight::DistTypes::WEB_DIST)
    {
      distribution    = L"ies";
      scaleIntensity *= PI;
    }
    else
      scaleIntensity *= PI;
    break;
  case HL_TYPE_RECTANGLE:
    area  = sizeX * sizeY; // because using half sizes 
    type  = L"area";
    shape = L"rect";
    sizeX = sizeX * 0.5F;
    sizeY = sizeY * 0.5F;

    if (distribType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
      distribution = L"spot";
    else if (distribType == LightscapeLight::DistTypes::WEB_DIST)
      distribution = L"ies";
    break;

  case HL_TYPE_DISK:
    area  = PI * radius * radius; // because using half sizes 
    type  = L"area";
    shape = L"disk";
    sizeX = radius;
    sizeY = radius;

    if (distribType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
      distribution = L"spot";
    else if (distribType == LightscapeLight::DistTypes::WEB_DIST)
      distribution = L"ies";
    break;

  case HL_TYPE_SPHERE:
    area  = 4.0F * PI * radius * radius; // because using half sizes 
    type  = L"area";
    shape = L"sphere";
    sizeX = radius;
    sizeY = radius;
    break;

  case HL_TYPE_CYLINDER:
    area  = TWOPI * radius * sizeX; // because using half sizes 
    type  = L"area";
    shape = L"cylinder";
    sizeY = radius;
    break;

  case HL_TYPE_PORTAL:
    type  = L"area";
    shape = L"rect";
    sizeX = sizeX * 0.5F;
    sizeY = sizeY * 0.5F;
    break;

  default:
    sizeX = 1.0F;
    sizeY = 1.0F;

    if (distribType == LightscapeLight::DistTypes::WEB_DIST)
      distribution = L"ies";
    break;
  }

  lightNode.force_attribute(L"type").set_value(type.c_str());
  lightNode.force_attribute(L"shape").set_value(shape.c_str());
  lightNode.force_attribute(L"distribution").set_value(distribution.c_str());

  area *= scaleIntensity;

  const float resIntensity = intensityType == 1 ? intensImage : (intensCd / 683.0F / fmax(area, 0.000001F));

  auto intensityNode = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"multiplier").force_attribute(L"val") = resIntensity;

  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(color).c_str());

  auto sizeNode = lightNode.force_child(L"size");
  if (shape == L"cylinder")
  {
    sizeNode.force_attribute(L"radius")      = radius;
    sizeNode.force_attribute(L"height")      = sizeX;
    sizeNode.force_attribute(L"angle")       = 360.0;
  }
  else if (shape == L"sphere")
    sizeNode.force_attribute(L"radius")      = radius;
  else
  {
    sizeNode.force_attribute(L"radius")      = radius;
    sizeNode.force_attribute(L"half_length") = sizeY;
    sizeNode.force_attribute(L"half_width")  = sizeX;
  }
  lightNode.attribute(L"visible").set_value(isVisible ? 1 : 0);

  if (distribution == L"ies")
  {
    Matrix3 lightTransform = ToMetersM3(node->GetObjectTM(t));
    lightTransform.SetRow(3, Point3(0.0F, 0.0F, 0.0F)); // kill translate
    ExtractIESDataFromScriptLight(a_light, &lightTransform, lightNode);    
  }
  if (distribution == L"spot")
  {
    lightNode.force_child(L"falloff_angle").force_attribute(L"val")  = fallsize;
    lightNode.force_child(L"falloff_angle2").force_attribute(L"val") = hotspot;
  }

  if (typeLight == HL_TYPE_PORTAL)
  {
    auto portalNode = lightNode.force_child(L"sky_portal");
    portalNode.force_attribute(L"val")        = 1;
    portalNode.force_attribute(L"source_id")  = m_envRef.id;
  }
  else
    lightNode.remove_child(L"sky_portal");

  ExtractSpectrFromScriptLight(a_light, lightNode);
}


void HydraRenderPlugin::ExtractIESDataFromScriptLight(LightObject * light, Matrix3 * lightTransform, pugi::xml_node lightNode)
{
  std::filesystem::path iesPath = FindString(L"iesFile", light);
  IFileResolutionManager* pFRM  = IFileResolutionManager::GetInstance();

  if (!iesPath.empty())
    iesPath = GetCorrectTexPath2(iesPath.c_str(), pFRM);

  const float iesRotX = FindFloat(L"iesRotX", light);
  const float iesRotY = FindFloat(L"iesRotY'", light);
  const float iesRotZ = FindFloat(L"iesRotZ", light);

#ifdef MAX2022
  Matrix3 rotU, rotV, rotW, mRot;
#else
  Matrix3 rotU(1), rotV(1), rotW(1), mRot(1);
#endif // MAx2022

  rotU.RotateX( iesRotY * DEG_TO_RAD);
  rotV.RotateZ(-iesRotX * DEG_TO_RAD);
  rotW.RotateY( iesRotZ * DEG_TO_RAD);

  mRot = rotU * rotV * rotW;

  // mRot = (*lightTransform) * mRot;

  float samplerMatrix[16];
  MaxMatrix3ToFloat16V2(mRot, samplerMatrix);

  auto distribution = lightNode.force_child(L"ies");
  distribution.force_attribute(L"data").set_value(iesPath.c_str());

  distribution.force_attribute(L"matrix");
  HydraXMLHelpers::WriteMatrix4x4(distribution, L"matrix", samplerMatrix);

}


void HydraRenderPlugin::ExtractSpectrFromScriptLight(LightObject* light, pugi::xml_node lightNode)
{
  if (!FindBool(L"spectrOn", light))
    return;

  std::filesystem::path spectrPath = FindString(L"spectrFile", light);
  IFileResolutionManager* pFRM     = IFileResolutionManager::GetInstance();


  if (!spectrPath.empty())
    spectrPath = GetCorrectTexPath2(spectrPath.c_str(), pFRM);

  pugi::xml_document xmlDoc;

  auto loadResult = xmlDoc.load_file(spectrPath.c_str());
  if (!loadResult)
  {
    std::string  str(loadResult.description());
    std::wstring errorMsg (str.begin(), str.end());
    g_hrPlugResMangr.m_pluginLog.Print(IRMM_S::kSource_ProductionRenderer, IRMM_T::kType_Error, errorMsg);
    return;
  }

  auto texturesLib  = xmlDoc.child(L"spectral");

  lightNode.append_copy(texturesLib);
}



void HydraRenderPlugin::ExtractHydraSunSkyParams(LightObject * light, HRLightRef lightRef, INode * node, pugi::xml_node lightNode, TimeValue t, Interval & valid)
{
  m_sunRef = lightRef;
  Color sunColor         = light->GetRGBColor(t, valid);
  const float sunMult    = light->GetIntensity(t, valid);
  const float softShadow = FindFloat(L"SunSoftShadow", light);
  const float hotspot    = light->GetHotspot(t, valid);
  const float falloff    = light->GetFallsize(t, valid);

  bool isTargeted = false;
  Matrix3 targetMatrix;
  INode* targetNode = node->GetTarget();
  if (targetNode != nullptr)
    targetMatrix = ToMetersM3(targetNode->GetObjectTM(t));

  isTargeted = (targetNode != nullptr);

  Matrix3 lightMatrix = ToMetersM3(node->GetObjectTM(t));
  Point3 lightDirPoint = -node->GetNodeTM(t).GetRow(2);

  if (isTargeted)
  {
    Point3 pos, target;

    pos = lightMatrix * Point3(0, 0, 0);
    target = targetMatrix * Point3(0, 0, 0);
    lightDirPoint = (target - pos).Normalize();
  }


  // Sun
  lightNode.force_attribute(L"type").set_value(L"directional");
  lightNode.force_attribute(L"shape").set_value(L"point");
  lightNode.force_attribute(L"distribution").set_value(L"directional");

  auto sizeNode = lightNode.force_child(L"size");
  sizeNode.force_attribute(L"inner_radius").set_value(ToMeters(hotspot));
  sizeNode.force_attribute(L"outer_radius").set_value(ToMeters(falloff + 1.0f));

  auto intensityNode = lightNode.force_child(L"intensity");

  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(sunColor).c_str());
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(sunMult);

  lightNode.force_child(L"shadow_softness").force_attribute(L"val").set_value(softShadow);


  // Sky
  if (FindBool(L"SkyOn", light))
  {
    m_perezSky.skyTint = FindColor(L"SkyTint", light);
    m_perezSky.skyHaze = FindFloat(L"SkyHaze", light);
    m_perezSky.skyMult = FindFloat(L"SkyMultiplier", light);
    m_hasSky = true;

    hrLightOpen(m_envRef, HR_WRITE_DISCARD);
    {
      pugi::xml_node lightNode = hrLightParamNode(m_envRef);

      lightNode.force_attribute(L"type").set_value(L"sky");
      lightNode.force_attribute(L"distribution").set_value(L"perez");

      auto intensityNode = lightNode.append_child(L"intensity");

      intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(m_perezSky.skyTint).c_str());
      intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(m_perezSky.skyMult);

      auto sunModel = lightNode.force_child(L"perez");

      sunModel.force_attribute(L"sun_id") = m_sunRef.id;
      sunModel.force_attribute(L"turbidity").set_value(m_perezSky.skyHaze);
    }
    hrLightClose(m_envRef);
  }
}


void HydraRenderPlugin::ExtractPhotometricLightParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
{
  auto  pLight                                     = dynamic_cast<LightscapeLight*>(light);
  if (!pLight)
    return;

  float intensCd                                   = pLight->GetResultingIntensity(t, valid);
  float sizeX                                      = ToMeters(pLight->GetLength(t, valid));
  float sizeY                                      = ToMeters(pLight->GetWidth (t, valid));
  float radius                                     = ToMeters(pLight->GetRadius(t, valid));
  std::wstring type                                = L"";
  std::wstring distribution                        = L"";
  std::wstring shape                               = L"";

  LightscapeLight::DistTypes lightDistributionType = pLight->GetDistribution();
  bool isVisible                                   = IsPhotometricVisible(light, t, valid);  
  Matrix3 lightTransform                           = ToMetersM3(node->GetObjectTM(t));

  float area = 1.0F;
  
  // Scaling the light source affects its area.
  lightTransform.SetRow(3, Point3(0.0F, 0.0F, 0.0F)); // kill translate
  float kf             = scaleWithMatrix(1.0F, lightTransform);
  float scaleIntensity = kf * kf;

  switch (pLight->Type())
  {
    case  LightscapeLight::TARGET_POINT_TYPE:
    case  LightscapeLight::POINT_TYPE:
      type           = L"point";
      shape          = L"point";
      sizeX          = 1.0F;
      sizeY          = 1.0F;
      scaleIntensity = 1.0F;

      if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
        distribution = L"spot";
      else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
      {
        distribution = L"ies";
        scaleIntensity *= PI;
      }
      else
      {
        distribution = L"diffuse";
        scaleIntensity *= PI;
      }
      break;

    case  LightscapeLight::TARGET_SPHERE_TYPE:
    case  LightscapeLight::SPHERE_TYPE:
      area         = 4.0F * PI * radius * radius; // because using half sizes 
      type         = L"area";
      shape        = L"sphere";
      sizeX        = radius;
      sizeY        = radius;
      distribution = L"diffuse";
      break;

    case  LightscapeLight::TARGET_CYLINDER_TYPE:
    case  LightscapeLight::CYLINDER_TYPE:
      area         = TWOPI * radius * sizeX; // because using half sizes 
      type         = L"area";
      shape        = L"cylinder";
      sizeY        = radius;
      distribution = L"diffuse";
      break;

    case LightscapeLight::TARGET_DISC_TYPE:
    case LightscapeLight::DISC_TYPE:
      area      = PI * radius * radius; // because using half sizes 
      type      = L"area";
      shape     = L"disk";
      sizeX     = radius;
      sizeY     = radius;

      if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
        distribution = L"spot";
      else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
        distribution = L"ies";
      else
        distribution = L"diffuse";
      break;

    case LightscapeLight::TARGET_AREA_TYPE:
    case LightscapeLight::AREA_TYPE:
      area      = sizeX * sizeY; // because using half sizes 
      type      = L"area";
      shape     = L"rect";
      sizeX     = sizeX * 0.5F;
      sizeY     = sizeY * 0.5F;

      if (lightDistributionType == LightscapeLight::DistTypes::SPOTLIGHT_DIST)
        distribution = L"spot";
      else if (lightDistributionType == LightscapeLight::DistTypes::WEB_DIST)
        distribution = L"ies";
      else
        distribution = L"diffuse";
      break;

    default:      
      break;
  }

  lightNode.force_attribute(L"type").set_value(type.c_str());
  lightNode.force_attribute(L"shape").set_value(shape.c_str());
  lightNode.force_attribute(L"distribution").set_value(distribution.c_str());

  area                      *= scaleIntensity;

  auto intensityNode         = lightNode.force_child(L"intensity");
  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(intensCd / 683.0F / fmax(area, 0.000001F));

  struct LightState ls;
  light->EvalLightState(t, valid, &ls);
  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(ls.color).c_str());

  auto sizeNode              = lightNode.force_child(L"size");

  if (shape                  == L"cylinder")
  {
    sizeNode.force_attribute(L"radius").set_value(sizeY);
    sizeNode.force_attribute(L"height").set_value(sizeX);
    sizeNode.force_attribute(L"angle").set_value(360.0F);
  }
  else if (shape == L"sphere")
    sizeNode.force_attribute(L"radius").set_value(sizeX);
  else
  {
    sizeNode.force_attribute(L"radius").set_value(sizeX);
    sizeNode.force_attribute(L"half_length").set_value(sizeY);
    sizeNode.force_attribute(L"half_width").set_value(sizeX);
  }

  lightNode.attribute(L"visible").set_value(isVisible ? 1 : 0);

  if (distribution == L"ies")
    ExtractIESData(light, lightNode);

  if (distribution == L"spot")
  {
    lightNode.force_child(L"falloff_angle").force_attribute(L"val").set_value(ls.fallsize);
    lightNode.force_child(L"falloff_angle2").force_attribute(L"val").set_value(ls.hotsize);
  }
}



void HydraRenderPlugin::ExtractAndInstanceLights(int& lightsNum, TimeValue t)
{
  ExtractEnvironmentLight(t);

  for (auto it = g_hrPlugResMangr.m_lightNodesPools.begin(); it != g_hrPlugResMangr.m_lightNodesPools.end(); ++it)
  {
    auto m_representativeNode = (*it)->GetRepresentativeNode();

    if (m_representativeNode == nullptr || (*it)->instancesPool.size() == 0)
      continue;

    auto os = m_representativeNode->EvalWorldState(t);
    auto light = static_cast<LightObject*>(os.obj);

    if (!os.obj || os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0) || !light->GetUseLight())
      continue;

    bool isNew = true;
    HRLightRef lightRef;

    auto findLightRef = g_hrPlugResMangr.m_lightsRefLib.find(os.obj);

    if (findLightRef == g_hrPlugResMangr.m_lightsRefLib.end())
    {
      lightRef = hrLightCreate(m_representativeNode->GetName());
      g_hrPlugResMangr.m_lightsRefLib[os.obj] = lightRef;
    }
    else
    {
      lightRef = findLightRef->second;
      isNew = false;
    }

    if ((*it)->needExport)
    {
      ExtractLightObject(m_representativeNode, t, lightRef, isNew);
      (*it)->needExport = false;
    }

    for (auto j = (*it)->instancesPool.begin(); j != (*it)->instancesPool.end(); ++j)
    {
      auto instanceOs         = (*j)->EvalWorldState(t);
      auto instanceLight      = static_cast<LightObject*>(instanceOs.obj);
      auto renderThisInstance = instanceLight->GetUseLight();

      if (renderThisInstance)
      {
        float matrixT[4][4];      
        auto pivot       = ToMetersM3((*j)->GetObjectTM(t));
        auto matrixFinal = TransformMatrixFromMax(pivot);
        MaxMatrix3ToFloat16V2(matrixFinal, &matrixT[0][0]);

        int  instId      = hrLightInstance(m_currentScene, g_hrPlugResMangr.m_lightsRefLib[os.obj], &matrixT[0][0]);
        auto instNode    = hrGetLightInstanceNode(m_currentScene, instId);
        ExtractRawTransformAndAddXmlNode(instNode, pivot);

        lightsNum += 1;
      }
    }
  }
}

void HydraRenderPlugin::ExtractEnvironmentLight(TimeValue t)
{
  Texmap* globEnvTex  = GetCOREInterface()->GetEnvironmentMap();
  BOOL    useMap      = GetCOREInterface()->GetUseEnvironmentMap();
  Point3 bgColor      = GetCOREInterface()->GetBackGround(t, FOREVER);
  Point3 amColor      = GetCOREInterface()->GetAmbient(t, FOREVER);
  Point3 tintColor    = GetCOREInterface()->GetLightTint(t, FOREVER);
  float  tintLevel    = GetCOREInterface()->GetLightLevel(t, FOREVER);

  float intensity = 1.0f;

  HR_OPEN_MODE mode = HR_WRITE_DISCARD;
  if (m_envRef.id < 0)//g_hrPlugResMangr.m_firstRender)
  {
    //mode = HR_WRITE_DISCARD;
    m_envRef = hrLightCreate(L"environment");
  }
  else
  {
    //mode = HR_OPEN_EXISTING;
  }

  bool doNotUpdateEnv = false;

  if (m_compressMeshOnExport)
    doNotUpdateEnv = true;
  else
    doNotUpdateEnv = m_rendParams.debugCheckBoxes[L"BLACK_IMAGE"]; //#TODO: fix this inside HydraAPI (via memcmp or hash)

  hrLightOpen(m_envRef, mode);
  {
    auto lightNode = hrLightParamNode(m_envRef);
    auto intensityNode = lightNode.force_child(L"intensity");
    lightNode.force_attribute(L"type").set_value(L"sky");
    if (useMap && globEnvTex != nullptr && !m_hasSky)
    {
      texInfo backTex;
      Texmap* backEnvTex;
      Texmap* envMap = globEnvTex;
      Texmap* subTexBack = nullptr;

      if (globEnvTex->ClassID() == HydraBackEnvir_CLASS_ID) // HydraBack/Envir node
      {
        backEnvTex          = globEnvTex;
        subTexBack          = backEnvTex->GetSubTexmap(0);
        Texmap* subTexEnvir = backEnvTex->GetSubTexmap(1);
        Point3 backColor    = FindColor(L"BackColor", backEnvTex);
        Point3 envColor     = FindColor(L"EnvirColor", backEnvTex);

        backTex = BakeTexture(nullptr, subTexBack, L"", std::get<0>(backTex), L"", doNotUpdateEnv);

        envMap = subTexEnvir;
        bgColor = envColor;
      }

      envTex = BakeTexture(nullptr, envMap, L"", std::get<0>(envTex), L"", doNotUpdateEnv);

      if (std::get<0>(envTex).id == -1)
      {
        lightNode.force_attribute(L"distribution").set_value(L"uniform");
        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(bgColor * tintColor).c_str());
      }
      else
      {
        lightNode.force_attribute(L"distribution").set_value(L"map");
        intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(tintColor).c_str());

        auto texNode = hrTextureBind(std::get<0>(envTex), intensityNode.force_child(L"color"));
        if (std::get<1>(envTex))
        {
          ExtractSampler(texNode, envTex, envMap);

          if (envMap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
            FixEnvironmentTexOffset(texNode);
        }
      }

      // Background
      if (globEnvTex->ClassID() == HydraBackEnvir_CLASS_ID) // HydraBack/Envir node
      {
        auto backNode = lightNode.force_child(L"back");
        auto texNode2 = hrTextureBind(std::get<0>(backTex), backNode);
        if (std::get<1>(backTex))
        {
          ExtractSampler(texNode2, backTex, subTexBack);

          if (subTexBack != nullptr && subTexBack->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
            FixEnvironmentTexOffset(texNode2);
        }
      }
    }
    else if (m_hasSky) // Because environment is always exported, and the HydraSunSky light only changes when.
    {
      lightNode.force_attribute(L"type").set_value(L"sky");
      lightNode.force_attribute(L"distribution").set_value(L"perez");

      intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(m_perezSky.skyTint).c_str());
      intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(m_perezSky.skyMult);

      auto sunModel = lightNode.force_child(L"perez");
      sunModel.force_attribute(L"sun_id") = m_sunRef.id;
      sunModel.force_attribute(L"turbidity").set_value(m_perezSky.skyHaze);
    }
    else
    {
      lightNode.force_attribute(L"distribution").set_value(L"uniform");
      intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(bgColor * tintColor).c_str());
    }

    intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(m_rendParams.env_mult * tintLevel);
  }
  hrLightClose(m_envRef);

  float matrixT[16] ={ 1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1 };

  hrLightInstance(m_currentScene, m_envRef, matrixT);
}

void HydraRenderPlugin::ExtractLightObject(INode* node, TimeValue t, HRLightRef lightRef, bool isNew)
{
  ObjectState os = node->EvalWorldState(t);
  if (!os.obj)
    return;

  LightObject* light = static_cast<LightObject*>(os.obj);
  Texmap* envMap = GetCOREInterface()->GetEnvironmentMap();

  if (light->GetUseLight())
  {
    Interval valid = FOREVER;
    HR_OPEN_MODE mode;
    if (isNew)
      mode = HR_WRITE_DISCARD;
    else
      mode = HR_OPEN_EXISTING;

    hrLightOpen(lightRef, mode);
    {
      auto lightNode = hrLightParamNode(lightRef);

      if      (IsHydraLight(light))       ExtractHydraLightParams(light, node, lightNode, t, valid);
      else if (IsHydraLight2(light))      ExtractHydraLight2Params(light, node, lightNode, t, valid);
      else if (IsHydraSky(light))         ExtractHydraSunSkyParams(light, lightRef, node, lightNode, t, valid);
      else if (IsPhotometricLight(light)) ExtractPhotometricLightParams(light, node, lightNode, t, valid);
      else ExtractStdLightParams(light, node, lightNode, t, valid);

    }
    hrLightClose(lightRef);
  }
}


bool HydraRenderPlugin::IsPhotometricLight(LightObject* a_light)
{
  return a_light->IsSubClassOf(LIGHTSCAPE_LIGHT_CLASS);
}



bool HydraRenderPlugin::IsHydraLight(LightObject* a_light)
{
  return a_light->IsSubClassOf(HydraLight_CLASS_ID);
}

bool HydraRenderPlugin::IsHydraLight2(LightObject* a_light)
{
  return a_light->IsSubClassOf(HydraLight2_CLASS_ID);
}


bool HydraRenderPlugin::IsHydraSky(LightObject* a_light)
{
  return a_light->IsSubClassOf(HydraSunSky_CLASS_ID);
}



/////////////////////////////////////////////////////////////////
// Old unused functions left for reference for the time being  //
/////////////////////////////////////////////////////////////////


//void HydraRenderPlugin::ExtractSunParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
//{
//  BaseInterface* bi = light->GetInterface(IID_MR_PHYSICAL_SUN_LIGHT);
//  IMRPhysicalSunLight* mrSun = dynamic_cast<IMRPhysicalSunLight*>(bi);
//
//  float redBlueShift = mrSun->getRedBlueShift(t, valid);
//  float shiftT = fabs(redBlueShift);
//
//  Color color = FindColor(L"rgb", light);
//  float mult = FindFloat(L"intensity", light);  // 99221.8
//  float lMult = FindFloat(L"Multiplier", light); // mrSun->getSkyMultiplier(t, valid); // simple mult
//  //float phRad = ToMeters(FindFloat(L"Photon Target", light));
//
//  ULONG classId = light->ClassID().PartA();
//  bool isTargeted = false;
//  Matrix3 targetMatrix;
//  INode* targetNode = node->GetTarget();
//  if (targetNode != nullptr)
//    targetMatrix = ToMeters(targetNode->GetObjectTM(t));
//
//  isTargeted = (targetNode != nullptr);
//
//  Matrix3 lightMatrix = ToMeters(node->GetObjectTM(t));
//  Point3 lightDirPoint = -node->GetNodeTM(t).GetRow(2);
//
//  if(isTargeted)
//  {
//    Point3 pos, target;
//
//    pos = lightMatrix*Point3(0, 0, 0);
//    target = targetMatrix*Point3(0, 0, 0);
//    lightDirPoint = (target - pos).Normalize();
//  }
//
//  float angleT = sqrtf(fabs(lightDirPoint.z));
//
//  Color colorZenit = color;
//  Color colorHorizon = Color(1.0f, 0.4f, 0.05f);
//
//  color = colorZenit*angleT + colorHorizon*(1.0f - angleT);
//
//  if (redBlueShift < 0.0f)
//    color = color*(1.0f - shiftT) + Color(0.1f, 0.8f, 1.0f);
//  else if (redBlueShift > 0.0f)
//    color = color*(1.0f - shiftT) + Color(1.0f, 0.6f, 0.1f);
//
//  lightNode.force_attribute(L"type").set_value(L"directional");
//  lightNode.force_attribute(L"shape").set_value(L"point");
//  lightNode.force_attribute(L"distribution").set_value(L"directional");
//
//  auto sizeNode = lightNode.force_child(L"size");
//  sizeNode.force_attribute(L"inner_radius").set_value(L"0.0");
//  sizeNode.force_attribute(L"outer_radius").set_value(L"1000.0");
//
//  auto intensityNode = lightNode.force_child(L"intensity");
//
//  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(color).c_str());
//  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(1.0f*(mult*lMult / 100000.0f));
//
//  lightNode.force_child(L"shadow_softness").force_attribute(L"val").set_value(mrSun->getShadowSoftness(t, valid)); 
//}

//void HydraRenderPlugin::ExtractPhysSkyParams(LightObject *light, pugi::xml_node lightNode, TimeValue t, Interval &valid)
//{
//  BaseInterface* skyInterface = light->GetInterface(MRPHYSSKY_LIGHT_INTERFACECLASS_ID);
//  IMRPhysicalSkyInterface* physSkyInterface = static_cast<IMRPhysicalSkyInterface*>(skyInterface);
//  if (physSkyInterface != nullptr)
//  {
//    float skyHaze = physSkyInterface->getHaze(t, valid);
//    float skyMult = physSkyInterface->getMultiplier(t, valid);
//
//    lightNode.force_attribute(L"type").set_value(L"sky");
//    lightNode.force_attribute(L"distribution").set_value(L"perez");
//
//    lightNode.force_child(L"intensity").force_child(L"color").force_attribute(L"val").set_value(L"1 1 1");
//    lightNode.force_child(L"intensity").force_child(L"multiplier").force_attribute(L"val").set_value(skyMult);
//
//    auto sunModel = lightNode.force_child(L"perez");
//
//    sunModel.force_attribute(L"sun_name").set_value(m_sunSkyNameW.c_str());
//    sunModel.force_attribute(L"turbidity").set_value(skyHaze + 1.0f);
//  }
//}

//void HydraRenderPlugin::ExtractMrSkyPortalParams(LightObject *light, INode* node, pugi::xml_node lightNode, TimeValue t, Interval &valid)
//{
//  BaseInterface* bi = light->GetInterface(IID_MR_SKY_PORTAL_LIGHT);
//  IMrSkyPortalLight* mrSkyPortal = dynamic_cast<IMrSkyPortalLight*>(bi);
//
//  Matrix3 lightTransform = ToMeters(node->GetObjectTM(t));
//
//  struct LightState ls;
//  light->EvalLightState(t, valid, &ls);
//
//  float kf = scaleWithMatrix(1.0f, lightTransform);
//  int unitDispType = GetUnitDisplayType();
//  float scaleIntensity = (unitDispType == UNITDISP_GENERIC) ? (1.0f / 75.0f) : 1.0f;
//  scaleIntensity *= 1.0f / (kf * kf);
//
//  auto intensityNode = lightNode.force_child(L"intensity");
//  intensityNode.force_child(L"color").force_attribute(L"val").set_value(strConverter::colorToWideString(ls.color).c_str());
//  intensityNode.force_child(L"multiplier").force_attribute(L"val").set_value(scaleIntensity * mrSkyPortal->GetMultiplier(t, valid));
//
//  lightNode.force_attribute(L"type").set_value(L"area");
//  lightNode.force_attribute(L"shape").set_value(L"rect");
//  lightNode.force_attribute(L"distribution").set_value(L"uniform");
//  lightNode.force_attribute(L"visible").set_value(int(mrSkyPortal->GetVisibleInRendering(t, valid)));
//
//  auto sizeNode = lightNode.force_child(L"size");
//
//  // In core "length" = "width" in 3ds max.
//  sizeNode.force_attribute(L"half_length").set_value(ToMeters(mrSkyPortal->GetWidth(t, valid))*0.5f);
//  sizeNode.force_attribute(L"half_width").set_value(ToMeters(mrSkyPortal->GetLength(t, valid))*0.5f);
//  
//
//  auto portalNode = lightNode.force_child(L"sky_portal");
//  portalNode.force_attribute(L"val").set_value(1);
//  
//  int mode = FindInt(L"Mode", light);
//
//  if (mode == 0)
//  {
//    portalNode.force_attribute(L"source_id").set_value(m_envRef.id);
//  }
//  else if (mode == 2)
//  {
//    portalNode.force_attribute(L"source_id").set_value(-1); // skylight
//  }
//  else if (mode == 1)
//  {
//    Texmap* pCustomMap = mrSkyPortal->GetIlluminationMap();
//    BitmapTex* pBmap = static_cast<BitmapTex*>(pCustomMap);
//
//    texInfo oneTex = BakeTexture(pCustomMap);
//
//    if (pCustomMap != nullptr && std::get<0>(oneTex).id != -1)
//    {
//      auto texNode = hrTextureBind(std::get<0>(oneTex), intensityNode.child(L"color"));
//      if (std::get<1>(oneTex))
//      {
//        ExtractSampler(texNode, oneTex);
//        FixEnvironmentTexOffset(texNode);
//      }
//
//      std::wstring tempStr = strConverter::ToWideString(pBmap->GetMapName());
//      portalNode.force_attribute(L"source_id").set_value(-1); //custom
//    }
//    else 
//      portalNode.force_attribute(L"source_id").set_value(-1);
//  }
//}


//bool HydraRenderPlugin::IsHydraScriptLight(LightObject* a_light)
//{
//  return a_light->IsSubClassOf(HydraScriptLight_CLASS_ID);
//}


//if(IsNewSunSky(envMap))
//{
//   IPhysicalSunSky* const physical_sun_sky = dynamic_cast<IPhysicalSunSky*>(envMap);
//   const IPhysicalSunSky::ShadingParameters params = physical_sun_sky->EvaluateShadingParameters(t, INFINITE);
//}

//bool  HydraRenderPlugin::IsMrSun(LightObject* a_light)
//{
//  BaseInterface* bi = a_light->GetInterface(IID_MR_PHYSICAL_SUN_LIGHT);
//  IMRPhysicalSunLight* mrSun = dynamic_cast<IMRPhysicalSunLight*>(bi);
//  if (mrSun != nullptr)
//    return true;
//  return false;
//}
//
//bool  HydraRenderPlugin::IsPhysSky(LightObject* a_light)
//{
//  BaseInterface* skyInterface = a_light->GetInterface(MRPHYSSKY_LIGHT_INTERFACECLASS_ID);
//  IMRPhysicalSkyInterface* physSkyInterface = static_cast<IMRPhysicalSkyInterface*>(skyInterface);
//  if (physSkyInterface != nullptr)
//    return true;
//  return false;
//}
//
//bool  HydraRenderPlugin::IsNewSunSky(Texmap* a_env)
//{
//  MaxSDK::IPhysicalSunSky* const physical_sun_sky = dynamic_cast<MaxSDK::IPhysicalSunSky*>(a_env);
//  if (physical_sun_sky != nullptr)
//    return true;
//  return false;
//}

//bool HydraRenderPlugin::IsMrSkyPortal(LightObject* a_light)
//{ 
//   BaseInterface* bi = a_light->GetInterface(IID_MR_SKY_PORTAL_LIGHT);
//   IMrSkyPortalLight* mrSkyPortal = dynamic_cast<IMrSkyPortalLight*>(bi);
//   if(mrSkyPortal != nullptr)
//     return true;
//   return false;
//}

