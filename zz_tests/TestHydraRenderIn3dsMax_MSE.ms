/*
Last change:  17.03.2023
Author: Ray Tracing Systems.
Run defferent scenes for comparisson render with reference images.
*/

-------------------------------------------------------------------------
-------- RUN THIS SCRIPT IN 3DS MAX 2021 (with administrator privileges) or later ---------
-------------------------------------------------------------------------

rootPath 	    = @"d:\Works\Ray-Tracing_Systems\Develop\3dsMaxPlugin\zz_tests\"

runTestsInRange = #(8, 8)


-------------------------------------------------------------------------
-- Main
-------------------------------------------------------------------------
(--start ms	
	
	fn ComparisonImgMse a_rootPath a_rendImg a_refImg &a_errorMsg &mse = 
	(
		--print (formattedPrint "render name:" format:"-16s" + a_rendImg)
		--print (formattedPrint "reference name:" format:"-16s" + a_refImg)
		
		if(doesFileExist a_refImg ignoreCache:true) then
		(					
			HiddenDOSCommand  ( a_rootPath + "imagemse.exe" + " " +  a_rendImg + " " +  a_refImg + " > " +   a_rootPath + "mse.txt")
			fileMse 		= openFile (a_rootPath + "mse.txt")	
			mse 			= readLine fileMse				
			close fileMse					
		)
		else
		(			
			 a_errorMsg 	= "not load reference."
		)
	)		
	
	
	fn PrintResult a_mse a_fileName &a_rendImg &a_numDifferScenes &a_logFile = 
	(
		res = ""
		
		if (a_mse as float < 30) then
		(
			res = "ok."
			deleteFile a_rendImg
		)
		else
		(			
			res = "FALSE!"
			a_numDifferScenes += 1
		)						
		
		message = formattedPrint a_fileName format:"-50s" + formattedPrint res format:"-10s" +  "mse = " + a_mse + "\n"			
		format message
		format message to:a_logFile		
	)
	
	
	
	
	escapeEnable = true	
	
	actionMan.executeAction 0 "40005"  -- File: Reset File	
	actionMan.executeAction 0 "40472"  -- MAXScript Listener
	clearListener()
	
	if (not isDirectoryWriteable rootPath) do
	(
		print "Not exist root path. Please change it to an existing one." 
		return 0
	)

	
	print "Start tests.\n"
	
	scenesPath 			= rootPath + @"scenes\"	
	referencePath 		= rootPath + @"reference\"
	renderOutputPath 	= rootPath + @"renderoutput\"

	wasCancelled			= false
	numDifferScenes 	= 0	
	numScenes 			= 0	
		
	logFile 		  	 		= createFile (rootPath + "Results.txt")		
	maxFiles 		 		= getFiles (scenesPath + "*.max")
	numAllScenes		= maxFiles.count as integer
	
	progressStart "Progress tests"
	
	if (not wasCancelled) do
	(
		-- Loop load scenes.
		for f in maxFiles do
		(			
			numScenes += 1
			
			progressUpdate ((numScenes as float) / (numAllScenes as float) * 100.0)
			if (getProgressCancel()) do exit	
				
			fileName = getFilenameFile f					

			numTests = (filterString fileName "_")[1] as integer		
			
			-- Test out of range, skip.
			if (numTests < runTestsInRange[1] or numTests > runTestsInRange[2]) then
			(
				message = formattedPrint fileName format:"-50s" + "skip.\n"	
				format message
				format message to:logFile		
				continue
			)					
						
			-- Load scene.
			if (loadMAXFile f useFileUnits:true quiet:true) then
			()			
			else
			(
				-- Scene not load.
				message = formattedPrint fileName format:"-50s" + "FALSE! The scene is not loaded.\n"	
				format message
				format message to:logFile				
				continue
			)		
						
			
			if (fileName == "008_RenderElements") then
			(		
				mse          = ""
				errorMsg	= ""
				renderElementsPath = renderOutputPath	+ @"RenderElements\"
				refElementsPath = referencePath	+ @"RenderElements\"
				---------------------------------
				-- Render
				---------------------------------
				
				render vfb:true outputFile:(renderElementsPath + fileName + ".png")	cancelled:&wasCancelled	
				
				rendFiles	= getFiles (renderElementsPath + "*.png")
								
				for rendImg in rendFiles do
				(
					nameElement = getFilenameFile rendImg
					
					refImg = refElementsPath + nameElement + ".png"
					
					---------------------------------
					-- Compare and Print log
					---------------------------------
					ComparisonImgMse rootPath rendImg refImg &errorMsg &mse
					PrintResult mse nameElement &rendImg &numDifferScenes &logFile
				)
			)		
			else
			(
				mse          = ""
				errorMsg	= ""
				
				---------------------------------
				-- Render
				---------------------------------
				render vfb:true outputFile:(renderOutputPath + fileName + ".png")	cancelled:&wasCancelled	
			
				---------------------------------
				-- Compare and Print log
				---------------------------------		
				bmRender = renderOutputPath + fileName + ".png"
				refImg 		= referencePath	+ fileName + ".png"	
				
				ComparisonImgMse rootPath bmRender refImg &errorMsg &mse
				PrintResult mse &message fileName &bmRender errorMsg &numDifferScenes &logFile
			)			
		)	
	)
	
	progressEnd() 
	close logFile
	
	format "\n"	
	print "Tests completed. Results:"
	format "\n"		
	format "Different % scenes from the % \n\n" numDifferScenes numAllScenes
	format "The log file is saved here:\n% \n" (rootPath + "Results.txt")	
	
	actionMan.executeAction 0 "40005" -- File: Reset File
)-- end ms