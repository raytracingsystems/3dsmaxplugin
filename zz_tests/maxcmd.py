# -*- coding: utf-8 -*-

"""
Created on Wed Jul 2016
@author: Lime
"""

import os, subprocess

max_path        = 'C:/Program Files/Autodesk/3ds Max 2019/3dsmaxcmd.exe' 
scenefiles_dir  = 'scenes'
images_dir      = 'renderoutput'
preset          = 'test_common.rps'

use_ldr         = True
compare_prog    = "image_compare_prog\\mse.exe"
reference_dir   = "reference"
img_format      = '.tiff' if use_ldr else '.exr'

run_list        = [(0,400)]


debug_mode      = False

results         = open("zresults.txt", "w")

i = 0
for scenename in os.listdir(scenefiles_dir):

  if scenename in ["textures"]:
    continue

  ''' ------------------- begin skip block -------------------  '''
  id = int(scenename[0:3])
 
  skipNow = True
  for (test_first, test_last) in run_list: 
    if id >= test_first and id <= test_last:
      skipNow = False
    
  if skipNow:
    i = i+1
    results.write('test ' + str(i).zfill(3) + ': {:50s} {:8s} {:3.2f} \n'.format(scenename, "skipped", 0))
    continue
  
  ''' ------------------- end  skip block -------------------  '''
  
  outImageName  = scenename                                + img_format
  outImageName2 = images_dir    + "/" + scenename + '0000' + img_format
  refImage      = reference_dir + "/" + scenename + '0000' + img_format

  cmdLine       = max_path + ' \"' + scenefiles_dir + '/' + scenename + '\"' + ' -outputName:\"' + images_dir + '/' + outImageName + '\"'
  if use_ldr:
    cmdLine += ' -gammaCorrection:"1" -TIF_TYPE:"1" -TIF_ALPHA:"1" -TIF_COMPRESSION:"0" ' #  -gammaValueIn:"2.2" -gammaValueOut:"2.2"
    
  #cmdLine += ' -showRFW:"0" ' # show Rendered Frame Window
    
  print(cmdLine)
  
  if not debug_mode:
    subprocess.call(cmdLine)
  
  imageRef = os.path.abspath(refImage)
  imageOut = os.path.abspath(outImageName2)
  imageDif = os.path.abspath("z_mse.txt")
  
  cmd2 = compare_prog + " \"" + imageRef + "\" \"" + imageOut + "\" \"" + imageDif + "\""
  print(cmd2)
  if not debug_mode:
    os.system(cmd2)
  
  myfile = open(imageDif, "r")
  mse    = float(myfile.read())
  print(scenename + " " + str(mse))
    
  if mse < 30.0:
    results.write('test ' + str(i).zfill(3) + ': {:50s} {:8s} {:3.2f} \n'.format(scenename, "ok", mse)) 
  else:
    results.write('test ' + str(i).zfill(3) + ': {:50s} {:8s} {:3.2f} \n'.format(scenename, "FAILED!", mse))
  
  i = i+1
  results.flush()
  
  
