(
fn ComparisonImgMse a_rootPath a_rendImg a_refImg &a_errorMsg &mse = 
	(
		print (formattedPrint "render name:" format:"-16s" + a_rendImg)
		print (formattedPrint "reference name:" format:"-16s" + a_refImg)
		
		if(doesFileExist a_refImg ignoreCache:true) then
		(					
			HiddenDOSCommand  ( a_rootPath + "imagemse.exe" + " " +  a_rendImg + " " +  a_refImg + " > " +   a_rootPath + "mse.txt")
			fileMse 		= openFile (a_rootPath + "mse.txt")	
			mse 			= readLine fileMse				
			format "MSE: %\n" mse
			close fileMse						
		)
		else
		(			
			 a_errorMsg 	= "not load reference."
		)
	)	

	
clearListener()
	
rootPath 	    		= @"d:\Works\Ray-Tracing_Systems\Develop\3dsMaxPlugin\zz_tests\"
referencePath 		= rootPath + @"reference\"
renderOutputPath 	= rootPath + @"renderoutput\"
	
renderElementsPath 	= renderOutputPath	+ @"RenderElements\"
refElementsPath 		= referencePath			+ @"RenderElements\"

rendFiles	= getFiles (renderElementsPath + "*.png")
								
for rendImg in rendFiles do
(
	mse          = 0
	nameElement = getFilenameFile rendImg
					
	refImg = refElementsPath + nameElement + ".png"
					
	ComparisonImgMse rootPath rendImg refImg &errorMsg &mse
					
)
)