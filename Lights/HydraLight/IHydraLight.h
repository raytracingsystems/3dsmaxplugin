#pragma once
#include "gfx.h"
#include "lslights.h"

////////////////////////////////////////////////////////////////////////////////////////////////
// DESCRIPTION: Hydra material
// AUTHOR: RAY TRACING SYSTEMS
////////////////////////////////////////////////////////////////////////////////////////////////

enum HLType { HL_TYPE_POINT, HL_TYPE_RECTANGLE, HL_TYPE_DISK, HL_TYPE_SPHERE, HL_TYPE_CYLINDER, HL_TYPE_PORTAL };


class IHydraLight : public LightscapeLight, public Light // interface for HydraLight compatible lights.
{
public:
  virtual ~IHydraLight() = default;

  // Export to Hydra render

  virtual HLType HlType() = 0; // Get Hydra light type from GUI.
};
