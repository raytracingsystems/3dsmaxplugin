#pragma once

#include "resource.h"
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <maxtypes.h>

#include "prim.h"        
#include "target.h"
#include "shadgen.h"
#include "notify.h"     
#include "macrorec.h"    
#include "decomp.h"      
//#include "INodeGIProperties.h"
//#include "INodeShadingProperties.h"
#include "IViewportShadingMgr.h"
#include "Graphics/Utilities/MeshEdgeRenderItem.h"
#include "Graphics/Utilities/SplineRenderItem.h"
#include "Graphics/CustomRenderItemHandle.h"
//#include "Graphics/RenderNodeHandle.h"
#include "MouseCursors.h"
#include "3dsmaxport.h" 

#include "KelvinTable.h"
#include "IHydraLight.h"

////////////////////////////////////////////////////////////////////////////////////
//
// The development of this light source has been discontinued and replaced
// with the HydraLight2 plugin, based on a 3d max script.
//
////////////////////////////////////////////////////////////////////////////////////

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define HydraLight_CLASS_ID	Class_ID(0x69729747, 0x8c40aaf7)

enum              { PB_MAIN_REF, PB_VIEWPOR_REF, NUM_PB_REF };
enum              { PB_MAIN, PB_VIEWPORT };
enum HLIntensUnit { INTENS_UNIT_IMAGE, INTENS_UNIT_CD, INTENS_UNIT_LM } ;

constexpr int   VERSION_CURRENT = 0;
                
constexpr int   NUM_CIRC_PTS    = 28;
constexpr int   SEG_INDEX       = 7;

constexpr float COS_45          = 0.7071067F;
constexpr float COS_45_2X       = 1.4142136F;

#define Dx (ray.dir.x)
#define Dy (ray.dir.y)
#define Dz (ray.dir.z)
#define Px (ray.p.x)
#define Py (ray.p.y)
#define Pz (ray.p.z)

using hydraStr  = std::wstring;
using hydraChar = wchar_t;

static int waitPostLoad = 0;


// Main parameters. Don`t delete param from enum list.
enum
{
  pb_on,
  pb_targeted,
  pb_visInRender,
  pb_type,
  pb_excludeList,
  pb_intensity,
  pb_intensityUnits,
  pb_color,
  pb_kelvin,
  pb_kelvinTemp,
  pb_length,
  pb_width,
  pb_radius,
  pb_WEB_on,
  pb_WEB_file,
  pb_WEB_rotX,
  pb_WEB_rotY,
  pb_WEB_rotZ,
  pb_spotlight,
  pb_showCone,
  pb_hotspot,
  pb_fallsize
};

// Viewport parameters
enum
{
  pb_simpleShape
};


////////////////////////////////////////////////////////////////////////////////////

class HydraLight: public IHydraLight
{
public:
  // Class vars
  static IObjParam* ip;

  // Object parameters
  IParamBlock2* pblockMain     = nullptr;
  IParamBlock2* pblockViewport = nullptr;

  explicit HydraLight(bool loading);
  ~HydraLight()                                                                 override;

  // Class tree: Animatable->ReferenceMaker->ReferenceTarget->BaseObject->Object->LightObject
  // inherited virtual methods:
  // From Animatable
  void            BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev) override;
  void            EndEditParams  (IObjParam *ip, ULONG flags, Animatable *next) override;
  void            DeleteThis()                                         noexcept override { delete this; }
  int             NumSubs()                                            noexcept override { return 1; }

#ifdef MAX2022
  MSTR            SubAnimName(int i, bool localized = false)                    override;
#else
  TSTR            SubAnimName(int i)                                            override;
#endif // MAX2022

  
  Animatable*     SubAnim(int i)                                                override;
  int	            NumParamBlocks()                                     noexcept override { return NUM_PB_REF; }	// return number of ParamBlocks in this instance
  IParamBlock2*   GetParamBlock(int i)                                          override; // return i'th ParamBlock
  IParamBlock2*   GetParamBlockByID(BlockID id)                                 override; // return id'd ParamBlock
  
  //virtual IOResult        Save(ISave *isave);
  IOResult        Load(ILoad* iload)                                            override;


  // From ReferenceMaker
  RefResult       NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, 
    RefMessage message, BOOL propagate)                                         override;

  int             NumRefs()                                            noexcept override { return NUM_PB_REF; }   // Maintain the number or references here
  RefTargetHandle GetReference(int i)                                           override;
  void            SetReference(int i, RefTargetHandle rtarg)                    override;


  // From ReferenceTarget
  RefTargetHandle Clone(RemapDir &remap)                                        override;


  // From BaseObject
  int             HitTest(TimeValue t, INode* inode, int type, int crossing, 
                          int flags, IPoint2 *p, ViewExp *vpt)                  override;
  CreateMouseCallBack * GetCreateMouseCallBack()                                override;

  // Disable legasy method Dysplay(), because use new 3ds Max graphics API. 
  //int           Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) override;
  void            GetWorldBoundBox(TimeValue t, INode *inode, ViewExp* vpt, Box3& box) override;
  void            GetLocalBoundBox(TimeValue t, INode *inode, ViewExp* vpt, Box3& box) override;
  //void 	        Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt) override;

#ifdef MAX2022
  const MCHAR* GetObjectName(bool localized) const override { return GetString(IDS_CLASS_NAME); }
#else
  const TCHAR*    GetObjectName() override { return GetString(IDS_CLASS_NAME); }
#endif // MAX2022

  BOOL            HasViewDependentBoundingBox() noexcept override { return false; }
  void            SetExtendedDisplay(int flags) noexcept override { m_extDispFlags = flags; }
  [[nodiscard]] int GetExtendedDisplay()  const noexcept          { return m_extDispFlags; }

  // From Object
  Interval        ObjectValidity(TimeValue t)            override;
  void            GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel) override;
  int 			      DoOwnSelectHilite()           noexcept override { return 1; }


  // From LightObject
  SClass_ID       SuperClassID()                noexcept override { return LIGHT_CLASS_ID; }
  void            InitNodeName(MSTR& s)                  override { s = GetString(IDS_CLASS_NAME); }
  Class_ID        ClassID()                              override { return HydraLight_CLASS_ID; }

#ifdef MAX2022
  void            GetClassName(MSTR& s, bool localized = true) const override { s = GetString(IDS_CLASS_NAME); }
#else  
  void            GetClassName(TSTR& s) override { s = GetString(IDS_CLASS_NAME); }
#endif // MAX2022

  int             GetShadowMethod()             noexcept override { return LIGHTSHADOW_NONE; }


  // From GenLight
  GenLight*       NewLight(int type)                    override { return new HydraLight(false); }
  RefResult       EvalLightState(TimeValue time, Interval& valid, LightState* ls) override;
  ObjLightDesc*   CreateLightDesc(INode* inode, BOOL forceShadowBuf = FALSE)      override; // only for render (scanline, etc.)

  BOOL      IsSpot()                                                    override;
  BOOL      IsDir()                                            noexcept override { return false; }
  void      SetUseLight(int onOff)                                      override;
  BOOL      GetUseLight(void)                                           override;
  void      SetSpotShape(int s)                                noexcept override {}
  int       GetSpotShape(void)                                 noexcept override { return CIRCLE_LIGHT; }
  void      SetHotspot (TimeValue time, float f)                        override;
  void      SetFallsize(TimeValue time, float f)                        override;
  void      SetAtten   (TimeValue time, int which, float f)             override;
  void      SetTDist   (TimeValue time, float f)               noexcept override { m_targDist = f; }
  float     GetHotspot (TimeValue t, Interval & valid = Interval(0, 0)) override;
  float     GetFallsize(TimeValue t, Interval & valid = Interval(0, 0)) override;
  float     GetAtten   (TimeValue t, int which, Interval & valid = Interval(0, 0)) override { return 1.0F; }
  float     GetTDist   (TimeValue t, Interval & valid = Interval(0, 0)) override { return m_targDist; }

#ifdef MAX2022
  void      SetRGBColor(TimeValue t, const Point3& rgb)                         override;
#else
  void      SetRGBColor(TimeValue t, Point3 & rgb)                              override;
#endif // MAX2022

  Point3    GetRGBColor        (TimeValue t, Interval & valid = Interval(0, 0)) override;
  Point3    GetHSVColor        (TimeValue t, Interval & valid = Interval(0, 0)) override { return 0; }
  float     GetIntensity       (TimeValue t, Interval & valid = Interval(0, 0)) override;
  float     GetContrast        (TimeValue t, Interval & valid = Interval(0, 0)) override { return 0.0F; }
  float     GetAspect          (TimeValue t, Interval & valid = Interval(0, 0)) override { return GetWidth(t) / GetLength(t); }
  void      SetHSVColor        (TimeValue t, Point3 & hsv)             noexcept override {}
  void      SetIntensity       (TimeValue time, float f)                        override;
  void      SetContrast        (TimeValue time, float f)               noexcept override {}
  void      SetAspect          (TimeValue t, float f)                  noexcept override {}
  void      SetConeDisplay     (int s, int notify = TRUE)              noexcept override { m_showCone = true; }
  BOOL      GetConeDisplay     (void)                                  noexcept override { return m_showCone; }
  BOOL      GetUseAtten        (void)                                  noexcept override { return false; }
  BOOL      GetAttenDisplay    (void)                                  noexcept override { return false; }
  BOOL      GetUseAttenNear    (void)                                  noexcept override { return false; }
  BOOL      GetAttenNearDisplay(void)                                  noexcept override { return false; }
  void      SetUseAtten        (int s)                                 noexcept override {}
  void      SetAttenDisplay    (int s)                                 noexcept override {}
  void      SetUseAttenNear    (int s)                                 noexcept override {}
  void      SetAttenNearDisplay(int s)                                 noexcept override {}
  void      Enable             (int enab)                              noexcept override { m_enable = enab; }

  float     GetMapBias   (TimeValue t, Interval & valid = Interval(0, 0)) override { return 0.01F; }
  float     GetMapRange  (TimeValue t, Interval & valid = Interval(0, 0)) override { return 10.0F; }
  int       GetMapSize   (TimeValue t, Interval & valid = Interval(0, 0)) override { return 512; }
  float     GetRayBias   (TimeValue t, Interval & valid = Interval(0, 0)) override { return 0.01F; }
  int       GetUseGlobal ()                                      noexcept override { return 1; }
  int       GetShadow    ()                                      noexcept override { return 1; } // enable shadow
  int       GetShadowType()                                      noexcept override { return 1; } // 0 - Shadow Map, 1 - Raytraced
  int       GetAbsMapBias()                                      noexcept override { return 1; }
  void      SetMapBias   (TimeValue t, float f)                  noexcept override {}
  void      SetMapRange  (TimeValue t, float f)                  noexcept override {}
  void      SetMapSize   (TimeValue t, int f)                    noexcept override {}
  void      SetRayBias   (TimeValue t, float f)                  noexcept override {}
  void      SetUseGlobal (int a)                                 noexcept override {}
  void      SetShadow    (int a)                                 noexcept override {}
  void      SetShadowType(int a)                                          override;
  void      SetAbsMapBias(int a)                                 noexcept override {}

  int       GetOvershoot()                                       noexcept override { return 0; }
  void      SetOvershoot(int a)                                  noexcept override {}
  void      SetExclusionList(ExclList & list)                             override { m_excludeList = list; }
  BOOL      SetHotSpotControl(Control * c)                       noexcept override { return false; }
  BOOL      SetFalloffControl(Control * c)                       noexcept override { return false; }
  BOOL      SetColorControl(Control * c)                         noexcept override { return false; }
  Control*  GetHotSpotControl()                                  noexcept override { return nullptr; }
  Control*  GetFalloffControl()                                  noexcept override { return nullptr; }
  Control*  GetColorControl()                                    noexcept override { return nullptr; }

  // From LightscapeLight 
  ObjectState Eval(TimeValue t)                                           override;
  ExclList& GetExclusionList()                                   noexcept override { return m_excludeList; }

  void      SetType(int type)                                             override;
  void      SetType(const MCHAR * name)                          noexcept override {}
  int       Type()                                                        override; // return 3ds max light type
  const MCHAR* TypeName()                                        noexcept override { return L"AREA_TYPE "; }

  void      SetDistribution(DistTypes dist)                      noexcept override { m_distrib = dist; }
  DistTypes GetDistribution()                              const noexcept override { return m_distrib; }

  void      SetIntensityAt(float f)                              noexcept override {}
  float     GetIntensityAt()                                     noexcept override { return 9999.0F; }
  void      SetIntensityType(IntensityType t)                    noexcept override {}
  IntensityType GetIntensityType()                               noexcept override { return CANDELAS; }

  void     SetFlux     (TimeValue t, float flux)                 noexcept override {}
  void     SetRGBFilter(TimeValue t, Point3& rgb)                noexcept override {}
  void     SetHSVFilter(TimeValue t, Point3& hsv)                noexcept override {}
  float    GetFlux     (TimeValue t, Interval& valid = Interval(0, 0)) const override;
  Point3   GetRGBFilter(TimeValue t, Interval& valid = Interval(0, 0)) override { return { 1.0F, 1.0F, 1.0F }; }
  Point3   GetHSVFilter(TimeValue t, Interval& valid = Interval(0, 0)) override { return { 0.0F, 0.0F, 1.0F }; }

  class AreaLightCustAttrib;


  // Plug-in shadow generator
  ShadowType*  ActiveShadowType()                                override;
  ShadowType*  GetShadowGenerator()                              override { return ActiveShadowType(); }
  const MCHAR* GetShadowGeneratorName()                 noexcept override { return L"shadow"; }
  void     SetShadowGenerator(ShadowType * s)           noexcept override {}
  void     SetShadowGenerator(const MCHAR * name)       noexcept override {}
  void     SetUseShadowColorMap(TimeValue t, int onOff) noexcept override {}
  int      GetUseShadowColorMap(TimeValue t)            noexcept override { return 0; }

  void     SetInclude(BOOL onOff)                       noexcept override {}
  void     UpdateTargDistance(TimeValue t, INode * inode)        override;

  BOOL     SetKelvinControl(Control * kelvin)           noexcept override { return false; }
  BOOL     SetFilterControl(Control * filter)           noexcept override { return false; }
  Control* GetKelvinControl()                           noexcept override { return nullptr; }
  Control* GetFilterControl()                           noexcept override { return nullptr; }
  float    GetKelvin(TimeValue t, Interval & v = Interval(0, 0)) override;
  void     SetKelvin(TimeValue t, float kelvin)                  override;
  BOOL     GetUseKelvin() override;
  void     SetUseKelvin(BOOL useKelvin)                          override;

  MaxSDK::AssetManagement::AssetUser GetWebFile()          const override;
  const MCHAR*  GetFullWebFileName()                       const override;
  void     SetWebFile(const MaxSDK::AssetManagement::AssetUser& file) noexcept override {}
  float    GetWebRotateX()                                 const override;
  void     SetWebRotateX(float degrees)                          override;
  float    GetWebRotateY()                                 const override;
  void     SetWebRotateY(float degrees)                          override;
  float    GetWebRotateZ()                                 const override;
  void     SetWebRotateZ(float degrees)                          override;

  float    GetDimmerValue       (TimeValue t, Interval & valid = Interval(0, 0)) const override { return 1.0F; }
  float    GetResultingIntensity(TimeValue t, Interval & valid = Interval(0, 0)) const override { return m_intensImage; }
  float    GetResultingFlux     (TimeValue t, Interval & valid = Interval(0, 0)) const override { return GetFlux(t); }
  BOOL     GetUseMultiplier     ()                                      const noexcept override { return false; }
  BOOL     IsColorShiftEnabled  ()                                      const noexcept override { return false; }
  void     SetDimmerValue       (TimeValue t, float dimmer)                   noexcept override {}
  void     SetUseMultiplier     (BOOL on)                                     noexcept override {}
  void     EnableColorShift     (BOOL on)                                     noexcept override {}

  Point3   GetCenter      ()                                                     const override { return { 0.1F, 0.1F, 0.1F }; } // local center in light coord
  float    GetRadius      (TimeValue t, Interval & valid = Interval(0, 0))       const override;
  float    GetLength      (TimeValue t, Interval & valid = Interval(0, 0))       const override;
  float    GetWidth       (TimeValue t, Interval & valid = Interval(0, 0))       const override;
  void     SetRadius      (TimeValue t, float radius)                                  override;
  void     SetLength      (TimeValue t, float length)                                  override;
  void     SetWidth       (TimeValue t, float width)                                   override;
  void     SetShape       (int count, const Point3* pointsIn)                 noexcept override {}
  int      GetShape       (Point3 * pointsOut, int bufSize)                      const override;
  float    GetOriginalFlux()                                                     const override { return GetFlux(0); }
  void     SetOriginalFlux(float flux)                                        noexcept override {}
  
  // intens in candelas (cd) units
  float    GetOriginalIntensity()                   const noexcept override { return m_intensCd; }
  void     SetOriginalIntensity(float candelas)           noexcept override { m_intensCd = candelas; }
  
  PresetLightColor GetColorPreset()                 const noexcept override { return FLUORESCENT_WHITE; }
  void     SetColorPreset(PresetLightColor presetID)      noexcept override {}

  // From  IObjectDisplay2 
  unsigned long GetObjectDisplayRequirement() const override;
  bool     PrepareDisplay    (const MaxSDK::Graphics::UpdateDisplayContext& prepareDisplayContext) override;
  bool     UpdatePerNodeItems(const MaxSDK::Graphics::UpdateDisplayContext& updateDisplayContext,
                                    MaxSDK::Graphics::UpdateNodeContext&    nodeContext,
                                    MaxSDK::Graphics::IRenderItemContainer& targetRenderItemContainer) override;

  // From IHydraLight
  HLType HlType() override;


  // My methods
  void     Update(TimeValue t, Interval& valid);  
  void     GetMat(TimeValue t, INode* inode, ViewExp &vpt, Matrix3& mat);
           
  [[nodiscard]] bool GetEnableInViewport()   const noexcept { return m_enable; }
  bool     isVisibleLight();
  bool     isTargeted();
  bool     isWeb();

  // intensity in image units
  [[nodiscard]] float GetIntensImage()               const noexcept { return m_intensImage; }
  void     SetIntensImage(const float a_intensImg)         noexcept { m_intensImage = a_intensImg; }

  // intensity units
  [[nodiscard]] HLIntensUnit GetIntensUnits()        const noexcept { return m_intensUnit; }
  void     SetIntensUnits(const HLIntensUnit a_intensUnit) noexcept { m_intensUnit = a_intensUnit; }

  void     SetIntensCd(const float a_intensCd)             noexcept { m_intensCd = a_intensCd; }

  [[nodiscard]] float GetIntensLm()                  const noexcept { return m_intensLm; } 
  void     SetIntensLm(const float a_intensLm)             noexcept { m_intensLm = a_intensLm; }
           
  void     SetVisibleLight(TimeValue t, bool on);
  void     SetTargeted    (TimeValue t, bool on);
  void     SetSpotLight   (TimeValue t, bool on);
  void     SetTarget      (const int tp);
           
  void     BuildMeshes            (TimeValue t, Interval& valid);
  void     BuildPointMesh         (TimeValue t, Interval& valid);
  void     BuildRectMesh          (TimeValue t, Interval& valid);
  void     BuildDiskMesh          (TimeValue t, Interval& valid);
  void     BuildSimpleSphereMesh  (TimeValue t, Interval& valid);
  void     BuildSphereMesh        (TimeValue t, Interval& valid);
  void     BuildCylinderMesh      (TimeValue t, Interval& valid);
  void     BuildSmallSpotlightMesh(TimeValue t, Interval& valid);
  void     GetConePoints          (TimeValue t, float angle, float dist, Point3 *q);

  // interval
  [[nodiscard]] Interval GetInterval()    noexcept { return m_ivalid; }
  void     SetInterval(const Interval& i) noexcept { m_ivalid = i; }
  
  void     InvalidateUI();
  void     Reset();
  
  bool         m_changeIntensUnit;
  HLType       m_hlType         = HL_TYPE_POINT;

private:
  HydraLight*  m_currHydraLight = nullptr;
  Interval     m_ivalid;
  ExclList     m_excludeList;
  bool         m_enable;           // for draw in Display.
  HLIntensUnit m_intensUnit;
  BOOL         m_simpleShape, m_showCone;    
  float        m_intensCd, m_intensImage, m_intensLm, m_targDist;
  DistTypes    m_distrib;
               
  Mesh         m_buildMesh;
  Mesh         m_buildSmallSpotMesh;
  Mesh*        m_mesh          = nullptr;
  Mesh*        m_meshSmallSpot = nullptr;
               
  int 	       m_extDispFlags;
};



class HydraLightClassDesc: public ClassDesc2
{
public:
  int           IsPublic()          noexcept override { return FALSE; }
  void*         Create(BOOL loading = FALSE) override { return new HydraLight(loading); }
  const TCHAR *	ClassName()                  override { return GetString(IDS_CLASS_NAME); }
  SClass_ID     SuperClassID()      noexcept override { return LIGHT_CLASS_ID; }
  Class_ID      ClassID()                    override { return HydraLight_CLASS_ID; }
  const TCHAR*  Category()                   override { return GetString(IDS_CATEGORY); }
  const TCHAR*  InternalName()      noexcept override { return _T("HydraLight"); }	// returns fixed parsable name (scripter-visible name)
  HINSTANCE     HInstance()         noexcept override { return hInstance; }					// returns owning module handle

#ifdef MAX2022
  const MCHAR* NonLocalizedClassName()       override { return GetString(IDS_CLASS_NAME); }
#endif // MAX2022

};



class HydraLightDlgProc: public ParamMap2UserDlgProc
{
public:
  HydraLight* hL = nullptr;

  //ICustButton     *ExcludeList;
  //ICustButton     *IES_file;


  explicit HydraLightDlgProc(HydraLight *cb);

  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
  void SetControlForTypeLight(HWND hwnd, IParamMap2 *map, const HLType type);
  void SetControlFromDistribution(HWND hwnd, IParamMap2 *map, const HydraLight::DistTypes m_distrib);

  //void DropFileInMapSlot(int mapControlID, hydraChar* filename);

  void DeleteThis() noexcept override { delete this; }
};




//Class for interactive creation of the object using the mouse
class HydraLightCreateCallBack: public CreateMouseCallBack
{
  HydraLight *ob = nullptr;
  Point3 p0;                // First point in world coordinates
  Point3 p1;                // Second point in world coordinates

  // added for exersice 6 to keep track of previous point.
  IPoint2 m_mousecallback_pt2_prev;

public:
  int  proc(ViewExp *vpt, int msg, int point, int flags, IPoint2 m, Matrix3& mat) override;
  void SetObj(HydraLight *obj) noexcept { ob = obj; }
};




class HydraLightPBAccessor: public PBAccessor
{
public:
  void Set(PB2Value& v, ReferenceMaker* owner, ParamID id, int tabIndex, TimeValue t) override;
};



template <void (HydraLight::*set)(int), bool notify>
class SimpleLightUndo: public RestoreObj
{
public:
  void Restore(int undo) override;
  void Redo()            override;
  int  Size()            override;
  TSTR Description()     override;

  static void Hold(HydraLight* light, int newVal, int oldVal)
  {
    if (theHold.Holding())
      theHold.Put(new SimpleLightUndo<set, notify>(light, newVal, oldVal));
  }

private:
  SimpleLightUndo(HydraLight* light, int newVal, int oldVal)
    : _light(light), _redo(newVal), _undo(oldVal){}

  HydraLight*	_light = nullptr;
  int					_redo;
  int					_undo;
};



class LightPostLoad: public PostLoadCallback
{
public:
  HydraLight* sp = nullptr;
  Interval valid;
  explicit LightPostLoad(HydraLight* l): sp(l){}

  void proc(ILoad *iload) noexcept override
  {
    //gl->FixOldVersions(iload);
    waitPostLoad--;
    delete this;
  }
};



class LightConeItem: public MaxSDK::Graphics::Utilities::SplineRenderItem
{
  HydraLight* mpLight = nullptr;

public:
  explicit LightConeItem(HydraLight* lt): mpLight(lt){}

  ~LightConeItem() override { mpLight = nullptr; }

  void Realize(MaxSDK::Graphics::DrawContext& drawContext) override;

  void BuildCone(TimeValue t, float dist);
  void BuildConeAndLine(TimeValue t, INode* inode);
  //void BuildX(TimeValue t, float asp, int npts, float dist, int indx, Color& color); 
};


class LightTargetLineItem: public MaxSDK::Graphics::Utilities::SplineRenderItem
{
  HydraLight* mpLight = nullptr;
  float m_lastDist    = -1.0F;
  Color m_lastColor   = { 0.0F, 0.0F, 0.0F };

public:
  explicit LightTargetLineItem(HydraLight* lt) : mpLight(lt){}

  ~LightTargetLineItem() override { mpLight = nullptr; }

  void Realize(MaxSDK::Graphics::DrawContext& drawContext) override;
  void OnHit(MaxSDK::Graphics::HitTestContext& /*hittestContext*/, MaxSDK::Graphics::DrawContext& drawContext) override;
};


// target spot light creation stuff...
class TSpotCreationManager: public MouseCallBack, public ReferenceMaker
{
private:
  CreateMouseCallBack* createCB              = nullptr;
  INode*               lgtNode               = nullptr;
  INode*               targNode              = nullptr;
  HydraLight*          lgtObject             = nullptr;
  TargetObject*        targObject            = nullptr;
  IObjCreate*          createInterface       = nullptr;
  ClassDesc*           cDesc                 = nullptr;

  Matrix3              mat;  // the nodes TM relative to the CP
  IPoint2              pt0;
  bool                 attachedToNode        = false;
  bool                 ignoreSelectionChange = false;
  int                  lastPutCount          = 0;

  void CreateNewObject();

#ifdef MAX2022
  void GetClassName(MSTR& s, bool localized = true) const override { s = _M("TSpotCreationManager"); }  
#else  
  void GetClassName(MSTR& s) override { s = _M("TSpotCreationManager"); }
#endif // MAX2022

  int             NumRefs()                                  noexcept override { return 1; }
  RefTargetHandle GetReference(int i)                        noexcept override { return (RefTargetHandle)lgtNode; }
  void            SetReference(int i, RefTargetHandle rtarg) noexcept override { lgtNode = dynamic_cast<INode*>(rtarg); }

  // StdNotifyRefChanged calls this, which can change the partID to new value 
  // If it doesnt depend on the particular message& partID, it should return
  // REF_DONTCARE
  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, 
                             PartID& partID, RefMessage message, BOOL propagate) override;

public:
  void Begin(IObjCreate* ioc, ClassDesc* desc);
  void End();

  int proc(HWND hwnd, int msg, int point, int flag, IPoint2 m)         override;
  BOOL SupportAutoGrid()                                      noexcept override { return TRUE; }
};


#define CID_TSPOTCREATE	(CID_USER + 3)

class TSpotCreateMode: public CommandMode
{
  TSpotCreationManager proc;

public:
  void Begin(IObjCreate *ioc, ClassDesc *desc) { proc.Begin(ioc, desc); }
  void End() { proc.End(); }

  int Class()                              noexcept override { return CREATE_COMMAND; }
  int ID()                                 noexcept override { return CID_TSPOTCREATE; }
  MouseCallBack* MouseProc(int *numPoints) noexcept override { *numPoints = 1000000; return &proc; }
  ChangeForegroundCallback* ChangeFGProc() noexcept override { return CHANGE_FG_SELECTED; }
  BOOL ChangeFG(CommandMode* oldMode)               override { return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }
  void EnterMode()                         noexcept override {}
  void ExitMode()                          noexcept override {}
  BOOL IsSticky()                             const noexcept { return FALSE; }
};

class HydraLight::AreaLightCustAttrib: public LightscapeLight::AreaLightCustAttrib
{
  // Inherited via AreaLightCustAttrib
  bool AreaLightComputationEnabled (TimeValue t, Interval* valid = NULL) const noexcept override { return true; }
  BOOL IsLightShapeRenderingEnabled(TimeValue t, Interval* valid = NULL) const noexcept override { return true; }
  void LightShapeRenderingEnabled  (TimeValue t, BOOL on)                      noexcept override {}
  int  GetNumSamples               (TimeValue t, Interval* valid = NULL) const noexcept override { return 32; }
  void SetNumSamples               (TimeValue t, int numSamples)               noexcept override {}
};


static HydraLightClassDesc  hydraLightDesc;
static HydraLightPBAccessor hydraLightPBAccessor;
static TSpotCreateMode      theTSpotCreateMode;

