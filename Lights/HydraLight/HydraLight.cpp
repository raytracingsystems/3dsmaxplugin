#include "HydraLight.h"

//////////////////////////////////////////////////////////////////////////

ClassDesc2* GetHydraLightDesc() { return &hydraLightDesc; }



static ParamBlockDesc2 main_param_blk(PB_MAIN, _T("Parameters"), 0, GetHydraLightDesc(),
                                      P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, PB_MAIN_REF, PB_MAIN,
                                      //Dlalog rollout
                                      IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
                                      // params
                                      pb_on, _T("on"), TYPE_BOOL, 0, IDS_ON,
                                        p_default, TRUE,
                                        p_ui, TYPE_SINGLECHEKBOX, IDC_ON,
                                        p_end,
                                      pb_targeted, _T("targeted"), TYPE_BOOL, 0, IDS_TARGETED,
                                        p_default, FALSE,
                                        p_ui, TYPE_SINGLECHEKBOX, IDC_TARGETED_ON,
                                        p_end,
                                      pb_visInRender, _T("visInRender"), TYPE_BOOL, 0, IDS_VISIBLE_IN_RENDER,
                                        p_default, TRUE,
                                        p_ui, TYPE_SINGLECHEKBOX, IDC_VISIBLE_IN_RENDER,
                                        p_end,
                                      pb_type, _T("type"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_TYPE,
                                        p_default, HL_TYPE_RECTANGLE,
                                        p_ui, TYPE_INT_COMBOBOX, IDC_TYPE, 6, IDS_TYPE1, IDS_TYPE2, IDS_TYPE3, IDS_TYPE4, IDS_TYPE5, IDS_TYPE6, /*IDS_TYPE7,*/ // TYPE7 - Lazer, not yet implemented.
                                        p_tooltip, IDS_TYPE,
                                        p_accessor, &hydraLightPBAccessor,
                                        p_end,
                                      pb_excludeList, _T("excludeList"), TYPE_CHECKBUTTON, 0, IDS_EXCLUDE_LIST,
                                        p_ui, TYPE_CHECKBUTTON, IDC_EXCLUDE_LIST,
                                        p_tooltip, IDS_EXCLUDE_LIST,
                                        p_end,
                                      pb_intensity, _T("intensity"), TYPE_FLOAT, P_ANIMATABLE, IDS_INTENSITY,
                                        p_default, 1.0F,
                                        p_range, 0.0F, 999999999.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_INTENSITY_EDIT, IDC_INTENSITY_SPIN, 0.1f,
                                        p_accessor, &hydraLightPBAccessor,
                                        p_end,
                                      pb_intensityUnits, _T("intensityUnits"), TYPE_INT, P_RESET_DEFAULT + P_ANIMATABLE, IDS_INTENSITY_UNITS,
                                        p_default, INTENS_UNIT_IMAGE,
                                        p_ui, TYPE_INT_COMBOBOX, IDC_INTENSITY_UNITS, 3, IDS_INTENSITY_UNIT1, IDS_INTENSITY_UNIT2, IDS_INTENSITY_UNIT3,
                                        p_tooltip, IDS_INTENSITY_UNITS,
                                        p_end,
                                      pb_color, _T("color"), TYPE_RGBA, P_ANIMATABLE, IDS_COLOR,
                                        p_default, Color(1.0F, 1.0F, 1.0F),
                                        p_ui, TYPE_COLORSWATCH, IDC_COLOR,
                                        p_accessor, &hydraLightPBAccessor,
                                        p_end,
                                      pb_kelvin, _T("kelvin"), TYPE_BOOL, 0, IDS_KELVIN_ON,
                                        p_default, FALSE,
                                        p_ui, TYPE_SINGLECHEKBOX, IDC_KELVIN_ON,
                                        p_accessor, &hydraLightPBAccessor,
                                        p_end,
                                      pb_kelvinTemp, _T("kelvinTemp"), TYPE_INT, P_ANIMATABLE, IDS_KELVIN,
                                        p_default, 6500,
                                        p_range, 1000, 40000,
                                        p_ui, TYPE_SPINNER, EDITTYPE_INT, IDC_KELVIN_EDIT, IDC_KELVIN_SPIN, 100.0F,
                                        p_accessor, &hydraLightPBAccessor,
                                        p_end,
                                      pb_length, _T("length"), TYPE_FLOAT, P_ANIMATABLE, IDS_LENGTH,
                                        p_default, 100.0F,
                                        p_range, 0.0001F, 999999999.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_LENGTH_EDIT, IDC_LENGTH_SPIN, 0.1F,
                                        p_end,
                                      pb_width, _T("width"), TYPE_FLOAT, P_ANIMATABLE, IDS_WIDTH,
                                        p_default, 100.0F,
                                        p_range, 0.0001F, 999999999.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_WIDTH_EDIT, IDC_WIDTH_SPIN, 0.1F,
                                        p_end,
                                      pb_radius, _T("radius"), TYPE_FLOAT, P_ANIMATABLE, IDS_RADIUS,
                                        p_default, 5.0F,
                                        p_range, 0.0001F, 999999999.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_RADIUS_EDIT, IDC_RADIUS_SPIN, 0.1F,
                                        p_end,
                                      pb_WEB_on, _T("WEB_on"), TYPE_BOOL, 0, IDS_WEB_ON,
                                        p_default, FALSE,
                                        p_ui, TYPE_SINGLECHEKBOX, IDC_WEB_ON,
                                        p_end,
                                      pb_WEB_file, _T("WEB_file"), TYPE_FILENAME, 0, IDS_WEB_FILE,
                                        p_assetTypeID, MaxSDK::AssetManagement::AssetType::kPhotometricAsset,
                                        p_file_types, IDS_WEB_FILE_FILTER,
                                        p_ui, TYPE_FILEOPENBUTTON, IDC_WEB_FILE,
                                        p_end,
                                      pb_WEB_rotX, _T("WEB_rotX"), TYPE_FLOAT, P_ANIMATABLE, IDS_WEB_ROT_X,
                                        p_default, 0.0F,
                                        p_range, -180.0F, 180.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_WEB_ROT_X_EDIT, IDC_WEB_ROT_X_SPIN, 1.0F,
                                        p_end,
                                      pb_WEB_rotY, _T("WEB_rotY"), TYPE_FLOAT, P_ANIMATABLE, IDS_WEB_ROT_Y,
                                        p_default, 0.0F,
                                        p_range, -180.0F, 180.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_WEB_ROT_Y_EDIT, IDC_WEB_ROT_Y_SPIN, 1.0F,
                                        p_end,
                                      pb_WEB_rotZ, _T("WEB_rotZ"), TYPE_FLOAT, P_ANIMATABLE, IDS_WEB_ROT_Z,
                                        p_default, 0.0F,
                                        p_range, -180.0F, 180.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_WEB_ROT_Z_EDIT, IDC_WEB_ROT_Z_SPIN, 1.0F,
                                        p_end,
                                      pb_spotlight, _T("spotlight"), TYPE_BOOL, 0, IDS_SPOTLIGHT_ON,
                                        p_default, FALSE,
                                        p_ui, TYPE_SINGLECHEKBOX, IDC_SPOTLIGHT_ON,
                                        p_end,
                                      pb_showCone, _T("showCone"), TYPE_BOOL, 0, IDS_SHOW_CONE_ON,
                                        p_default, FALSE,
                                        p_ui, TYPE_SINGLECHEKBOX, IDC_SHOWCONE_ON,
                                        p_end,
                                      pb_hotspot, _T("hotspot"), TYPE_FLOAT, P_ANIMATABLE, IDS_HOTSPOT,
                                        p_default, 40.0F,
                                        p_range, 0.0F, 179.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_HOTSPOT_EDIT, IDC_HOTSPOT_SPIN, 1.0F,
                                        p_accessor, &hydraLightPBAccessor,
                                        p_end,
                                      pb_fallsize, _T("falloff"), TYPE_FLOAT, P_ANIMATABLE, IDS_FALLOFF,
                                        p_default, 50.0F,
                                        p_range, 1.0F, 180.0F,
                                        p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_FALLOFF_EDIT, IDC_FALLOFF_SPIN, 1.0F,
                                        p_accessor, &hydraLightPBAccessor,
                                        p_end,
                                      p_end
);


static ParamBlockDesc2 viewport_param_blk(PB_VIEWPORT, _T("Viewport"), 0, GetHydraLightDesc(),
                                          P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, PB_VIEWPOR_REF, PB_VIEWPORT,
                                          //Dlalog rollout
                                          IDD_VIEWPORT, IDS_VIEWPORT, 0, 0, NULL,
                                          // params
                                          pb_simpleShape, _T("simpleShape"), TYPE_BOOL, 0, IDS_SIMPLE_SHAPE_ON,
                                            p_default, TRUE,
                                            p_ui, TYPE_SINGLECHEKBOX, IDC_SIMPLE_SHAPE_ON,
                                            p_end,
                                          p_end
);


//////////////////////////////////////////////////////////////////////////

IObjParam* HydraLight::ip       = nullptr;


//////////////////////////////////////////////////////////////////////////
// Param block version system for compatiple versions. Called in Load().
// The current version
constexpr int NPARAM_V0 = 22;

static ParamBlockDescID descMainV0[NPARAM_V0] ={
  { TYPE_BOOL,  NULL, TRUE,  pb_on             },
  { TYPE_BOOL,  NULL, FALSE, pb_targeted       },
  { TYPE_BOOL,  NULL, FALSE, pb_visInRender    },
  { TYPE_INT,   NULL, TRUE,  pb_type           },
  { TYPE_USER,  NULL, FALSE, pb_excludeList    },
  { TYPE_FLOAT, NULL, TRUE,  pb_intensity      },
  { TYPE_INT,   NULL, TRUE,  pb_intensityUnits },
  { TYPE_RGBA,  NULL, TRUE,  pb_color          },
  { TYPE_BOOL,  NULL, TRUE,  pb_kelvin         },
  { TYPE_FLOAT, NULL, TRUE,  pb_kelvinTemp     },
  { TYPE_FLOAT, NULL, TRUE,  pb_length         },
  { TYPE_FLOAT, NULL, TRUE,  pb_width          },
  { TYPE_FLOAT, NULL, TRUE,  pb_radius         },
  { TYPE_BOOL,  NULL, FALSE, pb_WEB_on         },
  { TYPE_USER,  NULL, FALSE, pb_WEB_file       },
  { TYPE_FLOAT, NULL, TRUE,  pb_WEB_rotX       },
  { TYPE_FLOAT, NULL, TRUE,  pb_WEB_rotY       },
  { TYPE_FLOAT, NULL, TRUE,  pb_WEB_rotZ       },
  { TYPE_BOOL,  NULL, TRUE,  pb_spotlight      },
  { TYPE_BOOL,  NULL, TRUE,  pb_showCone       },
  { TYPE_FLOAT, NULL, TRUE,  pb_hotspot        },
  { TYPE_FLOAT, NULL, TRUE,  pb_fallsize       },
};

static ParamBlockDescID descViewportV0[] ={
  { TYPE_FLOAT, NULL, FALSE, pb_simpleShape    },
};


static ParamVersionDesc versionsMainPB[VERSION_CURRENT+1] ={
  ParamVersionDesc(descMainV0, NPARAM_V0, 0)//,
  //ParamVersionDesc(descMainV1, NPARAM_V1, 1)
};

static ParamVersionDesc versionsViewportPB[VERSION_CURRENT+1] ={
  ParamVersionDesc(descViewportV0, 1, 0)//,
  //ParamVersionDesc(descViewportV1, ?, 1)
};


// For update version:
// 1) Add new parameters only to the end of the enum list.
// 2) Do not remove param from the list of enum, just add.
// 2) Add new or delete parameters in ParamBlockDesc2.
// 3) Create new ParamBlockDescID descVxx[] with new params.
// 4) Add new ParamVersionDesc in ParamVersionDesc versions[]
// 7) VERSION_CURRENT += 1

//////////////////////////////////////////////////////////////////////////




template <void (HydraLight::*set)(int), bool notify>
void SimpleLightUndo<set, notify>::Restore(int undo)
{
  (_light->*set)(_undo);
  if (notify)
    _light->NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}



template <void (HydraLight::*set)(int), bool notify>
void SimpleLightUndo<set, notify>::Redo()
{
  (_light->*set)(_redo);
  if (notify)
    _light->NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}



template <void (HydraLight::*set)(int), bool notify>
int SimpleLightUndo<set, notify>::Size()
{
  return sizeof(*this);
}



template <void (HydraLight::*set)(int), bool notify>
TSTR SimpleLightUndo<set, notify>::Description()
{
  return _T("SimpleLightUndo");
}



HydraLight::HydraLight(bool loading) :
  pblockMain(nullptr),
  pblockViewport(nullptr),
  m_currHydraLight(nullptr),
  m_excludeList{},
  m_mesh(nullptr),
  m_meshSmallSpot(nullptr),
  m_distrib(DIFFUSE_DIST),
  m_enable(true),
  m_showCone(false),
  m_targDist(100.0F),
  m_intensUnit(INTENS_UNIT_IMAGE),
  m_intensCd(0.0F),
  m_intensImage(0.0F),
  m_intensLm(0.0F),
  m_simpleShape(true),
  m_changeIntensUnit(false),
  m_extDispFlags(0)
{
  if (!loading)
    Reset();
}


HydraLight::~HydraLight()
{
  DeleteAllRefsFromMe();
  pblockMain = nullptr;
  pblockViewport = nullptr;

  //if (shadType && !shadType->SupportStdMapInterface()) {
  //  ReplaceReference(AS_SHADTYPE_REF, NULL);
  //  //		delete ASshadType;
  //  //		ASshadType = NULL;
  //}

  //UnRegisterNotification(&NotifyDist, (void *)this, NOTIFY_UNITS_CHANGE);

}



void HydraLight::BeginEditParams(IObjParam * ip, ULONG flags, Animatable * prev)
{
  this->ip = ip;
  LightObject::BeginEditParams(ip, flags, prev);
  hydraLightDesc.BeginEditParams(ip, this, flags, prev);

  main_param_blk.SetUserDlgProc(new HydraLightDlgProc(this));
}



void HydraLight::EndEditParams(IObjParam * ip, ULONG flags, Animatable * next)
{
  LightObject::EndEditParams(ip, flags, next);
  hydraLightDesc.EndEditParams(ip, this, flags, next);
}



Animatable * HydraLight::SubAnim(int i)
{
  switch (i)
  {
    case PB_MAIN:
      return (Animatable *)pblockMain;
      break;
    case PB_VIEWPORT:
      return (Animatable *)pblockViewport;
      break;
    default:
      return nullptr;
  }
}


#ifdef MAX2022


MSTR HydraLight::SubAnimName(int i, bool localized)
{
  switch (i)
  {
  case PB_MAIN:
    return GetString(IDS_PARAMS);
    break;
  case PB_VIEWPORT:
    return GetString(IDS_VIEWPORT);
    break;
  default:
    return _T("");
  }
}
#else
TSTR HydraLight::SubAnimName(int i)
{
  switch (i)
  {
    case PB_MAIN:
      return GetString(IDS_PARAMS);
      break;
    case PB_VIEWPORT:
      return GetString(IDS_VIEWPORT);
      break;
    default:
      return _T("");
  }
}
#endif // MAX2022



static HydraLightCreateCallBack hydraLightCreateCB;
CreateMouseCallBack * HydraLight::GetCreateMouseCallBack()
{
  hydraLightCreateCB.SetObj(this);
  return(&hydraLightCreateCB);
}



void HydraLight::SetShadowType(int a)
{
  ShadowType *s = a ? NewDefaultRayShadowType() : NewDefaultShadowMapType();
  GetCOREInterface()->SetGlobalShadowGenerator(s);
  //globShadowType = a;
}

ObjectState HydraLight::Eval(TimeValue t)
{
  Update(t, m_ivalid);
  return ObjectState(this);
}



RefResult HydraLight::EvalLightState(TimeValue t, Interval & valid, LightState * ls)
{
  if (ls == nullptr)
    return REF_FAIL;

  if (GetUseLight())
    ls->color        = GetRGBColor(t, valid);
  else
    ls->color        = Point3(0, 0, 0);

  ls->on             = GetUseLight();
  ls->intens         = GetResultingIntensity(t, valid);
  ls->hotsize        = GetHotspot(t, valid);
  ls->fallsize       = GetFallsize(t, valid);

  ls->shape          = CIRCLE_LIGHT;
  ls->aspect         = 1.0F;
  ls->overshoot      = false;
  ls->shadow         = false;
  ls->ambientOnly    = false;
  ls->affectDiffuse  = true;
  ls->affectSpecular = true;


  if (IsSpot())
    ls->type    = SPOT_LGT;
  else
    ls->type    = OMNI_LGT;

  Type();

  if (m_hlType == HL_TYPE_RECTANGLE || m_hlType == HL_TYPE_DISK || m_hlType == HL_TYPE_PORTAL)
  {
    ls->type = SPOT_LGT;
    ls->hotsize = 178;
    ls->fallsize = 179;
  }


  ls->useNearAtten   = 0;
  ls->nearAttenStart = 0.9F;   // GetAtten(t, ATTEN1_START, valid);
  ls->nearAttenEnd   = 1.0F;   // GetAtten(t, ATTEN1_END, valid);

  ls->useAtten       = DECAY_NONE;   //GetUseAtten();  
  ls->attenStart     = 1.1F;   // GetAtten(t, ATTEN_START, valid);
  ls->attenEnd       = 10000000.0F; // GetAtten(t, ATTEN_END, valid);

  ls->extra |= DECAY_INVSQ;

  if (ls->extra == 0x0100) // Not enter!
  {
    // Special code for the interactive viewport in order to handle
    // DecayType.  If extra is set to 0x0100, we assume that we are called
    // from interactive viewport code.
    //
    // Set bits on the extra field to pass on the type of attenuation in
    // use.
    //
    // Note that attenStart contains the DecayRadius if useAtten is not
    // DECAY_NONE, i.e. either DECAY_INV or DECAY_INVSQ.  The GFX code will
    // use this to handle these two types of attenuation.
    //
    // Please note that attenEnd always contains the attenEnd value of the Far
    // Attenuation.
    //

    ls->extra |= DECAY_INVSQ;
    //ls->extra |= GW_ATTEN_END;
  }

  // from Light class (used in the interactive renderer, but not work...) 
  this->type          = ls->type;
  this->color         = ls->color;
  this->attenType     = GW_ATTEN_NONE;
  this->attenStart    = 1.0F;
  this->attenEnd      = 100000.0F;
  this->intensity     = ls->intens;
  this->hotSpotAngle  = ls->hotsize;
  this->fallOffAngle  = ls->fallsize;
  this->shape         = GW_SHAPE_CIRCULAR;
  this->aspect        = 1.0F;
  this->overshoot     = 1;

  return REF_SUCCEED;
}


ObjLightDesc* HydraLight::CreateLightDesc(INode* inode, BOOL forceShadowBuf)
{
  return nullptr;
}



RefResult HydraLight::NotifyRefChanged(const Interval & changeInt, RefTargetHandle hTarget, PartID & partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
    case REFMSG_CHANGE:
      m_ivalid.SetEmpty();


      if (hTarget == pblockMain)
      {
        ParamID changing_param = pblockMain->LastNotifyParamID();
        main_param_blk.InvalidateUI(changing_param);
      }
      else if (hTarget == pblockViewport)
      {
        ParamID changing_param = pblockViewport->LastNotifyParamID();
        viewport_param_blk.InvalidateUI(changing_param);
      }

      break;
  }
  return REF_SUCCEED;
}

RefTargetHandle HydraLight::GetReference(int i)
{
  switch (i)
  {
    case PB_MAIN:
      return (RefTargetHandle)pblockMain;
      break;
    case PB_VIEWPORT:
      return (RefTargetHandle)pblockViewport;
      break;
    default:
      return nullptr;
      break;
  }
  return RefTargetHandle();
}

void HydraLight::SetReference(int i, RefTargetHandle rtarg)
{
  switch (i)
  {
    case PB_MAIN:
      pblockMain = (IParamBlock2*)rtarg;
      break;
    case PB_VIEWPORT:
      pblockViewport = (IParamBlock2*)rtarg;
      break;
    default:
      break;
  }
}

IParamBlock2 * HydraLight::GetParamBlock(int i)
{
  switch (i)
  {
    case PB_MAIN:
      return pblockMain;
      break;
    case PB_VIEWPORT:
      return pblockViewport;
      break;
    default:
      return nullptr;
      break;
  }
}

IParamBlock2 * HydraLight::GetParamBlockByID(BlockID id)
{
  if (pblockMain->ID() == id) return pblockMain;
  else if (pblockViewport->ID() == id) return pblockViewport;
  else                                return nullptr;
}

IOResult HydraLight::Load(ILoad * iload)
{
  iload->RegisterPostLoadCallback(new ParamBlock2PLCB(versionsMainPB,     VERSION_CURRENT, &main_param_blk,     this, PB_MAIN_REF));
  iload->RegisterPostLoadCallback(new ParamBlock2PLCB(versionsViewportPB, VERSION_CURRENT, &viewport_param_blk, this, PB_VIEWPOR_REF));

  return IO_OK;
}



int HydraLight::HitTest(TimeValue t, INode * a_inode, int a_type, int a_crossing, int a_flags, IPoint2 * a_p, ViewExp * a_vpt)
{
  if (!a_vpt || !a_vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return FALSE;
  }

  HitRegion hitRegion;
  DWORD     savedLimits;
  int       res = 0;
  Matrix3   m;
  m.IdentityMatrix();

  if (!GetEnableInViewport())
    return 0;
  
  GraphicsWindow *gw             = a_vpt->getGW();
  Material *mtl                  = gw->getMaterial();

  MakeHitRegion(hitRegion, a_type, a_crossing, 4, a_p);
  gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~(GW_ILLUM | GW_BACKCULL));
  GetMat(t, a_inode, *a_vpt, m);
  gw->setTransform(m);
  // if we get a hit on the mesh, we're done
  res                            = m_mesh->select(gw, mtl, &hitRegion, a_flags & HIT_ABORTONHIT);
  
  // if not, check the target line, and set the pair flag if it's hit
  if (!res)
  {
    // this special case only works with point selection of targeted lights
    if ((a_type != HITTYPE_POINT) || !a_inode->GetTarget())
      return 0;
    // don't let line be active if only looking at selected stuff and target isn't selected
    if ((a_flags & HIT_SELONLY) && !a_inode->GetTarget()->Selected())
      return 0;

    gw->clearHitCode();
    a_inode->SetTargetNodePair(1);
  }
  gw->setRndLimits(savedLimits);
  return res;
}




void HydraLight::GetWorldBoundBox(TimeValue t, INode * inode, ViewExp * vpt, Box3 & box)
{
  if (!vpt || !vpt->IsAlive())
  {
    box.Init();
    return;
  }

  int nv;
  Matrix3 tm;
  GetMat(t, inode, *vpt, tm);
  Point3 loc = tm.GetTrans();
  nv = m_mesh->getNumVerts();
  box.Init();

  box.IncludePoints(m_mesh->verts, nv, &tm);
}



void HydraLight::GetLocalBoundBox(TimeValue t, INode * inode, ViewExp * vpt, Box3 & box)
{
  if (!vpt || !vpt->IsAlive())
  {
    box.Init();
    return;
  }

  // This light source is real size, so everything scaleFactor is not needed.
  //if (Type() == TYPE_POINT)
  //{
  //  Point3 loc = inode->GetObjectTM(t).GetTrans();
  //  float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(loc) / 360.0F;
  //  box = mesh->getBoundingBox();                //same as GetDeformBBox
  //  box.Scale(scaleFactor);
  //}
  //else
  box = m_mesh->getBoundingBox();


  //JH 6/13/03 careful, the mesh extents in z are (0, ~-20)
  //thus the scale alone is wrong
  //move the top of the box back to the object space origin
  //#1055329,Boundbox of Omni_light error.
  //if (Type() != TYPE_POINT)
  //{
  box.Translate(Point3(0.0F, 0.0F, -box.pmax.z));
  //}
}



Interval HydraLight::ObjectValidity(TimeValue t)
{
  Interval valid;
  valid.SetInfinite();
  return valid;
}



void HydraLight::GetDeformBBox(TimeValue t, Box3 & box, Matrix3 * tm, BOOL useSel)
{
  box = m_mesh->getBoundingBox(tm);
}



MaxSDK::Graphics::Utilities::MeshEdgeKey LightMeshKey;
MaxSDK::Graphics::Utilities::SplineItemKey LightSplineKey;

unsigned long HydraLight::GetObjectDisplayRequirement() const
{
  // we do not want legacy display code called
  // we also do not have any per-view items
  return 0;
}

bool HydraLight::PrepareDisplay(const MaxSDK::Graphics::UpdateDisplayContext & prepareDisplayContext)
{
  LightMeshKey.SetFixedSize(false);
  return true;
}


bool HydraLight::UpdatePerNodeItems(const MaxSDK::Graphics::UpdateDisplayContext & updateDisplayContext, MaxSDK::Graphics::UpdateNodeContext & nodeContext, MaxSDK::Graphics::IRenderItemContainer & targetRenderItemContainer)
{
  INode* pNode = nodeContext.GetRenderNode().GetMaxNode();

  MaxSDK::Graphics::Utilities::MeshEdgeRenderItem* pMeshItem          = new MaxSDK::Graphics::Utilities::MeshEdgeRenderItem(m_mesh, false, false);
  MaxSDK::Graphics::Utilities::MeshEdgeRenderItem* pMeshSmallSpotItem = new MaxSDK::Graphics::Utilities::MeshEdgeRenderItem(m_meshSmallSpot, false, false);
  MaxSDK::Graphics::Utilities::SplineRenderItem*   pLineItem          = new LightConeItem(this);


  if (pNode->Dependent())
  {
    pMeshItem->SetColor(Color(ColorMan()->GetColor(kViewportShowDependencies)));
    pMeshSmallSpotItem->SetColor(Color(ColorMan()->GetColor(kViewportShowDependencies)));
  }
  else if (pNode->Selected())
  {
    pMeshItem->SetColor(Color(GetSelColor()));
    pMeshSmallSpotItem->SetColor(Color(GetSelColor()));
  }
  else if (pNode->IsFrozen())
  {
    pMeshItem->SetColor(Color(GetFreezeColor()));
    pMeshSmallSpotItem->SetColor(Color(GetFreezeColor()));
  }
  else
  {
    if (GetUseLight())
    {
      Color currColor = GetIViewportShadingMgr()->GetLightIconColor(*pNode);
      pMeshItem->SetColor(currColor);
      pMeshSmallSpotItem->SetColor(currColor / 2.0F);
    }
    else
    {
      pMeshItem->SetColor(Color(0, 0, 0));
      pMeshSmallSpotItem->SetColor(Color(0, 0, 0));
    }
  }

  // Main mesh
  MaxSDK::Graphics::CustomRenderItemHandle meshHandle;
  meshHandle.Initialize();
  meshHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
  meshHandle.SetCustomImplementation(pMeshItem);
  MaxSDK::Graphics::ConsolidationData data;
  data.Strategy = &MaxSDK::Graphics::Utilities::MeshEdgeConsolidationStrategy::GetInstance();
  data.Key = &LightMeshKey;
  meshHandle.SetConsolidationData(data);
  targetRenderItemContainer.AddRenderItem(meshHandle);


  // Small spot mesh
  Type();
  if (IsSpot() && (m_hlType == HL_TYPE_POINT || m_hlType == HL_TYPE_RECTANGLE || m_hlType == HL_TYPE_DISK))
  {
    MaxSDK::Graphics::CustomRenderItemHandle meshSmallSpotHandle;
    meshSmallSpotHandle.Initialize();
    meshSmallSpotHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
    meshSmallSpotHandle.SetCustomImplementation(pMeshSmallSpotItem);
    MaxSDK::Graphics::ConsolidationData dataSmallSpot;
    dataSmallSpot.Strategy = &MaxSDK::Graphics::Utilities::MeshEdgeConsolidationStrategy::GetInstance();
    dataSmallSpot.Key = &LightMeshKey;
    meshSmallSpotHandle.SetConsolidationData(dataSmallSpot);
    targetRenderItemContainer.AddRenderItem(meshSmallSpotHandle);
  }

  // Cone spotlight
  if (m_hlType == HL_TYPE_POINT || m_hlType == HL_TYPE_RECTANGLE || m_hlType == HL_TYPE_DISK)
  {
    MaxSDK::Graphics::CustomRenderItemHandle coneHandle;
    coneHandle.Initialize();
    coneHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
    coneHandle.SetCustomImplementation(pLineItem);
    data.Strategy = &MaxSDK::Graphics::Utilities::SplineConsolidationStrategy::GetInstance();
    data.Key = &LightSplineKey;
    coneHandle.SetConsolidationData(data);
    targetRenderItemContainer.AddRenderItem(coneHandle);
  }

  // Target line
  pLineItem = new LightTargetLineItem(this);
  MaxSDK::Graphics::CustomRenderItemHandle lineHandle;
  lineHandle.Initialize();
  lineHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
  lineHandle.SetCustomImplementation(pLineItem);
  data.Strategy = &MaxSDK::Graphics::Utilities::SplineConsolidationStrategy::GetInstance();
  data.Key = &LightSplineKey;
  lineHandle.SetConsolidationData(data);
  targetRenderItemContainer.AddRenderItem(lineHandle);

  return true;
}

HLType HydraLight::HlType()
{
  int i;
  pblockMain->GetValue(pb_type, TimeValue(0), i, FOREVER);
  return (HLType)i;
}




//ParamDlg * HydraLight::CreateParamDlg(HWND hwMtlEdit, IMtlParams * imp)
//{
//  IAutoMParamDlg* masterDlg = GetHydraLightDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
//
//  /*  hBitmap = reinterpret_cast<HBITMAP>(LoadImage(GetModuleHandle(NULL),MAKEINTRESOURCE(IDB_BITMAP1),IMAGE_BITMAP,0,0,0));
//
//    SendDlgItemMessage(hwMtlEdit,IDC_LOGO,STM_SETIMAGE,IMAGE_BITMAP,reinterpret_cast<LPARAM>(hBitmap));*/
//
//    // TODO: Set param block user dialog if necessary
//  main_param_blk.SetUserDlgProc(new HydraLightDlgProc(this));
//  return masterDlg;
//}

float toMeters(const float a_val)
{
#ifdef MAX2022
  const float masterScale = static_cast<float>(GetSystemUnitScale(UNITS_METERS));
#else
  const float masterScale = static_cast<float>(GetMasterScale(UNITS_METERS));
#endif // MAX2022

  return masterScale * a_val;
}

inline Point3 toMeters(Point3 p)
{
  p.x = toMeters(p.x);
  p.y = toMeters(p.y);
  p.z = toMeters(p.z);
  return p;
}

inline Matrix3 toMeters(Matrix3 m)
{
  Point3 translate = m.GetTrans();
  m.SetTrans(toMeters(translate));
  return m;
}

float scaleWithMatrix(float a_val, const Matrix3& a_externTransform)
{
  const Point3 tmpVec(0.0F, a_val, 0.0F);

  Matrix3 extTransform = a_externTransform;
  extTransform.SetRow(3, Point3(0, 0, 0));

  return (extTransform*tmpVec).Length();
}

float GetSteradian(const HLType a_currType) noexcept
{
  if (a_currType == HL_TYPE_POINT || a_currType == HL_TYPE_SPHERE || a_currType == HL_TYPE_CYLINDER) 
    return 4.0F * PI; // 360 deg.
  else
    return PI;        // 120 deg.
}



void ConvertIntensLmToCd(HydraLight* a_hL, const HLType a_currType, const float a_lm) noexcept
{
  if (!a_hL)
    return;
  
  const float candela = a_lm / GetSteradian(a_currType);

  a_hL->SetIntensCd(candela);
}


void ConvertIntensCdToLm(HydraLight* a_hL, const HLType a_currType, const float a_cd)
{
  if (!a_hL)
    return;

  const float sr = GetSteradian(a_currType);
  const float lm = a_cd * sr;

  a_hL->SetIntensLm(lm);
}

void ConvertIntensImageToCd(HydraLight* a_hL, const float a_intensImage, const float a_area) noexcept
{
  if (!a_hL)
    return;
      
  const float cd = a_intensImage * 683.0F * a_area;
  a_hL->SetIntensCd(cd);
}

void ConvertIntensCdToImage(HydraLight* a_hL, const float a_cd, const float a_area) noexcept
{
  if (!a_hL)
    return;
  
  const float intensImage = a_cd / 683.0F / a_area;
  a_hL->SetIntensImage(intensImage);
}



float GetArea(HydraLight* a_hL, HLType a_currType, TimeValue t, Interval& a_ivalid)
{
  if (!a_hL)
    return 1.0F;

  const float sizeX          = toMeters(a_hL->GetLength(t, a_ivalid));
  const float sizeY          = toMeters(a_hL->GetWidth (t, a_ivalid));
  const float radius         = toMeters(a_hL->GetRadius(t, a_ivalid));

  // The constant-intensity scaling of the light.
  ULONG handle               = 0;
  a_hL->NotifyDependents(FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE);
  INode* node                = GetCOREInterface()->GetINodeByHandle(handle);

  //float     scaleIntensity = (unitDispType == UNITDISP_GENERIC) ? (1.0F / 75.0F) : 1.0F;
  Matrix3   lightTransform   = toMeters(node->GetObjectTM(t));
  lightTransform.SetRow(3, Point3(0.0F, 0.0F, 0.0F)); // kill translate
  const float kf             = scaleWithMatrix(1.0F, lightTransform);  
  float scaleIntensity       = kf * kf;

  float area                 = 1.0F;

  switch (a_currType)
  {
  case HL_TYPE_POINT:     
    area                       = 1.0F;
    scaleIntensity             = 1.0F; 
    break;
  case HL_TYPE_RECTANGLE: [[fallthrough]];
  case HL_TYPE_PORTAL:    area =             sizeX  * sizeY;  break;
  case HL_TYPE_DISK:      area =        PI * radius * radius; break;
  case HL_TYPE_SPHERE:    area = 4.0F * PI * radius * radius; break;
  case HL_TYPE_CYLINDER:  area = 2.0F * PI * radius * sizeX;  break;
  default:                area = 1.0F;                        break;
  }

  return fmax(area * scaleIntensity, 0.000001F);
}


void UpdateIntensity(HydraLight* a_hL, const HLIntensUnit a_intensUnit, const TimeValue t, const float a_guiIntens, 
  const HLType a_currType, const float a_area)
{
  switch (a_intensUnit)
  {
  case INTENS_UNIT_IMAGE:
    if (a_hL->m_changeIntensUnit)
    {
      a_hL->pblockMain->SetValue(pb_intensity, t, a_hL->GetIntensImage());
      a_hL->m_changeIntensUnit = false;
    }
    else
    {
      a_hL->SetIntensImage(a_guiIntens);
      ConvertIntensImageToCd(a_hL, a_guiIntens, a_area);
      ConvertIntensCdToLm(a_hL, a_currType, a_hL->GetOriginalIntensity());
    }
    break;
  case INTENS_UNIT_CD:
    if (a_hL->m_changeIntensUnit)
    {
      a_hL->pblockMain->SetValue(pb_intensity, t, a_hL->GetOriginalIntensity());
      a_hL->m_changeIntensUnit = false;
    }
    else
    {
      a_hL->SetIntensCd(a_guiIntens);
      ConvertIntensCdToLm(a_hL, a_currType, a_guiIntens);
      ConvertIntensCdToImage(a_hL, a_guiIntens, a_area);
    }
    break;
  case INTENS_UNIT_LM:
    if (a_hL->m_changeIntensUnit)
    {
      a_hL->pblockMain->SetValue(pb_intensity, t, a_hL->GetIntensLm());
      a_hL->m_changeIntensUnit = false;
    }
    else
    {
      a_hL->SetIntensLm(a_guiIntens);
      ConvertIntensLmToCd(a_hL, a_currType, a_guiIntens);
      ConvertIntensCdToImage(a_hL, a_hL->GetOriginalIntensity(), a_area);
    }
    break;
  default:
    break;
  }
}

void HydraLight::Update(TimeValue t, Interval & valid)
{
  if (!m_ivalid.InInterval(t))
  {
    m_ivalid.SetInfinite();

    float guiIntens  = 0.0F;
    int   intensUnit = 0;

    pblockMain->    GetValue(pb_intensity     , t, guiIntens    , m_ivalid);
    pblockMain->    GetValue(pb_intensityUnits, t, intensUnit   , m_ivalid);
    pblockMain->    GetValue(pb_showCone      , t, m_showCone   , m_ivalid);
    pblockViewport->GetValue(pb_simpleShape   , t, m_simpleShape, m_ivalid);

    SetIntensUnits(static_cast<HLIntensUnit>(intensUnit));

    Type();  
    const float area = GetArea(this, m_hlType, t, m_ivalid);

    UpdateIntensity(this, static_cast<HLIntensUnit>(intensUnit), t, guiIntens, m_hlType, area);


    if      (isWeb())  m_distrib = WEB_DIST;
    else if (IsSpot()) m_distrib = SPOTLIGHT_DIST;
    else               m_distrib = ISOTROPIC_DIST;

    NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
  }
  valid &= m_ivalid;

  BuildMeshes(t, valid);
}



void RemoveScaling(Matrix3 &tm)
{
  AffineParts ap;
  decomp_affine(tm, &ap);
  tm.IdentityMatrix();
  tm.SetRotate(ap.q);
  tm.SetTrans(ap.t);
}

void HydraLight::GetMat(TimeValue t, INode * inode, ViewExp & vpt, Matrix3 & mat)
{
  if (!vpt.IsAlive())
  {
    mat.Zero();
    return;
  }

  mat = inode->GetObjectTM(t);

  // This light source is real size, so everything scaleFactor is not needed.
  //if (Type() == TYPE_POINT)
  //{
  //  RemoveScaling(mat);
  //  float scaleFactor = vpt.NonScalingObjectSize() * vpt.GetVPWorldWidth(mat.GetTrans()) / 360.0F;
  //  mat.Scale(Point3(scaleFactor, scaleFactor, scaleFactor));
  //}
}

bool HydraLight::isVisibleLight()
{
  int i;
  pblockMain->GetValue(pb_visInRender, TimeValue(0), i, FOREVER);
  return i;
}

bool HydraLight::isTargeted()
{
  int i;
  pblockMain->GetValue(pb_targeted, TimeValue(0), i, FOREVER);
  return i;
}

bool HydraLight::isWeb()
{
  int i;
  pblockMain->GetValue(pb_WEB_on, TimeValue(0), i, FOREVER);
  return i;
}



void HydraLight::SetVisibleLight(TimeValue t, bool on)
{
  pblockMain->SetValue(pb_visInRender, t, on);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void HydraLight::SetTargeted(TimeValue t, bool on)
{
  pblockMain->SetValue(pb_targeted, t, on);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void HydraLight::SetSpotLight(TimeValue t, bool on)
{
  pblockMain->SetValue(pb_spotlight, t, on);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void HydraLight::SetIntensity(TimeValue t, float f)
{
  pblockMain->SetValue(pb_intensity, t, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraLight::GetIntensity(TimeValue t, Interval & valid)
{
  float f;
  pblockMain->GetValue(pb_intensity, t, f, valid);
  return f;
}

void HydraLight::SetLength(TimeValue t, float length)
{
  pblockMain->SetValue(pb_length, t, length);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraLight::GetWidth(TimeValue t, Interval & valid) const
{
  float f;
  pblockMain->GetValue(pb_width, t, f, valid);
  return f;
}

void HydraLight::SetWidth(TimeValue t, float width)
{
  pblockMain->SetValue(pb_width, t, width);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}


int HydraLight::GetShape(Point3* pointsOut, int bufSize) const // not enter... How enable light in viewports?
{
  for (size_t i = 0; i < bufSize; ++i)
    pointsOut[i] = m_mesh->verts[i];
  
  return m_mesh->getNumVerts();
}


float HydraLight::GetRadius(TimeValue t, Interval & valid) const
{
  float f;
  pblockMain->GetValue(pb_radius, t, f, valid);
  return f;
}

void HydraLight::SetRadius(TimeValue t, float radius)
{
  pblockMain->SetValue(pb_radius, t, radius);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraLight::GetLength(TimeValue t, Interval & valid) const
{
  float f;
  pblockMain->GetValue(pb_length, t, f, valid);
  return f;
}

void HydraLight::SetType(int a_type)
{
  pblockMain->SetValue(pb_type, 0, a_type);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

int HydraLight::Type()
{
  m_hlType = HlType();
  
  int maxType = 0;

  switch (m_hlType)
  {
  case HL_TYPE_POINT:
  {
    if (isTargeted()) maxType = TARGET_POINT_TYPE;
    else              maxType = POINT_TYPE;
  }
  break;
  case HL_TYPE_RECTANGLE:
  {
    if (isTargeted()) maxType = TARGET_AREA_TYPE;
    else              maxType = AREA_TYPE;
  }
  break;
  case HL_TYPE_DISK:
  {
    if (isTargeted()) maxType = TARGET_DISC_TYPE;
    else              maxType = DISC_TYPE;
  }
  break;
  case HL_TYPE_SPHERE:
  {
    if (isTargeted()) maxType = TARGET_SPHERE_TYPE;
    else              maxType = SPHERE_TYPE;
  }
  break;
  case HL_TYPE_CYLINDER:
  {
    if (isTargeted()) maxType = TARGET_CYLINDER_TYPE;
    else              maxType = CYLINDER_TYPE;
  }
  break;
  case HL_TYPE_PORTAL:
  {
    if (isTargeted()) maxType = TARGET_AREA_TYPE;
    else              maxType = AREA_TYPE;
  }
  break;
  default:            maxType = POINT_TYPE;
    break;
  }  

  return maxType;
}


float HydraLight::GetFlux(TimeValue t, Interval & valid) const
{
  float f;
  pblockMain->GetValue(pb_intensity, t, f, valid);
  return 4.0f * PI * f;
}



static INode* FindNodeRef(ReferenceTarget *rt);

static INode* GetNodeRef(ReferenceMaker *rm)
{
  if (rm->SuperClassID() == BASENODE_CLASS_ID) return (INode *)rm;
  else return rm->IsRefTarget() ? FindNodeRef((ReferenceTarget *)rm) : NULL;
}

static INode* FindNodeRef(ReferenceTarget *rt)
{
  DependentIterator di(rt);
  ReferenceMaker *rm = NULL;
  INode *nd = NULL;
  while ((rm = di.Next()) != NULL)
  {
    nd = GetNodeRef(rm);
    if (nd) return nd;
  }
  return NULL;
}




void HydraLight::SetTarget(const int tp)
{
  // This is here to notify the viewports that the light type is changing.
  // It has to come before the change so that the viewport parameters don't
  // get screwed up.		// DB 4/29/99
  NotifyDependents(FOREVER, PART_OBJ, REFMSG_OBREF_CHANGE);


  Interface *iface = GetCOREInterface();
  TimeValue t = iface->GetTime();
  INode *nd = FindNodeRef(this);
  if (nd == NULL)
    return;

  //BOOL paramsShowing = FALSE;
  //if (hGeneralLight && (currentEditLight == this)) 
  //{ // LAM - 8/13/02 - defect 511609
  //  NotifyDependents(FOREVER, PART_OBJ, REFMSG_END_MODIFY_PARAMS);
  //  paramsShowing = TRUE;
  //  if (theHold.Holding())
  //    theHold.Put(new ParamsRest(this, 0));
  //}

  // bug fix
  // when changing the type of a light, this variable needs to be reinitialized or else
  // the shadow generator selector will be locked into its default (Shadow Map)
  //	ASshadType = NULL;
  // >>>>>the above was commented out when i went to make ASShadType a reference
  // not sure whether i shd add to new version of the commented line, which is
  // ReplaceReference( AS_SHADTYPE_REF, NULL );

  Interval v;
  float tdist = GetTDist(t, v);
  //if (oldtype == OMNI_LIGHT)
  //  tdist = 100.0F;
  //else if (HasTarg(oldtype))
  //  tdist = targDist;
  //type = tp;
  //int *pbd = pbdims[LIGHT_VERSION];
  //IParamBlock *pbnew = NULL;
  //if (pbd[oldtype] != pbd[type]) {
  //  pbnew = UpdateParameterBlock(
  //    GetDesc(LIGHT_VERSION, oldtype), GetDim(LIGHT_VERSION, oldtype), pblockMain,
  //    GetDesc(LIGHT_VERSION, type), GetDim(LIGHT_VERSION, type), LIGHT_VERSION);
  //  //ReplaceReference( 0, pbnew);	
  //}

  //if (theHold.Holding()) {
  //  theHold.Put(new SetTypeRest(this, oldtype, tp, pbnew ? pblock : NULL));
  //  if (pbnew) {
  //    theHold.Suspend();
  //    ReplaceReference(0, pbnew);
  //    theHold.Resume();
  //  }
  //}
  //else if (pbnew) {
  //  ReplaceReference(0, pbnew);
  //}


  //HWND hwgl = hGeneralLight;
  //hGeneralLight = NULL; // This keeps UpdateUI from jumping in

  //BuildMeshes(oldtype == OMNI_LIGHT ? 1 : 0);

  //hGeneralLight = hwgl;

  if (!isTargeted())
  {
    // get rid of target, assign a PRS controller for all instances
    DependentIterator di(this);
    ReferenceMaker *rm = NULL;
    // iterate through the instances
    while ((rm = di.Next()) != NULL)
    {
      nd = GetNodeRef(rm);
      if (nd)
      {
        INode* tn    = nd->GetTarget();
        Matrix3 tm   = nd->GetNodeTM(0);
        if (tn)
          iface->DeleteNode(tn);
        Control *tmc = NewDefaultMatrix3Controller();
        tmc->Copy(nd->GetTMController()); // didn't work!!?
        nd->SetTMController(tmc);
        nd->SetNodeTM(0, tm);
        SetTDist(t, tdist);	 //?? which one should this be for
      }
    }
  }

  if (isTargeted())
  {
    DependentIterator di(this);
    ReferenceMaker *rm = NULL;
    // iterate through the instances
    while ((rm = di.Next()) != NULL)
    {
      nd = GetNodeRef(rm);
      if (nd)
      {

        // > 9/9/02 - 2:17pm --MQM-- bug #435799
        // we were losing the light wire color when deleting/restoring the target.
        // (basically, the target had priority on color, so it forced the light
        // wire color to be set to the new target color).
        Color lightWireColor(nd->GetWireColor());

        // create a target, assign lookat controller
        Matrix3 tm         = nd->GetNodeTM(t);
        Matrix3 targtm     = tm;
        targtm.PreTranslate(Point3(0.0F, 0.0F, -tdist));
        Object *targObject = new TargetObject;
        INode *targNode    = iface->CreateObjectNode(targObject);
        TSTR targName;
        targName           = nd->GetName();
        targName += GetString(IDS_DB_DOT_TARGET);
        targNode->SetName(targName);
        Control *laControl = CreateLookatControl();
        targNode->SetNodeTM(0, targtm);
        laControl->SetTarget(targNode);
        laControl->Copy(nd->GetTMController());
        nd->SetTMController(laControl);
        targNode->SetIsTarget(1);

        // > 9/9/02 - 2:19pm --MQM-- 
        // force color of the new target...
        targNode->SetWireColor(lightWireColor.toRGB());
      }
    }
  }

  //if (IsSpot() && !IsDir()) 
  //{
  //  // make sure hotspot and fallof are properly constrained.
  //  SuspendAnimate();
  //  SetHotspot(t, GetHotspot(t, FOREVER));
  //  SetFallsize(t, GetFallsize(t, FOREVER));
  //  ResumeAnimate();
  //}

  //if (paramsShowing) 
  //{
  //  NotifyDependents(FOREVER, PART_OBJ, REFMSG_BEGIN_MODIFY_PARAMS);
  //  if (theHold.Holding())
  //    theHold.Put(new ParamsRest(this, 1));
  //}


  NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
  NotifyDependents(FOREVER, PART_OBJ, REFMSG_NUM_SUBOBJECTTYPES_CHANGED); // to redraw modifier stack
  iface->RedrawViews(t);
}



BOOL HydraLight::IsSpot()
{
  int i;
  pblockMain->GetValue(pb_spotlight, TimeValue(0), i, FOREVER);
  return i;
}

void HydraLight::SetUseLight(int onOff)
{
  pblockMain->SetValue(pb_on, TimeValue(0), onOff);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

BOOL HydraLight::GetUseLight(void)
{
  int i;
  pblockMain->GetValue(pb_on, TimeValue(0), i, FOREVER);
  return i;
}

#ifdef MAX2022
void HydraLight::SetRGBColor(TimeValue t, const Point3& rgb)
{
  pblockMain->SetValue(pb_color, t, rgb);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}
#else
void HydraLight::SetRGBColor(TimeValue t, Point3 & rgb)
{
  pblockMain->SetValue(pb_color, t, rgb);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}
#endif // MAX2022

Point3 HydraLight::GetRGBColor(TimeValue t, Interval & valid)
{
  Point3 p3;
  pblockMain->GetValue(pb_color, t, p3, valid);
  return p3;
}

void HydraLight::SetHotspot(TimeValue time, float f)
{
  pblockMain->SetValue(pb_hotspot, time, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraLight::GetHotspot(TimeValue t, Interval & valid)
{
  float f;
  pblockMain->GetValue(pb_hotspot, t, f, valid);
  return f;
}

void HydraLight::SetFallsize(TimeValue time, float f)
{
  pblockMain->SetValue(pb_fallsize, time, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraLight::GetFallsize(TimeValue t, Interval & valid)
{
  float f;
  pblockMain->GetValue(pb_fallsize, t, f, valid);
  return f;
}

void HydraLight::SetAtten(TimeValue time, int which, float f)
{
  //which = LIGHT_ATTEN_START;

}




void HydraLight::BuildMeshes(TimeValue t, Interval& valid)
{
  Type();
  switch(m_hlType)
  {
    case HL_TYPE_POINT:
      BuildPointMesh(t, valid);
      break;
    case HL_TYPE_RECTANGLE: [[fallthrough]];
    case HL_TYPE_PORTAL:
      BuildRectMesh(t, valid);
      break;
    case HL_TYPE_DISK:
      BuildDiskMesh(t, valid);
      break;
    case HL_TYPE_SPHERE:
      if (m_simpleShape)
        BuildSimpleSphereMesh(t, valid);
      else
        BuildSphereMesh(t, valid);
      break;
    case HL_TYPE_CYLINDER:
      BuildCylinderMesh(t, valid);
      break;
    default:
      break;
  }

  m_mesh = &m_buildMesh;

  if (IsSpot())
    BuildSmallSpotlightMesh(t, valid);
}

void HydraLight::BuildPointMesh(TimeValue t, Interval& valid)
{
  const float s = GetRadius(t, valid);
  const float FZ = 0.0F;

  const int nverts = 6;
  const int nfaces = 8;

  // Build a leetle octahedron
  m_buildMesh.setNumVerts(nverts);
  m_buildMesh.setNumFaces(nfaces);

  m_buildMesh.setVert(0, Point3(FZ, FZ, -s));
  m_buildMesh.setVert(1, Point3(s, FZ, FZ));
  m_buildMesh.setVert(2, Point3(FZ, s, FZ));
  m_buildMesh.setVert(3, Point3(-s, FZ, FZ));
  m_buildMesh.setVert(4, Point3(FZ, -s, FZ));
  m_buildMesh.setVert(5, Point3(FZ, FZ, s));
  m_buildMesh.faces[0].setVerts(0, 1, 4);
  m_buildMesh.faces[1].setVerts(0, 4, 3);
  m_buildMesh.faces[2].setVerts(0, 3, 2);
  m_buildMesh.faces[3].setVerts(0, 2, 1);
  m_buildMesh.faces[4].setVerts(5, 1, 2);
  m_buildMesh.faces[5].setVerts(5, 2, 3);
  m_buildMesh.faces[6].setVerts(5, 3, 4);
  m_buildMesh.faces[7].setVerts(5, 4, 1);

  for (int i = 0; i < nfaces; i++)
  {
    m_buildMesh.faces[i].setSmGroup(i);
    m_buildMesh.faces[i].setEdgeVisFlags(1, 1, 1);
  }

  m_buildMesh.buildNormals();
  m_buildMesh.EnableEdgeList(1);
}

void HydraLight::BuildRectMesh(TimeValue t, Interval& valid)
{
  const int nverts = 11;
  const int nfaces = 7;

  const float halfLength = GetLength(t, valid) / 2.0F;
  const float halfWidth = GetWidth(t, valid) / 2.0F;

  m_buildMesh.setNumVerts(nverts);
  m_buildMesh.setNumFaces(nfaces);

  // Rectangle
  m_buildMesh.setVert(0, Point3(-halfWidth, -halfLength, 0.0000F));   // offset for so that the geometry on sides view does not disappear.
  m_buildMesh.setVert(1, Point3(halfWidth, -halfLength, 0.0001f));
  m_buildMesh.setVert(2, Point3(halfWidth, halfLength, 0.0000F));
  m_buildMesh.setVert(3, Point3(-halfWidth, halfLength, 0.0001f));

  m_buildMesh.faces[0].setVerts(0, 1, 2);
  m_buildMesh.faces[0].setEdgeVisFlags(1, 1, 0);
  m_buildMesh.faces[1].setVerts(0, 2, 3);
  m_buildMesh.faces[1].setEdgeVisFlags(0, 1, 1);

  // Line with arrow
  const float lengthDirLine = (halfLength + halfWidth) / 4.0F;

  // Big arrow

  m_buildMesh.setVert(4, Point3(0.0000F, -lengthDirLine * 0.1f, 0.0F));
  m_buildMesh.setVert(5, Point3(0.0000F, -lengthDirLine * 0.1f, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(6, Point3(0.0000F, -lengthDirLine * 0.2f, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(7, Point3(0.0000F, 0.0F, -lengthDirLine));
  m_buildMesh.setVert(8, Point3(0.0001f, lengthDirLine * 0.2f, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(9, Point3(0.0001f, lengthDirLine * 0.1f, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(10, Point3(0.0001f, lengthDirLine * 0.1f, 0.0F));


  m_buildMesh.faces[2].setVerts(4, 5, 10);
  m_buildMesh.faces[3].setVerts(5, 9, 10);
  m_buildMesh.faces[4].setVerts(5, 6, 7);
  m_buildMesh.faces[5].setVerts(5, 7, 9);
  m_buildMesh.faces[6].setVerts(9, 7, 8);

  m_buildMesh.faces[2].setEdgeVisFlags(1, 0, 1);
  m_buildMesh.faces[3].setEdgeVisFlags(0, 1, 0);
  m_buildMesh.faces[4].setEdgeVisFlags(1, 1, 0);
  m_buildMesh.faces[5].setEdgeVisFlags(0, 0, 0);
  m_buildMesh.faces[6].setEdgeVisFlags(0, 1, 1);

  for (int i = 0; i < nfaces; i++)
    m_buildMesh.faces[i].setSmGroup(i);

  m_buildMesh.buildNormals();
  m_buildMesh.EnableEdgeList(1);
}



void HydraLight::BuildDiskMesh(TimeValue t, Interval& valid)
{
  //vertices  
  const int nVertForCenter = 1;
  const int nVertsForDisk = 16;
  const int nVertForArrow = 7;
  const int nAllVerts = nVertForCenter + nVertsForDisk + nVertForArrow;

  //faces
  const int nFacesForDisk = nVertsForDisk;
  const int nFacesForArrow = 5;
  const int nAllFaces = nFacesForDisk + nFacesForArrow;

  const float radius = GetRadius(t, valid);

  m_buildMesh.setNumVerts(nAllVerts);
  m_buildMesh.setNumFaces(nAllFaces);

  // center
  m_buildMesh.setVert(0, Point3(0.0F, 0.0F, radius*0.1f));   // offset for so that the geometry on sides view does not disappear.

  // Disk
  const float angle = (float)(TWOPI / (float)nVertsForDisk);

  for (int i = 0; i < nVertsForDisk; i++)
  {
    const float currAngle = angle * i;
    m_buildMesh.setVert(i + nVertForCenter, Point3(cos(currAngle) * radius, sin(currAngle) * radius, 0.0F));
  }

  for (int i = 0; i < nFacesForDisk; i++)
  {
    int currVert2 = i + 2;
    if (currVert2 > nFacesForDisk) currVert2 = 1; // end vert = first for loop circle.

    m_buildMesh.faces[i].setVerts(0, 1 + i, currVert2);
    m_buildMesh.faces[i].setEdgeVisFlags(0, 1, 0);
  }

  // Direct line
  const float lengthDirLine = radius / 2.0F;

  // Big arrow
  const int v4 = nAllVerts - 7;
  const int v5 = nAllVerts - 6;
  const int v6 = nAllVerts - 5;
  const int v7 = nAllVerts - 4;
  const int v8 = nAllVerts - 3;
  const int v9 = nAllVerts - 2;
  const int v10 = nAllVerts - 1;

  m_buildMesh.setVert(v4, Point3(-lengthDirLine * 0.1f, 0.0000F, 0.0F));
  m_buildMesh.setVert(v5, Point3(-lengthDirLine * 0.1f, 0.0000F, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(v6, Point3(-lengthDirLine * 0.2f, 0.0000F, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(v7, Point3(0.0F, 0.0000F, -lengthDirLine));
  m_buildMesh.setVert(v8, Point3(lengthDirLine * 0.2f, 0.0001f, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(v9, Point3(lengthDirLine * 0.1f, 0.0001f, -lengthDirLine * 0.8f));
  m_buildMesh.setVert(v10, Point3(lengthDirLine * 0.1f, 0.0001f, 0.0F));


  m_buildMesh.faces[nAllFaces - 5].setVerts(v4, v5, v10);
  m_buildMesh.faces[nAllFaces - 4].setVerts(v5, v9, v10);
  m_buildMesh.faces[nAllFaces - 3].setVerts(v5, v6, v7);
  m_buildMesh.faces[nAllFaces - 2].setVerts(v5, v7, v9);
  m_buildMesh.faces[nAllFaces - 1].setVerts(v9, v7, v8);

  m_buildMesh.faces[nAllFaces - 5].setEdgeVisFlags(1, 0, 1);
  m_buildMesh.faces[nAllFaces - 4].setEdgeVisFlags(0, 1, 0);
  m_buildMesh.faces[nAllFaces - 3].setEdgeVisFlags(1, 1, 0);
  m_buildMesh.faces[nAllFaces - 2].setEdgeVisFlags(0, 0, 0);
  m_buildMesh.faces[nAllFaces - 1].setEdgeVisFlags(0, 1, 1);

  for (int i = 0; i < nAllFaces; i++)
    m_buildMesh.faces[i].setSmGroup(0);

  m_buildMesh.buildNormals();
  m_buildMesh.EnableEdgeList(1);
}

void HydraLight::BuildSimpleSphereMesh(TimeValue t, Interval& valid)
{
  //vertices  
  const int nVertForCenter = 1;
  const int nVertsForDisk = 16;

  const int nAllVerts = nVertForCenter + nVertsForDisk * 3;

  //faces
  const int nFacesForDisk = nVertsForDisk;
  const int nAllFaces = nFacesForDisk * 3;

  const float radius = GetRadius(t, valid);

  m_buildMesh.setNumVerts(nAllVerts);
  m_buildMesh.setNumFaces(nAllFaces);

  // center
  m_buildMesh.setVert(0, Point3(-0.0001f, 0.0001f, 0.0001f));

  const float angle = (float)(TWOPI / (float)nVertsForDisk);

  // Disk 1
  for (int i = 0; i < nVertsForDisk; i++)
  {
    const float currAngle = angle * i;
    m_buildMesh.setVert(i + nVertForCenter, Point3(cos(currAngle) * radius, sin(currAngle) * radius, 0.0F));
  }

  for (int i = 0; i < nFacesForDisk; i++)
  {
    int currVert2 = i + 2;
    if (currVert2 > nFacesForDisk) currVert2 = 1; // end vert = first for loop circle.

    m_buildMesh.faces[i].setVerts(0, i + 1, currVert2);
    m_buildMesh.faces[i].setEdgeVisFlags(0, 1, 0);
  }

  // Disk 2
  for (int i = 0; i < nVertsForDisk; i++)
  {
    const float currAngle = angle * i;
    m_buildMesh.setVert(i + nVertForCenter + nVertsForDisk, Point3(cos(currAngle) * radius, 0.0F, sin(currAngle) * radius));
  }

  for (int i = nFacesForDisk; i < nFacesForDisk * 2; i++)
  {
    int currVert2 = i + 2;
    if (currVert2 > nVertsForDisk * 2) currVert2 = nVertsForDisk + 1; // end vert = first for loop circle.

    m_buildMesh.faces[i].setVerts(0, i + 1, currVert2);
    m_buildMesh.faces[i].setEdgeVisFlags(0, 1, 0);
  }

  // Disk 3
  for (int i = 0; i < nVertsForDisk; i++)
  {
    const float currAngle = angle * i;
    m_buildMesh.setVert(i + nVertForCenter + nVertsForDisk * 2, Point3(0.0F, cos(currAngle) * radius, sin(currAngle) * radius));
  }

  for (int i = nFacesForDisk * 2; i < nFacesForDisk * 3; i++)
  {
    int currVert2 = i + 2;
    if (currVert2 > nVertsForDisk * 3) currVert2 = nVertsForDisk * 2 + 1; // end vert = first for loop circle.

    m_buildMesh.faces[i].setVerts(0, i + 1, currVert2);
    m_buildMesh.faces[i].setEdgeVisFlags(0, 1, 0);
  }

  for (int i = 0; i < nAllFaces; i++)
  {
    m_buildMesh.faces[i].setSmGroup(0);
  }

  m_buildMesh.buildNormals();
  m_buildMesh.EnableEdgeList(1);
}

// Assumed in the following function: the vertices have the same radius, or
// distance from the origin, and they have nontrivial cross product.

void SphericalInterpolate(Mesh& amesh, int v1, int v2, int *current, int num)
{
  int i;
  float theta, theta1, theta2, sn, cs, rad;
  Point3 a, b, c;

  if (num < 2) { return; }
  a        = amesh.getVert(v1);
  b        = amesh.getVert(v2);
  rad      = DotProd(a, a);

  if (rad  == 0)
  {
    for (i = 1; i < num; ++i) amesh.setVert((*current)++, a);
    return;
  }
  cs       = DotProd(a, b) / rad;
  LimitValue(cs, -1.0F, 1.0F);
  theta    = (float)acos(cs);
  sn       = Sin(theta);

  for (i = 1; i < num; ++i)
  {
    theta1 = (theta*i) / num;
    theta2 = (theta*(num - i)) / num;
    c = (a*Sin(theta2) + b * Sin(theta1)) / sn;
    amesh.setVert((*current)++, c);
  }
}

int icosa_find_vert_index(int s, int f, int r, int c)
{							// segs, face, row, column

  if (r == 0)
  {	// Top corner of face
    if (f < 5) { return 0; }
    if (f > 14) { return 11; }
    return f - 4;
  }

  if ((r == s) && (c == 0))
  { // Lower left corner of face
    if (f < 5) { return f + 1; }
    if (f < 10) { return (f + 4) % 5 + 6; }
    if (f < 15) { return (f + 1) % 5 + 1; }
    return (f + 1) % 5 + 6;
  }

  if ((r == s) && (c == s))
  { // Lower right corner
    if (f < 5) { return (f + 1) % 5 + 1; }
    if (f < 10) { return f + 1; }
    if (f < 15) { return f - 9; }
    return f - 9;
  }

  if (r == s)
  { // Bottom edge
    if (f < 5) { return 12 + (5 + f)*(s - 1) + c - 1; }
    if (f < 10) { return 12 + (20 + (f + 4) % 5)*(s - 1) + c - 1; }
    if (f < 15) { return 12 + (f - 5)*(s - 1) + s - 1 - c; }
    return 12 + (5 + f)*(s - 1) + s - 1 - c;
  }

  if (c == 0)
  { // Left edge
    if (f < 5) { return 12 + f * (s - 1) + r - 1; }
    if (f < 10) { return 12 + (f % 5 + 15)*(s - 1) + r - 1; }
    if (f < 15) { return 12 + ((f + 1) % 5 + 15)*(s - 1) + s - 1 - r; }
    return 12 + ((f + 1) % 5 + 25)*(s - 1) + r - 1;
  }

  if (c == r)
  { // Right edge
    if (f < 5) { return 12 + ((f + 1) % 5)*(s - 1) + r - 1; }
    if (f < 10) { return 12 + (f % 5 + 10)*(s - 1) + r - 1; }
    if (f < 15) { return 12 + (f % 5 + 10)*(s - 1) + s - 1 - r; }
    return 12 + (f % 5 + 25)*(s - 1) + r - 1;
  }

  // Not an edge or corner.
  return 12 + 30 * (s - 1) + f * (s - 1)*(s - 2) / 2 + (r - 1)*(r - 2) / 2 + c - 1;
}

void HydraLight::BuildSphereMesh(TimeValue t, Interval& valid)
{
  size_t nf = 0;
  int    nv = 0;
  UINT row, column, face, a, b, c, d;
  float subrad, subz, theta, sn, cs;

  constexpr UINT  segs      = 3;
  constexpr UINT  nsections = 20;
  const float     radius    = GetRadius(t, valid);

  constexpr UINT  nfaces    = nsections * segs * segs;
  constexpr UINT  nverts    = nfaces / 2 + 2;

  m_buildMesh.setNumVerts(nverts);
  m_buildMesh.setNumFaces(nfaces);

  // Based on the Icosahedron
  // First 12 icosahedral vertices

  m_buildMesh.setVert(nv++, 0.0F, 0.0F, radius);
  subz   = Sqrt(0.2F) * radius;
  subrad = 2 * subz;

  for (face = 0; face < 5; ++face)
  {
    theta = 2.0F * PI * (float)face / 5.0F;
    SinCos(theta, &sn, &cs);
    m_buildMesh.setVert(nv++, subrad*cs, subrad*sn, subz);
  }

  for (face = 0; face < 5; ++face)
  {
    theta = PI * (float)(2 * face + 1) / 5.0F;
    SinCos(theta, &sn, &cs);
    m_buildMesh.setVert(nv++, subrad*cs, subrad*sn, -subz);
  }

  m_buildMesh.setVert(nv++, 0.0F, 0.0F, -radius);

  // Edge vertices: 6*5*(segs-1) of these.
  for (face = 0; face < 5; face++) SphericalInterpolate(m_buildMesh, 0, face + 1, &nv, segs);
  for (face = 0; face < 5; face++) SphericalInterpolate(m_buildMesh, face + 1, (face + 1) % 5 + 1, &nv, segs);
  for (face = 0; face < 5; face++) SphericalInterpolate(m_buildMesh, face + 1, face + 6, &nv, segs);
  for (face = 0; face < 5; face++) SphericalInterpolate(m_buildMesh, face + 1, (face + 4) % 5 + 6, &nv, segs);
  for (face = 0; face < 5; face++) SphericalInterpolate(m_buildMesh, face + 6, (face + 1) % 5 + 6, &nv, segs);
  for (face = 0; face < 5; face++) SphericalInterpolate(m_buildMesh, 11, face + 6, &nv, segs);

  // Face vertices: 4 rows of 5 faces each.
  for (face = 0; face < 5; ++face)
  {
    for (int i = 1; i < segs - 1; ++i)
      SphericalInterpolate(m_buildMesh, 12 + face * (segs - 1) + i, 12 + ((face + 1) % 5)*(segs - 1) + i, &nv, i + 1);
  }

  for (face = 0; face < 5; ++face)
  {
    for (int i = 1; i < segs - 1; ++i)
      SphericalInterpolate(m_buildMesh, 12 + (face + 15)*(segs - 1) + i, 12 + (face + 10)*(segs - 1) + i, &nv, i + 1);
  }

  for (face = 0; face < 5; ++face)
  {
    for (int i = 1; i < segs - 1; ++i)
      SphericalInterpolate(m_buildMesh, 12 + ((face + 1) % 5 + 15)*(segs - 1) + segs - 2 - i, 12 + (face + 10)*(segs - 1) + segs - 2 - i, &nv, i + 1);
  }

  for (face = 0; face < 5; ++face)
  {
    for (int i = 1; i < segs - 1; ++i)
      SphericalInterpolate(m_buildMesh, 12 + ((face + 1) % 5 + 25)*(segs - 1) + i, 12 + (face + 25)*(segs - 1) + i, &nv, i + 1);
  }


  // Now make faces 

  for (int i = 0; i < nfaces; ++i)
    m_buildMesh.faces[i].setEdgeVisFlags(1, 1, 1);


  for (face = 0; face < nsections; ++face)
  {
    for (row = 0; row < segs; ++row)
    {
      for (column = 0; column <= row; ++column)
      {
        a = icosa_find_vert_index(segs, face, row, column);
        b = icosa_find_vert_index(segs, face, row + 1, column);
        c = icosa_find_vert_index(segs, face, row + 1, column + 1);
        m_buildMesh.faces[nf].setVerts(a, b, c);
        nf++;

        if (column < row)
        {
          d = icosa_find_vert_index(segs, face, row, column + 1);
          m_buildMesh.faces[nf].setVerts(a, c, d);
          nf++;
        }
      }
    }
  }

  for (int i = 0; i < nfaces; ++i)
    m_buildMesh.faces[i].setSmGroup(0);

  m_buildMesh.buildNormals();
  m_buildSmallSpotMesh.EnableEdgeList(1);
}



void HydraLight::BuildCylinderMesh(TimeValue t, Interval& valid)
{
  const float radius = GetRadius(t, valid);
  const float halfLength = GetLength(t, valid) / 2.0F;

  const int nVertsForDisk = 16;
  const int nVerts = nVertsForDisk * 2;
  const int nFaces = nVerts;

  m_buildMesh.setNumVerts(nVerts);
  m_buildMesh.setNumFaces(nFaces);


  // Disk
  const float angle = (float)(TWOPI / (float)nVertsForDisk);

  for (int i = 0; i < nVertsForDisk; i++)
  {
    const float currAngle = angle * i;
    m_buildMesh.setVert(i, Point3(cos(currAngle) * radius * 0.9999f, halfLength, sin(currAngle) * radius * 0.9999f));
    m_buildMesh.setVert(i + nVertsForDisk, Point3(cos(currAngle) * radius, -halfLength, sin(currAngle) * radius));
  }

  for (int i = 0; i < nVertsForDisk; i++)
  {
    int secondVert = i + 1;
    if (secondVert >= nVertsForDisk) secondVert = 0; // end vert = first for loop circle.

    m_buildMesh.faces[i * 2].setVerts(i, secondVert, secondVert + nVertsForDisk);
    m_buildMesh.faces[i * 2].setEdgeVisFlags(1, 1, 0);

    m_buildMesh.faces[i * 2 + 1].setVerts(i, secondVert + nVertsForDisk, i + nVertsForDisk);
    m_buildMesh.faces[i * 2 + 1].setEdgeVisFlags(0, 1, 0);
  }


  for (int i = 0; i < nFaces; i++)
    m_buildMesh.faces[i].setSmGroup(i);

  m_buildMesh.buildNormals();
  m_buildMesh.EnableEdgeList(1);
}



void HydraLight::BuildSmallSpotlightMesh(TimeValue t, Interval& valid)
{
  const float radang = 3.1415926f * GetFallsize(TimeValue(0), m_ivalid) / 360.0F;
  float scale = 1.0F;
  Type();
  if      (m_hlType == HL_TYPE_DISK)  scale = GetRadius(t, valid);
  else if (m_hlType == HL_TYPE_POINT) scale = 20.0F;
  else                                scale = (GetWidth(t, valid) + GetLength(t, valid)) / 4.0F;

  const float FZ = 0.0F;
  const float h = 0.5f * scale;					    // hypotenuse
  const float d = -h * (float)cos(radang);	// dist from origin to cone circle
  const float r = h * (float)sin(radang);	  // radius of cone circle
  const float s = 0.70711f * r;  			      // sin(45) * r

  const int nverts = 9;
  const int nfaces = 8;

  m_buildSmallSpotMesh.setNumVerts(nverts);
  m_buildSmallSpotMesh.setNumFaces(nfaces);

  // build a cone
  m_buildSmallSpotMesh.setVert(0, Point3(FZ, FZ, FZ));
  m_buildSmallSpotMesh.setVert(1, Point3(-r, FZ, d));
  m_buildSmallSpotMesh.setVert(2, Point3(-s, -s, d));
  m_buildSmallSpotMesh.setVert(3, Point3(FZ, -r, d));
  m_buildSmallSpotMesh.setVert(4, Point3(s, -s, d));
  m_buildSmallSpotMesh.setVert(5, Point3(r, FZ, d));
  m_buildSmallSpotMesh.setVert(6, Point3(s, s, d));
  m_buildSmallSpotMesh.setVert(7, Point3(FZ, r, d));
  m_buildSmallSpotMesh.setVert(8, Point3(-s, s, d));
  m_buildSmallSpotMesh.faces[0].setVerts(0, 1, 2);
  m_buildSmallSpotMesh.faces[1].setVerts(0, 2, 3);
  m_buildSmallSpotMesh.faces[2].setVerts(0, 3, 4);
  m_buildSmallSpotMesh.faces[3].setVerts(0, 4, 5);
  m_buildSmallSpotMesh.faces[4].setVerts(0, 5, 6);
  m_buildSmallSpotMesh.faces[5].setVerts(0, 6, 7);
  m_buildSmallSpotMesh.faces[6].setVerts(0, 7, 8);
  m_buildSmallSpotMesh.faces[7].setVerts(0, 8, 1);

  for (int i = 0; i < nfaces; i++)
  {
    m_buildSmallSpotMesh.faces[i].setSmGroup(i);
    m_buildSmallSpotMesh.faces[i].setEdgeVisFlags(1, 1, 1);
  }

  m_buildSmallSpotMesh.buildNormals();
  m_buildSmallSpotMesh.EnableEdgeList(1);

  m_meshSmallSpot = &m_buildSmallSpotMesh;
}



void HydraLight::GetConePoints(TimeValue t, float angle, float dist, Point3* q)
{
  // CIRCULAR
  const float rad = dist * tanf(0.5F * DegToRad(angle));;
  
  for (size_t i = 0; i < NUM_CIRC_PTS; ++i)
  {
    const float a = (float)i * TWOPI / (float)NUM_CIRC_PTS;
    q[i]          = Point3(rad * sinf(a), rad * cosf(a), -dist);
  }

  // alexc - june.12.2003 - made Roll Angle Indicator size proportional to radius
  q[NUM_CIRC_PTS]    = q[0];
  q[NUM_CIRC_PTS].y *= 1.15F; // notch length is 15% of the radius
}



static int GetTargetPoint(TimeValue t, INode *inode, Point3& p)
{
  Matrix3 tmat;
  if (inode->GetTargetTM(t, tmat))
  {
    p = tmat.GetTrans();
    return 1;
  }
  else
    return 0;
}


void HydraLight::UpdateTargDistance(TimeValue t, INode* inode)
{
  if (inode && isTargeted())
  {
    Point3 pt;
                
    if (GetTargetPoint(t, inode, pt))
    {
      Matrix3 tm = inode->GetObjectTM(t);
      float den = FLength(tm.GetRow(2));
      float dist = (den != 0.0F) ? FLength(tm.GetTrans() - pt) / den : 0.0F;
      m_targDist = dist;
      //const TCHAR *buf;
      //buf = FormatUniverseValue(targDist);
      //SetWindowText(GetDlgItem(hGeneralLight, IDC_TARG_DISTANCE), buf);
    }
  }
}

float HydraLight::GetKelvin(TimeValue t, Interval & v)
{
  float f;
  pblockMain->GetValue(pb_kelvinTemp, t, f, v);
  return f;
}

void HydraLight::SetKelvin(TimeValue t, float kelvin)
{
  pblockMain->SetValue(pb_kelvinTemp, t, kelvin);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

BOOL HydraLight::GetUseKelvin()
{
  int i;
  pblockMain->GetValue(pb_kelvin, TimeValue(0), i, FOREVER);
  return i;
}

void HydraLight::SetUseKelvin(BOOL useKelvin)
{
  pblockMain->SetValue(pb_kelvin, TimeValue(0), useKelvin);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}


MaxSDK::AssetManagement::AssetUser HydraLight::GetWebFile() const
{
  MaxSDK::AssetManagement::AssetUser webfile;
  pblockMain->GetValue(pb_WEB_file, TimeValue(0), webfile, FOREVER);
  return webfile;
}

const MCHAR * HydraLight::GetFullWebFileName() const
{
  return GetWebFile().GetFullFilePath();
}


float HydraLight::GetWebRotateX() const
{
  float f;
  pblockMain->GetValue(pb_WEB_rotX, TimeValue(0), f, FOREVER);
  return f;
}

void HydraLight::SetWebRotateX(float degrees)
{
  pblockMain->SetValue(pb_WEB_rotX, TimeValue(0), degrees);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraLight::GetWebRotateY() const
{
  float f;
  pblockMain->GetValue(pb_WEB_rotY, TimeValue(0), f, FOREVER);
  return f;
}

void HydraLight::SetWebRotateY(float degrees)
{
  pblockMain->SetValue(pb_WEB_rotY, TimeValue(0), degrees);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraLight::GetWebRotateZ() const
{
  float f;
  pblockMain->GetValue(pb_WEB_rotZ, TimeValue(0), f, FOREVER);
  return f;
}

void HydraLight::SetWebRotateZ(float degrees)
{
  pblockMain->SetValue(pb_WEB_rotZ, TimeValue(0), degrees);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}



void HydraLight::InvalidateUI()
{
  main_param_blk.InvalidateUI(pblockMain->LastNotifyParamID());
  viewport_param_blk.InvalidateUI(pblockMain->LastNotifyParamID());
}



void HydraLight::Reset()
{
  m_ivalid.SetEmpty();

  //for (int i = NSUBMTL; i < NSUBTEX + NSUBMTL; i++)
  //{
  //  if (subTex[i])
  //  {
  //    DeleteReference(i);
  //    subTex[i] = NULL;
  //  }
  //}

  GetHydraLightDesc()->MakeAutoParamBlocks(this);
}




RefTargetHandle HydraLight::Clone(RemapDir & remap)
{
  HydraLight* hl = new HydraLight(false);
  hl->ReplaceReference(PB_MAIN, remap.CloneRef(pblockMain));
  hl->ReplaceReference(PB_VIEWPORT, remap.CloneRef(pblockViewport));
  BaseClone(this, hl, remap);
  return(hl);
}



int HydraLightCreateCallBack::proc(ViewExp * vpt, int msg, int point, int flags, IPoint2 m, Matrix3 & mat)
{
  if (!vpt || !vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return FALSE;
  }


#ifdef _OSNAP
  if (msg == MOUSE_FREEMOVE)
  {
#ifdef _3D_CREATE
    vpt->SnapPreview(m, m, NULL, SNAP_IN_3D);
#else
    vpt->SnapPreview(m, m, NULL, SNAP_IN_PLANE);
#endif
  }
#endif

  if (msg == MOUSE_POINT || msg == MOUSE_MOVE)
  {
    // 6/19/01 11:37am --MQM-- wire-color changes
    // since we're now allowing the user to set the color of
    // the light wire-frames, we need to set the initial light 
    // color to yellow, instead of the default random object color.
    ULONG handle;
    INode * node;

    ob->NotifyDependents(FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE);
    node = GetCOREInterface()->GetINodeByHandle(handle);
    if (node)
    {
      Point3 color = GetUIColor(COLOR_LIGHT_OBJ);	// yellow wire color
      node->SetWireColor(RGB(color.x*255.0F, color.y*255.0F, color.z*255.0F));
    }

    switch (point)
    {
      case 0: // only happens with MOUSE_POINT msg
        p0 = vpt->SnapPoint(m, m, NULL, SNAP_IN_3D);
        mat.SetTrans(p0);
        ob->Enable(1);
        break;
      case 1:
      {
        p1 = vpt->SnapPoint(m, m, NULL, SNAP_IN_PLANE);

        const float dragLength = p1.y - p0.y;
        const float dragWidth = p1.x - p0.x;

        if (abs(dragLength) > 1.0F && abs(dragWidth) > 1.0F)
        {
          Point3 center;
          TimeValue t = ob->ip->GetTime();
          const int type = ob->Type();

          switch (type)
          {
          case LightscapeLight::TARGET_AREA_TYPE:
          case LightscapeLight::AREA_TYPE:
          {
            ob->SetLength(t, abs(dragLength));
            ob->SetWidth(t, abs(dragWidth));
            const float radius = sqrt(dragLength * dragLength + dragWidth * dragWidth) / 2.0F;
            ob->SetRadius(t, radius);
            ob->SetTDist(t, radius * 2.0F);
            center.Set((float)p0.x + dragWidth / 2.0F, (float)p0.y + dragLength / 2.0F, (float)p0.z + (p1.z - p0.z) / 2.0F);
          }
          break;
          case LightscapeLight::TARGET_SPHERE_TYPE:
          case LightscapeLight::SPHERE_TYPE:
          {
            const float radius = sqrt(dragLength * dragLength + dragWidth * dragWidth);
            ob->SetLength(t, radius * 2.0F);
            ob->SetWidth(t, radius * 2.0F);
            ob->SetRadius(t, radius);
            ob->SetTDist(t, radius * 2.0F);
            center.Set((float)p0.x, (float)p0.y, (float)p0.z);
          }
          break;
          case LightscapeLight::TARGET_CYLINDER_TYPE:
          case LightscapeLight::CYLINDER_TYPE:
          {
            ob->SetLength(t, abs(dragLength));
            ob->SetWidth(t, abs(dragWidth));
            ob->SetRadius(t, dragWidth);
            ob->SetTDist(t, dragLength * 2.0F);
            center.Set((float)p0.x + dragWidth / 2.0F, (float)p0.y + dragLength / 2.0F, (float)p0.z + (p1.z - p0.z) / 2.0F);
          }
          break;
          default:
            center.Set((float)p1.x, (float)p1.y, (float)p1.z);
            break;
          }

          mat.SetTrans(center);
          ob->Enable(1);
        }

        if (msg == MOUSE_POINT)
          return CREATE_STOP;
        break;
      }
      case 2:
        return CREATE_STOP;
        break;
    }
  }
  else
  {
    if (msg == MOUSE_ABORT)
      return CREATE_ABORT;
  }

  return TRUE;
}



HydraLightDlgProc::HydraLightDlgProc(HydraLight * cb)
{
  hL = cb;
}




INT_PTR HydraLightDlgProc::DlgProc(TimeValue t, IParamMap2 * map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  Interface* ip          = GetCOREInterface();
  HydraLightDlgProc *dlg = DLGetWindowLongPtr<HydraLightDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
    case WM_INITDIALOG:
    {
      DragAcceptFiles(GetDlgItem(hWnd, IDC_WEB_FILE), true);

      // File name IES button
      MSTR webFileName = hL->GetWebFile().GetFileName();
      if (webFileName != nullptr)
      {
        wchar_t fname[255];
        wchar_t dir[255];
        wchar_t drive[3];
        wchar_t ext[5];
        _wsplitpath_s(webFileName, drive, dir, fname, ext);
        map->SetText(pb_WEB_file, fname);
      }

      // check and initialize
      if (IsDlgButtonChecked(hWnd, IDC_KELVIN_ON)) map->Enable(pb_kelvinTemp, true);
      else                                         map->Enable(pb_kelvinTemp, false);

      hL->Type();
      SetControlForTypeLight(hWnd, map, hL->m_hlType);
      ip->RedrawViews(ip->GetTime());

      return TRUE;
    }
    case WM_COMMAND:
      switch (LOWORD(wParam))
      {
        case IDC_TARGETED_ON:          
          hL->SetTarget(hL->Type());
          break;
        case IDC_TYPE:
          if (HIWORD(wParam) == LBN_SELCHANGE)
          {
            hL->Type();
            SetControlForTypeLight(hWnd, map, hL->m_hlType);
            ip->RedrawViews(ip->GetTime());
          }
          break;
        case IDC_EXCLUDE_LIST:
          if (HIWORD(wParam) == BN_BUTTONUP || HIWORD(wParam) == BN_BUTTONDOWN)
          {
            // open exclude list windows 
          }
          break;
        case IDC_INTENSITY_UNITS:
          if (HIWORD(wParam) == LBN_SELCHANGE)
            hL->m_changeIntensUnit = true;
          break;
        case IDC_KELVIN_ON:
          if (IsDlgButtonChecked(hWnd, IDC_KELVIN_ON)) map->Enable(pb_kelvinTemp, true);
          else                                         map->Enable(pb_kelvinTemp, false);          
          ip->RedrawViews(ip->GetTime());
          break;
        case IDC_WEB_ON:
          if (IsDlgButtonChecked(hWnd, IDC_WEB_ON)) hL->SetDistribution(HydraLight::DistTypes::WEB_DIST);
          else                                      hL->SetDistribution(HydraLight::DistTypes::ISOTROPIC_DIST);
          SetControlFromDistribution(hWnd, map, hL->GetDistribution());
          ip->RedrawViews(ip->GetTime());
          break;
        case IDC_SPOTLIGHT_ON:
          if (IsDlgButtonChecked(hWnd, IDC_SPOTLIGHT_ON)) hL->SetDistribution(HydraLight::DistTypes::SPOTLIGHT_DIST);
          else                                            hL->SetDistribution(HydraLight::DistTypes::ISOTROPIC_DIST);
          SetControlFromDistribution(hWnd, map, hL->GetDistribution());
          ip->RedrawViews(ip->GetTime());
          break;
      }

      break;
    case WM_DESTROY:
      //ReleaseICustButton(ExcludeList);
      //ReleaseICustButton(IES_file);
      break;

    default:
      return FALSE;
  }
  return TRUE;
}



void HydraLightDlgProc::SetControlForTypeLight(HWND hwnd, IParamMap2* map, const HLType type)
{
  const TimeValue t(0);

  switch (type)
  {
    case HL_TYPE_POINT: [[fallthrough]];
    case HL_TYPE_DISK:
      map->Enable(pb_length, false);
      map->Enable(pb_width, false);
      map->Enable(pb_radius, true);
      EnableWindow(GetDlgItem(hwnd, IDC_LENGHT_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WIDTH_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_RADIUS_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_INTENSITY_UNITS), true);
      break;
    case HL_TYPE_RECTANGLE:
      map->Enable(pb_length, true);
      map->Enable(pb_width, true);
      map->Enable(pb_radius, false);
      EnableWindow(GetDlgItem(hwnd, IDC_LENGHT_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WIDTH_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_RADIUS_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_INTENSITY_UNITS), true);
      break;
    case HL_TYPE_SPHERE:
      map->Enable(pb_length, false);
      map->Enable(pb_width, false);
      map->Enable(pb_radius, true);
      EnableWindow(GetDlgItem(hwnd, IDC_LENGHT_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WIDTH_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_RADIUS_STATIC), true);
      hL->SetDistribution(HydraLight::DistTypes::DIFFUSE_DIST);
      EnableWindow(GetDlgItem(hwnd, IDC_INTENSITY_UNITS), true);
      break;
    case HL_TYPE_CYLINDER:
      map->Enable(pb_length, true);
      map->Enable(pb_width, false);
      map->Enable(pb_radius, true);
      EnableWindow(GetDlgItem(hwnd, IDC_LENGHT_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WIDTH_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_RADIUS_STATIC), true);
      hL->SetDistribution(HydraLight::DistTypes::DIFFUSE_DIST);
      EnableWindow(GetDlgItem(hwnd, IDC_INTENSITY_UNITS), true);
      break;
    case HL_TYPE_PORTAL:
      map->Enable(pb_length, true);
      map->Enable(pb_width, true);
      map->Enable(pb_radius, false);
      EnableWindow(GetDlgItem(hwnd, IDC_LENGHT_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WIDTH_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_RADIUS_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_INTENSITY_UNITS), false);
      hL->SetDistribution(HydraLight::DistTypes::DIFFUSE_DIST);
      hL->pblockMain->SetValue(pb_intensityUnits, t, INTENS_UNIT_IMAGE);
      break;
    default:
      break;
  }
  SetControlFromDistribution(hwnd, map, hL->GetDistribution());
}

void HydraLightDlgProc::SetControlFromDistribution(HWND hwnd, IParamMap2 * map, const HydraLight::DistTypes a_distrib)
{
  TimeValue t(0);

  switch (a_distrib)
  {
    case HydraLight::DistTypes::ISOTROPIC_DIST: // All enable
      map->Enable(pb_WEB_on, true);
      map->Enable(pb_WEB_file, true);
      map->Enable(pb_WEB_rotX, true);
      map->Enable(pb_WEB_rotY, true);
      map->Enable(pb_WEB_rotZ, true);
      EnableWindow(GetDlgItem(hwnd, IDC_IES_FRAME_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_X_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Y_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Z_STATIC), true);

      map->Enable(pb_spotlight, true);
      map->Enable(pb_showCone, true);
      map->Enable(pb_hotspot, true);
      map->Enable(pb_fallsize, true);
      EnableWindow(GetDlgItem(hwnd, IDC_SPOTLIGHT_FRAME_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_SHOWCONE_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_HOTSPOT_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_FALLOFF_STATIC), true);
      break;
    case HydraLight::DistTypes::DIFFUSE_DIST:
      map->Enable(pb_WEB_on, false);
      map->Enable(pb_WEB_file, false);
      map->Enable(pb_WEB_rotX, false);
      map->Enable(pb_WEB_rotY, false);
      map->Enable(pb_WEB_rotZ, false);
      hL->pblockMain->SetValue(pb_WEB_on, t, 0);
      EnableWindow(GetDlgItem(hwnd, IDC_IES_FRAME_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_X_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Y_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Z_STATIC), false);

      map->Enable(pb_spotlight, false);
      map->Enable(pb_showCone, false);
      map->Enable(pb_hotspot, false);
      map->Enable(pb_fallsize, false);
      hL->pblockMain->SetValue(pb_spotlight, t, FALSE);
      EnableWindow(GetDlgItem(hwnd, IDC_SPOTLIGHT_FRAME_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_SHOWCONE_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_HOTSPOT_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_FALLOFF_STATIC), false);
      break;
    case HydraLight::DistTypes::WEB_DIST:
      map->Enable(pb_WEB_on, true);
      map->Enable(pb_WEB_file, true);
      map->Enable(pb_WEB_rotX, true);
      map->Enable(pb_WEB_rotY, true);
      map->Enable(pb_WEB_rotZ, true);
      hL->pblockMain->SetValue(pb_WEB_on, t, TRUE);
      EnableWindow(GetDlgItem(hwnd, IDC_IES_FRAME_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_X_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Y_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Z_STATIC), true);

      map->Enable(pb_spotlight, false);
      map->Enable(pb_showCone, false);
      map->Enable(pb_hotspot, false);
      map->Enable(pb_fallsize, false);
      hL->pblockMain->SetValue(pb_spotlight, t, FALSE);
      EnableWindow(GetDlgItem(hwnd, IDC_SPOTLIGHT_FRAME_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_SHOWCONE_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_HOTSPOT_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_FALLOFF_STATIC), false);
      break;
    case HydraLight::DistTypes::SPOTLIGHT_DIST:
      map->Enable(pb_WEB_on, false);
      map->Enable(pb_WEB_file, false);
      map->Enable(pb_WEB_rotX, false);
      map->Enable(pb_WEB_rotY, false);
      map->Enable(pb_WEB_rotZ, false);
      hL->pblockMain->SetValue(pb_WEB_on, t, FALSE);
      EnableWindow(GetDlgItem(hwnd, IDC_IES_FRAME_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_X_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Y_STATIC), false);
      EnableWindow(GetDlgItem(hwnd, IDC_WEB_ROT_Z_STATIC), false);

      map->Enable(pb_spotlight, true);
      map->Enable(pb_showCone, true);
      map->Enable(pb_hotspot, true);
      map->Enable(pb_fallsize, true);
      hL->pblockMain->SetValue(pb_spotlight, t, TRUE);
      EnableWindow(GetDlgItem(hwnd, IDC_SPOTLIGHT_FRAME_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_SHOWCONE_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_HOTSPOT_STATIC), true);
      EnableWindow(GetDlgItem(hwnd, IDC_FALLOFF_STATIC), true);
      break;

    default:
      break;
  }
}



void LightConeItem::Realize(MaxSDK::Graphics::DrawContext& drawContext)
{
  const INode* node = drawContext.GetCurrentNode();

  if (!node)
    return;

  mpLight->SetExtendedDisplay(drawContext.GetExtendedDisplayMode());
  ClearLines();
  BuildConeAndLine(drawContext.GetTime(), drawContext.GetCurrentNode());
  SplineRenderItem::Realize(drawContext);
}

void LightConeItem::BuildCone(TimeValue t, float dist)
{
  if (mpLight == nullptr)
  {
    DbgAssert(true);
    return;
  }

  Point3 posArray[NUM_CIRC_PTS + 1], tmpArray[2];
  int dirLight = 0;
  int i;

  mpLight->GetConePoints(t, mpLight->GetHotspot(t), dist, posArray);
  Color color(GetUIColor(COLOR_HOTSPOT));

  // CIRCULAR
  if (mpLight->GetHotspot(t) >= mpLight->GetFallsize(t))
  {
    // draw (far) hotspot circle
    tmpArray[0] = posArray[0];
    tmpArray[1] = posArray[NUM_CIRC_PTS];
    AddLineStrip(tmpArray, color, 2, false, false);
  }
  AddLineStrip(posArray, color, NUM_CIRC_PTS, true, false);

  // draw 4 axial lines
  tmpArray[0] = Point3(0, 0, 0);
  for (i = 0; i < NUM_CIRC_PTS; i += SEG_INDEX)
  {
    tmpArray[1] = posArray[i];
    AddLineStrip(tmpArray, color, 2, false, false);
  }

  mpLight->GetConePoints(t, mpLight->GetFallsize(t), dist, posArray);
  color = GetUIColor(COLOR_FALLOFF);
  if (mpLight->GetHotspot(t) < mpLight->GetFallsize(t))
  {
    // draw (far) fallsize circle
    tmpArray[0] = posArray[0];
    tmpArray[1] = posArray[NUM_CIRC_PTS];
    AddLineStrip(tmpArray, color, 2, false, false);
    tmpArray[0] = Point3(0, 0, 0);
  }
  AddLineStrip(posArray, color, NUM_CIRC_PTS, true, false);

  float cs = (float)cos(DEG_TO_RAD * (mpLight->GetFallsize(t)*0.5f));
  float dfar = posArray[0].z;

  tmpArray[0] = Point3(0, 0, 0);
  for (i = 0; i < NUM_CIRC_PTS; i += SEG_INDEX)
  {
    //Give a default Point3 instead of a division by zero
    tmpArray[1] = (dist != 0.0F ? -posArray[i] * dfar / dist : Point3(0.0F, 0.0F, 0.0F));
    AddLineStrip(tmpArray, color, 2, false, false);
  }
}


void LightConeItem::BuildConeAndLine(TimeValue t, INode* inode)
{
  mpLight->Type();
  if (!inode || !mpLight->IsSpot() || (mpLight->m_hlType == HL_TYPE_PORTAL || mpLight->m_hlType == HL_TYPE_SPHERE || mpLight->m_hlType == HL_TYPE_CYLINDER))
    return;

  if (mpLight->isTargeted())
  {
    Point3 pt;

    if (GetTargetPoint(t, inode, pt))
    {
      const Matrix3 tm   = inode->GetObjectTM(t);
      const float   den  = FLength(tm.GetRow(2));
      const float   dist = (den != 0.0F) ? FLength(tm.GetTrans() - pt) / den : 0.0F;
      mpLight->SetTDist(0, dist);

      //if (mpLight->hSpotLight && (mpLight->currentEditLight == mpLight)) { // LAM - 8/13/02 - defect 511609
      //  const TCHAR *buf = FormatUniverseValue(mpLight->targDist);
      //  SetWindowText(GetDlgItem(mpLight->hGeneralLight, IDC_TARG_DISTANCE), buf);
      //}

      if (mpLight->GetConeDisplay() || (mpLight->GetExtendedDisplay() & EXT_DISP_ONLY_SELECTED))
        BuildCone(t, dist);
    }
  }
  else
  {
    if (mpLight->GetConeDisplay() || (mpLight->GetExtendedDisplay() & EXT_DISP_ONLY_SELECTED))
      BuildCone(t, mpLight->GetTDist(t));
  }
}



void LightTargetLineItem::Realize(MaxSDK::Graphics::DrawContext& drawContext)
{
  INode* inode = drawContext.GetCurrentNode();

  if (inode == nullptr /*|| !mpLight->isSpotLight()*/)
    return;

  if (mpLight->isTargeted())
  {
    mpLight->SetExtendedDisplay(drawContext.GetExtendedDisplayMode());
    inode->SetTargetNodePair(0);
    const TimeValue t  = drawContext.GetTime();
    Point3 pt;

    if (GetTargetPoint(t, inode, pt))
    {
      const Matrix3 tm   = inode->GetObjectTM(t);
      const float   den  = FLength(tm.GetRow(2));
      const float   dist = (den != 0.0F) ? FLength(tm.GetTrans() - pt) / den : 0.0F;
      mpLight->SetTDist(0, dist);
      Color color(inode->GetWireColor());

      if (!inode->IsFrozen() && !inode->Dependent())
      {
        // 6/22/01 2:18pm --MQM-- 
        // if the user has changed the light's wire-frame color,
        // use that color to draw the target line.
        // otherwise, use the standard target-line color.
        if (color == GetUIColor(COLOR_LIGHT_OBJ))
          color = GetUIColor(COLOR_TARGET_LINE);
      }
      if (m_lastColor != color || m_lastDist != dist)
      {
        ClearLines();
        Point3 posArray[2] ={ Point3(0,0,0), Point3(0.0F, 0.0F, -dist) };
        AddLineStrip(&posArray[0], color, 2, false, true);
        m_lastColor = color;
        m_lastDist = dist;
      }
    }
  }
  SplineRenderItem::Realize(drawContext);
}

void LightTargetLineItem::OnHit(MaxSDK::Graphics::HitTestContext& /*hittestContext*/, MaxSDK::Graphics::DrawContext& drawContext)
{
  INode* node = drawContext.GetCurrentNode();
  if (node) 
    node->SetTargetNodePair(1);
}


void TSpotCreationManager::CreateNewObject()
{
  lgtObject    = (HydraLight *)cDesc->Create();
  lastPutCount = theHold.GetGlobalPutCount();

  macroRec->BeginCreate(cDesc);  // JBW 4/23/99

                                 // Start the edit params process
  if (lgtObject)
    lgtObject->BeginEditParams((IObjParam*)createInterface, BEGIN_EDIT_CREATE, NULL);
}


RefResult TSpotCreationManager::NotifyRefChanged(const Interval & changeInt, RefTargetHandle hTarget, PartID & partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
    case REFMSG_PRENOTIFY_PASTE:
    case REFMSG_TARGET_SELECTIONCHANGE:
      if (ignoreSelectionChange)
      {
        break;
      }
      if (lgtObject && lgtNode == hTarget)
      {
        // this will set camNode== NULL;
        theHold.Suspend();
        DeleteReference(0);
        theHold.Resume();
        goto endEdit;
      }
      // fall through

    case REFMSG_TARGET_DELETED:
      if (lgtObject && lgtNode == hTarget)
      {
      endEdit:
        lgtObject->EndEditParams((IObjParam*)createInterface, 0, NULL);
        lgtObject       = NULL;
        lgtNode         = NULL;
        CreateNewObject();
        attachedToNode  = FALSE;
      }
      else if (targNode == hTarget)
      {
        targNode        = NULL;
        targObject      = NULL;
      }
      break;
  }
  return REF_SUCCEED;
}

void TSpotCreationManager::Begin(IObjCreate * ioc, ClassDesc * desc)
{
  createInterface = ioc;
  cDesc           = desc;
  attachedToNode  = FALSE;
  createCB        = NULL;
  lgtNode         = NULL;
  targNode        = NULL;
  lgtObject       = NULL;
  targObject      = NULL;
  CreateNewObject();
}

void TSpotCreationManager::End()
{
  if (lgtObject)
  {
    lgtObject->EndEditParams((IObjParam*)createInterface, END_EDIT_REMOVEUI, NULL);
    if (!attachedToNode)
    {
      // RB 4-9-96: Normally the hold isn't holding when this 
      // happens, but it can be in certain situations (like a track view paste)
      // Things get confused if it ends up with undo...
      theHold.Suspend();
      //delete lgtObject;
      lgtObject->DeleteAllRefsFromMe();
      lgtObject->DeleteAllRefsToMe();
      lgtObject->DeleteThis();  // JBW 11.1.99, this allows scripted plugin lights to delete cleanly
      lgtObject = NULL;
      theHold.Resume();
      // RB 7/28/97: If something has been put on the undo stack since this object was created, we have to flush the undo stack.
      if (theHold.GetGlobalPutCount() != lastPutCount)
      {
        GetSystemSetting(SYSSET_CLEAR_UNDO);
      }
      macroRec->Cancel();  // JBW 4/23/99
    }
    else if (lgtNode)
    {
      // Get rid of the reference.
      theHold.Suspend();
      DeleteReference(0);  // sets lgtNode = NULL
      theHold.Resume();
    }
  }
}

static BOOL needToss;

int TSpotCreationManager::proc(HWND hwnd, int msg, int point, int flag, IPoint2 m)
{
  int res = CREATE_CONTINUE;
  TSTR targName;
  ViewExp& vpx = createInterface->GetViewExp(hwnd);
  assert(vpx.IsAlive());

  switch (msg)
  {
    case MOUSE_POINT:
      switch (point)
      {
        case 0:
        {
          pt0 = m;
          assert(lgtObject);
          vpx.CommitImplicitGrid(m, flag); //KENNY MERGE
          if (createInterface->SetActiveViewport(hwnd))
            return FALSE;

          if (createInterface->IsCPEdgeOnInView())
          {
            res = FALSE;
            goto done;
          }

          // if lights were hidden by category, re-display them
          GetCOREInterface()->SetHideByCategoryFlags(
            GetCOREInterface()->GetHideByCategoryFlags() & ~HIDE_LIGHTS);

          if (attachedToNode)
          {
            // send this one on its way
            lgtObject->EndEditParams((IObjParam*)createInterface, 0, NULL);
            macroRec->EmitScript();  // JBW 4/23/99

                                     // Get rid of the reference.
            if (lgtNode)
            {
              theHold.Suspend();
              DeleteReference(0);
              theHold.Resume();
            }

            // new object
            CreateNewObject();   // creates lgtObject
          }

          needToss = theHold.GetGlobalPutCount() != lastPutCount;

          theHold.Begin();	 // begin hold for undo
          mat.IdentityMatrix();

          // link it up
          INode *lightNode = createInterface->CreateObjectNode(lgtObject);
          attachedToNode   = TRUE;
          assert(lightNode);
          createCB         = lgtObject->GetCreateMouseCallBack();
          createInterface->SelectNode(lightNode);

          // Create target object and node
          targObject = new TargetObject;
          assert(targObject);
          targNode = createInterface->CreateObjectNode(targObject);
          assert(targNode);
          targName = lightNode->GetName();
          targName += GetString(IDS_DB_DOT_TARGET);
          macroRec->Disable();
          targNode->SetName(targName);
          macroRec->Enable();

          // hook up camera to target using lookat controller.
          createInterface->BindToTarget(lightNode, targNode);

          // Reference the new node so we'll get notifications.
          theHold.Suspend();
          ReplaceReference(0, lightNode);
          theHold.Resume();

          // Position camera and target at first point then drag.
          mat.IdentityMatrix();
          //mat[3] = vpx.GetPointOnCP(m);
          mat.SetTrans(vpx.SnapPoint(m, m, NULL, SNAP_IN_3D));
          createInterface->SetNodeTMRelConstPlane(lgtNode, mat);
          createInterface->SetNodeTMRelConstPlane(targNode, mat);
          lgtObject->Enable(1);

          ignoreSelectionChange = TRUE;
          createInterface->SelectNode(targNode, 0);
          ignoreSelectionChange = FALSE;
          res = TRUE;

          // 6/19/01 11:37am --MQM-- 
          // set the wire-color of the light to be the default
          // color (yellow)
          if (lgtNode)
          {
            Point3 color = GetUIColor(COLOR_LIGHT_OBJ);
            lgtNode->SetWireColor(RGB(color.x*255.0F, color.y*255.0F, color.z*255.0F));
          }
          break;
        }

        case 1:
          if (Length(m - pt0) < 2)
            goto abort;
          //mat[3] = vpx.GetPointOnCP(m);
          mat.SetTrans(vpx.SnapPoint(m, m, NULL, SNAP_IN_3D));
          macroRec->Disable();   // JBW 4/23/99
          createInterface->SetNodeTMRelConstPlane(targNode, mat);
          macroRec->Enable();

          ignoreSelectionChange = TRUE;
          createInterface->SelectNode(lgtNode);
          ignoreSelectionChange = FALSE;

          theHold.Accept(IDS_DS_CREATE);

          createInterface->AddLightToScene(lgtNode);
          createInterface->RedrawViews(createInterface->GetTime());

          res = FALSE;	// We're done
          break;
      }
      break;

    case MOUSE_MOVE:
      //mat[3] = vpx.GetPointOnCP(m);
      mat.SetTrans(vpx.SnapPoint(m, m, NULL, SNAP_IN_3D));
      macroRec->Disable();   // JBW 4/23/99
      createInterface->SetNodeTMRelConstPlane(targNode, mat);
      macroRec->Enable();
      createInterface->RedrawViews(createInterface->GetTime());

      macroRec->SetProperty(lgtObject, _T("target"),   // JBW 4/23/99
                            mr_create, Class_ID(TARGET_CLASS_ID, 0), GEOMOBJECT_CLASS_ID, 1, _T("transform"), mr_matrix3, &mat);

      res = TRUE;
      break;

    case MOUSE_FREEMOVE:
      SetCursor(UI::MouseCursors::LoadMouseCursor(UI::MouseCursors::Crosshair));
      //Snap Preview
      vpx.SnapPreview(m, m, NULL, SNAP_IN_3D);
      vpx.TrackImplicitGrid(m); //KENNY MERGE
      break;

    case MOUSE_PROPCLICK:
      // right click while between creations
      createInterface->RemoveMode(NULL);
      break;

    case MOUSE_ABORT:
    abort:
      assert(lgtObject);
      lgtObject->EndEditParams((IObjParam*)createInterface, 0, NULL);
      // Toss the undo stack if param changes have been made
      macroRec->Cancel();  // JBW 4/23/99
      theHold.Cancel();	 // deletes both the camera and target.
      if (needToss)
        GetSystemSetting(SYSSET_CLEAR_UNDO);
      lgtNode = NULL;
      targNode = NULL;
      createInterface->RedrawViews(createInterface->GetTime());
      CreateNewObject();
      attachedToNode = FALSE;
      res = FALSE;
  }

done:
  if (res == CREATE_STOP || res == CREATE_ABORT)
    vpx.ReleaseImplicitGrid();

  return res;
}

void HydraLightPBAccessor::Set(PB2Value & v, ReferenceMaker * owner, ParamID id, int tabIndex, TimeValue t)
{
  HydraLight* u = (HydraLight*)owner;

  int kelvinTemp;
  unsigned int numElem;
  Color color(1.0F, 1.0F, 1.0F);
  float hotspot, falloff;

  switch (id)
  {
    case pb_kelvin:
    case pb_kelvinTemp:
      u->pblockMain->GetValue(pb_kelvinTemp, t, kelvinTemp, FOREVER);

      numElem = (kelvinTemp - 1000) / 100 * 3;
      color.r = kelvinTempTable[numElem];
      color.g = kelvinTempTable[numElem + 1];
      color.b = kelvinTempTable[numElem + 2];

      u->pblockMain->SetValue(pb_color, t, color);
      break;

    case pb_hotspot:
      u->pblockMain->GetValue(pb_fallsize, t, falloff, FOREVER);
      hotspot = v.f;
      if (hotspot > falloff)
        u->pblockMain->SetValue(pb_fallsize, t, hotspot + 1.0F);
      break;

    case pb_fallsize:
      u->pblockMain->GetValue(pb_hotspot, t, hotspot, FOREVER);
      falloff = v.f;
      if (hotspot > falloff)
        u->pblockMain->SetValue(pb_hotspot, t, falloff - 1.0F);
      break;

    default:
      break;
  }
}


ShadowType * HydraLight::ActiveShadowType()
{
  Interface *ip = GetCOREInterface();
  return ip->GetGlobalShadowGenerator();
}

