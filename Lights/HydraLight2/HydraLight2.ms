/*
Last change: 27.03.2023
Author: Ray Tracing Systems.
Autosave the framebuffer at intervals.
*/

clearListener()


fn KelvinToRgb a_kelvinTemp = 
(
	--http://www.vendian.org/mncharity/dir3/blackbody/UnstableURLs/bbr_color.html
	--Blackbody color. Mitchell Charity. CIE 1964 10 degree CMFs.
	-- 1000K - 40000K, step: 100K.

	local kelvinTempTable = #(
	1.0000, 0.0401, 0.0000,
	1.0000, 0.0631, 0.0000,
	1.0000, 0.0860, 0.0000,
	1.0000, 0.1085, 0.0000,
	1.0000, 0.1303, 0.0000,
	1.0000, 0.1515, 0.0000,
	1.0000, 0.1718, 0.0000,
	1.0000, 0.1912, 0.0000,
	1.0000, 0.2097, 0.0000,
	1.0000, 0.2272, 0.0000,
	1.0000, 0.2484, 0.0061,
	1.0000, 0.2709, 0.0153,
	1.0000, 0.2930, 0.0257,
	1.0000, 0.3149, 0.0373,
	1.0000, 0.3364, 0.0501,
	1.0000, 0.3577, 0.0640,
	1.0000, 0.3786, 0.0790,
	1.0000, 0.3992, 0.0950,
	1.0000, 0.4195, 0.1119,
	1.0000, 0.4394, 0.1297,
	1.0000, 0.4589, 0.1483,
	1.0000, 0.4781, 0.1677,
	1.0000, 0.4970, 0.1879,
	1.0000, 0.5155, 0.2087,
	1.0000, 0.5336, 0.2301,
	1.0000, 0.5515, 0.2520,
	1.0000, 0.5689, 0.2745,
	1.0000, 0.5860, 0.2974,
	1.0000, 0.6028, 0.3207,
	1.0000, 0.6193, 0.3444,
	1.0000, 0.6354, 0.3684,
	1.0000, 0.6511, 0.3927,
	1.0000, 0.6666, 0.4172,
	1.0000, 0.6817, 0.4419,
	1.0000, 0.6966, 0.4668,
	1.0000, 0.7111, 0.4919,
	1.0000, 0.7253, 0.5170,
	1.0000, 0.7392, 0.5422,
	1.0000, 0.7528, 0.5675,
	1.0000, 0.7661, 0.5928,
	1.0000, 0.7792, 0.6180,
	1.0000, 0.7919, 0.6433,
	1.0000, 0.8044, 0.6685,
	1.0000, 0.8167, 0.6937,
	1.0000, 0.8286, 0.7187,
	1.0000, 0.8403, 0.7437,
	1.0000, 0.8518, 0.7686,
	1.0000, 0.8630, 0.7933,
	1.0000, 0.8740, 0.8179,
	1.0000, 0.8847, 0.8424,
	1.0000, 0.8952, 0.8666,
	1.0000, 0.9055, 0.8907,
	1.0000, 0.9156, 0.9147,
	1.0000, 0.9254, 0.9384,
	1.0000, 0.9351, 0.9619,
	1.0000, 0.9445, 0.9853,
	0.9917, 0.9458, 1.0000,
	0.9696, 0.9336, 1.0000,
	0.9488, 0.9219, 1.0000,
	0.9290, 0.9107, 1.0000,
	0.9102, 0.9000, 1.0000,
	0.8923, 0.8897, 1.0000,
	0.8753, 0.8799, 1.0000,
	0.8591, 0.8704, 1.0000,
	0.8437, 0.8614, 1.0000,
	0.8289, 0.8527, 1.0000,
	0.8149, 0.8443, 1.0000,
	0.8014, 0.8363, 1.0000,
	0.7885, 0.8285, 1.0000,
	0.7762, 0.8211, 1.0000,
	0.7644, 0.8139, 1.0000,
	0.7531, 0.8069, 1.0000,
	0.7423, 0.8002, 1.0000,
	0.7319, 0.7938, 1.0000,
	0.7219, 0.7875, 1.0000,
	0.7123, 0.7815, 1.0000,
	0.7030, 0.7757, 1.0000,
	0.6941, 0.7700, 1.0000,
	0.6856, 0.7645, 1.0000,
	0.6773, 0.7593, 1.0000,
	0.6693, 0.7541, 1.0000,
	0.6617, 0.7492, 1.0000,
	0.6543, 0.7444, 1.0000,
	0.6471, 0.7397, 1.0000,
	0.6402, 0.7352, 1.0000,
	0.6335, 0.7308, 1.0000,
	0.6271, 0.7265, 1.0000,
	0.6208, 0.7224, 1.0000,
	0.6148, 0.7183, 1.0000,
	0.6089, 0.7144, 1.0000,
	0.6033, 0.7106, 1.0000,
	0.5978, 0.7069, 1.0000,
	0.5925, 0.7033, 1.0000,
	0.5873, 0.6998, 1.0000,
	0.5823, 0.6964, 1.0000,
	0.5774, 0.6930, 1.0000,
	0.5727, 0.6898, 1.0000,
	0.5681, 0.6866, 1.0000,
	0.5637, 0.6836, 1.0000,
	0.5593, 0.6806, 1.0000,
	0.5551, 0.6776, 1.0000,
	0.5510, 0.6748, 1.0000,
	0.5470, 0.6720, 1.0000,
	0.5432, 0.6693, 1.0000,
	0.5394, 0.6666, 1.0000,
	0.5357, 0.6640, 1.0000,
	0.5322, 0.6615, 1.0000,
	0.5287, 0.6590, 1.0000,
	0.5253, 0.6566, 1.0000,
	0.5220, 0.6542, 1.0000,
	0.5187, 0.6519, 1.0000,
	0.5156, 0.6497, 1.0000,
	0.5125, 0.6474, 1.0000,
	0.5095, 0.6453, 1.0000,
	0.5066, 0.6432, 1.0000,
	0.5037, 0.6411, 1.0000,
	0.5009, 0.6391, 1.0000,
	0.4982, 0.6371, 1.0000,
	0.4955, 0.6351, 1.0000,
	0.4929, 0.6332, 1.0000,
	0.4904, 0.6314, 1.0000,
	0.4879, 0.6295, 1.0000,
	0.4854, 0.6277, 1.0000,
	0.4831, 0.6260, 1.0000,
	0.4807, 0.6243, 1.0000,
	0.4785, 0.6226, 1.0000,
	0.4762, 0.6209, 1.0000,
	0.4740, 0.6193, 1.0000,
	0.4719, 0.6177, 1.0000,
	0.4698, 0.6161, 1.0000,
	0.4677, 0.6146, 1.0000,
	0.4657, 0.6131, 1.0000,
	0.4638, 0.6116, 1.0000,
	0.4618, 0.6102, 1.0000,
	0.4599, 0.6087, 1.0000,
	0.4581, 0.6073, 1.0000,
	0.4563, 0.6060, 1.0000,
	0.4545, 0.6046, 1.0000,
	0.4527, 0.6033, 1.0000,
	0.4510, 0.6020, 1.0000,
	0.4493, 0.6007, 1.0000,
	0.4477, 0.5994, 1.0000,
	0.4460, 0.5982, 1.0000,
	0.4445, 0.5970, 1.0000,
	0.4429, 0.5958, 1.0000,
	0.4413, 0.5946, 1.0000,
	0.4398, 0.5935, 1.0000,
	0.4384, 0.5923, 1.0000,
	0.4369, 0.5912, 1.0000,
	0.4355, 0.5901, 1.0000,
	0.4341, 0.5890, 1.0000,
	0.4327, 0.5879, 1.0000,
	0.4313, 0.5869, 1.0000,
	0.4300, 0.5859, 1.0000,
	0.4287, 0.5848, 1.0000,
	0.4274, 0.5838, 1.0000,
	0.4261, 0.5829, 1.0000,
	0.4249, 0.5819, 1.0000,
	0.4236, 0.5809, 1.0000,
	0.4224, 0.5800, 1.0000,
	0.4212, 0.5791, 1.0000,
	0.4201, 0.5781, 1.0000,
	0.4189, 0.5772, 1.0000,
	0.4178, 0.5763, 1.0000,
	0.4167, 0.5755, 1.0000,
	0.4156, 0.5746, 1.0000,
	0.4145, 0.5738, 1.0000,
	0.4134, 0.5729, 1.0000,
	0.4124, 0.5721, 1.0000,
	0.4113, 0.5713, 1.0000,
	0.4103, 0.5705, 1.0000,
	0.4093, 0.5697, 1.0000,
	0.4083, 0.5689, 1.0000,
	0.4074, 0.5681, 1.0000,
	0.4064, 0.5674, 1.0000,
	0.4055, 0.5666, 1.0000,
	0.4045, 0.5659, 1.0000,
	0.4036, 0.5652, 1.0000,
	0.4027, 0.5644, 1.0000,
	0.4018, 0.5637, 1.0000,
	0.4009, 0.5630, 1.0000,
	0.4001, 0.5623, 1.0000,
	0.3992, 0.5616, 1.0000,
	0.3984, 0.5610, 1.0000,
	0.3975, 0.5603, 1.0000,
	0.3967, 0.5596, 1.0000,
	0.3959, 0.5590, 1.0000,
	0.3951, 0.5584, 1.0000,
	0.3943, 0.5577, 1.0000,
	0.3935, 0.5571, 1.0000,
	0.3928, 0.5565, 1.0000,
	0.3920, 0.5559, 1.0000,
	0.3913, 0.5553, 1.0000,
	0.3905, 0.5547, 1.0000,
	0.3898, 0.5541, 1.0000,
	0.3891, 0.5535, 1.0000,
	0.3884, 0.5529, 1.0000,
	0.3877, 0.5524, 1.0000,
	0.3870, 0.5518, 1.0000,
	0.3863, 0.5513, 1.0000,
	0.3856, 0.5507, 1.0000,
	0.3850, 0.5502, 1.0000,
	0.3843, 0.5496, 1.0000,
	0.3836, 0.5491, 1.0000,
	0.3830, 0.5486, 1.0000,
	0.3824, 0.5481, 1.0000,
	0.3817, 0.5476, 1.0000,
	0.3811, 0.5471, 1.0000,
	0.3805, 0.5466, 1.0000,
	0.3799, 0.5461, 1.0000,
	0.3793, 0.5456, 1.0000,
	0.3787, 0.5451, 1.0000,
	0.3781, 0.5446, 1.0000,
	0.3776, 0.5441, 1.0000,
	0.3770, 0.5437, 1.0000,
	0.3764, 0.5432, 1.0000,
	0.3759, 0.5428, 1.0000,
	0.3753, 0.5423, 1.0000,
	0.3748, 0.5419, 1.0000,
	0.3742, 0.5414, 1.0000,
	0.3737, 0.5410, 1.0000,
	0.3732, 0.5405, 1.0000,
	0.3726, 0.5401, 1.0000,
	0.3721, 0.5397, 1.0000,
	0.3716, 0.5393, 1.0000,
	0.3711, 0.5389, 1.0000,
	0.3706, 0.5384, 1.0000,
	0.3701, 0.5380, 1.0000,
	0.3696, 0.5376, 1.0000,
	0.3692, 0.5372, 1.0000,
	0.3687, 0.5368, 1.0000,
	0.3682, 0.5365, 1.0000,
	0.3677, 0.5361, 1.0000,
	0.3673, 0.5357, 1.0000,
	0.3668, 0.5353, 1.0000,
	0.3664, 0.5349, 1.0000,
	0.3659, 0.5346, 1.0000,
	0.3655, 0.5342, 1.0000,
	0.3650, 0.5338, 1.0000,
	0.3646, 0.5335, 1.0000,
	0.3642, 0.5331, 1.0000,
	0.3637, 0.5328, 1.0000,
	0.3633, 0.5324, 1.0000,
	0.3629, 0.5321, 1.0000,
	0.3625, 0.5317, 1.0000,
	0.3621, 0.5314, 1.0000,
	0.3617, 0.5310, 1.0000,
	0.3613, 0.5307, 1.0000,
	0.3609, 0.5304, 1.0000,
	0.3605, 0.5300, 1.0000,
	0.3601, 0.5297, 1.0000,
	0.3597, 0.5294, 1.0000,
	0.3593, 0.5291, 1.0000,
	0.3589, 0.5288, 1.0000,
	0.3586, 0.5284, 1.0000,
	0.3582, 0.5281, 1.0000,
	0.3578, 0.5278, 1.0000,
	0.3575, 0.5275, 1.0000,
	0.3571, 0.5272, 1.0000,
	0.3567, 0.5269, 1.0000,
	0.3564, 0.5266, 1.0000,
	0.3560, 0.5263, 1.0000,
	0.3557, 0.5260, 1.0000,
	0.3553, 0.5257, 1.0000,
	0.3550, 0.5255, 1.0000,
	0.3546, 0.5252, 1.0000,
	0.3543, 0.5249, 1.0000,
	0.3540, 0.5246, 1.0000,
	0.3536, 0.5243, 1.0000,
	0.3533, 0.5241, 1.0000,
	0.3530, 0.5238, 1.0000,
	0.3527, 0.5235, 1.0000,
	0.3524, 0.5232, 1.0000,
	0.3520, 0.5230, 1.0000,
	0.3517, 0.5227, 1.0000,
	0.3514, 0.5225, 1.0000,
	0.3511, 0.5222, 1.0000,
	0.3508, 0.5219, 1.0000,
	0.3505, 0.5217, 1.0000,
	0.3502, 0.5214, 1.0000,
	0.3499, 0.5212, 1.0000,
	0.3496, 0.5209, 1.0000,
	0.3493, 0.5207, 1.0000,
	0.3490, 0.5204, 1.0000,
	0.3487, 0.5202, 1.0000,
	0.3485, 0.5200, 1.0000,
	0.3482, 0.5197, 1.0000,
	0.3479, 0.5195, 1.0000,
	0.3476, 0.5192, 1.0000,
	0.3473, 0.5190, 1.0000,
	0.3471, 0.5188, 1.0000,
	0.3468, 0.5186, 1.0000,
	0.3465, 0.5183, 1.0000,
	0.3463, 0.5181, 1.0000,
	0.3460, 0.5179, 1.0000,
	0.3457, 0.5177, 1.0000,
	0.3455, 0.5174, 1.0000,
	0.3452, 0.5172, 1.0000,
	0.3450, 0.5170, 1.0000,
	0.3447, 0.5168, 1.0000,
	0.3444, 0.5166, 1.0000,
	0.3442, 0.5164, 1.0000,
	0.3439, 0.5161, 1.0000,
	0.3437, 0.5159, 1.0000,
	0.3435, 0.5157, 1.0000,
	0.3432, 0.5155, 1.0000,
	0.3430, 0.5153, 1.0000,
	0.3427, 0.5151, 1.0000,
	0.3425, 0.5149, 1.0000,
	0.3423, 0.5147, 1.0000,
	0.3420, 0.5145, 1.0000,
	0.3418, 0.5143, 1.0000,
	0.3416, 0.5141, 1.0000,
	0.3413, 0.5139, 1.0000,
	0.3411, 0.5137, 1.0000,
	0.3409, 0.5135, 1.0000,
	0.3407, 0.5133, 1.0000,
	0.3404, 0.5132, 1.0000,
	0.3402, 0.5130, 1.0000,
	0.3400, 0.5128, 1.0000,
	0.3398, 0.5126, 1.0000,
	0.3396, 0.5124, 1.0000,
	0.3393, 0.5122, 1.0000,
	0.3391, 0.5120, 1.0000,
	0.3389, 0.5119, 1.0000,
	0.3387, 0.5117, 1.0000,
	0.3385, 0.5115, 1.0000,
	0.3383, 0.5113, 1.0000,
	0.3381, 0.5112, 1.0000,
	0.3379, 0.5110, 1.0000,
	0.3377, 0.5108, 1.0000,
	0.3375, 0.5106, 1.0000,
	0.3373, 0.5105, 1.0000,
	0.3371, 0.5103, 1.0000,
	0.3369, 0.5101, 1.0000,
	0.3367, 0.5100, 1.0000,
	0.3365, 0.5098, 1.0000,
	0.3363, 0.5096, 1.0000,
	0.3361, 0.5095, 1.0000,
	0.3359, 0.5093, 1.0000,
	0.3357, 0.5091, 1.0000,
	0.3356, 0.5090, 1.0000,
	0.3354, 0.5088, 1.0000,
	0.3352, 0.5087, 1.0000,
	0.3350, 0.5085, 1.0000,
	0.3348, 0.5084, 1.0000,
	0.3346, 0.5082, 1.0000,
	0.3345, 0.5080, 1.0000,
	0.3343, 0.5079, 1.0000,
	0.3341, 0.5077, 1.0000,
	0.3339, 0.5076, 1.0000,
	0.3338, 0.5074, 1.0000,
	0.3336, 0.5073, 1.0000,
	0.3334, 0.5071, 1.0000,
	0.3332, 0.5070, 1.0000,
	0.3331, 0.5068, 1.0000,
	0.3329, 0.5067, 1.0000,
	0.3327, 0.5066, 1.0000,
	0.3326, 0.5064, 1.0000,
	0.3324, 0.5063, 1.0000,
	0.3322, 0.5061, 1.0000,
	0.3321, 0.5060, 1.0000,
	0.3319, 0.5058, 1.0000,
	0.3317, 0.5057, 1.0000,
	0.3316, 0.5056, 1.0000,
	0.3314, 0.5054, 1.0000,
	0.3313, 0.5053, 1.0000,
	0.3311, 0.5052, 1.0000,
	0.3309, 0.5050, 1.0000,
	0.3308, 0.5049, 1.0000,
	0.3306, 0.5048, 1.0000,
	0.3305, 0.5046, 1.0000,
	0.3303, 0.5045, 1.0000,
	0.3302, 0.5044, 1.0000,
	0.3300, 0.5042, 1.0000,
	0.3299, 0.5041, 1.0000,
	0.3297, 0.5040, 1.0000,
	0.3296, 0.5038, 1.0000,
	0.3294, 0.5037, 1.0000,
	0.3293, 0.5036, 1.0000,
	0.3291, 0.5035, 1.0000,
	0.3290, 0.5033, 1.0000,
	0.3288, 0.5032, 1.0000,
	0.3287, 0.5031, 1.0000,
	0.3286, 0.5030, 1.0000,
	0.3284, 0.5028, 1.0000,
	0.3283, 0.5027, 1.0000,
	0.3281, 0.5026, 1.0000,
	0.3280, 0.5025, 1.0000,
	0.3279, 0.5024, 1.0000,
	0.3277, 0.5022, 1.0000
	)
	    numElem 	= ((a_kelvinTemp - 1000) / 100 * 3 + 1) as integer 
		
		colorRgb  	= color 255 255 255
		colorRgb.r	= kelvinTempTable[numElem] * 255.0
		colorRgb.g	= kelvinTempTable[numElem + 1] * 255.0
		colorRgb.b	= kelvinTempTable[numElem + 2] * 255.0
		
		colorRgb --return colorRgb
)


fn TargetedAndType a_delegate a_targeted a_type =
(
	if a_targeted == true then
	(
		case a_type of
		(			
			1: a_delegate.type = "Target_Point"
			2: a_delegate.type = "Target_Rectangle"
			3: a_delegate.type = "Target_Disc"
			4: a_delegate.type = "Target_Sphere"
			5: a_delegate.type = "Target_Cylinder"
		)
	)
	else
	(
		case a_type of
		(			
			1: a_delegate.type = "Free_Point"
			2: a_delegate.type = "Free_Rectangle"
			3: a_delegate.type = "Free_Disc"
			4: a_delegate.type = "Free_Sphere"
			5: a_delegate.type = "Free_Cylinder"
		)
	)
)


fn ConvertToMeters a_val = 
(
	local scaleFactor = 1.0
	case units.systemType of
	(
		#Inches: scaleFactor 			= 0.0254
		#Feet: scaleFactor 			= 0.3048
		#Millimeters: scaleFactor 	= 0.001
		#Centimeters: scaleFactor 	= 0.01
		#Miles: scaleFactor 			= 1609.34
		#Meters: scaleFactor 			= 1.0
		#Kilometers: scaleFactor 	= 1000.0
		default: scaleFactor 			= 0.0254
	)
	
	a_val * scaleFactor
)


fn GetArea a_type a_delegate = 
(		
	local areaLight = 1.0
		
	lenght 	= ConvertToMeters a_delegate.light_length
	width	= ConvertToMeters a_delegate.light_Width
	radius 	= ConvertToMeters a_delegate.light_Radius
	
	case a_type of
	(	
		1: areaLight = 1 									-- point
		2: areaLight = lenght * width					-- rectangle
		3: areaLight = PI * radius * radius 			-- disk
		4: areaLight = 4.0 * PI * radius * radius -- sphere
		5: areaLight = 2.0 * PI * radius * lenght	-- cylinder
		6: areaLight = lenght * width					-- portal(rectangle)
	)			
			
	areaLight
)
		
		
fn ConvertLmToCd a_type a_lm = 
(
	local cd = 0		
	  
	if (a_type == 1 or a_type == 4 or a_type == 5) then
		cd = a_lm / (4.0 * PI) -- point, sphere and cylinder
	else
		cd = a_lm / PI
				
	cd --return cd
)


fn ConvertCdToLm a_type a_cd =
(
	local lm = 0
	
	if (a_type == 1 or a_type == 4 or a_type == 5) then
		lm = a_cd * 4.0 * PI -- point, sphere and cylinder
    else
		lm = a_cd * PI

	lm --return Lm
)


fn ConvertIntensImageToCd a_intensityImage a_area = 
(	 
	a_intensityImage * 683.0 * a_area
)


fn ConvertIntensCdToImage a_cd a_area = 
(	
	a_cd / 683.0 / a_area
)


fn intensityGuiToCd a_intensityGUI &a_intensityImage &a_intensityCd &a_intensityLm a_intensityType a_type a_delegate = 
(		
	areaLight = GetArea a_type a_delegate
	
	intensViewport = 1
	
	case a_intensityType of
			(			
				1: 
				(
					a_intensityImage 	= a_intensityGUI --image					
					intensViewport		= ConvertIntensImageToCd a_intensityImage areaLight
				)
				2: 
				(
					a_intensityCd 		= a_intensityGUI --cd						
					intensViewport		= a_intensityCd
				)
				3: 
				(
					a_intensityLm		= a_intensityGUI --lm					
					intensViewport	= ConvertLmToCd a_type a_intensityLm 
				)
			)
	
	
	a_delegate.intensity = intensViewport 
)



-------------------------------------------------------------------------------------------------------------------------------------


plugin light HydraLight2
name:"HydraLight2"
category:"Hydra"
classID:#(0x6601ed90, 0x35f9b299) 
extends:Free_Rectangle replaceUI:true version:1 
(	
  	parameters main rollout:rollParams
	(				
		lightOn				type:#boolean		default:true			 		ui:lightOnUI
		targeted  			type:#boolean		default:false					ui:targetedUI  						
		visInRen			type:#boolean		default:true			 		ui:visInRenUI	
		type					type:#integer		default:2				 		ui:typeShapeUI
		
		intensityGUI		type:#float 			default:1500.0 				ui:intensityUI 
		intensityCd		type:#float 			default:1500.0  
		intensityImage	type:#float 			default:8.785  
		intensityLm       type:#float 			default:4712.389  

		intensityType		type:#integer 		default:2		 				ui:intensityTypeUI
		  
		colorRgb 			type:#color 			default:white 				ui:colorRgbUI
		kelvinOn			type:#boolean		default:false			 		ui:kelvinOnUI	
		kelvinTemp		type:#integer		default:6500					ui:kelvinTempUI
		  
		light_length 		type:#float  			default:50.0 					ui:light_lengthUI
		light_Width 		type:#float 			default:50.0 					ui:light_WidthUI
		light_Radius  	type:#float			default:3.0 					ui:light_RadiusUI  
		
		distribType		type:#integer		default:2
		
		iesOn				type:#boolean		default:false					ui:iesOnUI
		iesFile				type:#filename		assettype:#Photometric	
		iesFilename  		type:#string			default:"Choose photometric file"
-- 		iesRotX				type:#float 			default:0.0	 				ui:iesRotX_UI
-- 		iesRotY				type:#float 			default:0.0	 				ui:iesRotY_UI
-- 		iesRotZ				type:#float 			default:0.0	 				ui:iesRotZ_UI

		spotOn				type:#boolean		default:false					ui:spotOnUI			
		showCone			type:#boolean		default:false					ui:showConeUI			
		hotSpot  			type:#float 			default:40.0	 				ui:hotSpotUI  			
		fallSize 				type:#float 			default:60.0	 				ui:fallSizeUI 		
		
		spectrOn				type:#boolean		default:false					ui:spectrOnUI
		spectrFile				type:#filename		assettype:#Other	
		spectrFilename  		type:#string			default:"Choose spectrum file"

		spotControlEnable 					type:#boolean		default:true
		iesControlEnable  					type:#boolean		default:true	
		lengthControlEnable 				type:#boolean		default:true
		widthControlEnable 				type:#boolean		default:true 
		radiusControlEnable 				type:#boolean		default:false
		transformGroupControlEnable type:#boolean		default:true
	
	

		
		on lightOn 	set val do delegate.on = val					
		on visInRen	set val do delegate.Area_Light_Sampling_Custom_Attribute.mr_EnableLightShapeRendering	= val	  		
		on intensityGUI	set val do	intensityGuiToCd val &intensityImage &intensityCd &intensityLm intensityType type delegate
		
		----------------------
		--Color	
		----------------------
		
		on colorRgb	set val do delegate.rgbFilter = val
			
		on kelvinOn	set val do 
		(
			if val == true then colorRgb = KelvinToRgb kelvinTemp
		)
		on kelvinTemp set val do
		(
			if kelvinOn == true then colorRgb = KelvinToRgb kelvinTemp 
		)


		----------------------
		--Targeted
		----------------------

		on targeted	set val do TargetedAndType delegate targeted type

		
-- 		on iesRotX	set val do delegate.xRotation = val
-- 		on iesRotY		set val do delegate.yRotation = val
-- 		on iesRotZ	set val do delegate.zRotation = val
			
		----------------------
		--Spotlight
		----------------------
		
		on showCone set val do delegate.showCone = val

		on hotSpot set val do 	
		(
			delegate.hotspot = val		
			if	(hotSpot > (fallSize - 1.0)) then fallSize = hotSpot + 1.0
		)

		on fallSize set val do 	
		(
			delegate.falloff = val
			if	(hotSpot > (fallSize - 1.0)) then hotSpot = fallSize - 1.0
		)		
	)-- end parameters	   	

	----------------------------------------------------------
	-- Rollout	
	----------------------------------------------------------
	
	rollout rollParams "Parameters"
	(		
		local groupOffsetX = 0
		local groupWidth 	= 173
		local fieldWidth 	= 60
		
		checkbox  		lightOnUI 			"On"  					 checked: true align:#left across:3 
		  
		label 				lbl1 					"Targeted:" 			 offset:[32,0]
		checkbox  		targetedUI			""  						 offset:[10,0] checked: false	align:#right fieldwidth:1

		label 				lbl2 					"Visible in render:"	 offset:[55,0] across:2 align:#right 
		checkbox  		visInRenUI		""  						 offset:[10,0] checked: true align:#right fieldwidth:1

		-- Type		
		label 				lbl3 					"Type:" 				across:2 align:#right 
		dropdownList 	typeShapeUI				"" 					width:70 align:#right items:#("Point", "Rectangle", "Disk", "Sphere", "Cylinder", "Sky portal")  selection:2 
		
		
		group "Intensity"		
		(
			spinner			intensityUI		""							across:2 align:#left width:fieldWidth range:[0.001, 100000, 1]  
			dropdownList 	intensityTypeUI "" 							width:70 align:#right items:#("Image", "Cd", "Lm") selection:2  
		)
		
			
		group "Color"		
		(		
			colorpicker 		colorRgbUI		"RGB: " 				 color:white width:100 align:#right
			checkbox  		kelvinOnUI		""  						 offset:[5,0] across:2 checked: false  align:#left 
			spinner			kelvinTempUI	"In Kelvin:"			 fieldwidth:fieldWidth align:#right range:[1000, 20000, 6500] scale:100	
		)
		
		
		group "Size"		
		(		
			spinner 			light_lengthUI	"Length: "				fieldwidth:fieldWidth align:#right range:[0.001, 100000, 50.0] type:#worldunits enabled:lengthControlEnable 
			spinner 			light_WidthUI 	"Width: " 				align:#right fieldwidth:fieldWidth range:[0.001, 100000, 50.0] type:#worldunits enabled:widthControlEnable
			spinner 			light_RadiusUI	"Radius: " 			align:#right fieldwidth:fieldWidth range:[0.001, 100000, 3.0] type:#worldunits enabled:radiusControlEnable
		)
		
				
		group "IES"		
		(	
			checkbox  		iesOnUI				"On"						offset:[5,0] checked: false  across:2 align:#left enabled:iesControlEnable
			button 			iesButtonUI		iesFilename 		 	 width:100 align:#right enabled:iesControlEnable
-- 			spinner 			iesRotX_UI		"Rotation X: " 		align:#right fieldwidth:fieldWidth range:[-180.0, 180.0, 0.0] align:#right enabled:iesControlEnable
-- 			spinner 			iesRotY_UI		"Rotation Y: " 		align:#right fieldwidth:fieldWidth range:[-180.0, 180.0, 0.0] enabled:iesControlEnable
-- 			spinner 			iesRotZ_UI		"Rotation Z: " 		align:#right fieldwidth:fieldWidth range:[-180.0, 180.0, 0.0] enabled:iesControlEnable
		)
		
				
		group "Spotlight"		
		(	
			checkbox  		spotOnUI			"On"						offset:[5,0] checked: false  across:3 enabled:spotControlEnable
			label 				showConeLbl		"Show cone:" 		offset:[30,0] align:#right enabled:spotControlEnable
			checkbox  		showConeUI		""					 		offset:[10,0] checked: false  align:#right fieldwidth:1 enabled:spotControlEnable
			spinner 			hotSpotUI			"Hotspot: " 			align:#right fieldwidth:fieldWidth range:[0.0, 179.0, 40]	enabled:spotControlEnable
			spinner 			fallSizeUI			"Falloff: " 				align:#right fieldwidth:fieldWidth range:[0.1, 180.0, 60]	enabled:spotControlEnable
		)
   
		
		group "Spectrum"		
		(
			checkbox  		SpectrOnUI			"On"						offset:[5,0] checked: false  across:2 align:#left 
			button 			SpectrButtonUI		spectrFilename 		width:100 align:#right toolTip:"The plugin does not support spectral rendering yet. It is only for export and internal use with Hydra API."
		)
   
		
		
		fn DisableAllSizeControl val = 
	   (			
			light_lengthUI.enabled 			= not val								
			light_WidthUI.enabled 			= not val	
			light_RadiusUI.enabled 			= not val				
		    lengthControlEnable 				= not val
			widthControlEnable 				= not val
			radiusControlEnable 				= not val
	   )
	   
	   fn DisableAllIesControl val = 
	   (
			iesOn 						= off			
			iesOnUI.enabled 		= not val								
			iesButtonUI.enabled 	= not val	
-- 			iesRotX_UI.enabled 	= not val	
-- 			iesRotY_UI.enabled 	= not val	
-- 			iesRotZ_UI.enabled 	= not val
		    iesControlEnable		= not val
	   )
   
	   fn DisableAllSpotlightControl val = 
		(
			spotOn 						= off	
			spotOnUI.enabled 		= not val			
			showConeLbl.enabled = not val
			showConeUI.enabled 	= not val
			hotSpotUI.enabled 	= not val		
			fallSizeUI.enabled 		= not val					
			spotControlEnable		= not val
		)
		
		
		
		
		fn GetMeanSize a_type = 
		(
			local meanSize = 0
			
			case a_type of
			(	
				1: meanSize = 0 														-- point
				2: meanSize = (light_length + light_Width) * 0.5		-- rectangle
				3: meanSize = light_Radius							 			-- disk
				4: meanSize = light_Radius									 	-- sphere
				5: meanSize = (light_Radius + light_length) * 0.5		-- cylinder
				6: meanSize = (light_length + light_Width) * 0.5		-- portal(rectangle)
			)			
			meanSize
		)
		
		
		fn Lerp a b f =
		(
			a + f * (b - a)
		)

		fn SoftShadowInViewport a_type = 
		(
			delegate.shadowGeneratorByName	= "Shadow Map"	
			
			meanSizeLight = GetMeanSize a_type						
			meanSizeLight            = 1.0 / (meanSizeLight + 1.0) --compress
			delegate.mapsize 		 = Lerp 128 512 meanSizeLight
			delegate.samplerange = Lerp 10 0 meanSizeLight			
		)

		----------------------
		--Type shape
		----------------------
		
		on typeShapeUI selected val do
		(			
			hasUniformOnly = false
			DisableAllSizeControl false			
			
			if targeted == on then
			(
				case val of
				(	
					1: delegate.type = "Target_Point"					   
					2: delegate.type = "Target_Rectangle"					
					3: delegate.type = "Target_Disc"					
					4: delegate.type = "Target_Sphere"							
					5: delegate.type = "Target_Cylinder"							
					6: delegate.type = "Target_Rectangle"						
				)
			)
			else
			(			
				case val of
				(			
					1: delegate.type = "Free_Point"					   
					2: delegate.type = "Free_Rectangle"					
					3: delegate.type = "Free_Disc"					
					4: delegate.type = "Free_Sphere"							
					5: delegate.type = "Free_Cylinder"							
					6: delegate.type = "Free_Rectangle"
				)
			)
			
			case val of
			(	
				1: 
				(
					DisableAllSizeControl true				
					delegate.Distribution 	= 0 --uniform spherical
					distribType 				= 0
				)
				2: 
				(						
					light_RadiusUI.enabled 	= false
					radiusControlEnable	  		= false
				)
				3: 
				(						
					light_lengthUI.enabled 	= false
					light_WidthUI.enabled 	= false 
					lengthControlEnable	  	= false
					widthControlEnable	  		= false
				)
				4:
				(						
					light_lengthUI.enabled 	= false
					light_WidthUI.enabled 	= false 	
					lengthControlEnable	  	= false
					widthControlEnable	  		= false						
					hasUniformOnly 				= true
				)
				5: 
				(						
					light_WidthUI.enabled 	= false		
					widthControlEnable	  		= false							
					hasUniformOnly 				= true
				)
				6: 
				(						
					light_RadiusUI.enabled	= false	
					radiusControlEnable	  		= false						
					hasUniformOnly 				= true
				)
			)
			
			areaLight = GetArea val delegate
			
			case intensityType of
			(
				1: intensityGUI = ConvertIntensCdToImage intensityCd areaLight	
				2: intensityGUI = intensityCd 
				3: intensityGUI = ConvertCdToLm type intensityCd
			)
							
			if val != 1 do
			(
				if hasUniformOnly == true then 
				(
					DisableAllIesControl true
					DisableAllSpotlightControl true
					delegate.Distribution 	= 0 --uniform spherical
					distribType 				= 0
				)
				else 
				(
					DisableAllIesControl false
					DisableAllSpotlightControl false
					delegate.Distribution 	= 2 --uniform diffuse
					distribType 				= 2
				)
			)
			
			SoftShadowInViewport val
		)

		----------------------
		--Intensity
		----------------------
		
		on intensityTypeUI selected val do
		(				
			areaLight = GetArea type delegate
			
			case val of
			(		
				1: 
				(
					delegate.intensityType 	= 1
					intensityGUI 					= ConvertIntensCdToImage intensityCd areaLight										
				)
				2: 
				(
					delegate.intensityType 	= 1 -- cd
					intensityGUI 					= intensityCd					
				)
				3: 
				(
					delegate.intensityType 	= 0 -- lm			
					intensityGUI 					= ConvertCdToLm type intensityCd
				)
			)		
		)

		----------------------
		--Size
		----------------------
		
		on light_lengthUI changed val do
		(
			delegate.light_length = val
			SoftShadowInViewport type
		)
		on light_WidthUI changed val do
		(
			delegate.light_Width 	= val
			SoftShadowInViewport type
		)
		on light_RadiusUI changed val do
		(
			delegate.light_Radius	= val
			SoftShadowInViewport type
		)

		----------------------
		--IES
		----------------------
		
		on iesOnUI changed val do
		(
			if val == true then
			(
				delegate.Distribution 	= 3 
				distribType 				= 3
			)
			else 
			(
				if type == 1 or type == 5 then 
				(
					delegate.Distribution 	= 0
					distribType 				= 0				
				)
				else
				(				
					delegate.Distribution 	= 2
					distribType 				= 2				
				)
			)
			
			DisableAllSpotlightControl val				
		)
		
		
		on iesButtonUI pressed do
		(
			f = getOpenFileName types:"IES(*.ies)|*.ies" 
					
			if f != undefined do		
			(			
				iesFile = f 
				delegate.webfile 		= iesFile 
				iesFilename 				= getFilenameFile iesFile 
				iesButtonUI.caption 	= iesFilename 
				intensityCd				= delegate.intensity			
				intensityType 			= 2
			)
		)				
		
		----------------------
		--Spotlight
		----------------------
		
		on spotOnUI changed val do
		(
			if val==true then
			(
				delegate.Distribution = 1 
				distribType = 1				
			)
			else
			(
				if type == 1 or type == 5 then 
				(
					delegate.Distribution 	= 0
					distribType 				= 0				
				)
				else
				(				
					delegate.Distribution 	= 2
					distribType 				= 2				
				)
			)
						
			DisableAllIesControl val
		)
		
		----------------------
		-- Spectrum
		----------------------
		
		on spectrButtonUI pressed do
		(
			f = getOpenFileName types:"XML (*.xml)|*.xml" 
					
			if f != undefined do		
			(			
				spectrFile = f 				
				spectrFilename 			= getFilenameFile spectrFile 
				spectrButtonUI.caption = spectrFilename 				
			)
		)		
		
	)--end rollout

	
	on create do
	(		
		-- setup initial 
		delegate.light_length 	= 50
		delegate.light_Width 		= 50
		delegate.light_Radius 	= 3		
		delegate.castShadows	= on
		delegate.Distribution 		= 2
		delegate.shadowGeneratorByName	= "Shadow Map"	
	)
)-- end plugin

