#pragma once

#include "resource.h"
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <maxtypes.h>

#include "prim.h"        
#include "target.h"
#include <shadgen.h>
#include "notify.h"     
#include "macrorec.h"    
#include "decomp.h"      
#include "stdmat.h"
//#include <INodeGIProperties.h>
//#include <INodeShadingProperties.h>
#include <IViewportShadingMgr.h>
#include <Graphics/Utilities/MeshEdgeRenderItem.h>
#include <Graphics/Utilities/SplineRenderItem.h>
#include <Graphics/CustomRenderItemHandle.h>
//#include <Graphics/RenderNodeHandle.h>
#include "MouseCursors.h"
#include "3dsmaxport.h" 
#include <lslights.h>

#include <DaylightSimulation/IPhysicalSunSky.h>
#include <DaylightSimulation/ISunPositioner.h>
#include <assert1.h>

////////////////////////////////////////////////////////////////////////////////////


extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;


#define HydraLight_CLASS_ID	Class_ID(0x69729747, 0x8c40aaf7)
#define HydraSunSky_CLASS_ID	Class_ID(0x3e362b95, 0x4bb75481)

constexpr int PB_SUN_REF = 0;
constexpr int PB_SKY_REF = 1;


constexpr int NUM_CIRC_PTS = 28;
constexpr int SEG_INDEX    = 7;

constexpr float COS_45     = 0.7071067F;
constexpr float COS_45_2X  = 1.4142136F;

#define Dx (ray.dir.x)
#define Dy (ray.dir.y)
#define Dz (ray.dir.z)
#define Px (ray.p.x)
#define Py (ray.p.y)
#define Pz (ray.p.z)

using hydraStr  = std::wstring;
using hydraChar = wchar_t;

static int waitPostLoad = 0;

enum { PB_SUN, PB_SKY };

constexpr int numPB = 1 + PB_SKY;


// Sun parameters
enum
{
  pb_sun_on,
  pb_sun_excludeList,
  pb_sun_multiplier,
  pb_sun_softShadow,
  pb_sun_color,
  pb_sun_zenith,
  pb_sun_azimuth,
  pb_sun_distanceInViewport,
  pb_sun_hotspot,
  pb_sun_falloff,
  pb_sun_alightToPortals
};

// Sky parameters
enum
{
  pb_sky_overrEnvir,
  pb_sky_tint,
  pb_sky_multiplier,
  pb_sky_haze
};

// Don`t delete param from enum list.

////////////////////////////////////////////////////////////////////////////////////

class HydraSunSky: public LightObject, public MaxSDK::IPhysicalSunSky
{
public:
  // Class vars
  static IObjParam* ip;

  // Object parameters
  IParamBlock2* pblockSun = nullptr;
  IParamBlock2* pblockSky = nullptr;

  Interval      ivalid;

  ExclList      excludeList;

  bool          enable    = true; // for draw in Display.
  BOOL          showCone  = true;
  float         targDist  = 0.0F;

  Mesh          buildMesh;
  Mesh*         mesh      = nullptr;

  int 	        extDispFlags;


  explicit HydraSunSky(bool loading);
  ~HydraSunSky()            override;

  // Class tree: Animatable->ReferenceMaker->ReferenceTarget->BaseObject->Object->LightObject
  // inherited virtual methods:
  // From Animatable
  void            BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev) override;
  void            EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)   override;
  void            DeleteThis()                                         noexcept override { delete this; }
  int             NumSubs()                                            noexcept override { return 1; }

#ifdef MAX2022
  MSTR            SubAnimName(int i, bool localized = false)                    override;
#else
  TSTR            SubAnimName(int i)                                            override;
#endif // MAX2022

  Animatable*     SubAnim(int i)                                                override;
  int	            NumParamBlocks()                                     noexcept override { return numPB; }	// return number of ParamBlocks in this instance
  IParamBlock2*   GetParamBlock(int i)                                          override; // return i'th ParamBlock
  IParamBlock2*   GetParamBlockByID(BlockID id)                                 override; // return id'd ParamBlock
  IOResult        Load(ILoad *iload)                                            override;
  //IOResult      Save(ISave *isave)                                            override;


  // From ReferenceMaker
  RefResult       NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, 
                                   RefMessage message, BOOL propagate)          override;
  int             NumRefs()                                            noexcept override { return numPB; }   // Maintain the number or references here
  RefTargetHandle GetReference(int i)                                           override;
  void            SetReference(int i, RefTargetHandle rtarg)                    override;


  // From ReferenceTarget
  RefTargetHandle Clone(RemapDir &remap)                                        override;


  // From BaseObject
  int             HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) override;
  CreateMouseCallBack* GetCreateMouseCallBack() override;

  // Disable legasy method Dysplay(), because use new 3ds Max graphics API. 
  //int           Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) override;
  void            GetWorldBoundBox(TimeValue t, INode *inode, ViewExp* vpt, Box3& box) override;
  void            GetLocalBoundBox(TimeValue t, INode *inode, ViewExp* vpt, Box3& box) override;  

#ifdef MAX2022
  const MCHAR* GetObjectName(bool localized)                                     const override { return GetString(IDS_CLASS_NAME); }
#else

  const TCHAR*    GetObjectName()                                                      override { return GetString(IDS_CLASS_NAME); }
#endif // MAX2022

  BOOL            HasViewDependentBoundingBox()                               noexcept override { return false; }
  void            SetExtendedDisplay(int flags)                               noexcept override { extDispFlags = flags; }


  // From Object
  Interval        ObjectValidity(TimeValue t)                                          override;
  void            GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel)      override;
  int 			      DoOwnSelectHilite()                                         noexcept override { return 1; }


  // From LightObject
  SClass_ID   SuperClassID()                                             noexcept override { return LIGHT_CLASS_ID; }
  Class_ID    ClassID()                                                           override { return HydraSunSky_CLASS_ID; }
  int         GetShadowMethod()                                          noexcept override { return LIGHTSHADOW_MAPPED; }
  void        InitNodeName(MSTR& s)                                               override { s = GetString(IDS_CLASS_NAME); }
#ifdef MAX2022
  void        GetClassName(MSTR& s, bool localized = true)                  const override { s = GetString(IDS_CLASS_NAME); }
#else  
  void        GetClassName(TSTR& s)                                               override { s = GetString(IDS_CLASS_NAME); }
#endif // MAX2022


  ObjectState Eval              (TimeValue t)                                              override;
  RefResult   EvalLightState    (TimeValue time, Interval & valid, LightState * ls)        override;
  void        SetUseLight       (int onOff)                                                override;
  BOOL        GetUseLight       (void)                                                     override;
  float       GetIntensity      (TimeValue t, Interval& valid = Interval(0, 0))            override;
  Point3      GetRGBColor       (TimeValue t, Interval& valid = Interval(0, 0))            override;
  float       GetHotspot        (TimeValue t, Interval& valid = Interval(0, 0))            override;
  float       GetFallsize       (TimeValue t, Interval& valid = Interval(0, 0))            override;
  float       GetTDist          (TimeValue t, Interval& valid = Interval(0, 0))            override;
  float       GetAtten          (TimeValue t, int which, Interval& valid = Interval(0, 0)) override;
  void        SetIntensity      (TimeValue t, float f)                                     override;

#ifdef MAX2022
  void        SetRGBColor       (TimeValue t, const Point3& rgb)                           override;
#else
  void        SetRGBColor       (TimeValue t, Point3& rgb)                                 override;
#endif // MAX2022

  void        SetHotspot        (TimeValue time, float f)                                  override;
  void        SetFallsize       (TimeValue time, float f)                                  override;
  void        SetAtten          (TimeValue time, int which, float f)                       override;
  void        SetTDist          (TimeValue time, float f)                                  override;
  void        UpdateTargDistance(TimeValue 	t, INode* inode)                               override;
  void        SetConeDisplay    (int s, int notify = TRUE)                                 override;
  BOOL        GetConeDisplay    (void)                                                     override;



  // From IObjectDisplay2 
  unsigned long GetObjectDisplayRequirement() const override;
  bool          PrepareDisplay    (const MaxSDK::Graphics::UpdateDisplayContext& prepareDisplayContext)     override;
  bool          UpdatePerNodeItems(const MaxSDK::Graphics::UpdateDisplayContext& updateDisplayContext,
                                         MaxSDK::Graphics::UpdateNodeContext&    nodeContext,
                                         MaxSDK::Graphics::IRenderItemContainer& targetRenderItemContainer) override;



  // From IPhysicalSunSky
  ShadingParameters        EvaluateShadingParameters(const TimeValue t, Interval& validity) const override;
  std::unique_ptr<IShader> InstantiateShader        (const TimeValue t, Interval& validity) const override;
  class Shader;
  class ShaderGenerator;


  //// From Texmap
  //virtual AColor    EvalColor(ShadeContext& sc)         override;
  //virtual Point3    EvalNormalPerturb(ShadeContext& sc) override;
  //virtual int       GetUVWSource()                      override { return UVWSRC_EXPLICIT; }  // Bogus - doesn't matter
  //virtual int       MapSlotType(int i)                  override { return MAPSLOT_ENVIRON; }
  //virtual int       IsHighDynamicRange()          const override { return true; }

  //// From MtlBase
  //virtual void      Update(TimeValue t, Interval& valid)            override;
  //virtual void      Reset()                                         override;
  //virtual Interval  Validity(TimeValue t)                           override;
  //virtual ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) override;

  // Accesses the sun positioner object being referenced by this map
  INode*                  get_sun_positioner_node()                    const;
  MaxSDK::ISunPositioner* get_sun_positioner_object(const TimeValue t) const;
  void                    set_sun_positioner_object(INode* node);


  // My methods
  void  Update(TimeValue t, Interval& valid);
  void  Reset();

  bool  isTargeted() const noexcept { return true; }
  void  GetMat               (TimeValue t, INode* inode, ViewExp &vpt, Matrix3& mat);
  float GetZenith            (TimeValue t, Interval& valid) const;
  float GetAzimuth           (TimeValue t, Interval& valid) const;
  float GetSoftShadow        (TimeValue t, Interval& valid) const;
  float GetHaze              (TimeValue t, Interval& valid) const;
  float GetDistanceInViewport()                             const;
  void  SetTarget            ();
  void  SetZenith            (TimeValue t, float zenith);
  void  SetAzimuth           (TimeValue t, float azimuth);
  void  SetSoftShadow        (TimeValue t, float softShadow);
  void  SetDistanceInViewport(TimeValue t, const float dist);

  void  BuildSunMesh         (TimeValue t, Interval& valid);
  void  GetConePoints        (TimeValue t, float angle, float dist, Point3* q);

  void  InvalidateUI();

private:
  // Static instance of this core interface
  static ShaderGenerator   m_shader_generator;

  // The shader instance that was created by the call to Update()
  std::unique_ptr<IShader> m_shader;
  Interval                 m_shader_valdity;
};



//==================================================================================================
// class PhysicalSunSkyEnv::Shader
//
// Implementation of the environment shader object.
//
class HydraSunSky::Shader: public IPhysicalSunSky::IShader
{
public:

  explicit Shader(const ShadingParameters& shading_parameters);
  ~Shader()                                     override;

  // -- inherited from IPhysicalSunSky::IShader
  Color Evaluate(const Point3& direction) const override;

private:
  const ShadingParameters m_shading_parameters;
};

//==================================================================================================
// class PhysicalSunSkyEnv::ShaderGenerator
//
class HydraSunSky::ShaderGenerator: public IPhysicalSunSky::IShaderGenerator
{
public:

  // Interface constructor declarator
  DECLARE_DESCRIPTOR_INIT(ShaderGenerator);

  // -- inherited from IShaderGenerator
  ShadingParameters CreateShadingParameters(
    const float global_intensity,
    const float haze,
    const bool sun_enabled,
    const float sun_disc_intensity,
    const float sun_glow_intensity,
    const float sun_disc_scale,
    const float sky_intensity,
    const Color night_color,
    const float night_intensity,
    const float horizon_height_deg,
    const float horizon_blur_deg,
    const Color ground_color,
    const float saturation,
    const Color tint,
    const Point3 sun_direction,
    const bool use_custom_perez_coefficients,
    const float custom_perez_A,
    const float custom_perez_B,
    const float custom_perez_C,
    const float custom_perez_D,
    const float custom_perez_E,
    const float custom_perez_diffuse_horizontal_illuminance,
    const float custom_perez_direct_normal_illuminance,
    const TimeValue t,
    Interval& validity) const override;

  std::unique_ptr<IShader> InstantiateShader(const ShadingParameters& shading_parameters) const override;
};




class HydraSunSkyClassDesc: public ClassDesc2
{
public:
  int           IsPublic()                   noexcept override { return TRUE; }
  void*         Create(BOOL loading = FALSE)          override { return new HydraSunSky(loading); }
  const TCHAR* 	ClassName()                           override { return GetString(IDS_CLASS_NAME); }
  SClass_ID     SuperClassID()               noexcept override { return LIGHT_CLASS_ID; }
  Class_ID      ClassID()                             override { return HydraSunSky_CLASS_ID; }
  const TCHAR*  Category()                            override { return GetString(IDS_CATEGORY); }
  const TCHAR*  InternalName()               noexcept override { return _T("HydraSky"); }	 // returns fixed parsable name (scripter-visible name)
  HINSTANCE     HInstance()                  noexcept override { return hInstance; }			 // returns owning module handle

#ifdef MAX2022
  const MCHAR* NonLocalizedClassName()                override { return GetString(IDS_CLASS_NAME); }
#endif // MAX2022

};



class HydraSunSkyDlgProc: public ParamMap2UserDlgProc
{
public:
  HydraSunSky* hSky = nullptr;

  explicit HydraSunSkyDlgProc(HydraSunSky* cb);

  INT_PTR DlgProc(TimeValue t, IParamMap2 *map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
  void DeleteThis()                                                                       noexcept override { delete this; }
};



//Class for interactive creation of the object using the mouse
class HydraSunSkyCreateCallBack: public CreateMouseCallBack
{
  HydraSunSky* ob;
  Point3       p0;                // First point in world coordinates
  Point3       p1;                // Second point in world coordinates
  Point3       center;
  // added for exersice 6 to keep track of previous point.
  IPoint2      m_mousecallback_pt2_prev;

public:
  int  proc(ViewExp *vpt, int msg, int point, int flags, IPoint2 m, Matrix3& mat) override;
  void SetObj(HydraSunSky *obj) noexcept { ob = obj; }
};




template <void (HydraSunSky::*set)(int), bool notify>
class SimpleLightUndo: public RestoreObj
{
public:
  void Restore(int undo);
  void Redo();
  int  Size();
  TSTR Description();

  static void Hold(HydraSunSky* light, int newVal, int oldVal)
  {
    if (theHold.Holding())
      theHold.Put(new SimpleLightUndo<set, notify>(light, newVal, oldVal));
  }

private:
  SimpleLightUndo(HydraSunSky* light, int newVal, int oldVal)
    : _light(light), _redo(newVal), _undo(oldVal)
  {}

  HydraSunSky*	_light;
  int					_redo;
  int					_undo;
};



class LightPostLoad: public PostLoadCallback
{
public:
  HydraSunSky *sp;
  Interval valid;
  LightPostLoad(HydraSunSky *l) { sp = l; }

  void proc(ILoad *iload)
  {
    //gl->FixOldVersions(iload);
    waitPostLoad--;
    delete this;
  }
};





class LightConeItem: public MaxSDK::Graphics::Utilities::SplineRenderItem
{
  HydraSunSky* mpLight;

public:
  LightConeItem(HydraSunSky* lt)
    : mpLight(lt)
  {}

  ~LightConeItem() { mpLight = nullptr; }

  void Realize(MaxSDK::Graphics::DrawContext& drawContext) override;

  void BuildCone(TimeValue t, float dist);
  void BuildConeAndLine(TimeValue t, INode* inode);
};


class LightTargetLineItem: public MaxSDK::Graphics::Utilities::SplineRenderItem
{
  HydraSunSky* mpLight = nullptr;
  float mLastDist  = -1.0F;
  Color mLastColor = { 0.0F,0.0F,0.0F };

public:
  explicit LightTargetLineItem(HydraSunSky* lt) : mpLight(lt) {}

  ~LightTargetLineItem() { mpLight = nullptr; }

  void Realize(MaxSDK::Graphics::DrawContext& drawContext) override;
  void OnHit(MaxSDK::Graphics::HitTestContext& /*hittestContext*/, MaxSDK::Graphics::DrawContext& drawContext) override;
};


// target spot light creation stuff...
class TSpotCreationManager: public MouseCallBack, public ReferenceMaker
{
private:
  CreateMouseCallBack* createCB              = nullptr;
  INode*               lgtNode               = nullptr;
  INode*               targNode              = nullptr;
  HydraSunSky*         lgtObject             = nullptr;
  TargetObject*        targObject            = nullptr;
  IObjCreate*          createInterface       = nullptr;
  ClassDesc*           cDesc                 = nullptr;

  Matrix3 mat;  // the nodes TM relative to the CP
  IPoint2 pt0;

  bool                 attachedToNode        = false;
  bool                 ignoreSelectionChange = false;
  int                  lastPutCount          = 0;

  void CreateNewObject();

#ifdef MAX2022
  void GetClassName(MSTR& s, bool localized = true) const override { s = _M("TSpotCreationManager"); }
#else  
  void GetClassName(MSTR& s) { s = _M("TSpotCreationManager"); }
#endif // MAX2022

  int             NumRefs()                                  noexcept override { return 1; }
  RefTargetHandle GetReference(int i)                        noexcept override { return (RefTargetHandle)lgtNode; }
  void            SetReference(int i, RefTargetHandle rtarg) noexcept override { lgtNode = dynamic_cast<INode*>(rtarg); }

  // StdNotifyRefChanged calls this, which can change the partID to new value 
  // If it doesnt depend on the particular message& partID, it should return
  // REF_DONTCARE
  RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget,
                             PartID& partID, RefMessage message, BOOL propagate) override;

public:
  void Begin(IObjCreate *ioc, ClassDesc *desc);
  void End();
    
  int proc(HWND hwnd, int msg, int point, int flag, IPoint2 m)         override;
  BOOL SupportAutoGrid()                                      noexcept override { return TRUE; }
};


#define CID_TSPOTCREATE	(CID_USER + 3)

class TSpotCreateMode : public CommandMode
{
  TSpotCreationManager proc;

public:
  void Begin(IObjCreate* ioc, ClassDesc* desc) { proc.Begin(ioc, desc); }
  void End() { proc.End(); }

  int Class()                              noexcept override { return CREATE_COMMAND; }
  int ID()                                 noexcept override { return CID_TSPOTCREATE; }
  MouseCallBack* MouseProc(int* numPoints) noexcept override { *numPoints = 1000000; return &proc; }
  ChangeForegroundCallback* ChangeFGProc() noexcept override { return CHANGE_FG_SELECTED; }
  BOOL ChangeFG(CommandMode* oldMode)               override { return (oldMode->ChangeFGProc() != CHANGE_FG_SELECTED); }
  void EnterMode()                         noexcept override {}
  void ExitMode()                          noexcept override {}
  BOOL IsSticky()                             const noexcept { return FALSE; }
};



class HydraSunSkyPBAccessor: public PBAccessor
{
public:
  void Set(PB2Value& v, ReferenceMaker* owner, ParamID id, int tabIndex, TimeValue t) override;
};


static HydraSunSkyClassDesc  hydraSunSkyDesc;
static TSpotCreateMode       theTSpotCreateMode;
static HydraSunSkyPBAccessor hydraSunSkyPBAccessor;
