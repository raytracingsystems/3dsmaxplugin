//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by HydraSunSky.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SUN_PARAMS                  4
#define IDS_DB_HYDRA_LIGHT              5
#define IDS_HYDRA_LIGHT                 5
#define IDS_SOFT_SHADOW                 5
#define IDS_SUN_SOFT_SHADOW             5
#define IDS_DB_HYDRA_SKY_PORTAL         6
#define IDS_ON                          6
#define IDS_SUN_ON                      6
#define IDS_DB_HYDRASKYPORTAL           7
#define IDS_TARGETED                    7
#define IDS_VISIBLE_IN_RENDER           8
#define IDS_SUN_ZENITH                  8
#define IDS_TYPE                        9
#define IDS_SUN_AZIMUTH                 9
#define IDS_EXCLUDE_LIST                10
#define IDS_COLOR                       11
#define IDS_SUN_COLOR                   11
#define IDS_SUN_TINT                    11
#define IDS_INTENSITY                   12
#define IDS_SUN_MULTIPLIER              12
#define IDS_LENGTH                      13
#define IDS_SUN_ALIGN_TO_PORTAL         13
#define IDS_SUN_ALIGN_TO_PORTALS        13
#define IDS_WIDTH                       14
#define IDS_SKY_ON                      14
#define IDS_RADIUS                      15
#define IDS_IES_FILE                    16
#define IDS_WEB_FILE                    16
#define IDS_SKY_TINT                    16
#define IDS_IES_ROT_X                   17
#define IDS_WEB_ROT_X                   17
#define IDS_SKY_MULTIPLIER              17
#define IDS_IES_ROT_Y                   18
#define IDS_WEB_ROT_Y                   18
#define IDS_SKY_HAZE                    18
#define IDS_IES_ROT_Z                   19
#define IDS_WEB_ROT_Z                   19
#define IDS_SKY_PARAMS                  19
#define IDS_SPOTLIGHT_ON                20
#define IDS_SUN_IRR_AREA_ON             20
#define IDS_HOTSPOT                     21
#define IDS_SUN_DISTANCE_IN_VIEWPORT    21
#define IDS_FALLOFF                     22
#define IDS_SUN_IRR_RADIUS              22
#define IDS_SUN_HOTSPOT                 22
#define IDS_TYPE1                       23
#define IDS_SUN_IRR_RADIUS_OUT          23
#define IDS_SUN_FALLOFF                 23
#define IDS_TYPE2                       24
#define IDS_TYPE3                       25
#define IDS_TYPE4                       26
#define IDS_TYPE5                       27
#define IDS_TYPE6                       28
#define IDS_TYPE7                       29
#define IDS_INTENSITY_UNITS             30
#define IDS_INTENSITY_UNIT1             31
#define IDS_INTENSITY_UNIT2             32
#define IDS_INTENSITY_UNIT3             33
#define IDS_KELVIN                      34
#define IDS_SUN_KELVIN                  34
#define IDS_KELVIN_ON                   35
#define IDS_SUN_KELVIN_ON               35
#define IDS_SIMPLE_SHAPE_ON             36
#define IDS_SKY                         37
#define IDS_SHOW_CONE_ON                38
#define IDS_SUN_SHOW_IRR_AREA_ON        38
#define IDS_DB_DOT_TARGET               39
#define IDS_DS_CREATE                   40
#define IDS_DB_TARGET                   41
#define IDS_RB_PRIMITIVES               42
#define IDS_DB_TARGET_CLASS             43
#define IDS_IES_FILE_FILTER             44
#define IDS_WEB_FILE_FILTER             44
#define IDS_WEB_ON                      45
#define IDD_SUN                         103
#define IDD_SKY                         104
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_ON                          1001
#define IDC_SUN_ON                      1001
#define IDC_VISIBLE                     1002
#define IDC_VISIBLE_IN_RENDER           1002
#define IDC_SPOTLIGHT_ON                1003
#define IDC_IRR_AREA_ON                 1003
#define IDC_SUN_IRR_AREA_ON             1003
#define IDC_TARGETED_ON                 1004
#define IDC_KELVIN_ON                   1005
#define IDC_SUN_KELVIN_ON               1005
#define IDC_WEB_ON                      1006
#define IDC_TYPE_LIGHT                  1007
#define IDC_EXCLUDE_LIST                1008
#define IDC_IES_FILE                    1009
#define IDC_WEB_FILE                    1009
#define IDC_INTENSITY_UNITS             1010
#define IDC_SHOW_CONE_ON                1010
#define IDC_SHOWCONE_ON                 1011
#define IDC_SHOW_AREA_ON                1011
#define IDC_SUN_SHOW_IRR_AREA_ON        1011
#define IDC_LENGHT_STATIC               1012
#define IDC_WIDTH_STATIC                1013
#define IDC_RADIUS_STATIC               1014
#define IDC_SHOWCONE_STATIC             1015
#define IDC_WIDTH_STATIC2               1015
#define IDC_HOTSPOT_STATIC              1016
#define IDC_LENGHT_STATIC2              1016
#define IDC_FALLOFF_STATIC              1017
#define IDC_WEB_ROT_X_STATIC            1018
#define IDC_FALLOFF_STATIC2             1018
#define IDC_WEB_ROT_Y_STATIC            1019
#define IDC_WEB_ROT_Z_STATIC            1020
#define IDC_KELVIN_STATIC               1021
#define IDC_IES_FRAME_STATIC            1022
#define IDC_WEB_FRAME_STATIC            1022
#define IDC_SPOTLIGHT_FRAME_STATIC      1023
#define IDC_TARGETED_STATIC             1024
#define IDC_SUN_ALIGN_TO_PORTALS        1026
#define IDC_SUN_ZENITH_EDIT             1027
#define IDC_SUN_ZENITH_SPIN             1028
#define IDC_SKY_OVERR_ENVIR             1029
#define IDC_SUN_COLOR                   1456
#define IDC_SKY_TINT                    1457
#define IDC_EDIT                        1490
#define IDC_INTENSITY_EDIT              1490
#define IDC_SUN_MULT_EDIT               1490
#define IDC_LENGTH_EDIT                 1491
#define IDC_SKY_MULT_EDIT               1491
#define IDC_WIDTH_EDIT                  1492
#define IDC_AZIMUTH_EDIT                1492
#define IDC_SUN_AZIMUTH_EDIT            1492
#define IDC_RADIUS_EDIT                 1493
#define IDC_SOFT_SHADOW_EDIT            1493
#define IDC_SUN_SOFT_SHADOW_EDIT        1493
#define IDC_IES_ROT_X_EDIT              1494
#define IDC_WEB_ROT_X_EDIT              1494
#define IDC_HAZE_EDIT                   1494
#define IDC_SKY_HAZE_EDIT               1494
#define IDC_SUN_DISTANCE_IN_VIEWPORT_EDIT 1494
#define IDC_IES_ROT_Y_EDIT              1495
#define IDC_WEB_ROT_Y_EDIT              1495
#define IDC_SPIN                        1496
#define IDC_INTENSITY_SPIN              1496
#define IDC_SUN_MULT_SPIN               1496
#define IDC_LENGTH_SPIN                 1497
#define IDC_SKY_MULT_SPIN               1497
#define IDC_WIDTH_SPIN                  1498
#define IDC_AZIMUTH_SPIN                1498
#define IDC_SUN_AZIMUTH_SPIN            1498
#define IDC_RADIUS_SPIN                 1499
#define IDC_SOFT_SHADOW_SPIN            1499
#define IDC_SUN_SOFT_SHADOW_SPIN        1499
#define IDC_IES_ROT_X_SPIN              1500
#define IDC_WEB_ROT_X_SPIN              1500
#define IDC_HAZE_SPIN                   1500
#define IDC_SKY_HAZE_SPIN               1500
#define IDC_SUN_DISTANCE_IN_VIEWPORT_SPIN 1500
#define IDC_IES_ROT_Y_SPIN              1501
#define IDC_WEB_ROT_Y_SPIN              1501
#define IDC_IES_ROT_Z_EDIT              1502
#define IDC_WEB_ROT_Z_EDIT              1502
#define IDC_IES_ROT_Z_SPIN              1503
#define IDC_WEB_ROT_Z_SPIN              1503
#define IDC_HOTSPOT_EDIT                1504
#define IDC_HOTSPOT_SPIN                1505
#define IDC_FALLOFF_EDIT                1506
#define IDC_IRR_RADIUS_EDIT             1506
#define IDC_SUN_IRR_RADIUS_EDIT         1506
#define IDC_SUN_HOTSPOT_EDIT            1506
#define IDC_FALLOFF_SPIN                1507
#define IDC_IRR_RADIUS_SPIN             1507
#define IDC_SUN_IRR_RADIUS_SPIN         1507
#define IDC_SUN_HOTSPOT_SPIN            1507
#define IDC_KELVIN_EDIT                 1508
#define IDC_SUN_KELVIN_EDIT             1508
#define IDC_SUN_FALLOFF_EDIT            1508
#define IDC_KELVIN_SPIN                 1509
#define IDC_SUN_KELVIN_SPIN             1509
#define IDC_SUN_FALLOFF_SPIN            1509
#define IDS_DS_TINT                     10175
#define IDS_DB_INTENSITY                20315
#define IDS_DS_LENGTH                   20316
#define IDS_DS_WIDTH                    20317
#define IDS_DB_COLOR                    20320
#define IDS_RB_PARAMETERS               30028
#define IDS_DB_GENERAL_PARAMS           30064
#define IDS_DS_PARAMCHG                 31316
#define IDS_PARAMCHG                    31316
#define IDS_DS_SETTYPE                  31343
#define IDS_DS_SETSHADTYPE              31376

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
