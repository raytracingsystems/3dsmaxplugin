#include <vector>
#include "HydraSunSky.h"
#include "PhysicalSunSkyShader.h"
#include <DaylightSimulation/IPerezAllWeather.h>

//////////////////////////////////////////////////////////////////////////

ClassDesc2* GetHydraSunSkyDesc() { return &hydraSunSkyDesc; }



static ParamBlockDesc2 sun_param_blk(PB_SUN, _T("Sun parameters"), 0, GetHydraSunSkyDesc(),
                                     P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, PB_SUN_REF, PB_SUN,

                                     //Dlalog rollout
                                     IDD_SUN, IDS_SUN_PARAMS, 0, 0, NULL,

                                     // params
                                     pb_sun_on, _T("sun_on"), TYPE_BOOL, 0, IDS_SUN_ON,
                                     p_default, TRUE,
                                     p_ui, TYPE_SINGLECHEKBOX, IDC_SUN_ON,
                                     p_end,
                                     pb_sun_excludeList, _T("sun_excludeList"), TYPE_CHECKBUTTON, 0, IDS_EXCLUDE_LIST,
                                     p_ui, TYPE_CHECKBUTTON, IDC_EXCLUDE_LIST,
                                     p_tooltip, IDS_EXCLUDE_LIST,
                                     p_end,
                                     pb_sun_multiplier, _T("sun_multiplier"), TYPE_FLOAT, P_ANIMATABLE, IDS_SUN_MULTIPLIER,
                                     p_default, 1.0f,
                                     p_range, 0.0f, 1000.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SUN_MULT_EDIT, IDC_SUN_MULT_SPIN, 0.1f,
                                     p_end,
                                     pb_sun_softShadow, _T("sun_softShadow"), TYPE_FLOAT, P_ANIMATABLE, IDS_SUN_SOFT_SHADOW,
                                     p_default, 1.0f,
                                     p_range, 1.0f, 10.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SUN_SOFT_SHADOW_EDIT, IDC_SUN_SOFT_SHADOW_SPIN, 0.1f,
                                     p_end,
                                     pb_sun_color, _T("sun_color"), TYPE_RGBA, P_ANIMATABLE, IDS_SUN_COLOR,
                                     p_default, Color(1.0, 1.0, 1.0),
                                     p_ui, TYPE_COLORSWATCH, IDC_SUN_COLOR,
                                     p_end,
                                     pb_sun_zenith, _T("sun_zenith"), TYPE_FLOAT, P_ANIMATABLE, IDS_SUN_ZENITH,
                                     p_default, 45.0f,
                                     p_range, 0.0f, 89.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SUN_ZENITH_EDIT, IDC_SUN_ZENITH_SPIN, 0.1f,
                                     p_end,
                                     pb_sun_azimuth, _T("sun_azimuth"), TYPE_FLOAT, P_ANIMATABLE, IDS_SUN_AZIMUTH,
                                     p_default, 0.0f,
                                     p_range, 0.0f, 359.9f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SUN_AZIMUTH_EDIT, IDC_SUN_AZIMUTH_SPIN, 0.5f,
                                     p_end,
                                     pb_sun_distanceInViewport, _T("sun_distanceInViewport"), TYPE_FLOAT, P_ANIMATABLE, IDS_SUN_DISTANCE_IN_VIEWPORT,
                                     p_default, 100.0f,
                                     p_range, 1.0f, 99999999.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SUN_DISTANCE_IN_VIEWPORT_EDIT, IDC_SUN_DISTANCE_IN_VIEWPORT_SPIN, 1.0f,
                                     p_end,
                                     pb_sun_hotspot, _T("sun_hotspot"), TYPE_FLOAT, P_ANIMATABLE, IDS_SUN_HOTSPOT,
                                     p_default, 99.0f,
                                     p_range, 1.0f, 99999999.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_SUN_HOTSPOT_EDIT, IDC_SUN_HOTSPOT_SPIN, 1.0f,
                                     p_accessor, &hydraSunSkyPBAccessor,
                                     p_end,
                                     pb_sun_falloff, _T("sun_falloff"), TYPE_FLOAT, P_ANIMATABLE, IDS_SUN_FALLOFF,
                                     p_default, 100.0f,
                                     p_range, 2.0f, 100000000.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_SUN_FALLOFF_EDIT, IDC_SUN_FALLOFF_SPIN, 1.0f,
                                     p_accessor, &hydraSunSkyPBAccessor,
                                     p_end,
                                     pb_sun_alightToPortals, _T("sun_alightToPortals"), TYPE_BOOL, 0, IDS_SUN_ALIGN_TO_PORTALS,
                                     p_ui, TYPE_SINGLECHECKBOX, IDC_SUN_ALIGN_TO_PORTALS,
                                     p_tooltip, IDS_SUN_ALIGN_TO_PORTALS,
                                     p_end,
                                     p_end
);


static ParamBlockDesc2 sky_param_blk(PB_SKY, _T("Sky parameters"), 0, GetHydraSunSkyDesc(),
                                     P_VERSION + P_AUTO_CONSTRUCT + P_AUTO_UI, PB_SKY_REF, PB_SKY,

                                     //Dlalog rollout
                                     IDD_SKY, IDS_SKY_PARAMS, 0, 0, NULL,

                                     // params
                                     pb_sky_overrEnvir, _T("sky_on"), TYPE_BOOL, 0, IDS_SKY_ON,
                                     p_default, TRUE,
                                     p_ui, TYPE_SINGLECHEKBOX, IDC_SKY_OVERR_ENVIR,
                                     p_end,
                                     pb_sky_tint, _T("sky_tint"), TYPE_RGBA, P_ANIMATABLE, IDS_SKY_TINT,
                                     p_default, Color(1.0, 1.0, 1.0),
                                     p_ui, TYPE_COLORSWATCH, IDC_SKY_TINT,
                                     p_end,
                                     pb_sky_multiplier, _T("sky_multiplier"), TYPE_FLOAT, P_ANIMATABLE, IDS_SKY_MULTIPLIER,
                                     p_default, 1.0f,
                                     p_range, 0.0f, 10.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SKY_MULT_EDIT, IDC_SKY_MULT_SPIN, 0.1f,
                                     p_end,
                                     pb_sky_haze, _T("sky_haze"), TYPE_FLOAT, P_ANIMATABLE, IDS_SKY_HAZE,
                                     p_default, 3.0f,
                                     p_range, 2.0f, 10.0f,
                                     p_ui, TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SKY_HAZE_EDIT, IDC_SKY_HAZE_SPIN, 0.1f,
                                     p_end,
                                     p_end
);


//////////////////////////////////////////////////////////////////////////

IObjParam* HydraSunSky::ip = nullptr;

HydraSunSky::ShaderGenerator HydraSunSky::m_shader_generator(
  /*IShaderGenerator::GetInterfaceID()*/Interface_ID(0x8d249b3, 0x6233b0a), _T("HydraSunSky_ShaderGenerator"), 0, nullptr, FP_CORE,
  // No published properties
  p_end);


//////////////////////////////////////////////////////////////////////////
// Param block version system for compatiple versions. Called in Load().
// The current version
const int skyNumParam = 11;
static ParamBlockDescID descSkyV0[skyNumParam] ={
  { TYPE_BOOL,  NULL, TRUE,  pb_sun_on                 },
  { TYPE_USER,  NULL, FALSE, pb_sun_excludeList        },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_multiplier         },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_softShadow         },
  { TYPE_RGBA,  NULL, TRUE,  pb_sun_color              },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_zenith             },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_azimuth            },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_distanceInViewport },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_hotspot            },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_falloff            },
  { TYPE_FLOAT, NULL, TRUE,  pb_sun_alightToPortals    }
};
const int sunNumParam = 4;

static ParamBlockDescID descSunV0[sunNumParam] ={
  { TYPE_FLOAT, NULL, TRUE, pb_sky_overrEnvir },
  { TYPE_FLOAT, NULL, TRUE, pb_sky_tint       },
  { TYPE_FLOAT, NULL, TRUE, pb_sky_multiplier },
  { TYPE_FLOAT, NULL, TRUE, pb_sky_haze       }
};


// Array of all versions
#define VERSION_CURRENT_SUN 0
#define VERSION_CURRENT_SKY 0

#define NUM_ALLVERSIONS_SUN VERSION_CURRENT_SUN + 1
#define NUM_ALLVERSIONS_SKY VERSION_CURRENT_SKY + 1

static ParamVersionDesc versionsSunPB[NUM_ALLVERSIONS_SUN] ={
  ParamVersionDesc(descSunV0, skyNumParam, 0)//,
  //ParamVersionDesc(descSunV1, ?, 1)
};
static ParamVersionDesc versionsSkyPB[NUM_ALLVERSIONS_SKY] ={
  ParamVersionDesc(descSkyV0, sunNumParam, 0)//,
  //ParamVersionDesc(descSkyV1, ?, 1)
};


// For update version:
// 1) Add new parameters only to the end of the enum list.
// 2) Do not remove param from the list of enum, just add.
// 2) Add new or delete parameters in ParamBlockDesc2.
// 3) Create new ParamBlockDescID descVxx[] with new params.
// 4) Add new ParamVersionDesc in ParamVersionDesc versions[]
// 7) VERSION_CURRENT += 1

//////////////////////////////////////////////////////////////////////////


template <void (HydraSunSky::*set)(int), bool notify>
void SimpleLightUndo<set, notify>::Restore(int undo)
{
  (_light->*set)(_undo);
  if (notify)
    _light->NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}



template <void (HydraSunSky::*set)(int), bool notify>
void SimpleLightUndo<set, notify>::Redo()
{
  (_light->*set)(_redo);
  if (notify)
    _light->NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
}



template <void (HydraSunSky::*set)(int), bool notify>
int SimpleLightUndo<set, notify>::Size()
{
  return sizeof(*this);
}



template <void (HydraSunSky::*set)(int), bool notify>
TSTR SimpleLightUndo<set, notify>::Description()
{
  return _T("SimpleLightUndo");
}


[[nodiscard]] Point3 Lerp(const Point3& inData1, const Point3& inData2, const float coeff) // 0 - data1, 1 - data2
{
  return inData1 + (inData2 - inData1) * coeff;
}


Point3 SphereToDecart(const float zenith, const float azimuth, const float radius)
{
  Point3 decart(0.0F, 0.0F, 0.0F);
  decart.x = radius * sin(zenith) * cos(azimuth);
  decart.y = radius * sin(zenith) * sin(azimuth);
  decart.z = radius * cos(zenith);
  return decart;
}


void SetSphereCoordFromDecart(Point3 pos, HydraSunSky* hSky, TimeValue t)
{
  if (pos.x == 0.0F) pos.x = 0.0001F;
  if (pos.y == 0.0F) pos.y = 0.0001F;
  pos.z = fmax(pos.z, 0.01F);

  const float radius  = sqrt(pos.x * pos.x + pos.y * pos.y + pos.z * pos.z);
  const float zenith  = atan(sqrt(pos.x * pos.x + pos.y * pos.y) / pos.z);  
  float       azimuth = atan(pos.y / pos.x);

  if (pos.x < 0.0F)                 azimuth += PI;
  if (pos.x > 0.0F && pos.y < 0.0F) azimuth += TWOPI;

  hSky->SetZenith (t, RadToDeg(zenith));
  hSky->SetAzimuth(t, RadToDeg(azimuth));
  hSky->SetDistanceInViewport(t, radius);
  hSky->SetTDist(t, radius);
}


///////////////////////////////////////////////////////////////////////////

HydraSunSky::HydraSunSky(bool loading)
{
  if (!loading)
    Reset();
}

HydraSunSky::~HydraSunSky()
{
  ReferenceMaker::DeleteAllRefsFromMe();
  pblockSun = nullptr;
  pblockSky = nullptr;

  //if (shadType && !shadType->SupportStdMapInterface()) {
  //  ReplaceReference(AS_SHADTYPE_REF, NULL);
  //  //		delete ASshadType;
  //  //		ASshadType = NULL;
  //}

  //UnRegisterNotification(&NotifyDist, (void *)this, NOTIFY_UNITS_CHANGE);

}



void HydraSunSky::BeginEditParams(IObjParam * ip, ULONG flags, Animatable * prev)
{
  HydraSunSky::ip = ip;
  LightObject::BeginEditParams(ip, flags, prev);
  hydraSunSkyDesc.BeginEditParams(ip, this, flags, prev);

  sun_param_blk.SetUserDlgProc(new HydraSunSkyDlgProc(this));
}



void HydraSunSky::EndEditParams(IObjParam * ip, ULONG flags, Animatable * next)
{
  LightObject::EndEditParams(ip, flags, next);
  hydraSunSkyDesc.EndEditParams(ip, this, flags, next);
}



Animatable * HydraSunSky::SubAnim(int i)
{
  switch (i)
  {
    case PB_SUN:
      return (Animatable *)pblockSun;
      break;
    case PB_SKY:
      return (Animatable *)pblockSky;
      break;
    default:
      return nullptr;
  }
}


#ifdef MAX2022
MSTR HydraSunSky::SubAnimName(int i, bool localized)
{
  switch (i)
  {
  case PB_SUN:
    return GetString(IDS_SUN_PARAMS);
    break;
  case PB_SKY:
    return GetString(IDS_SKY_PARAMS);
    break;
  default:
    return _T("");
  }
}
#else
TSTR HydraSunSky::SubAnimName(int i)
{
  switch (i)
  {
    case PB_SUN:
      return GetString(IDS_SUN_PARAMS);
      break;
    case PB_SKY:
      return GetString(IDS_SKY_PARAMS);
      break;
    default:
      return _T("");
  }
}
#endif // MAX2022



static HydraSunSkyCreateCallBack HydraSkyCreateCB;
CreateMouseCallBack * HydraSunSky::GetCreateMouseCallBack()
{
  HydraSkyCreateCB.SetObj(this);
  return(&HydraSkyCreateCB);
}

ObjectState HydraSunSky::Eval(TimeValue t)
{
  Update(t, ivalid);
  return ObjectState(this);
}



RefResult HydraSunSky::EvalLightState(TimeValue t, Interval & valid, LightState * ls)
{
  if (GetUseLight())
    ls->color        = GetRGBColor(t, valid);
  else
    ls->color        = Point3(0, 0, 0);

  ls->on             = GetUseLight();
  ls->intens         = GetIntensity(t, valid);
  ls->hotsize        = GetHotspot(t, valid);
  ls->fallsize       = GetFallsize(t, valid);

  ls->shape          = CIRCLE_LIGHT;
  ls->aspect         = 1.0f;
  ls->overshoot      = false;
  ls->shadow         = true;
  ls->ambientOnly    = false;
  ls->affectDiffuse  = true;
  ls->affectSpecular = true;
  ls->type           = DIRECT_LGT;

  ls->useNearAtten   = false;
  ls->nearAttenStart = 1.0f;
  ls->nearAttenEnd   = 1000000.0f;

  ls->useAtten       = DECAY_NONE;
  ls->attenStart     = 1.0f;
  ls->attenEnd       = 1000000.0f;

  return REF_SUCCEED;
}




RefResult HydraSunSky::NotifyRefChanged(const Interval & changeInt, RefTargetHandle hTarget, PartID & partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {
    case REFMSG_WANT_SHOWPARAMLEVEL: // message = 264. Monitoring transform sun and update sphere coordinates in paramblock.
    {
      if (ip != nullptr && ip->GetSelNode(0) != nullptr)
      {
        TimeValue t(0);
        Point3 pos = ip->GetSelNode(0)->GetNodeTM(t).GetTrans();
        SetSphereCoordFromDecart(pos, this, t);
      }
      break;
    }
    case REFMSG_CHANGE:
      ivalid.SetEmpty();

      if (hTarget == pblockSun)
      {
        ParamID changing_param = pblockSun->LastNotifyParamID();
        sun_param_blk.InvalidateUI(changing_param);
      }
      else if (hTarget == pblockSky)
      {
        ParamID changing_param = pblockSky->LastNotifyParamID();
        sky_param_blk.InvalidateUI(changing_param);
      }
      break;
  }
  return REF_SUCCEED;
}

RefTargetHandle HydraSunSky::GetReference(int i)
{
  switch (i)
  {
    case PB_SUN:
      return (RefTargetHandle)pblockSun;
      break;
    case PB_SKY:
      return (RefTargetHandle)pblockSky;
      break;
    default:
      return nullptr;
      break;
  }
  return RefTargetHandle();
}

void HydraSunSky::SetReference(int i, RefTargetHandle rtarg)
{
  switch (i)
  {
    case PB_SUN:
      pblockSun = (IParamBlock2*)rtarg;
      break;
    case PB_SKY:
      pblockSky = (IParamBlock2*)rtarg;
      break;
    default:
      break;
  }
}

IParamBlock2 * HydraSunSky::GetParamBlock(int i)
{
  switch (i)
  {
    case PB_SUN:
      return pblockSun;
      break;
    case PB_SKY:
      return pblockSky;
      break;
    default:
      return nullptr;
      break;
  }
}

IParamBlock2 * HydraSunSky::GetParamBlockByID(BlockID id)
{
  if (pblockSun->ID() == id) return pblockSun;
  else if (pblockSky->ID() == id) return pblockSky;
  else                            return nullptr;
}

IOResult HydraSunSky::Load(ILoad * iload)
{
  iload->RegisterPostLoadCallback(new ParamBlock2PLCB(versionsSunPB, NUM_ALLVERSIONS_SUN, &sun_param_blk, this, PB_SUN_REF));
  iload->RegisterPostLoadCallback(new ParamBlock2PLCB(versionsSkyPB, NUM_ALLVERSIONS_SKY, &sky_param_blk, this, PB_SKY_REF));

  return IO_OK;
}



int HydraSunSky::HitTest(TimeValue t, INode * inode, int type, int crossing, int flags, IPoint2 * p, ViewExp * vpt)
{
  if (!vpt || !vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return FALSE;
  }

  HitRegion hitRegion;
  DWORD savedLimits;
  int res = 0;
  Matrix3 m;
  m.IdentityMatrix();

  if (!enable)
    return 0;

  GraphicsWindow* gw  = vpt->getGW();
  Material*       mtl = gw->getMaterial();

  MakeHitRegion(hitRegion, type, crossing, 4, p);
  gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~(GW_ILLUM | GW_BACKCULL));
  GetMat(t, inode, *vpt, m);
  gw->setTransform(m);

  // if we get a hit on the mesh, we're done
  res = mesh->select(gw, mtl, &hitRegion, flags & HIT_ABORTONHIT);
  
  // if not, check the target line, and set the pair flag if it's hit
  if (!res)
  {
    // this special case only works with point selection of targeted lights
    if ((type != HITTYPE_POINT) || !inode->GetTarget())
      return 0;
    // don't let line be active if only looking at selected stuff and target isn't selected
    if ((flags & HIT_SELONLY) && !inode->GetTarget()->Selected())
      return 0;

    gw->clearHitCode();

    //if (res)                        bug in SDK?: always false.
    //  inode->SetTargetNodePair(1);
  }

  gw->setRndLimits(savedLimits);
  return res;
}




void HydraSunSky::GetWorldBoundBox(TimeValue t, INode * inode, ViewExp * vpt, Box3 & box)
{
  if (!vpt || !vpt->IsAlive())
  {
    box.Init();
    return;
  }

  int nv;
  Matrix3 tm;
  tm.IdentityMatrix();
  GetMat(t, inode, *vpt, tm);
  const Point3 loc = tm.GetTrans();
  nv = mesh->getNumVerts();
  box.Init();

  box.IncludePoints(mesh->verts, nv, &tm);
}



void HydraSunSky::GetLocalBoundBox(TimeValue t, INode * inode, ViewExp * vpt, Box3 & box)
{
  if (!vpt || !vpt->IsAlive())
  {
    box.Init();
    return;
  }

  // This light source is real size, so everything scaleFactor is not needed.
  //if (Type() == TYPE_POINT)
  //{
  //  Point3 loc = inode->GetObjectTM(t).GetTrans();
  //  float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(loc) / 360.0f;
  //  box = mesh->getBoundingBox();                //same as GetDeformBBox
  //  box.Scale(scaleFactor);
  //}
  //else
  box = mesh->getBoundingBox();


  //JH 6/13/03 careful, the mesh extents in z are (0, ~-20)
  //thus the scale alone is wrong
  //move the top of the box back to the object space origin
  //#1055329,Boundbox of Omni_light error.
  //if (Type() != TYPE_POINT)
  //{
  box.Translate(Point3(0.0f, 0.0f, -box.pmax.z));
  //  }
}



Interval HydraSunSky::ObjectValidity(TimeValue t)
{
  Interval valid;
  valid.SetInfinite();
  return valid;
}



void HydraSunSky::GetDeformBBox(TimeValue t, Box3 & box, Matrix3 * tm, BOOL useSel)
{
  box = mesh->getBoundingBox(tm);
}



MaxSDK::Graphics::Utilities::MeshEdgeKey LightMeshKey;
MaxSDK::Graphics::Utilities::SplineItemKey LightSplineKey;

unsigned long HydraSunSky::GetObjectDisplayRequirement() const
{
  // we do not want legacy display code called
  // we also do not have any per-view items
  return 0;
}

bool HydraSunSky::PrepareDisplay(const MaxSDK::Graphics::UpdateDisplayContext & prepareDisplayContext)
{
  LightMeshKey.SetFixedSize(false);
  return true;
}


bool HydraSunSky::UpdatePerNodeItems(const MaxSDK::Graphics::UpdateDisplayContext & updateDisplayContext, MaxSDK::Graphics::UpdateNodeContext & nodeContext, MaxSDK::Graphics::IRenderItemContainer & targetRenderItemContainer)
{
  INode* pNode = nodeContext.GetRenderNode().GetMaxNode();

  MaxSDK::Graphics::Utilities::MeshEdgeRenderItem* pMeshItem = new MaxSDK::Graphics::Utilities::MeshEdgeRenderItem(mesh, false, false);


  if (pNode->Dependent())
    pMeshItem->SetColor(Color(ColorMan()->GetColor(kViewportShowDependencies)));
  else if (pNode->Selected())
    pMeshItem->SetColor(Color(GetSelColor()));
  else if (pNode->IsFrozen())
    pMeshItem->SetColor(Color(GetFreezeColor()));
  else
  {
    if (GetUseLight())
    {
      Color color = GetIViewportShadingMgr()->GetLightIconColor(*pNode);
      pMeshItem->SetColor(color);
    }
    else
      pMeshItem->SetColor(Color(0, 0, 0));
  }

  // Main mesh
  MaxSDK::Graphics::CustomRenderItemHandle meshHandle;
  meshHandle.Initialize();
  meshHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
  meshHandle.SetCustomImplementation(pMeshItem);
  MaxSDK::Graphics::ConsolidationData data;
  data.Strategy = &MaxSDK::Graphics::Utilities::MeshEdgeConsolidationStrategy::GetInstance();
  data.Key = &LightMeshKey;
  meshHandle.SetConsolidationData(data);
  targetRenderItemContainer.AddRenderItem(meshHandle);

  // Cone spotlight
  MaxSDK::Graphics::Utilities::SplineRenderItem* pLineItem = new LightConeItem(this);
  MaxSDK::Graphics::CustomRenderItemHandle coneHandle;
  coneHandle.Initialize();
  coneHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
  coneHandle.SetCustomImplementation(pLineItem);
  data.Strategy = &MaxSDK::Graphics::Utilities::SplineConsolidationStrategy::GetInstance();
  data.Key = &LightSplineKey;
  coneHandle.SetConsolidationData(data);
  if (GetUseLight()) targetRenderItemContainer.AddRenderItem(coneHandle);

  // Target line
  pLineItem = new LightTargetLineItem(this);
  MaxSDK::Graphics::CustomRenderItemHandle lineHandle;
  lineHandle.Initialize();
  lineHandle.SetVisibilityGroup(MaxSDK::Graphics::RenderItemVisible_Gizmo);
  lineHandle.SetCustomImplementation(pLineItem);
  data.Strategy = &MaxSDK::Graphics::Utilities::SplineConsolidationStrategy::GetInstance();
  data.Key = &LightSplineKey;
  lineHandle.SetConsolidationData(data);
  targetRenderItemContainer.AddRenderItem(lineHandle);

  return true;
}



void HydraSunSky::Update(TimeValue t, Interval & valid)
{
  // Re-instantiate the shader at the given time
  m_shader = InstantiateShader(t, valid);



  valid &= ivalid;

  BuildSunMesh(t, valid);
}

void RemoveScaling(Matrix3 &tm)
{
  AffineParts ap;
  decomp_affine(tm, &ap);
  tm.IdentityMatrix();
  tm.SetRotate(ap.q);
  tm.SetTrans(ap.t);
}

void HydraSunSky::GetMat(TimeValue t, INode * inode, ViewExp & vpt, Matrix3 & mat)
{
  if (!vpt.IsAlive())
  {
    mat.Zero();
    return;
  }

  mat = inode->GetObjectTM(t);

  // This light source is real size, so everything scaleFactor is not needed.
  //if (Type() == TYPE_POINT)
  //{
    //RemoveScaling(mat);
    //float scaleFactor = vpt.NonScalingObjectSize() * vpt.GetVPWorldWidth(mat.GetTrans()) / 360.0f;
    //mat.Scale(Point3(scaleFactor, scaleFactor, scaleFactor));
  //}
}



void HydraSunSky::SetIntensity(TimeValue t, float f)
{
  pblockSun->SetValue(pb_sun_multiplier, t, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraSunSky::GetIntensity(TimeValue t, Interval & valid)
{
  float f;
  pblockSun->GetValue(pb_sun_multiplier, t, f, valid);
  return f;
}

void HydraSunSky::SetZenith(TimeValue t, float zenith)
{
  pblockSun->SetValue(pb_sun_zenith, t, zenith);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraSunSky::GetZenith(TimeValue t, Interval & valid) const
{
  float f;
  pblockSun->GetValue(pb_sun_zenith, t, f, valid);
  return f;
}

void HydraSunSky::SetAzimuth(TimeValue t, float azimuth)
{
  pblockSun->SetValue(pb_sun_azimuth, t, azimuth);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraSunSky::GetAzimuth(TimeValue t, Interval & valid) const
{
  float f;
  pblockSun->GetValue(pb_sun_azimuth, t, f, valid);
  return f;
}

void HydraSunSky::SetDistanceInViewport(TimeValue t, const float dist)
{
  pblockSun->SetValue(pb_sun_distanceInViewport, t, dist);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraSunSky::GetDistanceInViewport() const
{
  float f;
  pblockSun->GetValue(pb_sun_distanceInViewport, TimeValue(0), f, FOREVER);
  return f;
}

void HydraSunSky::SetSoftShadow(TimeValue t, float softShadow)
{
  pblockSun->SetValue(pb_sun_softShadow, t, softShadow);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

float HydraSunSky::GetSoftShadow(TimeValue t, Interval & valid) const
{
  float f;
  pblockSun->GetValue(pb_sun_softShadow, t, f, valid);
  return f;
}

float HydraSunSky::GetHaze(TimeValue t, Interval & valid) const
{
  float f;
  pblockSky->GetValue(pb_sky_haze, t, f, valid);
  return f;
}




static INode* FindNodeRef(ReferenceTarget *rt);

static INode* GetNodeRef(ReferenceMaker *rm)
{
  if (rm->SuperClassID() == BASENODE_CLASS_ID) return (INode *)rm;
  else return rm->IsRefTarget() ? FindNodeRef((ReferenceTarget *)rm) : NULL;
}

static INode* FindNodeRef(ReferenceTarget *rt)
{
  DependentIterator di(rt);
  ReferenceMaker *rm = NULL;
  INode *nd = NULL;
  while ((rm = di.Next()) != NULL)
  {
    nd = GetNodeRef(rm);
    if (nd) return nd;
  }
  return NULL;
}


void HydraSunSky::SetTarget()
{
  // This is here to notify the viewports that the light type is changing.
  // It has to come before the change so that the viewport parameters don't
  // get screwed up.		// DB 4/29/99
  NotifyDependents(FOREVER, PART_OBJ, REFMSG_OBREF_CHANGE);


  Interface *iface = GetCOREInterface();
  const TimeValue t(iface->GetTime());
  INode *nd = FindNodeRef(this);
  if (nd == NULL)
    return;


  // bug fix
  // when changing the type of a light, this variable needs to be reinitialized or else
  // the shadow generator selector will be locked into its default (Shadow Map)
  //	ASshadType = NULL;
  // >>>>>the above was commented out when i went to make ASShadType a reference
  // not sure whether i shd add to new version of the commented line, which is
  // ReplaceReference( AS_SHADTYPE_REF, NULL );

  Interval v;
  float tdist = GetTDist(t, v);

  if (!isTargeted())
  {
    // get rid of target, assign a PRS controller for all instances
    DependentIterator di(this);
    ReferenceMaker *rm = NULL;
    // iterate through the instances
    while ((rm = di.Next()) != NULL)
    {
      nd = GetNodeRef(rm);
      if (nd)
      {
        INode* tn = nd->GetTarget();
        Matrix3 tm = nd->GetNodeTM(0);
        if (tn)
          iface->DeleteNode(tn);
        Control *tmc = NewDefaultMatrix3Controller();
        tmc->Copy(nd->GetTMController()); // didn't work!!?
        nd->SetTMController(tmc);
        nd->SetNodeTM(0, tm);
        SetTDist(t, tdist);	 //?? which one should this be for
      }
    }
  }

  if (isTargeted())
  {
    DependentIterator di(this);
    ReferenceMaker *rm = NULL;
    // iterate through the instances
    while ((rm = di.Next()) != NULL)
    {
      nd = GetNodeRef(rm);
      if (nd)
      {

        // > 9/9/02 - 2:17pm --MQM-- bug #435799
        // we were losing the light wire color when deleting/restoring the target.
        // (basically, the target had priority on color, so it forced the light
        // wire color to be set to the new target color).
        Color lightWireColor(nd->GetWireColor());

        // create a target, assign lookat controller
        Matrix3 tm = nd->GetNodeTM(t);
        Matrix3 targtm = tm;
        targtm.PreTranslate(Point3(0.0f, 0.0f, -tdist));
        Object *targObject = new TargetObject;
        INode *targNode = iface->CreateObjectNode(targObject);
        TSTR targName;
        targName = nd->GetName();
        targName += GetString(IDS_DB_DOT_TARGET);
        targNode->SetName(targName);
        Control *laControl = CreateLookatControl();
        targNode->SetNodeTM(0, targtm);
        laControl->SetTarget(targNode);
        laControl->Copy(nd->GetTMController());
        nd->SetTMController(laControl);
        targNode->SetIsTarget(1);

        // > 9/9/02 - 2:19pm --MQM-- 
        // force color of the new target...
        targNode->SetWireColor(lightWireColor.toRGB());
      }
    }
  }


  NotifyDependents(FOREVER, PART_OBJ, REFMSG_CHANGE);
  NotifyDependents(FOREVER, PART_OBJ, REFMSG_NUM_SUBOBJECTTYPES_CHANGED); // to redraw modifier stack
  iface->RedrawViews(t);
}



void HydraSunSky::SetUseLight(int onOff)
{
  pblockSun->SetValue(pb_sun_on, TimeValue(0), onOff);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}


BOOL HydraSunSky::GetUseLight(void)
{
  int i;
  pblockSun->GetValue(pb_sun_on, TimeValue(0), i, FOREVER);
  return i;
}


#ifdef MAX2022
void HydraSunSky::SetRGBColor(TimeValue t, const Point3& rgb)
{
  pblockSun->SetValue(pb_sun_color, t, rgb);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}
#else
void HydraSunSky::SetRGBColor(TimeValue t, Point3& rgb)
{
  pblockSun->SetValue(pb_sun_color, t, rgb);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}
#endif // MAX2022


Point3 HydraSunSky::GetRGBColor(TimeValue t, Interval & valid)
{
  Point3 p3;
  pblockSun->GetValue(pb_sun_color, t, p3, valid);
  return p3;
}


void HydraSunSky::SetHotspot(TimeValue time, float f)
{
  pblockSun->SetValue(pb_sun_hotspot, time, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}


float HydraSunSky::GetHotspot(TimeValue t, Interval & valid)
{
  float f;
  pblockSun->GetValue(pb_sun_hotspot, t, f, valid);
  return f;
}

void HydraSunSky::SetFallsize(TimeValue time, float f)
{
  pblockSun->SetValue(pb_sun_falloff, time, f);
  NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);

}

float HydraSunSky::GetFallsize(TimeValue t, Interval & valid)
{
  float f;
  pblockSun->GetValue(pb_sun_falloff, t, f, valid);
  return f;
}


float HydraSunSky::GetAtten(TimeValue t, int which, Interval & valid)
{
  return 100000.0f;
}

void HydraSunSky::SetTDist(TimeValue time, float f)
{
  targDist = f;
}

float HydraSunSky::GetTDist(TimeValue t, Interval & valid)
{
  return targDist;
}

void HydraSunSky::SetConeDisplay(int s, int notify)
{}

BOOL HydraSunSky::GetConeDisplay(void)
{
  return TRUE;
}

void HydraSunSky::SetAtten(TimeValue time, int which, float f)
{}

void Rotate2D(Point3& p, const float angle)
{
  p.x = p.x * cos(angle) - p.y * sin(angle);
  p.y = p.x * sin(angle) + p.y * cos(angle);
}


void HydraSunSky::BuildSunMesh(TimeValue t, Interval& valid)
{
  const float   radius         = GetHotspot(t, valid) / 10.0F;
  constexpr int numRays        = 8;

  //vertices  
  constexpr int nVertForCenter = 1;
  constexpr int nVertsForDisk  = 16;
  constexpr int nVertsForRays  = numRays * 4;
  constexpr int allVertForDisk = nVertForCenter + nVertsForDisk;

  constexpr int nAllVerts      = allVertForDisk + nVertsForRays;

  //faces
  constexpr int nFacesForDisk  = nVertsForDisk;
  constexpr int nFacesForRays  = numRays * 2;
  constexpr int nAllFaces      = nFacesForDisk + nFacesForRays;

  buildMesh.setNumVerts(nAllVerts);
  buildMesh.setNumFaces(nAllFaces);

  // center
  buildMesh.setVert(0, Point3(0.0F, 0.0F, 0.0001F));

  const auto angle = (float)(TWOPI / (float)nVertsForDisk);

  for (int i = 0; i < nVertsForDisk; i++)
  {
    const float currAngle = angle * (float)i;
    buildMesh.setVert(i + nVertForCenter, Point3(cos(currAngle) * radius, sin(currAngle) * radius, 0.0F));
  }

  for (int i = 0; i < nFacesForDisk; i++)
  {
    int currVert2 = i + 2;
    if (currVert2 > nFacesForDisk) currVert2 = 1; // end vert = first for loop circle.

    buildMesh.faces[i].setVerts(0, 1 + i, currVert2);
    buildMesh.faces[i].setEdgeVisFlags(0, 1, 0);
  }

  // Rays
  int d = allVertForDisk;
  buildMesh.setVert(d + 0,  Point3( 1.5000F,  0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 1,  Point3( 1.5000F, -0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 2,  Point3( 2.5000F,  0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 3,  Point3( 2.5000F, -0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 4,  Point3(-0.1000F,  1.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 5,  Point3( 0.1000F,  1.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 6,  Point3(-0.1000F,  2.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 7,  Point3( 0.1000F,  2.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 8,  Point3(-1.1314F,  0.9899F, 0.0F) * radius);
  buildMesh.setVert(d + 9,  Point3(-0.9899F,  1.1314F, 0.0F) * radius);
  buildMesh.setVert(d + 10, Point3(-1.8385F,  1.6971F, 0.0F) * radius);
  buildMesh.setVert(d + 11, Point3(-1.6971F,  1.8385F, 0.0F) * radius);
  buildMesh.setVert(d + 12, Point3(-1.5000F, -0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 13, Point3(-1.5000F,  0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 14, Point3(-2.5000F, -0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 15, Point3(-2.5000F,  0.1000F, 0.0F) * radius);
  buildMesh.setVert(d + 16, Point3(-0.9899F, -1.1314F, 0.0F) * radius);
  buildMesh.setVert(d + 17, Point3(-1.1314F, -0.9899F, 0.0F) * radius);
  buildMesh.setVert(d + 18, Point3(-1.6971F, -1.8385F, 0.0F) * radius);
  buildMesh.setVert(d + 19, Point3(-1.8385F, -1.6971F, 0.0F) * radius);
  buildMesh.setVert(d + 20, Point3( 0.1000F, -1.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 21, Point3(-0.1000F, -1.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 22, Point3( 0.1000F, -2.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 23, Point3(-0.1000F, -2.5000F, 0.0F) * radius);
  buildMesh.setVert(d + 24, Point3( 1.1314F, -0.9899F, 0.0F) * radius);
  buildMesh.setVert(d + 25, Point3( 0.9899F, -1.1314F, 0.0F) * radius);
  buildMesh.setVert(d + 26, Point3( 1.8385F, -1.6971F, 0.0F) * radius);
  buildMesh.setVert(d + 27, Point3( 1.6971F, -1.8385F, 0.0F) * radius);
  buildMesh.setVert(d + 28, Point3( 0.9900F,  1.1314F, 0.0F) * radius);
  buildMesh.setVert(d + 29, Point3( 1.1314F,  0.9899F, 0.0F) * radius);
  buildMesh.setVert(d + 30, Point3( 1.6971F,  1.8385F, 0.0F) * radius);
  buildMesh.setVert(d + 31, Point3( 1.8385F,  1.6971F, 0.0F) * radius);

  d -= 1;
  buildMesh.faces[nFacesForDisk + 0].setVerts(d + 1, d + 2, d + 4);
  buildMesh.faces[nFacesForDisk + 1].setVerts(d + 4, d + 3, d + 1);
  buildMesh.faces[nFacesForDisk + 2].setVerts(d + 5, d + 6, d + 8);
  buildMesh.faces[nFacesForDisk + 3].setVerts(d + 8, d + 7, d + 5);
  buildMesh.faces[nFacesForDisk + 4].setVerts(d + 9, d + 10, d + 12);
  buildMesh.faces[nFacesForDisk + 5].setVerts(d + 12, d + 11, d + 9);
  buildMesh.faces[nFacesForDisk + 6].setVerts(d + 13, d + 14, d + 16);
  buildMesh.faces[nFacesForDisk + 7].setVerts(d + 16, d + 15, d + 13);
  buildMesh.faces[nFacesForDisk + 8].setVerts(d + 17, d + 18, d + 20);
  buildMesh.faces[nFacesForDisk + 9].setVerts(d + 20, d + 19, d + 17);
  buildMesh.faces[nFacesForDisk + 10].setVerts(d + 21, d + 22, d + 24);
  buildMesh.faces[nFacesForDisk + 11].setVerts(d + 24, d + 23, d + 21);
  buildMesh.faces[nFacesForDisk + 12].setVerts(d + 25, d + 26, d + 28);
  buildMesh.faces[nFacesForDisk + 13].setVerts(d + 28, d + 27, d + 25);
  buildMesh.faces[nFacesForDisk + 14].setVerts(d + 29, d + 30, d + 32);
  buildMesh.faces[nFacesForDisk + 15].setVerts(d + 32, d + 31, d + 29);


  for (int i = 0; i < nFacesForRays; ++i)
    buildMesh.faces[nFacesForDisk + i].setEdgeVisFlags(1, 1, 0);

  buildMesh.buildNormals();
  buildMesh.EnableEdgeList(1);

  mesh = &buildMesh;
}


void HydraSunSky::GetConePoints(TimeValue t, float angle, float dist, Point3* q)
{
  // CIRCULAR
  const float rad = angle;  

  for (size_t i = 0; i < NUM_CIRC_PTS; ++i)
  {
    const float a = (float)i * TWOPI / (float)NUM_CIRC_PTS;
    q[i]          = Point3(rad * sinf(a), rad * cosf(a), -dist);
  }

  // alexc - june.12.2003 - made Roll Angle Indicator size proportional to radius
  q[NUM_CIRC_PTS]    = q[0];
  q[NUM_CIRC_PTS].y *= 1.15F; // notch length is 15% of the radius
}



static int GetTargetPoint(TimeValue t, INode *inode, Point3& p)
{
  Matrix3 tmat;
  tmat.IdentityMatrix();
  if (inode->GetTargetTM(t, tmat))
  {
    p = tmat.GetTrans();
    return 1;
  }
  else
    return 0;
}


void HydraSunSky::UpdateTargDistance(TimeValue t, INode * inode)
{
  if (isTargeted())
  {
    Point3 pt;

    if (GetTargetPoint(t, inode, pt))
    {
      const Matrix3 tm   = inode->GetObjectTM(t);
      const float   den  = FLength(tm.GetRow(2));
      const float   dist = (den != 0) ? FLength(tm.GetTrans() - pt) / den : 0.0F;
      targDist           = dist;
      //const TCHAR *buf;
      //buf = FormatUniverseValue(targDist);
      //SetWindowText(GetDlgItem(hGeneralLight, IDC_TARG_DISTANCE), buf);
    }
  }
}




void HydraSunSky::InvalidateUI()
{
  sun_param_blk.InvalidateUI(pblockSun->LastNotifyParamID());
  sky_param_blk.InvalidateUI(pblockSky->LastNotifyParamID());
}



void HydraSunSky::Reset()
{
  ivalid.SetEmpty();

  //for (int i = NSUBMTL; i < NSUBTEX + NSUBMTL; i++)
  //{
  //  if (subTex[i])
  //  {
  //    DeleteReference(i);
  //    subTex[i] = NULL;
  //  }
  //}

  GetHydraSunSkyDesc()->MakeAutoParamBlocks(this);
}




RefTargetHandle HydraSunSky::Clone(RemapDir & remap)
{
  HydraSunSky* hl = new HydraSunSky(false);
  hl->ReplaceReference(PB_SUN, remap.CloneRef(pblockSun));
  hl->ReplaceReference(PB_SKY, remap.CloneRef(pblockSky));
  BaseClone(this, hl, remap);
  return(hl);
}

void Clamp(float &data, const float floor, const float top)
{
  if (data > top)   data = top;
  else if (data < floor) data = floor;
}




int HydraSunSkyCreateCallBack::proc(ViewExp * vpt, int msg, int point, int flags, IPoint2 m, Matrix3 & mat)
{
  if (!vpt || !vpt->IsAlive())
  {
    // why are we here
    DbgAssert(!_T("Invalid viewport!"));
    return FALSE;
  }


#ifdef _OSNAP
  if (msg == MOUSE_FREEMOVE)
  {
#ifdef _3D_CREATE
    vpt->SnapPreview(m, m, NULL, SNAP_IN_3D);
#else
    vpt->SnapPreview(m, m, NULL, SNAP_IN_PLANE);
#endif
  }
#endif

  if (msg == MOUSE_POINT || msg == MOUSE_MOVE)
  {
    // 6/19/01 11:37am --MQM-- wire-color changes
    // since we're now allowing the user to set the color of
    // the light wire-frames, we need to set the initial light 
    // color to yellow, instead of the default random object color.
    ULONG handle;
    INode * node;

    ob->NotifyDependents(FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE);
    node = GetCOREInterface()->GetINodeByHandle(handle);
    if (node)
    {
      Point3 color = GetUIColor(COLOR_LIGHT_OBJ);	// yellow wire color
      node->SetWireColor(RGB(color.x*255.0f, color.y*255.0f, color.z*255.0f));
    }

    TimeValue t(0);

    switch (point)
    {
      case 0: // only happens with MOUSE_POINT msg
        ob->SetTarget();
        p0 = vpt->SnapPoint(m, m, NULL, SNAP_IN_3D);
        break;
      case 1:
        p1 = vpt->SnapPoint(m, m, NULL, SNAP_IN_PLANE);
        p1.z = 1.0f;

        SetSphereCoordFromDecart(p1, ob, t);

        // Position from sphere coord
        const float zenith  = DegToRad(ob->GetZenith(t, FOREVER));
        const float azimuth = DegToRad(ob->GetAzimuth(t, FOREVER));
        const float radius  = ob->GetDistanceInViewport();

        center = SphereToDecart(zenith, azimuth, radius);
        mat.SetTrans(center);
        ob->enable = true;

        if (msg == MOUSE_POINT) return CREATE_STOP;
        break;
    }
  }
  else
  {
    if (msg == MOUSE_ABORT)
      return CREATE_ABORT;
  }

  return TRUE;
}



HydraSunSkyDlgProc::HydraSunSkyDlgProc(HydraSunSky * cb)
{
  hSky = cb;
}




INT_PTR HydraSunSkyDlgProc::DlgProc(TimeValue t, IParamMap2 * map, HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  HydraSunSkyDlgProc *dlg = DLGetWindowLongPtr<HydraSunSkyDlgProc*>(hWnd);

  if (!dlg && msg != WM_INITDIALOG) return FALSE;

  switch (msg)
  {
    case WM_INITDIALOG:
    {
      const float zenith = DegToRad(hSky->GetZenith(t, FOREVER));
      const float azimuth = DegToRad(hSky->GetAzimuth(t, FOREVER));
      const float radius = hSky->GetDistanceInViewport();

      // Color
      Color colorZenit(1.0f, 1.0f, 1.0f);
      Color colorHorizon(1.0f, 0.4f, 0.05f);
      Point3 sunColor = Lerp(colorHorizon, colorZenit, cos(abs(zenith)));
      hSky->SetRGBColor(t, sunColor);

      // Position from sphere coord
      Point3 center;
      Matrix3 mat;
      mat.IdentityMatrix();
      center = SphereToDecart(zenith, azimuth, radius);
      mat.SetTrans(center);

      if (HydraSunSky::ip->GetSelNode(0))
        HydraSunSky::ip->GetSelNode(0)->SetNodeTM(t, mat);
      
      return TRUE;
    }
    case WM_COMMAND:
    {
      if (LOWORD(wParam) == IDC_SUN_ALIGN_TO_PORTALS)
      {
        INode* root = GetCOREInterface()->GetRootNode();

        const int numChildren = root->NumberOfChildren();
        if (numChildren == 0) break; // Nothing selected.

        const Point3 zero(0.0f, 0.0f, 0.0f);
        Box3 bboxLights;

        for (int i = 0; i < numChildren; i++)
        {
          const ObjectState os = root->GetChildNode(i)->EvalWorldState(t).obj;

          if (os.obj->IsSubClassOf(HydraLight_CLASS_ID))
          {
            Box3 currBbox(zero, zero);
            Matrix3 tm  = root->GetChildNode(i)->GetObjectTM(t);
            os.obj->GetDeformBBox(t, currBbox, &tm);

            bboxLights += currBbox;

            Matrix3 mat;
            mat.IdentityMatrix();
            mat.SetTrans(bboxLights.Center());
            hSky->ip->GetSelNode(0)->GetTarget()->SetNodeTM(t, mat);
            hSky->SetHotspot(t, bboxLights.Width().Length() / 2.0f);
            hSky->SetFallsize(t, bboxLights.Width().Length() / 2.0f + 1.0f);
          }
        }
      }
      break;
    }
    case CC_SPINNER_CHANGE:
      if (LOWORD(wParam) == IDC_SUN_ZENITH_SPIN || LOWORD(wParam) == IDC_SUN_AZIMUTH_SPIN || LOWORD(wParam) == IDC_SUN_DISTANCE_IN_VIEWPORT_SPIN)
      {
        const float zenith = DegToRad(hSky->GetZenith(t, FOREVER));
        const float azimuth = DegToRad(hSky->GetAzimuth(t, FOREVER));
        const float radius = hSky->GetDistanceInViewport();

        // Color
        Color colorZenit(1.0f, 1.0f, 1.0f);
        Color colorHorizon(1.0f, 0.4f, 0.05f);
        Point3 sunColor = Lerp(colorHorizon, colorZenit, cos(abs(zenith)));
        hSky->SetRGBColor(t, sunColor);

        // Position from sphere coord
        Point3 center;
        Matrix3 mat;
        mat.IdentityMatrix();
        center = SphereToDecart(zenith, azimuth, radius);
        mat.SetTrans(center);
        hSky->ip->GetSelNode(0)->SetNodeTM(t, mat);
      }
      break;

    case WM_DESTROY:
      break;

    default:
      return FALSE;
  }
  return TRUE;
}


void LightConeItem::Realize(MaxSDK::Graphics::DrawContext& drawContext)
{
  const INode* node = drawContext.GetCurrentNode();

  if (!node)
    return;

  mpLight->SetExtendedDisplay(drawContext.GetExtendedDisplayMode());
  ClearLines();
  BuildConeAndLine(drawContext.GetTime(), drawContext.GetCurrentNode());
  SplineRenderItem::Realize(drawContext);
}


void LightConeItem::BuildCone(TimeValue t, float dist)
{
  if (nullptr == mpLight)
  {
    DbgAssert(true);
    return;
  }

  std::vector<Point3> posArray(NUM_CIRC_PTS + 1);
  Point3 tmpArray[2];
  int i;

  mpLight->GetConePoints(t, mpLight->GetHotspot(t), dist, posArray.data());
  Color color(GetUIColor(COLOR_HOTSPOT));

  // CIRCULAR
  if (mpLight->GetHotspot(t) >= mpLight->GetFallsize(t))
  {
    // draw (far) hotspot circle
    tmpArray[0] = posArray[0];
    tmpArray[1] = posArray[NUM_CIRC_PTS];
    AddLineStrip(tmpArray, color, 2, false, false);
  }

  AddLineStrip(posArray.data(), color, NUM_CIRC_PTS, true, false);

  // draw 4 axial hotspot lines
  for (i = 0; i < NUM_CIRC_PTS; i += SEG_INDEX)
  {
    tmpArray[0] = posArray[i];
    tmpArray[1] = posArray[i];
    tmpArray[1].z += dist;
    AddLineStrip(tmpArray, color, 2, false, false);
  }

  mpLight->GetConePoints(t, mpLight->GetHotspot(t), 0.0F, posArray.data());
  AddLineStrip(posArray.data(), color, NUM_CIRC_PTS, true, false);

  mpLight->GetConePoints(t, mpLight->GetFallsize(t), dist, posArray.data());
  color = GetUIColor(COLOR_FALLOFF);

  if (mpLight->GetHotspot(t) < mpLight->GetFallsize(t))
  {
    // draw (far) fallsize circle
    tmpArray[0] = posArray[0];
    tmpArray[1] = posArray[NUM_CIRC_PTS];

    AddLineStrip(tmpArray, color, 2, false, false);
    tmpArray[0] = Point3(0.0F, 0.0F, 0.0F);
  }

  AddLineStrip(posArray.data(), color, NUM_CIRC_PTS, true, false);

  const float dfar  = posArray[0].z;
  const float dnear = 0.0F;

  // draw axial fallsize lines
  for (i = 0; i < NUM_CIRC_PTS; i += SEG_INDEX)
  {
    tmpArray[0] = posArray[i];	
    tmpArray[0].z = dfar;
    tmpArray[1] = posArray[i];	
    tmpArray[1].z = dnear;

    AddLineStrip(tmpArray, color, 2, false, false);
  }

  mpLight->GetConePoints(t, mpLight->GetFallsize(t), 0.0F, posArray.data());
  AddLineStrip(posArray.data(), color, NUM_CIRC_PTS, true, false);
}


void LightConeItem::BuildConeAndLine(TimeValue t, INode* inode)
{
  Point3 pt;

  if (GetTargetPoint(t, inode, pt))
  {
    const Matrix3 tm   = inode->GetObjectTM(t);
    const float   den  = FLength(tm.GetRow(2));
    const float   dist = (den != 0.0F) ? FLength(tm.GetTrans() - pt) / den : 0.0F;
    mpLight->SetTDist(0, dist);

    if (mpLight->showCone || (mpLight->extDispFlags & EXT_DISP_ONLY_SELECTED))
      BuildCone(t, dist);
  }
}



void LightTargetLineItem::Realize(MaxSDK::Graphics::DrawContext& drawContext)
{
  INode* inode = drawContext.GetCurrentNode();

  if (inode == nullptr /*|| !mpLight->isSpotLight()*/)
    return;

  //if (mpLight->isTargeted())
  //{
  mpLight->SetExtendedDisplay(drawContext.GetExtendedDisplayMode());
  inode->SetTargetNodePair(0);
  const TimeValue t(drawContext.GetTime());
  const Matrix3 tm = inode->GetObjectTM(t);
  Point3 pt;

  if (GetTargetPoint(t, inode, pt))
  {
    const float den = FLength(tm.GetRow(2));
    const float dist = (den != 0) ? FLength(tm.GetTrans() - pt) / den : 0.0f;
    mpLight->targDist = dist;
    Color color(inode->GetWireColor());
    if (!inode->IsFrozen() && !inode->Dependent())
    {
      // 6/22/01 2:18pm --MQM-- 
      // if the user has changed the light's wire-frame color,
      // use that color to draw the target line.
      // otherwise, use the standard target-line color.
      if (color == GetUIColor(COLOR_LIGHT_OBJ))
        color = GetUIColor(COLOR_TARGET_LINE);
    }
    if (mLastColor != color || mLastDist != dist)
    {
      ClearLines();
      const Point3 posArray[2] ={ Point3(0,0,0), Point3(0.0f, 0.0f, -dist) };
      AddLineStrip(posArray, color, 2, false, true);
      mLastColor = color;
      mLastDist = dist;
    }
  }
  //}
  SplineRenderItem::Realize(drawContext);
}


void LightTargetLineItem::OnHit(MaxSDK::Graphics::HitTestContext& /*hittestContext*/, MaxSDK::Graphics::DrawContext& drawContext)
{
  INode* node = drawContext.GetCurrentNode();
  if (node)
    node->SetTargetNodePair(1);
}


void TSpotCreationManager::CreateNewObject()
{
  lgtObject = (HydraSunSky *)cDesc->Create();
  lastPutCount = theHold.GetGlobalPutCount();

  macroRec->BeginCreate(cDesc);  // JBW 4/23/99

                                 // Start the edit params process
  if (lgtObject)
  {
    lgtObject->BeginEditParams((IObjParam*)createInterface, BEGIN_EDIT_CREATE, NULL);
  }
}

RefResult TSpotCreationManager::NotifyRefChanged(const Interval & changeInt, RefTargetHandle hTarget, PartID & partID, RefMessage message, BOOL propagate)
{
  switch (message)
  {

    case REFMSG_PRENOTIFY_PASTE:
    case REFMSG_TARGET_SELECTIONCHANGE:
      if (ignoreSelectionChange)
      {
        break;
      }
      if (lgtObject && lgtNode == hTarget)
      {
        // this will set camNode== NULL;
        theHold.Suspend();
        DeleteReference(0);
        theHold.Resume();
        goto endEdit;
      }
      // fall through

    case REFMSG_TARGET_DELETED:
      if (lgtObject && lgtNode == hTarget)
      {
      endEdit:
        lgtObject->EndEditParams((IObjParam*)createInterface, 0, NULL);
        lgtObject = NULL;
        lgtNode = NULL;
        CreateNewObject();
        attachedToNode = FALSE;
      }
      else if (targNode == hTarget)
      {
        targNode = NULL;
        targObject = NULL;
      }
      break;
  }
  return REF_SUCCEED;
}

void TSpotCreationManager::Begin(IObjCreate * ioc, ClassDesc * desc)
{
  createInterface = ioc;
  cDesc           = desc;
  attachedToNode  = FALSE;
  createCB        = NULL;
  lgtNode         = NULL;
  targNode        = NULL;
  lgtObject       = NULL;
  targObject      = NULL;
  CreateNewObject();
}


void TSpotCreationManager::End()
{
  if (lgtObject)
  {
    lgtObject->EndEditParams((IObjParam*)createInterface, END_EDIT_REMOVEUI, NULL);
    if (!attachedToNode)
    {
      // RB 4-9-96: Normally the hold isn't holding when this 
      // happens, but it can be in certain situations (like a track view paste)
      // Things get confused if it ends up with undo...
      theHold.Suspend();
      //delete lgtObject;
      lgtObject->DeleteAllRefsFromMe();
      lgtObject->DeleteAllRefsToMe();
      lgtObject->DeleteThis();  // JBW 11.1.99, this allows scripted plugin lights to delete cleanly
      lgtObject = NULL;
      theHold.Resume();
      // RB 7/28/97: If something has been put on the undo stack since this object was created, we have to flush the undo stack.
      if (theHold.GetGlobalPutCount() != lastPutCount)
      {
        GetSystemSetting(SYSSET_CLEAR_UNDO);
      }
      macroRec->Cancel();  // JBW 4/23/99
    }
    else if (lgtNode)
    {
      // Get rid of the reference.
      theHold.Suspend();
      DeleteReference(0);  // sets lgtNode = NULL
      theHold.Resume();
    }
  }
}

static BOOL needToss;


int TSpotCreationManager::proc(HWND hwnd, int msg, int point, int flag, IPoint2 m)
{
  int res = CREATE_CONTINUE;
  TSTR targName;
  ViewExp& vpx = createInterface->GetViewExp(hwnd);
  assert(vpx.IsAlive());

  switch (msg)
  {
    case MOUSE_POINT:
      switch (point)
      {
        case 0:
        {
          pt0 = m;
          assert(lgtObject);
          vpx.CommitImplicitGrid(m, flag); //KENNY MERGE
          if (createInterface->SetActiveViewport(hwnd))
          {
            return FALSE;
          }

          if (createInterface->IsCPEdgeOnInView())
          {
            res = FALSE;
            goto done;
          }

          // if lights were hidden by category, re-display them
          GetCOREInterface()->SetHideByCategoryFlags(
            GetCOREInterface()->GetHideByCategoryFlags() & ~HIDE_LIGHTS);

          if (attachedToNode)
          {
            // send this one on its way
            lgtObject->EndEditParams((IObjParam*)createInterface, 0, NULL);
            macroRec->EmitScript();  // JBW 4/23/99

                                     // Get rid of the reference.
            if (lgtNode)
            {
              theHold.Suspend();
              DeleteReference(0);
              theHold.Resume();
            }

            // new object
            CreateNewObject();   // creates lgtObject
          }

          needToss = theHold.GetGlobalPutCount() != lastPutCount;

          theHold.Begin();	 // begin hold for undo
          mat.IdentityMatrix();

          // link it up
          INode *lightNode = createInterface->CreateObjectNode(lgtObject);
          attachedToNode = TRUE;
          assert(lightNode);
          createCB = lgtObject->GetCreateMouseCallBack();
          createInterface->SelectNode(lightNode);

          // Create target object and node
          targObject = new TargetObject;
          assert(targObject);
          targNode = createInterface->CreateObjectNode(targObject);
          assert(targNode);
          targName = lightNode->GetName();
          targName += GetString(IDS_DB_DOT_TARGET);
          macroRec->Disable();
          targNode->SetName(targName);
          macroRec->Enable();

          // hook up camera to target using lookat controller.
          createInterface->BindToTarget(lightNode, targNode);

          // Reference the new node so we'll get notifications.
          theHold.Suspend();
          ReplaceReference(0, lightNode);
          theHold.Resume();

          // Position camera and target at first point then drag.
          mat.IdentityMatrix();
          //mat[3] = vpx.GetPointOnCP(m);
          mat.SetTrans(vpx.SnapPoint(m, m, NULL, SNAP_IN_3D));
          createInterface->SetNodeTMRelConstPlane(lgtNode, mat);
          createInterface->SetNodeTMRelConstPlane(targNode, mat);
          lgtObject->Enable(1);

          ignoreSelectionChange = TRUE;
          createInterface->SelectNode(targNode, 0);
          ignoreSelectionChange = FALSE;
          res = TRUE;

          // 6/19/01 11:37am --MQM-- 
          // set the wire-color of the light to be the default
          // color (yellow)
          if (lgtNode)
          {
            Point3 color = GetUIColor(COLOR_LIGHT_OBJ);
            lgtNode->SetWireColor(RGB(color.x*255.0f, color.y*255.0f, color.z*255.0f));
          }
          break;
        }

        case 1:
          if (Length(m - pt0) < 2)
            goto abort;
          //mat[3] = vpx.GetPointOnCP(m);
          mat.SetTrans(vpx.SnapPoint(m, m, NULL, SNAP_IN_3D));
          macroRec->Disable();   // JBW 4/23/99
          createInterface->SetNodeTMRelConstPlane(targNode, mat);
          macroRec->Enable();

          ignoreSelectionChange = TRUE;
          createInterface->SelectNode(lgtNode);
          ignoreSelectionChange = FALSE;

          theHold.Accept(IDS_DS_CREATE);

          createInterface->AddLightToScene(lgtNode);
          createInterface->RedrawViews(createInterface->GetTime());

          res = FALSE;	// We're done
          break;
      }
      break;

    case MOUSE_MOVE:
      //mat[3] = vpx.GetPointOnCP(m);
      mat.SetTrans(vpx.SnapPoint(m, m, NULL, SNAP_IN_3D));
      macroRec->Disable();   // JBW 4/23/99
      createInterface->SetNodeTMRelConstPlane(targNode, mat);
      macroRec->Enable();
      createInterface->RedrawViews(createInterface->GetTime());

      macroRec->SetProperty(lgtObject, _T("target"),   // JBW 4/23/99
                            mr_create, Class_ID(TARGET_CLASS_ID, 0), GEOMOBJECT_CLASS_ID, 1, _T("transform"), mr_matrix3, &mat);

      res = TRUE;
      break;

    case MOUSE_FREEMOVE:
      SetCursor(UI::MouseCursors::LoadMouseCursor(UI::MouseCursors::Crosshair));
      //Snap Preview
      vpx.SnapPreview(m, m, NULL, SNAP_IN_3D);
      vpx.TrackImplicitGrid(m); //KENNY MERGE
      break;

    case MOUSE_PROPCLICK:
      // right click while between creations
      createInterface->RemoveMode(NULL);
      break;

    case MOUSE_ABORT:
    abort:
      assert(lgtObject);
      lgtObject->EndEditParams((IObjParam*)createInterface, 0, NULL);
      // Toss the undo stack if param changes have been made
      macroRec->Cancel();  // JBW 4/23/99
      theHold.Cancel();	 // deletes both the camera and target.
      if (needToss)
        GetSystemSetting(SYSSET_CLEAR_UNDO);
      lgtNode = NULL;
      targNode = NULL;
      createInterface->RedrawViews(createInterface->GetTime());
      CreateNewObject();
      attachedToNode = FALSE;
      res = FALSE;
  }

done:
  if ((res == CREATE_STOP) || (res == CREATE_ABORT))
    vpx.ReleaseImplicitGrid();

  return res;
}


void HydraSunSkyPBAccessor::Set(PB2Value & v, ReferenceMaker * owner, ParamID id, int tabIndex, TimeValue t)
{
  HydraSunSky* u = (HydraSunSky*)owner;

  float hotspot, falloff;

  switch (id)
  {
    case pb_sun_hotspot:
      u->pblockSun->GetValue(pb_sun_falloff, t, falloff, FOREVER);
      hotspot = v.f;
      if (hotspot > falloff)
        u->pblockSun->SetValue(pb_sun_falloff, t, hotspot + 1.0f);
      break;

    case pb_sun_falloff:
      u->pblockSun->GetValue(pb_sun_hotspot, t, hotspot, FOREVER);
      falloff = v.f;
      if (hotspot > falloff)
        u->pblockSun->SetValue(pb_sun_hotspot, t, falloff - 1.0f);
      break;

    default:
      break;
  }
}


////////////////////////////////////////////////////////////////
// Shader for sun & sky
////////////////////////////////////////////////////////////////
using namespace MaxSDK;
using namespace Max::RapidSLShaders;

INode* HydraSunSky::get_sun_positioner_node() const
{
  if (DbgVerify(pblockSun != nullptr))
    return pblockSun->GetINode(0, 0);
  else
    return nullptr;
}


ISunPositioner* HydraSunSky::get_sun_positioner_object(const TimeValue t) const
{
  // Fetch the node
  INode* const sun_positioner_node = get_sun_positioner_node();
  if (sun_positioner_node != nullptr)
  {
    // Evaluate the node
    const ObjectState object_state = sun_positioner_node->EvalWorldState(t);

    // Fetch the underlying object
    ISunPositioner* const sun_positioner = dynamic_cast<ISunPositioner*>(object_state.obj);
    DbgAssert(sun_positioner == object_state.obj);      // Only sun positioner objects should be assignable
    return sun_positioner;
  }
  else
    return nullptr;
}


void HydraSunSky::set_sun_positioner_object(INode * node)
{
  if (DbgVerify(pblockSun != nullptr))
    DbgVerify(pblockSun->SetValue(0, 0, node));
}


HydraSunSky::ShadingParameters HydraSunSky::EvaluateShadingParameters(const TimeValue t, Interval& validity) const
{
  if (DbgVerify(pblockSun != nullptr && pblockSky != nullptr))
  {
    // Fetch param block values
    const float pb_global_intensity     = 1.0f;// m_param_block->GetFloat(kMainParamID_GlobalIntensity, t, validity);
    const float pb_haze                 = 0.0f;//GetHaze(t, validity)/10.0f;// m_param_block->GetFloat(kMainParamID_Haze, t, validity);
    const bool  pb_sun_enabled          = true;//(pblockSun->GetInt(pb_sun_on, t, validity) != 0);//(m_param_block->GetInt(kMainParamID_SunEnabled, t, validity) != 0);
    const float pb_sun_disc_intensity   = 1.0f;//pb_sun_enabled ? m_param_block->GetFloat(kMainParamID_DiscIntensity, t, validity) : 0.0f;
    const float pb_sun_glow_intensity   = 1.0f;//pb_sun_enabled ? m_param_block->GetFloat(kMainParamID_DiscGlowIntensity, t, validity) : 0.0f;
    const float pb_sun_disc_scale       = 1.0f;//m_param_block->GetFloat(kMainParamID_DiscScale, t, validity);
    const float pb_sky_intensity        = 1.0f;//m_param_block->GetFloat(kMainParamID_SkyIntensity, t, validity);
    const Color pb_night_color          ={ 1.0f, 1.0f, 1.0f };//m_param_block->GetColor(kMainParamID_NightColor, t, validity);
    const float pb_night_intensity      = 1.0f;//m_param_block->GetFloat(kMainParamID_NightIntensity, t, validity);
    const float pb_horizon_height       = 0.0f;//m_param_block->GetFloat(kMainParamID_HorizonHeight, t, validity);
    const float pb_horizon_blur         = 0.1f;//m_param_block->GetFloat(kMainParamID_HorizonBlur, t, validity);
    const Color pb_ground_color         ={ 0.2f, 0.2f, 0.2f };//m_param_block->GetColor(kMainParamID_GroundColor, t, validity);
    const float pb_saturation           = 1.0f;//m_param_block->GetFloat(kMainParamID_Saturation, t, validity);
    const Color pb_tint                 ={ 1.0f, 1.0f, 1.0f };//m_param_block->GetColor(kMainParamID_Tint, t, validity);

    const Point3 original_sun_direction ={ 1.0f, 1.0f, 1.0f };// get_sun_direction(t, validity);

    // Override the Perez coefficients, if needed, according to the measured data provided
    bool use_custom_perez                       = false;
    float perez_A                               = 0.0f;
    float perez_B                               = 0.0f;
    float perez_C                               = 0.0f;
    float perez_D                               = 0.0f;
    float perez_E                               = 0.0f;
    float perez_diffuse_horizontal_illuminance  = 10000.0f;
    float perez_direct_normal_illuminance       = 50000.0f;
    //{
      //const IlluminanceModel illuminance_model = static_cast<IlluminanceModel>(m_param_block->GetInt(kMainParamID_IlluminanceModel, t, validity));
      //ISunPositioner::WeatherMeasurements weather_measurements;
      //if (illuminance_model == kIlluminanceModel_Automatic)
      //{
      //  // Obtain the measured values from the weather data file, if possible
      //  ISunPositioner* const sun_positioner = get_sun_positioner_object(t);
      //  if (sun_positioner != nullptr)
      //  {
      //    use_custom_perez = sun_positioner->GetWeatherMeasurements(weather_measurements, t, validity);
      //  }
      //}
      //else if (illuminance_model == IlluminanceModel_Perez)
      //{
      //weather_measurements.diffuse_horizontal_illuminance = 10000.0f;// m_param_block->GetFloat(kMainParamID_PerezDiffuseHorizontalIlluminance, t, validity);
      //  weather_measurements.direct_normal_illuminance = 50000.0f; //m_param_block->GetFloat(kMainParamID_PerezDirectNormalIlluminance, t, validity);
      //  weather_measurements.dew_point_temperature = 6500;
      //  weather_measurements.dew_point_temperature_valid = false;
      //  use_custom_perez = true;
      //}

      // Derive the Perez coefficients from the weather data, if needed
    //  if (use_custom_perez)
    //  {
    //    // Convert the illuminance values to Perez coefficients. Then use these coefficients for the sky luminance - but keep the Preetham
    //    // chromacity.
    //    IPerezAllWeather* const paw = IPerezAllWeather::GetInterface();
    //    if (DbgVerify(paw != nullptr))
    //    {
    //      // Get the day of year from the sun positioner object - if not available, then use a bogus value
    //      ISunPositioner* const sun_positioner = get_sun_positioner_object(t);
    //      int day_of_year = 0;
    //      if ((sun_positioner == nullptr) || !sun_positioner->GetDayOfYear(day_of_year, t, validity))
    //      {
    //        day_of_year = 183;      // default to mid-year
    //      }
    //      // Calculate the angle between the sun and zenith
    //      // Apparently, the code breaks if we have a value >= to 90.0 deg. So let's clamp it to 89.999 deg.
    //      const float angle_between_sun_and_zenith = std::min(acosf(original_sun_direction.z), 89.999f * float(PI) / 180.0f);

    //      const float atmWaterContent = paw->GetAtmosphericPrecipitableWaterContent(weather_measurements.dew_point_temperature_valid, weather_measurements.dew_point_temperature);
    //      IPerezAllWeather::PerezParams perez_params;
    //      paw->GetPerezParamsFromIlluminances(day_of_year, atmWaterContent, angle_between_sun_and_zenith, weather_measurements.diffuse_horizontal_illuminance, weather_measurements.direct_normal_illuminance, perez_params);
    //      perez_A = perez_params.a;
    //      perez_B = perez_params.b;
    //      perez_C = perez_params.c;
    //      perez_D = perez_params.d;
    //      perez_E = perez_params.e;
    //      perez_diffuse_horizontal_illuminance = weather_measurements.diffuse_horizontal_illuminance;
    //      perez_direct_normal_illuminance = weather_measurements.direct_normal_illuminance;
    //    }
    //  }
    //}

    // Let the shader generator interface generate the shading parameters
    return m_shader_generator.CreateShadingParameters(
      pb_global_intensity,
      pb_haze,
      pb_sun_enabled,
      pb_sun_disc_intensity,
      pb_sun_glow_intensity,
      pb_sun_disc_scale,
      pb_sky_intensity,
      pb_night_color,
      pb_night_intensity,
      pb_horizon_height,
      pb_horizon_blur,
      pb_ground_color,
      pb_saturation,
      pb_tint,
      original_sun_direction,
      use_custom_perez,
      perez_A,
      perez_B,
      perez_C,
      perez_D,
      perez_E,
      perez_diffuse_horizontal_illuminance,
      perez_direct_normal_illuminance,
      t,
      validity);
  }
  else
  {
    return ShadingParameters();
  }
}


std::unique_ptr<HydraSunSky::IShader> HydraSunSky::InstantiateShader(const TimeValue t, Interval& validity) const
{
  const ShadingParameters shading_params = EvaluateShadingParameters(t, validity);
  //return std::unique_ptr<HydraSunSky::IShader>(new Shader(shading_params));
  return std::make_unique<HydraSunSky::Shader>(shading_params);
}


namespace
{
  double square(const double val)
  {
    return val * val;
  }

  double cube(const double val)
  {
    return val * val * val;
  }


  // Returns the sun's illuminance in lux
  Color calculate_sun_illuminance(const Point3& sun_direction, const float turbidity)
  {
    if (sun_direction.z >= 0.0f)
    {
      // Approximated sRGB primaries wavelengths, from "Color Imaging" by Reinhard et al., page 391, in *micrometers*
      const Point3 wavelengths = float3(0.605, 0.540, 0.445);
      // Coefficients for the chosen wavelengths, in m^-1, extracted from Preetham paper (table 2)
      const float3 k0 = float3(12.25, 7.5, 0.3);
      const float l = 0.0035;     // Value suggested by Preetham, converted from cm to m
      const float alpha = 1.3;
      const float beta = 0.04608 * turbidity - 0.04586;
      const float m = 1.0 / (sun_direction.z + 0.15 * ::pow((93.885 - acos(sun_direction.z) * (180.0 / PI)), -1.253));

      // Calculate the various transmission coefficients from Preetham's paper. Note that there's an error in the paper, which the code sample fixes:
      // m isn't part of the exponent.
      const float3 transmission_rayleight = exp(-m * 0.008735f * pow(wavelengths, -4.08));
      const float3 transmission_aeorosol = exp(-m * beta * pow(wavelengths, -alpha));
      const float3 transmission_ozone = exp(-k0 * l * m);
      // Transmission for mixed gases and water vapour are omitted because, at the chose wavelengths, they have no impact (their coefficients
      // in the Preetham paper are 0).

      // Calculate the sun color
      // The illuminance of the sun is 128e3 lux before it goes through the atmosphere (http://en.wikipedia.org/wiki/Sunlight)
      // The color of the sun above the atmosphere is about 5900K (http://en.wikipedia.org/wiki/Color_temperature#The_Sun)
      const float3 sun_color = float3(1.05513096, 0.993359745, 0.903543472);
      float3 sun_luminance = 128e3f * sun_color / rgb_luminance(sun_color);

      // Apply atmospheric transmission to the sun color
      sun_luminance *= transmission_rayleight * transmission_aeorosol * transmission_ozone;
      return { 1000.0f, 1000.0f, 1000.0f };// sun_luminance;
    }
    else
    {
      return { 1000.0f, 1000.0f, 1000.0f };//float3(0.0f, 0.0f, 0.0f);
    }
  }

  // Calculates the integral for the sun's density function
  float calculate_sun_contribution_integral(
    const float sun_disc_angular_radius,
    const float sun_smooth_angular_radius,
    const float sun_glow_angular_radius,
    const float sun_glow_multiplier)
  {
    // Integrate the sun contribution in three parts (all solved with Wolfram Alpha using the expressions below):
    // * the solid sun disc: integral[0 to a]sin(x) dx
    // * the smooth sun disc edge: integral[a to b]((-cos(s*PI) + 1.0) * 0.5) * sin(x) dx, where s=(x - b) / (a - b)
    //                           = integral[a to b]((-0.5*cos((x - b) / (a - b)*PI) + 0.5)) * sin(x) dx
    // * the glow: integral[0 to c] s exp(-(1.0 - s) 6.0) sin(x) dx, where s=(1.0 - (x / c))
    //           = integral[0 to c] s exp(6s - 6) sin(x) dx, where s=(1.0 - (x / c))
    // Note that this integral is dependent on the sun/glow interpolation used in the shader itself!
    // Note: it's crucial to use doubles here - floats don't have enough precision and provide bogus results.
    const double a = sun_disc_angular_radius;
    const double b = sun_smooth_angular_radius;
    const double c = sun_glow_angular_radius;
    const double pi = PI;
    const double sun_disc_integral = 1.0 - cos(a);
    const double sun_smooth_integral = ((square(pi) - 2 * square(a - b))*cos(a) - square(pi)*cos(b)) / (2 * (a - b + pi)*(-a + b + pi));
    const double sun_glow_integral = (c*((0.0892351 - 0.00247875*square(c))*sin(c) + cube(c) + 24.*c + 0.029745*c*cos(c))) / square((square(c) + 36.));

    // Our integrations were performed over sin(x), which integrates to 2 over the sphere. We want to scale to steradians, which should integrate
    // to 4PI over the sphere, so we must multiply by 2PI.
    const double sun_contribution_integral = 2.0 * PI * (
      sun_disc_integral
      + sun_smooth_integral
      + sun_glow_multiplier * sun_glow_integral);

    return sun_contribution_integral;
  }

  // Estimates the diffuse illuminance, on a horizontal surface, from the sky by way of sampling.
  // The returned value is an RGB illuminance. The luminance (Y) of the color is returned as a separate parameter reference.
  float3 estimate_diffuse_horizontal_sky_illuminance(
    const float3 sun_direction,
    const float3 night_luminance,
    const float night_falloff,
    const Point3& perez_A, const Point3& perez_B, const Point3& perez_C, const Point3& perez_D, const Point3& perez_E, const Point3& perez_Z,
    float& Y)
  {
    float3 result = float3(0.0);
    Y = 0.0f;

    // Generate a bunch of samples, following a lambertian distribution
    const unsigned int num_horizontal_samples = 30;
    const unsigned int num_vertical_samples = num_horizontal_samples / 2;
    for (unsigned int horizontal_sample_index = 0; horizontal_sample_index < num_horizontal_samples; ++horizontal_sample_index)
    {
      const float horizontal_sample = horizontal_sample_index / float(num_horizontal_samples);
      const float horizontal_angle = (horizontal_sample * 2.0 * PI);
      for (unsigned int vertical_sample_index = 0; vertical_sample_index < num_vertical_samples; ++vertical_sample_index)
      {
        const float vertical_sample = (vertical_sample_index / float(num_vertical_samples));
        const float z = 1.0 - vertical_sample; // 1.0-x, to force sampling the zenith and disable sampling the horizon
        const float x = sqrt(1.0 - z * z) * cos(horizontal_angle);
        const float y = sqrt(1.0 - z * z) * sin(horizontal_angle);
        const float3 sample_direction = float3(x, y, z);

        const float cosine_angle_between_sun_and_ray_directions = dot(sample_direction, sun_direction);
        const float angle_between_sun_and_ray_directions = acos(cosine_angle_between_sun_and_ray_directions);
        const float3 contrib_rgb = compute_sky_contribution_rgb(
          sun_direction,
          sample_direction,
          angle_between_sun_and_ray_directions, cosine_angle_between_sun_and_ray_directions,
          night_luminance,
          night_falloff,
          perez_A, perez_B, perez_C, perez_D, perez_E, perez_Z);
        const float3 perez_Yxy = compute_perez_sky_Yxy(
          sun_direction,
          sample_direction,
          angle_between_sun_and_ray_directions, cosine_angle_between_sun_and_ray_directions,
          night_falloff,
          perez_A, perez_B, perez_C, perez_D, perez_E, perez_Z);

        result += sample_direction.z * contrib_rgb;
        Y += perez_Yxy.x;
      }
    }

    // Multiply by PI to complete integration of cosine (which integrates to PI)
    result *= PI / (num_horizontal_samples * num_vertical_samples);
    Y *= PI / (num_horizontal_samples * num_vertical_samples);
    return { 1000.0f,1000.0f,1000.0f };//result;
  }
}


//==================================================================================================
// class PhysicalSunSkyEnv::Shader
//==================================================================================================

HydraSunSky::Shader::Shader(const ShadingParameters& shading_parameters)
  : m_shading_parameters(shading_parameters)
{

}

HydraSunSky::Shader::~Shader()
{

}

Color HydraSunSky::Shader::Evaluate(const Point3& direction) const
{
  // Call the RapidSL shader code
  const Color result = Max::RapidSLShaders::compute_sunsky_env_color(
    direction,
    m_shading_parameters.horizon_height,
    m_shading_parameters.horizon_blur,
    m_shading_parameters.global_multiplier,
    m_shading_parameters.sun_illuminance,
    m_shading_parameters.sun_luminance,
    m_shading_parameters.sun_glow_intensity,
    m_shading_parameters.sky_contribution_multiplier,
    m_shading_parameters.sky_ground_contribution,
    m_shading_parameters.sun_disc_angular_radius,
    m_shading_parameters.sun_smooth_angular_radius,
    m_shading_parameters.sun_glow_angular_radius,
    m_shading_parameters.color_saturation,
    m_shading_parameters.color_tint,
    m_shading_parameters.ground_color,
    m_shading_parameters.night_luminance,
    m_shading_parameters.night_falloff,
    m_shading_parameters.sun_direction,
    m_shading_parameters.sun_direction_for_sky_contribution,
    m_shading_parameters.perez_A,
    m_shading_parameters.perez_B,
    m_shading_parameters.perez_C,
    m_shading_parameters.perez_D,
    m_shading_parameters.perez_E,
    m_shading_parameters.perez_Z);

  return { 1.0f,1.0f,1.0f };//result;
}


//==================================================================================================
// class PhysicalSunSkyEnv::ShaderGenerator
//==================================================================================================

HydraSunSky::ShadingParameters HydraSunSky::ShaderGenerator::CreateShadingParameters(
  const float global_intensity,
  const float haze,
  const bool sun_enabled,
  const float sun_disc_intensity,
  const float sun_glow_intensity,
  const float sun_disc_scale,
  const float sky_intensity,
  const Color night_color,
  const float night_intensity,
  const float horizon_height_deg,
  const float horizon_blur_deg,
  const Color ground_color,
  const float saturation,
  const Color tint,
  const Point3 sun_direction,
  const bool use_custom_perez_coefficients,
  const float custom_perez_A,
  const float custom_perez_B,
  const float custom_perez_C,
  const float custom_perez_D,
  const float custom_perez_E,
  const float custom_perez_diffuse_horizontal_illuminance,
  const float custom_perez_direct_normal_illuminance,
  const TimeValue t,
  Interval& validity) const
{
  ShadingParameters params;

  // Fetch tone operator and global system values
  //ToneOperatorInterface* const toneOpInt = dynamic_cast<ToneOperatorInterface*>(GetCOREInterface(TONE_OPERATOR_INTERFACE));
  //ToneOperator* const toneOp = (toneOpInt != nullptr) ? toneOpInt->GetToneOperator() : nullptr;
  const float physical_scale = 1500.0f;// ((toneOp != nullptr) && (toneOp->Active(t))) ? toneOp->GetPhysicalUnit(t, validity) : 1500.0f;     // 1500 is the system-wide default value
  const float global_light_level = GetCOREInterface()->GetLightLevel(t, validity);
  const Color global_light_tint = GetCOREInterface()->GetLightTint(t, validity);

  // Remap haze (in [0,1]) to turbidity in [2, 17] (required by Preetham paper)
  const float turbidity = (haze * 15.0f) + 2.0f;

  // Get sun direction
  const Point3 original_sun_direction = sun_direction;

  // Internal horizon height uses the sine of the angle
  params.horizon_height = sinf(horizon_height_deg * (PI / 180.0f));
  params.horizon_blur = sinf(horizon_blur_deg * (PI / 180.0f));
  // The sun direction is adjusted according to the horizon height, such that the sun still sets at the horizon.
  const Point3 adjusted_sun_direction = Normalize(Point3(original_sun_direction.x, original_sun_direction.y, original_sun_direction.z - params.horizon_height));

  // Thee result is multiplied by PI to work around the problem that lighting is generally too bright by PI (everywhere). This problem originates
  // from the lack of a division by PI in the standard material's lambertian BRDF (see MAXX-21782).
  params.global_multiplier = global_intensity * global_light_tint * global_light_level * PI / physical_scale;

  params.sun_glow_intensity = 0.0025f * sun_glow_intensity;
  // 0.00465 is the sun's average angular radius, in radians
  params.sun_disc_angular_radius = 0.00465 * sun_disc_scale;
  // We make the sun smoother as the glow intensity is increased
  params.sun_smooth_angular_radius = params.sun_disc_angular_radius * (1.01f + sun_glow_intensity * 0.2f);
  params.sun_glow_angular_radius = params.sun_smooth_angular_radius * 10.0f;
  params.color_saturation = saturation;
  params.color_tint = tint;
  params.ground_color = ground_color;
  // Same bogus multiplication by PI as above (see MAXX-21782)
  params.night_luminance = night_color * night_intensity;
  params.sun_direction = adjusted_sun_direction;

  // If the sun is below the horizon, we want a smooth transition to a black sky (to avoid an abrupt transition to black when the sun dips
  // below the horizon). We therefore render the sky as if the sun were very slightly above the horizon, but apply a falloff such that the sky 
  // become completely black when at nadir. 
  params.sky_contribution_multiplier = sky_intensity;
  params.sun_direction_for_sky_contribution = adjusted_sun_direction;
  const float sun_z_clamp_for_sky = 0.01f;
  if (params.sun_direction_for_sky_contribution.z <= sun_z_clamp_for_sky)
  {
    // See http://en.wikipedia.org/wiki/Twilight#Civil_twilight
    // Transition to black when we reach 18 degrees below the horizon
    const float z_black = -cos((90.0f - 18.0f) * PI / 180.0f);

    if (params.sun_direction_for_sky_contribution.z > z_black)
    {
      // Calculate falloff in [0,1]
      params.night_falloff = 1.0f - ((params.sun_direction_for_sky_contribution.z - sun_z_clamp_for_sky) / (z_black - sun_z_clamp_for_sky));
      // Apply exponential falloff
      params.night_falloff = powf(params.night_falloff, 4.0f);
    }
    else
    {
      // Sky is black
      params.night_falloff = 0.0f;
    }

    // Clamp sun height for purposes of sky contribution
    params.sun_direction_for_sky_contribution.z = sun_z_clamp_for_sky;
    params.sun_direction_for_sky_contribution = normalize(params.sun_direction_for_sky_contribution);
  }
  else
  {
    params.night_falloff = 1.0f;
  }

  // Calculate the Perez coefficients, according to the Preetham paper
  {
    params.perez_A.x = 0.1787 * turbidity + -1.4630;
    params.perez_B.x = -0.3554 * turbidity + 0.4275;
    params.perez_C.x = -0.0227 * turbidity + 5.3251;
    params.perez_D.x = 0.1206 * turbidity + -2.5771;
    params.perez_E.x = -0.0670 * turbidity + 0.3703;
    params.perez_A.y = -0.0193 * turbidity - 0.2592
      // This piece of magic eliminates the pink shade at the horizon
      - 0.03 + sqrt(params.sun_direction_for_sky_contribution.z) * 0.09;
    params.perez_B.y = -0.0665 * turbidity + 0.0008;
    params.perez_C.y = -0.0004 * turbidity + 0.2125;
    params.perez_D.y = -0.0641 * turbidity - 0.8989;
    params.perez_E.y = -0.0033 * turbidity + 0.0452;
    params.perez_A.z = -0.0167 * turbidity - 0.2608;
    params.perez_B.z = -0.0950 * turbidity + 0.0092;
    params.perez_C.z = -0.0079 * turbidity + 0.2102;
    params.perez_D.z = -0.0441 * turbidity - 1.6537;
    params.perez_E.z = -0.0109 * turbidity + 0.0529;
    // Calculate zenith values
    const float T = turbidity;
    const float T2 = T * T;
    const float theta_s = acos(params.sun_direction_for_sky_contribution.z);
    const float theta_s_2 = theta_s * theta_s;
    const float theta_s_3 = theta_s_2 * theta_s;
    const float chi = (4.0 / 9.0 - T / 120.0) * (PI - 2.0 * theta_s);
    params.perez_Z.x = 1000.0f * (((4.0453 * T - 4.9710) * tan(chi)) + (-0.2155 * T + 2.4192));
    params.perez_Z.y =
      (T2 * 0.00166 + T * -0.02903 + 0.11693) * theta_s_3
      + (T2 * -0.00375 + T * 0.06377 + -0.21196) * theta_s_2
      + (T2 * 0.00209 + T * -0.03202 + 0.06052) * theta_s
      + (T * 0.00394 + 0.25886);
    params.perez_Z.z =
      (T2 * 0.00275 + T * -0.04214 + 0.15346) * theta_s_3
      + (T2 * -0.00610 + T * 0.08970 + -0.26756) * theta_s_2
      + (T2 * 0.00317 + T * -0.04153 + 0.06670) * theta_s
      + (T * 0.00516 + 0.26688);

    // Override the Perez coefficients, if needed
    if (use_custom_perez_coefficients)
    {
      params.perez_A.x = custom_perez_A;
      params.perez_B.x = custom_perez_B;
      params.perez_C.x = custom_perez_C;
      params.perez_D.x = custom_perez_D;
      params.perez_E.x = custom_perez_E;
    }
  }

  // Calculate the overall/average/integrated contribution from the sky (only the sky, not the sun), to be used for fudging
  // illumination on the procedural ground plane.
  float nominal_diffuse_horizontal_illuminance_Y = 0.0f;
  const float3 nominal_diffuse_horizontal_illuminance_rgb =
    estimate_diffuse_horizontal_sky_illuminance(
    params.sun_direction_for_sky_contribution,
    params.night_luminance,
    params.night_falloff,
    params.perez_A, params.perez_B, params.perez_C, params.perez_D, params.perez_E, params.perez_Z,
    nominal_diffuse_horizontal_illuminance_Y);

  // If a diffuse horizontal illuminance was specified, we try to match it by normalizing the sky illuminance
  if (use_custom_perez_coefficients && (custom_perez_diffuse_horizontal_illuminance > 0.0f))
  {
    params.sky_contribution_multiplier *= (custom_perez_diffuse_horizontal_illuminance / nominal_diffuse_horizontal_illuminance_Y);
  }

  // Fake ground plane illuminance from the sky: pre-calculated diffuse reflected luminance for 100% reflectivity (ground color applied in shader)
  params.sky_ground_contribution = params.sky_contribution_multiplier * nominal_diffuse_horizontal_illuminance_rgb / PI;

  // Calculate the sun's total illuminance
  params.sun_illuminance = sun_disc_intensity * calculate_sun_illuminance(adjusted_sun_direction, turbidity);

  // Normalize the sun's illuminance if a custom illuminance was passed
  if (use_custom_perez_coefficients && (custom_perez_direct_normal_illuminance > 0.0f))
  {
    params.sun_illuminance *= custom_perez_direct_normal_illuminance / rgb_luminance(params.sun_illuminance);
  }

  // Calculate the sun's luminance, relative to its distribution function (such that the sun+glow will, overall, emit
  // "sun_illuminance".
  const float sun_contribution_integral = calculate_sun_contribution_integral(
    params.sun_disc_angular_radius,
    params.sun_smooth_angular_radius,
    params.sun_glow_angular_radius,
    params.sun_glow_intensity);
  params.sun_luminance = params.sun_illuminance / sun_contribution_integral;

  return params;
}


std::unique_ptr<HydraSunSky::IShader> HydraSunSky::ShaderGenerator::InstantiateShader(const ShadingParameters& shading_parameters) const
{
  //return std::unique_ptr<HydraSunSky::IShader>(new Shader(shading_parameters));
  return std::make_unique<HydraSunSky::Shader>(shading_parameters);
}

void HydraSunSky::ShaderGenerator::init()
{
  // nothing to do
}
