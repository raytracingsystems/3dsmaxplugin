/**********************************************************************
 *<
	FILE: target.h

	DESCRIPTION:  Defines a Target Object Class

	CREATED BY: Dan Silva

	HISTORY: created 11 January 1995

 *>	Copyright (c) 1994, All Rights Reserved.
 **********************************************************************/

#ifndef __TARGET__H__ 

#define __TARGET__H__

class TargetObject: public GeomObject
{			   
	friend class TargetObjectCreateCallBack;
	friend INT_PTR CALLBACK TargetParamDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );
	
		// Mesh cache
		static HWND hSimpleCamParams;
		static IObjParam* iObjParams;		
		static Mesh mesh;		
		static int meshBuilt;

		void GetMat(TimeValue t, INode* inode, ViewExp& vpt, Matrix3& tm);
		void BuildMesh();

	//  inherited virtual methods for Reference-management
		RefResult NotifyRefChanged(const Interval& changeInt, RefTargetHandle hTarget, PartID& partID, 
			                         RefMessage message, BOOL propagate) override;

	public:
		TargetObject();
		~TargetObject() override;

		//  inherited virtual methods:

		// From BaseObject
		int  HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) override;
		void Snap   (TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt)                    override;
		int  Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)                                     override;
		CreateMouseCallBack* GetCreateMouseCallBack()                                                        override;
		void BeginEditParams(IObjParam *ip, ULONG flags,Animatable *prev)                                    override;
		void EndEditParams  (IObjParam *ip, ULONG flags,Animatable *next)                                    override;

#ifdef MAX2022
		[[nodiscard]] const MCHAR* GetObjectName(bool localized) const override { return GetString(IDS_DB_TARGET); }
#else
		[[nodiscard]] const TCHAR* GetObjectName() override { return GetString(IDS_DB_TARGET); }
#endif // MAX2022


		// From Object
		ObjectState  Eval(TimeValue time)  override;
		void         InitNodeName(MSTR& s) override { s = GetString(IDS_DB_TARGET); }
		ObjectHandle ApplyTransform(Matrix3& matrix);

		[[nodiscard]] int UsesWireColor()                override { return 1; }						// 6/19/01 2:26pm --MQM-- now we can set object color
		[[nodiscard]] int IsRenderable()                 override { return 0; }

		// From GeomObject
		void GetWorldBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box) override;
		void GetLocalBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box) override;
		void GetDeformBBox   (TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel) override;
				
		int IntersectRay(TimeValue 	t, Ray& r, float& at, Point3& norm)         override { return 0; }

		ObjectHandle CreateTriObjRep(TimeValue t);  // for rendering, also for deformation		
		[[nodiscard]] BOOL HasViewDependentBoundingBox()            override { return true; }
																											          
		// From Animatable 																          
		void DeleteThis()                                           override { delete this; }

		[[nodiscard]] Class_ID ClassID()                            override { return Class_ID(TARGET_CLASS_ID, 0); }

#ifdef MAX2022
		void     GetClassName(MSTR& s, bool localized = true) const override { s = GetString(IDS_DB_TARGET_CLASS); }
#else  		
		void     GetClassName(TSTR& s)                              override { s = GetString(IDS_DB_TARGET_CLASS); }
#endif // MAX2022

		[[nodiscard]] int IsKeyable() const { return 1; }

		LRESULT CALLBACK TrackViewWinProc( HWND hwnd,  UINT message, WPARAM wParam,   LPARAM lParam ) override { return 0; }

		// From ref.h
		RefTargetHandle Clone(RemapDir& remap)                      override;
																					                      
		// IO																	                      
		IOResult Save(ISave* isave)                                 override;
		IOResult Load(ILoad* iload)                                 override;
		
		void AddRenderitem(const MaxSDK::Graphics::UpdateDisplayContext& updateDisplayContext,
			                       MaxSDK::Graphics::UpdateNodeContext& nodeContext,
			                       MaxSDK::Graphics::IRenderItemContainer& targetRenderItemContainer);

		unsigned long GetObjectDisplayRequirement() const override;

		bool PrepareDisplay(const MaxSDK::Graphics::UpdateDisplayContext& prepareDisplayContext) override;

		bool UpdatePerNodeItems(const MaxSDK::Graphics::UpdateDisplayContext& updateDisplayContext,
			                            MaxSDK::Graphics::UpdateNodeContext& nodeContext,
			                            MaxSDK::Graphics::IRenderItemContainer& targetRenderItemContainer) override;
	};

#endif

